<?php

use Tiixstone\Stats\Health;

class HealthTest extends \PHPUnit\Framework\TestCase
{
    public function test_it_works()
    {
        $health = new Health(30);

        $this->assertEquals(30, $health->total());
        $this->assertEquals(30, $health->current->get());
        $this->assertEquals(30, $health->maximum->get());
        $this->assertEquals(0, $health->buff->get());

        $this->assertFalse($health->isDamaged());

        $health->damage(10);
        $this->assertEquals(20, $health->total());
        $this->assertTrue($health->isDamaged());

        $health->heal(5);
        $this->assertEquals(25, $health->total());

        $this->assertFalse($health->isDead());
        $this->assertTrue($health->isAlive());

        $health->damage(50);
        $this->assertTrue($health->isDead());
        $this->assertFalse($health->isAlive());

        $health->buff->set(5);
        $this->assertTrue($health->isBuffed());
    }
}