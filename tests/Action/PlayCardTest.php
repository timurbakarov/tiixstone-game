<?php

namespace Tiixstone\Tests\Action;

use Tiixstone\Action;
use Tiixstone\Game;
use Tiixstone\Factory;
use Tiixstone\Exception;
use Tiixstone\Card\Alias\Sheep;
use PHPUnit\Framework\TestCase;

class PlayCardTest extends TestCase
{
    /**
     * @var Game
     */
    protected $game;

    public function setUp()
    {
        $this->game = Factory::createForTest([
            Sheep::create(),
            Sheep::create(),
            Sheep::create(),
            Sheep::create(),
            Sheep::create(),
            Sheep::create(),
            Sheep::create(),
            Sheep::create(),
            Sheep::create(),
            Sheep::create(),
        ], [
            Sheep::create(),
            Sheep::create(),
            Sheep::create(),
            Sheep::create(),
            Sheep::create(),
            Sheep::create(),
            Sheep::create(),
            Sheep::create(),
            Sheep::create(),
            Sheep::create(),
        ]);
        $this->game->start();
    }

    public function testMinionPlayed()
    {
        $this->assertEquals(0, $this->game->currentPlayer()->board->count());
        $this->assertEquals(4, $this->game->currentPlayer()->hand->count());

        (new Action\PlayCard($this->game->currentPlayer()->hand->first()->id()))->run($this->game);

        $this->assertEquals(1, $this->game->currentPlayer()->board->count());
        $this->assertEquals(3, $this->game->currentPlayer()->hand->count());
    }

    public function testNoVacantPlaceForMinion()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('There is no vacant place on board');

        $game = Factory::createForTest();
        $game->start();

        $game->currentPlayer()->hand->appendMany([Sheep::create()]);
        $game->currentPlayer()->board->appendMany([Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create()]);

        (new Action\PlayCard($game->currentPlayer()->hand->first()->id()))->run($game);
    }

//    public function testPlayerDoesNotHaveCardWithRequiredKey()
//    {
//        $this->expectException(Exception::class);
//        $this->expectExceptionMessage(Exception::PLAYER_DOESNT_HAVE_CARD_IN_HAND_WITH_REQUIRED_KEY);
//
//        (new Action\PlayCard((Sheep::create())->id()))->run($this->game);
//    }

    public function testPlayerDoenNotHaveEnoughManaToPlayCard()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionCode(Exception::PLAYER_DOESNT_HAVE_ENOUGH_MANA_TO_PLAY_CARD);

        $card = $this->game->currentPlayer()->hand->first();
        (new Action\PlayCard($card->id()))->run($this->game);

        $card = $this->game->currentPlayer()->hand->first();
        (new Action\PlayCard($card->id()))->run($this->game);
    }
}