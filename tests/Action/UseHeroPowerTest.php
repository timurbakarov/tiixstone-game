<?php

namespace Tiixstone\Tests\Action;

use Tiixstone\Block;
use Tiixstone\Exception;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\ChillwindYeti;
use Tiixstone\Action\UseHeroPowerRefactored;

class UseHeroPowerTest extends TestCase
{
    public function test()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $yeti = ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->idlePlayer(), $yeti),
        ]);

        $game->action(new UseHeroPowerRefactored($yeti));

        $this->assertEquals(4, $yeti->health->total());
        $this->assertEquals(8, $game->currentPlayer()->availableMana());
    }

    public function testUseMana()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Player does not have enough mana');

        $game = Factory::createForTestWithDecks();
        $game->start();

        $game->action(new UseHeroPowerRefactored($game->currentPlayer()->hero));
    }

    public function testCanNotUseTwice()
    {
        $this->expectException(Exception::class);

        $game = Factory::createForTestWithDecks();
        $game->start();

        $yeti = ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->idlePlayer(), $yeti),
        ]);

        $game->action(new UseHeroPowerRefactored($yeti));
        $game->action(new UseHeroPowerRefactored($yeti));
    }

//    public function testCanUseNextTurn()
//    {
//        $game = Factory::createForTestWithDecks();
//        $game->start();
//
//        $yeti = ChillwindYeti::create();
//        $game->idlePlayer()->board->append($yeti);
//
//        $game->currentPlayer()->setAvailableMana(10);
//
//        (new UseHeroPower($yeti->id()))->run($game);
//
//        $game->action(new EndTurn());
//        $game->action(new EndTurn());
//
//        (new UseHeroPower($yeti->id()))->run($game);
//
//        $this->assertEquals(3, $yeti->health->total());
//    }
}