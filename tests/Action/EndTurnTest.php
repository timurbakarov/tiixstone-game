<?php

namespace Tiixstone\Tests\Action;

use Tiixstone\Action;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Manager\GameManager;

class EndTurnTest extends TestCase
{
    public function testMoveIncrements()
    {
        $game = Factory::createForTest();
        $game->start();
        $this->assertEquals(1, $game->turnNumber());

        $game->action(new Action\EndTurn());
        $this->assertEquals(2, $game->turnNumber());

        $game->action(new Action\EndTurn());
        $this->assertEquals(3, $game->turnNumber());
    }
}