<?php

namespace Tiixstone\Tests\Action;

use Tiixstone\Block;
use Tiixstone\Exception;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias\Sheep;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\EndTurn;
use Tiixstone\Card\Alias\ChillwindYeti;
use Tiixstone\Action\AttackCharacter;

class AttackCharacterTest extends TestCase
{
    public function testHeroTakesDamage()
    {
        $game = Factory::createForTest(Factory::createCardsArray(30), Factory::createCardsArray(30));
        $game->start();
        $this->assertEquals(30, $game->idlePlayer()->hero->health->total());

        (new Action\PlayCard($game->currentPlayer()->hand->first()->id()))->run($game);
        $game->action(new EndTurn());
        $game->action(new EndTurn());

        (new Action\AttackCharacter($game->currentPlayer()->board->first()->id(),  $game->idlePlayer()->hero->id()))->run($game);

        $this->assertEquals(29, $game->idlePlayer()->hero->health->total());
    }

    public function testAttackerCanNotAttackIfAttackRateIsZero()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Attack should be greater than 0');

        $game = Factory::createForTest();
        $game->start();

        $card = Sheep::create();

        $game->resolver->resolveBlock($game, new Block\SummonMinion($game->currentPlayer(), $card));

        $targetCard = ChillwindYeti::create();

        $game->resolver->resolveBlock($game, new Block\SummonMinion($game->idlePlayer(), $targetCard));

        $game->action(new EndTurn());

        $targetCard->attack->current->set(0);

        (new AttackCharacter($targetCard->id(), $card->id()))->run($game);
    }

    public function test_invalid_attacker()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Invalid attacker character');

        $game = Factory::createForTest();
        $game->start();

        $card = Sheep::create();
        $targetCard = ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->player1, $card),
            new Block\SummonMinion($game->player2, $targetCard),
        ]);

        $game->action(new Action\EndTurn());

        $invalidAttacker = Sheep::create();

        $game->action(new Action\AttackCharacterRefactored($invalidAttacker, $card));
    }

    public function test_invalid_target()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Target should be an enemy');

        $game = Factory::createForTest();
        $game->start();

        $sheep = Sheep::create();
        $yeti = ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->player1, $sheep),
            new Block\SummonMinion($game->player2, $yeti),
        ]);

        $game->action(new Action\EndTurn());

        $invalidTarget = Sheep::create();

        $game->action(new Action\AttackCharacterRefactored($yeti, $invalidTarget));
    }

    public function testMinionsTakeDamage()
    {
        $game = Factory::createForTest();
        $game->start();

        $card = Sheep::create();
        $targetCard = ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->player1, $card),
            new Block\SummonMinion($game->player2, $targetCard),
        ]);

        $game->action(new EndTurn());
        $game->action(new EndTurn());

        (new AttackCharacter($card->id(), $targetCard->id()))->run($game);

        $this->assertFalse($game->player1->board->has($card->id()));
        $this->assertTrue($game->player2->board->has($targetCard->id()));
        $this->assertEquals(4, $targetCard->health->total());
    }

    public function testMinionExhaustionAfterSummon()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Attacker is exhausted');

        $game = Factory::createForTest();
        $game->start();

        $card = Sheep::create();
        $targetCard = ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->player1, $card),
            new Block\SummonMinion($game->player2, $targetCard),
        ]);

        $game->action(new AttackCharacter($card->id(), $targetCard->id()));
    }

    public function testMinionExhaustionAfterAttack()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Attacker is exhausted');

        $game = Factory::createForTest();
        $game->start();

        $sheep1 = Sheep::create();
        $sheep2 = Sheep::create();

        $game->resolver->resolveBlock($game, new Block\SummonMinion($game->currentPlayer(), $sheep1));
        $game->resolver->resolveBlock($game, new Block\SummonMinion($game->currentPlayer(), $sheep2));

        $yeti = ChillwindYeti::create();
        $game->resolver->resolveBlock($game, new Block\SummonMinion($game->idlePlayer(), $yeti));

        $game->action(new EndTurn());

        (new AttackCharacter($yeti->id(), $sheep1->id()))->run($game);
        (new AttackCharacter($yeti->id(), $sheep2->id()))->run($game);
    }
}