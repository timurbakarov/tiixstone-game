<?php

use Tiixstone\Card\Alias\ChillwindYeti;
use Tiixstone\Factory;
use Tiixstone\Block\TakeDamage;

class TakeDamageTest extends \PHPUnit\Framework\TestCase
{
    public function testDamageToHero()
    {
        $game = Factory::createForTest(Factory::createCardsArray(30), Factory::createCardsArray(30));
        $game->start();
        $this->assertEquals(30, $game->currentPlayer()->hero->health->total());

        $game->resolver->resolveBlock($game, new TakeDamage($game->currentPlayer()->hero, 10));
        $this->assertEquals(20, $game->currentPlayer()->hero->health->total());
    }

    public function testDamageMinion()
    {
        $game = Factory::createForTest();
        $game->start();

        $minion = ChillwindYeti::create();
        $game->player1->board->append($minion);
        $this->assertEquals(5, $minion->health->total());

        $game->resolver->resolveBlock($game, new TakeDamage($minion, 3));
        $this->assertEquals(2, $minion->health->total());

        $game->resolver->resolveBlock($game, new TakeDamage($minion, 4));
        $this->assertEquals(-2, $minion->health->total());
    }
}