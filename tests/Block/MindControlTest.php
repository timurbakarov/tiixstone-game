<?php

namespace Tiixstone\Tests;

use Tiixstone\Block;
use Tiixstone\Card\Alias\Abomination;
use Tiixstone\Effect\Deathrattle\AbominationDeathrattle;
use Tiixstone\Effect\TauntEffect;
use Tiixstone\Factory;
use Tiixstone\Card\Alias\Sheep;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\ChillwindYeti;

class MindControlTest extends TestCase
{
    public function test_it_removes_from_opponent()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $minion = ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), $minion),
        ]);

        $game->resolver->resolveSequence($game, [
            new Block\MindControl($minion),
        ]);

        $this->assertFalse($game->idlePlayer()->board->has($minion->id()));
        $this->assertTrue($game->currentPlayer()->board->has($minion->id()));

        $this->assertTrue($game->currentPlayer()->id() == $minion->getPlayer()->id());
    }

    public function test_controled_minion_is_exhausted()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $minion = ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), $minion),
        ]);

        $game->resolver->resolveSequence($game, [
            new Block\MindControl($minion),
        ]);

        $this->assertTrue($game->attackTracker->isExhausted($minion));
    }

    public function test_if_board_full_minion_destroyed_on_opponent_side()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $minion = ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), $minion),

            new Block\SummonMinion($game->currentPlayer(), Sheep::create()),
            new Block\SummonMinion($game->currentPlayer(), Sheep::create()),
            new Block\SummonMinion($game->currentPlayer(), Sheep::create()),
            new Block\SummonMinion($game->currentPlayer(), Sheep::create()),
            new Block\SummonMinion($game->currentPlayer(), Sheep::create()),
            new Block\SummonMinion($game->currentPlayer(), Sheep::create()),
            new Block\SummonMinion($game->currentPlayer(), Sheep::create()),
        ]);

        $game->resolver->resolveSequence($game, [
            new Block\MindControl($minion),
        ]);

        $this->assertTrue($game->idlePlayer()->id() == $minion->getPlayer()->id());
        $this->assertFalse($game->idlePlayer()->board->has($minion->id()));
        $this->assertFalse($game->currentPlayer()->board->has($minion->id()));
    }

    public function test_effects_of_controled_minion_doesnt_doubled()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $minion = Abomination::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), $minion),
        ]);

        $game->resolver->resolveSequence($game, [
            new Block\MindControl($minion),
        ]);

        $game->resolver->resolveSequence($game, [
            new Block\MarkDestroyed($minion),
        ]);

        $this->assertEquals(28, $game->currentPlayer()->hero->health->total(), 'Abomination effect triggers');
    }
}