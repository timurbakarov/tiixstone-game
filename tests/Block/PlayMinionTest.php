<?php

use Tiixstone\Card\Alias\ChillwindYeti;
use Tiixstone\Factory;

class PlayMinionTest extends \PHPUnit\Framework\TestCase
{
    public function test()
    {
        $game = Factory::createForTest(Factory::createCardsArray(30), Factory::createCardsArray(30));
        $game->start();

        $yeti = ChillwindYeti::create();
        $game->currentPlayer()->hand->append($yeti);
        $game->currentPlayer()->setAvailableMana(10);

        (new \Tiixstone\Action\PlayCard($yeti->id()))->run($game);

        $this->assertEquals(6, $game->currentPlayer()->availableMana());
    }
}