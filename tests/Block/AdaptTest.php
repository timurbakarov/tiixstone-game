<?php

namespace Tiixstone\Tests;

use Tiixstone\Block;
use Tiixstone\Action\ChooseOne;
use Tiixstone\Effect\ChooseCardEffect;
use Tiixstone\Effect\WindfuryEffect;
use Tiixstone\Factory;
use Tiixstone\Action\EndTurn;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\ViciousFledgling;
use Tiixstone\Action\AttackCharacterRefactored;
use Tiixstone\RNG\NoRNG;

class AdaptTest extends TestCase
{
    public function test_windfury()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();
        $game->setRNG(new NoRNG(1));

        $minion = ViciousFledgling::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $game->action(new EndTurn());
        $game->action(new EndTurn());

        // атакуем
        $game->action(new AttackCharacterRefactored($minion, $game->idlePlayer()->hero));

        // выбираем карту
        $cards = $game->effects->getByEffect(ChooseCardEffect::class)[0]->cards();
        $windfury = $cards[0];

        $game->action(new ChooseOne($windfury));

        $this->assertTrue($game->effects->cardHasEffect($minion, WindfuryEffect::class));

        // атакуем по новой с виндфьюри
        $game->action(new AttackCharacterRefactored($minion, $game->idlePlayer()->hero));

        // выбираем карту
        $cards = $game->effects->getByEffect(ChooseCardEffect::class)[0]->cards();
        $windfury = $cards[0];

        $game->action(new ChooseOne($windfury));
    }
}