<?php

namespace Tiixstone\Tests\Effect;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class LifestealEffectTest extends TestCase
{
    public function test_it_heal_hero()
    {
        $game = Factory::createForTest();
        $game->start();
		
		$minion = Alias\AcolyteOfAgony::create();
		
		$game->resolver->resolveSequence($game, [
			new Block\SummonMinion($game->currentPlayer(), $minion),
		]);
		
		$game->action(new Action\EndTurn);
		$game->action(new Action\EndTurn);
		
		$game->action(new Action\AttackCharacter($minion->id(), $game->idlePlayer()->hero->id()));
		
		$this->assertEquals(18, $game->currentPlayer()->hero->health->total());
    }
}