<?php

namespace Tiixstone\Tests\Effect;

use Tiixstone\Action\UseHeroPowerRefactored;
use Tiixstone\Block;
use Tiixstone\Effect\ElusiveEffect;
use Tiixstone\Factory;
use Tiixstone\Exception;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\PlayCardRefactored;

class ElusiveEffectTest extends TestCase
{
    public function test_cant_be_target_of_spells()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Нельзя использовать заклинание на этого персонажа');

        $game = Factory::createForTest();
        $game->start();

        $wolf = Alias\Wolfrider::create();
        $fireball = Alias\Fireball::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), $wolf),
            new Block\PutCardInHand($game->currentPlayer(), $fireball),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\GiveEffect($wolf, ElusiveEffect::class),
        ]);

        $game->action(new PlayCardRefactored($fireball, null, $wolf));
    }

    public function test_cant_be_target_of_hero_power()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Нельзя использовать cилу героя на этого персонажа');

        $game = Factory::createForTest();
        $game->start();

        $wolf = Alias\Wolfrider::create();
        $fireball = Alias\Fireball::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), $wolf),
            new Block\HeroPower\Replace($game->currentPlayer(), Alias\Fireblast::create()),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\GiveEffect($wolf, ElusiveEffect::class),
        ]);

        $game->action(new UseHeroPowerRefactored($wolf));
    }
}