<?php

namespace Tiixstone\Tests\Effect;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Block\SilenceMinion;
use Tiixstone\Effect\SilenceEffect;
use Tiixstone\Factory;
use Tiixstone\Exception;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\Sheep;
use Tiixstone\Card\Alias\Windfury;
use Tiixstone\Action\EndTurn;
use Tiixstone\Action\PlayCard;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Block\PutCardInHand;
use Tiixstone\Effect\WindfuryEffect;
use Tiixstone\Block\SetManaAvailable;
use Tiixstone\Action\AttackCharacter;

class WindfuryEffectTest extends TestCase
{
    public function test_character_can_attack_twice()
    {
        $game = Factory::createForTest([], [Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create()]);
        $game->start();

        $sheep = Sheep::create();

        $game->resolver->resolveBlock($game, new SummonMinion($game->currentPlayer(), $sheep));
        $game->resolver->resolveBlock($game, new GiveEffect($sheep, WindfuryEffect::class));
        $game->resolver->resolveBlock($game, new SetManaAvailable($game->currentPlayer(), 10));

        $game->action(new EndTurn());
        $game->action(new EndTurn());

        (new AttackCharacter($sheep->id(), $game->idlePlayer()->hero->id()))->run($game);
        (new AttackCharacter($sheep->id(), $game->idlePlayer()->hero->id()))->run($game);

        $this->assertEquals(28, $game->idlePlayer()->hero->health->total());
    }

    public function test_character_can_not_attack_if_was_exhausted()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Attacker is exhausted');

        $game = Factory::createForTest([], []);
        $game->start();

        $sheep = Sheep::create();

        $game->resolver->resolveSequence($game, [
            new SummonMinion($game->currentPlayer(), $sheep),
            new GiveEffect($sheep, WindfuryEffect::class),
        ]);

        (new AttackCharacter($sheep->id(), $game->idlePlayer()->hero->id()))->run($game);
    }

    public function test_character_can_not_attack_more_than_twice()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Attacker is exhausted');

        $game = Factory::createForTest([], [Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create()]);
        $game->start();

        $sheep = Sheep::create();

        $game->resolver->resolveBlock($game, new SummonMinion($game->currentPlayer(), $sheep));
        $game->resolver->resolveBlock($game, new GiveEffect($sheep, WindfuryEffect::class));
        $game->resolver->resolveBlock($game, new SetManaAvailable($game->currentPlayer(), 10));

        $game->action(new EndTurn());
        $game->action(new EndTurn());

        (new AttackCharacter($sheep->id(), $game->idlePlayer()->hero->id()))->run($game);
        (new AttackCharacter($sheep->id(), $game->idlePlayer()->hero->id()))->run($game);
        (new AttackCharacter($sheep->id(), $game->idlePlayer()->hero->id()))->run($game);
    }

    public function test_character_can_attack_then_gain_windfury_and_can_attack_once_more()
    {
        $game = Factory::createForTest([], [Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create()]);
        $game->start();

        $sheep = Sheep::create();

        $game->resolver->resolveBlock($game, new SummonMinion($game->currentPlayer(), $sheep));
        $game->resolver->resolveBlock($game, new SetManaAvailable($game->currentPlayer(), 10));

        $game->action(new EndTurn());
        $game->action(new EndTurn());

        (new AttackCharacter($sheep->id(), $game->idlePlayer()->hero->id()))->run($game);

        $game->resolver->resolveSequence($game, [
            new GiveEffect($sheep, WindfuryEffect::class),
        ]);

        (new AttackCharacter($sheep->id(), $game->idlePlayer()->hero->id()))->run($game);

        $this->assertEquals(28, $game->idlePlayer()->hero->health->total());
    }

    public function test_character_can_not_attack_twice_if_effect_removed()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Attacker is exhausted');

        $game = Factory::createForTest([], [Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create()]);
        $game->start();

        $sheep = Sheep::create();

        $game->resolver->resolveBlock($game, new SummonMinion($game->currentPlayer(), $sheep));
        $game->resolver->resolveBlock($game, new GiveEffect($sheep, WindfuryEffect::class));
        $game->resolver->resolveBlock($game, new SetManaAvailable($game->currentPlayer(), 10));

        $game->action(new EndTurn());
        $game->action(new EndTurn());

        (new AttackCharacter($sheep->id(), $game->idlePlayer()->hero->id()))->run($game);

        $game->resolver->resolveSequence($game, [new GiveEffect($sheep, SilenceEffect::class)]);

        (new AttackCharacter($sheep->id(), $game->idlePlayer()->hero->id()))->run($game);
    }
}