<?php

namespace Tiixstone\Tests\Effect;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Exception;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\ImmuneEffect;
use Tiixstone\Effect\StealthEffect;

class TauntEffectTest extends TestCase
{
    public function test_character_can_not_attack_not_taunt()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('You should attack taunt');

        $game = Factory::createForTest();
        $game->start();

        $sheep = Alias\Sheep::create();
        $taunt = Alias\FrostwolfGrunt::create();
        $wolf = Alias\Wolfrider::create();

        // добавляем сопернику двух существ, таунта и не таунта
        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), $sheep),
            new Block\SummonMinion($game->idlePlayer(), $taunt),

            // добавляем активному игроку существо с рывком
            new Block\SummonMinion($game->currentPlayer(), $wolf),
        ]);

        $game->action(new Action\AttackCharacterRefactored($wolf, $sheep));
    }

    public function test_character_can_attack_taunt()
    {
        $game = Factory::createForTest();
        $game->start();

        $sheep = Alias\Sheep::create();
        $taunt = Alias\FrostwolfGrunt::create();
        $wolf = Alias\Wolfrider::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), $sheep),
            new Block\SummonMinion($game->idlePlayer(), $taunt),
            new Block\SummonMinion($game->currentPlayer(), $wolf),
        ]);

        $game->action(new Action\AttackCharacterRefactored($wolf, $taunt));

        $this->assertFalse($game->idlePlayer()->board->has($taunt->id()));
    }

    public function test_taunt_not_count_if_minion_with_stealth()
    {
        $game = Factory::createForTest();
        $game->start();

        $sheep = Alias\Sheep::create();
        $taunt = Alias\FrostwolfGrunt::create();
        $wolf = Alias\Wolfrider::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), $sheep),
            new Block\SummonMinion($game->idlePlayer(), $taunt),
            new Block\SummonMinion($game->currentPlayer(), $wolf),
            new Block\GiveEffect($taunt, StealthEffect::class),
        ]);

        $game->action(new Action\AttackCharacterRefactored($wolf, $sheep));

        $this->assertFalse($game->idlePlayer()->board->has($sheep->id()));
    }

    public function test_taunt_not_count_if_minion_with_immune()
    {
        $game = Factory::createForTest();
        $game->start();

        $sheep = Alias\Sheep::create();
        $taunt = Alias\FrostwolfGrunt::create();
        $wolf = Alias\Wolfrider::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), $sheep),
            new Block\SummonMinion($game->idlePlayer(), $taunt),
            new Block\SummonMinion($game->currentPlayer(), $wolf),
            new Block\GiveEffect($taunt, ImmuneEffect::class),
        ]);

        $game->action(new Action\AttackCharacterRefactored($wolf, $sheep));

        $this->assertFalse($game->idlePlayer()->board->has($sheep->id()));
    }
}