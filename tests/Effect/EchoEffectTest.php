<?php

namespace Tiixstone\Tests\Effect;

use Tiixstone\Action;
use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Effect\EchoEffect;
use PHPUnit\Framework\TestCase;

class EchoEffectTest extends TestCase
{
    public function test_it_copies_card_in_hand_when_played_and_remove_when_turn_ends()
    {
        $game = Factory::createForTest();
        $game->start();
		
		$echoMinion = Alias\GhostLightAngler::create();
		
		$game->resolver->resolveSequence($game, [
			new Block\PutCardInHand($game->currentPlayer(), $echoMinion),
			new Block\SetManaAvailable($game->currentPlayer(), 10),
			new Block\GiveEffect($echoMinion, EchoEffect::class),
		]);
		
		$game->action(new Action\PlayCard($echoMinion->id()));
		$this->assertInstanceof(Alias\GhostLightAngler::className(), $game->currentPlayer()->hand->first());
		
		$game->action(new Action\PlayCard($game->currentPlayer()->hand->first()->id()));
		$this->assertInstanceof(Alias\GhostLightAngler::className(), $game->currentPlayer()->hand->first());
		
		$game->action(new Action\EndTurn);
		$this->assertEquals(0, $game->idlePlayer()->hand->count());
    }
}