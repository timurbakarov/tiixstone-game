<?php

namespace Tiixstone\Tests\Effect;

use Tiixstone\Battlecryable;
use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Action;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Attackable;
use Tiixstone\Card\Character;
use Tiixstone\Card\Minion;
use Tiixstone\Effect\ComboEffect;
use Tiixstone\Factory;
use Tiixstone\Game;

class ComboEffectTest extends TestCase
{
    public function test_it_doesnt_use_combo_if_no_card_plays_this_turn()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = new TestSpell();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
        ]);

        $game->action(new Action\PlayCardRefactored($spell));

        $this->assertEquals(20, $game->currentPlayer()->hero->health->total());
    }

    public function test_it_use_combo_if_for_spell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell1 = new TestSpell();
        $spell2 = new TestSpell();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell1),
            new Block\PutCardInHand($game->currentPlayer(), $spell2),
        ]);

        $game->action(new Action\PlayCardRefactored($spell1));
        $game->action(new Action\PlayCardRefactored($spell2));

        $this->assertEquals(19, $game->currentPlayer()->hero->health->total());
    }

    public function test_it_use_combo_if_for_minion()
    {
        $game = Factory::createForTest();
        $game->start();

        $minion1 = new TestMinion();
        $minion2 = new TestMinion();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion1),
            new Block\PutCardInHand($game->currentPlayer(), $minion2),
        ]);

        $game->action(new Action\PlayCardRefactored($minion1));
        $game->action(new Action\PlayCardRefactored($minion2));

        $this->assertEquals(19, $game->currentPlayer()->hero->health->total());
    }
}

class TestSpell extends Card\Spell
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return '';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|NULL $target
     * @return array
     */
    public function cast(Game $game, Character $target = NULL): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function handEffects()
    {
        return [TestComboEffect::class];
    }
}

class TestMinion extends Card\Minion implements Battlecryable
{

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }

    /**
     * @return string
     */
    public function globalId(): string
    {
        return '';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        return [];
    }

    /**
     * @return array
     */
    public function handEffects()
    {
        return [TestComboEffect::class];
    }
}

class TestComboEffect extends ComboEffect
{
    /**
     * @return array
     */
    public function combo(Game $game, Card $owner, Card\Character $target = null): Block
    {
        return new Block\TakeDamage($this->owner->getPlayer()->hero, 1, $this->owner());
    }
}