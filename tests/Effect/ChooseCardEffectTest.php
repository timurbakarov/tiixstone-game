<?php

namespace Tiixstone\Tests\Effect;

use Tiixstone\Card\Alias\GadgetzanAuctioneer;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\ChooseOne;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Effect\TauntEffect;
use Tiixstone\Block\PutCardInHand;
use Tiixstone\Block\SetManaAvailable;
use Tiixstone\Card\Alias\MarkOfNature;
use Tiixstone\Effect\ChooseCardEffect;
use Tiixstone\Card\Alias\ChillwindYeti;
use Tiixstone\Action\PlayCardRefactored;

class ChooseCardEffectTest extends TestCase
{
    public function test_take_first_card()
    {
        $game = Factory::createForTestWithDecks();

        $game->start();

        $markOfNature = MarkOfNature::create();
        $minion = ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new SummonMinion($game->player1, $minion),
            new PutCardInHand($game->player1, $markOfNature),
            new SetManaAvailable($game->player1, 10),
        ]);

        $game->action(new PlayCardRefactored($markOfNature, null, $minion));

        /** @var ChooseCardEffect $chooseCardEffect */
        $chooseCardEffect = $game->effects->getByEffect(ChooseCardEffect::class)[0];

        $cards = $chooseCardEffect->cards();

        $game->action(new ChooseOne($cards[0]));

        $this->assertEquals(8, $minion->attack->total());
    }

    public function test_take_second_card()
    {
        $game = Factory::createForTestWithDecks();

        $game->start();

        $markOfNature = MarkOfNature::create();
        $minion = ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new SummonMinion($game->player1, $minion),
            new PutCardInHand($game->player1, $markOfNature),
            new SetManaAvailable($game->player1, 10),
        ]);

        $game->action(new PlayCardRefactored($markOfNature, null, $minion));

        /** @var ChooseCardEffect $chooseCardEffect */
        $chooseCardEffect = $game->effects->getByEffect(ChooseCardEffect::class)[0];

        $cards = $chooseCardEffect->cards();

        $game->action(new ChooseOne($cards[1]));

        $this->assertEquals(9, $minion->health->total());
        $this->assertTrue($game->effects->cardHasEffect($minion, TauntEffect::class));
    }

    public function test_it_stops_normal_flow()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $markOfNature = MarkOfNature::create();
        $minion = ChillwindYeti::create();
        $auctioneer = GadgetzanAuctioneer::create();

        $game->resolver->resolveSequence($game, [
            new SummonMinion($game->player1, $minion),
            new SummonMinion($game->player1, $auctioneer),
            new PutCardInHand($game->player1, $markOfNature),
            new SetManaAvailable($game->player1, 10),
        ]);

        $game->action(new PlayCardRefactored($markOfNature, null, $minion));

        $this->assertEquals(5, $game->player1->hand->count());

        /** @var ChooseCardEffect $chooseCardEffect */
        $chooseCardEffect = $game->effects->getByEffect(ChooseCardEffect::class)[0];

        $cards = $chooseCardEffect->cards();

        $game->action(new ChooseOne($cards[1]));

        $this->assertEquals(5, $game->player1->hand->count());
    }
}