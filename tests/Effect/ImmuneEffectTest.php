<?php

namespace Tiixstone\Tests\Effect;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Effect;
use Tiixstone\Factory;
use Tiixstone\Exception;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class ImmuneEffectTest extends TestCase
{
    public function test_cant_be_target_of_enemy_spells()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Нельзя использовать заклинание на персонажа c неуязвимостью');

        $game = Factory::createForTest();
        $game->start();

        $wolf = Alias\Wolfrider::create();
        $fireball = Alias\Fireball::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), $wolf),
            new Block\PutCardInHand($game->currentPlayer(), $fireball),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\GiveEffect($wolf, Effect\ImmuneEffect::class),
        ]);

        $game->action(new Action\PlayCardRefactored($fireball, null, $wolf));
    }

    public function test_can_be_target_of_friendly_spells()
    {
        $game = Factory::createForTest();
        $game->start();

        $wolf = Alias\Wolfrider::create();
        $fireball = Alias\Fireball::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $wolf),
            new Block\PutCardInHand($game->currentPlayer(), $fireball),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\GiveEffect($wolf, Effect\ImmuneEffect::class),
        ]);

        $game->action(new Action\PlayCardRefactored($fireball, null, $wolf));

        $this->assertTrue($game->currentPlayer()->board->has($wolf->id()));
    }

    public function test_cant_be_target_of_enemy_hero_power()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Нельзя использовать cилу героя на персонажа с неуязвимостью');

        $game = Factory::createForTest();
        $game->start();

        $wolf = Alias\Wolfrider::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), $wolf),
            new Block\HeroPower\Replace($game->currentPlayer(), Alias\Fireblast::create()),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\GiveEffect($wolf, Effect\ImmuneEffect::class),
        ]);

        $game->action(new Action\UseHeroPowerRefactored($wolf));
    }

    public function test_can_be_target_of_friendly_hero_power()
    {
        $game = Factory::createForTest();
        $game->start();

        $wolf = Alias\Wolfrider::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $wolf),
            new Block\HeroPower\Replace($game->currentPlayer(), Alias\Fireblast::create()),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\GiveEffect($wolf, Effect\ImmuneEffect::class),
        ]);

        $game->action(new Action\UseHeroPowerRefactored($wolf));

        $this->assertTrue($game->currentPlayer()->board->has($wolf->id()));
    }

    public function test_cant_be_attacked()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Нельзя атаковать персонажа с неуязвимостью');

        $game = Factory::createForTest();
        $game->start();

        $wolf = Alias\Wolfrider::create();
        $ourWolf = Alias\Wolfrider::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), $wolf),
            new Block\SummonMinion($game->currentPlayer(), $ourWolf),
            new Block\GiveEffect($wolf, Effect\ImmuneEffect::class),
        ]);

        $game->action(new Action\AttackCharacterRefactored($ourWolf, $wolf));
    }
}