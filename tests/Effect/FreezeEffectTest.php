<?php

namespace Tiixstone\Tests\Effect;

use PHPUnit\Framework\TestCase;
use Tiixstone\Block\GiveEffect;
use Tiixstone\Card\Alias\Wolfrider;
use Tiixstone\Action\EndTurn;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Effect\FreezeEffect;
use Tiixstone\Exception;
use Tiixstone\Factory;
use Tiixstone\Action\AttackCharacterRefactored;

class FreezeEffectTest extends TestCase
{
    public function test_char_can_not_attack_if_frozen()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Frozen character can not attack');

        $game = Factory::createForTest();
        $game->start();

        $wolfrider = Wolfrider::create();

        $game->resolver->resolveSequence($game, [
            new SummonMinion($game->currentPlayer(), $wolfrider),
            new GiveEffect($wolfrider, FreezeEffect::class),
        ]);

        $game->action(new AttackCharacterRefactored($wolfrider, $game->idlePlayer()->hero));
    }

    public function test_if_freeze_char_to_next_turn_if_player_freeze_enemy_at_his_turn()
    {
        $game = Factory::createForTest();
        $game->start();

        $wolfrider = Wolfrider::create();

        $game->resolver->resolveBlock($game, new SummonMinion($game->idlePlayer(), $wolfrider));
        $game->effects->add($game, $wolfrider, FreezeEffect::class);

        $this->assertTrue($game->effects->cardHasEffect($wolfrider, FreezeEffect::class));

        $game->action(new EndTurn());
        $this->assertTrue($game->effects->cardHasEffect($wolfrider, FreezeEffect::class));

        $game->action(new EndTurn());
        $this->assertFalse($game->effects->cardHasEffect($wolfrider, FreezeEffect::class));
    }
}