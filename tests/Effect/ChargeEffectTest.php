<?php

namespace Tiixstone\Tests\Effect;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Exception;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Card\Alias\Wolfrider;

class ChargeEffectTest extends TestCase
{
    public function test_get_charge_when_played()
    {
        $game = Factory::createForTest();
        $game->start();

        $wolfrider = Wolfrider::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $wolfrider),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($wolfrider));
        $game->action(new Action\AttackCharacterRefactored($wolfrider, $game->idlePlayer()->hero));

        $this->assertEquals(17, $game->idlePlayer()->hero->health->total());
    }

    public function testGetChargeWhenSummoned()
    {
        $game = Factory::createForTest();
        $game->start();

        $wolf = Wolfrider::create();

        $game->resolver->resolveSequence($game, [
            new SummonMinion($game->currentPlayer(), $wolf)
        ]);

        $game->action(new Action\AttackCharacterRefactored($wolf, $game->idlePlayer()->hero));
        $this->assertEquals(17, $game->idlePlayer()->hero->health->total());
    }

    public function test_charge_can_not_attack_twice()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Attacker is exhausted');

        $game = Factory::createForTest();
        $game->start();

        $wolf = Wolfrider::create();

        $game->resolver->resolveSequence($game, [
            new SummonMinion($game->currentPlayer(), $wolf)
        ]);

        $game->action(new Action\AttackCharacterRefactored($wolf, $game->idlePlayer()->hero));
        $game->action(new Action\AttackCharacterRefactored($wolf, $game->idlePlayer()->hero));
    }
}