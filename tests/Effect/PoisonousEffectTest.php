<?php

namespace Tiixstone\Tests\Effect;

use PHPUnit\Framework\TestCase;
use Tiixstone\Action\AttackCharacter;
use Tiixstone\Card\Alias\ChillwindYeti;
use Tiixstone\Action;
use Tiixstone\Block;
use Tiixstone\Exception;
use Tiixstone\Factory;
use Tiixstone\Effect\PoisonousEffect;

class PoisonousEffectTest extends TestCase
{
    public function test_it_kills_minion_on_damage()
    {
        $game = Factory::createForTest();
        $game->start();
		
		$minion = ChillwindYeti::create();
		$poisonousMinion = ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $poisonousMinion),
			new Block\SummonMinion($game->idlePlayer(), $minion),
			new Block\GiveEffect($poisonousMinion, PoisonousEffect::class),
        ]);
		
		$game->action(new Action\EndTurn());
		$game->action(new Action\EndTurn());
		
		$game->action(new AttackCharacter($poisonousMinion->id(), $minion->id()));
		
		$this->assertFalse($game->idlePlayer()->board->has($minion->id()));
    }
	
	public function test_it_doesnt_kill_hero_on_damage()
    {
        $game = Factory::createForTest();
        $game->start();
		
		$minion = ChillwindYeti::create();
		$poisonousMinion = ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $poisonousMinion),
			new Block\GiveEffect($poisonousMinion, PoisonousEffect::class),
        ]);
		
		$game->action(new Action\EndTurn());
		$game->action(new Action\EndTurn());
		
		$game->action(new AttackCharacter($poisonousMinion->id(), $game->idlePlayer()->hero->id()));
		
		$this->assertTrue($game->idlePlayer()->hero->health->isAlive());
    }
}