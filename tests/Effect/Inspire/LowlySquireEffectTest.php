<?php

namespace Tiixstone\Tests\Effect;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class LowlySquireEffectTest extends TestCase
{
    public function test_get_charge_when_played()
    {
        $game = Factory::createForTest();
        $game->start();

        $lowlySquire = Alias\LowlySquire::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $lowlySquire),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\UseHeroPowerRefactored($game->idlePlayer()->hero));

        $this->assertEquals(2, $lowlySquire->attack->total());
    }
}