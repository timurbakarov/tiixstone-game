<?php

namespace Tiixstone\Tests\Effect;

use Tiixstone\Action\AttackCharacterRefactored;
use Tiixstone\Block;
use Tiixstone\Effect\StealthEffect;
use Tiixstone\Factory;
use Tiixstone\Exception;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\ElusiveEffect;
use Tiixstone\Action\PlayCardRefactored;
use Tiixstone\Action\UseHeroPowerRefactored;

class StealthEffectTest extends TestCase
{
    public function test_cant_be_target_of_enemy_spells()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Нельзя использовать заклинание на персонажа c маскировкой');

        $game = Factory::createForTest();
        $game->start();

        $wolf = Alias\Wolfrider::create();
        $fireball = Alias\Fireball::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), $wolf),
            new Block\PutCardInHand($game->currentPlayer(), $fireball),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\GiveEffect($wolf, StealthEffect::class),
        ]);

        $game->action(new PlayCardRefactored($fireball, null, $wolf));
    }

    public function test_can_be_target_of_friendly_spells()
    {
        $game = Factory::createForTest();
        $game->start();

        $wolf = Alias\Wolfrider::create();
        $fireball = Alias\Fireball::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $wolf),
            new Block\PutCardInHand($game->currentPlayer(), $fireball),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\GiveEffect($wolf, StealthEffect::class),
        ]);

        $game->action(new PlayCardRefactored($fireball, null, $wolf));

        $this->assertFalse($game->currentPlayer()->board->has($wolf->id()));
    }

    public function test_cant_be_target_of_enemy_hero_power()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Нельзя использовать cилу героя на персонажа с маскировкой');

        $game = Factory::createForTest();
        $game->start();

        $wolf = Alias\Wolfrider::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), $wolf),
            new Block\HeroPower\Replace($game->currentPlayer(), Alias\Fireblast::create()),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\GiveEffect($wolf, StealthEffect::class),
        ]);

        $game->action(new UseHeroPowerRefactored($wolf));
    }

    public function test_can_be_target_of_friendly_hero_power()
    {
        $game = Factory::createForTest();
        $game->start();

        $wolf = Alias\Wolfrider::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $wolf),
            new Block\HeroPower\Replace($game->currentPlayer(), Alias\Fireblast::create()),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\GiveEffect($wolf, StealthEffect::class),
        ]);

        $game->action(new UseHeroPowerRefactored($wolf));

        $this->assertFalse($game->currentPlayer()->board->has($wolf->id()));
    }

    public function test_cant_be_attacked()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Нельзя атаковать персонажа с маскировкой');

        $game = Factory::createForTest();
        $game->start();

        $wolf = Alias\Wolfrider::create();
        $ourWolf = Alias\Wolfrider::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), $wolf),
            new Block\SummonMinion($game->currentPlayer(), $ourWolf),
            new Block\GiveEffect($wolf, StealthEffect::class),
        ]);

        $game->action(new AttackCharacterRefactored($ourWolf, $wolf));
    }

    public function test_stealth_removed_after_attack()
    {
        $game = Factory::createForTest();
        $game->start();

        $wolf = Alias\Wolfrider::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $wolf),
            new Block\GiveEffect($wolf, StealthEffect::class),
        ]);

        $game->action(new AttackCharacterRefactored($wolf, $game->idlePlayer()->hero));

        $this->assertFalse($game->effects->cardHasEffect($wolf, StealthEffect::class));
    }
}