<?php

namespace Tiixstone\Tests\Effect;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Exception;
use Tiixstone\Card\Minion;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\RushEffect;

class RushEffectTest extends TestCase
{
    public function test_minion_can_not_attack_hero()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Target should be a minion');

        $game = Factory::createForTest();
        $game->start();

        $minion = $this->cardWithRushEffect();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($minion));

        $game->resolver->resolveSequence($game, [new Block\GiveEffect($minion, RushEffect::class)]);

        $game->action(new Action\AttackCharacterRefactored($minion, $game->idlePlayer()->hero));
    }

    private function cardWithRushEffect()
    {
        return new class extends Minion {

            /**
             * @return int
             */
            public function defaultAttackRate(): int
            {
                return 1;
            }

            public function globalId(): string
            {
                return '13';
            }

            /**
             * @return int
             */
            public function defaultCost(): int
            {
                return 0;
            }

            /**
             * @return int
             */
            public function defaultMaximumHealth(): int
            {
                return 1;
            }

            /**
             * @return array
             */
            public function boardEffects()
            {
                return [RushEffect::class];
            }
        };
    }
}