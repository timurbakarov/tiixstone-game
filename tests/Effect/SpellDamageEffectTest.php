<?php

namespace Tiixstone\Tests\Effect;

use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;

class SpellDamageEffectTest extends TestCase
{
    public function test_get_charge_when_played()
    {
        $game = Factory::createForTest();
        $game->start();

        $game->resolver->resolveSequence($game, [
            new Block\GiveEffect($game->currentPlayer()->hero, new Effect\SpellDamageEffect(1)),
        ]);

        $this->assertEquals(1, $game->withSpellDamage(0));
    }
}