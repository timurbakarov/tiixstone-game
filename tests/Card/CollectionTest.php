<?php

namespace Tiixstone\Tests;

use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\Sheep;
use Tiixstone\Card\Collection;

class CollectionTest extends TestCase
{
    public function testCollectionCreate()
    {
        $card1 = Sheep::create();
        $card2 = Sheep::create();
        $card3 = Sheep::create();
        $board = new TestCollection([$card1, $card2, $card3]);
        $this->assertEquals(3, $board->count());

        $board = new TestCollection();
        $board->append($card1);
        $board->append($card2);
        $board->append($card3);
        $this->assertEquals(3, $board->count());

        $board = new TestCollection();
        $board->appendMany([$card1, $card2, $card3]);
        $this->assertEquals(3, $board->count());

        // --------------------------------------------

        $card1 = Sheep::create();
        $card2 = Sheep::create();
        $card3 = Sheep::create();

        $board = new TestCollection([$card1, $card2, $card3]);
        $this->assertEquals(3, $board->count());
        
        $newCard = Sheep::create();
        $board->addBefore($newCard, $card1->id());
        $this->assertEquals(4, $board->count());
        $this->assertEquals($newCard->id(), $board->first()->id());

        $newCard2 = Sheep::create();

        $board->addBefore($newCard2, $card3->id());

        $this->assertEquals(5, $board->count());
        $this->assertNotEquals($newCard2->id(), $board->first()->id());
        $this->assertEquals($newCard2->id(), $board->getByPosition(4)->id());

        $first = $board->shift();
        $this->assertEquals(4, $board->count());
        $this->assertEquals($first->id(), $newCard->id());

        $board->remove($card2->id());
        $this->assertEquals(3, $board->count());
        $this->assertEquals($card1->id(), $board->getByPosition(1)->id());
        $this->assertEquals($newCard2->id(), $board->getByPosition(2)->id());
        $this->assertEquals($card3->id(), $board->getByPosition(3)->id());

        $this->assertEquals($card1->id(), $board->prev($newCard2->id())->id());
        $this->assertEquals($card3->id(), $board->next($newCard2->id())->id());

        $this->assertEquals($newCard2->id(), $board->next($card1->id())->id());
        $this->assertEquals($newCard2->id(), $board->prev($card3->id())->id());
    }
}

class TestCollection extends Collection
{

}