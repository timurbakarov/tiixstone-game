<?php

namespace Tiixstone\Tests\Cards\Item;

use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\SkullOfTheManari;
use Tiixstone\Card\Alias\Voidlord;
use Tiixstone\Action\EndTurn;
use Tiixstone\Action\PlayCard;
use Tiixstone\Block\PutCardInHand;
use Tiixstone\Block\SetManaAvailable;
use Tiixstone\Factory;

class SkullOfTheManaryTest extends TestCase
{
    public function test_it_summon_minion_from_hand()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $skull = SkullOfTheManari::create();
        $demon = Voidlord::create();

        $game->resolver->resolveSequence($game, [
            new PutCardInHand($game->currentPlayer(), $skull),
            new PutCardInHand($game->currentPlayer(), $demon),
            new SetManaAvailable($game->currentPlayer(), 10),
        ]);

        (new PlayCard($skull->id()))->run($game);

        $game->action(new EndTurn());
        $game->action(new EndTurn());

        $this->assertTrue($game->currentPlayer()->board->has($demon->id()));
        $this->assertFalse($game->currentPlayer()->hand->has($demon->id()));
    }
}