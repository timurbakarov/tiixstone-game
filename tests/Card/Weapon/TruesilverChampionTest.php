<?php

namespace Tiixstone\Tests\Cards\Item;

use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias;
use Tiixstone\Factory;
use Tiixstone\Card;
use Tiixstone\Action;
use Tiixstone\Block;

class TruesilverChampionTest extends TestCase
{
    public function test_it_heals_hero()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();
		
		$truesilverChampion = Alias\TruesilverChampion::create();
		
		$game->resolver->resolveSequence($game, [
			new Block\PutCardInHand($game->currentPlayer(), $truesilverChampion),
			new Block\SetManaAvailable($game->currentPlayer(), 10),
		]);
		
		$game->currentPlayer()->hero->health->damage(20);
		
		$game->action(new Action\PlayCard($truesilverChampion->id()));
		$game->action(new Action\AttackCharacter($game->currentPlayer()->hero->id(), $game->idlePlayer()->hero->id()));
		
		$this->assertEquals(12, $game->currentPlayer()->hero->health->total());
    }
}