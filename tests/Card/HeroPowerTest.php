<?php

namespace Tiixstone\Tests\Card\HeroPower;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Effect;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Card\Minion;
use PHPUnit\Framework\TestCase;

class HeroPowerTest extends TestCase
{
    public function test_armor_up()
    {
        $game = Factory::createForTest();
        $game->start();

		$armorUp = Alias\ArmorUp::create();

		$game->resolver->resolveSequence($game, [
			new Block\HeroPower\Replace($game->currentPlayer(), $armorUp),
			new Block\SetManaAvailable($game->currentPlayer(), 10),
		]);

		$game->action(new Action\UseHeroPower());

		$this->assertEquals(2, $game->currentPlayer()->hero->armor->get());
    }

	public function test_dagger_mastery()
    {
        $game = Factory::createForTest();
        $game->start();

		$power = Alias\DaggerMastery::create();

		$game->resolver->resolveSequence($game, [
			new Block\HeroPower\Replace($game->currentPlayer(), $power),
			new Block\SetManaAvailable($game->currentPlayer(), 10),
		]);

		$game->action(new Action\UseHeroPower());

		$this->assertInstanceof(Alias\WickedKnife::className(), $game->currentPlayer()->hero->weapon);
    }

	public function test_fireblast()
    {
        $game = Factory::createForTest();
        $game->start();

		$power = Alias\Fireblast::create();

		$game->resolver->resolveSequence($game, [
			new Block\HeroPower\Replace($game->currentPlayer(), $power),
			new Block\SetManaAvailable($game->currentPlayer(), 10),
		]);

		$game->action(new Action\UseHeroPower($game->idlePlayer()->hero->id()));

		$this->assertEquals(19, $game->idlePlayer()->hero->health->total());
    }

    public function test_syphon_life()
    {
        $game = Factory::createForTest();
        $game->start();

        $power = Alias\SiphonLife::create();

        $game->resolver->resolveSequence($game, [
            new Block\HeroPower\Replace($game->currentPlayer(), $power),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\UseHeroPower($game->idlePlayer()->hero->id()));

        $this->assertEquals(17, $game->idlePlayer()->hero->health->total());
        $this->assertEquals(23, $game->currentPlayer()->hero->health->total());
    }

	public function test_lesser_heal()
    {
        $game = Factory::createForTest();
        $game->start();

		$power = Alias\LesserHeal::create();

		$game->resolver->resolveSequence($game, [
			new Block\HeroPower\Replace($game->currentPlayer(), $power),
			new Block\SetManaAvailable($game->currentPlayer(), 10),
		]);

		$game->action(new Action\UseHeroPower($game->idlePlayer()->hero->id()));

		$this->assertEquals(22, $game->idlePlayer()->hero->health->total());
    }

	public function test_life_tap()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

		$power = Alias\LifeTap::create();

		$game->resolver->resolveSequence($game, [
			new Block\HeroPower\Replace($game->currentPlayer(), $power),
			new Block\SetManaAvailable($game->currentPlayer(), 10),
		]);

		$game->action(new Action\UseHeroPower());

		$this->assertEquals(28, $game->currentPlayer()->hero->health->total());
		$this->assertEquals(5, $game->currentPlayer()->hand->count());
    }

	public function test_reinforce()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

		$power = Alias\Reinforce::create();

		$game->resolver->resolveSequence($game, [
			new Block\HeroPower\Replace($game->currentPlayer(), $power),
			new Block\SetManaAvailable($game->currentPlayer(), 10),
		]);

		$game->action(new Action\UseHeroPower());

		$this->assertInstanceOf(Alias\SilverHandRecruit::className(), $game->currentPlayer()->board->first());
    }

	public function test_shapeshift()
    {
        $game = Factory::createForTest();
        $game->start();

		$armorUp = Alias\Shapeshift::create();

		$game->resolver->resolveSequence($game, [
			new Block\HeroPower\Replace($game->currentPlayer(), $armorUp),
			new Block\SetManaAvailable($game->currentPlayer(), 10),
		]);

		$game->action(new Action\UseHeroPower());

		$this->assertEquals(1, $game->currentPlayer()->hero->armor->get());
		$this->assertEquals(1, $game->currentPlayer()->hero->attack->total());
    }

	public function test_steady_shot()
    {
        $game = Factory::createForTest();
        $game->start();

		$power = Alias\SteadyShot::create();

		$game->resolver->resolveSequence($game, [
			new Block\HeroPower\Replace($game->currentPlayer(), $power),
			new Block\SetManaAvailable($game->currentPlayer(), 10),
		]);

		$game->action(new Action\UseHeroPower());

		$this->assertEquals(18, $game->idlePlayer()->hero->health->total());
    }

	public function test_totemic_call()
	{
		$game = Factory::createForTestWithDecks();
        $game->start();

		$power = Alias\TotemicCall::create();

		$game->resolver->resolveSequence($game, [
			new Block\HeroPower\Replace($game->currentPlayer(), $power),
			new Block\SetManaAvailable($game->currentPlayer(), 10),
		]);

		$game->action(new Action\UseHeroPower());

		$this->assertEquals(1, $game->currentPlayer()->board->count());
	}

    public function test_totemic_slam()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $power = Alias\TotemicSlam::create();

        $silence = Alias\Silence::create();

        $game->resolver->resolveSequence($game, [
            new Block\HeroPower\Replace($game->currentPlayer(), $power),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\PutCardInHand($game->currentPlayer(), $silence),
        ]);

        $game->action(new Action\UseHeroPower());

        $cards = $game->effects->getByEffect(Effect\ChooseCardEffect::class)[0]->cards();

        $game->action(new Action\ChooseOne($cards[0]));

        $game->action(new Action\PlayCardRefactored($silence, null, $cards[0]));

        $this->assertTrue($game->effects->cardHasEffect($cards[0], Effect\SilenceEffect::class));
        $this->assertEquals(1, $game->currentPlayer()->board->count());
    }

    public function test_inferno()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $power = Alias\INFERNO::create();

        $game->resolver->resolveSequence($game, [
            new Block\HeroPower\Replace($game->currentPlayer(), $power),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\UseHeroPower());

        $this->assertInstanceOf(Alias\Infernal::className(), $game->currentPlayer()->board->first());
    }
}