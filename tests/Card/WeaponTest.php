<?php

namespace Tiixstone\Tests;

use Tiixstone\Action\PlayCardRefactored;
use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Weapon;
use Tiixstone\Card\Alias;
use Tiixstone\Action\EndTurn;
use Tiixstone\Action\PlayCard;
use PHPUnit\Framework\TestCase;
use Tiixstone\Block\PutCardInHand;
use Tiixstone\Block\SetManaAvailable;
use Tiixstone\Action\AttackCharacter;

class WeaponTest extends TestCase
{
	/**
     * @dataProvider provider
     */
    public function test_weapons(Weapon $weapon, array $effects = [])
    {
        $game = Factory::createForTest();
        $game->start();

		$game->resolver->resolveSequence($game, [
			new Block\Weapon\EquipWeapon($game->currentPlayer(), $weapon),
		]);

		$this->assertInstanceof(get_class($weapon), $game->currentPlayer()->hero->weapon);

		if($effects) {
			foreach($effects as $effect) {
				$this->assertTrue($game->effects->cardHasEffect($weapon, $effect), 'Has no effect of '. $effect);
			}
		}
    }

	public function provider()
	{
		return [
			'ArcaniteReaper' => [Alias\ArcaniteReaper::create()],
			'LightsJustice' => [Alias\LightsJustice::create()],
			'WickedKnife' => [Alias\WickedKnife::create()],
			'FieryWarAxe' => [Alias\FieryWarAxe::create()],
			'AssassinsBlade' => [Alias\AssassinsBlade::create()],
			'BloodFury' => [Alias\BloodFury::create()],
			'EaglehornBow' => [Alias\EaglehornBow::create()],
			'BattleAxe' => [Alias\BattleAxe::create()],
			'Ashbringer' => [Alias\Ashbringer::create()],
		];
	}

    public function test_equip()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $axe = Alias\FieryWarAxe::create();

        $game->resolver->resolveSequence($game, [
            new SetManaAvailable($game->currentPlayer(), 10),
            new PutCardInHand($game->currentPlayer(), $axe),
        ]);

        $game->action(new PlayCardRefactored($axe));

        $this->assertInstanceOf(Alias\FieryWarAxe::className(), $game->currentPlayer()->hero->weapon);
        $this->assertEquals(3, $game->currentPlayer()->hero->attack->total());
    }

    public function test_durability_lose()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $axe = Alias\FieryWarAxe::create();

        $game->resolver->resolveSequence($game, [
            new SetManaAvailable($game->currentPlayer(), 10),
            new PutCardInHand($game->currentPlayer(), $axe),
        ]);

        $game->action(new PlayCardRefactored($axe));

        (new AttackCharacter($game->currentPlayer()->hero->id(), $game->idlePlayer()->hero->id()))->run($game);
        $this->assertEquals(1, $game->currentPlayer()->hero->weapon->durability->total());

        $game->action(new EndTurn());
        $game->action(new EndTurn());

        (new AttackCharacter($game->currentPlayer()->hero->id(), $game->idlePlayer()->hero->id()))->run($game);
        $this->assertNull($game->currentPlayer()->hero->weapon);
    }

    public function test_change_weapon()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $axe = Alias\FieryWarAxe::create();

        $game->resolver->resolveSequence($game, [
            new SetManaAvailable($game->currentPlayer(), 10),
            new PutCardInHand($game->currentPlayer(), $axe),
        ]);

        $game->action(new PlayCardRefactored($axe));

        $this->assertEquals(3, $game->currentPlayer()->hero->weapon->attack->total());
        $this->assertEquals(3, $game->currentPlayer()->hero->attack->total());

        (new AttackCharacter($game->currentPlayer()->hero->id(), $game->idlePlayer()->hero->id()))->run($game);
        $this->assertEquals(1, $game->currentPlayer()->hero->weapon->durability->total());
        $this->assertEquals(27, $game->idlePlayer()->hero->health->total());

        $game->action(new EndTurn());
        $game->action(new EndTurn());

        $axe2 = Alias\FieryWarAxe::create();
        $game->resolver->resolveSequence($game, [new PutCardInHand($game->currentPlayer(), $axe2)]);
        $game->resolver->resolveSequence($game, [new SetManaAvailable($game->currentPlayer(), 10)]);

        (new PlayCard($axe2->id()))->run($game);
        $this->assertEquals(2, $game->currentPlayer()->hero->weapon->durability->total());

        (new AttackCharacter($game->currentPlayer()->hero->id(), $game->idlePlayer()->hero->id()))->run($game);
        $this->assertEquals(1, $game->currentPlayer()->hero->weapon->durability->total());

        $game->action(new EndTurn());
        $game->action(new EndTurn());


        (new AttackCharacter($game->currentPlayer()->hero->id(), $game->idlePlayer()->hero->id()))->run($game);
    }
}