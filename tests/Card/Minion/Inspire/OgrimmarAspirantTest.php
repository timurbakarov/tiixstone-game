<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\UseHeroPowerRefactored;

class OgrimmarAspirantTest extends TestCase
{
    public function test_inspire()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\OrgrimmarAspirant::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 2),
            new Block\Weapon\EquipWeapon($game->currentPlayer(), Alias\Ashbringer::create()),
        ]);

        $game->action(new UseHeroPowerRefactored($minion));

        $this->assertEquals(6, $game->currentPlayer()->hero->weapon->attack->total());
    }
}