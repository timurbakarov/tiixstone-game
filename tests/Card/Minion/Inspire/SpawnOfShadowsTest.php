<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\UseHeroPowerRefactored;

class SpawnOfShadowsTest extends TestCase
{
    public function test_inspire()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\SpawnOfShadows::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 2),
        ]);

        $game->action(new UseHeroPowerRefactored($minion));

        $this->assertEquals(16, $game->currentPlayer()->hero->health->total());
        $this->assertEquals(16, $game->idlePlayer()->hero->health->total());
    }
}