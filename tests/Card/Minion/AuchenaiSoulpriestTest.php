<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Card\Alias\Sheep;
use PHPUnit\Framework\TestCase;

class AuchenaiSoulpriestTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest([], [Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create()]);
        $game->start();

        $minion = Alias\AuchenaiSoulpriest::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\HealCharacter($game->currentPlayer()->hero, 5),
        ]);

        $this->assertEquals(15, $game->currentPlayer()->hero->health->total());
    }
}