<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;

class TaurenWarriorTest extends TestCase
{
    public function test_enrage()
    {
        $game = Factory::createForTest();
        $game->start();

        $minion = Card\Alias\TaurenWarrior::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\TakeDamage($minion, 1),
        ]);

        $this->assertEquals(5, $minion->attack->total());

        $game->resolver->resolveSequence($game, [
            new Block\HealCharacter($minion, 1),
        ]);

        $this->assertEquals(2, $minion->attack->total());
    }
}