<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;

class AngryChickenTest extends TestCase
{
    public function test_enrage()
    {
        $game = Factory::createForTest();
        $game->start();

        $chicken = Card\Alias\AngryChicken::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $chicken),
            new Block\GiveEffect($chicken, Effect\OneHealthEffect::class),
        ]);

        $this->assertEquals(1, $chicken->attack->total());
        $this->assertEquals(2, $chicken->health->total());

        $game->resolver->resolveSequence($game, [
            new Block\TakeDamage($chicken, 1),
        ]);

        $this->assertEquals(6, $chicken->attack->total());
        $this->assertEquals(1, $chicken->health->total());
    }
}