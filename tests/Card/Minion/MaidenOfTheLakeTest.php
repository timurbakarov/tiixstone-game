<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class MaidenOfTheLakeTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest();
        $game->start();

        $minion = Alias\MaidenOfTheLake::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $this->assertEquals(1, $game->currentPlayer()->hero->power->cost->total());
    }
}