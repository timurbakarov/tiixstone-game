<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Action\EndTurn;
use PHPUnit\Framework\TestCase;

class TarCreeperTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest();
        $game->start();

        $minion = Alias\TarCreeper::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $this->assertEquals(1, $minion->attack->total());

        $game->action(new EndTurn());

        $this->assertEquals(3, $minion->attack->total());
    }
}