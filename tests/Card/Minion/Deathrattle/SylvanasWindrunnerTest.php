<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\TauntEffect;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Block\MarkDestroyed;
use Tiixstone\Card\Alias\Abomination;
use Tiixstone\Card\Alias\ChillwindYeti;
use Tiixstone\Card\Alias\SylvanasWindrunner;
use Tiixstone\Effect\Deathrattle\AbominationDeathrattle;

class SylvanasWindrunnerTest extends TestCase
{
    public function test_it_control_enemy_minion()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $sylvana = SylvanasWindrunner::create();
        $enemy = ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new SummonMinion($game->currentPlayer(), $sylvana),
            new SummonMinion($game->idlePlayer(), $enemy),
        ]);

        $this->assertFalse($game->currentPlayer()->board->has($enemy->id()));

        $game->resolver->resolveSequence($game, [new MarkDestroyed($sylvana)]);

        $this->assertTrue($game->currentPlayer()->board->has($enemy->id()));
    }
}