<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Factory;
use Tiixstone\Action\PlayCard;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\Hyena;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Block\MarkDestroyed;
use Tiixstone\Card\Alias\SavannahHighmane;
use Tiixstone\Block\SetManaAvailable;

class SavannahHighmaneTest extends TestCase
{
    public function test_it_works()
    {
        $highmane = SavannahHighmane::create();

        $game = Factory::createForTest([$highmane]);
        $game->start();

        $game->resolver->resolveSequence($game, [
            new SetManaAvailable($game->currentPlayer(), 10),
        ]);

        (new PlayCard($highmane->id()))->run($game);

        $this->assertTrue($game->currentPlayer()->board->has($highmane->id()));
    }

    public function test_deathrattle()
    {
        $highmane = SavannahHighmane::create();

        $game = Factory::createForTest();
        $game->start();

        $game->resolver->resolveSequence($game, [
            new SummonMinion($game->currentPlayer(), $highmane),
            new MarkDestroyed($highmane),
        ]);

        $this->assertFalse($game->currentPlayer()->board->has($highmane->id()));

        $this->assertEquals(2, $game->currentPlayer()->board->count());

        $minions = $game->currentPlayer()->board->all();

        foreach($minions as $minion) {
            $this->assertInstanceOf(Hyena::className(), $minion);
        }
    }
}
