<?php

namespace Tiixstone\Tests\Cards\Item;

use PHPUnit\Framework\TestCase;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Block;

class TentacleOfNZothTest extends TestCase
{
    public function test_it_deals_2_damage()
    {
        $game = Factory::createForTest();
        $game->start();

        $minion = Alias\TentacleOfNZoth::create();
        $yeti = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->currentPlayer(), $yeti),
			new Block\MarkDestroyed($minion),
        ]);
		
		$this->assertEquals(4, $yeti->health->total());
    }
}