<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\Voidlord;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Block\MarkDestroyed;

class VoidlordTest extends TestCase
{
    public function test_deathrattle()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $voidlord = Voidlord::create();

        $game->resolver->resolveSequence($game, [
            new SummonMinion($game->currentPlayer(), $voidlord),
            new MarkDestroyed($voidlord),
        ]);

        $this->assertEquals(3, $game->currentPlayer()->board->count());
    }
}