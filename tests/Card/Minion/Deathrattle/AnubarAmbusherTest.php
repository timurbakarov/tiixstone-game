<?php

namespace Tiixstone\Tests\Cards\Item;

use PHPUnit\Framework\TestCase;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Block;

class AnubarAmbusherTest extends TestCase
{
    public function test_it_deals_2_damage()
    {
        $game = Factory::createForTest();
        $game->start();

        $minion = Alias\AnubarAmbusher::create();
        $sheep = Alias\Sheep::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->currentPlayer(), $sheep),
			new Block\MarkDestroyed($minion),
        ]);
		
		$this->assertEquals(1, $game->currentPlayer()->hand->count());
    }
}