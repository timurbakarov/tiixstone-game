<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\Voidlord;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Card\Alias\PossessedLackey;

class PossessedLackeyTest extends TestCase
{
    public function test_deathrattle()
    {
        $game = Factory::createForTest(Factory::createCardsArray(10, Voidlord::className()));
        $game->start();

        $lackey = PossessedLackey::create();
        $voidlord = Voidlord::create();

        $game->resolver->resolveSequence($game, [
            new SummonMinion($game->currentPlayer(), $lackey),
        ]);

        $this->assertTrue(true);
    }
}