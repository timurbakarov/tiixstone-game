<?php

namespace Tiixstone\Tests\Cards\Item;

use PHPUnit\Framework\TestCase;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Block;

class DrygulchJailorTest extends TestCase
{
    public function test_deathrattle()
    {
        $game = Factory::createForTest();
        $game->start();

        $jailor = Alias\DrygulchJailor::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $jailor),
			new Block\MarkDestroyed($jailor),
        ]);
		
		$this->assertEquals(3, $game->currentPlayer()->hand->count());
    }
}