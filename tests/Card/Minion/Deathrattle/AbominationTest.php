<?php

namespace Tiixstone\Tests\Cards\Item;

use PHPUnit\Framework\TestCase;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Block;

class AbominationTest extends TestCase
{
    public function test_it_deals_2_damage()
    {
        $game = Factory::createForTest();
        $game->start();

        $minion = Alias\Abomination::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
			new Block\MarkDestroyed($minion),
        ]);
		
		$this->assertEquals(18, $game->currentPlayer()->hero->health->total());
    }
}