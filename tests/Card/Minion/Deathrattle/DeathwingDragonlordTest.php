<?php

namespace Tiixstone\Tests\Cards\Item;

use PHPUnit\Framework\TestCase;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Block;

class DeathwingDragonlordTest extends TestCase
{
    public function test_deathrattle_with_dragon_in_hand()
    {
        $game = Factory::createForTest();
        $game->start();

        $minion = Alias\DeathwingDragonlord::create();
        $dragon = Alias\FaerieDragon::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
			new Block\PutCardInHand($game->currentPlayer(), $dragon),
			new Block\MarkDestroyed($minion),
        ]);

        $this->assertInstanceOf(Alias\FaerieDragon::className(), $game->currentPlayer()->board->first());
    }

    public function test_deathrattle_with_no_dragon_in_hand()
    {
        $game = Factory::createForTest();
        $game->start();

        $minion = Alias\DeathwingDragonlord::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\MarkDestroyed($minion),
        ]);

        $this->assertEquals(0, $game->currentPlayer()->board->count());
    }
}