<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Card\Alias\Sheep;
use PHPUnit\Framework\TestCase;

class OldMurkEyeTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\OldMurkEye::create();
        $murloc = Alias\MurlocScout::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->currentPlayer(), $murloc),
        ]);

        $this->assertEquals(3, $minion->attack->total());
    }
}