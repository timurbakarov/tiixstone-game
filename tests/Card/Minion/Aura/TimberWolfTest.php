<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Race;
use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Card\Minion;
use PHPUnit\Framework\TestCase;

class TimberWolfTest extends TestCase
{
    public function test_it_gives_plus_one_attack_to_friendly_beast_minions()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\TimberWolf::create();

        $enemyBeast = new Beast();
        $enemyMinion = new JustMinion();
        $friendlyBeast = new Beast();
        $friendlyMinion = new JustMinion();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->currentPlayer(), $friendlyMinion),
            new Block\SummonMinion($game->currentPlayer(), $friendlyBeast),
            new Block\SummonMinion($game->idlePlayer(), $enemyMinion),
            new Block\SummonMinion($game->idlePlayer(), $enemyBeast),
        ]);

        $this->assertEquals(1, $minion->attack->total());
        $this->assertEquals(3, $friendlyBeast->attack->total());
        $this->assertEquals(2, $friendlyMinion->attack->total());
        $this->assertEquals(2, $enemyBeast->attack->total());
        $this->assertEquals(2, $enemyMinion->attack->total());
    }
}

class JustMinion extends Minion
{    /**
 * @return int
 */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    public function globalId(): string
    {
        return '';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }
}

class Beast extends Minion
{
    /**
     * @var
     */
    protected $race = Race::BEAST;

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    public function globalId(): string
    {
        return '';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }
}