<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Battlecryable;
use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Card\Attackable;
use Tiixstone\Card\Character;
use Tiixstone\Card\Minion;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\PlayCardRefactored;
use Tiixstone\Game;

class BrannBronzebeardTest extends TestCase
{
    public function test_it_doubles_battlecry()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Card\Alias\BrannBronzebeard::create();
        $otherMinion = new SomeMinionBrann();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\PutCardInHand($game->currentPlayer(), $otherMinion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new PlayCardRefactored($otherMinion));

        $this->assertEquals(18, $game->idlePlayer()->hero->health->total());
    }

    public function test_branss_doesnt_stack()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Card\Alias\BrannBronzebeard::create();
        $minion2 = Card\Alias\BrannBronzebeard::create();
        $otherMinion = new SomeMinionBrann();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->currentPlayer(), $minion2),
            new Block\PutCardInHand($game->currentPlayer(), $otherMinion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new PlayCardRefactored($otherMinion));

        $this->assertEquals(18, $game->idlePlayer()->hero->health->total());
    }
}

class SomeMinionBrann extends Card\Minion implements Battlecryable
{
    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }

    public function globalId(): string
    {
        return '';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        return [new Block\TakeDamage($game->idlePlayer()->hero, 1)];
    }
}