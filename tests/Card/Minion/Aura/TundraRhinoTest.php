<?php

namespace Tiixstone\Tests\Card;

use Tiixstone\Action\AttackCharacter;
use Tiixstone\Action\PlayCard;
use Tiixstone\Block\PutCardInHand;
use Tiixstone\Exception;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Block\SetManaAvailable;
use Tiixstone\Card\Alias\TundraRhino;

class TundraRhinoTest extends TestCase
{
    public function test_it_can_attack_only_once()
    {
        $this->expectException(Exception::class);

        $game = Factory::createForTest();
        $game->start();

        $rhino = TundraRhino::create();

        $game->resolver->resolveSequence($game, [
            new SetManaAvailable($game->currentPlayer(), 10),
            new PutCardInHand($game->currentPlayer(), $rhino),
        ]);

        (new PlayCard($rhino->id()))->run($game);
        (new AttackCharacter($rhino->id(), $game->idlePlayer()->hero->id()))->run($game);
        (new AttackCharacter($rhino->id(), $game->idlePlayer()->hero->id()))->run($game);
    }
}