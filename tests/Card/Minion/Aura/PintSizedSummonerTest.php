<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Action\PlayCardRefactored;
use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;

class PintSizedSummonerTest extends TestCase
{
    public function test_effect()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Card\Alias\PintSizedSummoner::create();
        $otherMinion1 = new SomeMinion();
        $otherMinion2 = new SomeMinion();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\PutCardInHand($game->currentPlayer(), $otherMinion1),
            new Block\PutCardInHand($game->currentPlayer(), $otherMinion2),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $this->assertEquals(4, $otherMinion1->cost->total());

        $game->action(new PlayCardRefactored($otherMinion1));

        $this->assertEquals(5, $otherMinion2->cost->total());
    }
}

class SomeMinion extends Card\Minion
{
    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }

    public function globalId(): string
    {
        return '';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }
}