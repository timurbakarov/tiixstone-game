<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Battlecryable;
use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Card\Attackable;
use Tiixstone\Card\Character;
use Tiixstone\Card\Minion;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\PlayCardRefactored;
use Tiixstone\Game;

class WarsongCommanderTest extends TestCase
{
    public function test_it_doubles_battlecry()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Card\Alias\WarsongCommander::create();
        $otherMinion = Card\Alias\Wolfrider::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->currentPlayer(), $otherMinion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $this->assertEquals(4, $otherMinion->attack->total());
    }
}