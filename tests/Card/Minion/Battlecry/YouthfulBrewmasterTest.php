<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Exception;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Card\Alias\Sheep;
use PHPUnit\Framework\TestCase;

class YouthfulBrewmasterTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest([], [Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create()]);
        $game->start();

        $minion = Alias\YouthfulBrewmaster::create();
        $minionOnBoard = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->currentPlayer(), $minionOnBoard),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($minion, null, $minionOnBoard));

        $this->assertInstanceOf(Alias\ChillwindYeti::className(), $game->currentPlayer()->hand->first());
    }

    public function test_battlecry_with_no_targets()
    {
        $game = Factory::createForTest([], [Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create()]);
        $game->start();

        $minion = Alias\YouthfulBrewmaster::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($minion));

        $this->assertInstanceOf(Alias\YouthfulBrewmaster::className(), $game->currentPlayer()->board->first());
    }

    public function test_cant_use_battlecry_on_enemy_target()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Target should be friendly character");

        $game = Factory::createForTest([], [Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create()]);
        $game->start();

        $minion = Alias\YouthfulBrewmaster::create();

        $minionOnBoard = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->idlePlayer(), $minionOnBoard),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($minion, null, $minionOnBoard));

        $this->assertInstanceOf(Alias\YouthfulBrewmaster::className(), $game->currentPlayer()->board->first());
    }

    public function test_damaged_minion_returns_with_full_health()
    {
        $game = Factory::createForTest([], [Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create()]);
        $game->start();

        $minion = Alias\YouthfulBrewmaster::create();
        $minionOnBoard = Alias\ChillwindYeti::create();

        $minionOnBoard->health->damage(3);

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->currentPlayer(), $minionOnBoard),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($minion, null, $minionOnBoard));

        $this->assertEquals(5, $minionOnBoard->health->total());
    }
}