<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Card\Alias\Sheep;
use PHPUnit\Framework\TestCase;

class LordJaraxxusTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest([], [Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create()]);
        $game->start();

        $minion = Alias\LordJaraxxusMinion::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($minion));

        $this->assertInstanceOf(Alias\LordJaraxxusHero::className(), $game->currentPlayer()->hero);
//        $this->assertInstanceOf(Alias\INFERNO::className(), $game->currentPlayer()->hero->power);
//        $this->assertInstanceOf(Alias\BloodFury::className(), $game->currentPlayer()->hero->weapon);
    }
}