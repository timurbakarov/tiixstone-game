<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Effect\DivineShieldEffect;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\PlayCardRefactored;

class ArgenrProtectorTest extends TestCase
{
    public function test_it_gives_divine_shield()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\ArgentProtector::create();
        $sheep = Alias\Sheep::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->currentPlayer(), $sheep),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new PlayCardRefactored($minion, null, $sheep));

        $this->assertTrue($game->effects->cardHasEffect($sheep, DivineShieldEffect::class));
    }
}