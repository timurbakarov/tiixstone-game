<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\PlayCardRefactored;

class VoodooDoctorTest extends TestCase
{
    public function test_it_restores_3_health()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\VoodooDoctor::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new PlayCardRefactored($minion, null, $game->idlePlayer()->hero));

        $this->assertEquals(22, $game->idlePlayer()->hero->health->total());
    }
}