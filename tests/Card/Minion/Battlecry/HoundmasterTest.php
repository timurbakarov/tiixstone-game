<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Effect\TauntEffect;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\PlayCardRefactored;

class HoundmasterTest extends TestCase
{
    public function test_it_gives_windfury_to_minion()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\Houndmaster::create();
        $sheep = Alias\Sheep::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->currentPlayer(), $sheep),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new PlayCardRefactored($minion, null, $sheep));

        $this->assertEquals(3, $sheep->attack->total());
        $this->assertEquals(3, $sheep->health->total());
        $this->assertTrue($game->effects->cardHasEffect($sheep, TauntEffect::class));
    }
}