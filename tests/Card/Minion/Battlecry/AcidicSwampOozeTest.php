<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Card\Alias\Sheep;
use PHPUnit\Framework\TestCase;

class AcidicSwampOozeTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest([], [Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create()]);
        $game->start();

        $minion = Alias\AcidicSwampOoze::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\Weapon\EquipWeapon($game->idlePlayer(), Alias\ArcaniteReaper::create()),
        ]);

        $game->action(new Action\PlayCardRefactored($minion));

        $this->assertFalse($game->idlePlayer()->hero->hasWeapon());
    }

    public function test_battlecry_with_no_targets_should_return_error()
    {
        $game = Factory::createForTest([], [Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create()]);
        $game->start();

        $minion = Alias\AcidicSwampOoze::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($minion));

        $this->assertInstanceOf(Alias\AcidicSwampOoze::className(), $game->currentPlayer()->board->first());
    }
}