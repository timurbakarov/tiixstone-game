<?php

namespace Tiixstone\Tests\Cards;

use Tiixstone\Factory;
use Tiixstone\Action\PlayCard;
use Tiixstone\Block;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\TauntEffect;
use Tiixstone\Card\Alias;

class SpellbreakerTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spellbreaker = Alias\Spellbreaker::create();
		$taunt = Alias\Misha::create();
		
		$game->resolver->resolveSequence($game, [
			new Block\SetManaAvailable($game->currentPlayer(), 10),
			new Block\PutCardInHand($game->currentPlayer(), $spellbreaker),
			new Block\SummonMinion($game->idlePlayer(), $taunt),
		]);
		
		$game->action(new PlayCard($spellbreaker->id(), null, $taunt->id()));
		
		$this->assertFalse($game->effects->cardHasEffect($taunt, TauntEffect::class));
    }
}