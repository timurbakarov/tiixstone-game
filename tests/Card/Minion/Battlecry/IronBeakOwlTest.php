<?php

namespace Tiixstone\Tests\Cards;

use Tiixstone\Factory;
use Tiixstone\Block;
use Tiixstone\Card\Alias;
use Tiixstone\Action\PlayCard;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\TauntEffect;

class IronBeakOwlTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spellbreaker = Alias\IronbeakOwl::create();
		$taunt = Alias\Misha::create();
		
		$game->resolver->resolveSequence($game, [
			new Block\SetManaAvailable($game->currentPlayer(), 10),
			new Block\PutCardInHand($game->currentPlayer(), $spellbreaker),
			new Block\SummonMinion($game->idlePlayer(), $taunt),
		]);
		
		$game->action(new PlayCard($spellbreaker->id(), null, $taunt->id()));
		
		$this->assertFalse($game->effects->cardHasEffect($taunt, TauntEffect::class));
    }
}