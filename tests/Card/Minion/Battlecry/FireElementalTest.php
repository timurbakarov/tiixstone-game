<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Card\Alias\Sheep;
use PHPUnit\Framework\TestCase;

class FireElementalTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest([], [Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create()]);
        $game->start();

        $minion = Alias\FireElemental::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($minion, null, $game->idlePlayer()->hero));

        $this->assertEquals(27, $game->idlePlayer()->hero->health->total());
    }
}