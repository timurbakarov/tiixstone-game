<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\PlayCardRefactored;

class VulgarHomunculusTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\VulgarHomunculus::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new PlayCardRefactored($minion));

        $this->assertEquals(18, $game->currentPlayer()->hero->health->total());
    }
}