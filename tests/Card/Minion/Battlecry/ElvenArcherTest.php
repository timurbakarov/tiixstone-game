<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Exception;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Card\Alias\Sheep;
use PHPUnit\Framework\TestCase;

class ElvenArcherTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest([], [Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create()]);
        $game->start();

        $minion = Alias\ElvenArcher::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($minion, null, $game->idlePlayer()->hero));

        $this->assertEquals(29, $game->idlePlayer()->hero->health->total());
    }

    public function test_battlecry_with_no_targets_should_return_error()
    {
        $this->expectException(Exception::class);

        $game = Factory::createForTest([], [Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create()]);
        $game->start();

        $minion = Alias\ElvenArcher::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($minion));

        $this->assertEquals(29, $game->idlePlayer()->hero->health->total());
    }
}