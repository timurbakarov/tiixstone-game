<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\PlayCardRefactored;

class ResfreshmentVendorTest extends TestCase
{
    public function test_it_heals_heros_for_4()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\RefreshmentVendor::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new PlayCardRefactored($minion));

        $this->assertEquals(23, $game->currentPlayer()->hero->health->total());
        $this->assertEquals(23, $game->idlePlayer()->hero->health->total());
    }
}