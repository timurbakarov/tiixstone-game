<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\PlayCardRefactored;

class MurlocTidehunterTest extends TestCase
{
    public function test_it_gives_windfury_to_minion()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\MurlocTidehunter::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new PlayCardRefactored($minion));

        $this->assertEquals(2, $game->currentPlayer()->board->count());
    }
}