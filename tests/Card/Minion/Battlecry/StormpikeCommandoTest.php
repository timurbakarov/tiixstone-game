<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\PlayCardRefactored;

class StormpikeCommandoTest extends TestCase
{
    public function test_it_restores_3_health()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\StormpikeCommando::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new PlayCardRefactored($minion, null, $game->idlePlayer()->hero));

        $this->assertEquals(18, $game->idlePlayer()->hero->health->total());
    }
}