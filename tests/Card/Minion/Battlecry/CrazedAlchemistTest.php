<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Factory;
use Tiixstone\Action\PlayCard;
use PHPUnit\Framework\TestCase;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Block\PutCardInHand;
use Tiixstone\Card\Alias\MagmaRager;
use Tiixstone\Block\SetManaAvailable;
use Tiixstone\Card\Alias\CrazedAlchemist;
use Tiixstone\Card\Alias\FlametongueTotem;

class CrazedAlchemistTest extends TestCase
{
    public function test_it_swaps_attack_and_health_on_healty_minion()
    {
        $game = Factory::createForTest();
        $game->start();

        $crazedAlchemist = CrazedAlchemist::create();
        $minion = MagmaRager::create();

        $game->resolver->resolveSequence($game, [
            new SummonMinion($game->currentPlayer(), $minion),
            new PutCardInHand($game->currentPlayer(), $crazedAlchemist),
            new SetManaAvailable($game->currentPlayer(), 10),
        ]);

        (new PlayCard($crazedAlchemist->id(), null, $minion->id()))->run($game);

        $this->assertEquals(1, $minion->attack->total());
        $this->assertEquals(5, $minion->health->total());
    }

    public function test_it_swaps_attack_and_health_on_healty_minion_with_buffs()
    {
        $game = Factory::createForTest();
        $game->start();

        $crazedAlchemist1 = CrazedAlchemist::create();
        $crazedAlchemist2 = CrazedAlchemist::create();
        $minion = MagmaRager::create();
        $totem = FlametongueTotem::create();

        $game->resolver->resolveSequence($game, [
            new SummonMinion($game->currentPlayer(), $minion),
            new SummonMinion($game->currentPlayer(), $totem),
            new PutCardInHand($game->currentPlayer(), $crazedAlchemist1),
            new PutCardInHand($game->currentPlayer(), $crazedAlchemist2),
            new SetManaAvailable($game->currentPlayer(), 10),
        ]);

        (new PlayCard($crazedAlchemist1->id(), null, $minion->id()))->run($game);

        $this->assertEquals(3, $minion->attack->total());
        $this->assertEquals(7, $minion->health->total());

        (new PlayCard($crazedAlchemist2->id(), null, $minion->id()))->run($game);

        $this->assertEquals(9, $minion->attack->total());
        $this->assertEquals(3, $minion->health->total());
    }
}