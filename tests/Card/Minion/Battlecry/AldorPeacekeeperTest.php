<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\PlayCardRefactored;

class AldorPeacekeeperTest extends TestCase
{
    public function test_it_gives_windfury_to_minion()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\AldorPeacekeeper::create();
        $yeti = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->idlePlayer(), $yeti),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new PlayCardRefactored($minion, null, $yeti));

        $this->assertEquals(1, $yeti->attack->total());
    }
}