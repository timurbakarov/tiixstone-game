<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Factory;
use Tiixstone\Card\Alias\Sheep;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\PlayCard;
use Tiixstone\Card\Alias\Windspeaker;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Block\PutCardInHand;
use Tiixstone\Effect\WindfuryEffect;
use Tiixstone\Block\SetManaAvailable;

class WindspeakerTest extends TestCase
{
    public function test_it_gives_windfury_to_minion()
    {
        $game = Factory::createForTest([], [Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create()]);
        $game->start();

        $windspeaker = Windspeaker::create();
        $sheep = Sheep::create();

        $game->resolver->resolveBlock($game, new PutCardInHand($game->currentPlayer(), $windspeaker));
        $game->resolver->resolveBlock($game, new SummonMinion($game->currentPlayer(), $sheep));
        $game->resolver->resolveBlock($game, new SetManaAvailable($game->currentPlayer(), 10));

        (new PlayCard($windspeaker->id(), null, $sheep->id()))->run($game);

        $this->assertTrue($game->effects->cardHasEffect($sheep, WindfuryEffect::class));
    }
}