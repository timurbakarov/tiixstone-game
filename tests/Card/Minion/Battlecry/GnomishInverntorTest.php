<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\PlayCardRefactored;

class GnomishInverntorTest extends TestCase
{
    public function test_it_draw_card()
    {
        $game = Factory::createForTest([Alias\Sheep::create()], []);
        $game->start();

        $minion = Alias\GnomishInventor::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new PlayCardRefactored($minion));

        $this->assertEquals(1, $game->currentPlayer()->hand->count());
    }
}