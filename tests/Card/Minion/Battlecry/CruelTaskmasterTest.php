<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\PlayCardRefactored;

class CruelTaskmasterTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\CruelTaskmaster::create();
        $yeti = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->currentPlayer(), $yeti),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new PlayCardRefactored($minion, null, $yeti));

        $this->assertEquals(6, $yeti->attack->total());
        $this->assertEquals(4, $yeti->health->total());
    }
}