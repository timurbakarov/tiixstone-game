<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\PlayCardRefactored;

class ColdlightOracleTest extends TestCase
{
    public function test_it_draws_cards_for_you_and_your_opponent()
    {
        $game = Factory::createForTest([]);
        $game->start();

        $minion = Alias\ColdlightOracle::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->currentPlayer()->deck->appendMany([Alias\Sheep::create(), Alias\Sheep::create()]);
        $game->idlePlayer()->deck->appendMany([Alias\Sheep::create(), Alias\Sheep::create()]);

        $game->action(new PlayCardRefactored($minion));

        $this->assertEquals(2, $game->currentPlayer()->hand->count());
        $this->assertEquals(3, $game->idlePlayer()->hand->count());
    }
}