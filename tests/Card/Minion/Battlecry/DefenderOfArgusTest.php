<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Effect\TauntEffect;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\PlayCardRefactored;

class DefenderOfArgusTest extends TestCase
{
    public function test_it_gives_windfury_to_minion()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\DefenderOfArgus::create();
        $sheep1 = Alias\Sheep::create();
        $sheep2 = Alias\Sheep::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $sheep1),
            new Block\SummonMinion($game->currentPlayer(), $sheep2),
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new PlayCardRefactored($minion, $sheep2));

        $this->assertEquals(2, $sheep1->attack->total());
        $this->assertEquals(2, $sheep2->attack->total());
        $this->assertEquals(2, $sheep1->health->total());
        $this->assertEquals(2, $sheep2->health->total());
        $this->assertTrue($game->effects->cardHasEffect($sheep1, TauntEffect::class));
        $this->assertTrue($game->effects->cardHasEffect($sheep2, TauntEffect::class));
    }
}