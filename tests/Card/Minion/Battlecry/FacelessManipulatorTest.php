<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\PlayCardRefactored;

class FacelessManipulatorTest extends TestCase
{
    public function test_it_gives_windfury_to_minion()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\FacelessManipulator::create();
        $sheep = Alias\Sheep::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->currentPlayer(), $sheep),
            new Block\GiveEffect($sheep, new Effect\AttackChangerEffect(5)),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new PlayCardRefactored($minion, null, $sheep));

        $this->assertInstanceOf(Alias\Sheep::className(), $game->currentPlayer()->board->last());
        $this->assertEquals(6, $game->currentPlayer()->board->last()->attack->total());
    }
}