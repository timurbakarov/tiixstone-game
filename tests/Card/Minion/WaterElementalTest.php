<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Action\AttackCharacterRefactored;
use Tiixstone\Block;
use Tiixstone\Effect\ChargeEffect;
use Tiixstone\Effect\FreezeEffect;
use Tiixstone\Effect\HealthChangerEffect;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Card\Alias\Sheep;
use PHPUnit\Framework\TestCase;

class WaterElementalTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest([], [Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create(), Sheep::create()]);
        $game->start();

        $minion = Alias\WaterElemental::create();
        $sheep = Sheep::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\GiveEffect($minion, ChargeEffect::class),
            new Block\SummonMinion($game->idlePlayer(), $sheep),
            new Block\GiveEffect($sheep, new HealthChangerEffect(10)),
        ]);

        $game->action(new AttackCharacterRefactored($minion, $sheep));

        $this->assertTrue($game->effects->cardHasEffect($sheep, FreezeEffect::class));
    }
}