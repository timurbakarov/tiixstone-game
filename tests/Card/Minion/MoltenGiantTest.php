<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Card\Alias\Sheep;
use PHPUnit\Framework\TestCase;

class MoltenGiantTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\MoltenGiant::create();

        $game->currentPlayer()->hero->health->current->set(10);

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $minion),
        ]);

        $this->assertEquals(5, $minion->cost->total());
    }
}