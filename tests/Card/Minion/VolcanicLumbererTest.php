<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Action\EndTurn;
use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Card\Alias\Sheep;
use PHPUnit\Framework\TestCase;

class VolcanicLumbererTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\VolcanicLumberer::create();

        $minion1 = Sheep::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion1),
            new Block\PutCardInHand($game->currentPlayer(), $minion),
            new Block\MarkDestroyed($minion1),
        ]);

        $this->assertEquals(8, $minion->cost->total());

        $game->action(new EndTurn());

        $this->assertEquals(9, $minion->cost->total());
    }
}