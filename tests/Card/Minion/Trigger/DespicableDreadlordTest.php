<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Action\EndTurn;
use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;

class DespicableDreadlordTest extends TestCase
{
    public function test_it_draw_card_in_damage()
    {
        $game = Factory::createForTest([Card\Alias\Sheep::create()], []);
        $game->start();

        $item = Card\Alias\DespicableDreadlord::create();
        $sheep = Card\Alias\Sheep::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $item),
            new Block\SummonMinion($game->idlePlayer(), $sheep),
        ]);

        $game->action(new EndTurn());

        $this->assertEquals(0, $game->currentPlayer()->board->count());
    }
}