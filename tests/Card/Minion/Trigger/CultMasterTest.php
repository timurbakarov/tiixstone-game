<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;

class CultMasterTest extends TestCase
{
    public function test_it_draw_card_in_damage()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $item = Card\Alias\CultMaster::create();
        $minion = Card\Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $item),
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\Deck\AddCardToDeck($game->currentPlayer(), Card\Alias\Sheep::create()),
            new Block\MarkDestroyed($minion),
        ]);

        $this->assertEquals(1, $game->currentPlayer()->hand->count());
    }
}