<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Action\EndTurn;
use PHPUnit\Framework\TestCase;

class DoomsayerTest extends TestCase
{
    public function test_it_draw_card_in_damage()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $item = Card\Alias\Doomsayer::create();
        $minion = Card\Alias\Sheep::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), $item),
            new Block\SummonMinion($game->idlePlayer(), $minion),
        ]);

        $game->action(new EndTurn());

        $this->assertEquals(0, $game->currentPlayer()->board->count());
    }
}