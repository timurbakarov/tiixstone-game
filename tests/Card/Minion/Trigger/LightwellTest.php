<?php

namespace Tiixstone\Tests\Cards;

use Tiixstone\Factory;
use Tiixstone\Action\EndTurn;
use PHPUnit\Framework\TestCase;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Card\Alias\Lightwell;
use Tiixstone\Card\Alias\ChillwindYeti;

class LightwellTest extends TestCase
{
    public function test_it_heal_friendly_damaged_minion()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $lightwell = Lightwell::create();
        $damagedMinion = ChillwindYeti::create();

        $damagedMinion->health->damage(4);

        $game->resolver->resolveSequence($game, [
            new SummonMinion($game->idlePlayer(),$lightwell),
            new SummonMinion($game->idlePlayer(), $damagedMinion)
        ]);

        $game->action(new EndTurn());

        $this->assertEquals(4, $damagedMinion->health->total());
    }

//    public function test_it_heal_damaged_hero()
//    {
//        $game = Factory::createForTestWithDecks();
//        $game->start();
//
//        $lightwell = Lightwell::create();
//
//        $game->idlePlayer()->hero->health->damage(10);
//
//        $game->resolver->resolveSequence($game, [
//            new SummonMinion($game->idlePlayer(),$lightwell),
//        ]);
//
//        $game->action(new EndTurn());
//
//        $this->assertEquals(23, $game->currentPlayer()->hero->health->total());
//    }
}