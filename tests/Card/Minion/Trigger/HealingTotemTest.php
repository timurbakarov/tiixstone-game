<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class HealingTotemTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\HealingTotem::create();
        $otherMinion = Alias\ChillwindYeti::create();

        $otherMinion->health->damage(2);

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->currentPlayer(), $otherMinion),
        ]);

        $game->action(new Action\EndTurn());

        $this->assertEquals(4, $otherMinion->health->total());
    }
}