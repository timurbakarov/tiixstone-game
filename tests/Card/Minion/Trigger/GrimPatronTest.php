<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;

class GrimPatronTest extends TestCase
{
    public function test_it_summon_another_Grim_Patron_if_takes_non_lethal_damage()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $item = Card\Alias\GrimPatron::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $item),
            new Block\TakeDamage($item, 1),
        ]);

        $this->assertEquals(2, $game->currentPlayer()->board->count());
    }

    public function test_it_doesnt_summon_another_Grim_Patron_if_takes_lethal_damage()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $item = Card\Alias\GrimPatron::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $item),
            new Block\TakeDamage($item, 5),
        ]);

        $this->assertEquals(0, $game->currentPlayer()->board->count());
    }
}