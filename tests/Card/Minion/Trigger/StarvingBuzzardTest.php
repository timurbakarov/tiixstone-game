<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class StarvingBuzzardTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\StarvingBuzzard::create();

        $game->resolver->resolveSequence($game, [
            new Block\Deck\AddCardToDeck($game->currentPlayer(), Alias\Sheep::create()),
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->currentPlayer(), Alias\ChillwindYeti::create()),
        ]);

        $this->assertEquals(0, $game->currentPlayer()->hand->count());

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), Alias\Sheep::create()),
        ]);

        $this->assertEquals(1, $game->currentPlayer()->hand->count());
    }
}