<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class NorthshireClerikTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\NorthshireCleric::create();
        $minion->health->current->decrease(2);

        $game->resolver->resolveSequence($game, [
            new Block\Deck\AddCardToDeck($game->currentPlayer(), Alias\Sheep::create()),
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\HealCharacter($minion, 1),
        ]);

        $this->assertEquals(1, $game->currentPlayer()->hand->count());
    }
}