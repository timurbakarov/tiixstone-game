<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;

class AcolyteOfPainTest extends TestCase
{
    public function test_it_draw_card_in_damage()
    {
        $game = Factory::createForTest([Card\Alias\Sheep::create()], []);
        $game->start();

        $item = Card\Alias\AcolyteOfPain::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $item),
            new Block\TakeDamage($item, 1),
        ]);

        $this->assertEquals(1, $game->currentPlayer()->hand->count());
    }
}