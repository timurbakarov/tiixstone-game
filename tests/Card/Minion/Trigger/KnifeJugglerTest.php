<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;

class KnifeJugglerTest extends TestCase
{
    public function test_it_works()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $game->resolver->resolveBlock($game, new Block\SummonMinion($game->currentPlayer(), Card\Alias\KnifeJuggler::create()));
        $game->resolver->resolveBlock($game, new Block\SummonMinion($game->currentPlayer(), Card\Alias\Sheep::create()));

        $this->assertEquals(19, $game->idlePlayer()->hero->health->total());
    }
}