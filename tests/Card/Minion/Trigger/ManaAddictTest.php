<?php

namespace Tiixstone\Tests\Cards;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Character;
use PHPUnit\Framework\TestCase;

class ManaAddictTest extends TestCase
{
    public function test_it_gets_plus_2_attack_when_you_whenever_you_play_spell()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $manaAddict = Card\Alias\ManaAddict::create();
        $spell1 = new SomeSpell();
        $spell2 = new SomeSpell();
        $sheep = Card\Alias\Sheep::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $manaAddict),
            new BLock\PutCardInHand($game->currentPlayer(), $spell1),
            new BLock\PutCardInHand($game->currentPlayer(), $spell2),
            new BLock\PutCardInHand($game->currentPlayer(), $sheep),
        ]);

        $game->action(new Action\PlayCardRefactored($spell1));
        $game->action(new Action\PlayCardRefactored($spell2));
        $game->action(new Action\PlayCardRefactored($sheep));

        $this->assertEquals(5, $manaAddict->attack->total());
    }

    public function test_it_doesnt_get_2_attack_when_opponent_play_spells()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $manaAddict = Card\Alias\ManaAddict::create();
        $spell1 = new SomeSpell();
        $spell2 = new SomeSpell();
        $sheep = Card\Alias\Sheep::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), $manaAddict),
            new BLock\PutCardInHand($game->currentPlayer(), $spell1),
            new BLock\PutCardInHand($game->currentPlayer(), $spell2),
            new BLock\PutCardInHand($game->currentPlayer(), $sheep),
        ]);

        $game->action(new Action\PlayCardRefactored($spell1));
        $game->action(new Action\PlayCardRefactored($spell2));
        $game->action(new Action\PlayCardRefactored($sheep));

        $this->assertEquals(1, $manaAddict->attack->total());
    }
}

class SomeSpell extends Card\Spell
{

    public function globalId(): string
    {
        return '';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    public function cast(Game $game, Character $target = NULL): array
    {
        return [];
    }
}