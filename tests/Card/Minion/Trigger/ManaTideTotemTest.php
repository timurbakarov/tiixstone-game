<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class ManaTideTotemTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest([Alias\Sheep::create()], []);
        $game->start();

        $minion = Alias\ManaTideTotem::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $game->action(new Action\EndTurn());

        $this->assertEquals(1, $game->idlePlayer()->hand->count());
    }
}