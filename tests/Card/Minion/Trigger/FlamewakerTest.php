<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Action\PlayCardRefactored;
use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;

class FlamewakerTest extends TestCase
{
    public function test_it_deals_2_random_damage_after_you_play_spell()
    {
        $game = Factory::createForTest([Card\Alias\Sheep::create()], []);
        $game->start();

        $item = Card\Alias\Flamewaker::create();
        $coin = Card\Alias\TheCoin::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $item),
            new Block\PutCardInHand($game->currentPlayer(), $coin),
        ]);

        $game->action(new PlayCardRefactored($coin));

        $this->assertEquals(18, $game->idlePlayer()->hero->health->total());
    }
}