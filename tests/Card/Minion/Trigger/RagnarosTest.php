<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Exception;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class RagnarosTest extends TestCase
{
    public function test_it_cant_attack()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Character can not attack');

        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\RagnarosTheFirelord::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $game->action(new Action\EndTurn());
        $game->action(new Action\EndTurn());

        $game->action(new Action\AttackCharacterRefactored($minion, $game->idlePlayer()->hero));
    }

    public function test_it_can_attack_after_silence()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\RagnarosTheFirelord::create();
        $owl = Alias\IronbeakOwl::create();

        $game->idlePlayer()->hero->health->current->set(30);

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\PutCardInHand($game->currentPlayer(), $owl),
            new Block\SetManaCrystals($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\EndTurn());
        $game->action(new Action\EndTurn());

        $game->action(new Action\PlayCardRefactored($owl, null, $minion));
        $game->action(new Action\AttackCharacterRefactored($minion, $game->idlePlayer()->hero));

        $this->assertEquals(9, $game->idlePlayer()->hero->health->total());
    }

    public function test_it_triggers_effect_at_the_end_of_your_turn()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\RagnarosTheFirelord::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $game->action(new Action\EndTurn());

        $this->assertEquals(7, $game->currentPlayer()->hero->health->total());
    }
}