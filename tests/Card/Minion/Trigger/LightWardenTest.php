<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;

class LightWardenTest extends TestCase
{
    public function test_it_draw_card_in_damage()
    {
        $game = Factory::createForTest([Card\Alias\Sheep::create()], []);
        $game->start();

        $item = Card\Alias\Lightwarden::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $item),
            new Block\HealCharacter($game->currentPlayer()->hero, 2),
        ]);

        $this->assertEquals(3, $item->attack->total());
    }
}