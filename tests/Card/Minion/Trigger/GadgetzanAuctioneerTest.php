<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Action\PlayCardRefactored;
use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;

class GadgetzanAuctioneerTest extends TestCase
{
    public function test_it_draw_card_in_damage()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $item = Card\Alias\GadgetzanAuctioneer::create();
        $coin = Card\Alias\TheCoin::create();

        $game->resolver->resolveSequence($game, [
            new Block\Deck\AddCardToDeck($game->currentPlayer(), Card\Alias\Sheep::create()),
            new Block\SummonMinion($game->currentPlayer(), $item),
            new Block\PutCardInHand($game->currentPlayer(), $coin),
        ]);

        $game->action(new PlayCardRefactored($coin));

        $this->assertEquals(1, $game->currentPlayer()->hand->count());
    }
}