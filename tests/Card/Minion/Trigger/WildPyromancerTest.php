<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Action\PlayCardRefactored;
use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;

class WildPyromancerTest extends TestCase
{
    public function test_it_works()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $coin = Card\Alias\TheCoin::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), Card\Alias\WildPyromancer::create()),
            new Block\SummonMinion($game->idlePlayer(), Card\Alias\ChillwindYeti::create()),
            new Block\PutCardInHand($game->currentPlayer(), $coin),
        ]);

        $game->action(new PlayCardRefactored($coin));

        $this->assertEquals(1, $game->currentPlayer()->board->first()->health->total());
        $this->assertEquals(4, $game->idlePlayer()->board->first()->health->total());
    }
}