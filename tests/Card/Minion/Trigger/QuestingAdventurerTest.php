<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Action\PlayCardRefactored;
use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;

class QuestingAdventurerTest extends TestCase
{
    public function test_it_gets_1_attack_and_1_health_whenever_your_player_play_card()
    {
        $game = Factory::createForTest([Card\Alias\Sheep::create()], []);
        $game->start();

        $item = Card\Alias\QuestingAdventurer::create();
        $sheep = Card\Alias\Sheep::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $item),
            new Block\PutCardInHand($game->currentPlayer(), $sheep),
        ]);

        $game->action(new PlayCardRefactored($sheep));

        $this->assertEquals(3, $item->health->total());
        $this->assertEquals(3, $item->attack->total());
    }
}