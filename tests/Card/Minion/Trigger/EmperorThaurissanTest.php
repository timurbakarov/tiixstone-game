<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Action\EndTurn;
use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;

class EmperorThaurissanTest extends TestCase
{
    public function test_it_draw_card_in_damage()
    {
        $game = Factory::createForTest([Card\Alias\Sheep::create()], []);
        $game->start();

        $item = Card\Alias\EmperorThaurissan::create();
        $card = Card\Alias\MindControl::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $item),
            new Block\PutCardInHand($game->currentPlayer(), $card),
        ]);

        $game->action(new EndTurn());

        $this->assertEquals(9, $card->cost->total());
    }

    public function test_stack_effects()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $item = Card\Alias\EmperorThaurissan::create();
        $item2 = Card\Alias\EmperorThaurissan::create();
        $card = Card\Alias\MindControl::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $item),
            new Block\SummonMinion($game->currentPlayer(), $item2),
            new Block\PutCardInHand($game->currentPlayer(), $card),
        ]);

        $game->action(new EndTurn());

        $this->assertEquals(8, $card->cost->total());
    }
}