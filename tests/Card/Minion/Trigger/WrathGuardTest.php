<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;

class WrathGuardTest extends TestCase
{
    public function test_it_draw_card_in_damage()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $item = Card\Alias\Wrathguard::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $item),
            new Block\TakeDamage($item, 3),
        ]);

        $this->assertEquals(17, $game->currentPlayer()->hero->health->total());
    }
}