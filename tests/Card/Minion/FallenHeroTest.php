<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Action\UseHeroPowerRefactored;

class FallenHeroTest extends TestCase
{
    public function test_it_add_1_damage_to_hero_power_that_produce_damage()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\FallenHero::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new UseHeroPowerRefactored($game->idlePlayer()->hero));

        $this->assertEquals(18, $game->idlePlayer()->hero->health->total());
    }
}