<?php

namespace Tiixstone\Tests\Card\Minion\Battlecry;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class BaronRivendareTest extends TestCase
{
    public function test_battlecry()
    {
        $game = Factory::createForTest([], []);
        $game->start();

        $minion = Alias\BaronRivendare::create();
        $harvest = Alias\HarvestGolem::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->currentPlayer(), $harvest),
            new Block\MarkDestroyed($harvest),
        ]);

        $this->assertEquals(3, $game->currentPlayer()->board->count());
    }
}