<?php

namespace Tiixstone\Tests\Cards\Item;

use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\BoulderfistOgre;
use Tiixstone\Card\Alias\ChillwindYeti;
use Tiixstone\Card\Alias\KillCommand;
use Tiixstone\Action\PlayCard;
use Tiixstone\Block\PutCardInHand;
use Tiixstone\Block\SetManaAvailable;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Card\Minion;
use Tiixstone\Factory;
use Tiixstone\Race;

class KillCommandTest extends TestCase
{
    public function test_it_deal_3_damage_if_no_beasts_on_board()
    {
        $game = Factory::createForTest();
        $game->start();

        $yeti = ChillwindYeti::create();
        $killCommand = KillCommand::create();

        $game->resolver->resolveBlock($game, new SummonMinion($game->idlePlayer(), $yeti));
        $game->resolver->resolveBlock($game, new PutCardInHand($game->currentPlayer(), $killCommand));
        $game->resolver->resolveBlock($game, new SetManaAvailable($game->currentPlayer(), 10));

        (new PlayCard($killCommand->id(), null, $yeti->id()))->run($game);

        $this->assertEquals(2, $yeti->health->total());
    }

    public function test_it_deal_5_damage_if_beast_on_board()
    {
        $game = Factory::createForTest();
        $game->start();

        $orge = BoulderfistOgre::create();
        $killCommand = KillCommand::create();

        $game->resolver->resolveBlock($game, new SummonMinion($game->idlePlayer(), $orge));
        $game->resolver->resolveBlock($game, new SummonMinion($game->currentPlayer(), new GenericBeast()));
        $game->resolver->resolveBlock($game, new PutCardInHand($game->currentPlayer(), $killCommand));
        $game->resolver->resolveBlock($game, new SetManaAvailable($game->currentPlayer(), 10));

        (new PlayCard($killCommand->id(), null, $orge->id()))->run($game);

        $this->assertEquals(2, $orge->health->total());
    }
}

class GenericBeast extends Minion
{
    protected $race = Race::BEAST;

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    public function globalId(): string
    {
        return '123';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }
}