<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Action;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Block\PutCardInHand;
use Tiixstone\Block\SetManaAvailable;
use Tiixstone\Card\Alias\ChillwindYeti;
use Tiixstone\Card\Alias\ArcaneIntellect;

class ArcaneIntellectTest extends TestCase
{
    public function testCast()
    {
        $game = Factory::createForTest(
            Factory::createCardsArray(30, ChillwindYeti::className()),
            Factory::createCardsArray(30, ChillwindYeti::className())
        );

        $game->start();
        $this->assertEquals(4, $game->currentPlayer()->hand->count());

        $arcaneIntellect = ArcaneIntellect::create();

        $game->resolver->resolveSequence($game, [
            new SetManaAvailable($game->currentPlayer(), 10),
            new PutCardInHand($game->currentPlayer(), $arcaneIntellect),
        ]);

        (new Action\PlayCard($arcaneIntellect->id()))->run($game);
        
        $this->assertEquals(6, $game->currentPlayer()->hand->count());
    }
}