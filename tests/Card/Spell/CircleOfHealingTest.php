<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class CircleOfHealingTest extends TestCase
{
    public function test_can_cast_on_undamaged_minion()
    {
        $game = Factory::createForTest();
        $game->start();

        $circleOfHealing = Alias\CircleOfHealing::create();
        $yeti = Alias\ChillwindYeti::create();

        $yeti->health->damage(4);

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $circleOfHealing),
            new Block\SummonMinion($game->currentPlayer(), $yeti),
        ]);

        $game->action(new Action\PlayCardRefactored($circleOfHealing));

        $this->assertEquals(5, $yeti->health->total());
    }
}