<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block\PutCardInHand;
use Tiixstone\Block\SetManaAvailable;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Factory;
use Tiixstone\Exception;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\Execute;
use Tiixstone\Action\PlayCard;
use Tiixstone\Card\Alias\ChillwindYeti;

class ExecuteTest extends TestCase
{
    public function test_can_destroy_damaged_minion()
    {
        $game = Factory::createForTest();
        $game->start();

        $execute = Execute::create();
        $yeti = ChillwindYeti::create();
        $yeti->health->current->set(4);

        $game->resolver->resolveSequence($game, [
           new SetManaAvailable($game->currentPlayer(), 10),
           new SummonMinion($game->idlePlayer(), $yeti),
           new PutCardInHand($game->currentPlayer(), $execute),
        ]);

        (new PlayCard($execute->id(), null, $yeti->id()))->run($game);

        $this->assertFalse($game->idlePlayer()->board->has($yeti->id()));
    }

    public function test_can_not_destroy_undamaged_minion()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Target should be damaged");

        $game = Factory::createForTest();
        $game->start();

        $execute = Execute::create();
        $yeti = ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new SetManaAvailable($game->currentPlayer(), 10),
            new SummonMinion($game->idlePlayer(), $yeti),
            new PutCardInHand($game->currentPlayer(), $execute),
        ]);

        (new PlayCard($execute->id(), null, $yeti->id()))->run($game);

        $this->assertFalse($game->idlePlayer()->board->has($yeti->id()));
    }
}