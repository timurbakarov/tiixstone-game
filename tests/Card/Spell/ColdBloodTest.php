<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class ColdBloodTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $coldBlood1 = Alias\ColdBlood::create();
        $coldBlood2 = Alias\ColdBlood::create();

        $minion = Alias\Sheep::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $coldBlood1),
            new Block\PutCardInHand($game->currentPlayer(), $coldBlood2),
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($coldBlood1, null, $minion));
        $game->action(new Action\PlayCardRefactored($coldBlood2, null, $minion));

        $this->assertEquals(7, $minion->attack->total());
    }
}