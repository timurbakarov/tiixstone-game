<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class SiphonSoulTest extends TestCase
{
    public function test_is_works()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\SiphonSoul::create();
        $minion = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $minion));

        $this->assertEquals(0, $game->currentPlayer()->board->count());
        $this->assertEquals(23, $game->currentPlayer()->hero->health->total());
    }
}