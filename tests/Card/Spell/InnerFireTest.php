<?php

namespace Tiixstone\Tests\Cards\Item;

use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\InnerFire;
use Tiixstone\Card\Alias\StormwindKnight;
use Tiixstone\Action\PlayCard;
use Tiixstone\Block\PutCardInHand;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Factory;

class InnerFireTest extends TestCase
{
    public function test_it_works()
    {
        $game = Factory::createForTest();
        $game->start();

        $innerFire = InnerFire::create();
        $knight = StormwindKnight::create();

        $game->resolver->resolveSequence($game, [
            new PutCardInHand($game->currentPlayer(), $innerFire),
            new SummonMinion($game->currentPlayer(), $knight),
        ]);

        (new PlayCard($innerFire->id(), null, $knight->id()))->run($game);

        $this->assertEquals(5, $knight->attack->total());
    }
}