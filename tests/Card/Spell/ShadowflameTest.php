<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class ShadowflameTest extends TestCase
{
    public function test_can_cast_on_undamaged_minion()
    {
        $game = Factory::createForTest();
        $game->start();

        $shadowflame = Alias\Shadowflame::create();
        $yeti = Alias\ChillwindYeti::create();
        $yeti2 = Alias\ChillwindYeti::create();

        $yeti->health->damage(4);

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $shadowflame),
            new Block\SummonMinion($game->currentPlayer(), $yeti),
            new Block\SummonMinion($game->idlePlayer(), $yeti2),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($shadowflame, null, $yeti));

        $this->assertEquals(1, $yeti2->health->total());
        $this->assertFalse($game->currentPlayer()->board->has($yeti->id()));
    }
}