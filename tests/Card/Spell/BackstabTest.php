<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Exception;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class BackstabTest extends TestCase
{
    public function test_can_cast_on_undamaged_minion()
    {
        $game = Factory::createForTest();
        $game->start();

        $backstab = Alias\Backstab::create();
        $yeti = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $backstab),
            new Block\SummonMinion($game->currentPlayer(), $yeti),
        ]);

        $game->action(new Action\PlayCardRefactored($backstab, null, $yeti));

        $this->assertEquals(3, $yeti->health->total());
    }

    public function test_can_not_cast_on_damaged_minion()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Target should have full health");

        $game = Factory::createForTest();
        $game->start();

        $backstab = Alias\Backstab::create();
        $yeti = Alias\ChillwindYeti::create();

        $yeti->health->current->set(3);

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $backstab),
            new Block\SummonMinion($game->idlePlayer(), $yeti),
        ]);

        $game->action(new Action\PlayCardRefactored($backstab, null, $yeti));
    }
}