<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Effect\SilenceEffect;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\TauntEffect;

class EqualityTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\Equality::create();
        $minion = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($spell));

        $this->assertEquals(1, $minion->health->total());
    }

    public function test_equality_with_wildpyromancer()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\Equality::create();
        $minion = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->currentPlayer(), Alias\WildPyromancer::create()),
        ]);

        $game->action(new Action\PlayCardRefactored($spell));

        $this->assertEquals(0, $game->currentPlayer()->board->count());
    }

    public function test_equality_with_silence()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\Equality::create();
        $minion = Alias\ChillwindYeti::create();
        $silence = Alias\Silence::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\PutCardInHand($game->currentPlayer(), $silence),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($spell));

        $game->resolver->resolveSequence($game, [
            new Block\GiveEffect($minion, SilenceEffect::class),
        ]);

        $this->assertEquals(1, $game->currentPlayer()->board->count());
    }
}