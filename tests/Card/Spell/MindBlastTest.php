<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class MindBlastTest extends TestCase
{
    public function test_it_deals_damage_to_an_enemy_hero()
    {
        $game = Factory::createForTest();
        $game->start();

        $mindBlast = Alias\MindBlast::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $mindBlast),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($mindBlast));

        $this->assertEquals(15, $game->idlePlayer()->hero->health->total());
    }
}