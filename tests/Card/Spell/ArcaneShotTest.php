<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\ArcaneShot;
use Tiixstone\Card\Alias\ChillwindYeti;

class ArcaneShotTest extends TestCase
{
    public function testCast()
    {
        $game = Factory::createForTest();
        $game->start();

        $yeti = ChillwindYeti::create();
        $arcaneShot = ArcaneShot::create();

        $game->resolver->resolveBlock($game, new Block\SummonMinion($game->idlePlayer(), $yeti));
        $game->resolver->resolveBlock($game, new Block\PutCardInHand($game->currentPlayer(), $arcaneShot));
        $game->resolver->resolveBlock($game, new Block\SetManaAvailable($game->currentPlayer(), 10));

        (new Action\PlayCard($arcaneShot->id(), null, $yeti->id()))->run($game);

        $this->assertEquals(3, $yeti->health->total());
    }
}