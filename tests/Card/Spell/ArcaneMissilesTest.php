<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\ChillwindYeti;
use Tiixstone\Card\Alias\ArcaneMissiles;

class ArcaneMissilesTest extends TestCase
{
    public function test_deal_damage_to_all_characters()
    {
        $game = Factory::createForTest(
            Factory::createCardsArray(30, ChillwindYeti::create()),
            Factory::createCardsArray(30, ChillwindYeti::create())
        );
        $game->start();

        $arcaneMissiles = ArcaneMissiles::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $arcaneMissiles),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($arcaneMissiles));
        $this->assertEquals(27, $game->idlePlayer()->hero->health->total());
    }
}