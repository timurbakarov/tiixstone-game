<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class DemonwrathTest extends TestCase
{
    public function test_it_deals_2_damage_to_all_minions_except_demons()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\Demonwrath::create();
        $minion = Alias\ChillwindYeti::create();
        $demon = Alias\Infernal::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->currentPlayer(), $demon),
        ]);

        $game->action(new Action\PlayCardRefactored($spell));

        $this->assertEquals(3, $minion->health->total());
        $this->assertEquals(6, $demon->health->total());
    }
}