<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Action;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\TheCoin;

class TheCoinTest extends TestCase
{
    public function testAddMana()
    {
        $game = Factory::createForTest();
        $game->start();

        $this->assertEquals(1, $game->currentPlayer()->maximumMana());
        $this->assertEquals(1, $game->currentPlayer()->availableMana());

        $theCoin = TheCoin::create();
        $game->currentPlayer()->hand->append($theCoin);

        (new Action\PlayCard($theCoin->id()))->run($game);

        $this->assertEquals(1, $game->currentPlayer()->maximumMana());
        $this->assertEquals(2, $game->currentPlayer()->availableMana());
    }
}