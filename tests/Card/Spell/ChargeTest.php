<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Exception;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\ChargeEffect;

class ChargeTest extends TestCase
{
    public function test_it_gives_charge()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\Charge::create();
        $minion = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $minion));

        $this->assertTrue($game->effects->cardHasEffect($minion, ChargeEffect::class));
    }

    public function test_it_can_not_attack_hero()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Target should be a minion");

        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\Charge::create();
        $minion = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $minion));

        $game->action(new Action\AttackCharacterRefactored($minion, $game->idlePlayer()->hero));
    }
}