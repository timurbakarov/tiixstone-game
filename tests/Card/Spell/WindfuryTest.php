<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Effect\WindfuryEffect;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class WindfuryTest extends TestCase
{
    public function test_can_cast_on_undamaged_minion()
    {
        $game = Factory::createForTest();
        $game->start();

        $windfury = Alias\Windfury::create();
        $yeti = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $windfury),
            new Block\SummonMinion($game->currentPlayer(), $yeti),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($windfury, null, $yeti));

        $this->assertTrue($game->effects->cardHasEffect($yeti, WindfuryEffect::class));
    }
}