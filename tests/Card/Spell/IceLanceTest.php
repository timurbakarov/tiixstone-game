<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\FreezeEffect;

class IceLanceTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\IceLance::create();
        $spell2 = Alias\IceLance::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\PutCardInHand($game->currentPlayer(), $spell2),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $game->idlePlayer()->hero));
        $this->assertTrue($game->effects->cardHasEffect($game->idlePlayer()->hero, FreezeEffect::class));

        $game->action(new Action\PlayCardRefactored($spell2, null, $game->idlePlayer()->hero));
        $this->assertEquals(16, $game->idlePlayer()->hero->health->total());
    }
}