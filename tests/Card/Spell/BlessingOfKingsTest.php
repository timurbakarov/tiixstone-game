<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Factory;
use Tiixstone\Exception;
use Tiixstone\Block\PutCardInHand;
use Tiixstone\Block\SetManaAvailable;
use Tiixstone\Block\SummonMinion;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias;
use Tiixstone\Action\PlayCard;
use Tiixstone\Card\Alias\ChillwindYeti;

class BlessingOfKingsTest extends TestCase
{
    public function testCanDestroyDamagedMinion()
    {
        $game = Factory::createForTest();
        $game->start();

        $blessingOfKings = Alias\BlessingOfKings::create();
        $yeti = ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
           new SetManaAvailable($game->currentPlayer(), 10),
           new SummonMinion($game->currentPlayer(), $yeti),
           new PutCardInHand($game->currentPlayer(), $blessingOfKings),
        ]);

        $game->action(new PlayCard($blessingOfKings->id(), null, $yeti->id()));

        $this->assertEquals(8, $yeti->attack->total());
		$this->assertEquals(9, $yeti->health->total());
    }
}