<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\TauntEffect;

class SinisterStrikeTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\SinisterStrike::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($spell));

        $this->assertEquals(17, $game->idlePlayer()->hero->health->total());
    }
}