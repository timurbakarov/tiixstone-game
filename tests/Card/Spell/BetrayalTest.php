<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class BetrayalTest extends TestCase
{
    public function test_it_deals_damage_and_draw_card()
    {
        $game = Factory::createForTest([]);
        $game->start();

        $spell = Alias\Betrayal::create();

        $yeti1 = Alias\ChillwindYeti::create();
        $yeti2 = Alias\ChillwindYeti::create();
        $sheep = Alias\Sheep::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->idlePlayer(), $yeti1),
            new Block\SummonMinion($game->idlePlayer(), $sheep),
            new Block\SummonMinion($game->idlePlayer(), $yeti2),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $sheep));

        $this->assertEquals(4, $yeti1->health->total());
        $this->assertEquals(4, $yeti2->health->total());
    }
}