<?php

namespace Tiixstone\Tests\Cards\Item;

use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\BluegillWarrior;
use Tiixstone\Card\Alias\BoulderfistOgre;
use Tiixstone\Card\Alias\ShadowWordPain;
use Tiixstone\Action\PlayCard;
use Tiixstone\Block\SetManaAvailable;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Exception;
use Tiixstone\Factory;

class ShadowWordPainTest extends TestCase
{
    public function test_it_destroy_minions_with_3_or_less_attack()
    {
        $game = Factory::createForTest([ShadowWordPain::create()]);
        $game->start();

        $bluegillWarrior = BluegillWarrior::create();

        $game->resolver->resolveSequence($game, [
            new SetManaAvailable($game->currentPlayer(), 10),
            new SummonMinion($game->idlePlayer(), $bluegillWarrior),
        ]);

        (new PlayCard($game->currentPlayer()->hand->first()->id(), null, $bluegillWarrior->id()))->run($game);

        $this->assertFalse($game->idlePlayer()->board->has($bluegillWarrior->id()));
    }

    public function test_it_can_not_destroy_minions_with_4_or_more_attack()
    {
        $this->expectException(Exception::class);

        $game = Factory::createForTest([ShadowWordPain::create()]);
        $game->start();

        $minion = BoulderfistOgre::create();

        $game->resolver->resolveSequence($game, [
            new SetManaAvailable($game->currentPlayer(), 10),
            new SummonMinion($game->idlePlayer(), $minion),
        ]);

        (new PlayCard($game->currentPlayer()->hand->first()->id(), null, $minion->id()))->run($game);

        $this->assertFalse($game->idlePlayer()->board->has($minion->id()));
    }
}