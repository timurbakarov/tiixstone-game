<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class FistOfJaraxxusTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\FistOfJaraxxus::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($spell));

        $this->assertEquals(16, $game->idlePlayer()->hero->health->total());
    }

    public function test_it_deals_damage_if_discarded()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\FistOfJaraxxus::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\Discard\DiscardOne($spell),
        ]);

        $this->assertEquals(16, $game->idlePlayer()->hero->health->total());
    }
}