<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\Sheep;
use Tiixstone\Card\Alias\Defile;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Block\PutCardInHand;
use Tiixstone\Block\SetManaAvailable;
use Tiixstone\Card\Alias\ChillwindYeti;
use Tiixstone\Action\PlayCardRefactored;

class DefileTest extends TestCase
{
    public function test_it_repeats_if_any_minion_is_destroyed()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $defile = Defile::create();
        $minion1 = Sheep::create();
        $minion3 = Sheep::create();
        $minion2 = ChillwindYeti::create();

        $minion3->health->current->increase(1);

        $game->resolver->resolveSequence($game, [
            new SetManaAvailable($game->currentPlayer(), 10),
            new PutCardInHand($game->currentPlayer(), $defile),
            new SummonMinion($game->idlePlayer(), $minion1),
            new SummonMinion($game->currentPlayer(), $minion2),
            new SummonMinion($game->currentPlayer(), $minion3),
        ]);

        $game->action(new PlayCardRefactored($defile));

        $this->assertFalse($game->idlePlayer()->board->has($minion1->id()));
        $this->assertFalse($game->currentPlayer()->board->has($minion3->id()));
        $this->assertEquals(2, $minion2->health->total());
    }
}