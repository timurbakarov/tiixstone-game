<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Effect\SilenceEffect;
use Tiixstone\Exception;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\TauntEffect;

class ShadowMadnessTest extends TestCase
{
    public function test_it_take_control_minion()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\ShadowMadness::create();
        $minion = Alias\Sheep::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->idlePlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $minion));

        $this->assertTrue($minion->getPlayer()->id() == $game->currentPlayer()->id());
    }

    public function test_it_cant_take_control_minion_if_minion_has_more_than_3_attack()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Attack should be lower than 4");

        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\ShadowMadness::create();
        $minion = Alias\Sheep::create();

        $minion->attack->current->set(4);

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->idlePlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $minion));

        $this->assertTrue($minion->getPlayer()->id() == $game->currentPlayer()->id());
    }

    public function test_minion_returns_back_at_the_end_of_the_turn()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\ShadowMadness::create();
        $minion = Alias\Sheep::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->idlePlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $minion));

        $game->action(new Action\EndTurn());

        $this->assertTrue($minion->getPlayer()->id() == $game->currentPlayer()->id());
    }

    public function test_minion_returns_back_if_silenced()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\ShadowMadness::create();
        $minion = Alias\Sheep::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->idlePlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $minion));

        $game->resolver->resolveSequence($game, [new Block\GiveEffect($minion, SilenceEffect::class)]);

        $this->assertTrue($minion->getPlayer()->id() == $game->idlePlayer()->id());
    }
}