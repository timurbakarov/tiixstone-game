<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\ChillwindYeti;
use Tiixstone\Card\Alias\ArcaneExplosion;

class ArcaneExplosionTest extends TestCase
{
    public function test_spell()
    {
        $game = Factory::createForTest(
            Factory::createCardsArray(30, ChillwindYeti::className()),
            Factory::createCardsArray(30, ChillwindYeti::className())
        );

        $game->start();

        $arcaneExplosion = ArcaneExplosion::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->idlePlayer(), ChillwindYeti::create()),
            new Block\SummonMinion($game->idlePlayer(), ChillwindYeti::create()),
            new Block\SummonMinion($game->idlePlayer(), ChillwindYeti::create()),
            new Block\SummonMinion($game->idlePlayer(), ChillwindYeti::create()),

            new Block\PutCardInHand($game->currentPlayer(), $arcaneExplosion),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($arcaneExplosion));

        /** @var Card\Minion $minion */
        foreach($game->idlePlayer()->board->all() as $minion) {
            $this->assertEquals(4, $minion->health->total());
        }
    }
}