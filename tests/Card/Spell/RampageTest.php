<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\TauntEffect;

class RampageTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\Rampage::create();
        $minion = Alias\ChillwindYeti::create();
        $minion->health->current->set(4);

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $minion));

        $this->assertEquals(7, $minion->health->total());
        $this->assertEquals(7, $minion->attack->total());
    }
}