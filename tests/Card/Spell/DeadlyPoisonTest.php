<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Exception;
use Tiixstone\Card\Weapon;
use PHPUnit\Framework\TestCase;

class DeadlyPoisonTest extends TestCase
{
    public function testCantPlayIfNoWeapon()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Your hero should have weapon');

        $game = Factory::createForTestWithDecks();
        $game->start();

        $deadlyPoison = Card\Alias\DeadlyPoison::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $deadlyPoison),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        (new Action\PlayCard($deadlyPoison->id()))->run($game);
    }

    public function testSpell()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $axe = new SomeWeapon();
        $deadlyPoison = Card\Alias\DeadlyPoison::create();

        $game->resolver->resolveSequence($game, [
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\Weapon\EquipWeapon($game->currentPlayer(), $axe),
            new Block\PutCardInHand($game->currentPlayer(), $deadlyPoison),
        ]);

        $game->action(new Action\PlayCardRefactored($deadlyPoison));

        $this->assertEquals(2, $game->currentPlayer()->hero->weapon->attack->total());
    }
}

class SomeWeapon extends Weapon
{
    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }

    public function globalId(): string
    {
        return '';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 5;
    }
}