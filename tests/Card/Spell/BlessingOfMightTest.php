<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class BlessingOfMightTest extends TestCase
{
    public function test()
    {
        $game = Factory::createForTest();
        $game->start();

        $blessingOfMight = Alias\BlessingOfMight::create();
        $yeti = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $blessingOfMight),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->idlePlayer(), $yeti),
        ]);

        $game->action(new Action\PlayCardRefactored($blessingOfMight, null, $yeti));

        $this->assertEquals(7, $yeti->attack->total());
    }
}