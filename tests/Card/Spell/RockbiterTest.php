<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class RockbiterTest extends TestCase
{
    public function test_can_cast_on_undamaged_minion()
    {
        $game = Factory::createForTest();
        $game->start();

        $rockbiter = Alias\RockbiterWeapon::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $rockbiter),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($rockbiter, null, $game->currentPlayer()->hero));

        $this->assertEquals(3, $game->currentPlayer()->hero->attack->total());
    }
}