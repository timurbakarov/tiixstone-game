<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class DrainLifeTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $drainLife = Alias\DrainLife::create();

        $game->currentPlayer()->hero->health->current->set(20);

        $yeti = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $drainLife),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->idlePlayer(), $yeti),
        ]);

        $game->action(new Action\PlayCardRefactored($drainLife, null, $yeti));

        $this->assertEquals(3, $yeti->health->total());
        $this->assertEquals(22, $game->currentPlayer()->hero->health->total());
    }
}