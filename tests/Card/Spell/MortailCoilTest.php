<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class MortailCoilTest extends TestCase
{
    public function test_it_deals_1_damage()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\MortalCoil::create();
        $minion = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $minion));

        $this->assertEquals(4, $minion->health->total());
    }

    public function test_if_it_kills_draw_a_card()
    {
        $game = Factory::createForTest([]);
        $game->start();

        $spell = Alias\MortalCoil::create();
        $minion = Alias\ChillwindYeti::create();

        $minion->health->damage(4);

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $game->currentPlayer()->deck->append(Alias\Sheep::create());

        $game->action(new Action\PlayCardRefactored($spell, null, $minion));

        $this->assertEquals(1, $game->currentPlayer()->hand->count());
    }
}