<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\TauntEffect;

class LightOfTheNaaruTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\LightOfTheNaaru::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $game->currentPlayer()->hero));

        $this->assertEquals(23, $game->currentPlayer()->hero->health->total());
        $this->assertEquals(1, $game->currentPlayer()->board->count());
    }
}