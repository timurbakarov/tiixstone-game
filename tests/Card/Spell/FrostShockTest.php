<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Action;
use Tiixstone\Effect\FreezeEffect;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Block\PutCardInHand;
use Tiixstone\Block\SetManaAvailable;
use Tiixstone\Card;

class FrostShockTest extends TestCase
{
    public function testCast()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Card\Alias\FrostShock::create();

        $game->resolver->resolveSequence($game, [
            new SetManaAvailable($game->currentPlayer(), 10),
            new PutCardInHand($game->currentPlayer(), $spell),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $game->idlePlayer()->hero));
        
        $this->assertEquals(19, $game->idlePlayer()->hero->health->total());
        $this->assertTrue($game->effects->cardHasEffect($game->idlePlayer()->hero, FreezeEffect::class));
    }
}