<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class SapTest extends TestCase
{
    public function test_it_returns_card_to_hand()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\Sap::create();
        $yeti = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->idlePlayer(), $yeti),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $yeti));

        $this->assertEquals(2, $game->idlePlayer()->hand->count());
    }
}