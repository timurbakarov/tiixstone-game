<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Effect\ImmuneEffect;
use Tiixstone\Effect\Secret\IceBlock;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\TauntEffect;
use Tiixstone\Game;

class IceBlockTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest(
            [Alias\Sheep::create(), Alias\Sheep::create(), Alias\Sheep::create(), Alias\Sheep::create(), Alias\Sheep::create(), Alias\Sheep::create()],
            [Alias\Sheep::create(), Alias\Sheep::create(), Alias\Sheep::create(), Alias\Sheep::create(), Alias\Sheep::create(), Alias\Sheep::create()]
        );
        $game->start();

        $spell = Alias\IceBlock::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($spell));

        $game->action(new Action\EndTurn());

        $game->player2->hero->attack->current->set(50);

        $game->action(new Action\AttackCharacterRefactored($game->player2->hero, $game->player1->hero));

        $this->assertEquals(30, $game->player1->hero->health->total());
        $this->assertTrue($game->effects->cardHasEffect($game->player1->hero, ImmuneEffect::class));

        $game->action(new Action\EndTurn());

        $this->assertFalse($game->effects->cardHasEffect($game->player1->hero, ImmuneEffect::class));

        $this->assertEquals(0, $game->currentPlayer()->secrets->count());
        $this->assertFalse($game->effects->cardHasEffect($spell, IceBlock::class));
    }
}