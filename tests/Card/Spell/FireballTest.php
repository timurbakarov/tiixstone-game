<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class FireballTest extends TestCase
{
    public function testCast()
    {
        $game = Factory::createForTest(Factory::createCardsArray(30), Factory::createCardsArray(30));
        $game->start();

        $this->assertEquals(30, $game->idlePlayer()->hero->health->total());

        $fireball = Alias\Fireball::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $fireball),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($fireball, null, $game->idlePlayer()->hero));

        $this->assertEquals(24, $game->idlePlayer()->hero->health->total());
    }
}