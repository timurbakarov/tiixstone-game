<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Exception;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class ShadowBoltTest extends TestCase
{
    public function test_it_deals_4_damage()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\ShadowBolt::create();
        $minion = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $minion));

        $this->assertEquals(1, $minion->health->total());
    }

    public function test_can_not_target_hero()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Target should be a minion');

        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\ShadowBolt::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $game->idlePlayer()->hero));
    }
}