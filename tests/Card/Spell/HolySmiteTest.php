<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class HolySmiteTest extends TestCase
{
    public function test_it_destroy_minions_with_3_or_less_attack()
    {
        $game = Factory::createForTest([]);
        $game->start();

        $minion = Alias\BoulderfistOgre::create();
        $holySmite = Alias\HolySmite::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $holySmite),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->idlePlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($holySmite, null, $minion));

        $this->assertEquals(5, $minion->health->total());
    }
}