<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class PowerWordShieldTest extends TestCase
{
    public function test_can_cast_on_undamaged_minion()
    {
        $game = Factory::createForTest([Alias\ChillwindYeti::create()]);
        $game->start();

        $backstab = Alias\PowerWordShield::create();
        $yeti = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $backstab),
            new Block\SummonMinion($game->currentPlayer(), $yeti),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($backstab, null, $yeti));

        $this->assertEquals(7, $yeti->health->total());
        $this->assertEquals(1, $game->currentPlayer()->hand->count());
    }
}