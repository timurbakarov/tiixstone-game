<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class TwistingNetherTest extends TestCase
{
    public function test_it_destroys_all_minions()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\TwistingNether::create();
        $yeti = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $yeti),
        ]);

        $game->action(new Action\PlayCardRefactored($spell));

        $this->assertTrue($yeti->isDestroyed());
    }
}