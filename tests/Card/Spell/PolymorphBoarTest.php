<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class PolymorphBoarTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\PolymorphBoar::create();
        $minion = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $minion));

        $this->assertInstanceOf(Card\AT_005t::class, $game->currentPlayer()->board->first());
    }

    public function test_on_flamewaker()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\PolymorphBoar::create();
        $minion = Alias\Flamewaker::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $minion));

        $this->assertInstanceOf(Card\AT_005t::class, $game->currentPlayer()->board->first());
        $this->assertEquals(20, $game->idlePlayer()->hero->health->total());
    }
}