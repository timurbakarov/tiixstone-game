<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class WildGrowthTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\WildGrowth::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 5),
            new Block\SetManaCrystals($game->currentPlayer(), 5),
        ]);

        $game->action(new Action\PlayCardRefactored($spell));

        $this->assertEquals(6, $game->currentPlayer()->maximumMana());
    }
}