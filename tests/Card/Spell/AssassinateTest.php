<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Exception;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class AssassinateTest extends TestCase
{
    public function testDestroyEnemyMinion()
    {
        $game = Factory::createForTest();
        $game->start();

        $assassinate = Alias\Assassinate::create();
        $yeti = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\PutCardInHand($game->currentPlayer(), $assassinate),
            new Block\SummonMinion($game->idlePlayer(), $yeti),
        ]);

        $game->action(new Action\PlayCardRefactored($assassinate, null, $yeti));

        $this->assertFalse($game->idlePlayer()->board->has($yeti->id()));
    }

    public function testCantDestroyFriendlyMinion()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Target should be an enemy");

        $game = Factory::createForTest();
        $game->start();

        $game->currentPlayer()->setAvailableMana(10);
        $assassinate = Alias\Assassinate::create();

        $yeti = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\PutCardInHand($game->currentPlayer(), $assassinate),
            new BLock\SummonMinion($game->currentPlayer(), $yeti),
        ]);

        $game->action(new Action\PlayCardRefactored($assassinate, null, $yeti));
    }
}