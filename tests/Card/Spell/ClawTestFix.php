<?php

namespace Tiixstone\Tests\Cards\Item;

use PHPUnit\Framework\TestCase;
use Tiixstone\Cards\Item\Claw;
use Tiixstone\Cards\Item\TheCoin;
use Tiixstone\Factory;


class ClawTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest(Factory::createCardsArray(30), Factory::createCardsArray(30));
        $game->start();
        $game->currentPlayer()->hero->setAttackRate(5);
        $this->assertEquals(0, $game->currentPlayer()->hero->armor());
        $this->assertEquals(5, $game->currentPlayer()->hero->attack->rate($game));

        $claw = new Claw();
        $game->currentPlayer()->hand->append($claw);
        $game->action(new Action\PlayCard($claw->id()));
        $this->assertEquals(2, $game->currentPlayer()->hero->armor());
        $this->assertEquals(7, $game->currentPlayer()->hero->attack->rate($game));

        $game->action(new Action\EndTurn());
        $this->assertEquals(2, $game->idlePlayer()->hero->armor());
        $this->assertEquals(5, $game->idlePlayer()->hero->attack->rate($game));

        $game->action(new Action\EndTurn());
        $this->assertEquals(2, $game->currentPlayer()->hero->armor());
        $this->assertEquals(5, $game->currentPlayer()->hero->attack->rate($game));

        $game->action(new Action\EndTurn());
        $this->assertEquals(2, $game->idlePlayer()->hero->armor());
        $this->assertEquals(5, $game->idlePlayer()->hero->attack->rate($game));
    }
}