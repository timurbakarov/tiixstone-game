<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\TauntEffect;

class VanishTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\Vanish::create();
        $minion1 = Alias\ChillwindYeti::create();
        $minion2 = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion1),
            new Block\SummonMinion($game->idlePlayer(), $minion2),
        ]);

        $game->action(new Action\PlayCardRefactored($spell));

        $this->assertEquals(1, $game->currentPlayer()->hand->count());
        $this->assertEquals(2, $game->idlePlayer()->hand->count());
    }
}