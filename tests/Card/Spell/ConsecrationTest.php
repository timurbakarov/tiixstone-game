<?php

namespace Tiixstone\Tests\Cards\Item;

use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias;
use Tiixstone\Factory;
use Tiixstone\Card;
use Tiixstone\Action;
use Tiixstone\Block;

class ConsecrationTest extends TestCase
{
    public function testAddMana()
    {
        $game = Factory::createForTest();
        $game->start();

		$yeti = Alias\ChillwindYeti::create();
		$consecration = Alias\Consecration::create();
		
		$game->resolver->resolveSequence($game, [
			new Block\PutCardInHand($game->currentPlayer(), $consecration),
			new Block\SummonMinion($game->idlePlayer(), $yeti),
			new Block\SetManaAvailable($game->currentPlayer(), 10),
		]);
		
		$game->action(new Action\PlayCard($consecration->id()));
		
		$this->assertEquals(3, $yeti->health->total());
		$this->assertEquals(18, $game->idlePlayer()->hero->health->total());
    }
}