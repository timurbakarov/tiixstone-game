<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class SabotageTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\Sabotage::create();
        $minion = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->idlePlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($spell));

        $this->assertEquals(0, $game->idlePlayer()->board->count());
    }

    public function test_combo_destroys_weapon()
    {
        $game = Factory::createForTest();
        $game->start();

        $coin = Alias\TheCoin::create();
        $spell = Alias\Sabotage::create();
        $minion = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\Weapon\EquipWeapon($game->idlePlayer(), Alias\BattleAxe::create()),
            new Block\PutCardInHand($game->currentPlayer(), $coin),
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->idlePlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($coin));
        $game->action(new Action\PlayCardRefactored($spell));

        $this->assertEquals(0, $game->idlePlayer()->board->count());
        $this->assertFalse($game->idlePlayer()->hero->hasWeapon());
    }
}