<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class ShivTest extends TestCase
{
    public function test_it_deals_damage_and_draw_card()
    {
        $game = Factory::createForTest([Alias\Sheep::create()]);
        $game->start();

        $spell = Alias\Shiv::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $game->idlePlayer()->hero));

        $this->assertEquals(19, $game->idlePlayer()->hero->health->total());
        $this->assertEquals(1, $game->currentPlayer()->hand->count());
    }
}