<?php

namespace Tiixstone\Tests\Cards\Item;

use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\ChillwindYeti;
use Tiixstone\Card\Alias\DivineSpirit;
use Tiixstone\Action\PlayCard;
use Tiixstone\Block\HealCharacter;
use Tiixstone\Block\PutCardInHand;
use Tiixstone\Block\SetManaAvailable;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Block\TakeDamage;
use Tiixstone\Factory;

class DivineSpiritTest extends TestCase
{
    public function test_it_doubles_minion_health_with_full_health()
    {
        $game = Factory::createForTest();
        $game->start();

        $minion = ChillwindYeti::create();
        $divineSpirit = DivineSpirit::create();

        $game->resolver->resolveSequence($game, [
            new SummonMinion($game->currentPlayer(), $minion),
            new PutCardInHand($game->currentPlayer(), $divineSpirit),
            new SetManaAvailable($game->currentPlayer(), 10),
        ]);

        (new PlayCard($divineSpirit->id(), null, $minion->id()))->run($game);

        $this->assertEquals(10, $minion->health->total());
        $this->assertEquals(10, $minion->health->maximumTotal());

        $game->resolver->resolveSequence($game, [
            new TakeDamage($minion, 2),
            new HealCharacter($minion, 2),
        ]);

        $this->assertEquals(10, $minion->health->total());
    }

    public function test_it_doubles_damaged_minion()
    {
        $game = Factory::createForTest();
        $game->start();

        $minion = ChillwindYeti::create();
        $divineSpirit = DivineSpirit::create();

        $game->resolver->resolveSequence($game, [
            new SummonMinion($game->currentPlayer(), $minion),
            new PutCardInHand($game->currentPlayer(), $divineSpirit),
            new SetManaAvailable($game->currentPlayer(), 10),
            new TakeDamage($minion, 2),
        ]);

        (new PlayCard($divineSpirit->id(), null, $minion->id()))->run($game);

        $this->assertEquals(6, $minion->health->total());
        $this->assertEquals(5, $minion->health->maximum->get());
        $this->assertEquals(8, $minion->health->maximumTotal());

        $game->resolver->resolveSequence($game, [
            new TakeDamage($minion, 3),
            new HealCharacter($minion, 2),
        ]);

        $this->assertEquals(5, $minion->health->total());
        $this->assertEquals(8, $minion->health->maximumTotal());
    }
}