<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class PowerWordGloryTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\PowerWordGlory::create();
        $minion = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\Deck\AddCardToDeck($game->currentPlayer(), Alias\Sheep::create()),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $minion));
        $game->action(new Action\EndTurn());
        $game->action(new Action\EndTurn());

        $game->action(new Action\AttackCharacterRefactored($minion, $game->idlePlayer()->hero));

        $this->assertEquals(24, $game->currentPlayer()->hero->health->total());
    }
}