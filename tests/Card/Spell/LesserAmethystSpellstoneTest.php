<?php

namespace Tiixstone\Tests\Cards;

use Tiixstone\Card\Alias\AmethystSpellstone;
use Tiixstone\Card\Alias\Hellfire;
use Tiixstone\Card\Alias\LesserAmethystSpellstone;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\GreaterAmethystSpellstone;
use Tiixstone\Card\Alias\KoboldLibrarian;
use Tiixstone\Action\PlayCard;
use Tiixstone\Block\PutCardInHand;
use Tiixstone\Block\SetManaAvailable;

class LesserAmethystSpellstoneTest extends TestCase
{
    public function test_bug_with_hellfire_it_doesnt_update_twice()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $lesserAmethysySpellstone = LesserAmethystSpellstone::create();
        $hellfire = Hellfire::create();

        $game->resolver->resolveSequence($game, [
            new SummonMinion($game->currentPlayer(), KoboldLibrarian::create()),
            new PutCardInHand($game->currentPlayer(), $lesserAmethysySpellstone),
            new PutCardInHand($game->currentPlayer(), $hellfire),
            new SetManaAvailable($game->currentPlayer(), 10),
        ]);

        (new PlayCard($hellfire->id()))->run($game);

        $this->assertFalse($game->currentPlayer()->hand->has($lesserAmethysySpellstone->id()));
        $this->assertInstanceOf(AmethystSpellstone::className(), $game->currentPlayer()->hand->last());
    }

    public function test_it_updates_to_amethyst()
    {
        $game = Factory::createForTest();
        $game->start();

        $lesserAmethysySpellstone = LesserAmethystSpellstone::create();
        $kobold = KoboldLibrarian::create();

        $game->resolver->resolveSequence($game, [
            new PutCardInHand($game->currentPlayer(), $lesserAmethysySpellstone),
            new PutCardInHand($game->currentPlayer(), $kobold),
            new SetManaAvailable($game->currentPlayer(), 10),
        ]);

        (new PlayCard($kobold->id()))->run($game);

        $this->assertFalse($game->currentPlayer()->hand->has($lesserAmethysySpellstone->id()));
        $this->assertInstanceOf(AmethystSpellstone::className(), $game->currentPlayer()->hand->last());
    }

    public function test_amethyst_updates_to_greater()
    {
        $game = Factory::createForTest();
        $game->start();

        $amethysySpellstone = AmethystSpellstone::create();
        $kobold = KoboldLibrarian::create();

        $game->resolver->resolveSequence($game, [
            new PutCardInHand($game->currentPlayer(), $amethysySpellstone),
            new PutCardInHand($game->currentPlayer(), $kobold),
            new SetManaAvailable($game->currentPlayer(), 10),
        ]);

        (new PlayCard($kobold->id()))->run($game);

        $this->assertFalse($game->currentPlayer()->hand->has($amethysySpellstone->id()));
        $this->assertInstanceOf(GreaterAmethystSpellstone::className(), $game->currentPlayer()->hand->last());
    }

    public function test_it_wont_update_if_greater_amethyst()
    {
        $game = Factory::createForTestWithDecks();
        $game->start();

        $greaterAmethystSpellstone = GreaterAmethystSpellstone::create();
        $kobold = KoboldLibrarian::create();

        $game->resolver->resolveSequence($game, [
            new PutCardInHand($game->currentPlayer(), $greaterAmethystSpellstone),
            new PutCardInHand($game->currentPlayer(), $kobold),
            new SetManaAvailable($game->currentPlayer(), 10),
        ]);

        (new PlayCard($kobold->id()))->run($game);

        $this->assertTrue($game->currentPlayer()->hand->has($greaterAmethystSpellstone->id()));
    }
}