<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class AncestralSpiritTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\AncestralSpirit::create();
        $minion = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $minion));

        $game->resolver->resolveSequence($game, [
            new Block\MarkDestroyed($minion),
        ]);

        $this->assertEquals(1, $game->currentPlayer()->board->count());
    }
}