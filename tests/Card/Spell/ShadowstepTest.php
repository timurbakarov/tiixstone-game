<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class ShadowstepTest extends TestCase
{
    public function test_return_a_friendly_minion_to_hand_and_lower_cost_on_2()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\Shadowstep::create();
        $yeti = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $yeti),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $yeti));

        $this->assertInstanceOf(Alias\ChillwindYeti::className(), $game->currentPlayer()->hand->first());
        $this->assertEquals(2, $game->currentPlayer()->hand->first()->cost->total());
    }
}