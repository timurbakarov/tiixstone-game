<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\TauntEffect;

class SprintTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\Sprint::create();

        $game->resolver->resolveSequence($game, [
            new Block\Deck\AddCardToDeck($game->currentPlayer(), Alias\Sheep::create()),
            new Block\Deck\AddCardToDeck($game->currentPlayer(), Alias\Sheep::create()),
            new Block\Deck\AddCardToDeck($game->currentPlayer(), Alias\Sheep::create()),
            new Block\Deck\AddCardToDeck($game->currentPlayer(), Alias\Sheep::create()),
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($spell));

        $this->assertEquals(4, $game->currentPlayer()->hand->count());
    }
}