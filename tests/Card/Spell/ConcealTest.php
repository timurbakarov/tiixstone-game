<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\StealthEffect;

class ConcealTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\Conceal::create();
        $minion = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $minion));

        $this->assertTrue($game->effects->cardHasEffect($minion, StealthEffect::class));

        $game->action(new Action\EndTurn());

        $this->assertTrue($game->effects->cardHasEffect($minion, StealthEffect::class));

        $game->action(new Action\EndTurn());

        $this->assertFalse($game->effects->cardHasEffect($minion, StealthEffect::class));
    }
}