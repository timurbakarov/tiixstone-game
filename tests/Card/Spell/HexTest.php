<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class HexTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\Hex::create();
        $minion = Alias\ChillwindYeti::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion),
        ]);

        $game->action(new Action\PlayCardRefactored($spell, null, $minion));

        $this->assertInstanceOf(Alias\Frog::className(), $game->currentPlayer()->board->first());
    }
}