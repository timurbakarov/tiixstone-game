<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\TauntEffect;

class ClawTest extends TestCase
{
    public function testSpell() {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\Claw::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($spell));

        $this->assertEquals(2, $game->currentPlayer()->hero->attack->total());
        $this->assertEquals(2, $game->currentPlayer()->hero->armor->get());

        $game->action(new Action\EndTurn());

        $this->assertEquals(0, $game->currentPlayer()->hero->attack->total());
    }
}