<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class TotemicMightTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\TotemicMight::create();
        $minion = Alias\ChillwindYeti::create();
        $totem = Alias\HealingTotem::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
            new Block\SummonMinion($game->currentPlayer(), $minion),
            new Block\SummonMinion($game->currentPlayer(), $totem),
        ]);

        $game->action(new Action\PlayCardRefactored($spell));

        $this->assertEquals(5, $minion->health->total());
        $this->assertEquals(4, $totem->health->total());
    }
}