<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;
use Tiixstone\Effect\TauntEffect;

class UnleashTheHoundsTest extends TestCase
{
    public function test_it_summons_hounds()
    {
        $game = Factory::createForTest();
        $game->start();

        $spell = Alias\UnleashTheHounds::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $spell),
            new Block\SetManaAvailable($game->currentPlayer(), 10),

            new Block\SummonMinion($game->idlePlayer(), Alias\Sheep::create()),
            new Block\SummonMinion($game->idlePlayer(), Alias\Sheep::create()),
            new Block\SummonMinion($game->idlePlayer(), Alias\Sheep::create()),
            new Block\SummonMinion($game->idlePlayer(), Alias\Sheep::create()),
            new Block\SummonMinion($game->idlePlayer(), Alias\Sheep::create()),
        ]);

        $game->action(new Action\PlayCardRefactored($spell));

        $this->assertEquals(5, $game->currentPlayer()->board->count());
    }
}