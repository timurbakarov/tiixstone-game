<?php

namespace Tiixstone\Tests\Cards\Item;

use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use PHPUnit\Framework\TestCase;

class HealingTouchTest extends TestCase
{
    public function testSpell()
    {
        $game = Factory::createForTest();
        $game->start();

        $game->currentPlayer()->hero->health->current->set(20);

        $healingTouch = Alias\HealingTouch::create();

        $game->resolver->resolveSequence($game, [
            new Block\PutCardInHand($game->currentPlayer(), $healingTouch),
            new Block\SetManaAvailable($game->currentPlayer(), 10),
        ]);

        $game->action(new Action\PlayCardRefactored($healingTouch, null, $game->currentPlayer()->hero));

        $this->assertEquals(28, $game->currentPlayer()->hero->health->total());
    }
}