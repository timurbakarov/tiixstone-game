<?php

namespace Tiixstone\Tests;

use Tiixstone\Race;
use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Factory;
use Tiixstone\Card\Alias;
use Tiixstone\Card\Minion;
use PHPUnit\Framework\TestCase;

class MinionTest extends TestCase
{
	/**
     * @dataProvider minionsProvider
     */
    public function test_minions(Minion $minion, array $effects = [], Race $race = null)
    {
        $game = Factory::createForTest();
        $game->start();
		
		$game->resolver->resolveSequence($game, [
			new Block\SummonMinion($game->currentPlayer(), $minion),
		]);
		
		$this->assertInstanceof(get_class($minion), $game->currentPlayer()->board->first());

        if($race) {
            $this->assertTrue($minion->race()->is($race));
        }
		
		if($effects) {
			foreach($effects as $effect) {
				$this->assertTrue($game->effects->cardHasEffect($minion, $effect), 'Has no effect of '. $effect);
			}
		}
    }

    /**
     * @dataProvider minionsProviderWithHandEffect
     */
    public function test_minions_with_hand_effect(Minion $minion, array $effects = [], Race $race = null)
    {
        $game = Factory::createForTest();
        $game->start();

		$game->resolver->resolveSequence($game, [
			new Block\PutCardInHand($game->currentPlayer(), $minion),
		]);

		$this->assertInstanceof(get_class($minion), $game->currentPlayer()->hand->first());

		if($race) {
		    $this->assertTrue($minion->race()->is($race));
        }

		if($effects) {
			foreach($effects as $effect) {
				$this->assertTrue($game->effects->cardHasEffect($minion, $effect), 'Has no effect of '. $effect);
			}
		}
    }
	
	public function minionsProvider()
	{
		return [
			'ChillwindYeti' => [Alias\ChillwindYeti::create(), []],
			'ClassicWhelp' => [Alias\ClassicWhelp::create()],
			'BoulderfistOgre' => [Alias\BoulderfistOgre::create()],
			'Sheep' => [Alias\Sheep::create(), [], new Race(Race::BEAST)],
			'RiverCrocolisk' => [Alias\RiverCrocolisk::create(), [], new Race(Race::BEAST)],
			'Boar' => [Alias\Boar::create(), [], new Race(Race::BEAST)],
			'MurlocScout' => [Alias\MurlocScout::create(), [], new Race(Race::MURLOC)],
			'SilverHandRecruit' => [Alias\SilverHandRecruit::create()],
			'MechanicalDragonling' => [Alias\MechanicalDragonling::create(), [], new Race(Race::MECH)],
			'BloodfenRaptor' => [Alias\BloodfenRaptor::create(), [], new Race(Race::BEAST)],
			'CoreHound' => [Alias\CoreHound::create(), [], new Race(Race::BEAST)],
			'MagmaRager' => [Alias\MagmaRager::create(), [], new Race(Race::ELEMENTAL)],
			'SearingTotem' => [Alias\SearingTotem::create(), [], new Race(Race::TOTEM)],
			'WarGolem' => [Alias\WarGolem::create()],
			'MurlocRaider' => [Alias\MurlocRaider::create(), [], new Race(Race::MURLOC)],
			'OasisSnapjaw' => [Alias\OasisSnapjaw::create(), [], new Race(Race::BEAST)],
			'Hyena' => [Alias\Hyena::create(), [], new Race(Race::BEAST)],
			'Imp' => [Alias\Imp::create(), [], new Race(Race::DEMON)],
			'Chicken' => [Alias\Chicken::create(), [], new Race(Race::BEAST)],
			'DruidOfTheFang' => [Alias\DruidOfTheFang::create(), [], new Race(Race::BEAST)],
			'CapturedJormungar' => [Alias\CapturedJormungar::create(), [], new Race(Race::BEAST)],
			'Infernal' => [Alias\Infernal::create(), [], new Race(Race::DEMON)],

			'TundraRhino' => [Alias\TundraRhino::create(), [Effect\ChargeEffect::class], new Race(Race::BEAST)],
			'LeeroyJenkins' => [Alias\LeeroyJenkins::create(), [Effect\ChargeEffect::class]],
			'Boar42' => [new Card\AT_005t, [Effect\ChargeEffect::class]],
			'Hound' => [Alias\Hound::create(), [Effect\ChargeEffect::class], new Race(Race::BEAST)],
			'StormwindKnight' => [Alias\StormwindKnight::create(), [Effect\ChargeEffect::class]],
			'KorkronElite' => [Alias\KorkronElite::create(), [Effect\ChargeEffect::class]],
			'Wolfrider' => [Alias\Wolfrider::create(), [Effect\ChargeEffect::class]],
			'RecklessRocketeer' => [Alias\RecklessRocketeer::create(), [Effect\ChargeEffect::class]],
			'Huffer' => [Alias\Huffer::create(), [Effect\ChargeEffect::class], new Race(Race::BEAST)],
			'StonetuskBoar' => [Alias\StonetuskBoar::create(), [Effect\ChargeEffect::class], new Race(Race::BEAST)],
			'BluegillWarrior' => [Alias\BluegillWarrior::create(), [Effect\ChargeEffect::class], new Race(Race::MURLOC)],

			'Sunwalker' => [Alias\Sunwalker::create(), [Effect\TauntEffect::class, Effect\DivineShieldEffect::class]],
			'ArcaneNullifierX21' => [Alias\ArcaneNullifierX21::create(), [Effect\TauntEffect::class, Effect\ElusiveEffect::class]],
			'GnomereganInfantry' => [Alias\GnomereganInfantry::create(), [Effect\ChargeEffect::class, Effect\TauntEffect::class]],

			'StoneclawTotem' => [Alias\StoneclawTotem::create(), [Effect\TauntEffect::class], new Race(Race::TOTEM)],
			'SparringPartner' => [Alias\SparringPartner::create(), [Effect\TauntEffect::class]],
			'Slime' => [Alias\Slime::create(), [Effect\TauntEffect::class]],
			'TaurenWarrior' => [Alias\TaurenWarrior::create(), [Effect\TauntEffect::class]],
			'TirionFordring' => [Alias\TirionFordring::create(), [Effect\TauntEffect::class]],
			'FrostwolfGrunt' => [Alias\FrostwolfGrunt::create(), [Effect\TauntEffect::class]],
			'IronfurGrizzly' => [Alias\IronfurGrizzly::create(), [Effect\TauntEffect::class], new Race(Race::BEAST)],
			'SenjinShieldmasta' => [Alias\SenjinShieldmasta::create(), [Effect\TauntEffect::class]],
			'GoldshireFootman' => [Alias\GoldshireFootman::create(), [Effect\TauntEffect::class]],
			'IronbarkProtector' => [Alias\IronbarkProtector::create(), [Effect\TauntEffect::class]],
			'LordOfTheArena' => [Alias\LordOfTheArena::create(), [Effect\TauntEffect::class]],
			'Shieldbearer' => [Alias\Shieldbearer::create(), [Effect\TauntEffect::class]],
			'Misha' => [Alias\Misha::create(), [Effect\TauntEffect::class], new Race(Race::BEAST)],
			'BootyBayBodyguard' => [Alias\BootyBayBodyguard::create(), [Effect\TauntEffect::class]],
			'VulgarHomunculus' => [Alias\VulgarHomunculus::create(), [Effect\TauntEffect::class]],
			'SilverbackPatriarch' => [Alias\SilverbackPatriarch::create(), [Effect\TauntEffect::class], new Race(Race::BEAST)],
			'MirrorImage' => [new Card\CS2_mirror, [Effect\TauntEffect::class]],
			'Voidwalker' => [Alias\Voidwalker::create(), [Effect\TauntEffect::class], new Race(Race::DEMON)],
			'Frog' => [Alias\Frog::create(), [Effect\TauntEffect::class], new Race(Race::BEAST)],
			'FenCreeper' => [Alias\FenCreeper::create(), [Effect\TauntEffect::class]],
			'MogushanWarden' => [Alias\MogushanWarden::create(), [Effect\TauntEffect::class]],

			'ThrallmarFarseer' => [Alias\ThrallmarFarseer::create(), [Effect\WindfuryEffect::class]],

			'StubbornGastropod' => [Alias\StubbornGastropod::create(), [Effect\PoisonousEffect::class, Effect\TauntEffect::class], new Race(Race::BEAST)],

			'LowlySquire' => [Alias\LowlySquire::create(), [Effect\Inspire\LowlySquireEffect::class]],
			'ArgentSquire' => [Alias\ArgentSquire::create(), [Effect\DivineShieldEffect::class]],

			'FaerieDragon' => [Alias\FaerieDragon::create(), [Effect\ElusiveEffect::class], new Race(Race::DRAGON)],

			'WorgenInfiltrator' => [Alias\WorgenInfiltrator::create(), [Effect\StealthEffect::class]],
			'JunglePanther' => [Alias\JunglePanther::create(), [Effect\StealthEffect::class], new Race(Race::BEAST)],

			'Archmage' => [Alias\Archmage::create(), [Effect\SpellDamageEffect::class]],
			'DalaranMage' => [Alias\DalaranMage::create(), [Effect\SpellDamageEffect::class]],
			'OgreMagi' => [Alias\OgreMagi::create(), [Effect\SpellDamageEffect::class]],
			'Malygos' => [Alias\Malygos::create(), [Effect\SpellDamageEffect::class], new Race(Race::DRAGON)],
			'WrathOfAirTotem' => [Alias\WrathOfAirTotem::create(), [Effect\SpellDamageEffect::class], new Race(Race::TOTEM)],

			'FlametongueTotem' => [Alias\FlametongueTotem::create(), [Effect\AuraAttackChangerEffect::class], new Race(Race::TOTEM)],
			'RaidLeader' => [Alias\RaidLeader::create(), [Effect\AuraAttackChangerEffect::class]],
			'StormwindChampion' => [Alias\StormwindChampion::create(), [Effect\AuraAttackChangerEffect::class, Effect\AuraHealthChangerEffect::class]],

			'HealingTotem' => [Alias\HealingTotem::create(), [Effect\HealingTotemEffect::class], new Race(Race::TOTEM)],

			'RagingWorgen' => [Alias\RagingWorgen::create(), [Effect\EnragedEffect::class]],

			'SoggothTheSlitherer' => [Alias\SoggothTheSlitherer::create(), [Effect\TauntEffect::class, Effect\ElusiveEffect::class]],

			'TaintedZealot' => [Alias\TaintedZealot::create(), [Effect\DivineShieldEffect::class, Effect\SpellDamageEffect::class]],
			'KoboldGeomancer' => [Alias\KoboldGeomancer::create(), [Effect\SpellDamageEffect::class]],
			'SootSpewer' => [Alias\SootSpewer::create(), [Effect\SpellDamageEffect::class], new Race(Race::MECH)],
		];
	}

	public function minionsProviderWithHandEffect()
	{
		return [
			'GhostLightAngler' => [Alias\GhostLightAngler::create(), [Effect\EchoEffect::class], new Race(Race::MURLOC)],
		];
	}
}