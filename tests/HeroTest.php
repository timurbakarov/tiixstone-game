<?php

namespace Tiixstone\Tests;

use Tiixstone\Card\Alias\JainaProudmoore;
use Tiixstone\Factory;

class HeroTest extends \PHPUnit\Framework\TestCase
{
    public function testIsAlive()
    {
        $game = Factory::createForTest();

        $jaina = JainaProudmoore::create();
        $this->assertTrue($jaina->health->isAlive());

        $jaina->health->current->set(0);
        $this->assertFalse($jaina->health->isAlive());

        $jaina->health->current->set(-10);
        $this->assertFalse($jaina->health->isAlive());

        $jaina->health->current->set(10);
        $this->assertTrue($jaina->health->isAlive());
    }
}