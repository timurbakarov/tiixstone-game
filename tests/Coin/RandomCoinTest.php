<?php

namespace Tiixstone\Tests;

use Tiixstone\Player;
use PHPUnit\Framework\TestCase;
use Tiixstone\Coin\RandomCoin;
use Tiixstone\Card\Collection\Deck;
use Tiixstone\Card\Collection\Hand;
use Tiixstone\Card\Alias\JainaProudmoore;
use Tiixstone\Card\Collection\Board;

class RandomCoinTest extends TestCase
{
    public function testWorksCorrecly()
    {
        $player1Hero = JainaProudmoore::create();
        $player2Hero = JainaProudmoore::create();

        $player1Deck = new Deck([]);
        $player1Hand = new Hand([]);
        $player1Board = new Board([]);

        $player2Deck = new Deck([]);
        $player2Hand = new Hand([]);
        $player2Board = new Board([]);

        $player1 = new Player('Jonh Doe', $player1Hero, $player1Deck, $player1Hand, $player1Board);
        $player2 = new Player('Agent Smith', $player2Hero, $player2Deck, $player2Hand, $player2Board);

        $coin = new RandomCoin();
        $coin->toss($player1, $player2);

        $this->assertTrue($coin->winner()->id() == $player1->id() OR $coin->winner()->id() == $player2->id());
        $this->assertTrue($coin->loser()->id() == $player1->id() OR $coin->loser()->id() == $player2->id());
    }
}