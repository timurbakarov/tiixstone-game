<?php

namespace Tiixstone\Tests;

use Tiixstone\Coin;
use Tiixstone\Player;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Collection\Deck;
use Tiixstone\Card\Collection\Hand;
use Tiixstone\Card\Collection\Board;
use Tiixstone\Card\Alias\JainaProudmoore;

class PredictableCoinTest extends TestCase
{
    public function testWorksCorrecly()
    {
        $player1Hero = JainaProudmoore::create();
        $player2Hero = JainaProudmoore::create();

        $player1Deck = new Deck([]);
        $player1Hand = new Hand([]);
        $player1Board = new Board([]);

        $player2Deck = new Deck([]);
        $player2Hand = new Hand([]);
        $player2Board = new Board([]);

        $player1 = new Player('Jonh Doe', $player1Hero, $player1Deck, $player1Hand, $player1Board);
        $player2 = new Player('Agent Smith', $player2Hero, $player2Deck, $player2Hand, $player2Board);

        $coin = new Coin\PredictableCoin();
        $coin->toss($player1, $player2);

        $this->assertEquals($player1->id(), $coin->winner()->id());
        $this->assertEquals($player2->id(), $coin->loser()->id());
    }
}