<?php

namespace Tests\Tiixstone;

use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Game\Status;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase
{
    public function test_end_of_game()
    {
        $game = Factory::createForTest();
        $game->start();

        $game->resolver->resolveSequence($game, [
            new Block\MarkDestroyed($game->idlePlayer()->hero),
        ]);

        $this->assertTrue($game->status->is(Status::STATUS_OVER));
    }
}