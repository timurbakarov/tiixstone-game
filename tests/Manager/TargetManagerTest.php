<?php

namespace Tiixstone\Tests\Manager;

use Tiixstone\Block;
use Tiixstone\Factory;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\ChillwindYeti;

class TargetManagerTest extends TestCase
{
    public function testAllEnemyCharacters()
    {
        $game = Factory::createForTest();
        $game->start();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->player2, ChillwindYeti::create()),
            new Block\SummonMinion($game->player2, ChillwindYeti::create()),
        ]);

        $this->assertEquals(2, $game->player2->board->count());

        $allEnemyCharacters = $game->targetManager->allEnemyCharacters($game, $game->player1);
        $this->assertEquals(3, count($allEnemyCharacters));
    }

    public function testAllEnemyMinions()
    {
        $game = Factory::createForTest();
        $game->start();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->player2, ChillwindYeti::create()),
            new Block\SummonMinion($game->player2, ChillwindYeti::create()),
        ]);
        $this->assertEquals(2, $game->player2->board->count());

        $allEnemyMinions = $game->targetManager->allEnemyMinions($game, $game->player1);
        $this->assertEquals(2, count($allEnemyMinions));
    }

    public function testAllCharacters()
    {
        $game = Factory::createForTest();
        $game->start();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->player1, ChillwindYeti::create()),
            new Block\SummonMinion($game->player2, ChillwindYeti::create()),
            new Block\SummonMinion($game->player2, ChillwindYeti::create()),
        ]);

        $this->assertEquals(5, count($game->targetManager->allCharacters($game)));
    }
}