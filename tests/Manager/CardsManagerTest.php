<?php

namespace Tiixstone\Tests\Manager;

use Tiixstone\Factory;
use Tiixstone\Action\EndTurn;
use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias\TheCoin;

class CardsManagerTest extends TestCase
{
    public function testDraw()
    {
        $game = Factory::createForTest(Factory::createCardsArray(30), Factory::createCardsArray(30));
        $game->start();

        $this->assertEquals(4, $game->currentPlayer()->hand->count());

        $game->action(new EndTurn());
        $game->action(new EndTurn());

        $this->assertEquals(5, $game->currentPlayer()->hand->count());
    }

    public function testOverdraw()
    {
        $game = Factory::createForTest(Factory::createCardsArray(30), Factory::createCardsArray(30));
        $game->start();

        $this->assertEquals(4, $game->currentPlayer()->hand->count());

        $game->currentPlayer()->hand->appendMany([TheCoin::create(), TheCoin::create(), TheCoin::create(), TheCoin::create(), TheCoin::create(), TheCoin::create()]);
        $this->assertEquals(10, $game->currentPlayer()->hand->count());

        $game->action(new EndTurn());
        $game->action(new EndTurn());

        $this->assertEquals(10, $game->currentPlayer()->hand->count());
    }
}