<?php

namespace Tiixstone\Tests\Manager;


use Tiixstone\Cards;
use Tiixstone\Game;
use Tiixstone\Factory;
use Tiixstone\Card\Alias\Sheep;
use Tiixstone\Action\EndTurn;

class GameManagerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Game
     */
    protected $game;

    public function setUp()
    {
        $this->game = Factory::createForTest(Factory::createCardsArray(10), Factory::createCardsArray(10));
        $this->game->start();
    }

    public function testDrawingCardsAndFatigue()
    {
        $this->assertEquals(6, $this->game->player1->deck->count());
        $this->assertEquals(30, $this->game->player1->hero->health->total());

        $this->assertEquals(6, $this->game->player2->deck->count());
        $this->assertEquals(30, $this->game->player2->hero->health->total());

        (new EndTurn())->run($this->game);

        $this->assertEquals(6, $this->game->player1->deck->count());
        $this->assertEquals(30, $this->game->player1->hero->health->total());

        $this->assertEquals(5, $this->game->player2->deck->count());
        $this->assertEquals(30, $this->game->player2->hero->health->total());

        (new EndTurn())->run($this->game);
        (new EndTurn())->run($this->game);
        (new EndTurn())->run($this->game);
        (new EndTurn())->run($this->game);
        (new EndTurn())->run($this->game);
        (new EndTurn())->run($this->game);
        (new EndTurn())->run($this->game);
        (new EndTurn())->run($this->game);
        (new EndTurn())->run($this->game);
        (new EndTurn())->run($this->game);
        (new EndTurn())->run($this->game);

        $this->assertEquals(0, $this->game->player1->deck->count());
        $this->assertEquals(30, $this->game->player1->hero->health->total());

        $this->assertEquals(0, $this->game->player2->deck->count());
        $this->assertEquals(30, $this->game->player2->hero->health->total());

        (new EndTurn())->run($this->game);

        $this->assertEquals(0, $this->game->player1->deck->count());
        $this->assertEquals(30, $this->game->player1->hero->health->total());

        $this->assertEquals(0, $this->game->player2->deck->count());
        $this->assertEquals(29, $this->game->player2->hero->health->total());

        (new EndTurn())->run($this->game);

        $this->assertEquals(0, $this->game->player1->deck->count());
        $this->assertEquals(29, $this->game->player1->hero->health->total());

        $this->assertEquals(0, $this->game->player2->deck->count());
        $this->assertEquals(29, $this->game->player2->hero->health->total());

        (new EndTurn())->run($this->game);

        $this->assertEquals(0, $this->game->player1->deck->count());
        $this->assertEquals(29, $this->game->player1->hero->health->total());

        $this->assertEquals(0, $this->game->player2->deck->count());
        $this->assertEquals(27, $this->game->player2->hero->health->total());
    }

    public function testPlayerHandCardsCount()
    {
        $this->assertEquals(4, $this->game->player1->hand->count());
        $this->assertEquals(5, $this->game->player2->hand->count());
    }

    public function testCurrentPlayerChangedAfterEndTurn()
    {
        $this->assertEquals($this->game->player1, $this->game->currentPlayer());

        (new EndTurn())->run($this->game);
        $this->assertEquals($this->game->player2, $this->game->currentPlayer());

        (new EndTurn())->run($this->game);
        $this->assertEquals($this->game->player1, $this->game->currentPlayer());

        (new EndTurn())->run($this->game);
        $this->assertEquals($this->game->player2, $this->game->currentPlayer());
    }

    public function testMaximumManaIncrease()
    {
        $this->assertEquals(1, $this->game->player1->maximumMana());
        $this->assertEquals(0, $this->game->player2->maximumMana());

        $this->assertEquals(1, $this->game->player1->availableMana());
        $this->assertEquals(0, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(1, $this->game->player1->maximumMana());
        $this->assertEquals(1, $this->game->player2->maximumMana());

        $this->assertEquals(1, $this->game->player1->availableMana());
        $this->assertEquals(1, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(2, $this->game->player1->maximumMana());
        $this->assertEquals(1, $this->game->player2->maximumMana());

        $this->assertEquals(2, $this->game->player1->availableMana());
        $this->assertEquals(1, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(2, $this->game->player1->maximumMana());
        $this->assertEquals(2, $this->game->player2->maximumMana());

        $this->assertEquals(2, $this->game->player1->availableMana());
        $this->assertEquals(2, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(3, $this->game->player1->maximumMana());
        $this->assertEquals(2, $this->game->player2->maximumMana());

        $this->assertEquals(3, $this->game->player1->availableMana());
        $this->assertEquals(2, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(3, $this->game->player1->maximumMana());
        $this->assertEquals(3, $this->game->player2->maximumMana());

        $this->assertEquals(3, $this->game->player1->availableMana());
        $this->assertEquals(3, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(4, $this->game->player1->maximumMana());
        $this->assertEquals(3, $this->game->player2->maximumMana());

        $this->assertEquals(4, $this->game->player1->availableMana());
        $this->assertEquals(3, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(4, $this->game->player1->maximumMana());
        $this->assertEquals(4, $this->game->player2->maximumMana());

        $this->assertEquals(4, $this->game->player1->availableMana());
        $this->assertEquals(4, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(5, $this->game->player1->maximumMana());
        $this->assertEquals(4, $this->game->player2->maximumMana());

        $this->assertEquals(5, $this->game->player1->availableMana());
        $this->assertEquals(4, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(5, $this->game->player1->maximumMana());
        $this->assertEquals(5, $this->game->player2->maximumMana());

        $this->assertEquals(5, $this->game->player1->availableMana());
        $this->assertEquals(5, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(6, $this->game->player1->maximumMana());
        $this->assertEquals(5, $this->game->player2->maximumMana());

        $this->assertEquals(6, $this->game->player1->availableMana());
        $this->assertEquals(5, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(6, $this->game->player1->maximumMana());
        $this->assertEquals(6, $this->game->player2->maximumMana());

        $this->assertEquals(6, $this->game->player1->availableMana());
        $this->assertEquals(6, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(7, $this->game->player1->maximumMana());
        $this->assertEquals(6, $this->game->player2->maximumMana());

        $this->assertEquals(7, $this->game->player1->availableMana());
        $this->assertEquals(6, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(7, $this->game->player1->maximumMana());
        $this->assertEquals(7, $this->game->player2->maximumMana());

        $this->assertEquals(7, $this->game->player1->availableMana());
        $this->assertEquals(7, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(8, $this->game->player1->maximumMana());
        $this->assertEquals(7, $this->game->player2->maximumMana());

        $this->assertEquals(8, $this->game->player1->availableMana());
        $this->assertEquals(7, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(8, $this->game->player1->maximumMana());
        $this->assertEquals(8, $this->game->player2->maximumMana());

        $this->assertEquals(8, $this->game->player1->availableMana());
        $this->assertEquals(8, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(9, $this->game->player1->maximumMana());
        $this->assertEquals(8, $this->game->player2->maximumMana());

        $this->assertEquals(9, $this->game->player1->availableMana());
        $this->assertEquals(8, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(9, $this->game->player1->maximumMana());
        $this->assertEquals(9, $this->game->player2->maximumMana());

        $this->assertEquals(9, $this->game->player1->availableMana());
        $this->assertEquals(9, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(10, $this->game->player1->maximumMana());
        $this->assertEquals(9, $this->game->player2->maximumMana());

        $this->assertEquals(10, $this->game->player1->availableMana());
        $this->assertEquals(9, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(10, $this->game->player1->maximumMana());
        $this->assertEquals(10, $this->game->player2->maximumMana());

        $this->assertEquals(10, $this->game->player1->availableMana());
        $this->assertEquals(10, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(10, $this->game->player1->maximumMana());
        $this->assertEquals(10, $this->game->player2->maximumMana());

        $this->assertEquals(10, $this->game->player1->availableMana());
        $this->assertEquals(10, $this->game->player2->availableMana());

        (new EndTurn())->run($this->game);

        $this->assertEquals(10, $this->game->player1->maximumMana());
        $this->assertEquals(10, $this->game->player2->maximumMana());

        $this->assertEquals(10, $this->game->player1->availableMana());
        $this->assertEquals(10, $this->game->player2->availableMana());
    }
}