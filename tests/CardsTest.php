<?php

namespace Tiixstone\Tests;

use PHPUnit\Framework\TestCase;
use Tiixstone\Card\Alias;
use Tiixstone\Block;
use Tiixstone\Factory;
use Tiixstone\Action;

class CardsTest extends TestCase
{
    public function test_that_minions_creates_and_places_on_board()
    {
        $game = Factory::createForTest();
        $game->start();
		
		$direWolfAlpha = Alias\DireWolfAlpha::create();

        $game->resolver->resolveSequence($game, [
            new Block\SummonMinion($game->currentPlayer(), $direWolfAlpha),
        ]);
		
		// intentional
		$this->assertTrue(true);
    }
	
	public function test_minions_battlecry()
    {
        $game = Factory::createForTest();
        $game->start();
		
		$minionsClass = [
			Alias\SunkeeperTarim::class,
		];

		foreach($minionsClass as $minionClass) {
			$minion = $minionClass::create();
			
			$game->resolver->resolveSequence($game, [
				new Block\SetManaAvailable($game->currentPlayer(), 10),
				new Block\PutCardInHand($game->currentPlayer(), $minion),
			]);
			
			$game->action(new Action\PlayCard($minion->id()));
		}
		
		// intentional
		$this->assertTrue(true);
    }
}