<?php

namespace Tiixstone;

use Tiixstone\Game\Status;

/**
 * @package Tiixstone
 */
abstract class Action
{
    abstract public function process(Game $game);

    /**
     * @deprecated
     * @param Game $game
     * @return array
     * @throws Exception
     * @throws \Exception
     */
    public function run(Game &$game)
    {
        if(!$game->status->is(Status::STATUS_STARTED)) {
            throw new Exception("Game should be in started status");
        }

        try {
            $tempGame = clone $game;

            $blocks = $this->process($game);

            $game->resolver->resolveSequence($game, $blocks);

            return $game->logs();
        } catch (\Exception $e) {
            $game = $tempGame;

            throw $e;
        }
    }
}