<?php

namespace Tiixstone;

class Exception extends \Exception
{
    const ACTION_CAN_NOT_BE_PROCESSED_GAME_IS_OVER = 1;
    const INVALID_ACTION = 2;
    const EXCEEDED_MAX_NUMBER_CARDS_IN_HAND = 3;
    const NOT_ENOUGH_MANA = 4;
    const PLAYER_DOESNT_HAVE_CARD_IN_HAND_WITH_REQUIRED_KEY = 5;
    const PLAYER_DOESNT_HAVE_ENOUGH_MANA_TO_PLAY_CARD = 6;
    const EXCEEDED_PLACES_ON_BOARD = 7;
    const MINION_EXHAUSTED_CANT_ATTACK = 8;
    const INVALID_CARD = 9;
    const MINION_CAN_NOT_ATTACK_ZERO_ATTACK_RATE = 10;
    const SHOULD_ATTACK_MINION_WITH_TAUNT = 11;
    const MINION_CAN_NOT_ATTACK_HAS_NO_ATTACK_TARGET = 12;
    const MINION_CAN_NOT_ATTACK_IT_IS_FROZEN = 13;
    const CARD_PLAY_CONDITION_IS_NOT_FULFILLED = 14;
}