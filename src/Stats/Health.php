<?php

namespace Tiixstone\Stats;

use Tiixstone\Exception;

class Health
{
    /**
     * @var General
     */
    public $maximum;

    /**
     * @var General
     */
    public $current;

    /**
     * @var General
     */
    public $buff;

    public function __construct(int $value)
    {
        $this->maximum = new General($value);
        $this->current = new General($value);
        $this->buff = new General(0);
    }

    /**
     * @return int
     */
    public function total() : int
    {
        return $this->current->get() + $this->buff->get();
    }

    /**
     * @return int
     */
    public function maximumTotal() : int
    {
        return $this->maximum->get() + $this->buff->get();
    }

    /**
     * @param int $amount
     * @return int
     */
    public function heal(int $amount) : self
    {
        $this->current->increase($amount);

        return $this;
    }

    /**
     * @param int $amount
     * @return Health
     */
    public function damage(int $amount) : self
    {
		if($amount < 1) {
			throw new Exception("Damage can not be below or equal zero");
		}

        $this->current->decrease($amount);

        return $this;
    }

    /**
     * @return bool
     */
    public function isDead() : bool
    {
        return !$this->isAlive();
    }

    /**
     * @return bool
     */
    public function isAlive() : bool
    {
        return $this->total() > 0;
    }

    /**
     * @return bool
     */
    public function isDamaged() : bool
    {
        return $this->current->lessThan($this->maximum);
    }

    /**
     * @return bool
     */
    public function isFullHealth() : bool
    {
        return !$this->isDamaged();
    }

    /**
     * @return bool
     */
    public function isBuffed() : bool
    {
        return $this->buff->get() > 0;
    }
}