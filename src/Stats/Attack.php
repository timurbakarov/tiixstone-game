<?php

namespace Tiixstone\Stats;

class Attack
{
    /**
     * @var int
     */
    public $current;

    /**
     * @var int
     */
    public $buff;

    /**
     * @param int $value
     */
    public function __construct(int $value)
    {
        $this->current = new General($value);
        $this->buff = new General(0);
    }

    /**
     * @return int
     */
    public function total() : int
    {
        $total = $this->current->get() + $this->buff->get();

        return $total > 0 ? $total : 0;
    }

    /**
     * @return bool
     */
    public function isBuffed() : bool
    {
        return $this->buff->get() > 0;
    }

    /**
     * @return bool
     */
    public function isDebuffed() : bool
    {
        return $this->buff->get() < 0;
    }
}