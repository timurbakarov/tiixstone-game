<?php

namespace Tiixstone\Stats;

use Tiixstone\Exception;

class Armor extends General
{
    /**
     * @param int $armor
     * @return $this
     */
    public function set(int $value)
    {
        if($value < 0) {
            throw new Exception("Armor can not be below zero");
        }

        parent::set($value);

        return $this;
    }

    /**
     * @param int $value
     * @return $this
     * @throws Exception
     */
    public function decrease(int $value)
    {
        parent::decrease($value);

        return $this;
    }
}