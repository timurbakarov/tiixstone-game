<?php

namespace Tiixstone\Stats;

class General
{
    /**
     * @var int
     */
    private $value;

    public function __construct(int $default)
    {
        $this->value = $default;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function set(int $value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return int
     */
    public function get()
    {
        return $this->value;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function increase(int $value)
    {
        $this->set($this->value + $value);

        return $this;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function decrease(int $value)
    {
        $this->set($this->value - $value);

        return $this;
    }

    /**
     * @param General $stat
     * @return bool
     */
    public function greaterThan(General $stat)
    {
        return $this->get() > $stat->get();
    }

    /**
     * @param General $stat
     * @return bool
     */
    public function lessThan(General $stat)
    {
        return $this->get() < $stat->get();
    }
}