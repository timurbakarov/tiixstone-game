<?php

namespace Tiixstone\Stats;

class Durability
{
    /**
     * @var int
     */
    public $current;

    /**
     * @var int
     */
    public $buff;

    public function __construct(int $value)
    {
        $this->current = new General($value);
        $this->buff = new General(0);
    }

    /**
     * @return int
     */
    public function total() : int
    {
        return $this->current->get() + $this->buff->get();
    }

    /**
     * @return bool
     */
    public function isBuffed() : bool
    {
        return $this->buff->get() > 0;
    }

    /**
     * @return bool
     */
    public function isDebuffed() : bool
    {
        return $this->buff->get() < 0;
    }
}