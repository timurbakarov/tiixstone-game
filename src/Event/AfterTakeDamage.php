<?php

namespace Tiixstone\Event;

use Tiixstone\Card;
use Tiixstone\Event;
use Tiixstone\Card\Character;

class AfterTakeDamage extends Event
{
    /**
     * @var Character
     */
    public $character;

    /**
     * @var int
     */
    public $amount;

    /**
     * @var Card
     */
    public $source;

    public function __construct(Character $character, int $amount, Card $source = null)
    {
        $this->character = $character;
        $this->amount = $amount;
        $this->source = $source;
    }
}