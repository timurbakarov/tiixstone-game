<?php

namespace Tiixstone\Event;

use Tiixstone\Event;
use Tiixstone\Card\Minion;
use Tiixstone\Player;

class AfterSummon extends Event
{
    /**
     * @var Player
     */
    private $player;

    /**
     * @var Minion
     */
    public $minion;

    /**
     * @var Minion
     */
    public $positionMinion;

    public function __construct(Player $player = null, Minion $minion, Minion $positionMinion = null)
    {
        $this->player = $player;
        $this->minion = $minion;
        $this->positionMinion = $positionMinion;
    }

    /**
     * @return Player
     */
    public function player() : Player
    {
        return $this->player;
    }
}