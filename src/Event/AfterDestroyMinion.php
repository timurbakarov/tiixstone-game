<?php

namespace Tiixstone\Event;

use Tiixstone\Event;
use Tiixstone\Card\Minion;

class AfterDestroyMinion extends Event
{
    /**
     * @var Minion
     */
    public $minion;

    public function __construct(Minion $minion)
    {
        $this->minion = $minion;
    }
}