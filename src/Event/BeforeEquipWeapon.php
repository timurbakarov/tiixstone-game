<?php

namespace Tiixstone\Event;

use Tiixstone\Event;
use Tiixstone\Player;
use Tiixstone\Card\Weapon;

class BeforeEquipWeapon extends Event
{
    /**
     * @var Player
     */
    public $player;

    /**
     * @var Weapon
     */
    public $weapon;

    public function __construct(Player $player, Weapon $weapon)
    {
        $this->player = $player;
        $this->weapon = $weapon;
    }
}