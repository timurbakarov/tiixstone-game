<?php

namespace Tiixstone\Event;

use Tiixstone\Card;
use Tiixstone\Event;
use Tiixstone\Card\Character;

class AfterPlayCard extends Event
{
    /**
     * @var Card
     */
    public $card;

    /**
     * @var Character
     */
    public $target;

    public function __construct(Card $card, Character $target = null)
    {
        $this->card = $card;
        $this->target = $target;
    }
}