<?php

namespace Tiixstone\Event;

use Tiixstone\Card\Character;
use Tiixstone\Event;

class AfterHeal extends Event
{
    /**
     * @var Character
     */
    public $character;

    /**
     * @var int
     */
    public $amount;

    public function __construct(Character $character, int $amount)
    {
        $this->character = $character;
        $this->amount = $amount;
    }
}