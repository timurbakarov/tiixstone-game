<?php

namespace Tiixstone\Event;

use Tiixstone\Action;
use Tiixstone\Event;

class BeforeAction extends Event
{
    /**
     * @var Action
     */
    private $action;

    /**
     * @param Action $action
     */
    public function __construct(Action $action)
    {
        $this->action = $action;
    }

    /**
     * @return Action
     */
    public function action()
    {
        return $this->action;
    }
}