<?php

namespace Tiixstone\Event;

use Tiixstone\Card;
use Tiixstone\Event;

class ValidateUseHeroPower extends Event
{
    /**
     * @var Card\Power
     */
    public $heroPower;
    /**
     * @var Card\Character
     */
    public $target;

    public function __construct(Card\Power $heroPower, Card\Character $target = null)
    {
        $this->heroPower = $heroPower;
        $this->target = $target;
    }
}