<?php

namespace Tiixstone\Event;

use Tiixstone\Card\Power;
use Tiixstone\Event;

class BeforeUseHeroPower extends Event
{
    /**
     * @var Power
     */
    public $heroPower;

    public function __construct(Power $heroPower)
    {
        $this->heroPower = $heroPower;
    }
}