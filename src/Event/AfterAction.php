<?php

namespace Tiixstone\Event;

use Tiixstone\Action;
use Tiixstone\Event;

class AfterAction extends Event
{
    /**
     * @var Action
     */
    private $action;

    /**
     * @param Action $action
     */
    public function __construct(Action $action)
    {
        $this->action = $action;
    }

    /**
     * @return Action
     */
    public function action(): Action
    {
        return $this->action;
    }
}