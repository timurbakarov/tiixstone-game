<?php

namespace Tiixstone\Event;

use Tiixstone\Card;
use Tiixstone\Event;

class ValidatePlayCard extends Event
{
    /**
     * @var Card
     */
    public $card;
    /**
     * @var Card\Minion
     */
    public $positionMinion;
    /**
     * @var Card\Character
     */
    public $target;

    public function __construct(Card $card, Card\Minion $positionMinion = null, Card\Character $target = null)
    {
        $this->card = $card;
        $this->positionMinion = $positionMinion;
        $this->target = $target;
    }
}