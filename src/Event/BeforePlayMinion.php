<?php

namespace Tiixstone\Event;

use Tiixstone\Card\Character;
use Tiixstone\Card\Minion;
use Tiixstone\Event;

class BeforePlayMinion extends Event
{
    /**
     * @var Minion
     */
    public $minion;

    /**
     * @var Minion
     */
    public $positionMinion;

    /**
     * @var Character
     */
    public $target;

    public function __construct(Minion $minion, Minion $positionMinion = null, Character $target = null)
    {
        $this->minion = $minion;
        $this->positionMinion = $positionMinion;
        $this->target = $target;
    }
}