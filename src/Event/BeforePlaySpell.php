<?php

namespace Tiixstone\Event;

use Tiixstone\Card\Character;
use Tiixstone\Card\Spell;
use Tiixstone\Event;

class BeforePlaySpell extends Event
{
    /**
     * @var Spell
     */
    public $spell;

    /**
     * @var Character
     */
    public $target;

    public function __construct(Spell $spell, Character $target = null)
    {
        $this->spell = $spell;
        $this->target = $target;
    }
}