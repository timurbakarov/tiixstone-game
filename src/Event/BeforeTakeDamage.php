<?php

namespace Tiixstone\Event;

use Tiixstone\Event;
use Tiixstone\Card\Character;

class BeforeTakeDamage extends Event
{
    /**
     * @var Character
     */
    public $character;

    /**
     * @var int
     */
    public $amount;

    public function __construct(Character $character, int $amount)
    {
        $this->character = $character;
        $this->amount = $amount;
    }
}