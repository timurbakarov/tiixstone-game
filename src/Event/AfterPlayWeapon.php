<?php

namespace Tiixstone\Event;

use Tiixstone\Card\Weapon;
use Tiixstone\Event;

class AfterPlayWeapon extends Event
{
    /**
     * @var Weapon
     */
    public $weapon;

    public function __construct(Weapon $weapon)
    {
        $this->weapon = $weapon;
    }
}