<?php

namespace Tiixstone\Event;

use Tiixstone\Event;
use Tiixstone\Card\Power;

class AfterUseHeroPower extends Event
{
    /**
     * @var Power
     */
    public $heroPower;

    public function __construct(Power $heroPower)
    {
        $this->heroPower = $heroPower;
    }
}