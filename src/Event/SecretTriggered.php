<?php

namespace Tiixstone\Event;

use Tiixstone\Card\Secret;
use Tiixstone\Event;

class SecretTriggered extends Event
{
    /**
     * @var Secret
     */
    public $secret;

    public function __construct(Secret $secret)
    {
        $this->secret = $secret;
    }
}