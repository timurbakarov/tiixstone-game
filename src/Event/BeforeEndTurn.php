<?php

namespace Tiixstone\Event;

use Tiixstone\Event;
use Tiixstone\Player;

class BeforeEndTurn extends Event
{
    /**
     * @var Player
     */
    public $player;

    public function __construct(Player $player)
    {
        $this->player = $player;
    }
}