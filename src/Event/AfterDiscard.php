<?php

namespace Tiixstone\Event;

use Tiixstone\Card;
use Tiixstone\Event;

class AfterDiscard extends Event
{
    /**
     * @var Card
     */
    private $card;

    public function __construct(Card $card)
    {
        $this->card = $card;
    }

    /**
     * @return Card
     */
    public function card() : Card
    {
        return $this->card;
    }
}