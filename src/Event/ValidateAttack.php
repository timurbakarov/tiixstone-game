<?php

namespace Tiixstone\Event;

use Tiixstone\Card;
use Tiixstone\Event;

class ValidateAttack extends Event
{
    /**
     * @var Card\Character
     */
    public $attacker;
    /**
     * @var Card\Character
     */
    public $defender;

    public function __construct(Card\Character $attacker, Card\Character $defender)
    {
        $this->attacker = $attacker;
        $this->defender = $defender;
    }
}