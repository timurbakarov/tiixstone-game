<?php

namespace Tiixstone\Event;

use Tiixstone\Event;
use Tiixstone\Player;

class TurnStarted extends Event
{
    /**
     * @var Player
     */
    public $player;

    public function __construct(Player $player)
    {
        $this->player = $player;
    }
}