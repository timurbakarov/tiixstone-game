<?php

namespace Tiixstone\Event;

use Tiixstone\Event;
use Tiixstone\Card\Character;

class AfterAttack extends Event
{
    /**
     * @var Character
     */
    public $attacker;

    /**
     * @var Character
     */
    public $defender;

    public function __construct(Character $attacker, Character $defender)
    {
        $this->attacker = $attacker;
        $this->defender = $defender;
    }
}