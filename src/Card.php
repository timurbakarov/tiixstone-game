<?php declare(strict_types=1);

namespace Tiixstone;

use Tiixstone\Card\Minion;
use Tiixstone\Card\Character;
use Tiixstone\Condition\None;
use Tiixstone\Stats\ManaCost;

abstract class Card implements Identifiable
{
    use Identifier;

    /**
     * @var ManaCost
     */
    public $cost;

    /**
     * @var Player
     */
    private $player;

    public function __construct()
    {
        $this->id = $this->generateId();
        $this->cost = new ManaCost($this->defaultCost());
    }

    abstract public function globalId() : string;

    /**
     * @return int
     */
    abstract public function defaultCost() : int;

    /**
     * @param Game $game
     * @param Minion|null $positionMinion
     * @param Character|null $target
     * @return array
     */
    abstract public function play(Game $game, Minion $positionMinion = null, Character $target = null) : array;

    /**
     * @return string
     */
    private function generateId()
    {
        return Game::uuid();
    }

    /**
     * @param Player $player
     * @return $this
     */
    public function setPlayer(Player $player)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * @return Player
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * @return array
     */
    public function defaultEffects()
    {
        return [];
    }

    /**
     * @return bool
     */
    public function isSpell() : bool
    {
        return $this instanceof Card\Spell;
    }

    /**
     * @return bool
     */
    public function isMinion() : bool
    {
        return $this instanceof Card\Minion;
    }

    /**
     * @return bool
     */
    public function isWeapon() : bool
    {
        return $this instanceof Card\Weapon;
    }

    /**
     * @return bool
     */
    public function isHeroPower() : bool
    {
        return $this instanceof Card\Power;
    }

    /**
     * @return bool
     */
    public function isHero() : bool
    {
        return $this instanceof Card\Hero;
    }

    /**
     * @return PlayCardCondition
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new None(),
        ]);
    }

    /**
     * @param Card $card
     * @return bool
     */
    public function isSelf(Card $card)
    {
        return $this->id() == $card->id();
    }

    /**
     * @return array
     */
    public function handEffects()
    {
        return [];
    }

    /**
     * @return Card
     */
    public function copy()
    {
        $copyClass = get_class($this);
		
        return new $copyClass;
    }
}