<?php

namespace Tiixstone;

class Race
{
    const GENERAL = 1;
    const BEAST = 2;
    const TOTEM = 3;
    const MURLOC = 4;
    const DEMON = 5;
    const MECH = 6;
    const ELEMENTAL = 7;
    const DRAGON = 8;

    /**
     * @var int
     */
    private $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

    /**
     * @return Race
     */
    public static function demon()
    {
        return new self(self::DEMON);
    }

    /**
     * @return bool
     */
    public function isBeast()
    {
        return $this->value == self::BEAST;
    }

    /**
     * @return bool
     */
    public function isTotem()
    {
        return $this->value == self::TOTEM;
    }

    /**
     * @return bool
     */
    public function isDemon()
    {
        return $this->value == self::DEMON;
    }

    /**
     * @return bool
     */
    public function isMurloc()
    {
        return $this->value == self::MURLOC;
    }

    /**
     * @return bool
     */
    public function isMech()
    {
        return $this->value == self::MECH;
    }

    /**
     * @return bool
     */
    public function isElemental()
    {
        return $this->value == self::ELEMENTAL;
    }

    /**
     * @return bool
     */
    public function isDragon()
    {
        return $this->value == self::DRAGON;
    }

    /**
     * @return int
     */
    public function value()
    {
        return $this->value;
    }

    /**
     * @return Race
     */
    public static function beast()
    {
        return new self(self::BEAST);
    }

    /**
     * @return Race
     */
    public static function murloc()
    {
        return new self(self::MURLOC);
    }

    /**
     * @param Race $race
     * @return bool
     */
    public function is(Race $race)
    {
        return $this->value == $race->value;
    }

    /**
     * @return mixed
     */
    public function label()
    {
        $labels = [
            self::GENERAL => '',
            self::BEAST => 'Beast',
            self::TOTEM => 'Totem',
            self::MURLOC => 'Murloc',
            self::DEMON => 'Demon',
            self::MECH => 'Mech',
            self::ELEMENTAL => 'Elemental',
            self::DRAGON => 'Dragon',
        ];

        return $labels[$this->value];
    }
}