<?php

namespace Tiixstone\Card;

/**
 * Akali's Horn
 */
class TRLA_171 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_171';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}