<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Hidden Wisdom
 */
class GIL_903 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_903';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}