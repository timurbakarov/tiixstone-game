<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Twisted Worgen
 */
class OG_247 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_247';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}