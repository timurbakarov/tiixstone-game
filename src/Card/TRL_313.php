<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Scorch
 */
class TRL_313 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_313';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}