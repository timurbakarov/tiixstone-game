<?php

namespace Tiixstone\Card;

/**
 * Wyrmguard
 */
class GIL_526 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_526';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 11;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}