<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Kil'rek
 */
class KARA_09_08 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KARA_09_08';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}