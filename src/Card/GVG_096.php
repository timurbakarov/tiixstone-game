<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Piloted Shredder
 */
class GVG_096 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_096';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}