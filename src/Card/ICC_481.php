<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Thrall, Deathseer
 */
class ICC_481 extends Hero
{
    public function globalId() : string
    {
        return 'ICC_481';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}