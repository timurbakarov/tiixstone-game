<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Magmaw
 */
class BRMA14_12 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA14_12';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 10;
    }
}