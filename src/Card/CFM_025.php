<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Wind-up Burglebot
 */
class CFM_025 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_025';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}