<?php

namespace Tiixstone\Card;

/**
 * Booty Bay Bookie
 */
class TRL_504 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_504';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}