<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Lyra the Sunshard
 */
class UNG_963 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_963';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}