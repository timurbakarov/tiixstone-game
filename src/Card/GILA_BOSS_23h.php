<?php

namespace Tiixstone\Card;

/**
 * Grubb the Swampdrinker
 */
class GILA_BOSS_23h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_23h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}