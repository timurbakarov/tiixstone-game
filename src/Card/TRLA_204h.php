<?php

namespace Tiixstone\Card;

/**
 * Zul'jin
 */
class TRLA_204h extends Hero
{
    public function globalId() : string
    {
        return 'TRLA_204h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}