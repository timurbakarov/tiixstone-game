<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Lost Secrets
 */
class GILA_Toki_09 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_Toki_09';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}