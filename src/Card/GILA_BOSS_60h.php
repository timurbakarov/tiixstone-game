<?php

namespace Tiixstone\Card;

/**
 * Captain Shivers
 */
class GILA_BOSS_60h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_60h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}