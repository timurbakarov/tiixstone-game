<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Wispering Woods
 */
class GIL_553 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_553';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}