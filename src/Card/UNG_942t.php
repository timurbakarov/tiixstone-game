<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Megafin
 */
class UNG_942t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_942t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}