<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;
use Tiixstone\Card\Alias\Reinforce;

/**
 * Uther Lightbringer
 */
class HERO_04 extends Hero
{
    public function globalId() : string
    {
        return 'HERO_04';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return Reinforce::create();
    }
}