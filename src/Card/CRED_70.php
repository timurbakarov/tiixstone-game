<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Monique Ory
 */
class CRED_70 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_70';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}