<?php

namespace Tiixstone\Card;

/**
 * Wisp
 */
class GIL_553t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_553t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}