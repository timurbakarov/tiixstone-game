<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Master of Evolution
 */
class OG_328 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_328';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}