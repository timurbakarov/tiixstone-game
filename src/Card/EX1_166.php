<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Keeper of the Grove
 */
class EX1_166 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_166';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}