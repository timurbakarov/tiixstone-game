<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Lorewalker Cho
 */
class EX1_100 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_100';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}