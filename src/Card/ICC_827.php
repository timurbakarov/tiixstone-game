<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Valeera the Hollow
 */
class ICC_827 extends Hero
{
    public function globalId() : string
    {
        return 'ICC_827';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}