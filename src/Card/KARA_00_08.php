<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Archmage's Apprentice
 */
class KARA_00_08 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KARA_00_08';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}