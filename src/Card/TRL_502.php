<?php

namespace Tiixstone\Card;

/**
 * Spirit of the Dead
 */
class TRL_502 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_502';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}