<?php

namespace Tiixstone\Card;

/**
 * Brushwood Centurion
 */
class GILA_BOSS_41h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_41h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}