<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Gentleman's Top Hat
 */
class GILA_825 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_825';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}