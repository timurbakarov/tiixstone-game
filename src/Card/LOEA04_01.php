<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Temple Escape
 */
class LOEA04_01 extends Hero
{
    public function globalId() : string
    {
        return 'LOEA04_01';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}