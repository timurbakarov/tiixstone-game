<?php

namespace Tiixstone\Card;

/**
 * Blunderbuss
 */
class GILA_500t3 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_500t3';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}