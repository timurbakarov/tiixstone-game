<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Fight Promoter
 */
class CFM_328 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_328';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}