<?php

namespace Tiixstone\Card;

/**
 * Daring Fire-Eater
 */
class TRL_390 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_390';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}