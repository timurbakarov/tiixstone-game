<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Partytown Stormwind
 */
class TB_SPT_MTH_Boss2 extends Hero
{
    public function globalId() : string
    {
        return 'TB_SPT_MTH_Boss2';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}