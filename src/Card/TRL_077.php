<?php

namespace Tiixstone\Card;

/**
 * Gurubashi Hypemon
 */
class TRL_077 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_077';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}