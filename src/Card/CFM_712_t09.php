<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jade Golem
 */
class CFM_712_t09 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_712_t09';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 9;
    }
}