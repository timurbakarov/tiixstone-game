<?php

namespace Tiixstone\Card;

/**
 * Halazzi's Hunt
 */
class TRLA_162 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_162';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}