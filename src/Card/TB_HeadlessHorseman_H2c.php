<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Stormwind Investigator Pirate
 */
class TB_HeadlessHorseman_H2c extends Hero
{
    public function globalId() : string
    {
        return 'TB_HeadlessHorseman_H2c';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}