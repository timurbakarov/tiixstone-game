<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mechanical Parrot
 */
class LOEA07_25 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA07_25';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}