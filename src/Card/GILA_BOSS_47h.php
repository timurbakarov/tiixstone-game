<?php

namespace Tiixstone\Card;

/**
 * Shudderwock
 */
class GILA_BOSS_47h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_47h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}