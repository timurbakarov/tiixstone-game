<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Festergut
 */
class ICCA07_008 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA07_008';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}