<?php

namespace Tiixstone\Card;

/**
 * Wharrgarbl
 */
class GILA_BOSS_37h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_37h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}