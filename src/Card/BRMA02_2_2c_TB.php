<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * Jeering Crowd
 */
class BRMA02_2_2c_TB extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA02_2_2c_TB';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}