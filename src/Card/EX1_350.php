<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Prophet Velen
 */
class EX1_350 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_350';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}