<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Grave Vengeance
 */
class ICC_829t extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_829t';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 8;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}