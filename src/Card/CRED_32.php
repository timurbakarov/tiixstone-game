<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jerry Mascho
 */
class CRED_32 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_32';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}