<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Virmen Sensei
 */
class CFM_816 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_816';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}