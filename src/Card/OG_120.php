<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Anomalus
 */
class OG_120 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_120';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}