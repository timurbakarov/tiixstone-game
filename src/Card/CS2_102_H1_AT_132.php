<?php

namespace Tiixstone\Card;

use Tiixstone\Block\IncreaseArmor;
use Tiixstone\Game;

/**
 * Tank Up!
 */
class CS2_102_H1_AT_132 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'CS2_102_H1_AT_132';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [new IncreaseArmor($this->getPlayer()->hero, 4)];
    }
}