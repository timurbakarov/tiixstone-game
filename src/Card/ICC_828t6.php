<?php

namespace Tiixstone\Card;

/**
 * Vilebrood Skitterer
 */
class ICC_828t6 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_828t6';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}