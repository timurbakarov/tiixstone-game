<?php

namespace Tiixstone\Card;

/**
 * Cathedral Gargoyle
 */
class GIL_635 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_635';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}