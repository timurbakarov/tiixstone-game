<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Medivh
 */
class KARA_00_03 extends Hero
{
    public function globalId() : string
    {
        return 'KARA_00_03';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}