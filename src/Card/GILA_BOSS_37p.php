<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * It's Raining Fin
 */
class GILA_BOSS_37p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_37p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}