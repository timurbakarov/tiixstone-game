<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Book Wyrm
 */
class KAR_033 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_033';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}