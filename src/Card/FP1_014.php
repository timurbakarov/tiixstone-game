<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Stalagg
 */
class FP1_014 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_014';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}