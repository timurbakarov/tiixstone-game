<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Auctionmaster Beardo
 */
class CFM_807 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_807';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}