<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Time Out!
 */
class TRL_302 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_302';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}