<?php

namespace Tiixstone\Card;

use Tiixstone\Effect;

/**
 * Tainted Zealot
 */
class ICC_913 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_913';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     * @throws \Tiixstone\Exception
     */
    public function boardEffects()
    {
        return [Effect\DivineShieldEffect::class, new Effect\SpellDamageEffect(1)];
    }
}