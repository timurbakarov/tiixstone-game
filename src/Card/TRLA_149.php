<?php

namespace Tiixstone\Card;

/**
 * Ghastcoiler
 */
class TRLA_149 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_149';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}