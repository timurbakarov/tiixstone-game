<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Bloodworm
 */
class ICC_905 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_905';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}