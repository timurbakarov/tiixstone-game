<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Malorne
 */
class GVG_035 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_035';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 9;
    }
}