<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\FallenHeroEffect;

/**
 * Fallen Hero
 */
class AT_003 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_003';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [FallenHeroEffect::class];
    }
}