<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Nefarian
 */
class BRMA17_2 extends Hero
{
    public function globalId() : string
    {
        return 'BRMA17_2';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}