<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Poisoned Drink
 */
class GILA_BOSS_45p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_45p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}