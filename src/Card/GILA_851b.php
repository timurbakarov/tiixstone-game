<?php

namespace Tiixstone\Card;

/**
 * Transforming Tracker
 */
class GILA_851b extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_851b';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}