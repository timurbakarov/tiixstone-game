<?php

namespace Tiixstone\Card;

/**
 * Bwonsamdi's Sanctum
 */
class TRLA_114 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_114';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}