<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Rod of the Sun
 */
class LOEA01_11 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA01_11';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}