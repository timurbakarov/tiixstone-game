<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Stormforged Axe
 */
class EX1_247 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_247';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}