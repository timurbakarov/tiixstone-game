<?php

namespace Tiixstone\Card;

/**
 * Shirvallah, the Tiger
 */
class TRL_300 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_300';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 25;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}