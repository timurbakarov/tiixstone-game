<?php

namespace Tiixstone\Card;

/**
 * Emeriss
 */
class GIL_128 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_128';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}