<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Romulo
 */
class KARA_13_23 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KARA_13_23';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}