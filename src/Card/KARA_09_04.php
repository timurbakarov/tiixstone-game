<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Dark Pact
 */
class KARA_09_04 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'KARA_09_04';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}