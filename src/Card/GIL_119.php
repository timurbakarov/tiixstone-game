<?php

namespace Tiixstone\Card;

/**
 * Cauldron Elemental
 */
class GIL_119 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_119';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}