<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Murloc Razorgill
 */
class CFM_310t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_310t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}