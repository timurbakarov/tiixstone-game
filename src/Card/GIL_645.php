<?php

namespace Tiixstone\Card;

/**
 * Bonfire Elemental
 */
class GIL_645 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_645';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}