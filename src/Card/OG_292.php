<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Forlorn Stalker
 */
class OG_292 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_292';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}