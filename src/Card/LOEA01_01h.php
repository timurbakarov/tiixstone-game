<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Sun Raider Phaerix
 */
class LOEA01_01h extends Hero
{
    public function globalId() : string
    {
        return 'LOEA01_01h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}