<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Winter's Veil Gift
 */
class TB_GiftExchange_Treasure extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_GiftExchange_Treasure';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}