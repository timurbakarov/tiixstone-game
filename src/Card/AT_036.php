<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Anub'arak
 */
class AT_036 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_036';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}