<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Wish for Companionship
 */
class LOEA02_10 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA02_10';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}