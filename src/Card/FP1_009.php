<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Deathlord
 */
class FP1_009 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_009';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}