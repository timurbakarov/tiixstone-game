<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect;

/**
 * Timber Wolf
 */
class DS1_175 extends Minion
{
    protected $race = Race::BEAST;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'DS1_175';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [
            new Effect\AuraAttackChangerEffect(
                1,
                new Effect\Target\Aggregate([
                    new Effect\Target\Friendly,
                    new Effect\Target\Race(Race::beast()),
                    new Effect\Target\NotSelfTarget,
                ])
            ),
        ];
    }
}