<?php

namespace Tiixstone\Card;

abstract class MageSecret extends Secret
{
    /**
     * @return int
     */
    public final function defaultCost(): int
    {
        return 3;
    }
}