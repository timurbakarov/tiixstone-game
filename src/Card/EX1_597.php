<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Imp Master
 */
class EX1_597 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_597';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}