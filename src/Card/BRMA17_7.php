<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Chromatic Prototype
 */
class BRMA17_7 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA17_7';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}