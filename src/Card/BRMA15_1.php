<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Maloriak
 */
class BRMA15_1 extends Hero
{
    public function globalId() : string
    {
        return 'BRMA15_1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}