<?php

namespace Tiixstone\Card;

/**
 * Cragtorr
 */
class GILA_BOSS_54h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_54h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}