<?php

namespace Tiixstone\Card;

/**
 * Ragnaros the Firelord
 */
class TRL_316t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_316t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}