<?php

namespace Tiixstone\Card;

/**
 * Stuffed Sack
 */
class GILA_BOSS_26t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_BOSS_26t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}