<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tournament Attendee
 */
class AT_097 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_097';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}