<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\Trigger\FlamewakerEffect;

/**
 * Flamewaker
 */
class BRM_002 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_002';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [FlamewakerEffect::class];
    }
}