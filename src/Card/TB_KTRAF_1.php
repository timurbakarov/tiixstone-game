<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Anub'Rekhan
 */
class TB_KTRAF_1 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_KTRAF_1';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}