<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Steady Throw
 */
class TRLA_065p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TRLA_065p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}