<?php

namespace Tiixstone\Card;

/**
 * Former Champ
 */
class TRL_151 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_151';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}