<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect;

/**
 * Tundra Rhino
 */
class DS1_178 extends Minion
{
    protected $race = Race::BEAST;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'DS1_178';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [new Effect\ChargeEffect(new Effect\Target\Aggregate([
            new Effect\Target\Battlefield,
            new Effect\Target\Minion,
            new Effect\Target\Friendly,
            new Effect\Target\Race(new Race(Race::BEAST)),
        ]))];
    }
}