<?php

namespace Tiixstone\Card;

/**
 * High Priest Thekal
 */
class TRL_308 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_308';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}