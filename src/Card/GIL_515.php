<?php

namespace Tiixstone\Card;

/**
 * Ratcatcher
 */
class GIL_515 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_515';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}