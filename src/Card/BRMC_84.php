<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dragonkin Spellcaster
 */
class BRMC_84 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMC_84';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}