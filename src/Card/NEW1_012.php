<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mana Wyrm
 */
class NEW1_012 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_012';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}