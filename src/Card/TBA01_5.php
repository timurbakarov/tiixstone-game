<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Wild Magic
 */
class TBA01_5 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TBA01_5';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}