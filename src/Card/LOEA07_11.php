<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Debris
 */
class LOEA07_11 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA07_11';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}