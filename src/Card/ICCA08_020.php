<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Frostmourne
 */
class ICCA08_020 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA08_020';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 7;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}