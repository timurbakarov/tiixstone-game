<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Salty Dog
 */
class GVG_070 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_070';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}