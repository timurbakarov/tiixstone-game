<?php

namespace Tiixstone\Card;

use Tiixstone\Block\PutCardInHand;
use Tiixstone\Condition;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Convert
 */
class AT_015 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_015';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new PutCardInHand($game->currentPlayer(), $target->copy())];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Enemy,
            new Condition\Target\Minion,
        ]);
    }
}