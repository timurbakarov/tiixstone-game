<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Sea Reaver
 */
class AT_130 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_130';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}