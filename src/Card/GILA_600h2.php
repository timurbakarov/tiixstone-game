<?php

namespace Tiixstone\Card;

/**
 * Darius Crowley
 */
class GILA_600h2 extends Hero
{
    public function globalId() : string
    {
        return 'GILA_600h2';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}