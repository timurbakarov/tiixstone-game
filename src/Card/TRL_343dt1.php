<?php

namespace Tiixstone\Card;

/**
 * Ravasaur
 */
class TRL_343dt1 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_343dt1';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}