<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect;
use Tiixstone\Condition;

/**
 * Ragnaros the Firelord
 */
class EX1_298 extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::ELEMENTAL;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_298';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [
            new Effect\AttackConditionChangerEffect(
                [new Condition\Initiator\CanNotAttack],
                new Effect\Target\SelfTarget
            ),
            new Effect\Trigger\RagnarosEffect(),
        ];
    }
}