<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\TauntEffect;

/**
 * Booty Bay Bodyguard
 */
class CS2_187 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_187';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [TauntEffect::class];
    }
}