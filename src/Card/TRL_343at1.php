<?php

namespace Tiixstone\Card;

/**
 * Ankylodon
 */
class TRL_343at1 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_343at1';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}