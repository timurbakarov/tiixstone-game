<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tabbycat
 */
class CFM_315t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_315t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}