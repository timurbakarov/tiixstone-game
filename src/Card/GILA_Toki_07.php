<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Elementalist
 */
class GILA_Toki_07 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_Toki_07';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}