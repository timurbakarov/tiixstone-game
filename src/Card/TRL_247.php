<?php

namespace Tiixstone\Card;

/**
 * Soulwarden
 */
class TRL_247 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_247';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}