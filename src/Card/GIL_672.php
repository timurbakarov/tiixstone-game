<?php

namespace Tiixstone\Card;

/**
 * Spectral Cutlass
 */
class GIL_672 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_672';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}