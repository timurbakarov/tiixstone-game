<?php

namespace Tiixstone\Card;

/**
 * Shirvallah's Vengeance
 */
class TRLA_137t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_137t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}