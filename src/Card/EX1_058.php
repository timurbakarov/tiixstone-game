<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Battlecryable;
use Tiixstone\Block\GiveEffect;

/**
 * Sunfury Protector
 */
class EX1_058 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_058';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        $adjacentMinions = $game->targetManager->adjacentMinions($game, $attackable);

        $blocks = [];

        foreach($adjacentMinions as $minion) {
            if(!$minion) {
                continue;
            }

            $blocks[] = new GiveEffect($minion, Effect\TauntEffect::class);
        }

        return $blocks;
    }
}