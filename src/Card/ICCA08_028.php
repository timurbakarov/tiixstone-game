<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * The Price of Power
 */
class ICCA08_028 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA08_028';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}