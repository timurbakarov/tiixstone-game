<?php

namespace Tiixstone\Card;

/**
 * Niira the Trickster
 */
class GILA_BOSS_32h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_32h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}