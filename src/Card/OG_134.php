<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Yogg-Saron, Hope's End
 */
class OG_134 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_134';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}