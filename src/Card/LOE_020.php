<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Desert Camel
 */
class LOE_020 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_020';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}