<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Berserker Throw
 */
class TRL_065h extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TRL_065h';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}