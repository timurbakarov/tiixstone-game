<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Charged Hammer
 */
class AT_050 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_050';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}