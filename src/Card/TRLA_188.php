<?php

namespace Tiixstone\Card;

/**
 * Pesky Rascal
 */
class TRLA_188 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_188';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}