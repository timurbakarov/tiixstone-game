<?php

namespace Tiixstone\Card;

/**
 * Gonk's Armament
 */
class TRLA_112 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_112';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}