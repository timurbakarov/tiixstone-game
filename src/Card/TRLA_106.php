<?php

namespace Tiixstone\Card;

/**
 * Bottled Terror
 */
class TRLA_106 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_106';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}