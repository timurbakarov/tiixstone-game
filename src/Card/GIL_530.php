<?php

namespace Tiixstone\Card;

/**
 * Murkspark Eel
 */
class GIL_530 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_530';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}