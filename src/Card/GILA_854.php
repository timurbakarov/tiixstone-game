<?php

namespace Tiixstone\Card;

/**
 * Beastly Beauty
 */
class GILA_854 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_854';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}