<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Terestian Illhoof
 */
class KARA_09_01 extends Hero
{
    public function globalId() : string
    {
        return 'KARA_09_01';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}