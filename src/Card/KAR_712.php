<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Violet Illusionist
 */
class KAR_712 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_712';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}