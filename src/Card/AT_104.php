<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tuskarr Jouster
 */
class AT_104 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_104';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}