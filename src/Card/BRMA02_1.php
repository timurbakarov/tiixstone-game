<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * High Justice Grimstone
 */
class BRMA02_1 extends Hero
{
    public function globalId() : string
    {
        return 'BRMA02_1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}