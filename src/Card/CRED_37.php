<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ricardo Robaina
 */
class CRED_37 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_37';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}