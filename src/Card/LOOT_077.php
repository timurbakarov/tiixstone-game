<?php

namespace Tiixstone\Card;

use Tiixstone\Block\SummonMinion;
use Tiixstone\Block\TakeDamage;
use Tiixstone\Card\Alias\Wolf;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Flanking Strike
 */
class LOOT_077 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOOT_077';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new TakeDamage($target, $game->withSpellDamage(3), $this),
            new SummonMinion($target->getPlayer(), Wolf::create()),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion(),
        ]);
    }
}