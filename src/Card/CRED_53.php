<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Camille Sanford
 */
class CRED_53 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_53';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 14;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}