<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Condition;
use Tiixstone\Block\GiveEffect;
use Tiixstone\PlayCardCondition;
use Tiixstone\Manager\TargetManager;

/**
 * Windfury
 */
class CS2_039 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_039';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null): array
    {
        $game->logBlock('windfury-spell');

        return [new GiveEffect($target, Effect\WindfuryEffect::class)];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Minion,
        ]);
    }
}