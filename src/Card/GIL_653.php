<?php

namespace Tiixstone\Card;

/**
 * Woodcutter's Axe
 */
class GIL_653 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_653';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}