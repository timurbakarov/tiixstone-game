<?php

namespace Tiixstone\Card;

/**
 * Rabid Worgen
 */
class GIL_113 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_113';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}