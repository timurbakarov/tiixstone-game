<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Cellar Spider
 */
class KAR_030 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_030';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}