<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ice Walker
 */
class ICC_068 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_068';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}