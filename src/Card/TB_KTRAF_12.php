<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Patchwerk
 */
class TB_KTRAF_12 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_KTRAF_12';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}