<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Chasing Trogg
 */
class LOEA07_09 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA07_09';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}