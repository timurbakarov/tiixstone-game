<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Grook Fu Master
 */
class CFM_666 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_666';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}