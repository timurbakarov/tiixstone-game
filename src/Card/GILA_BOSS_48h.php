<?php

namespace Tiixstone\Card;

/**
 * Infinite Toki
 */
class GILA_BOSS_48h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_48h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}