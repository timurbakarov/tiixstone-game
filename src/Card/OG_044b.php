<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Druid of the Flame
 */
class OG_044b extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_044b';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}