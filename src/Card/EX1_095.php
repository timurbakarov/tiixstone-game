<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\Trigger\GadgetzanAuctioneerEffect;

/**
 * Gadgetzan Auctioneer
 */
class EX1_095 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_095';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [GadgetzanAuctioneerEffect::class];
    }
}