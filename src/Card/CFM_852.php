<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Lotus Agents
 */
class CFM_852 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_852';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}