<?php

namespace Tiixstone\Card;

/**
 * Inquisitor Hav'nixx
 */
class GILA_BOSS_34h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_34h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}