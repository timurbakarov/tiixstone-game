<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Staff of Origination
 */
class LOEA16_2H extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'LOEA16_2H';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}