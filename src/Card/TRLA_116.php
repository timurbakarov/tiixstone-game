<?php

namespace Tiixstone\Card;

/**
 * Bonds of Balance
 */
class TRLA_116 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_116';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}