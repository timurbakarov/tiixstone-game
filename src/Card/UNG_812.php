<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Sabretooth Stalker
 */
class UNG_812 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_812';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}