<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * C'Thun
 */
class OG_279 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_279';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}