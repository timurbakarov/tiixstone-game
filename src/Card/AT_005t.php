<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect\ChargeEffect;

/**
 * Boar
 */
class AT_005t extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::BEAST;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_005t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [ChargeEffect::class];
    }
}