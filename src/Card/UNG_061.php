<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Obsidian Shard
 */
class UNG_061 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_061';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}