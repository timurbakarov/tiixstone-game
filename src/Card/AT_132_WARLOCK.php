<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Soul Tap
 */
class AT_132_WARLOCK extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'AT_132_WARLOCK';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}