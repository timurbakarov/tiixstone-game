<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Steve Walker
 */
class CRED_75 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_75';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}