<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Nexus-Champion Saraad
 */
class AT_127 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_127';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}