<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Revenge of the Wild
 */
class TRL_566 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_566';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}