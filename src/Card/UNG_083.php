<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Card\Alias\Devilsaur;
use Tiixstone\Effect\DeathrattleEffect;

/**
 * Devilsaur Egg
 */
class UNG_083 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_083';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [
            new class extends DeathrattleEffect {

                /**
                 * @param Game $game
                 * @param Minion $minion
                 * @param Minion|NULL $nextMinion
                 * @return mixed
                 */
                public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL): array
                {
                    return [
                        new SummonMinion($minion->getPlayer(), new UNG_083t1, $nextMinion),
                    ];
                }
            }
        ];
    }
}