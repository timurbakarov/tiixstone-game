<?php

namespace Tiixstone\Card;

/**
 * Wicked Knife
 */
class CS2_082 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_082';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}