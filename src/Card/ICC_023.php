<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Snowflipper Penguin
 */
class ICC_023 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_023';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}