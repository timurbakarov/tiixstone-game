<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Coren Direbrew
 */
class BRMC_92 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMC_92';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}