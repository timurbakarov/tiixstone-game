<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Baron Rivendare
 */
class NAX9_01 extends Hero
{
    public function globalId() : string
    {
        return 'NAX9_01';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}