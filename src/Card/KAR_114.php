<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Barnes
 */
class KAR_114 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_114';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}