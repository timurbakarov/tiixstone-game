<?php

namespace Tiixstone\Card;

/**
 * Overlord's Whip
 */
class TRL_360 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_360';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}