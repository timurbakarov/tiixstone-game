<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Deathbringer Saurfang
 */
class ICCA09_002 extends Hero
{
    public function globalId() : string
    {
        return 'ICCA09_002';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}