<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Twister
 */
class KARA_04_02hp extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'KARA_04_02hp';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}