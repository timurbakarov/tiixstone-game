<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Blood Beast
 */
class ICCA09_001t1 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA09_001t1';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}