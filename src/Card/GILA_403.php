<?php

namespace Tiixstone\Card;

/**
 * Butch
 */
class GILA_403 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_403';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}