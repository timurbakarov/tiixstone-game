<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect\EchoEffect;

/**
 * Ghost Light Angler
 */
class GIL_678 extends Minion
{
    protected $race = Race::MURLOC;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_678';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function handEffects()
    {
        return [EchoEffect::class];
    }
}