<?php

namespace Tiixstone\Card;

/**
 * High Priestess Jeklik
 */
class TRL_252 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_252';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}