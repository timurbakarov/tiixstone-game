<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ogre Ninja
 */
class GVG_088 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_088';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}