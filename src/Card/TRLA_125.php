<?php

namespace Tiixstone\Card;

/**
 * The Fan Favorite
 */
class TRLA_125 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_125';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 10;
    }
}