<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jason MacAllister
 */
class CRED_29 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_29';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}