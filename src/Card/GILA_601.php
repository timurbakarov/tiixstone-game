<?php

namespace Tiixstone\Card;

/**
 * Cannon
 */
class GILA_601 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_601';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}