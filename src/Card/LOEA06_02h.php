<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Stonesculpting
 */
class LOEA06_02h extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'LOEA06_02h';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}