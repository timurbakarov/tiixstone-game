<?php

namespace Tiixstone\Card;

use Tiixstone\Effect;
use Tiixstone\Effect\Target\Aggregate;

/**
 * Raid Leader
 */
class CS2_122 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_122';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        $target = new Aggregate([
            new Effect\Target\Minion,
            new Effect\Target\Friendly,
            new Effect\Target\NotSelfTarget,
        ]);

        return [new Effect\AuraAttackChangerEffect(1, $target)];
    }
}