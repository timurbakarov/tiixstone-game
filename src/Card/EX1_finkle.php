<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Finkle Einhorn
 */
class EX1_finkle extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_finkle';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}