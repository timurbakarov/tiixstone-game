<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Bonemare
 */
class ICC_705 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_705';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}