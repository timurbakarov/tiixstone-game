<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Savage Combatant
 */
class AT_039 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_039';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}