<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Earthen Pursuer
 */
class LOEA07_12 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA07_12';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}