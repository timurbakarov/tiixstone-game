<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Cryomancer
 */
class CFM_671 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_671';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}