<?php

namespace Tiixstone\Card;

/**
 * Nalaa the Redeemer
 */
class GILA_826 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_826';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 9;
    }
}