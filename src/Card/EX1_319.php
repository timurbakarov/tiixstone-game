<?php

namespace Tiixstone\Card;

use Tiixstone\Block\TakeDamage;
use Tiixstone\Game;
use Tiixstone\Battlecryable;
use Tiixstone\Race;

/**
 * Flame Imp
 */
class EX1_319 extends Minion implements Battlecryable
{
    /**
     * @var int
     */
    protected $race = Race::DEMON;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_319';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        return [new TakeDamage($this->getPlayer()->hero, 3, $this)];
    }
}