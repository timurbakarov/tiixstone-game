<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Injured Kvaldir
 */
class AT_105 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_105';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}