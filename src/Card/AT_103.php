<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * North Sea Kraken
 */
class AT_103 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_103';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 9;
    }
}