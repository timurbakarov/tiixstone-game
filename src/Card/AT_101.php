<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Pit Fighter
 */
class AT_101 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_101';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}