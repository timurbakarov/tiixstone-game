<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Vampiric Bite
 */
class ICCA05_002p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'ICCA05_002p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}