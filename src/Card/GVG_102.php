<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tinkertown Technician
 */
class GVG_102 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_102';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}