<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Stormwind Investigator Witch
 */
class TB_HeadlessHorseman_H2a extends Hero
{
    public function globalId() : string
    {
        return 'TB_HeadlessHorseman_H2a';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}