<?php

namespace Tiixstone\Card;

/**
 * The Whisperer
 */
class GILA_BOSS_50h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_50h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}