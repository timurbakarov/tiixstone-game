<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Condition;
use Tiixstone\PlayCardCondition;
use Tiixstone\Effect\FreezeEffect;

/**
 * Frost Shock
 */
class CS2_037 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_037';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new Block\TakeDamage($target, 1, $this),
            new Block\GiveEffect($target, FreezeEffect::class),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            //new Condition\Target\Enemy,
            new Condition\Target\Character,
        ]);
    }
}