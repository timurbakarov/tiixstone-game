<?php

namespace Tiixstone\Card;

/**
 * Witch's Apprentice
 */
class GIL_531 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_531';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}