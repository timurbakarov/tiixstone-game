<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ethereal Conjurer
 */
class LOE_003 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_003';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}