<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Shade of Naxxramas
 */
class FP1_005 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_005';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}