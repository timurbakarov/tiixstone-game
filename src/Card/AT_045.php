<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\Target\Friendly;
use Tiixstone\Effect\Target\Aggregate;
use Tiixstone\Effect\AuraCostChangerEffect;

/**
 * Aviana
 */
class AT_045 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_045';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [
            new AuraCostChangerEffect(1, new Aggregate([
                new Friendly(),
                new \Tiixstone\Effect\Target\Minion(),
            ]), true),
        ];
    }
}