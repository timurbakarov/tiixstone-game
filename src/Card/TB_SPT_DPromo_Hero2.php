<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * The Cow King
 */
class TB_SPT_DPromo_Hero2 extends Hero
{
    public function globalId() : string
    {
        return 'TB_SPT_DPromo_Hero2';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}