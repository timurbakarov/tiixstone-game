<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Party Capital
 */
class TB_SPT_MTH_Boss3 extends Hero
{
    public function globalId() : string
    {
        return 'TB_SPT_MTH_Boss3';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}