<?php

namespace Tiixstone\Card;

/**
 * Conjuring Attendant
 */
class TRLA_153 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_153';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}