<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Animated Armor
 */
class LOE_119 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_119';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}