<?php

namespace Tiixstone\Card;

/**
 * Rabble Bouncer
 */
class TRL_515 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_515';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}