<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Bilefin Tidehunter
 */
class OG_156 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_156';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}