<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hooded Acolyte
 */
class OG_334 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_334';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}