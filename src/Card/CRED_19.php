<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Beomki Hong
 */
class CRED_19 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_19';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}