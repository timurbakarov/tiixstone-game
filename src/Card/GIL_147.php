<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Cinderstorm
 */
class GIL_147 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_147';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}