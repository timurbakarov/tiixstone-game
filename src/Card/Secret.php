<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Block\GiveEffect;

/**
 * Class Secret
 * @package Tiixstone\Card
 */
abstract class Secret extends Spell
{
    /**
     * @param Game $game
     * @param Character|NULL $target
     * @return array
     */
    public final function cast(Game $game, Character $target = NULL): array
    {
        $this->getPlayer()->secrets->append($this);

        return [new GiveEffect($this, $this->effect())];
    }

    /**
     * @return mixed
     */
    abstract public function effect() : Effect\Secret;
}