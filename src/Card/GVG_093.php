<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\TauntEffect;

/**
 * Target Dummy
 */
class GVG_093 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_093';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [TauntEffect::class];
    }
}