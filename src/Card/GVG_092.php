<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Gnomish Experimenter
 */
class GVG_092 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_092';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}