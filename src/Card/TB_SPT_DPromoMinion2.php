<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Guardian
 */
class TB_SPT_DPromoMinion2 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_SPT_DPromoMinion2';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}