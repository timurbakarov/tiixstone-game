<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Toxic Sewer Ooze
 */
class CFM_655 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_655';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}