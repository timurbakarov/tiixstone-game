<?php

namespace Tiixstone\Card;

/**
 * Likkim
 */
class TRL_352 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_352';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}