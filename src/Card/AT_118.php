<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Grand Crusader
 */
class AT_118 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_118';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}