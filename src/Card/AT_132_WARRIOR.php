<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\IncreaseArmor;

/**
 * Tank Up!
 */
class AT_132_WARRIOR extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'AT_132_WARRIOR';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [new IncreaseArmor($this->getPlayer()->hero, 4)];
    }
}