<?php

namespace Tiixstone\Card;

/**
 * Voodoo Doll
 */
class GIL_614 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_614';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}