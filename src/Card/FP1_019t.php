<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Treant
 */
class FP1_019t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_019t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}