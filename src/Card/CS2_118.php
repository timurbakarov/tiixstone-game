<?php

namespace Tiixstone\Card;

use Tiixstone\Race;

/**
 * Magma Rager
 */
class CS2_118 extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::ELEMENTAL;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_118';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}