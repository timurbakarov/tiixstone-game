<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hamilton Chu
 */
class CRED_16 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_16';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 9;
    }
}