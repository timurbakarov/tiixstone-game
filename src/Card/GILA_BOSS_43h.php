<?php

namespace Tiixstone\Card;

/**
 * Gravekeeper Damph
 */
class GILA_BOSS_43h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_43h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}