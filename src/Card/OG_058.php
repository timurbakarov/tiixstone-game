<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Rusty Hook
 */
class OG_058 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_058';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}