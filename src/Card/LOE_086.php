<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Summoning Stone
 */
class LOE_086 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_086';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}