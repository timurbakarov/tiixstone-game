<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\PlayCardCondition;

/**
 * Mark of the Wild
 */
class CS2_009 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_009';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new Block\GiveEffect($target, Effect\TauntEffect::class),
            new Block\GiveEffect($target, new Effect\AttackChangerEffect(2)),
            new Block\GiveEffect($target, new Effect\HealthChangerEffect(2)),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
        ]);
    }
}