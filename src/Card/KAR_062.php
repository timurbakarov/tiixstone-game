<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Netherspite Historian
 */
class KAR_062 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_062';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}