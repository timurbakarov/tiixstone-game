<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Piñata Golem
 */
class TB_MammothParty_Boss002 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_MammothParty_Boss002';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 85;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}