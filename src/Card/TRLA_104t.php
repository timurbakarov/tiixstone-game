<?php

namespace Tiixstone\Card;

/**
 * Akali's Champion
 */
class TRLA_104t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_104t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}