<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * "Little Friend"
 */
class CFM_648t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_648t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}