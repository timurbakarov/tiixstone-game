<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Primalfin Totem
 */
class UNG_201 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_201';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}