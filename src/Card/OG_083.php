<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Twilight Flamecaller
 */
class OG_083 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_083';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}