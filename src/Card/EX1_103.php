<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Coldlight Seer
 */
class EX1_103 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_103';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}