<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;
use Tiixstone\Effect\SylvanasWindrunnerDeathrattle;

/**
 * Sylvanas Windrunner
 */
class EX1_016 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_016';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [SylvanasWindrunnerDeathrattle::class];
    }
}