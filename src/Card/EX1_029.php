<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\Deathrattle\LeperGnomeDeathrattle;

/**
 * Leper Gnome
 */
class EX1_029 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_029';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [LeperGnomeDeathrattle::class];
    }
}