<?php

namespace Tiixstone\Card;

use Tiixstone\Block\DealSplashDamage;
use Tiixstone\Block\DrawCard;
use Tiixstone\Game;
use Tiixstone\Manager\TargetManager;

/**
 * Fan of Knives
 */
class EX1_129 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_129';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new DealSplashDamage(TargetManager::ANY_OPPONENT_MINION, $game->withSpellDamage(1), $this),
            new DrawCard($this->getPlayer()),
        ];
    }
}