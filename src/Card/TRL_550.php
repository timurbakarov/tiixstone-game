<?php

namespace Tiixstone\Card;

/**
 * Amani War Bear
 */
class TRL_550 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_550';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}