<?php

namespace Tiixstone\Card;

/**
 * Bwonsamdi's Sanctum
 */
class TRLA_114t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_114t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}