<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Princess Huhuran
 */
class OG_309 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_309';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}