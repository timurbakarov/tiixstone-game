<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Skelesaurus Hex
 */
class LOEA13_1 extends Hero
{
    public function globalId() : string
    {
        return 'LOEA13_1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}