<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Steam Surger
 */
class UNG_021 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_021';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}