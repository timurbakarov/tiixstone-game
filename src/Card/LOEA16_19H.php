<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Sun Raider Phaerix
 */
class LOEA16_19H extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA16_19H';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 10;
    }
}