<?php

namespace Tiixstone\Card;

/**
 * Krag'wa's Grace
 */
class TRLA_155t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_155t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}