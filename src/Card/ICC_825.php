<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Abominable Bowman
 */
class ICC_825 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_825';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}