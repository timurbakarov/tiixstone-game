<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Brilliance
 */
class KARA_00_04 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'KARA_00_04';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}