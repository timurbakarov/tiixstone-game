<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tuskarr Totemic
 */
class AT_046 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_046';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}