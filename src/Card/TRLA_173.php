<?php

namespace Tiixstone\Card;

/**
 * Redcrest Rocker
 */
class TRLA_173 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_173';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}