<?php

namespace Tiixstone\Card;

/**
 * Nightscale Whelp
 */
class GIL_190t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_190t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}