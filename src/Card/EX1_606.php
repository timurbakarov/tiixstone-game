<?php

namespace Tiixstone\Card;

use Tiixstone\Block\DrawCard;
use Tiixstone\Block\IncreaseArmor;
use Tiixstone\Game;

/**
 * Shield Block
 */
class EX1_606 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_606';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new IncreaseArmor($this->getPlayer()->hero, 5),
            new DrawCard($this->getPlayer()),
        ];
    }
}