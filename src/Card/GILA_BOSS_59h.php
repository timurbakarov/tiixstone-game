<?php

namespace Tiixstone\Card;

/**
 * Lord Godfrey
 */
class GILA_BOSS_59h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_59h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}