<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Verdant Longneck
 */
class UNG_100 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_100';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}