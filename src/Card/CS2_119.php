<?php

namespace Tiixstone\Card;

use Tiixstone\Race;

/**
 * Oasis Snapjaw
 */
class CS2_119 extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::BEAST;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_119';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}