<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Power of the Firelord
 */
class BRMA03_2 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'BRMA03_2';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}