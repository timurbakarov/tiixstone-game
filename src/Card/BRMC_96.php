<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * High Justice Grimstone
 */
class BRMC_96 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMC_96';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}