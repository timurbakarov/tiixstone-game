<?php

namespace Tiixstone\Card;

/**
 * Bubba
 */
class GILA_410 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_410';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 9;
    }
}