<?php

namespace Tiixstone\Card;

/**
 * Gnarlroot
 */
class GILA_BOSS_31h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_31h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}