<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ancient Shieldbearer
 */
class OG_301 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_301';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}