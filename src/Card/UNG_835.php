<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Chittering Tunneler
 */
class UNG_835 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_835';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}