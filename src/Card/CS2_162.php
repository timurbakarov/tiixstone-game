<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\TauntEffect;

/**
 * Lord of the Arena
 */
class CS2_162 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_162';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [TauntEffect::class];
    }
}