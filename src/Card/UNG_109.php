<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Elder Longneck
 */
class UNG_109 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_109';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}