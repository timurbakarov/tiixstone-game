<?php

namespace Tiixstone\Card;

/**
 * Rikkar
 */
class TRLA_209h_Hunter extends Hero
{
    public function globalId() : string
    {
        return 'TRLA_209h_Hunter';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}