<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Fireblast
 */
class CS2_034_H2 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'CS2_034_H2';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}