<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Nerubian Egg
 */
class FP1_007 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_007';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}