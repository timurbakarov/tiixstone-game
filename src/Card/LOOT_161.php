<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Condition;
use Tiixstone\Battlecryable;
use Tiixstone\PlayCardCondition;

/**
 * Carnivorous Cube
 */
class LOOT_161 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOOT_161';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param \Tiixstone\Card\Attackable $attackable
     * @param \Tiixstone\Card\Minion|NULL $positionMinion
     * @param \Tiixstone\Card\Character|NULL|Minion $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = null, Character $target = null)
    {
        if(!$target) {
            return [];
        }

        $game->logBlock('carnivorous-cube-battlecry');

        return [
            new Block\MarkDestroyed($target, $this),
            new Block\GiveEffect($this, new Effect\Deathrattle\CarnivorousCubeDeathrattle($target))
        ];
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [Effect\Deathrattle\CarnivorousCubeDeathrattle::class];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Minion,
            new Condition\Target\Friendly,
        ]);
    }
}