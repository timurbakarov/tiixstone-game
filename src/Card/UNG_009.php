<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ravasaur Runt
 */
class UNG_009 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_009';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}