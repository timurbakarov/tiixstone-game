<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\SpellDamageEffect;

/**
 * Archmage
 */
class CS2_155 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_155';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }

    /**
     * @return array
     * @throws \Tiixstone\Exception
     */
    public function boardEffects()
    {
        return [new SpellDamageEffect(1)];
    }
}