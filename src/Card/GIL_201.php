<?php

namespace Tiixstone\Card;

/**
 * Pumpkin Peasant
 */
class GIL_201 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_201';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}