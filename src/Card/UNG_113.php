<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Bright-Eyed Scout
 */
class UNG_113 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_113';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}