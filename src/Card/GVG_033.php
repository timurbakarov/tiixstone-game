<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * Tree of Life
 */
class GVG_033 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_033';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 9;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}