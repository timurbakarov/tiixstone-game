<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Atramedes
 */
class BRMA16_1H extends Hero
{
    public function globalId() : string
    {
        return 'BRMA16_1H';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}