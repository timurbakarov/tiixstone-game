<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Shado-Pan Rider
 */
class AT_028 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_028';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}