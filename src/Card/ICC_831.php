<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Bloodreaver Gul'dan
 */
class ICC_831 extends Hero
{
    public function globalId() : string
    {
        return 'ICC_831';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}