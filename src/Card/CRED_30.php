<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * JC Park
 */
class CRED_30 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_30';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}