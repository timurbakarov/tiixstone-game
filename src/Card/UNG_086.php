<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Giant Anaconda
 */
class UNG_086 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_086';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}