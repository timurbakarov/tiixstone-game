<?php

namespace Tiixstone\Card;

/**
 * Sandbinder
 */
class GIL_581 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_581';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}