<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Keening Banshee
 */
class ICC_911 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_911';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}