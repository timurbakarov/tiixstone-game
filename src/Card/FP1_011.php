<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Webspinner
 */
class FP1_011 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_011';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}