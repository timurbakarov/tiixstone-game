<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Twin Emperor Vek'nilash
 */
class OG_319 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_319';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}