<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\Target\SelfTarget;
use Tiixstone\Condition\Initiator\CanNotAttack;
use Tiixstone\Effect\AttackConditionChangerEffect;

/**
 * Ancient Watcher
 */
class EX1_045 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_045';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [
            new AttackConditionChangerEffect([
                new CanNotAttack(),
            ], new SelfTarget()),
        ];
    }
}