<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Cho'gall
 */
class OG_121 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_121';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}