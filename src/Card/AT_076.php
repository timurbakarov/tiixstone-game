<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Murloc Knight
 */
class AT_076 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_076';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}