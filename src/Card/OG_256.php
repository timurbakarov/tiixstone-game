<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Spawn of N'Zoth
 */
class OG_256 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_256';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}