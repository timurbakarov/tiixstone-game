<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Arcanite Reaper
 */
class CS2_112 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_112';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 5;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}