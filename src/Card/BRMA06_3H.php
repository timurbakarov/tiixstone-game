<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Ragnaros the Firelord
 */
class BRMA06_3H extends Hero
{
    public function globalId() : string
    {
        return 'BRMA06_3H';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}