<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Madder Bomber
 */
class GVG_090 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_090';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}