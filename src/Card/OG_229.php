<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ragnaros, Lightlord
 */
class OG_229 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_229';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}