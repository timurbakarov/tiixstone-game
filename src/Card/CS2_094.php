<?php

namespace Tiixstone\Card;

use Tiixstone\Block\DrawCard;
use Tiixstone\Block\TakeDamage;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Hammer of Wrath
 */
class CS2_094 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_094';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new TakeDamage($target, $game->withSpellDamage(3)),
            new DrawCard($this->getPlayer()),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Character,
        ]);
    }
}