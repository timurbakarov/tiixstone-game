<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Rare Spear
 */
class LOEA09_4H extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA09_4H';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}