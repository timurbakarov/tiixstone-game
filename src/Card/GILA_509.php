<?php

namespace Tiixstone\Card;

/**
 * Frenzied Trapper
 */
class GILA_509 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_509';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}