<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\Block\GiveEffect;
use Tiixstone\PlayCardCondition;
use Tiixstone\Manager\TargetManager;
use Tiixstone\Effect\Buff\BlessingOfMightEffect;

/**
 * Blessing of Might
 */
class CS2_087 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_087';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $game->logBlock('blessing-of-might');

        return [
            new GiveEffect($target, BlessingOfMightEffect::class),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Minion,
        ]);
    }
}