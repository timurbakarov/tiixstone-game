<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Shadowbomber
 */
class GVG_009 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_009';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}