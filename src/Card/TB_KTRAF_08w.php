<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Massive Runeblade
 */
class TB_KTRAF_08w extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_KTRAF_08w';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}