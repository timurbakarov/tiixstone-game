<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Armorsmith
 */
class EX1_402 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_402';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}