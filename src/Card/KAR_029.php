<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Runic Egg
 */
class KAR_029 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_029';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}