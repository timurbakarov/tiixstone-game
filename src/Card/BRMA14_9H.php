<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Magmatron
 */
class BRMA14_9H extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA14_9H';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}