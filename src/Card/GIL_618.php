<?php

namespace Tiixstone\Card;

/**
 * Glinda Crowskin
 */
class GIL_618 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_618';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}