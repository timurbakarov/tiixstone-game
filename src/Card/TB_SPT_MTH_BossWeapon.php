<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Party Armory
 */
class TB_SPT_MTH_BossWeapon extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_SPT_MTH_BossWeapon';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}