<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Firesworn
 */
class BRMA04_3H extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA04_3H';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}