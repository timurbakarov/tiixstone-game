<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Psych-o-Tron
 */
class OG_145 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_145';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}