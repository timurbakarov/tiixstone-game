<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect\AttackChangerEffect;
use Tiixstone\Effect\Trigger\EndOfTheTurn;
use Tiixstone\Game;

/**
 * Savage Roar
 */
class CS2_011 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_011';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $chars = $game->targetManager->allFriendlyCharacters($game, $this->getPlayer());

        return array_map(function(Character $character) {
            return new GiveEffect(
                $character,
                new EndOfTheTurn(new AttackChangerEffect(2))
            );
        }, $chars);
    }
}