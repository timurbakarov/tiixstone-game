<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Giant Insect
 */
class LOEA04_23h extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA04_23h';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 10;
    }
}