<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect;

/**
 * Soot Spewer
 */
class GVG_123 extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::MECH;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_123';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     * @throws \Tiixstone\Exception
     */
    public function boardEffects()
    {
        return [new Effect\SpellDamageEffect(1)];
    }
}