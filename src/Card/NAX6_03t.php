<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Spore
 */
class NAX6_03t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX6_03t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}