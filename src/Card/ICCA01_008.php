<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Terrible Tank
 */
class ICCA01_008 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA01_008';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}