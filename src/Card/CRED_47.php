<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dan Emmons
 */
class CRED_47 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_47';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}