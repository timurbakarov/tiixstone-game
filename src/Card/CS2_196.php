<?php

namespace Tiixstone\Card;

use Tiixstone\Block\SummonMinion;
use Tiixstone\Card\Alias\Boar;
use Tiixstone\Game;
use Tiixstone\Battlecryable;

/**
 * Razorfen Hunter
 */
class CS2_196 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_196';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        return [new SummonMinion($attackable->getPlayer(), new CS2_boar(), $attackable, 'right')];
    }
}