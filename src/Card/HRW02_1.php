<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Gearmaster Mechazod
 */
class HRW02_1 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'HRW02_1';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 80;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}