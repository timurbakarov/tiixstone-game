<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Scaled Nightmare
 */
class OG_271 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_271';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}