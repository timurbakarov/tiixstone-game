<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\TauntEffect;
use Tiixstone\Effect\Deathrattle\UnstableGhoulDeathrattle;

/**
 * Unstable Ghoul
 */
class FP1_024 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_024';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [UnstableGhoulDeathrattle::class, TauntEffect::class];
    }
}