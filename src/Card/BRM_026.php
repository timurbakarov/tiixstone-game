<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hungry Dragon
 */
class BRM_026 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_026';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}