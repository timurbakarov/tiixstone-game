<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Bone Construct
 */
class BRMA17_6 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA17_6';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}