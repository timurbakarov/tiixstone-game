<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Grave Shambler
 */
class ICC_097 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_097';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}