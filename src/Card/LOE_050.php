<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mounted Raptor
 */
class LOE_050 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_050';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}