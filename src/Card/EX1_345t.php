<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Shadow of Nothing
 */
class EX1_345t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_345t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}