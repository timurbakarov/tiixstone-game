<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\Trigger\FloatingWatcherEffect;

/**
 * Floating Watcher
 */
class GVG_100 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_100';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [FloatingWatcherEffect::class];
    }
}