<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Alex Tsang
 */
class CRED_84 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_84';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 14;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}