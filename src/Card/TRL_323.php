<?php

namespace Tiixstone\Card;

/**
 * Emberscale Drake
 */
class TRL_323 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_323';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}