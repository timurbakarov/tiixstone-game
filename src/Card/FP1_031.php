<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\BaronRivendareEffect;

/**
 * Baron Rivendare
 */
class FP1_031 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_031';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [BaronRivendareEffect::class];
    }
}