<?php

namespace Tiixstone\Card;

use Tiixstone\Race;

/**
 * Scarab
 */
class TRL_503t extends Minion
{
    protected $race = Race::BEAST;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_503t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}