<?php

namespace Tiixstone\Card;

/**
 * Deepsea Diver
 */
class TRLA_156 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_156';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}