<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Silver Hand Recruit
 */
class CS2_101t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_101t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}