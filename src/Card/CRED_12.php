<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Rachelle Davis
 */
class CRED_12 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_12';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}