<?php

namespace Tiixstone\Card;

/**
 * Plaguemaster Rancel
 */
class GILA_BOSS_68h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_68h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}