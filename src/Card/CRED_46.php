<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Keith Landes
 */
class CRED_46 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_46';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}