<?php

namespace Tiixstone\Card;

use Tiixstone\Battlecryable;
use Tiixstone\Block\Weapon\ReduceDurability;
use Tiixstone\Game;

/**
 * Bloodsail Corsair
 */
class NEW1_025 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_025';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        if($attackable->getPlayer()->getOpponent($game)->hero->hasWeapon()) {
            return [new ReduceDurability($attackable->getPlayer()->getOpponent($game)->hero->weapon)];
        }

        return [];
    }
}