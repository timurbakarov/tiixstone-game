<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Varian Wrynn
 */
class AT_072 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_072';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}