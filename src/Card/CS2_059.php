<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Blood Imp
 */
class CS2_059 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_059';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}