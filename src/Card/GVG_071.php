<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Lost Tallstrider
 */
class GVG_071 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_071';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}