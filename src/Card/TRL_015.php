<?php

namespace Tiixstone\Card;

/**
 * Ticket Scalper
 */
class TRL_015 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_015';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}