<?php

namespace Tiixstone\Card;

/**
 * Hench-Clan Thug
 */
class GIL_534 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_534';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}