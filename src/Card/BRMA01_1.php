<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Coren Direbrew
 */
class BRMA01_1 extends Hero
{
    public function globalId() : string
    {
        return 'BRMA01_1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}