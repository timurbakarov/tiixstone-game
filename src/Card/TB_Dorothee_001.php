<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dorothee
 */
class TB_Dorothee_001 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_Dorothee_001';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}