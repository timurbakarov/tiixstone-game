<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Understudy
 */
class TB_KTRAF_Under extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_KTRAF_Under';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}