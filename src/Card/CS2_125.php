<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect;

/**
 * Ironfur Grizzly
 */
class CS2_125 extends Minion
{
    protected $race = Race::BEAST;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_125';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [Effect\TauntEffect::class];
    }
}