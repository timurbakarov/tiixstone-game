<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Sulfuras
 */
class UNG_934t1 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_934t1';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}