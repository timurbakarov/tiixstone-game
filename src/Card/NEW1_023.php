<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect;

/**
 * Faerie Dragon
 */
class NEW1_023 extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::DRAGON;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_023';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [Effect\ElusiveEffect::class];
    }
}