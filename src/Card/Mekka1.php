<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Homing Chicken
 */
class Mekka1 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'Mekka1';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}