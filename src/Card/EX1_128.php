<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\GiveEffect;
use Tiixstone\PlayCardCondition;
use Tiixstone\Effect\StealthEffect;
use Tiixstone\Condition\Target\Friendly;
use Tiixstone\Effect\Trigger\UntilYourNextTurn;

/**
 * Conceal
 */
class EX1_128 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_128';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new GiveEffect($target, new UntilYourNextTurn(new StealthEffect))];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
            new Friendly,
        ]);
    }
}