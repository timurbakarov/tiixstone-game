<?php

namespace Tiixstone\Card;

/**
 * Spirit of the Rhino
 */
class TRL_327 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_327';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}