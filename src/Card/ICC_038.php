<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Righteous Protector
 */
class ICC_038 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_038';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}