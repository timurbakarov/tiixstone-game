<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Sorcerer's Apprentice
 */
class EX1_608 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_608';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}