<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Blazing Invocation
 */
class GIL_836 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_836';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}