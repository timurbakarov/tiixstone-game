<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\Deathrattle\HarvestGolemDeathrattle;

/**
 * Harvest Golem
 */
class EX1_556 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_556';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [HarvestGolemDeathrattle::class];
    }
}