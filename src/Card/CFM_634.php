<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Lotus Assassin
 */
class CFM_634 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_634';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}