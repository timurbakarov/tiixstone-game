<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Seyil Yoon
 */
class CRED_41 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_41';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}