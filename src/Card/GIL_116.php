<?php

namespace Tiixstone\Card;

/**
 * Arcane Keysmith
 */
class GIL_116 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_116';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}