<?php

namespace Tiixstone\Card;

use Tiixstone\Race;

/**
 * Murloc Raider
 */
class CS2_168 extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::MURLOC;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_168';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}