<?php

namespace Tiixstone\Card;

/**
 * Razzle Dazzler
 */
class TRLA_130 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_130';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}