<?php

namespace Tiixstone\Card;

/**
 * Dark Reliquary
 */
class TRLA_178 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_178';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}