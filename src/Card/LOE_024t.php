<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Rolling Boulder
 */
class LOE_024t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_024t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}