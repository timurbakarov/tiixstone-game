<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Vaelastrasz the Corrupt
 */
class BRMA11_1 extends Hero
{
    public function globalId() : string
    {
        return 'BRMA11_1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}