<?php

namespace Tiixstone\Card;

/**
 * Rikkar
 */
class TRLA_209h_Shaman extends Hero
{
    public function globalId() : string
    {
        return 'TRLA_209h_Shaman';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}