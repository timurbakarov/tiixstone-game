<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Flesheating Ghoul
 */
class tt_004 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'tt_004';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}