<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\Deathrattle\LootHoarderDeathrattle;

/**
 * Loot Hoarder
 */
class EX1_096 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_096';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [LootHoarderDeathrattle::class];
    }
}