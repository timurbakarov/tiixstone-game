<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Block\TakeDamage;
use Tiixstone\Effect\FreezeEffect;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Ice Lance
 */
class CS2_031 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_031';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        if($game->effects->cardHasEffect($target, FreezeEffect::class)) {
            return [new TakeDamage($target, $game->withSpellDamage(4))];
        } else {
            return [new GiveEffect($target, FreezeEffect::class)];
        }
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Character,
        ]);
    }
}