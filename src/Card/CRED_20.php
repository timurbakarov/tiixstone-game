<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Brian Birmingham
 */
class CRED_20 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_20';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}