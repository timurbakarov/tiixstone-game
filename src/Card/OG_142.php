<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Eldritch Horror
 */
class OG_142 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_142';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}