<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Unholy Shadow
 */
class NAX9_06 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'NAX9_06';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}