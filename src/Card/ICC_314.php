<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * The Lich King
 */
class ICC_314 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_314';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}