<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect\ChargeEffect;

/**
 * Hound
 */
class EX1_538t extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::BEAST;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_538t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [ChargeEffect::class];
    }
}