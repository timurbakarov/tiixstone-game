<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Blood-Queen Lana'thel
 */
class FB_LK008h extends Hero
{
    public function globalId() : string
    {
        return 'FB_LK008h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}