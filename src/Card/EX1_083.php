<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tinkmaster Overspark
 */
class EX1_083 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_083';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}