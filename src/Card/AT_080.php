<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Garrison Commander
 */
class AT_080 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_080';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}