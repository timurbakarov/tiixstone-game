<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Barrel
 */
class TU4c_003 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TU4c_003';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}