<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Battlecryable;
use Tiixstone\Block\GiveEffect;
use Tiixstone\PlayCardCondition;
use Tiixstone\Effect\TauntEffect;
use Tiixstone\Condition\Target\Race;
use Tiixstone\Condition\Target\Friendly;
use Tiixstone\Effect\AttackChangerEffect;
use Tiixstone\Effect\HealthChangerEffect;

/**
 * Houndmaster
 */
class DS1_070 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'DS1_070';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
            new Friendly,
            new Race(new \Tiixstone\Race(\Tiixstone\Race::BEAST))
        ]);
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        if(!$target) {
            return [];
        }

        return [
            new GiveEffect($target, new AttackChangerEffect(2)),
            new GiveEffect($target, new HealthChangerEffect(2)),
            new GiveEffect($target, TauntEffect::class),
        ];
    }
}