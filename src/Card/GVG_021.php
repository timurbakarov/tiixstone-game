<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mal'Ganis
 */
class GVG_021 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_021';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 9;
    }
}