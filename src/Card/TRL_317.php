<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Blast Wave
 */
class TRL_317 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_317';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 5;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}