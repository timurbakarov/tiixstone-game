<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\Block\TakeDamage;
use Tiixstone\PlayCardCondition;

/**
 * Kill Command
 */
class EX1_539 extends Spell
{
    public function globalId() : string
    {
        return 'EX1_539';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null): array
    {
        return [new TakeDamage($target, $game->withSpellDamage($this->damage($game)))];
    }

    /**
     * @param Game $game
     * @return int
     */
    private function damage(Game $game)
    {
        return $this->hasFriendlyBeast($game) ? 5 : 3;
    }

    /***
     * @param Game $game
     * @return bool
     */
    private function hasFriendlyBeast(Game $game)
    {
        /** @var Minion $minion */
        foreach ($game->currentPlayer()->board->all() as $minion) {
            if($minion->race()->isBeast()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Character,
        ]);
    }
}