<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\TauntEffect;

/**
 * Slime
 */
class FP1_012t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_012t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [TauntEffect::class];
    }
}