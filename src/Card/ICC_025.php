<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Rattling Rascal
 */
class ICC_025 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_025';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}