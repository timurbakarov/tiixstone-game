<?php

namespace Tiixstone\Card;

/**
 * High Priestess Jeklik
 */
class TRLA_206h extends Hero
{
    public function globalId() : string
    {
        return 'TRLA_206h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}