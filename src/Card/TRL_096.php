<?php

namespace Tiixstone\Card;

/**
 * Griftah
 */
class TRL_096 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_096';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}