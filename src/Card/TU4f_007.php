<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Crazy Monkey
 */
class TU4f_007 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TU4f_007';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}