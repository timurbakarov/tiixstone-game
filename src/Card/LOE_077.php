<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\BrannBronzebeardEffect;

/**
 * Brann Bronzebeard
 */
class LOE_077 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_077';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    public function boardEffects()
    {
        return [BrannBronzebeardEffect::class];
    }
}