<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Tirion Fordring
 */
class ICCA01_013 extends Hero
{
    public function globalId() : string
    {
        return 'ICCA01_013';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}