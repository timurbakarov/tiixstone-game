<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Eaglehorn Bow
 */
class EX1_536 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_536';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}