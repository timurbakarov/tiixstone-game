<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Steward of Darkshire
 */
class OG_310 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_310';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}