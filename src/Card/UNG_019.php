<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\TauntEffect;
use Tiixstone\Race;
use Tiixstone\Effect\ElusiveEffect;

/**
 * Air Elemental
 */
class UNG_019 extends Minion
{
    protected $race = Race::ELEMENTAL;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_019';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [
            ElusiveEffect::class,
            TauntEffect::class,
        ];
    }
}