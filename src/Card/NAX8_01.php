<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Gothik the Harvester
 */
class NAX8_01 extends Hero
{
    public function globalId() : string
    {
        return 'NAX8_01';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}