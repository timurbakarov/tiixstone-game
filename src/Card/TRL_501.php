<?php

namespace Tiixstone\Card;

/**
 * Auchenai Phantasm
 */
class TRL_501 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_501';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}