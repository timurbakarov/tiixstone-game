<?php

namespace Tiixstone\Card;

/**
 * Jan'alai's Progeny
 */
class TRLA_129t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_129t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}