<?php

namespace Tiixstone\Card;

/**
 * Phantom Militia
 */
class GIL_207 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_207';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}