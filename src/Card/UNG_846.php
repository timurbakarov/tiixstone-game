<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Shimmering Tempest
 */
class UNG_846 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_846';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}