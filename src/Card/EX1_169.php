<?php

namespace Tiixstone\Card;

use Tiixstone\Block\SetManaAvailable;
use Tiixstone\Game;

/**
 * Innervate
 */
class EX1_169 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_169';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        if($game->currentPlayer()->availableMana() >= $game->settings->playerMaximumManaLimit()) {
            return [];
        }

        return [new SetManaAvailable($game->currentPlayer(), $game->currentPlayer()->availableMana() + 1)];
    }
}