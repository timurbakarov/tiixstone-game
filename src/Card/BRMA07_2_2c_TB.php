<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * ME SMASH
 */
class BRMA07_2_2c_TB extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA07_2_2c_TB';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}