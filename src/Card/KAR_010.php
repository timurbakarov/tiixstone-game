<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Nightbane Templar
 */
class KAR_010 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_010';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}