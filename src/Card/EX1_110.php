<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Cairne Bloodhoof
 */
class EX1_110 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_110';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}