<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Mark of the Loa
 */
class TRL_254 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_254';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}