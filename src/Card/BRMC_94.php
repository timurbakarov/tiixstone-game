<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Sulfuras
 */
class BRMC_94 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMC_94';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 6;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}