<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * OLDN3wb Mage
 */
class TBST_002 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TBST_002';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}