<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Michael Schweitzer
 */
class CRED_10 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_10';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}