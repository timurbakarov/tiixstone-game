<?php

namespace Tiixstone\Card;

/**
 * Hunting Mastiff
 */
class ICC_828t5 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_828t5';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}