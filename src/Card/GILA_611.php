<?php

namespace Tiixstone\Card;

/**
 * Tuskarr Raider
 */
class GILA_611 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_611';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}