<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Antique Healbot
 */
class GVG_069 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_069';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}