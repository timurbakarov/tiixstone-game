<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Lilian Voss
 */
class ICC_811 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_811';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}