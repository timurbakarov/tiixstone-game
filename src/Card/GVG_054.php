<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Ogre Warmaul
 */
class GVG_054 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_054';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}