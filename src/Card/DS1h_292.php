<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\TakeDamage;

/**
 * Steady Shot
 */
class DS1h_292 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'DS1h_292';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        $game->logBlock('steady-shot');

        return [new TakeDamage($game->idlePlayer()->hero, $this->damage(), $this)];
    }

    /**
     * @return int
     */
    private function damage()
    {
        return 2;
    }
}