<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Thunder Bluff Valiant
 */
class AT_049 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_049';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}