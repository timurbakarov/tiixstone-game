<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Fallen Sun Cleric
 */
class ICC_094 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_094';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}