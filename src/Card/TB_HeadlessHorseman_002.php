<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Sinister Squashling
 */
class TB_HeadlessHorseman_002 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_HeadlessHorseman_002';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}