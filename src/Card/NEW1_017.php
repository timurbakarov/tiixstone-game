<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hungry Crab
 */
class NEW1_017 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_017';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}