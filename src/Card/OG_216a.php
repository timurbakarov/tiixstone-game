<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Spider
 */
class OG_216a extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_216a';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}