<?php

namespace Tiixstone\Card;

use Tiixstone\Block\DealSplashDamage;
use Tiixstone\Game;
use Tiixstone\Manager\TargetManager;

/**
 * Flamestrike
 */
class CS2_032 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_032';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 7;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new DealSplashDamage(TargetManager::ANY_OPPONENT_MINION, $game->withSpellDamage(4), $this)];
    }
}