<?php

namespace Tiixstone\Card;

/**
 * Krag'wa, the Frog
 */
class TRL_345 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_345';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}