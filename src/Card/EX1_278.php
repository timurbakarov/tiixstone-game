<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Condition;
use Tiixstone\PlayCardCondition;

/**
 * Shiv
 */
class EX1_278 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_278';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new Block\TakeDamage($target, $game->withSpellDamage(1)),
            new Block\DrawCard($game->currentPlayer()),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Character,
        ]);
    }
}