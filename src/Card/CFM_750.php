<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Krul the Unshackled
 */
class CFM_750 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_750';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}