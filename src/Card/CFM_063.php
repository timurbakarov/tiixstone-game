<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Kooky Chemist
 */
class CFM_063 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_063';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}