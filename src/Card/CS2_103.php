<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Condition;
use Tiixstone\Effect\AttackConditionChangerEffect;
use Tiixstone\Effect\ChargeEffect;
use Tiixstone\Effect\Target\SelfTarget;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Charge
 */
class CS2_103 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_103';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new GiveEffect($target, ChargeEffect::class),
            new GiveEffect($target, new AttackConditionChangerEffect([new Condition\Target\Minion()], new SelfTarget())),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Friendly,
            new Condition\Target\Minion,
        ]);
    }
}