<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Eric Dodds
 */
class CRED_02 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_02';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}