<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Small-Time Buccaneer
 */
class CFM_325 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_325';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}