<?php

namespace Tiixstone\Card;

/**
 * Bwonsamdi's Keeper
 */
class TRLA_151 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_151';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}