<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Warhorse Trainer
 */
class AT_075 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_075';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}