<?php

namespace Tiixstone\Card;

/**
 * Bloodwash Medic
 */
class TRLA_150 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_150';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}