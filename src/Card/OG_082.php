<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Evolved Kobold
 */
class OG_082 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_082';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}