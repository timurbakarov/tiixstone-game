<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Gonk's Resilience
 */
class TRL_254a extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_254a';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}