<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dr. Boom Boom Boom Boom
 */
class TB_FW_DrBoomMega extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_FW_DrBoomMega';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 9;
    }
}