<?php

namespace Tiixstone\Card;

/**
 * Akali's War Drum
 */
class TRLA_170t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_170t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}