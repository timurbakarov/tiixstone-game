<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Fierce Monkey
 */
class LOE_022 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_022';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}