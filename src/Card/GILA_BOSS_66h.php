<?php

namespace Tiixstone\Card;

/**
 * The Mute
 */
class GILA_BOSS_66h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_66h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}