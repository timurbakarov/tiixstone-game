<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Ballista Shot
 */
class DS1h_292_H1_AT_132 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'DS1h_292_H1_AT_132';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}