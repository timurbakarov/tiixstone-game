<?php

namespace Tiixstone\Card;

/**
 * Dark Diviner
 */
class TRLA_184 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_184';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}