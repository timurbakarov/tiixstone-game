<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jacob Jarecki
 */
class CRED_82 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_82';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}