<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Sojin Hwang
 */
class CRED_81 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_81';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}