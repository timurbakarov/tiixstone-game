<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\PowerOverwhelmingEffect;
use Tiixstone\Game;
use Tiixstone\Block\GiveEffect;
use Tiixstone\PlayCardCondition;
use Tiixstone\Condition\Target\Friendly;
use Tiixstone\Effect\AttackChangerEffect;
use Tiixstone\Effect\HealthChangerEffect;
use Tiixstone\Effect\Trigger\EndOfTheTurn;

/**
 * Power Overwhelming
 */
class EX1_316 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_316';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new GiveEffect($target, new EndOfTheTurn(new AttackChangerEffect(4))),
            new GiveEffect($target, new EndOfTheTurn(new HealthChangerEffect(4))),
            new GiveEffect($target, PowerOverwhelmingEffect::class),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
            new Friendly,
        ]);
    }
}