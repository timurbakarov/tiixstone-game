<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ancient of Lore
 */
class NEW1_008 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_008';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}