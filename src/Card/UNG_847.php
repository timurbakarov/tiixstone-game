<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Blazecaller
 */
class UNG_847 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_847';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}