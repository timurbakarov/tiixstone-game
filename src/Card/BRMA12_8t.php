<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Chromatic Dragonkin
 */
class BRMA12_8t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA12_8t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}