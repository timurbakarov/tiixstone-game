<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Sleeping Acolyte
 */
class ICCA05_003 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA05_003';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}