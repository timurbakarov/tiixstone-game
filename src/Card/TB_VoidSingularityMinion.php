<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Void Singularity
 */
class TB_VoidSingularityMinion extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_VoidSingularityMinion';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}