<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Poisoned Blade
 */
class AT_034 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_034';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}