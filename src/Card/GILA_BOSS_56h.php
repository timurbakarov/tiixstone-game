<?php

namespace Tiixstone\Card;

/**
 * Face Collector
 */
class GILA_BOSS_56h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_56h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}