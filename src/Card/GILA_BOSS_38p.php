<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Call of the Raven
 */
class GILA_BOSS_38p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_38p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}