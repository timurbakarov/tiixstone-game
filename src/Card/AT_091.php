<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tournament Medic
 */
class AT_091 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_091';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}