<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect\TauntEffect;

/**
 * Voidwalker
 */
class CS2_065 extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::DEMON;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_065';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [TauntEffect::class];
    }
}