<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;
use Tiixstone\Block\GiveEffect;
use Tiixstone\Block\IncreaseArmor;
use Tiixstone\Effect\ShapeshiftEffect;

/**
 * Shapeshift
 */
class CS2_017 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'CS2_017';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        $game->logBlock('shapesshift');

        return [
            new IncreaseArmor($game->currentPlayer()->hero, 1),
            new GiveEffect($game->currentPlayer()->hero, ShapeshiftEffect::class),
        ];
    }
}