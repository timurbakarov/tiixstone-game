<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Runeforge Haunter
 */
class ICC_240 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_240';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}