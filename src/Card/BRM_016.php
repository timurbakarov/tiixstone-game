<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Axe Flinger
 */
class BRM_016 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_016';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}