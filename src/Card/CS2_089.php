<?php

namespace Tiixstone\Card;

use Tiixstone\Block\HealCharacter;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Holy Light
 */
class CS2_089 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_089';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new HealCharacter($target, 6, $this)];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Character,
        ]);
    }
}