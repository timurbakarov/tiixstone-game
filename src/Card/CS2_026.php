<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect\FreezeEffect;
use Tiixstone\Game;

/**
 * Frost Nova
 */
class CS2_026 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_026';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $minions = $game->targetManager->allEnemyMinions($game, $this->getPlayer());

        return array_map(function(Minion $minion) {
            return new GiveEffect($minion, FreezeEffect::class);
        }, $minions);
    }
}