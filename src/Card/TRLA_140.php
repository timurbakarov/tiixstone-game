<?php

namespace Tiixstone\Card;

/**
 * Blessed One
 */
class TRLA_140 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_140';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}