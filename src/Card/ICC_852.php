<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Prince Taldaram
 */
class ICC_852 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_852';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}