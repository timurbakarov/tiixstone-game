<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mistress of Pain
 */
class GVG_018 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_018';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}