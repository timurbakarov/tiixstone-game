<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Cup
 */
class KAR_A02_05 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_A02_05';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}