<?php

namespace Tiixstone\Card;

use Tiixstone\Block\SummonMinion;
use Tiixstone\Card\Alias\Hound;
use Tiixstone\Game;

/**
 * Unleash the Hounds
 */
class EX1_538 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_538';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $enemyMinionsCount = $game->idlePlayer()->board->count();

        $blocks = [];

        for($i=0; $i<$enemyMinionsCount; $i++) {
            $blocks[] = new SummonMinion($game->currentPlayer(), Hound::create());
        }

        return $blocks;
    }
}