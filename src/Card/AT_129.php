<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Fjola Lightbane
 */
class AT_129 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_129';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}