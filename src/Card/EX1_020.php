<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Scarlet Crusader
 */
class EX1_020 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_020';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}