<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Zombeast
 */
class ICC_828t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_828t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}