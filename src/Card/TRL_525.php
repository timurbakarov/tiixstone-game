<?php

namespace Tiixstone\Card;

/**
 * Arena Treasure Chest
 */
class TRL_525 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_525';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}