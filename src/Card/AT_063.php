<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Acidmaw
 */
class AT_063 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_063';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}