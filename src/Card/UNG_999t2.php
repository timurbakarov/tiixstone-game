<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Card\Alias\Plant;
use Tiixstone\Effect\DeathrattleEffect;
use Tiixstone\Game;

/**
 * Living Spores
 */
class UNG_999t2 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_999t2';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new GiveEffect($target, $this->livingSporesEffect())
        ];
    }

    /**
     * @return DeathrattleEffect
     */
    private function livingSporesEffect()
    {
        return new class extends DeathrattleEffect {

            /**
             * @param Game $game
             * @param Minion $minion
             * @param Minion|NULL $nextMinion
             * @return mixed
             */
            public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL): array
            {
                return [
                    new SummonMinion($minion->getPlayer(), Plant::create(), $nextMinion),
                    new SummonMinion($minion->getPlayer(), Plant::create(), $nextMinion),
                ];
            }
        };
    }
}