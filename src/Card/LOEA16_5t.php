<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mummy Zombie
 */
class LOEA16_5t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA16_5t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}