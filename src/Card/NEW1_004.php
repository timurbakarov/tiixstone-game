<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\ReturnMinionToHand;

/**
 * Vanish
 */
class NEW1_004 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_004';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 6;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $minions = $game->targetManager->allMinions($game);

        return array_map(function(Minion $minion) {
            return new ReturnMinionToHand($minion);
        }, $minions);
    }
}