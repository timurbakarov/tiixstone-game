<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * From the Swamp
 */
class GILA_BOSS_24p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_24p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}