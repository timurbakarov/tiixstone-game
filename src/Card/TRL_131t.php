<?php

namespace Tiixstone\Card;

/**
 * Zombie
 */
class TRL_131t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_131t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}