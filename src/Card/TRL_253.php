<?php

namespace Tiixstone\Card;

/**
 * Hir'eek, the Bat
 */
class TRL_253 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_253';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}