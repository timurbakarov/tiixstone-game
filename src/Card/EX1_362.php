<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Condition;
use Tiixstone\Battlecryable;
use Tiixstone\PlayCardCondition;

/**
 * Argent Protector
 */
class EX1_362 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_362';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Friendly,
            new Condition\Target\Minion,

        ]);
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        if(!$target) {
            return [];
        }

        return [new Block\GiveEffect($target, Effect\DivineShieldEffect::class)];
    }
}