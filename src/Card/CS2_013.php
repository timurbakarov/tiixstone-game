<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\PutCardInHand;
use Tiixstone\Block\SetManaCrystals;
use Tiixstone\Card\Alias\ExcessMana;

/**
 * Wild Growth
 */
class CS2_013 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_013';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $manaCrystals = $this->getPlayer()->maximumMana();
        $manaAvailable = $this->getPlayer()->availableMana();

        if($manaAvailable >= $game->settings->playerMaximumManaLimit()
            OR $manaCrystals >= $game->settings->playerMaximumManaLimit()) {
            return [new PutCardInHand($this->getPlayer(), ExcessMana::create())];
        }

        return [new SetManaCrystals($this->getPlayer(), $manaCrystals + 1)];
    }
}