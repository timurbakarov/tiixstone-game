<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Majordomo Executus
 */
class BRMA06_1H extends Hero
{
    public function globalId() : string
    {
        return 'BRMA06_1H';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}