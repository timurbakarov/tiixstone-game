<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Majordomo Executus
 */
class BRMA06_1 extends Hero
{
    public function globalId() : string
    {
        return 'BRMA06_1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}