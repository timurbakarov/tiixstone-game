<?php

namespace Tiixstone\Card;

/**
 * Belligerent Gnome
 */
class TRL_514 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_514';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}