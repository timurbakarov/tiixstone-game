<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Frostmourne
 */
class ICCA01_005 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA01_005';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}