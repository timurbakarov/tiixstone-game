<?php

namespace Tiixstone\Card;

/**
 * Woodchip
 */
class GIL_616t2 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_616t2';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}