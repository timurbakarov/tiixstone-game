<?php

namespace Tiixstone\Card;

use Tiixstone\Block\TakeDamage;
use Tiixstone\Game;

/**
 * Lightbomb
 */
class GVG_008 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_008';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 6;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $minions = $game->targetManager->allMinions($game);

        return array_map(function(Minion $minion) {
            return new TakeDamage($minion, $minion->attack->total(), $this);
        }, $minions);
    }
}