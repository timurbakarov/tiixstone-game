<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\PlayCardCondition;
use Tiixstone\Block\HealCharacter;

/**
 * Heal
 */
class AT_132_PRIEST extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'AT_132_PRIEST';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [
            new HealCharacter($target, 4, $this),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Character(),
        ]);
    }
}