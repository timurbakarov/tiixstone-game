<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Main Tank
 */
class TB_Coopv3_104 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_Coopv3_104';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}