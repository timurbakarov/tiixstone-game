<?php

namespace Tiixstone\Card;

use Tiixstone\Effect;

/**
 * Abomination
 */
class EX1_097 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_097';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [Effect\Deathrattle\AbominationDeathrattle::class, Effect\TauntEffect::class];
    }
}