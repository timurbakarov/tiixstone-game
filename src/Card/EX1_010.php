<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\StealthEffect;

/**
 * Worgen Infiltrator
 */
class EX1_010 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_010';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [StealthEffect::class];
    }
}