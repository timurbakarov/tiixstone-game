<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect\SpellDamageEffect;

/**
 * Malygos
 */
class EX1_563 extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::DRAGON;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_563';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 12;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [new SpellDamageEffect(5)];
    }
}