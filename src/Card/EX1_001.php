<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\Trigger\LightWardenEffect;

/**
 * Lightwarden
 */
class EX1_001 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_001';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [LightWardenEffect::class];
    }
}