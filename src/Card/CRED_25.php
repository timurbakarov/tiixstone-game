<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Elizabeth Cho
 */
class CRED_25 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_25';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}