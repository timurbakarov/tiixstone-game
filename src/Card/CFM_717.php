<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Jade Claws
 */
class CFM_717 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_717';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}