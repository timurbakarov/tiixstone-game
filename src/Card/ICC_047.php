<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Fatespinner
 */
class ICC_047 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_047';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}