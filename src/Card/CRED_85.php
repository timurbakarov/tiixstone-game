<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Pat Nagle
 */
class CRED_85 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_85';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}