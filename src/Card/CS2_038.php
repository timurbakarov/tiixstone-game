<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect\AncestralSpiritDeathrattle;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Ancestral Spirit
 */
class CS2_038 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_038';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new GiveEffect($target, AncestralSpiritDeathrattle::class)];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
        ]);
    }
}