<?php

namespace Tiixstone\Card;

use Tiixstone\Effect;

/**
 * Mirror Entity
 */
class EX1_294 extends MageSecret
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_294';
    }

    /**
     * @return mixed
     */
    public function effect(): Effect\Secret
    {
        return new Effect\Secret\MirrorEntityEffect;
    }
}