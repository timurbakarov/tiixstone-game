<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect;

/**
 * Flametongue Totem
 */
class EX1_565 extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::TOTEM;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_565';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [new Effect\AuraAttackChangerEffect(2, new Effect\Target\AdjacentTarget)];
    }
}