<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Derek Sakamoto
 */
class CRED_06 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_06';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}