<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Summoning Portal
 */
class EX1_315 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_315';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}