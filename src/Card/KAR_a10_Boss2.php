<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Black King
 */
class KAR_a10_Boss2 extends Hero
{
    public function globalId() : string
    {
        return 'KAR_a10_Boss2';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}