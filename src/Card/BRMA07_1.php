<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Highlord Omokk
 */
class BRMA07_1 extends Hero
{
    public function globalId() : string
    {
        return 'BRMA07_1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}