<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mind Control Tech
 */
class EX1_085 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_085';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}