<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Kill Objective: Anub'arak
 */
class HRW02_3 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'HRW02_3';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 100;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}