<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Spellslinger
 */
class AT_007 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_007';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}