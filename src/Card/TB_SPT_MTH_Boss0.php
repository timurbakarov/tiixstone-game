<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * City of Stormwind
 */
class TB_SPT_MTH_Boss0 extends Hero
{
    public function globalId() : string
    {
        return 'TB_SPT_MTH_Boss0';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}