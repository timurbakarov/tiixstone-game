<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Eggnapper
 */
class UNG_076 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_076';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}