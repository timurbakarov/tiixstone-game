<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Infested Wolf
 */
class OG_216 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_216';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}