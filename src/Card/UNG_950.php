<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Vinecleaver
 */
class UNG_950 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_950';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 7;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}