<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Dual Warglaives
 */
class TU4e_007 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TU4e_007';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 6;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}