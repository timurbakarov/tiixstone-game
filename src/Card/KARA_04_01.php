<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dorothee
 */
class KARA_04_01 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KARA_04_01';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}