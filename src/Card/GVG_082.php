<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Clockwork Gnome
 */
class GVG_082 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_082';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}