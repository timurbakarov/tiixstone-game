<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dragonkin Sorcerer
 */
class BRM_020 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_020';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}