<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * Wisps of the Old Gods
 */
class OG_195 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_195';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 7;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}