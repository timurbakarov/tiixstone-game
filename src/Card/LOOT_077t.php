<?php

namespace Tiixstone\Card;

/**
 * Wolf
 */
class LOOT_077t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOOT_077t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}