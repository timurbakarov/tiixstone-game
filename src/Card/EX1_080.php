<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Secretkeeper
 */
class EX1_080 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_080';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}