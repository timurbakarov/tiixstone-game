<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ancient Harbinger
 */
class OG_290 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_290';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}