<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Zap!
 */
class GIL_600 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_600';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}