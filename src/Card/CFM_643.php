<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hobart Grapplehammer
 */
class CFM_643 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_643';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}