<?php

namespace Tiixstone\Card;

/**
 * Arena Patron
 */
class TRL_521 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_521';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}