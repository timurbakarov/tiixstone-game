<?php

namespace Tiixstone\Card;

/**
 * Gloom Stag
 */
class GIL_130 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_130';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}