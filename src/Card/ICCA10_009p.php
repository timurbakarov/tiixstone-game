<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Whisper of Death
 */
class ICCA10_009p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'ICCA10_009p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}