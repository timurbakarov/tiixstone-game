<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jeweled Macaw
 */
class UNG_912 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_912';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}