<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * White Eyes
 */
class CFM_324 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_324';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}