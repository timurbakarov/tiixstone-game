<?php

namespace Tiixstone\Card;

/**
 * Gonk, the Raptor
 */
class TRL_241 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_241';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}