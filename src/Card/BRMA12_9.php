<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Chromatic Dragonkin
 */
class BRMA12_9 extends Hero
{
    public function globalId() : string
    {
        return 'BRMA12_9';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}