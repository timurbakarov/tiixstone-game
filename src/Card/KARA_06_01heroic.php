<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Romulo
 */
class KARA_06_01heroic extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KARA_06_01heroic';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}