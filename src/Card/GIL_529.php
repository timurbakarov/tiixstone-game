<?php

namespace Tiixstone\Card;

/**
 * Spellshifter
 */
class GIL_529 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_529';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}