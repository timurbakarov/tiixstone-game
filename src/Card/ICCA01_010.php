<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * A. F. Kay
 */
class ICCA01_010 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA01_010';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}