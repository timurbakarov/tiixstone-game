<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Corrupted Egg
 */
class BRMA10_4H extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA10_4H';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}