<?php

namespace Tiixstone\Card;

/**
 * Shudderwock
 */
class GIL_820 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_820';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}