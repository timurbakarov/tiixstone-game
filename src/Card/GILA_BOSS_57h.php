<?php

namespace Tiixstone\Card;

/**
 * Raeth Ghostsong
 */
class GILA_BOSS_57h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_57h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}