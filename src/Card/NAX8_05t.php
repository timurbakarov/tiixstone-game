<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Spectral Rider
 */
class NAX8_05t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX8_05t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}