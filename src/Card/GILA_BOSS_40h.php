<?php

namespace Tiixstone\Card;

/**
 * Splintergraft
 */
class GILA_BOSS_40h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_40h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}