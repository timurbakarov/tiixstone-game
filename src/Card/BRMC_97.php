<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Vaelastrasz
 */
class BRMC_97 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMC_97';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}