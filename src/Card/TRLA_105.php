<?php

namespace Tiixstone\Card;

/**
 * Shirvallah's Protection
 */
class TRLA_105 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_105';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}