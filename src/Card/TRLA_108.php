<?php

namespace Tiixstone\Card;

/**
 * Jan'alai's Mantle
 */
class TRLA_108 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_108';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}