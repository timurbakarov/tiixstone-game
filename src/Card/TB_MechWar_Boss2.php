<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Boom Bot
 */
class TB_MechWar_Boss2 extends Hero
{
    public function globalId() : string
    {
        return 'TB_MechWar_Boss2';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}