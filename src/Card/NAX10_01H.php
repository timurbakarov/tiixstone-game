<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Patchwerk
 */
class NAX10_01H extends Hero
{
    public function globalId() : string
    {
        return 'NAX10_01H';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}