<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jeremy Cranford
 */
class CRED_31 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_31';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}