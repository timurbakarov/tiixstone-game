<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Giantfin
 */
class LOEA16_24H extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA16_24H';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 10;
    }
}