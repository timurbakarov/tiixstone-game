<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Millhouse Manastorm
 */
class NEW1_029 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_029';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}