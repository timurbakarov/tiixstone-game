<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Whirling Ash
 */
class BRMC_89 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMC_89';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}