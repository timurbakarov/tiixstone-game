<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Nefarian
 */
class BRMA13_3 extends Hero
{
    public function globalId() : string
    {
        return 'BRMA13_3';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}