<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dispatch Kodo
 */
class CFM_335 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_335';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}