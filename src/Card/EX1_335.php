<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\LightspawnEffect;

/**
 * Lightspawn
 */
class EX1_335 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_335';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [LightspawnEffect::class];
    }
}