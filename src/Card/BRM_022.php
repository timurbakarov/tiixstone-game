<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\Trigger\DragonEggEffect;

/**
 * Dragon Egg
 */
class BRM_022 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_022';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [DragonEggEffect::class];
    }
}