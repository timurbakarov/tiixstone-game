<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Corpse Raiser
 */
class ICC_257 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_257';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}