<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Kel'Thuzad
 */
class FP1_013 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_013';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}