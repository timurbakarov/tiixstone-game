<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Henry Ho
 */
class CRED_27 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_27';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}