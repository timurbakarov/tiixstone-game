<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Faceless Behemoth
 */
class OG_141 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_141';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 10;
    }
}