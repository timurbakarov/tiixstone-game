<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Veteran's Militia Horn
 */
class GILA_852c extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_852c';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}