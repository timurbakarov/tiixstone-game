<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Blood Razor
 */
class ICC_064 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_064';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}