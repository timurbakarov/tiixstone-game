<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Burgly Bully
 */
class CFM_669 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_669';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}