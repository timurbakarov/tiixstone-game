<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Knife
 */
class KAR_A02_04H extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_A02_04H';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}