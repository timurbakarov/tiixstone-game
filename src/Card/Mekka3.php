<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Emboldener 3000
 */
class Mekka3 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'Mekka3';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}