<?php

namespace Tiixstone\Card;

/**
 * Mistwraith
 */
class GIL_510 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_510';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}