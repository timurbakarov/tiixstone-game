<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Second-Rate Bruiser
 */
class CFM_652 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_652';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}