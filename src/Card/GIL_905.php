<?php

namespace Tiixstone\Card;

/**
 * Carrion Drake
 */
class GIL_905 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_905';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}