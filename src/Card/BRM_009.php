<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\TauntEffect;
use Tiixstone\Effect\VolcanicLumbererEffect;

/**
 * Volcanic Lumberer
 */
class BRM_009 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_009';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [TauntEffect::class];
    }

    /**
     * @return array
     */
    public function handEffects()
    {
        return [VolcanicLumbererEffect::class];
    }
}