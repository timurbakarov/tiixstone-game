<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Zinaar
 */
class LOEA16_18H extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA16_18H';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 10;
    }
}