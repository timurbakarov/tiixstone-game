<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Wisp
 */
class OG_195c extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_195c';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}