<?php

namespace Tiixstone\Card;

/**
 * Azalina Soulthief
 */
class GILA_BOSS_55h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_55h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}