<?php

namespace Tiixstone\Card;

/**
 * Ice Cream Peddler
 */
class TRL_533 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_533';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}