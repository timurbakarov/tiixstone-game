<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tanaris Hogchopper
 */
class CFM_809 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_809';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}