<?php

namespace Tiixstone\Card;

/**
 * Griselda
 */
class GILA_BOSS_36h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_36h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}