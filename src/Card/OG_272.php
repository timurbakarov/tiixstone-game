<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Twilight Summoner
 */
class OG_272 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_272';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}