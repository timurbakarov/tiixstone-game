<?php

namespace Tiixstone\Card;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block\CastSpell;
use Tiixstone\Block\ChooseOne;

/**
 * Plague Lord
 */
class ICC_832p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'ICC_832p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [
            new ChooseOne(function(Spell $card) {
                return new CastSpell($card);
            }, ...[
                Card\Alias\SpiderFangs::create(),
                Card\Alias\ScarabShell::create(),
            ])
        ];
    }
}