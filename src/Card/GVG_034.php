<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mech-Bear-Cat
 */
class GVG_034 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_034';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}