<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hungry Naga
 */
class LOEA09_10 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA09_10';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}