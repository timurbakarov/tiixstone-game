<?php

namespace Tiixstone\Card;

/**
 * Vilebrood Skitterer
 */
class GIL_562 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_562';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}