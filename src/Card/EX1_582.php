<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;
use Tiixstone\Effect\SpellDamageEffect;

/**
 * Dalaran Mage
 */
class EX1_582 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_582';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     * @throws \Tiixstone\Exception
     */
    public function boardEffects()
    {
        return [new SpellDamageEffect(1)];
    }
}