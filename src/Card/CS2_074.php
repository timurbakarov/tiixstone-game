<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\Block\GiveEffect;
use Tiixstone\PlayCardCondition;
use Tiixstone\Effect\OneAttackEffect;

/**
 * Deadly Poison
 */
class CS2_074 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_074';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new GiveEffect($game->currentPlayer()->hero->weapon, OneAttackEffect::class),
            new GiveEffect($game->currentPlayer()->hero->weapon, OneAttackEffect::class),
        ];
    }

    /**
     * @return PlayCardCondition
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\FriendlyHeroHasWeapon(),
        ]);
    }
}