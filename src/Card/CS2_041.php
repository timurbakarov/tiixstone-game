<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Condition;
use Tiixstone\PlayCardCondition;

/**
 * Ancestral Healing
 */
class CS2_041 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_041';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $amount = $target->health->maximum->get();

        return [
            new Block\HealCharacter($target, $amount),
            new Block\GiveEffect($target, Effect\TauntEffect::class),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Minion,
        ]);
    }
}