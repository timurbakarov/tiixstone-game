<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Sergeant Sally
 */
class CFM_341 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_341';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}