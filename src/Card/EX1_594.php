<?php

namespace Tiixstone\Card;

use Tiixstone\Effect;

/**
 * Vaporize
 */
class EX1_594 extends MageSecret
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_594';
    }

    /**
     * @return mixed
     */
    public function effect(): Effect\Secret
    {
        return new Effect\Secret\VaporizeEffect();
    }
}