<?php

namespace Tiixstone\Card;

/**
 * Ornery Tortoise
 */
class TRL_546 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_546';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}