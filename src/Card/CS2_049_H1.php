<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;
use Tiixstone\Card\Alias\HealingTotem;
use Tiixstone\Card\Alias\SearingTotem;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Card\Alias\StoneclawTotem;
use Tiixstone\Card\Alias\WrathOfAirTotem;

/**
 * Totemic Call Skin
 */
class CS2_049_H1 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'CS2_049_H1';
    }
}