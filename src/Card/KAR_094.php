<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Deadly Fork
 */
class KAR_094 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_094';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}