<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Argent Horserider
 */
class AT_087 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_087';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}