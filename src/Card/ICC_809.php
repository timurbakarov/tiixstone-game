<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Plague Scientist
 */
class ICC_809 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_809';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}