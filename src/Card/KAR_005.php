<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Kindly Grandmother
 */
class KAR_005 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_005';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}