<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Brewmaster
 */
class TU4f_005 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TU4f_005';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}