<?php

namespace Tiixstone\Card;

use Tiixstone\Block\TakeDamage;
use Tiixstone\Game;

/**
 * Sinister Strike
 */
class CS2_075 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_075';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new TakeDamage($game->idlePlayer()->hero, $game->withSpellDamage(3), $this)];
    }
}