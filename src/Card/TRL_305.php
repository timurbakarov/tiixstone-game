<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * A New Challenger...
 */
class TRL_305 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_305';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 7;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}