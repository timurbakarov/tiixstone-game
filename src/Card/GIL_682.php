<?php

namespace Tiixstone\Card;

/**
 * Muck Hunter
 */
class GIL_682 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_682';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}