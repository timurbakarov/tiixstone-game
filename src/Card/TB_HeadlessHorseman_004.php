<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Captain Cookie
 */
class TB_HeadlessHorseman_004 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_HeadlessHorseman_004';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}