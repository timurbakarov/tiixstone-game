<?php

namespace Tiixstone\Card;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block\Trigger;
use Tiixstone\Block\CastSpell;
use Tiixstone\Block\ClosureBlock;
use Tiixstone\Event\AfterPlaySpell;
use Tiixstone\Event\BeforePlaySpell;

abstract class Spell extends Card
{
    final public function __construct()
    {
        parent::__construct();
    }

    abstract public function cast(Game $game, Character $target = null) : array;

    /**
     * @param Game $game
     * @param Minion|null $positionMinion
     * @param Character|null $target
     * @return array
     */
    public function play(Game $game, Minion $positionMinion = null, Character $target = null) : array
    {
        return [
            new Trigger(new BeforePlaySpell($this, $target)),
            new ClosureBlock(function() use($game, $target) {
                $game->logBlock('cast-spell', ['player' => $game->currentPlayer(), 'spell' => $this, 'target' => $target]);

                return [];
            }),
            new CastSpell($this, $target),
            new Trigger(new AfterPlaySpell($this, $target)),
        ];
    }
}