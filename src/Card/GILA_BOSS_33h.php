<?php

namespace Tiixstone\Card;

/**
 * The Scarecrow
 */
class GILA_BOSS_33h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_33h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}