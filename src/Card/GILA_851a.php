<?php

namespace Tiixstone\Card;

/**
 * Gilnean Tracker
 */
class GILA_851a extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_851a';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}