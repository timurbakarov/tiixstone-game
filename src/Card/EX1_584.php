<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ancient Mage
 */
class EX1_584 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_584';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}