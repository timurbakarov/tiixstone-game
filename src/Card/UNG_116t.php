<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Barnabus the Stomper
 */
class UNG_116t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_116t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}