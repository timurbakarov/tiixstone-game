<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Snap Freeze
 */
class GIL_801 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_801';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}