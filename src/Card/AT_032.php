<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Shady Dealer
 */
class AT_032 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_032';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}