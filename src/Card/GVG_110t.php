<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Boom Bot
 */
class GVG_110t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_110t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}