<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Hefty Sack of Coins
 */
class GILA_816c extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_816c';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}