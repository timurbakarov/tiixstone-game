<?php

namespace Tiixstone\Card;

/**
 * Sharkfin Fan
 */
class TRL_507 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_507';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}