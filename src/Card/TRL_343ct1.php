<?php

namespace Tiixstone\Card;

/**
 * Pterrordax
 */
class TRL_343ct1 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_343ct1';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}