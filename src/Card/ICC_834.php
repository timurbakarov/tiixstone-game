<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Scourgelord Garrosh
 */
class ICC_834 extends Hero
{
    public function globalId() : string
    {
        return 'ICC_834';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}