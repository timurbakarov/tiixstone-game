<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Dark Wanderer
 */
class TB_SPT_DPromo_Hero extends Hero
{
    public function globalId() : string
    {
        return 'TB_SPT_DPromo_Hero';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}