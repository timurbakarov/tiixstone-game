<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Cheap Shot
 */
class GIL_506 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_506';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}