<?php

namespace Tiixstone\Card;

/**
 * Curio Collector
 */
class GIL_640 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_640';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}