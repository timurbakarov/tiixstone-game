<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Skeleton
 */
class NAX4_03 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX4_03';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}