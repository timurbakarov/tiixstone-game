<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Stitched Tracker
 */
class ICC_415 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_415';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}