<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Noth the Plaguebringer
 */
class NAX4_01 extends Hero
{
    public function globalId() : string
    {
        return 'NAX4_01';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}