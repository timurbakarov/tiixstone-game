<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Lady Liadrin
 */
class HERO_04a extends Hero
{
    public function globalId() : string
    {
        return 'HERO_04a';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}