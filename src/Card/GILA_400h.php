<?php

namespace Tiixstone\Card;

/**
 * Houndmaster Shaw
 */
class GILA_400h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_400h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}