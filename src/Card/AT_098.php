<?php

namespace Tiixstone\Card;

use Tiixstone\Block\HeroPower\Replace;
use Tiixstone\Game;
use Tiixstone\Battlecryable;

/**
 * Sideshow Spelleater
 */
class AT_098 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_098';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        $power = $game->idlePlayer()->hero->power->copy();

        return [new Replace($game->currentPlayer(), $power)];
    }
}