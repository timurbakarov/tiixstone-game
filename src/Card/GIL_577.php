<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Rat Trap
 */
class GIL_577 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_577';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}