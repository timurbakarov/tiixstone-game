<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Kris Zierhut
 */
class CRED_49 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_49';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}