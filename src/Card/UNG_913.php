<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tol'vir Warden
 */
class UNG_913 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_913';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}