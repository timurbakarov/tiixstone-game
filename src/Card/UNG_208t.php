<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Rock Elemental
 */
class UNG_208t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_208t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}