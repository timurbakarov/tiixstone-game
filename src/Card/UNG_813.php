<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Stormwatcher
 */
class UNG_813 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_813';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}