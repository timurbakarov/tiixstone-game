<?php

namespace Tiixstone\Card;

/**
 * Experiment 3C
 */
class GILA_BOSS_27h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_27h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}