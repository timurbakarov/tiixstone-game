<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\WaterElementEffect;

/**
 * Water Elemental
 */
class ICC_833t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_833t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [WaterElementEffect::class];
    }
}