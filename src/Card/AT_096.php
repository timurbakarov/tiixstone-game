<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Clockwork Knight
 */
class AT_096 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_096';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}