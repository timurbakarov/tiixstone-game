<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Darkshire Councilman
 */
class OG_113 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_113';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}