<?php

namespace Tiixstone\Card;

/**
 * Raptor
 */
class TRL_254t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_254t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}