<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;
use Tiixstone\Effect\TauntEffect;
use Tiixstone\Effect\VoidlordEffect;
use Tiixstone\Race;

/**
 * Voidlord
 */
class LOOT_368 extends Minion
{
    protected $race = Race::DEMON;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOOT_368';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [
            TauntEffect::class,
            VoidlordEffect::class,
        ];
    }
}