<?php

namespace Tiixstone\Card;

use Tiixstone\Core\Game;
use Tiixstone\Core\Card\Power;
use Tiixstone\Core\Card\Character;

/**
 * Blackwing
 */
class BRMA09_4H extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'BRMA09_4H';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}