<?php

namespace Tiixstone\Card;

/**
 * Rikkar
 */
class TRLA_209h extends Hero
{
    public function globalId() : string
    {
        return 'TRLA_209h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}