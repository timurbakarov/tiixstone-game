<?php

namespace Tiixstone\Card;

use Tiixstone\Block\Transform;
use Tiixstone\Card\Alias\Frog;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Hex
 */
class EX1_246 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_246';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new Transform($target, Frog::create())];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
        ]);
    }
}