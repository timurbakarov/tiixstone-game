<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jomaro Kindred
 */
class CRED_33 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_33';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}