<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\ManaAddictEffect;

/**
 * Mana Addict
 */
class EX1_055 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_055';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [ManaAddictEffect::class];
    }
}