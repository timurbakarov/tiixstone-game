<?php

namespace Tiixstone\Card;

/**
 * Halazzi's Guise
 */
class TRLA_163t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_163t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}