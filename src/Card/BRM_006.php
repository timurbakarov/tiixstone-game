<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Imp Gang Boss
 */
class BRM_006 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_006';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}