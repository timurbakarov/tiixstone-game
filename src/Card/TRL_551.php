<?php

namespace Tiixstone\Card;

/**
 * Reckless Diretroll
 */
class TRL_551 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_551';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}