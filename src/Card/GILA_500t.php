<?php

namespace Tiixstone\Card;

/**
 * Stake Thrower
 */
class GILA_500t extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_500t';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 8;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}