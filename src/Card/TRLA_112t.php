<?php

namespace Tiixstone\Card;

/**
 * Gonk's Armament
 */
class TRLA_112t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_112t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}