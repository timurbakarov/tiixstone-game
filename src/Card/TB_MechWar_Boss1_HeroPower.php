<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Hello! Hello! Hello!
 */
class TB_MechWar_Boss1_HeroPower extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TB_MechWar_Boss1_HeroPower';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}