<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\TakeDamage;
use Tiixstone\PlayCardCondition;
use Tiixstone\Effect\LifestealEffect;

/**
 * Siphon Life
 */
class ICC_831p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'ICC_831p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [new TakeDamage($target, 3, $this)];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Character,
        ]);
    }

    /**
     * @return array
     */
    public function effects() : array
    {
        return [LifestealEffect::class];
    }
}