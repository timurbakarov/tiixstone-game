<?php

namespace Tiixstone\Card;

/**
 * Cheaty Anklebiter
 */
class TRL_512 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_512';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}