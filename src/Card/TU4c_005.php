<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hidden Gnome
 */
class TU4c_005 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TU4c_005';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}