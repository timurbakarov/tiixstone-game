<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Frozen Champion
 */
class ICC_838t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_838t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}