<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect\SilenceEffect;
use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\Battlecryable;
use Tiixstone\PlayCardCondition;
use Tiixstone\Block\SilenceMinion;

/**
 * Spellbreaker
 */
class EX1_048 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_048';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
	
	/**
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = null, Character $target = null)
    {
        $game->logBlock('spellbreaker-battlecry');
		
		if(!$target) {
			return [];
		}

        return [new GiveEffect($target, SilenceEffect::class)];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Minion,
        ]);
    }
}