<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Hammer of Twilight
 */
class OG_031 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_031';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 5;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}