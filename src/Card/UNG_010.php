<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Sated Threshadon
 */
class UNG_010 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_010';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}