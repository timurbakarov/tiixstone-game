<?php

namespace Tiixstone\Card;

/**
 * Akali's Champion
 */
class TRLA_104 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_104';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}