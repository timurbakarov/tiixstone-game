<?php

namespace Tiixstone\Card;

use Tiixstone\Effect;

/**
 * Stormwind Champion
 */
class CS2_222 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_222';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        $target = new Effect\Target\Aggregate([
            new Effect\Target\Friendly,
            new Effect\Target\Minion,
            new Effect\Target\NotSelfTarget,
            new Effect\Target\Battlefield(),
        ]);

        return [
            new Effect\AuraAttackChangerEffect(1, $target),
            new Effect\AuraHealthChangerEffect(1, $target),
        ];
    }
}