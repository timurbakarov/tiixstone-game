<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Aaron Gutierrez
 */
class CRED_79 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_79';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}