<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Alias\WickedKnife;
use Tiixstone\Block\Weapon\EquipWeapon;

/**
 * Dagger Mastery
 */
class CS2_083b extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'CS2_083b';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use(Game $game, Character $target = null)
    {
        $game->logBlock('dagger-mastery');

        return [new EquipWeapon($game->currentPlayer(), WickedKnife::create())];
    }
}