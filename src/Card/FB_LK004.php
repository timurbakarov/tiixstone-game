<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * Calling for Backup
 */
class FB_LK004 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FB_LK004';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 10;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}