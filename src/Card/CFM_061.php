<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jinyu Waterspeaker
 */
class CFM_061 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_061';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}