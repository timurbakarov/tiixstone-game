<?php

namespace Tiixstone\Card;

/**
 * Hagatha the Witch
 */
class GILA_BOSS_61h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_61h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}