<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Block\TakeDamage;
use Tiixstone\Effect\FreezeEffect;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Frostbolt
 */
class CS2_024 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_024';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new TakeDamage($target, $game->withSpellDamage(3)),
            new GiveEffect($target, FreezeEffect::class),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Character,
        ]);
    }
}