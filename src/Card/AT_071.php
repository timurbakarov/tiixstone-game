<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Alexstrasza's Champion
 */
class AT_071 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_071';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}