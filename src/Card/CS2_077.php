<?php

namespace Tiixstone\Card;

use Tiixstone\Block\DrawCard;
use Tiixstone\Game;

/**
 * Sprint
 */
class CS2_077 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_077';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 7;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new DrawCard($this->getPlayer(), 4)];
    }
}