<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Primordial Drake
 */
class UNG_848 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_848';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}