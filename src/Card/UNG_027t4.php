<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Pyros
 */
class UNG_027t4 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_027t4';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 10;
    }
}