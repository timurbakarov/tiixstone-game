<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;
use Tiixstone\Effect\LeokkAura;

/**
 * Leokk
 */
class LOEA02_10a extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA02_10a';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [LeokkAura::class];
    }
}