<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Ice Breaker
 */
class ICC_236 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_236';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}