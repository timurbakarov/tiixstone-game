<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Bewitch
 */
class GIL_504h extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GIL_504h';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}