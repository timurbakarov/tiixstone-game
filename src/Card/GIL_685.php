<?php

namespace Tiixstone\Card;

/**
 * Paragon of Light
 */
class GIL_685 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_685';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}