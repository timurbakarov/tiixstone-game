<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Usher of Souls
 */
class OG_302 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_302';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}