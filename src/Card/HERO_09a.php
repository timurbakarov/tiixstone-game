<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Tyrande Whisperwind
 */
class HERO_09a extends Hero
{
    public function globalId() : string
    {
        return 'HERO_09a';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}