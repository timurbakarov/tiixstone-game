<?php

namespace Tiixstone\Card;

/**
 * Shirvallah's Grace
 */
class TRLA_138t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_138t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}