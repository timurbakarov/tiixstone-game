<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Lord Slitherspear
 */
class LOEA09_1 extends Hero
{
    public function globalId() : string
    {
        return 'LOEA09_1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}