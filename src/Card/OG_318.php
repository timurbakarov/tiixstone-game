<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hogger, Doom of Elwynn
 */
class OG_318 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_318';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}