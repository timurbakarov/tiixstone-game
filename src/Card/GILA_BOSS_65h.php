<?php

namespace Tiixstone\Card;

/**
 * Gobbles
 */
class GILA_BOSS_65h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_65h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}