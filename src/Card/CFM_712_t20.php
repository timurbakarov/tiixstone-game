<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jade Golem
 */
class CFM_712_t20 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_712_t20';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 20;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 20;
    }
}