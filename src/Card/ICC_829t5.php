<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Darion Mograine
 */
class ICC_829t5 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_829t5';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}