<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ysera
 */
class EX1_572 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_572';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 12;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}