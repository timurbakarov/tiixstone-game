<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Giant Sand Worm
 */
class OG_308 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_308';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}