<?php

namespace Tiixstone\Card;

/**
 * Toxmonger
 */
class GIL_607 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_607';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}