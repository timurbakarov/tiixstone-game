<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\Transform;
use Tiixstone\PlayCardCondition;

/**
 * Polymorph: Boar
 */
class AT_005 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_005';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new Transform($target, new AT_005t)];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
        ]);
    }
}