<?php

namespace Tiixstone\Card;

/**
 * Lynx
 */
class TRL_348t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_348t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}