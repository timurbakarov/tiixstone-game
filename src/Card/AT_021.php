<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tiny Knight of Evil
 */
class AT_021 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_021';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}