<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Sword of Justice
 */
class EX1_366 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_366';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 5;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}