<?php

namespace Tiixstone\Card;

/**
 * Crooked Pete
 */
class GILA_BOSS_52h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_52h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}