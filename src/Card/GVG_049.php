<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Gahz'rilla
 */
class GVG_049 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_049';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}