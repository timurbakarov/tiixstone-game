<?php

namespace Tiixstone\Card;

/**
 * Marsh Drake
 */
class GIL_683 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_683';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}