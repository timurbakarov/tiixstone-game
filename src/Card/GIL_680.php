<?php

namespace Tiixstone\Card;

/**
 * Walnut Sprite
 */
class GIL_680 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_680';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}