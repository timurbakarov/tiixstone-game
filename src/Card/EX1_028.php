<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Stranglethorn Tiger
 */
class EX1_028 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_028';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}