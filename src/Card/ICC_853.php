<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Prince Valanar
 */
class ICC_853 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_853';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}