<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Kun the Forgotten King
 */
class CFM_308 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_308';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}