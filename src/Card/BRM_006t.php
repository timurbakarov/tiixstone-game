<?php

namespace Tiixstone\Card;

use Tiixstone\Race;

/**
 * Imp
 */
class BRM_006t extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::DEMON;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_006t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}