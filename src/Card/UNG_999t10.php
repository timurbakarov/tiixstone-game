<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect\StealthEffect;
use Tiixstone\Effect\Trigger\UntilYourNextTurn;

/**
 * Shrouding Mist
 */
class UNG_999t10 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_999t10';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new GiveEffect($target, new UntilYourNextTurn(new StealthEffect())),
        ];
    }
}