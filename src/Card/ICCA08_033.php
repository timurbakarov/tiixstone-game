<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Trapped Soul
 */
class ICCA08_033 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA08_033';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}