<?php

namespace Tiixstone\Card;

/**
 * Blood Troll Sapper
 */
class TRL_257 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_257';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}