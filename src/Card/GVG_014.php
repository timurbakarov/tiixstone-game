<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Vol'jin
 */
class GVG_014 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_014';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}