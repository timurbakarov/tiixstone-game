<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Needy Hunter
 */
class ICCA01_009 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA01_009';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}