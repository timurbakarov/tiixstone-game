<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\ChargeEffect;

/**
 * Reckless Rocketeer
 */
class CS2_213 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_213';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [ChargeEffect::class];
    }
}