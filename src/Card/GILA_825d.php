<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Gentlemanly Reequip Effect Dummy
 */
class GILA_825d extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_825d';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}