<?php

namespace Tiixstone\Card;

/**
 * Gloryseeker
 */
class TRLA_143 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_143';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}