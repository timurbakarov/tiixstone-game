<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\DrawCard;
use Tiixstone\Block\TakeDamage;

/**
 * Life Tap
 */
class CS2_056 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'CS2_056';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        $game->logBlock('life-tap');

        return [
            new TakeDamage($game->currentPlayer()->hero, $this->damage(), $this),
            new DrawCard($game->currentPlayer()),
        ];
    }

    /**
     * @return int
     */
    private function damage()
    {
        return 2;
    }
}