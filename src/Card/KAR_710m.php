<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Animated Shield
 */
class KAR_710m extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_710m';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}