<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\TauntEffect;
use Tiixstone\Effect\Deathrattle\TirionFordringDeathrattle;

/**
 * Tirion Fordring
 */
class EX1_383 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_383';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [TauntEffect::class, TirionFordringDeathrattle::class];
    }
}