<?php

namespace Tiixstone\Card;

/**
 * High Priest Thekal
 */
class TRLA_203h extends Hero
{
    public function globalId() : string
    {
        return 'TRLA_203h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}