<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\Trigger\GrimPatronEffect;

/**
 * Grim Patron
 */
class BRM_019 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_019';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [GrimPatronEffect::class];
    }
}