<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Kirin Tor Mage
 */
class EX1_612 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_612';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}