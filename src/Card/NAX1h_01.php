<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Anub'Rekhan
 */
class NAX1h_01 extends Hero
{
    public function globalId() : string
    {
        return 'NAX1h_01';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}