<?php

namespace Tiixstone\Card;

/**
 * Ironhide Runt
 */
class TRL_232t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_232t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}