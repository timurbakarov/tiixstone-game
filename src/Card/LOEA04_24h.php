<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Anubisath Temple Guard
 */
class LOEA04_24h extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA04_24h';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 15;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}