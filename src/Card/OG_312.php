<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * N'Zoth's First Mate
 */
class OG_312 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_312';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}