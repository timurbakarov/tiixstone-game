<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Battlecryable;

/**
 * Defender of Argus
 */
class EX1_093 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_093';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|null $positionMinion
     * @param Character|null $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = null, Character $target = null)
    {
        $blocks = [];

        $adjacentMinions = $game->targetManager->adjacentMinions($game, $attackable);

        foreach($adjacentMinions as $minion) {
            if(!($minion instanceof Minion)) {
                continue;
            }

            $blocks[] = new Block\GiveEffect($minion, new Effect\AttackChangerEffect(1));
            $blocks[] = new Block\GiveEffect($minion, new Effect\HealthChangerEffect(1));
            $blocks[] = new Block\GiveEffect($minion, Effect\TauntEffect::class);
        }

        return $blocks;
    }
}