<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Harrison Jones
 */
class EX1_558 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_558';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}