<?php

namespace Tiixstone\Card;

/**
 * Fiery War Axe
 */
class CS2_106 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_106';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}