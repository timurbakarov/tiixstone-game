<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Violet Teacher
 */
class NEW1_026 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_026';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}