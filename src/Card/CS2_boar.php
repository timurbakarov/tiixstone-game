<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Boar
 */
class CS2_boar extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_boar';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}