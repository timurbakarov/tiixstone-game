<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\ShadowboxerEffect;

/**
 * Shadowboxer
 */
class GVG_072 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_072';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [ShadowboxerEffect::class];
    }
}