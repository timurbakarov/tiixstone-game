<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Fel Reaver
 */
class GVG_016 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_016';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}