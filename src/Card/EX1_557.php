<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Nat Pagle
 */
class EX1_557 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_557';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}