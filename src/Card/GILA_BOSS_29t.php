<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Enchanting Tune
 */
class GILA_BOSS_29t extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_BOSS_29t';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}