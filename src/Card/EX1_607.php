<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Block\TakeDamage;
use Tiixstone\Effect\AttackChangerEffect;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Inner Rage
 */
class EX1_607 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_607';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new TakeDamage($target, 1, $this),
            new GiveEffect($target, new AttackChangerEffect(2)),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion(),
        ]);
    }
}