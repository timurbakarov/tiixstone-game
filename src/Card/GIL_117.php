<?php

namespace Tiixstone\Card;

/**
 * Worgen Abomination
 */
class GIL_117 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_117';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}