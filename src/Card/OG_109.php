<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Darkshire Librarian
 */
class OG_109 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_109';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}