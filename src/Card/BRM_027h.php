<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Ragnaros the Firelord
 */
class BRM_027h extends Hero
{
    public function globalId() : string
    {
        return 'BRM_027h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}