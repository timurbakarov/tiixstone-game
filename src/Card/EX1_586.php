<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\SeaGiantEffect;

/**
 * Sea Giant
 */
class EX1_586 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_586';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }

    /**
     * @return array
     */
    public function handEffects()
    {
        return [SeaGiantEffect::class];
    }
}