<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\Inspire\OrgrimmarAspirantEffect;

/**
 * Orgrimmar Aspirant
 */
class AT_066 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_066';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [OrgrimmarAspirantEffect::class];
    }
}