<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * King of Beasts
 */
class GVG_046 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_046';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}