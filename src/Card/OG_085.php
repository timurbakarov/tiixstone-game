<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Demented Frostcaller
 */
class OG_085 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_085';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}