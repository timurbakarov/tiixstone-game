<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Gadgetzan Jouster
 */
class AT_133 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_133';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}