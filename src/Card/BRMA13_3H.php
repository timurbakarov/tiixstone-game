<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Nefarian
 */
class BRMA13_3H extends Hero
{
    public function globalId() : string
    {
        return 'BRMA13_3H';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}