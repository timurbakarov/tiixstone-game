<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * King's Elekk
 */
class AT_058 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_058';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}