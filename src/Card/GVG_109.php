<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mini-Mage
 */
class GVG_109 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_109';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}