<?php

namespace Tiixstone\Card;

/**
 * Tribal Shrine
 */
class TRLA_107 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_107';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}