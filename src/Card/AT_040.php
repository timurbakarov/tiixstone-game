<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Wildwalker
 */
class AT_040 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_040';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}