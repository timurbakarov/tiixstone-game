<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Core Hound Pup
 */
class BRMC_95he extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMC_95he';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}