<?php

namespace Tiixstone\Card;

/**
 * Dragonmaw Scorcher
 */
class TRL_526 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_526';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}