<?php

namespace Tiixstone\Card;

/**
 * Crowskin Faithful
 */
class GILA_BOSS_49t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_BOSS_49t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}