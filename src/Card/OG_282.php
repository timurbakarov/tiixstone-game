<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Blade of C'Thun
 */
class OG_282 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_282';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}