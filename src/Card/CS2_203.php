<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Race;
use Tiixstone\Condition;
use Tiixstone\Battlecryable;
use Tiixstone\Block\GiveEffect;
use Tiixstone\PlayCardCondition;
use Tiixstone\Effect\SilenceEffect;

/**
 * Ironbeak Owl
 */
class CS2_203 extends Minion implements Battlecryable
{
    /**
     * @var int
     */
    protected $race = Race::BEAST;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_203';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = null, Character $target = null)
    {
        $game->logBlock('ironbeak-owl-battlecry');

        if(!$target) {
            return [];
        }

        return [new GiveEffect($target, SilenceEffect::class)];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Minion,
        ]);
    }
}