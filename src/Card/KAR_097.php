<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Medivh, the Guardian
 */
class KAR_097 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_097';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}