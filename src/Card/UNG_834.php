<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * Feeding Time
 */
class UNG_834 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_834';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 5;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}