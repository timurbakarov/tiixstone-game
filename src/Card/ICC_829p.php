<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * The Four Horsemen
 */
class ICC_829p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'ICC_829p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}