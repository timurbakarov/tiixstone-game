<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Spectral Trainee
 */
class NAX8_03t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX8_03t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}