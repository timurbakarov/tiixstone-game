<?php

namespace Tiixstone\Card;

use Tiixstone\Block\TakeDamage;
use Tiixstone\Condition\ClosureCondition;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Cleave
 */
class CS2_114 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_114';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $availableTargets = $game->targetManager->allEnemyMinions($game, $this->getPlayer());

        return array_map(function(Minion $minion) use($game) {
            return new TakeDamage($minion, $game->withSpellDamage(2), $this);
        }, $game->RNG->randomFromArray($availableTargets, 2));
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new ClosureCondition(function(Game $game) {
                return $this->getPlayer()->getOpponent($game)->board->count() >= 2;
            }, 'There should be two minions on enemy board at least'),
        ]);
    }
}