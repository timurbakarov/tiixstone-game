<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Baine Bloodhoof
 */
class EX1_110t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_110t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}