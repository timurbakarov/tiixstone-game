<?php

namespace Tiixstone\Card;

use Tiixstone\Block\HealCharacter;
use Tiixstone\Block\MarkDestroyed;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Siphon Soul
 */
class EX1_309 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_309';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 6;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new MarkDestroyed($target),
            new HealCharacter($game->currentPlayer()->hero, 3),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
        ]);
    }
}