<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Cornered Sentry
 */
class UNG_926 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_926';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}