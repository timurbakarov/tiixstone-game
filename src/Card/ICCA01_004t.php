<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ghoul
 */
class ICCA01_004t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA01_004t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}