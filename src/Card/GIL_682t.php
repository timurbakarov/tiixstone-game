<?php

namespace Tiixstone\Card;

/**
 * Muckling
 */
class GIL_682t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_682t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}