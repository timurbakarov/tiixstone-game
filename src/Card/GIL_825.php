<?php

namespace Tiixstone\Card;

/**
 * Lord Godfrey
 */
class GIL_825 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_825';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}