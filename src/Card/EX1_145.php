<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;
use Tiixstone\Block\GiveEffect;
use Tiixstone\Block\Effect\RemoveEffect;

/**
 * Preparation
 */
class EX1_145 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_145';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new GiveEffect($this->getPlayer()->hero, $this->effect()),
        ];
    }

    /**
     * @return Effect\AuraCostChangerEffect
     */
    private function effect()
    {
        $manaChange = -3;
        $targets = new Effect\Target\Aggregate([
            new Effect\Target\Friendly(),
            new Effect\Target\Hand(),
            new Effect\Target\SpellTarget(),
        ]);

        return new class($manaChange, $targets) extends Effect\AuraCostChangerEffect {

            /**
             * @return array
             */
            public function events()
            {
                return [
                    Event\BeforeEndTurn::class => 'beforeEndTurn',
                    Event\AfterPlaySpell::class => 'afterPlaySpell',
                ];
            }

            /**
             * @param Game $game
             * @param Event\BeforeEndTurn $event
             * @return array
             */
            public function beforeEndTurn(Game $game, Event\BeforeEndTurn $event)
            {
                return [new RemoveEffect([$this])];
            }

            /**
             * @param Game $game
             * @param Event\AfterPlaySpell $event
             * @return array
             */
            public function afterPlaySpell(Game $game, Event\AfterPlaySpell $event)
            {
                return [new RemoveEffect([$this])];
            }
        };
    }
}