<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Battlecryable;
use Tiixstone\Block\GiveEffect;
use Tiixstone\PlayCardCondition;
use Tiixstone\Block\MarkDestroyed;
use Tiixstone\Effect\Deathrattle\MoatLurkerDeathrattle;

/**
 * Moat Lurker
 */
class KAR_041 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_041';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        if(!$target) {
            return [];
        }

        return [
            new MarkDestroyed($target),
            new GiveEffect($this, new MoatLurkerDeathrattle($target)),
        ];
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [MoatLurkerDeathrattle::class];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
        ]);
    }
}