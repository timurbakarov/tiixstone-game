<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Terri Wellman
 */
class CRED_87 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_87';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}