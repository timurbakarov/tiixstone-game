<?php

namespace Tiixstone\Card;

/**
 * Treespeaker
 */
class TRL_341 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_341';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}