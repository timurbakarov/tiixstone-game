<?php

namespace Tiixstone\Card;

/**
 * Unbound Punisher
 */
class TRLA_177 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_177';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}