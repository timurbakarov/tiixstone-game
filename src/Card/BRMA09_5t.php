<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Gyth
 */
class BRMA09_5t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA09_5t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}