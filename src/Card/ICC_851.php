<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Prince Keleseth
 */
class ICC_851 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_851';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}