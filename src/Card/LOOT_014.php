<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Battlecryable;
use Tiixstone\Block\DrawCard;
use Tiixstone\Block\TakeDamage;

/**
 * Kobold Librarian
 */
class LOOT_014 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOOT_014';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = null, Character $target = null)
    {
        $game->logBlock('kobold-librarian');

        return [
            new DrawCard($game->currentPlayer()),
            new TakeDamage($game->currentPlayer()->hero, 2, $this),
        ];
    }
}