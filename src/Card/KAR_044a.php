<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Steward
 */
class KAR_044a extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_044a';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}