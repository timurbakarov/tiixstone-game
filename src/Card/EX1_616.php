<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\ManaWraithEffect;

/**
 * Mana Wraith
 */
class EX1_616 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_616';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [ManaWraithEffect::class];
    }
}