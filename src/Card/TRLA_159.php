<?php

namespace Tiixstone\Card;

/**
 * Croak Jouster
 */
class TRLA_159 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_159';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}