<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Lightning Jolt
 */
class AT_050t extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'AT_050t';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}