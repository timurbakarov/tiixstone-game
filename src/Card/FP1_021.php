<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Death's Bite
 */
class FP1_021 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_021';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}