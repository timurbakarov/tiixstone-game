<?php

namespace Tiixstone\Card;

/**
 * Masked Contender
 */
class TRL_530 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_530';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}