<?php

namespace Tiixstone\Card;

/**
 * Exactor of Justice
 */
class TRLA_144 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_144';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}