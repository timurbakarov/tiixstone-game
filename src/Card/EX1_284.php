<?php

namespace Tiixstone\Card;
use Tiixstone\Battlecryable;
use Tiixstone\Block\DrawCard;
use Tiixstone\Effect\SpellDamageEffect;
use Tiixstone\Game;

/**
 * Azure Drake
 */
class EX1_284 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_284';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }

    /**
     * @return array
     * @throws \Tiixstone\Exception
     */
    public function boardEffects()
    {
        return [new SpellDamageEffect(1)];
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        return [new DrawCard($attackable->getPlayer())];
    }
}