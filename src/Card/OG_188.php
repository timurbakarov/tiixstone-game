<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Klaxxi Amber-Weaver
 */
class OG_188 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_188';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}