<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Squirming Tentacle
 */
class OG_327 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_327';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}