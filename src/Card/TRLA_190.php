<?php

namespace Tiixstone\Card;

/**
 * Salty Looter
 */
class TRLA_190 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_190';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}