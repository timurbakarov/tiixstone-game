<?php

namespace Tiixstone\Card;

/**
 * Drakeslayer
 */
class GIL_683t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_683t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}