<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Atiesh
 */
class KAR_097t extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_097t';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}