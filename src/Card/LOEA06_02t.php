<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Earthen Statue
 */
class LOEA06_02t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA06_02t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}