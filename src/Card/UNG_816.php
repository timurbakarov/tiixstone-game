<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Servant of Kalimos
 */
class UNG_816 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_816';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}