<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Spellbender
 */
class tt_010a extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'tt_010a';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}