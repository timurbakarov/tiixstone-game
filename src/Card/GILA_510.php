<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Tools of the Trade
 */
class GILA_510 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_510';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}