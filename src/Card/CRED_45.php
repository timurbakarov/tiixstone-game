<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jonas Laster
 */
class CRED_45 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_45';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}