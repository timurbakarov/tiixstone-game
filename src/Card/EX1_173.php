<?php

namespace Tiixstone\Card;

use Tiixstone\Block\DrawCard;
use Tiixstone\Block\TakeDamage;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Starfire
 */
class EX1_173 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_173';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 6;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new TakeDamage($target, $game->withSpellDamage(5), $this),
            new DrawCard($this->getPlayer()),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Character,
        ]);
    }
}