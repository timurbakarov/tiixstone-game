<?php

namespace Tiixstone\Card;

/**
 * Manhunter Ivan
 */
class GILA_BOSS_25h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_25h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}