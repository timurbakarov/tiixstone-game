<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Frost Lich Jaina
 */
class ICC_833 extends Hero
{
    public function globalId() : string
    {
        return 'ICC_833';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}