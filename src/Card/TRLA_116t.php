<?php

namespace Tiixstone\Card;

/**
 * Bonds of Balance
 */
class TRLA_116t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_116t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}