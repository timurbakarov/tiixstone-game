<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * V-07-TR-0N
 */
class GVG_111t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_111t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}