<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\ChargeEffect;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Battlecryable;
use Tiixstone\Card\Alias\ClassicWhelp;

/**
 * Leeroy Jenkins
 */
class EX1_116 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_116';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [ChargeEffect::class];
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        return [
            new Block\SummonMinion($game->idlePlayer(), ClassicWhelp::create()),
            new Block\SummonMinion($game->idlePlayer(), ClassicWhelp::create()),
        ];
    }
}