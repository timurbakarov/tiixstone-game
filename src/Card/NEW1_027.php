<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Southsea Captain
 */
class NEW1_027 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_027';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}