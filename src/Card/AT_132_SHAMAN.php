<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\ChooseOne;
use Tiixstone\Block\SummonMinion;

/**
 * Totemic Slam
 */
class AT_132_SHAMAN extends CS2_049
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'AT_132_SHAMAN';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        $totems = $this->totemsNotOnBoard($game);

        if(count($totems) == 1) {
            $totem = array_shift($totems);

            return [new SummonMinion($game->currentPlayer(), new $totem)];
        } else {
            return [
                new ChooseOne(function(Minion $minion) use($game) {
                    return new SummonMinion($game->currentPlayer(), $minion);
                }, ...array_map(function($card) {
                        return new $card;
                    }, $totems)
                ),
            ];
        }
    }
}