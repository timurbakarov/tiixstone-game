<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Glacial Shard
 */
class UNG_205 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_205';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}