<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Deadly Arsenal
 */
class GIL_537 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_537';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 6;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}