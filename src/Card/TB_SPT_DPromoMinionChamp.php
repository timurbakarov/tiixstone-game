<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hell Bovine Champion
 */
class TB_SPT_DPromoMinionChamp extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_SPT_DPromoMinionChamp';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}