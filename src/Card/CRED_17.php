<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Rob Pardo
 */
class CRED_17 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_17';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 9;
    }
}