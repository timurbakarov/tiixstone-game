<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ethereal Arcanist
 */
class EX1_274 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_274';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}