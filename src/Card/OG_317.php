<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\Deathrattle\DeathwingDragonlordDeathrattle;
use Tiixstone\Race;

/**
 * Deathwing, Dragonlord
 */
class OG_317 extends Minion
{
    protected $race = Race::DRAGON;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_317';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 12;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 12;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [DeathwingDragonlordDeathrattle::class];
    }
}