<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Kalimos, Primal Lord
 */
class UNG_211 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_211';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}