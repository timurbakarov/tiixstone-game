<?php

namespace Tiixstone\Card;

/**
 * Gnomenapper
 */
class GILA_BOSS_26h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_26h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}