<?php

namespace Tiixstone\Card;

/**
 * The Walking Fort
 */
class TRLA_142 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_142';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}