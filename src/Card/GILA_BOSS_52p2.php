<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Beast Within
 */
class GILA_BOSS_52p2 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_52p2';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}