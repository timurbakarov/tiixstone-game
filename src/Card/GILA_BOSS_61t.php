<?php

namespace Tiixstone\Card;

/**
 * Ritual Dagger
 */
class GILA_BOSS_61t extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_BOSS_61t';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}