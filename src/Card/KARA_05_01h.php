<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Big Bad Wolf
 */
class KARA_05_01h extends Hero
{
    public function globalId() : string
    {
        return 'KARA_05_01h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}