<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Cursed Blade
 */
class LOE_118 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_118';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}