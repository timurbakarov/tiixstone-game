<?php

namespace Tiixstone\Card;

/**
 * Tess Greymane
 */
class GIL_598 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_598';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}