<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Laughing Sister
 */
class DREAM_01 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'DREAM_01';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}