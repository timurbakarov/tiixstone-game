<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Big Bad Claws
 */
class KARA_05_02heroic extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KARA_05_02heroic';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}