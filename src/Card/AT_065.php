<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * King's Defender
 */
class AT_065 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_065';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}