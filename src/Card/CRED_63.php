<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Evan Polekoff
 */
class CRED_63 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_63';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}