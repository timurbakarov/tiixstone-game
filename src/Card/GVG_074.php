<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Kezan Mystic
 */
class GVG_074 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_074';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}