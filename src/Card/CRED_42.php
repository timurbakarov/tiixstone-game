<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tim Erskine
 */
class CRED_42 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_42';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}