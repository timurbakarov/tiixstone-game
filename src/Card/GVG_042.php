<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Neptulon
 */
class GVG_042 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_042';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}