<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Stone Elemental
 */
class UNG_211aa extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_211aa';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}