<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Mad Science
 */
class ICCA07_002p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'ICCA07_002p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}