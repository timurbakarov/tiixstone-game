<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Zombeast
 */
class ICC_800h3t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_800h3t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}