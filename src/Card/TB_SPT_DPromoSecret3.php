<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * Visions of Valor
 */
class TB_SPT_DPromoSecret3 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_SPT_DPromoSecret3';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}