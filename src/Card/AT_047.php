<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Draenei Totemcarver
 */
class AT_047 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_047';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}