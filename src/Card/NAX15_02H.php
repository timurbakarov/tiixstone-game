<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Frost Blast
 */
class NAX15_02H extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'NAX15_02H';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}