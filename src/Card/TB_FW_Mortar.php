<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dwarf Demolitionist
 */
class TB_FW_Mortar extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_FW_Mortar';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}