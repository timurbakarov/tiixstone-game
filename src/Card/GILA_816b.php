<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Sack of Coins
 */
class GILA_816b extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_816b';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}