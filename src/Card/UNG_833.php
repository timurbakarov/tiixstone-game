<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Lakkari Felhound
 */
class UNG_833 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_833';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}