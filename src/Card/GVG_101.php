<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Scarlet Purifier
 */
class GVG_101 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_101';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}