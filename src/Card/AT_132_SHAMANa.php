<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;
use Tiixstone\Effect\HealingTotemEffect;

/**
 * Healing Totem
 */
class AT_132_SHAMANa extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_132_SHAMANa';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [HealingTotemEffect::class];
    }
}