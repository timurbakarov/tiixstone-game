<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Worshipper
 */
class NAX2_05 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX2_05';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}