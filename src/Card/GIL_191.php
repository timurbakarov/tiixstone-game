<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Fiendish Circle
 */
class GIL_191 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_191';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}