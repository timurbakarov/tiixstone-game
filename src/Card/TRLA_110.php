<?php

namespace Tiixstone\Card;

/**
 * Halazzi's Trap
 */
class TRLA_110 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_110';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}