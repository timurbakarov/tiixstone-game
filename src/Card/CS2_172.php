<?php

namespace Tiixstone\Card;

use Tiixstone\Race;

/**
 * Bloodfen Raptor
 */
class CS2_172 extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::BEAST;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_172';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}