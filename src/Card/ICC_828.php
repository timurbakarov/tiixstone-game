<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Deathstalker Rexxar
 */
class ICC_828 extends Hero
{
    public function globalId() : string
    {
        return 'ICC_828';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}