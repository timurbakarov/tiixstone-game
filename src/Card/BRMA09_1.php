<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Rend Blackhand
 */
class BRMA09_1 extends Hero
{
    public function globalId() : string
    {
        return 'BRMA09_1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}