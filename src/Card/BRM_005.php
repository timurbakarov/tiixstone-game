<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Manager\TargetManager;
use Tiixstone\Block\DealSplashDamage;

/**
 * Demonwrath
 */
class BRM_005 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_005';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new DealSplashDamage(
            TargetManager::ANY_MINION,
            $game->withSpellDamage(2),
            $this,
            new Effect\Target\Not(new Effect\Target\Race(\Tiixstone\Race::demon()))
        )];
    }
}