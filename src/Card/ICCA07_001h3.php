<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Professor Putricide
 */
class ICCA07_001h3 extends Hero
{
    public function globalId() : string
    {
        return 'ICCA07_001h3';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}