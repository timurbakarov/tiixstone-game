<?php

namespace Tiixstone\Card;

/**
 * Ravencaller Cozzlewurt
 */
class GILA_BOSS_38h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_38h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}