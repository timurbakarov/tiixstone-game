<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Mrglmrgl MRGL!
 */
class LOEA10_2 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'LOEA10_2';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}