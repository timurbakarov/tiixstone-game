<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Corpsetaker
 */
class ICC_912 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_912';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}