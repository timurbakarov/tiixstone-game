<?php

namespace Tiixstone\Card;

use Tiixstone\Battlecryable;
use Tiixstone\Block\RandomMissilesDamage;
use Tiixstone\Game;
use Tiixstone\Manager\TargetManager;
use Tiixstone\PlayCardCondition;
use Tiixstone\Condition\Target\Enemy;

/**
 * Bomb Lobber
 */
class GVG_099 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_099';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        return [new RandomMissilesDamage(TargetManager::ANY_OPPONENT_MINION, 4, 1, $this)];
    }
}