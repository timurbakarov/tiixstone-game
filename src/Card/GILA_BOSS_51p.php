<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Death and Taxes
 */
class GILA_BOSS_51p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_51p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}