<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Shaku, the Collector
 */
class CFM_781 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_781';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}