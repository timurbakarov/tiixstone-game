<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Lunar Signet
 */
class GILA_415 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_415';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}