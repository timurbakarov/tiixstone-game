<?php

namespace Tiixstone\Card;

/**
 * Assassin's Blade
 */
class CS2_080 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_080';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 5;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}