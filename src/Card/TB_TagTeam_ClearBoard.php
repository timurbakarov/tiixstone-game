<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * Your Next Victim Comes
 */
class TB_TagTeam_ClearBoard extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_TagTeam_ClearBoard';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 10;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}