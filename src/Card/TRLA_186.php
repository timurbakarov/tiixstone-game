<?php

namespace Tiixstone\Card;

/**
 * Treasure from Below
 */
class TRLA_186 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_186';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}