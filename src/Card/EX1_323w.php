<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Blood Fury
 */
class EX1_323w extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_323w';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 8;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}