<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\GiveEffect;
use Tiixstone\PlayCardCondition;
use Tiixstone\Effect\AttackChangerEffect;

/**
 * Mark of Nature
 */
class EX1_155a extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_155a';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new GiveEffect($target, new AttackChangerEffect(4)),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion(),
        ]);
    }
}