<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Corrupted Healbot
 */
class OG_147 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_147';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}