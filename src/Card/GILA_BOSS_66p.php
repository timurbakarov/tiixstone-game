<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * . . .
 */
class GILA_BOSS_66p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_66p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}