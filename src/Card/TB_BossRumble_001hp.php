<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Smuggle
 */
class TB_BossRumble_001hp extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TB_BossRumble_001hp';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}