<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Crystalline Oracle
 */
class UNG_032 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_032';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}