<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Patches the Pirate
 */
class CFM_637 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_637';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}