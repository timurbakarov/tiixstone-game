<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Spiked Decoy
 */
class LOEA07_24 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA07_24';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}