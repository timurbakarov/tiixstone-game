<?php

namespace Tiixstone\Card;

/**
 * Treasure from Below
 */
class TRLA_186t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_186t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}