<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\TauntEffect;

/**
 * Sen'jin Shieldmasta
 */
class CS2_179 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_179';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [TauntEffect::class];
    }
}