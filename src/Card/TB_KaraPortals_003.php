<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Party Elemental
 */
class TB_KaraPortals_003 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_KaraPortals_003';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}