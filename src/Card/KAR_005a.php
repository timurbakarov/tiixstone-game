<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Big Bad Wolf
 */
class KAR_005a extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_005a';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}