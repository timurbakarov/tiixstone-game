<?php

namespace Tiixstone\Card;

/**
 * Serpent Ward
 */
class TRL_057 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_057';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}