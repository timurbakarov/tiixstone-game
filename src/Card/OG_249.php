<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Infested Tauren
 */
class OG_249 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_249';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}