<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\Block\TakeDamage;
use Tiixstone\PlayCardCondition;

/**
 * Holy Smite
 */
class CS1_130 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS1_130';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $game->logBlock('holy-smite');

        return [new TakeDamage($target, $game->withSpellDamage($this->damage()))];
    }

    /**
     * @return int
     */
    private function damage() : int
    {
        return 2;
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Character,
        ]);
    }
}