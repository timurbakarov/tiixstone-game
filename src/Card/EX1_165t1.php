<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Druid of the Claw
 */
class EX1_165t1 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_165t1';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}