<?php

namespace Tiixstone\Card;

/**
 * Groddo the Bogwarden
 */
class GILA_BOSS_24h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_24h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}