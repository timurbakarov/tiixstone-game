<?php

namespace Tiixstone\Card;

/**
 * Sazzmi Gentlehorn
 */
class GILA_BOSS_29h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_29h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}