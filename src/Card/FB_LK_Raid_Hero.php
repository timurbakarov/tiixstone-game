<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Supreme Lich King
 */
class FB_LK_Raid_Hero extends Hero
{
    public function globalId() : string
    {
        return 'FB_LK_Raid_Hero';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}