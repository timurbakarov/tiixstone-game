<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Treant
 */
class EX1_158t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_158t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}