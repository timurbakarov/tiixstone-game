<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Fire!
 */
class GILA_600p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_600p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}