<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Eternal Sentinel
 */
class OG_026 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_026';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}