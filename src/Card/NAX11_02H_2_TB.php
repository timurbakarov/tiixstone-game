<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Poison Cloud
 */
class NAX11_02H_2_TB extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'NAX11_02H_2_TB';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}