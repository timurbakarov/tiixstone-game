<?php

namespace Tiixstone\Card;

/**
 * Houndmaster Shaw
 */
class GIL_650 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_650';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}