<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Brazier
 */
class TB_FireFestival_Brazier extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_FireFestival_Brazier';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}