<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * The Steel Sentinel
 */
class LOEA14_1 extends Hero
{
    public function globalId() : string
    {
        return 'LOEA14_1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}