<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Direhorn Matriarch
 */
class UNG_957t1 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_957t1';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}