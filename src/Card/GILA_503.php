<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Hunter's Insight
 */
class GILA_503 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_503';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}