<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Mass Hysteria
 */
class TRL_258 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_258';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 5;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}