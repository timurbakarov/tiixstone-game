<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\Adapt;
use Tiixstone\Battlecryable;
use Tiixstone\PlayCardCondition;
use Tiixstone\Condition\Target\Race;
use Tiixstone\Condition\Target\Friendly;

/**
 * Crackling Razormaw
 */
class UNG_915 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_915';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL|Minion $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        return [new Adapt($target)];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Friendly(),
            new \Tiixstone\Condition\Target\Minion(),
            new Race(\Tiixstone\Race::beast()),
        ]);
    }
}