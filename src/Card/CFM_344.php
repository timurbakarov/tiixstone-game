<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Finja, the Flying Star
 */
class CFM_344 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_344';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}