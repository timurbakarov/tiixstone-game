<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Slime
 */
class OG_202c extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_202c';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}