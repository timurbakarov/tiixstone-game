<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Christopher Yim
 */
class CRED_23 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_23';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}