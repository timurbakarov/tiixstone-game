<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Josh Durica
 */
class CRED_83 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_83';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}