<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Stats\Armor;
use Tiixstone\Stats\Health;
use Tiixstone\Manager\TargetManager;

abstract class Character extends Attackable
{
    /**
     * @var Health
     */
    public $health;

    /**
     * @var Armor
     */
    public $armor;

    /**
     * @var
     */
    protected $race = Race::GENERAL;

    /**
     * @var array
     */
    private $effects = [];

    /**
     * @var int
     */
    protected $attackTarget = TargetManager::ANY_OPPONENT_CHARACTER;

    public function __construct()
    {
        parent::__construct();

        $this->health = new Health($this->defaultMaximumHealth());
        $this->armor = new Armor(0);
        $this->race = new Race($this->race);
    }

    /**
     * @return int
     */
    abstract public function defaultMaximumHealth() : int;

    /**
     * @return bool
     */
    public function isDestroyed() : bool
    {
        return $this->destroyed || $this->health->isDead();
    }

    /**
     * @return Race
     */
    final public function race()
    {
        return $this->race;
    }

    /**
     * @param string $effectName
     * @return $this
     */
    final public function addEffect(string $effectName)
    {
        $this->effects[] = $effectName;

        return $this;
    }

    /**
     * @param string $effectName
     * @return bool
     */
    public function hasEffect(string $effectName) : bool
    {
        return in_array($effectName, $this->effects);
    }

    /**
     * @return array
     */
    public function effects()
    {
        return $this->effects;
    }

    /**
     * @return $this
     */
    public function flushEffects()
    {
        $this->effects = [];

        return $this;
    }
}