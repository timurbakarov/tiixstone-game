<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Instructor Razuvious
 */
class NAX7_01H extends Hero
{
    public function globalId() : string
    {
        return 'NAX7_01H';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}