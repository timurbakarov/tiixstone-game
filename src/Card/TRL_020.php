<?php

namespace Tiixstone\Card;

/**
 * Sightless Ranger
 */
class TRL_020 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_020';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}