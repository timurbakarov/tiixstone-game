<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Nerubian
 */
class FP1_007t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_007t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}