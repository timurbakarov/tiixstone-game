<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Toxitron
 */
class BRMA14_5H extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA14_5H';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}