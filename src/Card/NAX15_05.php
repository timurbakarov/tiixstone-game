<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mr. Bigglesworth
 */
class NAX15_05 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX15_05';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}