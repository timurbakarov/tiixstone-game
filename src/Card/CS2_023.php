<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\DrawCard;

/**
 * Arcane Intellect
 */
class CS2_023 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_023';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new DrawCard($game->currentPlayer(), 2),
        ];
    }
}