<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Flee the Mine!
 */
class LOEA07_03 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'LOEA07_03';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}