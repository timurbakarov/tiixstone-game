<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Grim Necromancer
 */
class ICC_026 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_026';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}