<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Intense Gaze
 */
class BRMA08_2 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'BRMA08_2';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}