<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Onyxia
 */
class BRMA17_3H extends Hero
{
    public function globalId() : string
    {
        return 'BRMA17_3H';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}