<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Stampeding Roar
 */
class TRL_255 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_255';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 6;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}