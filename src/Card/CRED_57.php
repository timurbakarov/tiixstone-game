<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Matt Wyble
 */
class CRED_57 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_57';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}