<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\LeokkAura;

/**
 * Leokk
 */
class NEW1_033 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_033';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [LeokkAura::class];
    }
}