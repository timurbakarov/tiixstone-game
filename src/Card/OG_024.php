<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Flamewreathed Faceless
 */
class OG_024 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_024';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}