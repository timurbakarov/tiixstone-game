<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Illidan Stormrage
 */
class TU4e_001 extends Hero
{
    public function globalId() : string
    {
        return 'TU4e_001';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}