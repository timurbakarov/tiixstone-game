<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Demolisher
 */
class EX1_102 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_102';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}