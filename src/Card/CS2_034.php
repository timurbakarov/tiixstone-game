<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\Block\TakeDamage;
use Tiixstone\PlayCardCondition;

/**
 * Fireblast
 */
class CS2_034 extends Power
{
    const DAMAGE = 1;

    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'CS2_034';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [new TakeDamage($target, self::DAMAGE, $this)];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Character,
        ]);
    }
}