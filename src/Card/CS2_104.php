<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\Block\GiveEffect;
use Tiixstone\PlayCardCondition;
use Tiixstone\Effect\AttackChangerEffect;
use Tiixstone\Effect\HealthChangerEffect;

/**
 * Rampage
 */
class CS2_104 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_104';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new GiveEffect($target, new AttackChangerEffect(3)),
            new GiveEffect($target, new HealthChangerEffect(3)),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Minion,
            new Condition\Target\Damaged,
        ]);
    }
}