<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Atiesh
 */
class KARA_13_26 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KARA_13_26';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}