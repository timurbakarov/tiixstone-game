<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Totemic Smash
 */
class TRL_012 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_012';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}