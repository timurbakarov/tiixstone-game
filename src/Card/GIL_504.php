<?php

namespace Tiixstone\Card;

/**
 * Hagatha the Witch
 */
class GIL_504 extends Hero
{
    public function globalId() : string
    {
        return 'GIL_504';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}