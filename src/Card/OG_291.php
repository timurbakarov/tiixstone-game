<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Shadowcaster
 */
class OG_291 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_291';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}