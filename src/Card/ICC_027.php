<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Bone Drake
 */
class ICC_027 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_027';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}