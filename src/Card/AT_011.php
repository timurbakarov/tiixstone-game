<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Holy Champion
 */
class AT_011 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_011';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}