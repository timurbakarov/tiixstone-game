<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ravenholdt Assassin
 */
class CS2_161 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_161';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}