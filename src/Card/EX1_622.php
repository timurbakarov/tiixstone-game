<?php

namespace Tiixstone\Card;

use Tiixstone\Block\MarkDestroyed;
use Tiixstone\Condition\Target\AttackGreaterThan;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Shadow Word: Death
 */
class EX1_622 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_622';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new MarkDestroyed($target)];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
           new \Tiixstone\Condition\Target\Minion,
           new AttackGreaterThan(4),
        ]);
    }
}