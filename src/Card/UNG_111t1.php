<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mana Treant
 */
class UNG_111t1 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_111t1';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}