<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mysterious Challenger
 */
class AT_079 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_079';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}