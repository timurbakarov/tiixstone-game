<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Chris Belcher
 */
class CRED_69 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_69';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}