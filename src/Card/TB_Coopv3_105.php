<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Raid Healer
 */
class TB_Coopv3_105 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_Coopv3_105';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}