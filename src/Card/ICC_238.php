<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Animated Berserker
 */
class ICC_238 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_238';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}