<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Skelesaurus Hex
 */
class LOEA16_26 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA16_26';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}