<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Unlicensed Apothecary
 */
class CFM_900 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_900';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}