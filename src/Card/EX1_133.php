<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Perdition's Blade
 */
class EX1_133 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_133';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}