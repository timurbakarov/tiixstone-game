<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mekgineer Thermaplugg
 */
class GVG_116 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_116';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 9;
    }
}