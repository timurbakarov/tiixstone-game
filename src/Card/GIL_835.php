<?php

namespace Tiixstone\Card;

/**
 * Squashling
 */
class GIL_835 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_835';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}