<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Roaring Edifice
 */
class TRLA_808 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_808';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}