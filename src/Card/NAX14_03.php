<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Frozen Champion
 */
class NAX14_03 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX14_03';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}