<?php

namespace Tiixstone\Card;

/**
 * Shieldbreaker
 */
class TRL_524 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_524';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}