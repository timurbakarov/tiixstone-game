<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Plate
 */
class KAR_A02_01 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_A02_01';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}