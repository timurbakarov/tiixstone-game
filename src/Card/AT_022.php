<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\FistOfJaraxxusEffect;
use Tiixstone\Game;
use Tiixstone\Manager\TargetManager;
use Tiixstone\Block\Damage\DealRandomDamage;

/**
 * Fist of Jaraxxus
 */
class AT_022 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_022';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new DealRandomDamage(TargetManager::ANY_OPPONENT_CHARACTER, $game->withSpellDamage(4), $this)];
    }

    /**
     * @return array
     */
    public function handEffects()
    {
        return [FistOfJaraxxusEffect::class];
    }
}