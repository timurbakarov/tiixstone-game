<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * The Steel Sentinel
 */
class LOEA16_27H extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA16_27H';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 10;
    }
}