<?php

namespace Tiixstone\Card;

/**
 * Handgonne
 */
class GILA_500t2 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_500t2';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}