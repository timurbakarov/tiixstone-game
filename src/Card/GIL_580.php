<?php

namespace Tiixstone\Card;

/**
 * Town Crier
 */
class GIL_580 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_580';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}