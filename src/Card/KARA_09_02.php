<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Curator
 */
class KARA_09_02 extends Hero
{
    public function globalId() : string
    {
        return 'KARA_09_02';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}