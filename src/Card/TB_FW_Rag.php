<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Ragnaros the Firelord
 */
class TB_FW_Rag extends Hero
{
    public function globalId() : string
    {
        return 'TB_FW_Rag';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}