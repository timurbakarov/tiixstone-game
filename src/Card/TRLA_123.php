<?php

namespace Tiixstone\Card;

/**
 * Succoring Skyscreamer
 */
class TRLA_123 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_123';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}