<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\CostChangerEffect;
use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;
use Tiixstone\Block\GiveEffect;
use Tiixstone\Manager\TargetManager;
use Tiixstone\Effect\ShadowstepEffect;
use Tiixstone\Block\ReturnMinionToHand;
use Tiixstone\PlayCardCondition;
use Tiixstone\Condition;

/**
 * Shadowstep
 */
class EX1_144 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_144';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new ReturnMinionToHand($target),
            new GiveEffect($target, new CostChangerEffect(-2)),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Minion,
            new Condition\Target\Friendly,
        ]);
    }
}