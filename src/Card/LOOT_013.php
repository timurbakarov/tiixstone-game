<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Race;
use Tiixstone\Effect;
use Tiixstone\Battlecryable;
use Tiixstone\Block\TakeDamage;

/**
 * Vulgar Homunculus
 */
class LOOT_013 extends Minion implements Battlecryable
{
    /**
     * @var int
     */
    protected $race = Race::DEMON;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOOT_013';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [Effect\TauntEffect::class];
    }


    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        return [new TakeDamage($game->currentPlayer()->hero, 2, $this)];
    }
}