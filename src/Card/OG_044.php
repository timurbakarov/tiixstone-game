<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Fandral Staghelm
 */
class OG_044 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_044';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}