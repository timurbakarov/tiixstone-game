<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Troggzor the Earthinator
 */
class GVG_118 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_118';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}