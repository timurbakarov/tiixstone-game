<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Naga Myrmidon
 */
class TU4e_003 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TU4e_003';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}