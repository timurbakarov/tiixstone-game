<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mad Bomber
 */
class EX1_082 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_082';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}