<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Crystalweaver
 */
class CFM_610 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_610';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}