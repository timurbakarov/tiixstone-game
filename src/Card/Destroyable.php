<?php

namespace Tiixstone\Card;

use Tiixstone\Card;
use Tiixstone\Game;

abstract class Destroyable extends Card
{
    /**
     * @var bool
     */
    protected $destroyed;

    /**
     * @param Game $game
     * @return array
     */
    abstract public function destroy(Game $game) : array;

    /**
     * @return bool
     */
    abstract public function isDestroyed() : bool;

    /**
     * @return $this
     */
    public function markDestroyed() : self
    {
        $this->destroyed = true;

        return $this;
    }
}