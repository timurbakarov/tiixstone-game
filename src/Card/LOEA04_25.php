<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Seething Statue
 */
class LOEA04_25 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA04_25';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}