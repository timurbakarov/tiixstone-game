<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Khadgar
 */
class HERO_08b extends Hero
{
    public function globalId() : string
    {
        return 'HERO_08b';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}