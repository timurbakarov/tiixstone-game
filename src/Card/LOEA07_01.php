<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Mine Cart
 */
class LOEA07_01 extends Hero
{
    public function globalId() : string
    {
        return 'LOEA07_01';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}