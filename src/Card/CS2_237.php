<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\Trigger\StarvingBuzzardEffect;

/**
 * Starving Buzzard
 */
class CS2_237 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_237';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [StarvingBuzzardEffect::class];
    }
}