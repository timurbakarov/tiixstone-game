<?php

namespace Tiixstone\Card;

/**
 * Regeneratin' Thug
 */
class TRL_508 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_508';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}