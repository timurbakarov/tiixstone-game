<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Headless Horseman (no head)
 */
class TB_HeadlessHorseman_H1a extends Hero
{
    public function globalId() : string
    {
        return 'TB_HeadlessHorseman_H1a';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}