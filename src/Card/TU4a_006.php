<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Jaina Proudmoore
 */
class TU4a_006 extends Hero
{
    public function globalId() : string
    {
        return 'TU4a_006';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}