<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Selfless Hero
 */
class OG_221 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_221';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}