<?php

namespace Tiixstone\Card;

/**
 * Shaw's Shank
 */
class GILA_413 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_413';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 5;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}