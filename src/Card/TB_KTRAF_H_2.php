<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Rafaam
 */
class TB_KTRAF_H_2 extends Hero
{
    public function globalId() : string
    {
        return 'TB_KTRAF_H_2';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}