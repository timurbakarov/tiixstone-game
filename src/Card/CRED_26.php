<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Eric Del Priore
 */
class CRED_26 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_26';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}