<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Remorseless Winter
 */
class FB_LK005 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'FB_LK005';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}