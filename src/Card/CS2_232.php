<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\TauntEffect;

/**
 * Ironbark Protector
 */
class CS2_232 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_232';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [TauntEffect::class];
    }
}