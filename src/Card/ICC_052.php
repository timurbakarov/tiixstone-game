<?php

namespace Tiixstone\Card;

use Tiixstone\Block\TriggerDeathrattle;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;
use Tiixstone\Effect\DeathrattleEffect;
use Tiixstone\Condition\Target\Friendly;
use Tiixstone\Condition\Target\TargetHasEffect;

/**
 * Play Dead
 */
class ICC_052 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_052';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null|Minion $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new TriggerDeathrattle($target),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Friendly(),
            new \Tiixstone\Condition\Target\Minion(),
            new TargetHasEffect(DeathrattleEffect::class),
        ]);
    }
}