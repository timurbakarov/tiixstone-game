<?php

namespace Tiixstone\Card;

use Tiixstone\Block\CheckDestroyed;
use Tiixstone\Block\MarkDestroyed;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Reincarnate
 */
class FP1_025 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_025';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $copy = $target->copy();

        return [
            new MarkDestroyed($target),
            new CheckDestroyed(),
            new SummonMinion($target->getPlayer(), $copy),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
        ]);
    }
}