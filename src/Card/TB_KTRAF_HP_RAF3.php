<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Staff, First Piece
 */
class TB_KTRAF_HP_RAF3 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TB_KTRAF_HP_RAF3';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}