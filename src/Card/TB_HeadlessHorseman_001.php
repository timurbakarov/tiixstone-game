<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Headless Horseman's Head
 */
class TB_HeadlessHorseman_001 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_HeadlessHorseman_001';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 35;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}