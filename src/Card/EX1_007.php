<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\AcolyteOfPainEffect;

/**
 * Acolyte of Pain
 */
class EX1_007 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_007';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [AcolyteOfPainEffect::class];
    }
}