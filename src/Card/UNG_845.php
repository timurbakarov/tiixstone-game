<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Igneous Elemental
 */
class UNG_845 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_845';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}