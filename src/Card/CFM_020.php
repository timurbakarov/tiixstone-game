<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Raza the Chained
 */
class CFM_020 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_020';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}