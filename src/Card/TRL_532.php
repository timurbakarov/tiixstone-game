<?php

namespace Tiixstone\Card;

/**
 * Mosh'Ogg Announcer
 */
class TRL_532 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_532';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}