<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dragon Consort
 */
class BRM_018 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_018';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}