<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Sindragosa
 */
class ICCA04_001 extends Hero
{
    public function globalId() : string
    {
        return 'ICCA04_001';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}