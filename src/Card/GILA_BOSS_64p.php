<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Hypnotize
 */
class GILA_BOSS_64p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_64p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}