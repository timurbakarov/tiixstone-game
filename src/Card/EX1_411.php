<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Gorehowl
 */
class EX1_411 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_411';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 7;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}