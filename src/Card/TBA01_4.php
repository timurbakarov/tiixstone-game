<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Nefarian
 */
class TBA01_4 extends Hero
{
    public function globalId() : string
    {
        return 'TBA01_4';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}