<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;
use Tiixstone\Effect\SkullOfTheManariEffect;

/**
 * Skull of the Man'ari
 */
class LOOT_420 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOOT_420';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 5;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }

    /**
     * @return array
     */
    public function equippedEffects() : array
    {
        return [SkullOfTheManariEffect::class];
    }
}