<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Red Riding Hood
 */
class KARA_13_19 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KARA_13_19';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}