<?php

namespace Tiixstone\Card;

/**
 * Gustave, the Gutripper
 */
class GILA_BOSS_62h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_62h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}