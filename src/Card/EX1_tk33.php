<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Alias\Infernal;

/**
 * INFERNO!
 */
class EX1_tk33 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'EX1_tk33';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [new Block\SummonMinion($game->currentPlayer(), Infernal::create())];
    }
}