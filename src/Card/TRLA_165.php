<?php

namespace Tiixstone\Card;

/**
 * Battlestarved Lynx
 */
class TRLA_165 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_165';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}