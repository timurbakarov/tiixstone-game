<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Knuckles
 */
class CFM_333 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_333';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}