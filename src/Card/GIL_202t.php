<?php

namespace Tiixstone\Card;

/**
 * Gilnean Royal Guard
 */
class GIL_202t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_202t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}