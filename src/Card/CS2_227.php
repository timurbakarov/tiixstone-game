<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Venture Co. Mercenary
 */
class CS2_227 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_227';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}