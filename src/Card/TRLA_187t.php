<?php

namespace Tiixstone\Card;

/**
 * Pirate's Mark
 */
class TRLA_187t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_187t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}