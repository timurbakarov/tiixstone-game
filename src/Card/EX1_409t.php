<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Heavy Axe
 */
class EX1_409t extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_409t';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}