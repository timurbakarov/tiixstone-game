<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\ChargeEffect;
use Tiixstone\Effect\TauntEffect;

/**
 * Gnomeregan Infantry
 */
class GVG_098 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_098';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [ChargeEffect::class, TauntEffect::class];
    }
}