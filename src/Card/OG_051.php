<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Forbidden Ancient
 */
class OG_051 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_051';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}