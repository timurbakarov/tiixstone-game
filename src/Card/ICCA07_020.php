<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Tentacles
 */
class ICCA07_020 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA07_020';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 6;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 6;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}