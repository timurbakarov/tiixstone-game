<?php

namespace Tiixstone\Card;

/**
 * Blood Portal
 */
class TRLA_185 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_185';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}