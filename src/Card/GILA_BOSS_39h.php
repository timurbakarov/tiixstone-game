<?php

namespace Tiixstone\Card;

/**
 * Forlorn Lovers
 */
class GILA_BOSS_39h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_39h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}