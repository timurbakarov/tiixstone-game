<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Fencing Coach
 */
class AT_115 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_115';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}