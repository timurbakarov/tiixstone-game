<?php

namespace Tiixstone\Card;

/**
 * Shadowmaw Panther
 */
class TRLA_164 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_164';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}