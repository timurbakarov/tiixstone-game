<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\GiveEffect;
use Tiixstone\Block\IncreaseArmor;
use Tiixstone\Effect\AttackChangerEffect;
use Tiixstone\Effect\Trigger\EndOfTheTurn;

/**
 * Claw
 */
class CS2_005 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_005';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new GiveEffect($this->getPlayer()->hero, new EndOfTheTurn(new AttackChangerEffect(2))),
            new IncreaseArmor($this->getPlayer()->hero, 2),
        ];
    }
}