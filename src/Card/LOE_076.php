<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Sir Finley Mrrgglton
 */
class LOE_076 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_076';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}