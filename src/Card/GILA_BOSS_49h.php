<?php

namespace Tiixstone\Card;

/**
 * Glinda Crowskin
 */
class GILA_BOSS_49h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_49h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}