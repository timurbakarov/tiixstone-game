<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\Block\TakeDamage;
use Tiixstone\PlayCardCondition;

/**
 * Fireball
 */
class CS2_029 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_029';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new TakeDamage($target, $game->withSpellDamage($this->damage()))];
    }

    /**
     * @return int
     */
    public function damage()
    {
        return 6;
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Character,
        ]);
    }
}