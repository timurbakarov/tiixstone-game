<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect\AttackChangerEffect;
use Tiixstone\Effect\Trigger\EndOfTheTurn;
use Tiixstone\Game;

/**
 * Bloodlust
 */
class CS2_046 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_046';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 5;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $targets = $game->targetManager->allPlayerMinions($game, $game->currentPlayer());

        return array_map(function(Minion $minion) {
            return [new GiveEffect($minion, new EndOfTheTurn(new AttackChangerEffect(3)))];
        }, $targets);
    }
}