<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hell Bovine
 */
class TB_SPT_DPromoMinionInit extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_SPT_DPromoMinionInit';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}