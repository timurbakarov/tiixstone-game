<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Doomhammer
 */
class EX1_567 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_567';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 5;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 8;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}