<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Manager\TargetManager;

/**
 * Avenging Wrath
 */
class EX1_384 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_384';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 6;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new Block\RandomMissilesDamage(TargetManager::ANY_OPPONENT_CHARACTER, 1, $game->withSpellDamage(8), $this)
        ];
    }
}