<?php

namespace Tiixstone\Card;

/**
 * Oondasta
 */
class TRL_542 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_542';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}