<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Kidnapper
 */
class NEW1_005 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_005';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}