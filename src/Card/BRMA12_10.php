<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Mutation
 */
class BRMA12_10 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'BRMA12_10';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}