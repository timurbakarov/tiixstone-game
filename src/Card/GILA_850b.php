<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Hallowed Water
 */
class GILA_850b extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_850b';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 7;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}