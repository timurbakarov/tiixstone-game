<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Activate!
 */
class BRMA14_10 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'BRMA14_10';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}