<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Fading Bite
 */
class GILA_BOSS_21p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_21p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}