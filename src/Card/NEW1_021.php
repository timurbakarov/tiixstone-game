<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\DoomsayerEffect;

/**
 * Doomsayer
 */
class NEW1_021 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_021';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [DoomsayerEffect::class];
    }
}