<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Grove Tender
 */
class GVG_032 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_032';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}