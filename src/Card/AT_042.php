<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Druid of the Saber
 */
class AT_042 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_042';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}