<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Nefarian
 */
class TB_CoOpv3_Boss extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_CoOpv3_Boss';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 200;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}