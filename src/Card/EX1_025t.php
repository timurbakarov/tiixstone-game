<?php

namespace Tiixstone\Card;

use Tiixstone\Race;

/**
 * Mechanical Dragonling
 */
class EX1_025t extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::MECH;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_025t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}