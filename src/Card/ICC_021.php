<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Exploding Bloatbat
 */
class ICC_021 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_021';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}