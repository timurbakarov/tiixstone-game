<?php

namespace Tiixstone\Card;

/**
 * Slamma Jamma
 */
class TRLA_169 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_169';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}