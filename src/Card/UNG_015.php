<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Minion;
use Tiixstone\Card\Character;
use Tiixstone\Card\Attackable;
use Tiixstone\Effect;

/**
 * Sunkeeper Tarim
 */
class UNG_015 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_015';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
	
	public function boardEffects() : array
	{
		return [Effect\TauntEffect::class];
	}
	
	/**
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = null, Character $target = null)
    {
        $game->logBlock('tarim-battlecry');
		
		$minions = $game->targetManager->allMinions($game);
		
		$blocks = [];
		foreach($minions as $minion) {
			if($minion->isSelf($this)) {
				continue;
			}
			
			$blocks[] = new Block\GiveEffect($minion, new Effect\AttackChangerEffect(3, null, true));
			$blocks[] = new Block\GiveEffect($minion, new Effect\HealthChangerEffect(3, null, true));
		}

        return $blocks;
    }
}