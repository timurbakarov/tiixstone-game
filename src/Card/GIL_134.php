<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Holy Water
 */
class GIL_134 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_134';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 5;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}