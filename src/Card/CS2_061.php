<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\Block\TakeDamage;
use Tiixstone\PlayCardCondition;
use Tiixstone\Block\HealCharacter;

/**
 * Drain Life
 */
class CS2_061 extends Spell
{
    const DAMAGE = 2;
    const HEAL = 2;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_061';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new TakeDamage($target, self::DAMAGE),
            new HealCharacter($game->currentPlayer()->hero, self::HEAL),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Character,
        ]);
    }
}