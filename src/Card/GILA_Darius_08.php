<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Legends
 */
class GILA_Darius_08 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_Darius_08';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}