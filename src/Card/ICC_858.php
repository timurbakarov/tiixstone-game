<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Bolvar, Fireblood
 */
class ICC_858 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_858';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}