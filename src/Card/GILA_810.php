<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Sneak Attack
 */
class GILA_810 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_810';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}