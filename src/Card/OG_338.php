<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Nat, the Darkfisher
 */
class OG_338 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_338';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}