<?php

namespace Tiixstone\Card;

/**
 * Gurubashi Offering
 */
class TRL_516 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_516';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}