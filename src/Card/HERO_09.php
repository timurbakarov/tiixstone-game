<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;
use Tiixstone\Card\Alias\LesserHeal;

/**
 * Anduin Wrynn
 */
class HERO_09 extends Hero
{
    public function globalId() : string
    {
        return 'HERO_09';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return LesserHeal::create();
    }
}