<?php

namespace Tiixstone\Card;

use Tiixstone\Block\MarkDestroyed;
use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;
use Tiixstone\Manager\TargetManager;

/**
 * Twisting Nether
 */
class EX1_312 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_312';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 8;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $targets = $game->targetManager->targets($game, TargetManager::ANY_MINION);

        $blocks = [];
        foreach($targets as $target) {
            $blocks[] = new MarkDestroyed($target);
        }

        return $blocks;
    }
}