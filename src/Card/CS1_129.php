<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\Block\GiveEffect;
use Tiixstone\PlayCardCondition;
use Tiixstone\Manager\TargetManager;
use Tiixstone\Effect\Buff\InnerFireBuff;

/**
 * Inner Fire
 */
class CS1_129 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS1_129';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $game->logBlock('inner-fire-spell');

        return [new GiveEffect($target, InnerFireBuff::class)];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Minion,
        ]);
    }
}