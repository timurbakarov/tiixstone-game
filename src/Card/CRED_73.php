<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * David Pendergrast
 */
class CRED_73 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_73';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}