<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Shado-Pan Monk
 */
class TU4f_003 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TU4f_003';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}