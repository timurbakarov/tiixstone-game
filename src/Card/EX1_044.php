<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\QuestingAdventurerEffect;

/**
 * Questing Adventurer
 */
class EX1_044 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_044';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [QuestingAdventurerEffect::class];
    }
}