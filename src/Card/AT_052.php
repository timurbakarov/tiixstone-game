<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Totem Golem
 */
class AT_052 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_052';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}