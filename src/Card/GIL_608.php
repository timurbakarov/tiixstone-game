<?php

namespace Tiixstone\Card;

/**
 * Witchwood Imp
 */
class GIL_608 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_608';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}