<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dark Iron Skulker
 */
class BRM_008 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_008';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}