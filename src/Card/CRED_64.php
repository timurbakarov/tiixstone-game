<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Liv Breeden
 */
class CRED_64 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_64';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}