<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Gnoll
 */
class TU4a_003 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TU4a_003';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}