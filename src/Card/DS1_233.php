<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\TakeDamage;

/**
 * Mind Blast
 */
class DS1_233 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'DS1_233';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $game->logBlock('mind-blast');

        return [new TakeDamage($game->idlePlayer()->hero, $game->withSpellDamage(5), $this)];
    }
}