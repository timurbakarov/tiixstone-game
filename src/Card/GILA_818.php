<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Haunted Curio
 */
class GILA_818 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_818';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}