<?php

namespace Tiixstone\Card;

/**
 * Tiger
 */
class TRL_309t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_309t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}