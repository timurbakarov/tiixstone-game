<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Young Dragonhawk
 */
class CS2_169 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_169';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}