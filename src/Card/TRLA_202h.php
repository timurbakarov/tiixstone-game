<?php

namespace Tiixstone\Card;

/**
 * Captain Hooktusk
 */
class TRLA_202h extends Hero
{
    public function globalId() : string
    {
        return 'TRLA_202h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}