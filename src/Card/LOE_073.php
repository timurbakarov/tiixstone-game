<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Fossilized Devilsaur
 */
class LOE_073 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_073';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}