<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\PlayCardCondition;
use Tiixstone\Block\HealCharacter;
use Tiixstone\Block\MarkDestroyed;
use Tiixstone\Condition\Target\Race;

/**
 * Sacrificial Pact
 */
class NEW1_003 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_003';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new MarkDestroyed($target),
            new HealCharacter($this->getPlayer()->hero, 5, $this),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Race(\Tiixstone\Race::demon()),
        ]);
    }
}