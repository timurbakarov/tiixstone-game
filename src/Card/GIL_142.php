<?php

namespace Tiixstone\Card;

/**
 * Chameleos
 */
class GIL_142 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_142';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}