<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Brave Archer
 */
class AT_059 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_059';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}