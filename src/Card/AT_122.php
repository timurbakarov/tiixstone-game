<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Gormok the Impaler
 */
class AT_122 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_122';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}