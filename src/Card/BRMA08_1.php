<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * General Drakkisath
 */
class BRMA08_1 extends Hero
{
    public function globalId() : string
    {
        return 'BRMA08_1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}