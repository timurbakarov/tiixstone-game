<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;
use Tiixstone\Manager\TargetManager;
use Tiixstone\Block\ReturnMinionToHand;
use Tiixstone\PlayCardCondition;
use Tiixstone\Condition;

/**
 * Sap
 */
class EX1_581 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_581';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new ReturnMinionToHand($target)];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Enemy,
            new Condition\Target\Minion,
        ]);
    }
}