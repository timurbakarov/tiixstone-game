<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Event;
use Tiixstone\Effect;
use Tiixstone\Exception;
use Tiixstone\Stats\Attack;
use Tiixstone\Stats\Health;
use Tiixstone\Battlecryable;

abstract class Minion extends Character
{
    /**
     * @param Game $game
     * @param Minion|null $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function play(Game $game, Minion $positionMinion = null, Character $target = null) : array
    {
        $battlecryBlocks = [];

        if($this instanceof Battlecryable) {
            $battlecryBlocks[] = new Block\Battlecry($this, $positionMinion, $target);
        }

        return [
            new Block\Trigger(new Event\BeforePlayMinion($this, $positionMinion, $target)),
            new Block\SummonMinion($game->currentPlayer(), $this, $positionMinion),
            new Block\Trigger(new Event\AfterPlayMinion($this, $positionMinion, $target)),
            $battlecryBlocks,
        ];
    }

    /**
     * @param Game $game
     * @return array
     */
    public function destroy(Game $game): array
    {
        $deathrattles = $this->deathrattles($game);

        return [
            new Block\Trigger(new Event\BeforeDestroyMinion($this)),
            new Block\Battlefield\RemoveFromBattlefield($this),
            $deathrattles,
            new Block\Trigger(new Event\AfterDestroyMinion($this)),
        ];
    }

    /**
     * @return $this
     */
    public function resetStats()
    {
        $this->health = new Health($this->defaultMaximumHealth());
        $this->attack = new Attack($this->defaultAttackRate());
        $this->setAttackCondition($this->defaultAttackCondition());

        return $this;
    }

    /**
     * @param Game $game
     * @return array
     */
    protected function deathrattles(Game $game)
    {
        $player = $this->getPlayer();

        /** @var Minion $nextMinion */
        $nextMinion = $player->board->next($this->id());

        if(!$nextMinion OR ($nextMinion AND $nextMinion->isDestroyed())) {
            $nextMinion = null;
        }

        $deathrattleEffects = $game->effects->getEffectByCard($this, Effect\DeathrattleEffect::class);

        return array_map(function(Effect\DeathrattleEffect $deathrattleEffect) use($nextMinion) {
            return new Block\Deathrattle($deathrattleEffect, $this, $nextMinion);
        }, $deathrattleEffects);
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [];
    }
}