<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Garr
 */
class BRMC_99 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMC_99';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}