<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Avatar of the Coin
 */
class GAME_002 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GAME_002';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}