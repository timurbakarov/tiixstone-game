<?php

namespace Tiixstone\Card;

use Tiixstone\Effect;
use Tiixstone\Card\Minion;

/**
 * Plated Beetle
 */
class LOOT_413 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOOT_413';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
	
	public function boardEffects()
	{
		return [Effect\PlatedBeetleDeathrattle::class];
	}
}