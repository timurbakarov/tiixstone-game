<?php

namespace Tiixstone\Card;

use Tiixstone\Effect;

/**
 * Raging Worgen
 */
class EX1_412 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_412';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [new Effect\EnragedEffect([Effect\WindfuryEffect::class, new Effect\AttackChangerEffect(1)])];
    }
}