<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Alias\LifeTap;
use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Gul'dan
 */
class HERO_07 extends Hero
{
    public function globalId() : string
    {
        return 'HERO_07';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return LifeTap::create();
    }
}