<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;
use Tiixstone\Card\Alias\Fireblast;

/**
 * Jaina Proudmoore
 */
class HERO_08 extends Hero
{
    public function globalId() : string
    {
        return 'HERO_08';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return Fireblast::create();
    }
}