<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hadidjah Chamberlin
 */
class CRED_54 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_54';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}