<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Guardian of Icecrown
 */
class NAX15_03n extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX15_03n';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}