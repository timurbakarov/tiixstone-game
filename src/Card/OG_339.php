<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Skeram Cultist
 */
class OG_339 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_339';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}