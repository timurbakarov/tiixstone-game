<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Martin Brochu
 */
class CRED_62 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_62';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}