<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Void Contract
 */
class TRL_246 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_246';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 8;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}