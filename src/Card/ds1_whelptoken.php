<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Whelp
 */
class ds1_whelptoken extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ds1_whelptoken';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}