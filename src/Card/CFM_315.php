<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Alleycat
 */
class CFM_315 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_315';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}