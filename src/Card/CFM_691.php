<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jade Swarmer
 */
class CFM_691 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_691';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}