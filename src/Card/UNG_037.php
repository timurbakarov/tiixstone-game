<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tortollan Shellraiser
 */
class UNG_037 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_037';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}