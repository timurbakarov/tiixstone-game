<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dark Iron Spectator
 */
class BRMA02_2t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA02_2t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}