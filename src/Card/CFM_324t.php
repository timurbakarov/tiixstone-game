<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * The Storm Guardian
 */
class CFM_324t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_324t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 10;
    }
}