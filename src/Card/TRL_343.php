<?php

namespace Tiixstone\Card;

/**
 * Wardruid Loti
 */
class TRL_343 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_343';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}