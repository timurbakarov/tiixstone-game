<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Bittertide Hydra
 */
class UNG_087 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_087';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}