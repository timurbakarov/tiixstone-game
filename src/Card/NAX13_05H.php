<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Stalagg
 */
class NAX13_05H extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX13_05H';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}