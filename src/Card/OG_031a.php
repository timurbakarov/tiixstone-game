<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Twilight Elemental
 */
class OG_031a extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_031a';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}