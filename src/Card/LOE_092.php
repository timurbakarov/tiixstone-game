<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Arch-Thief Rafaam
 */
class LOE_092 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_092';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}