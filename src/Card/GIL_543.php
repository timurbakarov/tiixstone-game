<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Dark Possession
 */
class GIL_543 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_543';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}