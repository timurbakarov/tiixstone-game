<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Uther of the Ebon Blade
 */
class ICC_829 extends Hero
{
    public function globalId() : string
    {
        return 'ICC_829';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}