<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Chieftain Scarvash
 */
class LOEA05_01 extends Hero
{
    public function globalId() : string
    {
        return 'LOEA05_01';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}