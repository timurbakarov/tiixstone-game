<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Drakkari Enchanter
 */
class ICC_901 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_901';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}