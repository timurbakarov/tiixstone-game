<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Tentacles for Arms
 */
class OG_033 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_033';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 5;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}