<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Moroes
 */
class KAR_044 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_044';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}