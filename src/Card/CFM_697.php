<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Lotus Illusionist
 */
class CFM_697 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_697';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}