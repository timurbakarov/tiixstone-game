<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\PintSizedSummonerEffect;

/**
 * Pint-Sized Summoner
 */
class EX1_076 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_076';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [PintSizedSummonerEffect::class];
    }
}