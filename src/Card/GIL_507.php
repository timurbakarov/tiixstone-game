<?php

namespace Tiixstone\Card;

/**
 * Bewitched Guardian
 */
class GIL_507 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_507';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}