<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Kabal Lackey
 */
class CFM_066 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_066';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}