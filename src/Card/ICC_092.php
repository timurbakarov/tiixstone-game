<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Condition\Target\Friendly;
use Tiixstone\Effect\AttackChangerEffect;
use Tiixstone\Game;
use Tiixstone\Battlecryable;
use Tiixstone\PlayCardCondition;

/**
 * Acherus Veteran
 */
class ICC_092 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_092';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        if(!$target) {
            return [];
        }

        return [
            new GiveEffect($target, new AttackChangerEffect(1)),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Friendly(),
            new \Tiixstone\Condition\Target\Minion(),
        ]);
    }
}