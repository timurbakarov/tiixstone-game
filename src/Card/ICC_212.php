<?php

namespace Tiixstone\Card;

use Tiixstone\Effect;

/**
 * Acolyte of Agony
 */
class ICC_212 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_212';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     */
	public function boardEffects()
	{
		return [new Effect\LifestealEffect];
	}
}