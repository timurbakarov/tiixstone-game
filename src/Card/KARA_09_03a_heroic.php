<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Icky Imp
 */
class KARA_09_03a_heroic extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KARA_09_03a_heroic';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}