<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Grotesque Dragonhawk
 */
class OG_152 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_152';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}