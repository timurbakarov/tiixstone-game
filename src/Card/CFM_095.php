<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Weasel Tunneler
 */
class CFM_095 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_095';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}