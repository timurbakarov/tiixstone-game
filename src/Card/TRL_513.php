<?php

namespace Tiixstone\Card;

/**
 * Mosh'Ogg Enforcer
 */
class TRL_513 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_513';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 14;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}