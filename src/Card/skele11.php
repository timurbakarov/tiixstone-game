<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Skeleton
 */
class skele11 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'skele11';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}