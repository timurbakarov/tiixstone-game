<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mimiron's Head
 */
class GVG_111 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_111';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}