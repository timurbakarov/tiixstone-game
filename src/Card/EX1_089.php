<?php

namespace Tiixstone\Card;

use Tiixstone\Block\SetManaCrystals;
use Tiixstone\Game;
use Tiixstone\Battlecryable;

/**
 * Arcane Golem
 */
class EX1_089 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_089';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        $opponent = $attackable->getPlayer()->getOpponent($game);

        return [
            new SetManaCrystals($opponent, $opponent->maximumMana() + 1),
        ];
    }
}