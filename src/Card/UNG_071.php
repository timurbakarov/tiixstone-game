<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Giant Mastodon
 */
class UNG_071 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_071';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}