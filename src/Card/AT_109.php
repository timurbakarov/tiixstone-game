<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Argent Watchman
 */
class AT_109 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_109';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}