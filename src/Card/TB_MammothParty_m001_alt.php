<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Party Crasher
 */
class TB_MammothParty_m001_alt extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_MammothParty_m001_alt';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}