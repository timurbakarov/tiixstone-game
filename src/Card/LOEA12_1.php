<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Lady Naz'jar
 */
class LOEA12_1 extends Hero
{
    public function globalId() : string
    {
        return 'LOEA12_1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}