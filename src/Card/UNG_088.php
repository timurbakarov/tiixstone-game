<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tortollan Primalist
 */
class UNG_088 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_088';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}