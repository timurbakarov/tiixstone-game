<?php

namespace Tiixstone\Card;

/**
 * Tribute from the Tides
 */
class TRLA_154t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_154t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}