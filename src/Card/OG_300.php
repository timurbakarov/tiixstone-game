<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * The Boogeymonster
 */
class OG_300 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_300';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}