<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Swamp King Dred
 */
class UNG_919 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_919';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 9;
    }
}