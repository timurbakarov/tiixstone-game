<?php

namespace Tiixstone\Card;

/**
 * Duskbat
 */
class GIL_508 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_508';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}