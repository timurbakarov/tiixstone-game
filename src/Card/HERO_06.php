<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Alias\Shapeshift;
use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Malfurion Stormrage
 */
class HERO_06 extends Hero
{
    public function globalId() : string
    {
        return 'HERO_06';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return Shapeshift::create();
    }
}