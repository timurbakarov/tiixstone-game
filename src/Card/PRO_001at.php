<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Murloc
 */
class PRO_001at extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'PRO_001at';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}