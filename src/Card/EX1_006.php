<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Alarm-o-Bot
 */
class EX1_006 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_006';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}