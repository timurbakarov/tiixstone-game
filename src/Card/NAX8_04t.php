<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Spectral Warrior
 */
class NAX8_04t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX8_04t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}