<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Race;
use Tiixstone\Effect;
use Tiixstone\Effect\TauntEffect;
use Tiixstone\Effect\TarCreeperEffect;

/**
 * Tar Creeper
 */
class UNG_928 extends Minion
{
    protected $race = Race::ELEMENTAL;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_928';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [
            $this->tarCreeperEffect(),
            TauntEffect::class,
        ];
    }

    /**
     * @return Effect|Effect\Changer\AttackChanger
     */
    private function tarCreeperEffect()
    {
        return new class extends Effect implements Effect\Changer\AttackChanger {

            /**
             * @param Game $game
             * @param Attackable $character
             * @param int $attackRate
             * @return int
             */
            public function changeAttack(Game $game, Attackable $character, int $attackRate): int
            {
                if(!$character->isSelf($this->owner())) {
                    return $attackRate;
                }

                if($game->currentPlayer()->id() == $this->owner()->getPlayer()->id()) {
                    return $attackRate;
                }

                return $attackRate + 2;
            }
        };
    }
}