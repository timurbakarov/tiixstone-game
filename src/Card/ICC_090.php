<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Snowfury Giant
 */
class ICC_090 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_090';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 11;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}