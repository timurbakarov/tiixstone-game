<?php

namespace Tiixstone\Card;

/**
 * Doctor Sezavo
 */
class GILA_BOSS_44h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_44h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}