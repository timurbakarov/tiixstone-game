<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Together Forever
 */
class GILA_BOSS_39p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_39p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}