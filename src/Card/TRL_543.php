<?php

namespace Tiixstone\Card;

/**
 * Bloodclaw
 */
class TRL_543 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_543';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}