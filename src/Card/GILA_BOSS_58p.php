<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Terrify
 */
class GILA_BOSS_58p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_58p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}