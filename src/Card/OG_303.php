<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Cult Sorcerer
 */
class OG_303 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_303';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}