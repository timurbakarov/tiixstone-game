<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Curator
 */
class KARA_07_01heroic extends Hero
{
    public function globalId() : string
    {
        return 'KARA_07_01heroic';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}