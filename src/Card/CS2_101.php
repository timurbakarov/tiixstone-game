<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Card\Alias\SilverHandRecruit;

/**
 * Reinforce
 */
class CS2_101 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'CS2_101';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        $game->logBlock('reinforce');

        return [
            new SummonMinion($game->currentPlayer(), SilverHandRecruit::create()),
        ];
    }
}