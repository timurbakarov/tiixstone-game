<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * War Golem
 */
class CS2_186 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_186';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}