<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Netherspite
 */
class KARA_08_01 extends Hero
{
    public function globalId() : string
    {
        return 'KARA_08_01';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}