<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Massive Gnoll
 */
class TU4a_005 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TU4a_005';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}