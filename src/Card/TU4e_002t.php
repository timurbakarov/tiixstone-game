<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Flame of Azzinoth
 */
class TU4e_002t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TU4e_002t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}