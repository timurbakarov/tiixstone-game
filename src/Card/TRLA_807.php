<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Boon Reaper
 */
class TRLA_807 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_807';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}