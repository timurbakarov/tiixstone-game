<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Bog Creeper
 */
class OG_153 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_153';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}