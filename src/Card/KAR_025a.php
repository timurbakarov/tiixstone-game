<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Candle
 */
class KAR_025a extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_025a';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}