<?php

namespace Tiixstone\Card;

/**
 * Festeroot Hulk
 */
class GIL_655 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_655';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}