<?php

namespace Tiixstone\Card;

use Tiixstone\Effect;
use Tiixstone\Card\Minion;

/**
 * Thrallmar Farseer
 */
class EX1_021 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_021';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

	public function boardEffects()
	{
		return [Effect\WindfuryEffect::class];
	}
}