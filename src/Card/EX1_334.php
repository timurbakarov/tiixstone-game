<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Condition;
use Tiixstone\PlayCardCondition;

/**
 * Shadow Madness
 */
class EX1_334 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_334';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|Minion|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new Block\MindControl($target),
            new Block\GiveEffect($target, new Effect\ChargeEffect()),
            new Block\GiveEffect($target, Effect\ReturnControlBack::class),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Enemy,
            new Condition\Target\Minion,
            new Condition\Target\AttackLowerThan(4),
        ]);
    }
}