<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Orsis Guard
 */
class LOEA04_13bth extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA04_13bth';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}