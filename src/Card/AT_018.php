<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Confessor Paletress
 */
class AT_018 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_018';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}