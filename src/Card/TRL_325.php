<?php

namespace Tiixstone\Card;

/**
 * Sul'thraze
 */
class TRL_325 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_325';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 6;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}