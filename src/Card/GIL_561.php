<?php

namespace Tiixstone\Card;

/**
 * Blackwald Pixie
 */
class GIL_561 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_561';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}