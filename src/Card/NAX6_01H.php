<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Loatheb
 */
class NAX6_01H extends Hero
{
    public function globalId() : string
    {
        return 'NAX6_01H';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}