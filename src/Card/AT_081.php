<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Eadric the Pure
 */
class AT_081 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_081';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}