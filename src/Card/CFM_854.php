<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ancient of Blossoms
 */
class CFM_854 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_854';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}