<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Dragonteeth
 */
class BRMA16_5 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA16_5';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 6;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}