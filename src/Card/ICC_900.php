<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Necrotic Geist
 */
class ICC_900 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_900';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}