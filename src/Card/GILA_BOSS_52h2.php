<?php

namespace Tiixstone\Card;

/**
 * Beastly Pete
 */
class GILA_BOSS_52h2 extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_52h2';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}