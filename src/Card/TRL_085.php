<?php

namespace Tiixstone\Card;

/**
 * Zentimo
 */
class TRL_085 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_085';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}