<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Rain of Toads
 */
class TRL_351 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_351';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 6;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}