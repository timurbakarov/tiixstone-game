<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Maiev Shadowsong
 */
class HERO_03a extends Hero
{
    public function globalId() : string
    {
        return 'HERO_03a';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}