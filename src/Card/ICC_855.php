<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hyldnir Frostrider
 */
class ICC_855 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_855';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}