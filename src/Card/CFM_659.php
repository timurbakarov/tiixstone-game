<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Gadgetzan Socialite
 */
class CFM_659 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_659';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}