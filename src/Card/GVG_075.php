<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ship's Cannon
 */
class GVG_075 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_075';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}