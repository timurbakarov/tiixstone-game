<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Treant
 */
class EX1_tk9 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_tk9';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}