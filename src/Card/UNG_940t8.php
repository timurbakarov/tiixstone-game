<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Amara, Warden of Hope
 */
class UNG_940t8 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_940t8';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}