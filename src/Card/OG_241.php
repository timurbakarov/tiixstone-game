<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Possessed Villager
 */
class OG_241 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_241';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}