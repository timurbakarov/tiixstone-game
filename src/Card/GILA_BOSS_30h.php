<?php

namespace Tiixstone\Card;

/**
 * Blood Witch Gretta
 */
class GILA_BOSS_30h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_30h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}