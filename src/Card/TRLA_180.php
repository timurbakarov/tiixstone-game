<?php

namespace Tiixstone\Card;

/**
 * Leering Bat
 */
class TRLA_180 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_180';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}