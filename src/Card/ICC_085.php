<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * Ultimate Infestation
 */
class ICC_085 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_085';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 10;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}