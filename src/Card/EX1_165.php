<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Druid of the Claw
 */
class EX1_165 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_165';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}