<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\PlayCardCondition;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Card\Alias\HealingTotem;
use Tiixstone\Card\Alias\SearingTotem;
use Tiixstone\Card\Alias\StoneclawTotem;
use Tiixstone\Card\Alias\WrathOfAirTotem;

/**
 * Totemic Call
 */
class CS2_049 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'CS2_049';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        $game->logBlock('totemic-call');

        return [new SummonMinion($game->currentPlayer(), $this->randomTotem($game))];
    }

    /**
     * @return mixed
     */
    public function randomTotem(Game $game)
    {
        $totems = $this->totemsNotOnBoard($game);

        shuffle($totems);

        return new $totems[$game->RNG->int(0, count($totems) - 1)];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\ClosureCondition(function(Game $game) {
                return count($this->totemsNotOnBoard($game)) > 0;
            }, 'All totems are on board'),
        ]);
    }

    /**
     * @param Game $game
     * @return array|null
     */
    public function totemsNotOnBoard(Game $game)
    {
        $totemsClass = array_flip(self::totems());

        foreach($game->targetManager->allPlayerMinions($game, $game->currentPlayer()) as $minion) {
            $minionClass = get_class($minion);

            if(array_key_exists($minionClass, $totemsClass)) {
                unset($totemsClass[$minionClass]);
            }
        }

        return array_flip($totemsClass);
    }

    /**
     * @return array
     */
    static public function totems()
    {
        return [
            HealingTotem::className(),
            StoneclawTotem::className(),
            WrathOfAirTotem::className(),
            SearingTotem::className(),
        ];
    }
}