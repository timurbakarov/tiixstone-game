<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Enchanted Raven
 */
class KAR_300 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_300';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}