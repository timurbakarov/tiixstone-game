<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Bolvar Fordragon
 */
class GVG_063 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_063';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}