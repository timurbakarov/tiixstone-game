<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\SpellDamageEffect;

/**
 * Ogre Magi
 */
class CS2_197 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_197';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }

    /**
     * @return array
     * @throws \Tiixstone\Exception
     */
    public function boardEffects()
    {
        return [new SpellDamageEffect(1)];
    }
}