<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * Stolen Winter's Veil Gift
 */
class TB_GiftExchange_Treasure_Spell extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_GiftExchange_Treasure_Spell';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}