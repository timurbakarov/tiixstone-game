<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Cobalt Scalebane
 */
class ICC_029 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_029';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}