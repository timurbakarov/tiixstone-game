<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * WANTED!
 */
class GIL_687 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_687';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}