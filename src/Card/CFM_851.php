<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Daring Reporter
 */
class CFM_851 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_851';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}