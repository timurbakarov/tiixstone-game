<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect\ChargeEffect;

/**
 * Stonetusk Boar
 */
class CS2_171 extends Minion
{
    protected $race = Race::BEAST;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_171';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [ChargeEffect::class];
    }
}