<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Imp
 */
class GVG_045t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_045t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}