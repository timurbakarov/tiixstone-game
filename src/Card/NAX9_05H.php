<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Runeblade
 */
class NAX9_05H extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX9_05H';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}