<?php

namespace Tiixstone\Card;
use Tiixstone\Race;

/**
 * Lord Jaraxxus
 */
class EX1_323h extends Hero
{
    /**
     * @var int
     */
    protected $race = Race::DEMON;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_323h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return Alias\INFERNO::create();
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth() : int
    {
        return 15;
    }
}