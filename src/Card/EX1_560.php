<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Nozdormu
 */
class EX1_560 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_560';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}