<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Pile On!!!
 */
class TB_BRMA01_2H_2 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TB_BRMA01_2H_2';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}