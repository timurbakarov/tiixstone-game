<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Malkorok
 */
class OG_220 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_220';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}