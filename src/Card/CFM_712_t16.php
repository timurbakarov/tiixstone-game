<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jade Golem
 */
class CFM_712_t16 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_712_t16';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 16;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 16;
    }
}