<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Vilefin Inquisitor
 */
class OG_006 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_006';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}