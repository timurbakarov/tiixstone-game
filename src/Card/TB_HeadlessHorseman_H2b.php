<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Stormwind Investigator Cat
 */
class TB_HeadlessHorseman_H2b extends Hero
{
    public function globalId() : string
    {
        return 'TB_HeadlessHorseman_H2b';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}