<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Moira Bronzebeard
 */
class BRMA03_3H extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA03_3H';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}