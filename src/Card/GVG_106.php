<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Junkbot
 */
class GVG_106 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_106';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}