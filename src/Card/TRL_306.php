<?php

namespace Tiixstone\Card;

/**
 * Immortal Prelate
 */
class TRL_306 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_306';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}