<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Nerubian
 */
class NAX1h_03 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX1h_03';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}