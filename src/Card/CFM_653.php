<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hired Gun
 */
class CFM_653 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_653';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}