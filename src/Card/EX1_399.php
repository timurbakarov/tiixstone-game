<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\Trigger\GurubashiBerserkerEffect;

/**
 * Gurubashi Berserker
 */
class EX1_399 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_399';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [GurubashiBerserkerEffect::class];
    }
}