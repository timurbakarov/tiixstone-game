<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Public Defender
 */
class CFM_300 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_300';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}