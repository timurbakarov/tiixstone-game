<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ravaging Ghoul
 */
class OG_149 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_149';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}