<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Crystal
 */
class CFM_606t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_606t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}