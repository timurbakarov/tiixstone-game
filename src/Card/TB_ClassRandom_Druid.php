<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * Second Class: Druid
 */
class TB_ClassRandom_Druid extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_ClassRandom_Druid';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}