<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Silithid Swarmer
 */
class OG_034 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_034';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}