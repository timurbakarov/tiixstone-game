<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Gladiator's Longbow
 */
class DS1_188 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'DS1_188';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 7;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}