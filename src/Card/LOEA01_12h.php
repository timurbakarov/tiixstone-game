<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tol'vir Hoplite
 */
class LOEA01_12h extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA01_12h';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}