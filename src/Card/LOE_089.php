<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Wobbling Runts
 */
class LOE_089 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_089';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}