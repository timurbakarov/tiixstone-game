<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mayor Noggenfogger
 */
class CFM_670 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_670';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}