<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\AuchenaiSoulpriestEffect;

/**
 * Auchenai Soulpriest
 */
class EX1_591 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_591';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [AuchenaiSoulpriestEffect::class];
    }
}