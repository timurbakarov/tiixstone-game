<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dave Kosak
 */
class CRED_55 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_55';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 12;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 12;
    }
}