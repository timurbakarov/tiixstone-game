<?php

namespace Tiixstone\Card;

/**
 * Ratcatcher Hannigul
 */
class GILA_BOSS_67h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_67h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}