<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Battle Standard
 */
class TB_SPT_Minion2 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_SPT_Minion2';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}