<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Chuck
 */
class GILA_BOSS_23t extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_BOSS_23t';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 6;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}