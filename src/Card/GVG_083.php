<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Upgraded Repair Bot
 */
class GVG_083 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_083';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}