<?php

namespace Tiixstone\Card;

/**
 * Impetuous Companion
 */
class GILA_906 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_906';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}