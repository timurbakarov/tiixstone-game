<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Staff of Origination
 */
class TB_KTRAF_HP_RAF5 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TB_KTRAF_HP_RAF5';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}