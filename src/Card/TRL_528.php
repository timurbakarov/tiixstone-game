<?php

namespace Tiixstone\Card;

/**
 * Linecracker
 */
class TRL_528 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_528';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}