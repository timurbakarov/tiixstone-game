<?php

namespace Tiixstone\Card;

/**
 * Black Cat
 */
class GIL_838 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_838';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}