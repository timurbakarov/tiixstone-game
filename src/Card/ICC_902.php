<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mindbreaker
 */
class ICC_902 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_902';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}