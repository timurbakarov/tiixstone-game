<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Professor Putricide
 */
class FB_LK_013h extends Hero
{
    public function globalId() : string
    {
        return 'FB_LK_013h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}