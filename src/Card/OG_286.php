<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Twilight Elder
 */
class OG_286 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_286';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}