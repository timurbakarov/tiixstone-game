<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Sun Raider Phaerix
 */
class LOEA16_19 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA16_19';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}