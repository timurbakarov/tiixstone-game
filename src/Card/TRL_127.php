<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Cannon Barrage
 */
class TRL_127 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_127';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 6;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}