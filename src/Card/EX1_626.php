<?php

namespace Tiixstone\Card;

use Tiixstone\Block\DrawCard;
use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect\SilenceEffect;
use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * Mass Dispel
 */
class EX1_626 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_626';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $blocks = [];

        foreach($game->targetManager->allEnemyMinions($game, $this->getPlayer()) as $minion) {
            $blocks[] = new GiveEffect($minion, SilenceEffect::class);
        }

        $blocks[] = new DrawCard($this->getPlayer());

        return $blocks;
    }
}