<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Giant Wasp
 */
class ICC_828t3 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_828t3';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}