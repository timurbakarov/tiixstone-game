<?php

namespace Tiixstone\Card;

/**
 * Akali's War Drum
 */
class TRLA_170 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_170';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}