<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Omegawarper
 */
class TB_FW_Warper extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_FW_Warper';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}