<?php

namespace Tiixstone\Card;

use Tiixstone\Block\ClosureBlock;
use Tiixstone\Block\HealCharacter;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Card\Alias\Lightwarden;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Light of the Naaru
 */
class GVG_012 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_012';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new HealCharacter($target, 3, $this),
            new ClosureBlock(function (Game $game) use($target) {
                if($target->health->isDamaged() OR $target->isDestroyed()) {
                    return [new SummonMinion($this->getPlayer(), Lightwarden::create())];
                }

                return [];
            }),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Character,
        ]);
    }
}