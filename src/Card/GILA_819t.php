<?php

namespace Tiixstone\Card;

/**
 * Weeping Ghost
 */
class GILA_819t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_819t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}