<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Condition\Target\Enemy;
use Tiixstone\Effect\CorruptionEffect;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Corruption
 */
class CS2_063 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_063';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new GiveEffect($target, new CorruptionEffect($this->getPlayer()))];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Enemy,
            new \Tiixstone\Condition\Target\Minion,
        ]);
    }
}