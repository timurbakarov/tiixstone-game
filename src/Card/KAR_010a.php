<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Whelp
 */
class KAR_010a extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_010a';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}