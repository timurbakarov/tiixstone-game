<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Alias\Frog;
use Tiixstone\Card\Alias\Sheep;
use Tiixstone\Card\Character;
use Tiixstone\Card\Alias\ChillwindYeti;

/**
 * Tracking
 */
class DS1_184 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'DS1_184';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $randomCards = $this->randomCardsFromDeck($game->currentPlayer()->deck);

        $game->chooseCard($randomCards, $this->afterChoose($game));

        return [];
    }

    /**
     * @param Game $game
     * @return \Closure
     */
    private function afterChoose(Game $game)
    {
        return function(Card $chosenCard, array $otherCards) use($game) {
            /** @var Card $card */
            foreach($otherCards as $card) {
                $game->currentPlayer()->deck->remove($card->id());
            }
        };
    }

    /**
     * @param Card\Collection\Deck $deck
     * @return array
     */
    private function randomCardsFromDeck(Card\Collection\Deck $deck)
    {
        $totalCards = $deck->count();

        if($totalCards >= 3) {
            return $deck->random(3);
        } else {
            return $deck->random($totalCards);
        }
    }
}