<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\TauntEffect;
use Tiixstone\Effect\EnragedEffect;
use Tiixstone\Effect\AttackChangerEffect;

/**
 * Tauren Warrior
 */
class EX1_390 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_390';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [TauntEffect::class, new EnragedEffect([new AttackChangerEffect(3)])];
    }
}