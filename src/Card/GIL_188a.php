<?php

namespace Tiixstone\Card;

/**
 * Dire Panther Form
 */
class GIL_188a extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_188a';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}