<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Temporal Loop
 */
class GILA_900p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_900p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}