<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Slithering Guard
 */
class LOEA09_8H extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA09_8H';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}