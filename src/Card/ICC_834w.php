<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Shadowmourne
 */
class ICC_834w extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_834w';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 8;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}