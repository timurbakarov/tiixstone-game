<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Polluted Hoarder
 */
class OG_323 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_323';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}