<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Max Ma
 */
class CRED_34 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_34';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}