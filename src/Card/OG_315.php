<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Bloodsail Cultist
 */
class OG_315 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_315';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}