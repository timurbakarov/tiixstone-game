<?php

namespace Tiixstone\Card;

/**
 * Redband Wasp
 */
class GIL_155 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_155';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}