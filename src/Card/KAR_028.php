<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Fool's Bane
 */
class KAR_028 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_028';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 5;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}