<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Nerubian
 */
class AT_036t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_036t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}