<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Dragon Roar
 */
class TRL_362 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_362';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}