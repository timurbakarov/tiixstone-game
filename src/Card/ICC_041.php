<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\ClosureBlock;
use Tiixstone\Manager\TargetManager;
use Tiixstone\Block\DealSplashDamage;

/**
 * Defile
 */
class ICC_041 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_041';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new DealSplashDamage(TargetManager::ANY_MINION, $game->withSpellDamage(1), $this),
            new ClosureBlock($this->ifAnyMinionDiesRepeat()),
        ];
    }

    /**
     * @return \Closure
     */
    public function ifAnyMinionDiesRepeat()
    {
        return function(Game $game) {
            foreach($game->targetManager->allMinions($game) as $minion) {
                if ($minion->isDestroyed()) {
                    $blocks = array_merge(
                        $game->resolver->afterSequenceBlocks(),
                        [new ClosureBlock(function(Game $game){ return $this->cast($game);})]
                    );

                    return $blocks;
                }
            }

            return [];
        };
    }
}