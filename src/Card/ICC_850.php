<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Shadowblade
 */
class ICC_850 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_850';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}