<?php

namespace Tiixstone\Card;

use Tiixstone\Race;

/**
 * Searing Totem
 */
class CS2_050 extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::TOTEM;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_050';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}