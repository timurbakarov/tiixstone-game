<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Amani Berserker
 */
class EX1_393 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_393';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}