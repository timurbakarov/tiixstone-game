<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\Battlecryable;
use Tiixstone\PlayCardCondition;
use Tiixstone\Block\ReturnMinionToHand;

/**
 * Youthful Brewmaster
 */
class EX1_049 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_049';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|null $positionMinion
     * @param Character|null $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = null, Character $target = null)
    {
        $game->logBlock('youthful-brewmaster-battlecry');

        return $target ? [new ReturnMinionToHand($target)] : [];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Friendly,
            new Condition\Target\Minion,
        ]);
    }
}