<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Hook
 */
class NAX10_02H extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX10_02H';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 8;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}