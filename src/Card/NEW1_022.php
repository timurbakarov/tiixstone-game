<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dread Corsair
 */
class NEW1_022 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_022';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}