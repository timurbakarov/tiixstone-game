<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Anodized Robo Cub
 */
class GVG_030 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_030';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}