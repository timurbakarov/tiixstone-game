<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Babbling Book
 */
class KAR_009 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_009';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}