<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Grobbulus
 */
class NAX11_01H extends Hero
{
    public function globalId() : string
    {
        return 'NAX11_01H';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}