<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Howlfiend
 */
class ICC_218 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_218';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}