<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Chromaggus
 */
class BRMA12_1 extends Hero
{
    public function globalId() : string
    {
        return 'BRMA12_1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}