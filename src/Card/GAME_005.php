<?php

namespace Tiixstone\Card;

use Tiixstone\Block\SetManaAvailable;
use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * The Coin
 */
class GAME_005 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GAME_005';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $game->logBlock('the-coin-spell');

        if($game->currentPlayer()->availableMana() >= 10) {
            return [];
        }

        return [new SetManaAvailable($game->currentPlayer(), $game->currentPlayer()->availableMana() + 1)];
    }
}