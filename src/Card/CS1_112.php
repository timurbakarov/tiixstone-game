<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\Heal\MassHeal;
use Tiixstone\Manager\TargetManager;
use Tiixstone\Block\DealSplashDamage;

/**
 * Holy Nova
 */
class CS1_112 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS1_112';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 5;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new DealSplashDamage(TargetManager::ANY_OPPONENT_CHARACTER, $game->withSpellDamage(2), $this),
            new MassHeal(TargetManager::ANY_FRIENDLY_CHARACTER, 2, $this),
        ];
    }
}