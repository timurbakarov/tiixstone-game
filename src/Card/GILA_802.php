<?php

namespace Tiixstone\Card;

/**
 * Cartographer
 */
class GILA_802 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_802';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}