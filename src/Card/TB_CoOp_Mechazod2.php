<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Overloaded Mechazod
 */
class TB_CoOp_Mechazod2 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_CoOp_Mechazod2';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 80;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 9;
    }
}