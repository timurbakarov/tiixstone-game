<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Piranha
 */
class CFM_337t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_337t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}