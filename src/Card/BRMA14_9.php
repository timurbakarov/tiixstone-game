<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Magmatron
 */
class BRMA14_9 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA14_9';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}