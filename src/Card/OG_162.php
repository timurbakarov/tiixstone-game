<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Disciple of C'Thun
 */
class OG_162 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_162';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}