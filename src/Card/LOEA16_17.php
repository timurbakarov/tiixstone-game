<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Animated Statue
 */
class LOEA16_17 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA16_17';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 10;
    }
}