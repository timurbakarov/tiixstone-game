<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Flamewaker Acolyte
 */
class BRMA06_4 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA06_4';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}