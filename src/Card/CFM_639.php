<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Grimestreet Enforcer
 */
class CFM_639 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_639';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}