<?php

namespace Tiixstone\Card;

/**
 * Devilsaur
 */
class EX1_tk29 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_tk29';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}