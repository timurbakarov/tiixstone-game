<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Warlock on Fire
 */
class ICCA01_011 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA01_011';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}