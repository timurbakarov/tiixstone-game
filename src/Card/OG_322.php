<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Blackwater Pirate
 */
class OG_322 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_322';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}