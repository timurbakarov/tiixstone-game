<?php

namespace Tiixstone\Card;

/**
 * Sharktoothed Harpooner
 */
class TRLA_191 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_191';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}