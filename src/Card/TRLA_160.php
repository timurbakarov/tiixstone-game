<?php

namespace Tiixstone\Card;

/**
 * Naga Tonguelasher
 */
class TRLA_160 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_160';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}