<?php

namespace Tiixstone\Card;

/**
 * Blood Pact
 */
class TRLA_113t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_113t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}