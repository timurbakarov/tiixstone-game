<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * MASSIVE Difficulty
 */
class FB_LK_BossSetup001a extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FB_LK_BossSetup001a';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}