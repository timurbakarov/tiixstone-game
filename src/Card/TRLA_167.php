<?php

namespace Tiixstone\Card;

/**
 * Rabid Saurolisk
 */
class TRLA_167 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_167';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}