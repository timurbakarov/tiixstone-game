<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Trap Preparation
 */
class GILA_BOSS_25p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_25p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}