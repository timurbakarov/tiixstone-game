<?php

namespace Tiixstone\Card;

/**
 * Rotten Applebaum
 */
class GIL_667 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_667';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}