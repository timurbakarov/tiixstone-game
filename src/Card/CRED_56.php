<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dustin Escoffery
 */
class CRED_56 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_56';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}