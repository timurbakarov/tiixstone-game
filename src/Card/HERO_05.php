<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Alias\SteadyShot;

/**
 * Rexxar
 */
class HERO_05 extends Hero
{
    public function globalId() : string
    {
        return 'HERO_05';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return SteadyShot::create();
    }
}