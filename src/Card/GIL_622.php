<?php

namespace Tiixstone\Card;

/**
 * Lifedrinker
 */
class GIL_622 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_622';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}