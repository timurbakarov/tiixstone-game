<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Guzzler
 */
class BRMA01_4t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA01_4t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}