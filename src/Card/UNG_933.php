<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * King Mosh
 */
class UNG_933 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_933';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 9;
    }
}