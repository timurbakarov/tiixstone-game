<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Condition;
use Tiixstone\PlayCardCondition;

/**
 * Rockbiter Weapon
 */
class CS2_045 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_045';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new Block\GiveEffect($target, new Effect\Trigger\EndOfTheTurn(new Effect\AttackChangerEffect(3)))];
    }

    /**
     * @return int
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Friendly,
            new Condition\Target\Character,
        ]);
    }
}