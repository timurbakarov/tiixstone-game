<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Harvest
 */
class NAX8_02H_TB extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'NAX8_02H_TB';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}