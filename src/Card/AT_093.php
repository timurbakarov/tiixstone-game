<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Frigid Snobold
 */
class AT_093 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_093';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}