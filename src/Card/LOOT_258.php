<?php

namespace Tiixstone\Card;

/**
 * Dire Mole
 */
class LOOT_258 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOOT_258';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}