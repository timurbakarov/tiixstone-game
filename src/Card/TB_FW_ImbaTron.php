<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Annoy-o-p-Tron
 */
class TB_FW_ImbaTron extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_FW_ImbaTron';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}