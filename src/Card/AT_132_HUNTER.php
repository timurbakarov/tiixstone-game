<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Ballista Shot
 */
class AT_132_HUNTER extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'AT_132_HUNTER';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}