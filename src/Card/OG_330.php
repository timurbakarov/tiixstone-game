<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Undercity Huckster
 */
class OG_330 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_330';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}