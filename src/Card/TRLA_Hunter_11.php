<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Survival Tactics
 */
class TRLA_Hunter_11 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_Hunter_11';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}