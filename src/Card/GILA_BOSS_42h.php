<?php

namespace Tiixstone\Card;

/**
 * Baran the Blind
 */
class GILA_BOSS_42h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_42h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}