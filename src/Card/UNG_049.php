<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tar Lurker
 */
class UNG_049 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_049';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}