<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\PlayCardCondition;
use Tiixstone\Block\HealCharacter;

/**
 * Lesser Heal
 */
class CS1h_001 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'CS1h_001';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [new HealCharacter($target, $this->defaultHealAmount(), $this)];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Character,
        ]);
    }

    /**
     * @return int
     */
    private function defaultHealAmount()
    {
        return 2;
    }
}