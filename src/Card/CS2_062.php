<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Manager\TargetManager;
use Tiixstone\Block\DealSplashDamage;

/**
 * Hellfire
 */
class CS2_062 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_062';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $game->logBlock('hellfire');

        return [new DealSplashDamage(TargetManager::ANY_CHARACTER, $game->withSpellDamage(3), $this)];
    }
}