<?php

namespace Tiixstone\Card;

/**
 * Gonk's Mark
 */
class TRLA_115t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_115t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}