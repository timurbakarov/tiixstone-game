<?php

namespace Tiixstone\Card;

/**
 * Garrow, the Rancorous
 */
class GILA_BOSS_51h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_51h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}