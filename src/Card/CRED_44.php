<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Walter Kong
 */
class CRED_44 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_44';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}