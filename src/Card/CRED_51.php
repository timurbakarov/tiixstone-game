<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jerry Cheng
 */
class CRED_51 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_51';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}