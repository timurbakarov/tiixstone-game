<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Squire
 */
class CS2_152 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_152';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}