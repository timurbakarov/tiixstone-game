<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Resourceful
 */
class GILA_593 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_593';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}