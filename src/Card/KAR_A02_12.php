<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Silverware Golem
 */
class KAR_A02_12 extends Hero
{
    public function globalId() : string
    {
        return 'KAR_A02_12';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}