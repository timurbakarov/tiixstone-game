<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\TakeDamage;
use Tiixstone\PlayCardCondition;
use Tiixstone\Block\HealCharacter;
use Tiixstone\Effect\AmethystSpellstoneEffect;

/**
 * Lesser Amethyst Spellstone
 */
class LOOT_043 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOOT_043';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new TakeDamage($target, $this->damage(), $this),
            new HealCharacter($game->currentPlayer()->hero, $this->damage(), $this),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion(),
        ]);
    }

    /**
     * @return int
     */
    public function damage()
    {
        return 3;
    }

    /**
     * @return array
     */
    public function handEffects()
    {
        return [AmethystSpellstoneEffect::class];
    }
}