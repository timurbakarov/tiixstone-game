<?php

namespace Tiixstone\Card;

/**
 * Lost Spirit
 */
class GIL_513 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_513';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}