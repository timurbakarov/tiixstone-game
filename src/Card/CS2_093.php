<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;
use Tiixstone\Block\TakeDamage;

/**
 * Consecration
 */
class CS2_093 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_093';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
		$characters = $game->targetManager->allEnemyCharacters($game, $game->currentPlayer());
		
		$blocks = [];
		foreach($characters as $character) {
			$blocks[] = new TakeDamage($character, $game->withSpellDamage(2), $this);
		}
		
        return $blocks;
    }
}