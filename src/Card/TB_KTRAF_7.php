<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Heigan the Unclean
 */
class TB_KTRAF_7 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_KTRAF_7';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}