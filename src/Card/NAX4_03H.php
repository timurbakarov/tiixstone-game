<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Skeleton
 */
class NAX4_03H extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX4_03H';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}