<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Pitcher
 */
class KAR_A02_06 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_A02_06';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}