<?php

namespace Tiixstone\Card;

/**
 * Toki, Time-Tinker
 */
class GILA_900h2 extends Hero
{
    public function globalId() : string
    {
        return 'GILA_900h2';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}