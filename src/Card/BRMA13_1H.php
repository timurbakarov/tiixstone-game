<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Lord Victor Nefarius
 */
class BRMA13_1H extends Hero
{
    public function globalId() : string
    {
        return 'BRMA13_1H';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}