<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Piranha Launcher
 */
class CFM_337 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_337';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 5;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}