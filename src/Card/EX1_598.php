<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Imp
 */
class EX1_598 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_598';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}