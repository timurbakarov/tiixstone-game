<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Skeletal Smith
 */
class NAXM_002 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAXM_002';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}