<?php

namespace Tiixstone\Card;

/**
 * Gurubashi Chicken
 */
class TRL_506 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_506';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}