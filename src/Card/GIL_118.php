<?php

namespace Tiixstone\Card;

/**
 * Deranged Doctor
 */
class GIL_118 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_118';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}