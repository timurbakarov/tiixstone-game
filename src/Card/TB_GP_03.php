<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Shadow Tower New
 */
class TB_GP_03 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_GP_03';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}