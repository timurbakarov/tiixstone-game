<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Flame of Azzinoth
 */
class EX1_614t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_614t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}