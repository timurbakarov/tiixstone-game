<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Alleria Windrunner
 */
class HERO_05a extends Hero
{
    public function globalId() : string
    {
        return 'HERO_05a';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}