<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Onyxia
 */
class BRMA17_3 extends Hero
{
    public function globalId() : string
    {
        return 'BRMA17_3';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}