<?php

namespace Tiixstone\Card;

/**
 * Weaponized Zombie
 */
class TRLA_148 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_148';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}