<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Poisoned Dagger
 */
class AT_132_ROGUEt_H1 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_132_ROGUEt_H1';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}