<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Feugen
 */
class NAX13_04H extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX13_04H';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}