<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect;

/**
 * Wrath of Air Totem
 */
class CS2_052 extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::TOTEM;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_052';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [new Effect\SpellDamageEffect(1)];
    }
}