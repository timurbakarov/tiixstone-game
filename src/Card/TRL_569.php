<?php

namespace Tiixstone\Card;

/**
 * Crowd Roaster
 */
class TRL_569 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_569';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}