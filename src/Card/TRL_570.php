<?php

namespace Tiixstone\Card;

/**
 * Soup Vendor
 */
class TRL_570 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_570';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}