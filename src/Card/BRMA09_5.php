<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Dismount
 */
class BRMA09_5 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'BRMA09_5';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}