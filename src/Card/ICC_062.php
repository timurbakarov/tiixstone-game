<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mountainfire Armor
 */
class ICC_062 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_062';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}