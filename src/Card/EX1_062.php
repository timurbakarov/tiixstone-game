<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\OldMurkEyeEffect;
use Tiixstone\Race;

/**
 * Old Murk-Eye
 */
class EX1_062 extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::MURLOC;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_062';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [OldMurkEyeEffect::class];
    }
}