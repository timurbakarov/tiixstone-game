<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Nerubian
 */
class NAX1_03 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX1_03';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}