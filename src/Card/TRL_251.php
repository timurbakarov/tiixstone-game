<?php

namespace Tiixstone\Card;

/**
 * Spirit of the Bat
 */
class TRL_251 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_251';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}