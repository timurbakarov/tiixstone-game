<?php

namespace Tiixstone\Card;

use Tiixstone\Block\IncreaseArmor;
use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Armor Up!
 */
class CS2_102_H1 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'CS2_102_H1';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [
            new IncreaseArmor($game->currentPlayer()->hero, $this->defaultArmorAmount()),
        ];
    }

    /**
     * @return int
     */
    private function defaultArmorAmount()
    {
        return 2;
    }
}