<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Intrepid Dragonstalker
 */
class TB_Coopv3_103 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_Coopv3_103';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}