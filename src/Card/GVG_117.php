<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Gazlowe
 */
class GVG_117 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_117';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}