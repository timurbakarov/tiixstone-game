<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Djinni of Zephyrs
 */
class LOE_053 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_053';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}