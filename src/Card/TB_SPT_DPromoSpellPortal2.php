<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * Stampede
 */
class TB_SPT_DPromoSpellPortal2 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_SPT_DPromoSpellPortal2';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}