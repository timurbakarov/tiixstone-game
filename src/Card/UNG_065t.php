<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Sherazin, Seed
 */
class UNG_065t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_065t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 11;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}