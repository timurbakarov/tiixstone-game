<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Party Barracks
 */
class TB_SPT_MTH_BossHeroPower extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TB_SPT_MTH_BossHeroPower';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}