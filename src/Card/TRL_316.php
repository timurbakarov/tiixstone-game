<?php

namespace Tiixstone\Card;

/**
 * Jan'alai, the Dragonhawk
 */
class TRL_316 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_316';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}