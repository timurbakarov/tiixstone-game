<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Noth the Plaguebringer
 */
class TB_KTRAF_10 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_KTRAF_10';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}