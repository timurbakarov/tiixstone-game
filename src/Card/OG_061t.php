<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mastiff
 */
class OG_061t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_061t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}