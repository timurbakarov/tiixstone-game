<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Warglaive of Azzinoth
 */
class TU4e_004 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TU4e_004';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}