<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hozen Healer
 */
class CFM_067 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_067';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}