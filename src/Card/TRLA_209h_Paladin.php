<?php

namespace Tiixstone\Card;

/**
 * Rikkar
 */
class TRLA_209h_Paladin extends Hero
{
    public function globalId() : string
    {
        return 'TRLA_209h_Paladin';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}