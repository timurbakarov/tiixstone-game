<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Trogg Hate Minions!
 */
class LOEA05_02ha extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'LOEA05_02ha';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}