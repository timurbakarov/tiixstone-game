<?php

namespace Tiixstone\Card;

/**
 * Blink Fox
 */
class GIL_827 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_827';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}