<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Alternate Timeline
 */
class GILA_BOSS_48t extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_BOSS_48t';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 10;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}