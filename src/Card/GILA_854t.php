<?php

namespace Tiixstone\Card;

/**
 * Beautiful Beast
 */
class GILA_854t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_854t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}