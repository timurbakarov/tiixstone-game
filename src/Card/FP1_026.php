<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\Deathrattle\AnubarAmbusherDeathrattle;

/**
 * Anub'ar Ambusher
 */
class FP1_026 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_026';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [AnubarAmbusherDeathrattle::class];
    }
}