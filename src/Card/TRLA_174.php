<?php

namespace Tiixstone\Card;

/**
 * Ironhorn Drummer
 */
class TRLA_174 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_174';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}