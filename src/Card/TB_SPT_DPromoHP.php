<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Diabolical Powers
 */
class TB_SPT_DPromoHP extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TB_SPT_DPromoHP';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}