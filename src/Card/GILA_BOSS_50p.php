<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Echoes of the Witchwood
 */
class GILA_BOSS_50p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_50p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}