<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jason DeFord
 */
class CRED_66 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_66';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 10;
    }
}