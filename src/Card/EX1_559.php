<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Archmage Antonidas
 */
class EX1_559 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_559';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}