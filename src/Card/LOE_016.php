<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Rumbling Elemental
 */
class LOE_016 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_016';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}