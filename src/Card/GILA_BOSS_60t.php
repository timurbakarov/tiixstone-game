<?php

namespace Tiixstone\Card;

/**
 * Cursed Crewmember
 */
class GILA_BOSS_60t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_BOSS_60t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}