<?php

namespace Tiixstone\Card;

/**
 * Ironhide Direhorn
 */
class TRL_232 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_232';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}