<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Exception;

abstract class Hero extends Character
{
    /**
     * @var Weapon
     */
    public $weapon;

    /**
     * @var Power
     */
    public $power;

    /**
     * @var bool
     */
    protected $exhausted = false;

    /**
     * @return Power
     */
    abstract public function defaultPower() : Power;

    /**
     * @param Power $power
     * @return $this
     */
	public function setPower(Power $power)
	{
		$this->power = $power;

		return $this;
	}

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth() : int
    {
        return 30;
    }

    /**
     * @return int
     */
    public function defaultAttackRate() : int
    {
        return 0;
    }

    /**
     * @param Weapon $weapon
     * @return $this
     */
    public function setWeapon(Weapon $weapon = null)
    {
        $this->weapon = $weapon;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasWeapon() : bool
    {
        return $this->weapon !== null;
    }

    /**
     * @param Game $game
     * @param int|null $positionCardId
     * @param Character|null $target
     * @return array
     * @throws Exception
     */
    public function play(Game $game, Minion $positionMinion = null, Character $target = null) : array
    {
        throw new Exception('Not implemented');
    }

    /**
     * @return array
     */
    public function destroy(Game $game) : array
    {
        $game->logBlock('hero-destroyed', ['hero' => $this]);

        $game->effects->removeByCard($this);

        $blocks = [];

        if($this->hasWeapon()) {
            $blocks[] = new Block\MarkDestroyed($this->weapon);
        }

        return $blocks;
    }
}