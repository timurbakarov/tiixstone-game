<?php

namespace Tiixstone\Card;

/**
 * Winslow Tobtock
 */
class GILA_BOSS_64h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_64h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}