<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Crypt Lord
 */
class ICC_808 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_808';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}