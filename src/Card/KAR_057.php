<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ivory Knight
 */
class KAR_057 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_057';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}