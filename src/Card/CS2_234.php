<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\PlayCardCondition;
use Tiixstone\Block\MarkDestroyed;

/**
 * Shadow Word: Pain
 */
class CS2_234 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_234';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new MarkDestroyed($target)];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Minion(),
            new Condition\Target\AttackLowerThan(4),
        ]);
    }
}