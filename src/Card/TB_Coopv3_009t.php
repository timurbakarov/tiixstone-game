<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Explosive Rune
 */
class TB_Coopv3_009t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_Coopv3_009t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}