<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ghoul Infestor
 */
class ICC_085t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_085t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}