<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Massive Runeblade
 */
class NAX7_04H extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX7_04H';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 10;
    }
}