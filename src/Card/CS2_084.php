<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect\HealthChangerEffect;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Hunter's Mark
 */
class CS2_084 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_084';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new GiveEffect($target, new HealthChangerEffect(1, true))];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
        ]);
    }
}