<?php

namespace Tiixstone\Card;

/**
 * Princess Talanji
 */
class TRLA_208h extends Hero
{
    public function globalId() : string
    {
        return 'TRLA_208h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}