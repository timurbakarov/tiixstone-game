<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Pristine Compass
 */
class GILA_853b extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_853b';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}