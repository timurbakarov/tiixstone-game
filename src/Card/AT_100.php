<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Silver Hand Regent
 */
class AT_100 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_100';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}