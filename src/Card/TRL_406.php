<?php

namespace Tiixstone\Card;

/**
 * Dozing Marksman
 */
class TRL_406 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_406';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}