<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Block of Ice
 */
class ICCA04_004 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA04_004';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}