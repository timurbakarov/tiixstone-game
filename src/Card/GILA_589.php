<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Old Bones
 */
class GILA_589 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_589';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}