<?php

namespace Tiixstone\Card;

/**
 * Hunting Mastiff
 */
class GIL_607t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_607t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}