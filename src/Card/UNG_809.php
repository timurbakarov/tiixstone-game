<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Fire Fly
 */
class UNG_809 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_809';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}