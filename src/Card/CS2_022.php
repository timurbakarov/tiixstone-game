<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\Transform;
use Tiixstone\Card\Alias\Sheep;
use Tiixstone\PlayCardCondition;

/**
 * Polymorph
 */
class CS2_022 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_022';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new Transform($target, Sheep::create())];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
        ]);
    }
}