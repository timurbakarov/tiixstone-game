<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Fireblast Rank 2
 */
class CS2_034_H1_AT_132 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'CS2_034_H1_AT_132';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}