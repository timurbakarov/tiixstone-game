<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Rock
 */
class LOE_016t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_016t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}