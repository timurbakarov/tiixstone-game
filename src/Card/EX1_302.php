<?php

namespace Tiixstone\Card;

use Tiixstone\Block\ClosureBlock;
use Tiixstone\Block\DrawCard;
use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\Block\TakeDamage;
use Tiixstone\PlayCardCondition;

/**
 * Mortal Coil
 */
class EX1_302 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_302';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Minion,
        ]);
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new TakeDamage($target, $game->withSpellDamage($this->damage())),
            new ClosureBlock($this->drawCardifMinionDies($game, $target)),
        ];
    }

    /**
     * @return \Closure
     */
    private function drawCardifMinionDies(Game $game, Minion $target)
    {
        return function() use($game, $target) {
            if($target->isDestroyed()) {
                return [new DrawCard($game->currentPlayer())];
            } else {
                return [];
            }
        };
    }

    /**
     * @return int
     */
    private function damage()
    {
        return 1;
    }
}