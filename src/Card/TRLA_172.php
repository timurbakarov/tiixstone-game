<?php

namespace Tiixstone\Card;

/**
 * Rallying Quilboar
 */
class TRLA_172 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_172';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}