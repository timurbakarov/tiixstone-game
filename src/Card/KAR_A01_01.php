<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Magic Mirror
 */
class KAR_A01_01 extends Hero
{
    public function globalId() : string
    {
        return 'KAR_A01_01';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}