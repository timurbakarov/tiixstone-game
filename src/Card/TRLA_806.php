<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Wrath of the Loas
 */
class TRLA_806 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_806';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}