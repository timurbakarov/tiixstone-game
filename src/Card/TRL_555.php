<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Demonbolt
 */
class TRL_555 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_555';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 8;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}