<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Sindragosa
 */
class FB_LK_014h extends Hero
{
    public function globalId() : string
    {
        return 'FB_LK_014h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}