<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Frozen Crusher
 */
class UNG_079 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_079';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}