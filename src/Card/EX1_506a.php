<?php

namespace Tiixstone\Card;

use Tiixstone\Race;

/**
 * Murloc Scout
 */
class EX1_506a extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::MURLOC;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_506a';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}