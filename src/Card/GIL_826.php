<?php

namespace Tiixstone\Card;

/**
 * Baku the Mooneater
 */
class GIL_826 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_826';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}