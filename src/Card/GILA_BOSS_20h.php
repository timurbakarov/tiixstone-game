<?php

namespace Tiixstone\Card;

/**
 * A Mangy Wolf
 */
class GILA_BOSS_20h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_20h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}