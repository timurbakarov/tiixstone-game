<?php

namespace Tiixstone\Card;

/**
 * Spellzerker
 */
class TRL_312 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_312';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}