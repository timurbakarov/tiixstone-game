<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Supreme Lich King
 */
class FB_LK_Raid_Hero_Battledamaged extends Hero
{
    public function globalId() : string
    {
        return 'FB_LK_Raid_Hero_Battledamaged';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}