<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Razorgore's Claws
 */
class BRMA10_6 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA10_6';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 5;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}