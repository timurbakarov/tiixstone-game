<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Chillblade Champion
 */
class ICC_820 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_820';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}