<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Southsea Deckhand
 */
class CS2_146 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_146';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}