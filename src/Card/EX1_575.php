<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect\Trigger\ManaTideTotemEffect;

/**
 * Mana Tide Totem
 */
class EX1_575 extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::TOTEM;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_575';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [ManaTideTotemEffect::class];
    }
}