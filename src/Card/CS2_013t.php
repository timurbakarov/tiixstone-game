<?php

namespace Tiixstone\Card;

use Tiixstone\Block\DrawCard;
use Tiixstone\Game;

/**
 * Excess Mana
 */
class CS2_013t extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_013t';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new DrawCard($this->getPlayer())];
    }
}