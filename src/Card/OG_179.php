<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Fiery Bat
 */
class OG_179 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_179';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}