<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Gentle Megasaur
 */
class UNG_089 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_089';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}