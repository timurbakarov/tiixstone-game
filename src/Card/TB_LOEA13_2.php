<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Ancient Power
 */
class TB_LOEA13_2 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TB_LOEA13_2';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}