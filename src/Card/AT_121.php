<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Crowd Favorite
 */
class AT_121 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_121';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}