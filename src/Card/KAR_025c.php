<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Teapot
 */
class KAR_025c extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_025c';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}