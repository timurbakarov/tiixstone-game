<?php

namespace Tiixstone\Card;

/**
 * Seabreaker Goliath
 */
class GILA_612 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_612';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 15;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 15;
    }
}