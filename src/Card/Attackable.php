<?php

namespace Tiixstone\Card;

use Tiixstone\Stats\Attack;
use Tiixstone\AttackCondition;

abstract class Attackable extends Destroyable
{
    /**
     * @var Attack
     */
    public $attack;

    /**
     * @var AttackCondition
     */
    public $attackCondition;

    public function __construct()
    {
        parent::__construct();

        $this->attack = new Attack($this->defaultAttackRate());
        $this->setAttackCondition($this->defaultAttackCondition());
    }

    /**
     * @return int
     */
    abstract public function defaultAttackRate() : int;

    /**
     * @return AttackCondition
     * @throws \Tiixstone\Exception
     */
    public function defaultAttackCondition() : AttackCondition
    {
        return new AttackCondition([]);
    }

    /**
     * @return AttackCondition
     */
    public function attackCondition() : AttackCondition
    {
        return $this->attackCondition;
    }

    /**
     * @param AttackCondition $attackCondition
     * @return $this
     */
    public function setAttackCondition(AttackCondition $attackCondition)
    {
        $this->attackCondition = $attackCondition;

        return $this;
    }
}