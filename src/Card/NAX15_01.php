<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Kel'Thuzad
 */
class NAX15_01 extends Hero
{
    public function globalId() : string
    {
        return 'NAX15_01';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}