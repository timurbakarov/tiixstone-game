<?php

namespace Tiixstone\Card;

/**
 * Lobstrok Tastetester
 */
class TRLA_192 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_192';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}