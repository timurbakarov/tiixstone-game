<?php

namespace Tiixstone\Card;

/**
 * Hakkar, the Soulflayer
 */
class TRL_541 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_541';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 9;
    }
}