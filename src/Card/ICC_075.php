<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\Trigger\DespicableDreadlordEffect;

/**
 * Despicable Dreadlord
 */
class ICC_075 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_075';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [DespicableDreadlordEffect::class];
    }
}