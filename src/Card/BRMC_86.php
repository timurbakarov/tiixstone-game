<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Atramedes
 */
class BRMC_86 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMC_86';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}