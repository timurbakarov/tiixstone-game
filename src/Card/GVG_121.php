<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Clockwork Giant
 */
class GVG_121 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_121';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 12;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}