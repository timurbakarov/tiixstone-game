<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Animated Statue
 */
class LOEA04_27 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA04_27';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 10;
    }
}