<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Ragnaros the Firelord
 */
class TBA01_1 extends Hero
{
    public function globalId() : string
    {
        return 'TBA01_1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}