<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\PlayCardCondition;

/**
 * Cold Blood
 */
class CS2_073 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_073';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new Block\GiveEffect($target, new Effect\AttackChangerEffect(2))];
    }

    /**
     * @return array
     */
    public function handEffects()
    {
        return [Effect\Combo\ColdBloodEffect::class];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
        ]);
    }
}