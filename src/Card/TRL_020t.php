<?php

namespace Tiixstone\Card;

/**
 * Bat
 */
class TRL_020t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_020t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}