<?php

namespace Tiixstone\Card;

/**
 * Druid of the Scythe
 */
class GIL_188t3 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_188t3';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}