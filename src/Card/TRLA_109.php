<?php

namespace Tiixstone\Card;

/**
 * Krag'wa's Lure
 */
class TRLA_109 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_109';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}