<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dark Cultist
 */
class FP1_023 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_023';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}