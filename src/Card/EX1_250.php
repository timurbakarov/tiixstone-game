<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Earth Elemental
 */
class EX1_250 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_250';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}