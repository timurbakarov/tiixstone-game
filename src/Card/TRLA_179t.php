<?php

namespace Tiixstone\Card;

/**
 * Hir'eek's Hunger
 */
class TRLA_179t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_179t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}