<?php

namespace Tiixstone\Card;

/**
 * Swamp Dragon Egg
 */
class GIL_816 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_816';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}