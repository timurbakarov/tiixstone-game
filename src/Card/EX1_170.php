<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Emperor Cobra
 */
class EX1_170 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_170';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}