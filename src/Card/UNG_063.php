<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Biteweed
 */
class UNG_063 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_063';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}