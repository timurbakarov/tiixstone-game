<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;
use Tiixstone\Effect\TruesilverChampionEffect;
use Tiixstone\Game;

/**
 * Truesilver Champion
 */
class CS2_097 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_097';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
	
	/**
     * @return array
     */
    public function equippedEffects() : array
    {
        return [TruesilverChampionEffect::class];
    }
}