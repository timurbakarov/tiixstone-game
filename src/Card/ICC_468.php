<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Wretched Tiller
 */
class ICC_468 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_468';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}