<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ultrasaur
 */
class UNG_806 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_806';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 14;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}