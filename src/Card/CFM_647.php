<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Blowgill Sniper
 */
class CFM_647 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_647';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}