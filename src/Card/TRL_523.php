<?php

namespace Tiixstone\Card;

/**
 * Firetree Witchdoctor
 */
class TRL_523 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_523';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}