<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Whelp
 */
class BRM_004t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_004t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}