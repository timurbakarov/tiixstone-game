<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Deadscale Knight
 */
class ICC_220 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_220';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}