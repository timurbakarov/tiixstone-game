<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Searing Totem
 */
class AT_132_SHAMANb extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_132_SHAMANb';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}