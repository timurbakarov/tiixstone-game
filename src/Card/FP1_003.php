<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Echoing Ooze
 */
class FP1_003 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_003';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}