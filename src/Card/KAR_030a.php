<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Pantry Spider
 */
class KAR_030a extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_030a';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}