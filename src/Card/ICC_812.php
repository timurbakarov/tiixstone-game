<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Meat Wagon
 */
class ICC_812 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_812';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}