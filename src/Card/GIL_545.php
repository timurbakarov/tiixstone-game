<?php

namespace Tiixstone\Card;

/**
 * Ghostly Charger
 */
class GIL_545 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_545';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}