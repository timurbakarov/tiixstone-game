<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Eater of Secrets
 */
class OG_254 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_254';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}