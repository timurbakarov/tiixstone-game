<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Volatile Elemental
 */
class UNG_818 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_818';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}