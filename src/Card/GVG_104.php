<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hobgoblin
 */
class GVG_104 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_104';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}