<?php

namespace Tiixstone\Card;

use Tiixstone\Block\Overload;
use Tiixstone\Block\TakeDamage;
use Tiixstone\Game;

/**
 * Lightning Storm
 */
class EX1_259 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_259';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $targets = $game->targetManager->allEnemyMinions($game, $this->getPlayer());

        $blocks = array_map(function(Minion $minion) use($game) {
            $damage = $game->RNG->int(2, 3);

            return new TakeDamage($minion, $game->withSpellDamage($damage), $this);
        }, $targets);

        $blocks[] = new Overload($this->getPlayer(), 2);

        return $blocks;
    }
}