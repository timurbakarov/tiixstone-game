<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect\HealthChangerEffect;

/**
 * Rocky Carapace
 */
class UNG_999t4 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_999t4';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new GiveEffect($target, new HealthChangerEffect(3))];
    }
}