<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Violet Apprentice
 */
class NEW1_026t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_026t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}