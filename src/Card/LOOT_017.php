<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\PlayCardCondition;
use Tiixstone\Block\MarkDestroyed;
use Tiixstone\Block\HealCharacter;

/**
 * Dark Pact
 */
class LOOT_017 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOOT_017';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $game->logBlock('dark-pact');

        return [
            new MarkDestroyed($target),
            new HealCharacter($game->currentPlayer()->hero, $this->healAmount()),
        ];
    }

    /**
     * @return int
     */
    private function healAmount() : int
    {
        return 8;
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Friendly,
            new Condition\Target\Minion,
        ]);
    }
}