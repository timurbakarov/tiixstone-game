<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Grimestreet Smuggler
 */
class CFM_853 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_853';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}