<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Trembling
 */
class KARA_05_01hp extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'KARA_05_01hp';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}