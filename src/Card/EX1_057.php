<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ancient Brewmaster
 */
class EX1_057 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_057';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}