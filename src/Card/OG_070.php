<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Bladed Cultist
 */
class OG_070 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_070';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}