<?php

namespace Tiixstone\Card;

use Tiixstone\Block\SetManaAvailable;
use Tiixstone\Game;

/**
 * Counterfeit Coin
 */
class CFM_630 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_630';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new SetManaAvailable($this->getPlayer(), $this->getPlayer()->availableMana() + 1),
        ];
    }
}