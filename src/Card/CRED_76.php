<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Nicholas Kinney
 */
class CRED_76 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_76';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}