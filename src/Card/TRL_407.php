<?php

namespace Tiixstone\Card;

/**
 * Waterboy
 */
class TRL_407 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_407';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}