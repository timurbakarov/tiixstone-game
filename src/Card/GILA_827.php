<?php

namespace Tiixstone\Card;

/**
 * Murloc Holmes
 */
class GILA_827 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_827';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}