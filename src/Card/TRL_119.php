<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * The Beast Within
 */
class TRL_119 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_119';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}