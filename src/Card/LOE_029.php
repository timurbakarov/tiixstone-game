<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jeweled Scarab
 */
class LOE_029 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_029';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}