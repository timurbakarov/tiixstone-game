<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Val'kyr Soulclaimer
 */
class ICC_408 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_408';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}