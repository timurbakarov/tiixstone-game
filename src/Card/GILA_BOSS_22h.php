<?php

namespace Tiixstone\Card;

/**
 * Cutthroat Willie
 */
class GILA_BOSS_22h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_22h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}