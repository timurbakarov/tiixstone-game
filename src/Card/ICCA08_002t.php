<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ghoul
 */
class ICCA08_002t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA08_002t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}