<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Ferocious Howl
 */
class GIL_637 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_637';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}