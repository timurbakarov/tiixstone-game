<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\Weapon\EquipWeapon;
use Tiixstone\Card\Alias\PoisonedDagger;

/**
 * Poisoned Daggers
 */
class AT_132_ROGUE extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'AT_132_ROGUE';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [new EquipWeapon($this->getPlayer(), PoisonedDagger::create())];
    }
}