<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Nadia, Mankrik's Wife
 */
class TB_MnkWf01 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_MnkWf01';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}