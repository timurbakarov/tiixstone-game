<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\IncreaseArmor;

/**
 * Scarab Shell
 */
class ICC_832pa extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_832pa';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new IncreaseArmor($game->currentPlayer()->hero, 3),
        ];
    }
}