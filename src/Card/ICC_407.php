<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Gnomeferatu
 */
class ICC_407 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_407';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}