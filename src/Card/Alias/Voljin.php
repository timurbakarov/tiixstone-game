<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Voljin
{
    public static function globalId() : string
    {
        return 'GVG_014';
    }

    public static function create()
    {
        return new Card\GVG_014;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_014';
    }
}