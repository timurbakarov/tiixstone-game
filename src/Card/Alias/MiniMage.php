<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MiniMage
{
    public static function globalId() : string
    {
        return 'GVG_109';
    }

    public static function create()
    {
        return new Card\GVG_109;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_109';
    }
}