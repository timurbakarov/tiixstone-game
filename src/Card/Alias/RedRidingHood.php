<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RedRidingHood
{
    public static function globalId() : string
    {
        return 'KARA_13_19';
    }

    public static function create()
    {
        return new Card\KARA_13_19;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_13_19';
    }
}