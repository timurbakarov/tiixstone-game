<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LocustSwarm
{
    public static function globalId() : string
    {
        return 'NAX1_05';
    }

    public static function create()
    {
        return new Card\NAX1_05;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX1_05';
    }
}