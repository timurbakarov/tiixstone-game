<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GreaterArcaneMissiles
{
    public static function globalId() : string
    {
        return 'CFM_623';
    }

    public static function create()
    {
        return new Card\CFM_623;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_623';
    }
}