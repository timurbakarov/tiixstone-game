<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Present
{
    public static function globalId() : string
    {
        return 'TB_Presents_001';
    }

    public static function create()
    {
        return new Card\TB_Presents_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_Presents_001';
    }
}