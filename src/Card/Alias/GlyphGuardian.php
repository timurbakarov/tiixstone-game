<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GlyphGuardian
{
    public static function globalId() : string
    {
        return 'TRLA_131';
    }

    public static function create()
    {
        return new Card\TRLA_131;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_131';
    }
}