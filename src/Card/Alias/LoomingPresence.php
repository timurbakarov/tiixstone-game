<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LoomingPresence
{
    public static function globalId() : string
    {
        return 'LOEA_01H';
    }

    public static function create()
    {
        return new Card\LOEA_01H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA_01H';
    }
}