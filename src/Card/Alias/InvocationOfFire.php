<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class InvocationOfFire
{
    public static function globalId() : string
    {
        return 'UNG_211c';
    }

    public static function create()
    {
        return new Card\UNG_211c;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_211c';
    }
}