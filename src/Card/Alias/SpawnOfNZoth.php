<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpawnOfNZoth
{
    public static function globalId() : string
    {
        return 'OG_256';
    }

    public static function create()
    {
        return new Card\OG_256;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_256';
    }
}