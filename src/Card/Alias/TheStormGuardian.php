<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheStormGuardian
{
    public static function globalId() : string
    {
        return 'CFM_324t';
    }

    public static function create()
    {
        return new Card\CFM_324t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_324t';
    }
}