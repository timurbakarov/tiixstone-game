<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JadeIdol
{
    public static function globalId() : string
    {
        return 'CFM_602';
    }

    public static function create()
    {
        return new Card\CFM_602;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_602';
    }
}