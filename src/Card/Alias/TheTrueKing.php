<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheTrueKing
{
    public static function globalId() : string
    {
        return 'ICCA08_027';
    }

    public static function create()
    {
        return new Card\ICCA08_027;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA08_027';
    }
}