<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShieldedMinibot
{
    public static function globalId() : string
    {
        return 'GVG_058';
    }

    public static function create()
    {
        return new Card\GVG_058;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_058';
    }
}