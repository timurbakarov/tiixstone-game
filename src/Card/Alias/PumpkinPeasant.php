<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PumpkinPeasant
{
    public static function globalId() : string
    {
        return 'GIL_201t';
    }

    public static function create()
    {
        return new Card\GIL_201t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_201t';
    }
}