<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LightfusedStegodon
{
    public static function globalId() : string
    {
        return 'UNG_962';
    }

    public static function create()
    {
        return new Card\UNG_962;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_962';
    }
}