<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class V07TR0N
{
    public static function globalId() : string
    {
        return 'GVG_111t';
    }

    public static function create()
    {
        return new Card\GVG_111t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_111t';
    }
}