<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DireWolfAlpha
{
    public static function globalId() : string
    {
        return 'EX1_162';
    }

    public static function create()
    {
        return new Card\EX1_162;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_162';
    }
}