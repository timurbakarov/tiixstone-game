<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GnomereganInfantry
{
    public static function globalId() : string
    {
        return 'GVG_098';
    }

    public static function create()
    {
        return new Card\GVG_098;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_098';
    }
}