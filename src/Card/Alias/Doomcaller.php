<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Doomcaller
{
    public static function globalId() : string
    {
        return 'OG_255';
    }

    public static function create()
    {
        return new Card\OG_255;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_255';
    }
}