<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SuddenGenesis
{
    public static function globalId() : string
    {
        return 'UNG_927';
    }

    public static function create()
    {
        return new Card\UNG_927;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_927';
    }
}