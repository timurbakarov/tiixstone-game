<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DariusCrowley
{
    public static function globalId() : string
    {
        return 'GIL_547';
    }

    public static function create()
    {
        return new Card\GIL_547;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_547';
    }
}