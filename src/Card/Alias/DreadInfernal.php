<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DreadInfernal
{
    public static function globalId() : string
    {
        return 'CS2_064';
    }

    public static function create()
    {
        return new Card\CS2_064;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_064';
    }
}