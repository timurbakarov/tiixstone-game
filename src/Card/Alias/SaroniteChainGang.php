<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SaroniteChainGang
{
    public static function globalId() : string
    {
        return 'ICC_466';
    }

    public static function create()
    {
        return new Card\ICC_466;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_466';
    }
}