<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Arcanosmith
{
    public static function globalId() : string
    {
        return 'KAR_710';
    }

    public static function create()
    {
        return new Card\KAR_710;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_710';
    }
}