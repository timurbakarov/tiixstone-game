<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DwarfDemolitionist
{
    public static function globalId() : string
    {
        return 'TB_FW_Mortar';
    }

    public static function create()
    {
        return new Card\TB_FW_Mortar;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_FW_Mortar';
    }
}