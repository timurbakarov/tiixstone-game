<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OrneryTortoise
{
    public static function globalId() : string
    {
        return 'TRL_546';
    }

    public static function create()
    {
        return new Card\TRL_546;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_546';
    }
}