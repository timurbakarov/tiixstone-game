<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HellBovineChampion
{
    public static function globalId() : string
    {
        return 'TB_SPT_DPromoMinionChamp';
    }

    public static function create()
    {
        return new Card\TB_SPT_DPromoMinionChamp;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_SPT_DPromoMinionChamp';
    }
}