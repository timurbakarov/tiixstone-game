<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WhisperOfDeath
{
    public static function globalId() : string
    {
        return 'ICCA10_009p';
    }

    public static function create()
    {
        return new Card\ICCA10_009p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA10_009p';
    }
}