<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Biteweed
{
    public static function globalId() : string
    {
        return 'UNG_063';
    }

    public static function create()
    {
        return new Card\UNG_063;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_063';
    }
}