<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PatNagle
{
    public static function globalId() : string
    {
        return 'CRED_85';
    }

    public static function create()
    {
        return new Card\CRED_85;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_85';
    }
}