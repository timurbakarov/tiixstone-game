<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PrimalFusion
{
    public static function globalId() : string
    {
        return 'OG_023';
    }

    public static function create()
    {
        return new Card\OG_023;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_023';
    }
}