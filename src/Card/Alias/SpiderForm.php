<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpiderForm
{
    public static function globalId() : string
    {
        return 'ICC_051a';
    }

    public static function create()
    {
        return new Card\ICC_051a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_051a';
    }
}