<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Powershot
{
    public static function globalId() : string
    {
        return 'AT_056';
    }

    public static function create()
    {
        return new Card\AT_056;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_056';
    }
}