<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MoshOggAnnouncer
{
    public static function globalId() : string
    {
        return 'TRL_532';
    }

    public static function create()
    {
        return new Card\TRL_532;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_532';
    }
}