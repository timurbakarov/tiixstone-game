<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CoreHoundPuppies
{
    public static function globalId() : string
    {
        return 'BRMC_95h';
    }

    public static function create()
    {
        return new Card\BRMC_95h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_95h';
    }
}