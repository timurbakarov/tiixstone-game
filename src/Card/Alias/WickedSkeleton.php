<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WickedSkeleton
{
    public static function globalId() : string
    {
        return 'ICC_904';
    }

    public static function create()
    {
        return new Card\ICC_904;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_904';
    }
}