<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NecroticAura
{
    public static function globalId() : string
    {
        return 'NAX6_02';
    }

    public static function create()
    {
        return new Card\NAX6_02;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX6_02';
    }
}