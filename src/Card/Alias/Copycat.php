<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Copycat
{
    public static function globalId() : string
    {
        return 'GILA_595';
    }

    public static function create()
    {
        return new Card\GILA_595;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_595';
    }
}