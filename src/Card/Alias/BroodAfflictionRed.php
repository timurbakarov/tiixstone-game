<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BroodAfflictionRed
{
    public static function globalId() : string
    {
        return 'BRMA12_3H';
    }

    public static function create()
    {
        return new Card\BRMA12_3H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA12_3H';
    }
}