<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IgniteMana
{
    public static function globalId() : string
    {
        return 'BRMA05_2';
    }

    public static function create()
    {
        return new Card\BRMA05_2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA05_2';
    }
}