<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Cauldron
{
    public static function globalId() : string
    {
        return 'LOEA09_7';
    }

    public static function create()
    {
        return new Card\LOEA09_7;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA09_7';
    }
}