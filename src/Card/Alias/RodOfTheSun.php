<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RodOfTheSun
{
    public static function globalId() : string
    {
        return 'LOEA01_11h';
    }

    public static function create()
    {
        return new Card\LOEA01_11h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA01_11h';
    }
}