<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FelCannon
{
    public static function globalId() : string
    {
        return 'GVG_020';
    }

    public static function create()
    {
        return new Card\GVG_020;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_020';
    }
}