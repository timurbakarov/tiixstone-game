<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BaronGeddon
{
    public static function globalId() : string
    {
        return 'BRMA05_1';
    }

    public static function create()
    {
        return new Card\BRMA05_1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA05_1';
    }
}