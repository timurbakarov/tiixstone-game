<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheFourHorsemen
{
    public static function globalId() : string
    {
        return 'ICC_829p';
    }

    public static function create()
    {
        return new Card\ICC_829p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_829p';
    }
}