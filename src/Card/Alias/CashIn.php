<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CashIn
{
    public static function globalId() : string
    {
        return 'TP_Bling_HP2';
    }

    public static function create()
    {
        return new Card\TP_Bling_HP2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TP_Bling_HP2';
    }
}