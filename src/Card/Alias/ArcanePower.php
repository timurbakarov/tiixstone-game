<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArcanePower
{
    public static function globalId() : string
    {
        return 'KARA_00_06';
    }

    public static function create()
    {
        return new Card\KARA_00_06;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_00_06';
    }
}