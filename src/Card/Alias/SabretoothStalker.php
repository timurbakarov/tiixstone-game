<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SabretoothStalker
{
    public static function globalId() : string
    {
        return 'UNG_812';
    }

    public static function create()
    {
        return new Card\UNG_812;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_812';
    }
}