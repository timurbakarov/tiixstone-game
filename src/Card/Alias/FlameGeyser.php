<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FlameGeyser
{
    public static function globalId() : string
    {
        return 'UNG_018';
    }

    public static function create()
    {
        return new Card\UNG_018;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_018';
    }
}