<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Tiger
{
    public static function globalId() : string
    {
        return 'TRL_309t';
    }

    public static function create()
    {
        return new Card\TRL_309t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_309t';
    }
}