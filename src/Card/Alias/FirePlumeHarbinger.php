<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FirePlumeHarbinger
{
    public static function globalId() : string
    {
        return 'UNG_202';
    }

    public static function create()
    {
        return new Card\UNG_202;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_202';
    }
}