<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SenjinShieldmasta
{
    public static function globalId() : string
    {
        return 'CS2_179';
    }

    public static function create()
    {
        return new Card\CS2_179;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_179';
    }
}