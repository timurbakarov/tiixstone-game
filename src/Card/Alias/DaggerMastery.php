<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DaggerMastery
{
    public static function globalId() : string
    {
        return 'CS2_083b';
    }

    public static function create()
    {
        return new Card\CS2_083b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_083b';
    }
}