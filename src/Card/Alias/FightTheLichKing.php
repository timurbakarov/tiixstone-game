<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FightTheLichKing
{
    public static function globalId() : string
    {
        return 'FB_LKStats001a';
    }

    public static function create()
    {
        return new Card\FB_LKStats001a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LKStats001a';
    }
}