<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FinjaTheFlyingStar
{
    public static function globalId() : string
    {
        return 'CFM_344';
    }

    public static function create()
    {
        return new Card\CFM_344;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_344';
    }
}