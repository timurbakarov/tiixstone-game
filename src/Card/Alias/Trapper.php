<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Trapper
{
    public static function globalId() : string
    {
        return 'GILA_596';
    }

    public static function create()
    {
        return new Card\GILA_596;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_596';
    }
}