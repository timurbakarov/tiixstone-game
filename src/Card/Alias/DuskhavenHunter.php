<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DuskhavenHunter
{
    public static function globalId() : string
    {
        return 'GIL_200t';
    }

    public static function create()
    {
        return new Card\GIL_200t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_200t';
    }
}