<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RumbleKings
{
    public static function globalId() : string
    {
        return 'TRLA_Druid_13';
    }

    public static function create()
    {
        return new Card\TRLA_Druid_13;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Druid_13';
    }
}