<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RedcrestRocker
{
    public static function globalId() : string
    {
        return 'TRLA_173';
    }

    public static function create()
    {
        return new Card\TRLA_173;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_173';
    }
}