<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ANewChallenger
{
    public static function globalId() : string
    {
        return 'TRL_305';
    }

    public static function create()
    {
        return new Card\TRL_305;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_305';
    }
}