<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LordJaraxxusMinion
{
    public static function globalId() : string
    {
        return 'EX1_323';
    }

    public static function create()
    {
        return new Card\EX1_323;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_323';
    }
}