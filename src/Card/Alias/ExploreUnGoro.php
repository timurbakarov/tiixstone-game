<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ExploreUnGoro
{
    public static function globalId() : string
    {
        return 'UNG_922';
    }

    public static function create()
    {
        return new Card\UNG_922;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_922';
    }
}