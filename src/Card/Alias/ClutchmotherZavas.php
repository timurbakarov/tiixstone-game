<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ClutchmotherZavas
{
    public static function globalId() : string
    {
        return 'UNG_836';
    }

    public static function create()
    {
        return new Card\UNG_836;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_836';
    }
}