<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RazzleDazzler
{
    public static function globalId() : string
    {
        return 'TRLA_130';
    }

    public static function create()
    {
        return new Card\TRLA_130;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_130';
    }
}