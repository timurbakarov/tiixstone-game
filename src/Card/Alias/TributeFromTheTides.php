<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TributeFromTheTides
{
    public static function globalId() : string
    {
        return 'TRLA_154t';
    }

    public static function create()
    {
        return new Card\TRLA_154t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_154t';
    }
}