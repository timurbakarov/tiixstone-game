<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WarlockOnFire
{
    public static function globalId() : string
    {
        return 'ICCA01_011';
    }

    public static function create()
    {
        return new Card\ICCA01_011;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA01_011';
    }
}