<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BroodAfflictionBlue
{
    public static function globalId() : string
    {
        return 'BRMA12_5H';
    }

    public static function create()
    {
        return new Card\BRMA12_5H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA12_5H';
    }
}