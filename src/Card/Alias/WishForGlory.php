<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WishForGlory
{
    public static function globalId() : string
    {
        return 'LOEA02_05';
    }

    public static function create()
    {
        return new Card\LOEA02_05;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA02_05';
    }
}