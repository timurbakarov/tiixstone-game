<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Scavenge
{
    public static function globalId() : string
    {
        return 'GILA_500p2';
    }

    public static function create()
    {
        return new Card\GILA_500p2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_500p2';
    }
}