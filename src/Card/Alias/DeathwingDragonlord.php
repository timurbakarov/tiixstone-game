<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DeathwingDragonlord
{
    public static function globalId() : string
    {
        return 'OG_317';
    }

    public static function create()
    {
        return new Card\OG_317;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_317';
    }
}