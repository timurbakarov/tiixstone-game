<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Puzzle6
{
    public static function globalId() : string
    {
        return 'TB_Lethal005';
    }

    public static function create()
    {
        return new Card\TB_Lethal005;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_Lethal005';
    }
}