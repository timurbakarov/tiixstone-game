<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Vryghoul
{
    public static function globalId() : string
    {
        return 'ICC_067';
    }

    public static function create()
    {
        return new Card\ICC_067;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_067';
    }
}