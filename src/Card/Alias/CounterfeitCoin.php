<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CounterfeitCoin
{
    public static function globalId() : string
    {
        return 'CFM_630';
    }

    public static function create()
    {
        return new Card\CFM_630;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_630';
    }
}