<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AnyfinCanHappen
{
    public static function globalId() : string
    {
        return 'LOE_026';
    }

    public static function create()
    {
        return new Card\LOE_026;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_026';
    }
}