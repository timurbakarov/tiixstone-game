<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RustyHorn
{
    public static function globalId() : string
    {
        return 'PART_003';
    }

    public static function create()
    {
        return new Card\PART_003;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\PART_003';
    }
}