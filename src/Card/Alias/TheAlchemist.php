<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheAlchemist
{
    public static function globalId() : string
    {
        return 'BRMA15_2';
    }

    public static function create()
    {
        return new Card\BRMA15_2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA15_2';
    }
}