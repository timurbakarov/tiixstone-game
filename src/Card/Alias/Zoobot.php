<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Zoobot
{
    public static function globalId() : string
    {
        return 'KAR_095';
    }

    public static function create()
    {
        return new Card\KAR_095;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_095';
    }
}