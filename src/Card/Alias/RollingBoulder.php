<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RollingBoulder
{
    public static function globalId() : string
    {
        return 'LOE_024t';
    }

    public static function create()
    {
        return new Card\LOE_024t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_024t';
    }
}