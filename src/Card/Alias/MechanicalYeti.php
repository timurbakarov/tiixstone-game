<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MechanicalYeti
{
    public static function globalId() : string
    {
        return 'GVG_078';
    }

    public static function create()
    {
        return new Card\GVG_078;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_078';
    }
}