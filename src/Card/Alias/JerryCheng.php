<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JerryCheng
{
    public static function globalId() : string
    {
        return 'CRED_51';
    }

    public static function create()
    {
        return new Card\CRED_51;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_51';
    }
}