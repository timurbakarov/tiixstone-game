<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FelfirePotion
{
    public static function globalId() : string
    {
        return 'CFM_094';
    }

    public static function create()
    {
        return new Card\CFM_094;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_094';
    }
}