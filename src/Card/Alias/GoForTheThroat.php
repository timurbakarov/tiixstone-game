<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GoForTheThroat
{
    public static function globalId() : string
    {
        return 'GILA_492';
    }

    public static function create()
    {
        return new Card\GILA_492;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_492';
    }
}