<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TarnishedCoin
{
    public static function globalId() : string
    {
        return 'TB_011';
    }

    public static function create()
    {
        return new Card\TB_011;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_011';
    }
}