<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Ancient
{
    public static function globalId() : string
    {
        return 'TRL_341t';
    }

    public static function create()
    {
        return new Card\TRL_341t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_341t';
    }
}