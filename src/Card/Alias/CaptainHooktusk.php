<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CaptainHooktusk
{
    public static function globalId() : string
    {
        return 'TRL_126';
    }

    public static function create()
    {
        return new Card\TRL_126;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_126';
    }
}