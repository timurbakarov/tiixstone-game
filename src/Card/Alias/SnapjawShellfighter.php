<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SnapjawShellfighter
{
    public static function globalId() : string
    {
        return 'TRL_535';
    }

    public static function create()
    {
        return new Card\TRL_535;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_535';
    }
}