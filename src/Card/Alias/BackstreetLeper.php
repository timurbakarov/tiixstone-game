<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BackstreetLeper
{
    public static function globalId() : string
    {
        return 'CFM_646';
    }

    public static function create()
    {
        return new Card\CFM_646;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_646';
    }
}