<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RicardoRobaina
{
    public static function globalId() : string
    {
        return 'CRED_37';
    }

    public static function create()
    {
        return new Card\CRED_37;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_37';
    }
}