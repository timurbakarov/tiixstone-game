<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ColiseumManager
{
    public static function globalId() : string
    {
        return 'AT_110';
    }

    public static function create()
    {
        return new Card\AT_110;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_110';
    }
}