<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Bogshaper
{
    public static function globalId() : string
    {
        return 'GIL_807';
    }

    public static function create()
    {
        return new Card\GIL_807;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_807';
    }
}