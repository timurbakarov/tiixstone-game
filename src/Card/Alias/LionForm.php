<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LionForm
{
    public static function globalId() : string
    {
        return 'AT_042a';
    }

    public static function create()
    {
        return new Card\AT_042a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_042a';
    }
}