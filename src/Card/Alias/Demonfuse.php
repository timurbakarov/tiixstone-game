<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Demonfuse
{
    public static function globalId() : string
    {
        return 'AT_024';
    }

    public static function create()
    {
        return new Card\AT_024;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_024';
    }
}