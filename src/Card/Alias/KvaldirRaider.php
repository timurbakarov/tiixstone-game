<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KvaldirRaider
{
    public static function globalId() : string
    {
        return 'AT_119';
    }

    public static function create()
    {
        return new Card\AT_119;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_119';
    }
}