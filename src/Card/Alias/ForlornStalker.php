<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ForlornStalker
{
    public static function globalId() : string
    {
        return 'OG_292';
    }

    public static function create()
    {
        return new Card\OG_292;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_292';
    }
}