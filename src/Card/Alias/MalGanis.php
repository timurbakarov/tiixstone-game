<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MalGanis
{
    public static function globalId() : string
    {
        return 'GVG_021';
    }

    public static function create()
    {
        return new Card\GVG_021;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_021';
    }
}