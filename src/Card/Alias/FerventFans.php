<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FerventFans
{
    public static function globalId() : string
    {
        return 'TRLA_Warrior_07';
    }

    public static function create()
    {
        return new Card\TRLA_Warrior_07;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Warrior_07';
    }
}