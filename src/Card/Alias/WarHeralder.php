<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WarHeralder
{
    public static function globalId() : string
    {
        return 'TRLA_161';
    }

    public static function create()
    {
        return new Card\TRLA_161;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_161';
    }
}