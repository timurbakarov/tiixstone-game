<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EveryfinIsAwesome
{
    public static function globalId() : string
    {
        return 'LOE_113';
    }

    public static function create()
    {
        return new Card\LOE_113;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_113';
    }
}