<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ToxicArrow
{
    public static function globalId() : string
    {
        return 'ICC_049';
    }

    public static function create()
    {
        return new Card\ICC_049;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_049';
    }
}