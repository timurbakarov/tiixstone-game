<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ThrowRocks
{
    public static function globalId() : string
    {
        return 'LOEA07_29';
    }

    public static function create()
    {
        return new Card\LOEA07_29;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA07_29';
    }
}