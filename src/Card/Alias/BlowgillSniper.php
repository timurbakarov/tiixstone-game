<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BlowgillSniper
{
    public static function globalId() : string
    {
        return 'CFM_647';
    }

    public static function create()
    {
        return new Card\CFM_647;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_647';
    }
}