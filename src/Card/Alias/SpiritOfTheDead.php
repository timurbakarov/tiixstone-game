<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpiritOfTheDead
{
    public static function globalId() : string
    {
        return 'TRL_502';
    }

    public static function create()
    {
        return new Card\TRL_502;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_502';
    }
}