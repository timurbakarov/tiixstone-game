<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IckyTentacle
{
    public static function globalId() : string
    {
        return 'OG_114a';
    }

    public static function create()
    {
        return new Card\OG_114a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_114a';
    }
}