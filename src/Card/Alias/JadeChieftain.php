<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JadeChieftain
{
    public static function globalId() : string
    {
        return 'CFM_312';
    }

    public static function create()
    {
        return new Card\CFM_312;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_312';
    }
}