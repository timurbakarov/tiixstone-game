<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OrneryDirehorn
{
    public static function globalId() : string
    {
        return 'UNG_925';
    }

    public static function create()
    {
        return new Card\UNG_925;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_925';
    }
}