<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SkeletalSmith
{
    public static function globalId() : string
    {
        return 'NAXM_002';
    }

    public static function create()
    {
        return new Card\NAXM_002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAXM_002';
    }
}