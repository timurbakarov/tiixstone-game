<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Wildwalker
{
    public static function globalId() : string
    {
        return 'AT_040';
    }

    public static function create()
    {
        return new Card\AT_040;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_040';
    }
}