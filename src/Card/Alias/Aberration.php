<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Aberration
{
    public static function globalId() : string
    {
        return 'BRMA15_4';
    }

    public static function create()
    {
        return new Card\BRMA15_4;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA15_4';
    }
}