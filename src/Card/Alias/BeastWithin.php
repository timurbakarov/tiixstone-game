<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BeastWithin
{
    public static function globalId() : string
    {
        return 'GILA_BOSS_52p2';
    }

    public static function create()
    {
        return new Card\GILA_BOSS_52p2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_BOSS_52p2';
    }
}