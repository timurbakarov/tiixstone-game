<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Acidmaw
{
    public static function globalId() : string
    {
        return 'AT_063';
    }

    public static function create()
    {
        return new Card\AT_063;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_063';
    }
}