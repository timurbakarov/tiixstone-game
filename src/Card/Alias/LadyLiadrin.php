<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LadyLiadrin
{
    public static function globalId() : string
    {
        return 'HERO_04a';
    }

    public static function create()
    {
        return new Card\HERO_04a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HERO_04a';
    }
}