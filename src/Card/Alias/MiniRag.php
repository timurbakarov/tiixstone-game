<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MiniRag
{
    public static function globalId() : string
    {
        return 'TB_FireFestival_MRag';
    }

    public static function create()
    {
        return new Card\TB_FireFestival_MRag;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_FireFestival_MRag';
    }
}