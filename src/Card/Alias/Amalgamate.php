<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Amalgamate
{
    public static function globalId() : string
    {
        return 'GILA_BOSS_27t';
    }

    public static function create()
    {
        return new Card\GILA_BOSS_27t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_BOSS_27t';
    }
}