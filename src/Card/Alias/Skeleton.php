<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Skeleton
{
    public static function globalId() : string
    {
        return 'ICC_026t';
    }

    public static function create()
    {
        return new Card\ICC_026t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_026t';
    }
}