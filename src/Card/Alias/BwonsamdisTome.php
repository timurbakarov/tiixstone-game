<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BwonsamdisTome
{
    public static function globalId() : string
    {
        return 'TRLA_146';
    }

    public static function create()
    {
        return new Card\TRLA_146;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_146';
    }
}