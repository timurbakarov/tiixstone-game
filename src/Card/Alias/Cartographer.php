<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Cartographer
{
    public static function globalId() : string
    {
        return 'GILA_802';
    }

    public static function create()
    {
        return new Card\GILA_802;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_802';
    }
}