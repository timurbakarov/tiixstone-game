<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Felguard
{
    public static function globalId() : string
    {
        return 'EX1_301';
    }

    public static function create()
    {
        return new Card\EX1_301;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_301';
    }
}