<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Sharpen
{
    public static function globalId() : string
    {
        return 'TB_BlingBrawl_Hero1p';
    }

    public static function create()
    {
        return new Card\TB_BlingBrawl_Hero1p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_BlingBrawl_Hero1p';
    }
}