<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WretchedTiller
{
    public static function globalId() : string
    {
        return 'ICC_468';
    }

    public static function create()
    {
        return new Card\ICC_468;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_468';
    }
}