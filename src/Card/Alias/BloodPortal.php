<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodPortal
{
    public static function globalId() : string
    {
        return 'TRLA_185';
    }

    public static function create()
    {
        return new Card\TRLA_185;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_185';
    }
}