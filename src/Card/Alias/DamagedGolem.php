<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DamagedGolem
{
    public static function globalId() : string
    {
        return 'skele21';
    }

    public static function create()
    {
        return new Card\skele21;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\skele21';
    }
}