<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Crystalweaver
{
    public static function globalId() : string
    {
        return 'CFM_610';
    }

    public static function create()
    {
        return new Card\CFM_610;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_610';
    }
}