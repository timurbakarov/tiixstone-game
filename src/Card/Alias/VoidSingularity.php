<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VoidSingularity
{
    public static function globalId() : string
    {
        return 'TB_VoidSingularityMinion';
    }

    public static function create()
    {
        return new Card\TB_VoidSingularityMinion;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_VoidSingularityMinion';
    }
}