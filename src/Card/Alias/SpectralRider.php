<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpectralRider
{
    public static function globalId() : string
    {
        return 'NAX8_05t';
    }

    public static function create()
    {
        return new Card\NAX8_05t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX8_05t';
    }
}