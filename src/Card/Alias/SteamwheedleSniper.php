<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SteamwheedleSniper
{
    public static function globalId() : string
    {
        return 'GVG_087';
    }

    public static function create()
    {
        return new Card\GVG_087;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_087';
    }
}