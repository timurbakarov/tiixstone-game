<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NecroticGeist
{
    public static function globalId() : string
    {
        return 'ICC_900';
    }

    public static function create()
    {
        return new Card\ICC_900;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_900';
    }
}