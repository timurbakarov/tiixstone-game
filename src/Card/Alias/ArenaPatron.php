<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArenaPatron
{
    public static function globalId() : string
    {
        return 'TRL_521';
    }

    public static function create()
    {
        return new Card\TRL_521;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_521';
    }
}