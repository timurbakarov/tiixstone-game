<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FateSpells
{
    public static function globalId() : string
    {
        return 'TB_PickYourFate_5';
    }

    public static function create()
    {
        return new Card\TB_PickYourFate_5;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_PickYourFate_5';
    }
}