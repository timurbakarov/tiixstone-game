<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Repairs
{
    public static function globalId() : string
    {
        return 'LOEA07_28';
    }

    public static function create()
    {
        return new Card\LOEA07_28;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA07_28';
    }
}