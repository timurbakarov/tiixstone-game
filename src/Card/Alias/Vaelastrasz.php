<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Vaelastrasz
{
    public static function globalId() : string
    {
        return 'BRMC_97';
    }

    public static function create()
    {
        return new Card\BRMC_97;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_97';
    }
}