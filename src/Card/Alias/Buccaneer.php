<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Buccaneer
{
    public static function globalId() : string
    {
        return 'AT_029';
    }

    public static function create()
    {
        return new Card\AT_029;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_029';
    }
}