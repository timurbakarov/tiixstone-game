<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NealKochhar
{
    public static function globalId() : string
    {
        return 'CRED_86';
    }

    public static function create()
    {
        return new Card\CRED_86;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_86';
    }
}