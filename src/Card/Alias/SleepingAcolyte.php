<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SleepingAcolyte
{
    public static function globalId() : string
    {
        return 'ICCA05_003';
    }

    public static function create()
    {
        return new Card\ICCA05_003;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA05_003';
    }
}