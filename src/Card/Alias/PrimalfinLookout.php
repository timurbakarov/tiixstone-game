<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PrimalfinLookout
{
    public static function globalId() : string
    {
        return 'UNG_937';
    }

    public static function create()
    {
        return new Card\UNG_937;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_937';
    }
}