<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WorgenGreaser
{
    public static function globalId() : string
    {
        return 'CFM_665';
    }

    public static function create()
    {
        return new Card\CFM_665;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_665';
    }
}