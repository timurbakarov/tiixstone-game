<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TickingAbomination
{
    public static function globalId() : string
    {
        return 'ICC_099';
    }

    public static function create()
    {
        return new Card\ICC_099;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_099';
    }
}