<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Dragonteeth
{
    public static function globalId() : string
    {
        return 'BRMA16_5';
    }

    public static function create()
    {
        return new Card\BRMA16_5;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA16_5';
    }
}