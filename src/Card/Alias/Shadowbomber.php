<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Shadowbomber
{
    public static function globalId() : string
    {
        return 'GVG_009';
    }

    public static function create()
    {
        return new Card\GVG_009;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_009';
    }
}