<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GeneralDrakkisath
{
    public static function globalId() : string
    {
        return 'BRMA08_1';
    }

    public static function create()
    {
        return new Card\BRMA08_1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA08_1';
    }
}