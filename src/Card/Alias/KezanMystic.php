<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KezanMystic
{
    public static function globalId() : string
    {
        return 'GVG_074';
    }

    public static function create()
    {
        return new Card\GVG_074;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_074';
    }
}