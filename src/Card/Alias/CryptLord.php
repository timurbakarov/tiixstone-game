<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CryptLord
{
    public static function globalId() : string
    {
        return 'ICC_808';
    }

    public static function create()
    {
        return new Card\ICC_808;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_808';
    }
}