<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TanglefurMystic
{
    public static function globalId() : string
    {
        return 'GIL_213';
    }

    public static function create()
    {
        return new Card\GIL_213;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_213';
    }
}