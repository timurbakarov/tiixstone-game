<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TotemicSlam
{
    public static function globalId() : string
    {
        return 'AT_132_SHAMAN';
    }

    public static function create()
    {
        return new Card\AT_132_SHAMAN;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_132_SHAMAN';
    }
}