<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WindupBurglebot
{
    public static function globalId() : string
    {
        return 'CFM_025';
    }

    public static function create()
    {
        return new Card\CFM_025;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_025';
    }
}