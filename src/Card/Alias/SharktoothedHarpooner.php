<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SharktoothedHarpooner
{
    public static function globalId() : string
    {
        return 'TRLA_191';
    }

    public static function create()
    {
        return new Card\TRLA_191;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_191';
    }
}