<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpiritOfTheFrog
{
    public static function globalId() : string
    {
        return 'TRL_060';
    }

    public static function create()
    {
        return new Card\TRL_060;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_060';
    }
}