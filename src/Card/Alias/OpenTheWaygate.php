<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OpenTheWaygate
{
    public static function globalId() : string
    {
        return 'UNG_028';
    }

    public static function create()
    {
        return new Card\UNG_028;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_028';
    }
}