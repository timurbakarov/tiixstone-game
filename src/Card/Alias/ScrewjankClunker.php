<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ScrewjankClunker
{
    public static function globalId() : string
    {
        return 'GVG_055';
    }

    public static function create()
    {
        return new Card\GVG_055;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_055';
    }
}