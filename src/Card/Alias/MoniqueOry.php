<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MoniqueOry
{
    public static function globalId() : string
    {
        return 'CRED_70';
    }

    public static function create()
    {
        return new Card\CRED_70;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_70';
    }
}