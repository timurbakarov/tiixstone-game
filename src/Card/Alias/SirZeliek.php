<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SirZeliek
{
    public static function globalId() : string
    {
        return 'TB_KTRAF_2s';
    }

    public static function create()
    {
        return new Card\TB_KTRAF_2s;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_KTRAF_2s';
    }
}