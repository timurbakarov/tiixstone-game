<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Devilsaur
{
    public static function globalId() : string
    {
        return 'TRL_347t';
    }

    public static function create()
    {
        return new Card\TRL_347t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_347t';
    }
}