<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DirehornHatchling
{
    public static function globalId() : string
    {
        return 'UNG_957';
    }

    public static function create()
    {
        return new Card\UNG_957;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_957';
    }
}