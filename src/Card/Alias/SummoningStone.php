<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SummoningStone
{
    public static function globalId() : string
    {
        return 'LOE_086';
    }

    public static function create()
    {
        return new Card\LOE_086;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_086';
    }
}