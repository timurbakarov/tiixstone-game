<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RunNGun
{
    public static function globalId() : string
    {
        return 'GILA_591';
    }

    public static function create()
    {
        return new Card\GILA_591;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_591';
    }
}