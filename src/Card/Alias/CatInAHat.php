<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CatInAHat
{
    public static function globalId() : string
    {
        return 'KAR_004a';
    }

    public static function create()
    {
        return new Card\KAR_004a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_004a';
    }
}