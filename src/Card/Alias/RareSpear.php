<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RareSpear
{
    public static function globalId() : string
    {
        return 'LOEA09_4';
    }

    public static function create()
    {
        return new Card\LOEA09_4;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA09_4';
    }
}