<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DoomedApprentice
{
    public static function globalId() : string
    {
        return 'ICC_083';
    }

    public static function create()
    {
        return new Card\ICC_083;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_083';
    }
}