<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class UnlicensedApothecary
{
    public static function globalId() : string
    {
        return 'CFM_900';
    }

    public static function create()
    {
        return new Card\CFM_900;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_900';
    }
}