<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FacelessDestroyer
{
    public static function globalId() : string
    {
        return 'OG_272t';
    }

    public static function create()
    {
        return new Card\OG_272t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_272t';
    }
}