<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Dinomancy
{
    public static function globalId() : string
    {
        return 'UNG_917';
    }

    public static function create()
    {
        return new Card\UNG_917;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_917';
    }
}