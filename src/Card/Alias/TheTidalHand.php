<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheTidalHand
{
    public static function globalId() : string
    {
        return 'OG_006b';
    }

    public static function create()
    {
        return new Card\OG_006b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_006b';
    }
}