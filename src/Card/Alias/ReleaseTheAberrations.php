<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ReleaseTheAberrations
{
    public static function globalId() : string
    {
        return 'BRMA15_3';
    }

    public static function create()
    {
        return new Card\BRMA15_3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA15_3';
    }
}