<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CrazedWorshipper
{
    public static function globalId() : string
    {
        return 'OG_321';
    }

    public static function create()
    {
        return new Card\OG_321;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_321';
    }
}