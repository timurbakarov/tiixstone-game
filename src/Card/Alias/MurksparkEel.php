<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MurksparkEel
{
    public static function globalId() : string
    {
        return 'GIL_530';
    }

    public static function create()
    {
        return new Card\GIL_530;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_530';
    }
}