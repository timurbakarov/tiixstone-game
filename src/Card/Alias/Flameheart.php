<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Flameheart
{
    public static function globalId() : string
    {
        return 'BRMA_01';
    }

    public static function create()
    {
        return new Card\BRMA_01;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA_01';
    }
}