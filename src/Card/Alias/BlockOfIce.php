<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BlockOfIce
{
    public static function globalId() : string
    {
        return 'ICCA04_004';
    }

    public static function create()
    {
        return new Card\ICCA04_004;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA04_004';
    }
}