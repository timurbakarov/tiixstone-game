<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EtherealConjurer
{
    public static function globalId() : string
    {
        return 'LOE_003';
    }

    public static function create()
    {
        return new Card\LOE_003;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_003';
    }
}