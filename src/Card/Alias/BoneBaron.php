<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BoneBaron
{
    public static function globalId() : string
    {
        return 'ICC_065';
    }

    public static function create()
    {
        return new Card\ICC_065;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_065';
    }
}