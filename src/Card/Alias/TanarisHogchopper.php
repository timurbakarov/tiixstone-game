<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TanarisHogchopper
{
    public static function globalId() : string
    {
        return 'CFM_809';
    }

    public static function create()
    {
        return new Card\CFM_809;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_809';
    }
}