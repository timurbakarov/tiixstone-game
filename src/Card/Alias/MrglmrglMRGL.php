<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MrglmrglMRGL
{
    public static function globalId() : string
    {
        return 'LOEA10_2H';
    }

    public static function create()
    {
        return new Card\LOEA10_2H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA10_2H';
    }
}