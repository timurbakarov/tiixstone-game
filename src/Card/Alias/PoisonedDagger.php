<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PoisonedDagger
{
    public static function globalId() : string
    {
        return 'AT_132_ROGUEt';
    }

    public static function create()
    {
        return new Card\AT_132_ROGUEt;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_132_ROGUEt';
    }
}