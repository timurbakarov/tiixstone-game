<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FinickyCloakfield
{
    public static function globalId() : string
    {
        return 'PART_004';
    }

    public static function create()
    {
        return new Card\PART_004;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\PART_004';
    }
}