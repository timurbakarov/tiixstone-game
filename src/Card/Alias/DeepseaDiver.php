<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DeepseaDiver
{
    public static function globalId() : string
    {
        return 'TRLA_156';
    }

    public static function create()
    {
        return new Card\TRLA_156;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_156';
    }
}