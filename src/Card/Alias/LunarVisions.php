<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LunarVisions
{
    public static function globalId() : string
    {
        return 'CFM_811';
    }

    public static function create()
    {
        return new Card\CFM_811;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_811';
    }
}