<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HoodedAcolyte
{
    public static function globalId() : string
    {
        return 'OG_334';
    }

    public static function create()
    {
        return new Card\OG_334;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_334';
    }
}