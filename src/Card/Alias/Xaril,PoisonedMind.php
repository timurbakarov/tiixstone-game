<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class XarilPoisonedMind
{
    public static function globalId() : string
    {
        return 'OG_080';
    }

    public static function create()
    {
        return new Card\OG_080;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_080';
    }
}