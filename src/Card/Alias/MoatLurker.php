<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MoatLurker
{
    public static function globalId() : string
    {
        return 'KAR_041';
    }

    public static function create()
    {
        return new Card\KAR_041;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_041';
    }
}