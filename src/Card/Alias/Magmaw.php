<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Magmaw
{
    public static function globalId() : string
    {
        return 'BRMA14_12';
    }

    public static function create()
    {
        return new Card\BRMA14_12;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA14_12';
    }
}