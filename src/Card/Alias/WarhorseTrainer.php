<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WarhorseTrainer
{
    public static function globalId() : string
    {
        return 'AT_075';
    }

    public static function create()
    {
        return new Card\AT_075;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_075';
    }
}