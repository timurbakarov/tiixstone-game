<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Puddlestomper
{
    public static function globalId() : string
    {
        return 'GVG_064';
    }

    public static function create()
    {
        return new Card\GVG_064;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_064';
    }
}