<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DarkmireMoonkin
{
    public static function globalId() : string
    {
        return 'GIL_121';
    }

    public static function create()
    {
        return new Card\GIL_121;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_121';
    }
}