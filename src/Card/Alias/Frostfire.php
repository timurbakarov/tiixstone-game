<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Frostfire
{
    public static function globalId() : string
    {
        return 'TRLA_129s';
    }

    public static function create()
    {
        return new Card\TRLA_129s;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_129s';
    }
}