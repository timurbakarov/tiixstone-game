<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Murloc
{
    public static function globalId() : string
    {
        return 'PRO_001at';
    }

    public static function create()
    {
        return new Card\PRO_001at;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\PRO_001at';
    }
}