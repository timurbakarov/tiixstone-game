<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LightsSorrow
{
    public static function globalId() : string
    {
        return 'ICC_071';
    }

    public static function create()
    {
        return new Card\ICC_071;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_071';
    }
}