<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DragonkinSorcerer
{
    public static function globalId() : string
    {
        return 'BRM_020';
    }

    public static function create()
    {
        return new Card\BRM_020;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_020';
    }
}