<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RazorgoreTheUntamed
{
    public static function globalId() : string
    {
        return 'BRMA10_1';
    }

    public static function create()
    {
        return new Card\BRMA10_1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA10_1';
    }
}