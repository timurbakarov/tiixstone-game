<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BookOfSpecters
{
    public static function globalId() : string
    {
        return 'GIL_548';
    }

    public static function create()
    {
        return new Card\GIL_548;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_548';
    }
}