<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Cogmaster
{
    public static function globalId() : string
    {
        return 'GVG_013';
    }

    public static function create()
    {
        return new Card\GVG_013;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_013';
    }
}