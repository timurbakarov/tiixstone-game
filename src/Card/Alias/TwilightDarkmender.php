<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TwilightDarkmender
{
    public static function globalId() : string
    {
        return 'OG_096';
    }

    public static function create()
    {
        return new Card\OG_096;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_096';
    }
}