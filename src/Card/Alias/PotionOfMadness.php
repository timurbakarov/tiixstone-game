<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PotionOfMadness
{
    public static function globalId() : string
    {
        return 'CFM_603';
    }

    public static function create()
    {
        return new Card\CFM_603;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_603';
    }
}