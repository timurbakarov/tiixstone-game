<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WarKodo
{
    public static function globalId() : string
    {
        return 'AT_099t';
    }

    public static function create()
    {
        return new Card\AT_099t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_099t';
    }
}