<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Crystal
{
    public static function globalId() : string
    {
        return 'CFM_606t';
    }

    public static function create()
    {
        return new Card\CFM_606t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_606t';
    }
}