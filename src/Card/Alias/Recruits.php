<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Recruits
{
    public static function globalId() : string
    {
        return 'TRLA_Paladin_03';
    }

    public static function create()
    {
        return new Card\TRLA_Paladin_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Paladin_03';
    }
}