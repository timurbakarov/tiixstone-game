<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StolenSteel
{
    public static function globalId() : string
    {
        return 'TRL_156';
    }

    public static function create()
    {
        return new Card\TRL_156;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_156';
    }
}