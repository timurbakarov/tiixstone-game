<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BabblingBook
{
    public static function globalId() : string
    {
        return 'KAR_009';
    }

    public static function create()
    {
        return new Card\KAR_009;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_009';
    }
}