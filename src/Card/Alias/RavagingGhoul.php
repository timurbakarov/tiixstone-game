<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RavagingGhoul
{
    public static function globalId() : string
    {
        return 'OG_149';
    }

    public static function create()
    {
        return new Card\OG_149;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_149';
    }
}