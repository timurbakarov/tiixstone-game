<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpiritOfTheLynx
{
    public static function globalId() : string
    {
        return 'TRL_901';
    }

    public static function create()
    {
        return new Card\TRL_901;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_901';
    }
}