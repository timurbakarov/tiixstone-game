<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EnigmaticPortal
{
    public static function globalId() : string
    {
        return 'TB_SPT_DpromoPortal';
    }

    public static function create()
    {
        return new Card\TB_SPT_DpromoPortal;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_SPT_DpromoPortal';
    }
}