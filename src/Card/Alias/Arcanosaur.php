<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Arcanosaur
{
    public static function globalId() : string
    {
        return 'TRL_311';
    }

    public static function create()
    {
        return new Card\TRL_311;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_311';
    }
}