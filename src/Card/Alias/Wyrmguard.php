<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Wyrmguard
{
    public static function globalId() : string
    {
        return 'GIL_526';
    }

    public static function create()
    {
        return new Card\GIL_526;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_526';
    }
}