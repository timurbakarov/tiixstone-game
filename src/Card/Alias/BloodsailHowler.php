<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodsailHowler
{
    public static function globalId() : string
    {
        return 'TRL_071';
    }

    public static function create()
    {
        return new Card\TRL_071;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_071';
    }
}