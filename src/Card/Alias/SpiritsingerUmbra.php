<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpiritsingerUmbra
{
    public static function globalId() : string
    {
        return 'UNG_900';
    }

    public static function create()
    {
        return new Card\UNG_900;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_900';
    }
}