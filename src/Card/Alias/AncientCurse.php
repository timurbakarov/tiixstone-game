<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AncientCurse
{
    public static function globalId() : string
    {
        return 'LOE_110t';
    }

    public static function create()
    {
        return new Card\LOE_110t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_110t';
    }
}