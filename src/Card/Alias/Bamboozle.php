<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Bamboozle
{
    public static function globalId() : string
    {
        return 'TB_CoOpv3_003';
    }

    public static function create()
    {
        return new Card\TB_CoOpv3_003;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_CoOpv3_003';
    }
}