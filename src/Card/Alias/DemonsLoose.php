<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DemonsLoose
{
    public static function globalId() : string
    {
        return 'KARA_07_06';
    }

    public static function create()
    {
        return new Card\KARA_07_06;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_07_06';
    }
}