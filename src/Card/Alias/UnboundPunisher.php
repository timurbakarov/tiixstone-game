<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class UnboundPunisher
{
    public static function globalId() : string
    {
        return 'TRLA_177';
    }

    public static function create()
    {
        return new Card\TRLA_177;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_177';
    }
}