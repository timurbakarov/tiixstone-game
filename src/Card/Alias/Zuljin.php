<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Zuljin
{
    public static function globalId() : string
    {
        return 'TRL_065';
    }

    public static function create()
    {
        return new Card\TRL_065;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_065';
    }
}