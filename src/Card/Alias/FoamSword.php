<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FoamSword
{
    public static function globalId() : string
    {
        return 'TB_BlingBrawl_Weapon';
    }

    public static function create()
    {
        return new Card\TB_BlingBrawl_Weapon;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_BlingBrawl_Weapon';
    }
}