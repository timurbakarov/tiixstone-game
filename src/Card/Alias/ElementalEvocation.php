<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ElementalEvocation
{
    public static function globalId() : string
    {
        return 'TRL_310';
    }

    public static function create()
    {
        return new Card\TRL_310;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_310';
    }
}