<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodWarriors
{
    public static function globalId() : string
    {
        return 'OG_276';
    }

    public static function create()
    {
        return new Card\OG_276;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_276';
    }
}