<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PintSizePotion
{
    public static function globalId() : string
    {
        return 'CFM_661';
    }

    public static function create()
    {
        return new Card\CFM_661;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_661';
    }
}