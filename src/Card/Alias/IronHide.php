<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IronHide
{
    public static function globalId() : string
    {
        return 'UNG_923';
    }

    public static function create()
    {
        return new Card\UNG_923;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_923';
    }
}