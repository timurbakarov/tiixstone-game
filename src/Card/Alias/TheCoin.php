<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheCoin
{
    public static function globalId() : string
    {
        return 'GAME_005';
    }

    public static function create()
    {
        return new Card\GAME_005;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GAME_005';
    }
}