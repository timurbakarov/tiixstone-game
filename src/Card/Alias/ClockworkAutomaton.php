<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ClockworkAutomaton
{
    public static function globalId() : string
    {
        return 'GIL_646';
    }

    public static function create()
    {
        return new Card\GIL_646;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_646';
    }
}