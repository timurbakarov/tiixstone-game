<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MadderBomber
{
    public static function globalId() : string
    {
        return 'GVG_090';
    }

    public static function create()
    {
        return new Card\GVG_090;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_090';
    }
}