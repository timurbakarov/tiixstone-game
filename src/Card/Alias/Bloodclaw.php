<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Bloodclaw
{
    public static function globalId() : string
    {
        return 'TRL_543';
    }

    public static function create()
    {
        return new Card\TRL_543;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_543';
    }
}