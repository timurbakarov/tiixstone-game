<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Soulwarden
{
    public static function globalId() : string
    {
        return 'TRL_247';
    }

    public static function create()
    {
        return new Card\TRL_247;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_247';
    }
}