<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Emboldener3000
{
    public static function globalId() : string
    {
        return 'Mekka3';
    }

    public static function create()
    {
        return new Card\Mekka3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\Mekka3';
    }
}