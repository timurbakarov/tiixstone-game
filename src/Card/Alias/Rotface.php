<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Rotface
{
    public static function globalId() : string
    {
        return 'ICC_405';
    }

    public static function create()
    {
        return new Card\ICC_405;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_405';
    }
}