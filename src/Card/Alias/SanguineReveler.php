<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SanguineReveler
{
    public static function globalId() : string
    {
        return 'ICC_903';
    }

    public static function create()
    {
        return new Card\ICC_903;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_903';
    }
}