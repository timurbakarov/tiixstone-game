<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DarkPact
{
    public static function globalId() : string
    {
        return 'LOOT_017';
    }

    public static function create()
    {
        return new Card\LOOT_017;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOOT_017';
    }
}