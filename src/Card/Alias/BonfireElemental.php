<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BonfireElemental
{
    public static function globalId() : string
    {
        return 'GIL_645';
    }

    public static function create()
    {
        return new Card\GIL_645;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_645';
    }
}