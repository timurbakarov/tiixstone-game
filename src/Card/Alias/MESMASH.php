<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MESMASH
{
    public static function globalId() : string
    {
        return 'BRMA07_2_2_TB';
    }

    public static function create()
    {
        return new Card\BRMA07_2_2_TB;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA07_2_2_TB';
    }
}