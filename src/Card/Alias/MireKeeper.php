<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MireKeeper
{
    public static function globalId() : string
    {
        return 'OG_202';
    }

    public static function create()
    {
        return new Card\OG_202;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_202';
    }
}