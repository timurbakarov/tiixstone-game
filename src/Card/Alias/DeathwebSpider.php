<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DeathwebSpider
{
    public static function globalId() : string
    {
        return 'GIL_565';
    }

    public static function create()
    {
        return new Card\GIL_565;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_565';
    }
}