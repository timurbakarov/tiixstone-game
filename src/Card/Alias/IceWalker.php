<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IceWalker
{
    public static function globalId() : string
    {
        return 'ICC_068';
    }

    public static function create()
    {
        return new Card\ICC_068;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_068';
    }
}