<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Vinecleaver
{
    public static function globalId() : string
    {
        return 'UNG_950';
    }

    public static function create()
    {
        return new Card\UNG_950;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_950';
    }
}