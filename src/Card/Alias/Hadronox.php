<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Hadronox
{
    public static function globalId() : string
    {
        return 'ICC_835';
    }

    public static function create()
    {
        return new Card\ICC_835;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_835';
    }
}