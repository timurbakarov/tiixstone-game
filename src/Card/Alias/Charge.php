<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Charge
{
    public static function globalId() : string
    {
        return 'CS2_103';
    }

    public static function create()
    {
        return new Card\CS2_103;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_103';
    }
}