<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TokiTimeTinker
{
    public static function globalId() : string
    {
        return 'GIL_549';
    }

    public static function create()
    {
        return new Card\GIL_549;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_549';
    }
}