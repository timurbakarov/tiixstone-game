<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AnimatedShield
{
    public static function globalId() : string
    {
        return 'KAR_710m';
    }

    public static function create()
    {
        return new Card\KAR_710m;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_710m';
    }
}