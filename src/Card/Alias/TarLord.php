<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TarLord
{
    public static function globalId() : string
    {
        return 'UNG_838';
    }

    public static function create()
    {
        return new Card\UNG_838;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_838';
    }
}