<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheProfessional
{
    public static function globalId() : string
    {
        return 'GILA_590';
    }

    public static function create()
    {
        return new Card\GILA_590;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_590';
    }
}