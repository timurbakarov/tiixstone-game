<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SoundTheBells
{
    public static function globalId() : string
    {
        return 'GIL_145';
    }

    public static function create()
    {
        return new Card\GIL_145;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_145';
    }
}