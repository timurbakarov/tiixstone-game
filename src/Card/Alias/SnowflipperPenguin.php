<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SnowflipperPenguin
{
    public static function globalId() : string
    {
        return 'ICC_023';
    }

    public static function create()
    {
        return new Card\ICC_023;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_023';
    }
}