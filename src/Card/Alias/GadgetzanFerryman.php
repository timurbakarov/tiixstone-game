<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GadgetzanFerryman
{
    public static function globalId() : string
    {
        return 'CFM_693';
    }

    public static function create()
    {
        return new Card\CFM_693;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_693';
    }
}