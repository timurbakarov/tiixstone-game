<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AttackMode
{
    public static function globalId() : string
    {
        return 'GVG_030a';
    }

    public static function create()
    {
        return new Card\GVG_030a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_030a';
    }
}