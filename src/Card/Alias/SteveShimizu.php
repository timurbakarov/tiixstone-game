<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SteveShimizu
{
    public static function globalId() : string
    {
        return 'CRED_72';
    }

    public static function create()
    {
        return new Card\CRED_72;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_72';
    }
}