<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HauntingVisions
{
    public static function globalId() : string
    {
        return 'TRL_058';
    }

    public static function create()
    {
        return new Card\TRL_058;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_058';
    }
}