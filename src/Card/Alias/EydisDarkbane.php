<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EydisDarkbane
{
    public static function globalId() : string
    {
        return 'AT_131';
    }

    public static function create()
    {
        return new Card\AT_131;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_131';
    }
}