<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DeathaxePunisher
{
    public static function globalId() : string
    {
        return 'ICC_810';
    }

    public static function create()
    {
        return new Card\ICC_810;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_810';
    }
}