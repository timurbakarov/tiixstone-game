<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MeatWagon
{
    public static function globalId() : string
    {
        return 'ICC_812';
    }

    public static function create()
    {
        return new Card\ICC_812;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_812';
    }
}