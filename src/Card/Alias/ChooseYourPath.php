<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ChooseYourPath
{
    public static function globalId() : string
    {
        return 'UNG_922t1';
    }

    public static function create()
    {
        return new Card\UNG_922t1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_922t1';
    }
}