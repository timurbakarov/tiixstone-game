<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MassiveGnoll
{
    public static function globalId() : string
    {
        return 'TU4a_005';
    }

    public static function create()
    {
        return new Card\TU4a_005;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TU4a_005';
    }
}