<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EliteTaurenChieftain
{
    public static function globalId() : string
    {
        return 'PRO_001';
    }

    public static function create()
    {
        return new Card\PRO_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\PRO_001';
    }
}