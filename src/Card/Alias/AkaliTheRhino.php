<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AkaliTheRhino
{
    public static function globalId() : string
    {
        return 'TRL_329';
    }

    public static function create()
    {
        return new Card\TRL_329;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_329';
    }
}