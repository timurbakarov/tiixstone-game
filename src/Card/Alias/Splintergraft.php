<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Splintergraft
{
    public static function globalId() : string
    {
        return 'GIL_658';
    }

    public static function create()
    {
        return new Card\GIL_658;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_658';
    }
}