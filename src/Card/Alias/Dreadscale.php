<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Dreadscale
{
    public static function globalId() : string
    {
        return 'AT_063t';
    }

    public static function create()
    {
        return new Card\AT_063t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_063t';
    }
}