<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DragonsBreath
{
    public static function globalId() : string
    {
        return 'BRM_003';
    }

    public static function create()
    {
        return new Card\BRM_003;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_003';
    }
}