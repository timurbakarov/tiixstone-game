<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HobartGrapplehammer
{
    public static function globalId() : string
    {
        return 'CFM_643';
    }

    public static function create()
    {
        return new Card\CFM_643;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_643';
    }
}