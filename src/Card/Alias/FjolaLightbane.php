<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FjolaLightbane
{
    public static function globalId() : string
    {
        return 'AT_129';
    }

    public static function create()
    {
        return new Card\AT_129;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_129';
    }
}