<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Dragonfire
{
    public static function globalId() : string
    {
        return 'GILA_Darius_11';
    }

    public static function create()
    {
        return new Card\GILA_Darius_11;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_Darius_11';
    }
}