<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Pyros
{
    public static function globalId() : string
    {
        return 'UNG_027t4';
    }

    public static function create()
    {
        return new Card\UNG_027t4;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_027t4';
    }
}