<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HenchClanThug
{
    public static function globalId() : string
    {
        return 'GIL_534';
    }

    public static function create()
    {
        return new Card\GIL_534;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_534';
    }
}