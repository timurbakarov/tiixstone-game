<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StoneSentinel
{
    public static function globalId() : string
    {
        return 'UNG_208';
    }

    public static function create()
    {
        return new Card\UNG_208;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_208';
    }
}