<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Starving
{
    public static function globalId() : string
    {
        return 'GILA_BOSS_65p';
    }

    public static function create()
    {
        return new Card\GILA_BOSS_65p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_BOSS_65p';
    }
}