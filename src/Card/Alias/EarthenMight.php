<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EarthenMight
{
    public static function globalId() : string
    {
        return 'GIL_586';
    }

    public static function create()
    {
        return new Card\GIL_586;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_586';
    }
}