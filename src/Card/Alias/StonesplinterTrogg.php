<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StonesplinterTrogg
{
    public static function globalId() : string
    {
        return 'GVG_067';
    }

    public static function create()
    {
        return new Card\GVG_067;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_067';
    }
}