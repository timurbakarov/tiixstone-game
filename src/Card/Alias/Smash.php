<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Smash
{
    public static function globalId() : string
    {
        return 'TRLA_Warrior_04';
    }

    public static function create()
    {
        return new Card\TRLA_Warrior_04;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Warrior_04';
    }
}