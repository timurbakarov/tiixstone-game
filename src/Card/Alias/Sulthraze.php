<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Sulthraze
{
    public static function globalId() : string
    {
        return 'TRL_325';
    }

    public static function create()
    {
        return new Card\TRL_325;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_325';
    }
}