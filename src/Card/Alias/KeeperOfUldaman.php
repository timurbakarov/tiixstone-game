<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KeeperOfUldaman
{
    public static function globalId() : string
    {
        return 'LOE_017';
    }

    public static function create()
    {
        return new Card\LOE_017;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_017';
    }
}