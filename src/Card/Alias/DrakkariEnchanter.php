<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DrakkariEnchanter
{
    public static function globalId() : string
    {
        return 'ICC_901';
    }

    public static function create()
    {
        return new Card\ICC_901;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_901';
    }
}