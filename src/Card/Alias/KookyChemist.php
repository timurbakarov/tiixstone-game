<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KookyChemist
{
    public static function globalId() : string
    {
        return 'CFM_063';
    }

    public static function create()
    {
        return new Card\CFM_063;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_063';
    }
}