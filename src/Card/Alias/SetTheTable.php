<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SetTheTable
{
    public static function globalId() : string
    {
        return 'KAR_A02_09H';
    }

    public static function create()
    {
        return new Card\KAR_A02_09H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_A02_09H';
    }
}