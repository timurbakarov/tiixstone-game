<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DOOM
{
    public static function globalId() : string
    {
        return 'OG_239';
    }

    public static function create()
    {
        return new Card\OG_239;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_239';
    }
}