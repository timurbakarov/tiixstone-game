<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MedivhsValet
{
    public static function globalId() : string
    {
        return 'KAR_092';
    }

    public static function create()
    {
        return new Card\KAR_092;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_092';
    }
}