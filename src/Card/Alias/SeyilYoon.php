<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SeyilYoon
{
    public static function globalId() : string
    {
        return 'CRED_41';
    }

    public static function create()
    {
        return new Card\CRED_41;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_41';
    }
}