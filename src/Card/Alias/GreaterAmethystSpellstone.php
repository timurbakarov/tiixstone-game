<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GreaterAmethystSpellstone
{
    public static function globalId() : string
    {
        return 'LOOT_043t3';
    }

    public static function create()
    {
        return new Card\LOOT_043t3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOOT_043t3';
    }
}