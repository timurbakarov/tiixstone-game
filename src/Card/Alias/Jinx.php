<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Jinx
{
    public static function globalId() : string
    {
        return 'TRLA_Warlock_11';
    }

    public static function create()
    {
        return new Card\TRLA_Warlock_11;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Warlock_11';
    }
}