<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FeedingTime
{
    public static function globalId() : string
    {
        return 'UNG_834';
    }

    public static function create()
    {
        return new Card\UNG_834;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_834';
    }
}