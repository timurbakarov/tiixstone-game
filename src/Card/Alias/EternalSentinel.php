<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EternalSentinel
{
    public static function globalId() : string
    {
        return 'OG_026';
    }

    public static function create()
    {
        return new Card\OG_026;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_026';
    }
}