<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class UnstablePortal
{
    public static function globalId() : string
    {
        return 'LOEA15_2';
    }

    public static function create()
    {
        return new Card\LOEA15_2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA15_2';
    }
}