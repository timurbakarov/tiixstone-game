<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LaughingSister
{
    public static function globalId() : string
    {
        return 'DREAM_01';
    }

    public static function create()
    {
        return new Card\DREAM_01;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\DREAM_01';
    }
}