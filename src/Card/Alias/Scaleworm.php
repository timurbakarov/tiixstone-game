<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Scaleworm
{
    public static function globalId() : string
    {
        return 'GIL_601';
    }

    public static function create()
    {
        return new Card\GIL_601;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_601';
    }
}