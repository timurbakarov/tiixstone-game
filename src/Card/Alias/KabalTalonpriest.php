<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KabalTalonpriest
{
    public static function globalId() : string
    {
        return 'CFM_626';
    }

    public static function create()
    {
        return new Card\CFM_626;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_626';
    }
}