<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LorewalkerCho
{
    public static function globalId() : string
    {
        return 'TU4f_001';
    }

    public static function create()
    {
        return new Card\TU4f_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TU4f_001';
    }
}