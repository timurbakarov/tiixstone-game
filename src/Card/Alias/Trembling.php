<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Trembling
{
    public static function globalId() : string
    {
        return 'KARA_05_01hp';
    }

    public static function create()
    {
        return new Card\KARA_05_01hp;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_05_01hp';
    }
}