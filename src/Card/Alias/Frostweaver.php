<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Frostweaver
{
    public static function globalId() : string
    {
        return 'TRLA_132';
    }

    public static function create()
    {
        return new Card\TRLA_132;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_132';
    }
}