<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Bash
{
    public static function globalId() : string
    {
        return 'AT_064';
    }

    public static function create()
    {
        return new Card\AT_064;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_064';
    }
}