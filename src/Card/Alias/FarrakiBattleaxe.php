<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FarrakiBattleaxe
{
    public static function globalId() : string
    {
        return 'TRL_304';
    }

    public static function create()
    {
        return new Card\TRL_304;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_304';
    }
}