<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TundraRhino
{
    public static function globalId() : string
    {
        return 'DS1_178';
    }

    public static function create()
    {
        return new Card\DS1_178;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\DS1_178';
    }
}