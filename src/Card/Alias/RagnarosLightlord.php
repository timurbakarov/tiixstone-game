<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RagnarosLightlord
{
    public static function globalId() : string
    {
        return 'OG_229';
    }

    public static function create()
    {
        return new Card\OG_229;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_229';
    }
}