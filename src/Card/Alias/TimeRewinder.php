<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TimeRewinder
{
    public static function globalId() : string
    {
        return 'PART_002';
    }

    public static function create()
    {
        return new Card\PART_002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\PART_002';
    }
}