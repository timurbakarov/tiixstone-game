<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpectralPillager
{
    public static function globalId() : string
    {
        return 'ICC_910';
    }

    public static function create()
    {
        return new Card\ICC_910;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_910';
    }
}