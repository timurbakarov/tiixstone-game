<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SI7Agent
{
    public static function globalId() : string
    {
        return 'EX1_134';
    }

    public static function create()
    {
        return new Card\EX1_134;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_134';
    }
}