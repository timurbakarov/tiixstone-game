<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Arfus
{
    public static function globalId() : string
    {
        return 'ICC_854';
    }

    public static function create()
    {
        return new Card\ICC_854;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_854';
    }
}