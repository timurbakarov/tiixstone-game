<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ErraticCreatures
{
    public static function globalId() : string
    {
        return 'GILA_Toki_03';
    }

    public static function create()
    {
        return new Card\GILA_Toki_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_Toki_03';
    }
}