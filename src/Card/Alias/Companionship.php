<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Companionship
{
    public static function globalId() : string
    {
        return 'GILA_414';
    }

    public static function create()
    {
        return new Card\GILA_414;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_414';
    }
}