<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LordMarrowgar
{
    public static function globalId() : string
    {
        return 'FB_LK_012h';
    }

    public static function create()
    {
        return new Card\FB_LK_012h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK_012h';
    }
}