<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DrewKorfe
{
    public static function globalId() : string
    {
        return 'CRED_74';
    }

    public static function create()
    {
        return new Card\CRED_74;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_74';
    }
}