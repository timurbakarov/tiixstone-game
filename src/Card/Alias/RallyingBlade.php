<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RallyingBlade
{
    public static function globalId() : string
    {
        return 'OG_222';
    }

    public static function create()
    {
        return new Card\OG_222;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_222';
    }
}