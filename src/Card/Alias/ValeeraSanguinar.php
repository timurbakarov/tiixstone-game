<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ValeeraSanguinar
{
    public static function globalId() : string
    {
        return 'HERO_03';
    }

    public static function create()
    {
        return new Card\HERO_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HERO_03';
    }
}