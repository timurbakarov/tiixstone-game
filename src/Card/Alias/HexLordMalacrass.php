<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HexLordMalacrass
{
    public static function globalId() : string
    {
        return 'TRL_318';
    }

    public static function create()
    {
        return new Card\TRL_318;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_318';
    }
}