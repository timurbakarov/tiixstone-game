<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GrandCrusader
{
    public static function globalId() : string
    {
        return 'AT_118';
    }

    public static function create()
    {
        return new Card\AT_118;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_118';
    }
}