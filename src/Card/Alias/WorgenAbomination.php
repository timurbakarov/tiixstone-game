<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WorgenAbomination
{
    public static function globalId() : string
    {
        return 'GIL_117';
    }

    public static function create()
    {
        return new Card\GIL_117;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_117';
    }
}