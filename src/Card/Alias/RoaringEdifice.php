<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RoaringEdifice
{
    public static function globalId() : string
    {
        return 'TRLA_808';
    }

    public static function create()
    {
        return new Card\TRLA_808;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_808';
    }
}