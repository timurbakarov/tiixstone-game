<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpiritOfTheTiger
{
    public static function globalId() : string
    {
        return 'TRL_309';
    }

    public static function create()
    {
        return new Card\TRL_309;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_309';
    }
}