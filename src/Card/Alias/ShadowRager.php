<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShadowRager
{
    public static function globalId() : string
    {
        return 'CFM_636';
    }

    public static function create()
    {
        return new Card\CFM_636;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_636';
    }
}