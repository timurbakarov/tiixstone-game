<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TreeOfLife
{
    public static function globalId() : string
    {
        return 'GVG_033';
    }

    public static function create()
    {
        return new Card\GVG_033;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_033';
    }
}