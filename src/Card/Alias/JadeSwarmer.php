<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JadeSwarmer
{
    public static function globalId() : string
    {
        return 'CFM_691';
    }

    public static function create()
    {
        return new Card\CFM_691;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_691';
    }
}