<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TailSwipe
{
    public static function globalId() : string
    {
        return 'BRM_030t';
    }

    public static function create()
    {
        return new Card\BRM_030t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_030t';
    }
}