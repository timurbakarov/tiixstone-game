<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LKPhase2Debug
{
    public static function globalId() : string
    {
        return 'FB_LKDebug001';
    }

    public static function create()
    {
        return new Card\FB_LKDebug001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LKDebug001';
    }
}