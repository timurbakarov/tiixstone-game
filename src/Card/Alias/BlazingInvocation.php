<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BlazingInvocation
{
    public static function globalId() : string
    {
        return 'GIL_836';
    }

    public static function create()
    {
        return new Card\GIL_836;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_836';
    }
}