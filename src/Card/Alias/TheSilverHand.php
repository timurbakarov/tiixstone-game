<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheSilverHand
{
    public static function globalId() : string
    {
        return 'CS2_101_H2_AT_132';
    }

    public static function create()
    {
        return new Card\CS2_101_H2_AT_132;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_101_H2_AT_132';
    }
}