<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GonksMark
{
    public static function globalId() : string
    {
        return 'TRLA_115t';
    }

    public static function create()
    {
        return new Card\TRLA_115t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_115t';
    }
}