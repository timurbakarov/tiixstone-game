<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Deathbloom
{
    public static function globalId() : string
    {
        return 'NAX6_03';
    }

    public static function create()
    {
        return new Card\NAX6_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX6_03';
    }
}