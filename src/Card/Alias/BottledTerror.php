<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BottledTerror
{
    public static function globalId() : string
    {
        return 'TRLA_106';
    }

    public static function create()
    {
        return new Card\TRLA_106;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_106';
    }
}