<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KingsbloodToxin
{
    public static function globalId() : string
    {
        return 'OG_080b';
    }

    public static function create()
    {
        return new Card\OG_080b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_080b';
    }
}