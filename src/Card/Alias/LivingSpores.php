<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LivingSpores
{
    public static function globalId() : string
    {
        return 'UNG_999t2';
    }

    public static function create()
    {
        return new Card\UNG_999t2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_999t2';
    }
}