<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CountessAshmore
{
    public static function globalId() : string
    {
        return 'GIL_578';
    }

    public static function create()
    {
        return new Card\GIL_578;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_578';
    }
}