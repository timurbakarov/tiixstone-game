<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GraveShambler
{
    public static function globalId() : string
    {
        return 'ICC_097';
    }

    public static function create()
    {
        return new Card\ICC_097;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_097';
    }
}