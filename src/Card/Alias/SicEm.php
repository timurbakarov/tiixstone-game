<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SicEm
{
    public static function globalId() : string
    {
        return 'GILA_401';
    }

    public static function create()
    {
        return new Card\GILA_401;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_401';
    }
}