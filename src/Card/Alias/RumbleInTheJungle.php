<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RumbleInTheJungle
{
    public static function globalId() : string
    {
        return 'TB_TagTeam_Druid';
    }

    public static function create()
    {
        return new Card\TB_TagTeam_Druid;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_TagTeam_Druid';
    }
}