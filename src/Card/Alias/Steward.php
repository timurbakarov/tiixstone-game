<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Steward
{
    public static function globalId() : string
    {
        return 'KAR_044a';
    }

    public static function create()
    {
        return new Card\KAR_044a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_044a';
    }
}