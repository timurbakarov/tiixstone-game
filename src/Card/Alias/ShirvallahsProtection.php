<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShirvallahsProtection
{
    public static function globalId() : string
    {
        return 'TRLA_105t';
    }

    public static function create()
    {
        return new Card\TRLA_105t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_105t';
    }
}