<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SplittingImage
{
    public static function globalId() : string
    {
        return 'TRL_400';
    }

    public static function create()
    {
        return new Card\TRL_400;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_400';
    }
}