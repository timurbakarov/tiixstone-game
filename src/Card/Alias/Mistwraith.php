<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Mistwraith
{
    public static function globalId() : string
    {
        return 'GIL_510';
    }

    public static function create()
    {
        return new Card\GIL_510;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_510';
    }
}