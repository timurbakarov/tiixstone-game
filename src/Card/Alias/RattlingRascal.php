<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RattlingRascal
{
    public static function globalId() : string
    {
        return 'ICC_025';
    }

    public static function create()
    {
        return new Card\ICC_025;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_025';
    }
}