<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ExplosiveSheep
{
    public static function globalId() : string
    {
        return 'GVG_076';
    }

    public static function create()
    {
        return new Card\GVG_076;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_076';
    }
}