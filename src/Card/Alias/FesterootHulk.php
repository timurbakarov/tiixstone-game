<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FesterootHulk
{
    public static function globalId() : string
    {
        return 'GIL_655';
    }

    public static function create()
    {
        return new Card\GIL_655;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_655';
    }
}