<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Specialist
{
    public static function globalId() : string
    {
        return 'GILA_598';
    }

    public static function create()
    {
        return new Card\GILA_598;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_598';
    }
}