<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JadeGolem
{
    public static function globalId() : string
    {
        return 'CFM_712_t15';
    }

    public static function create()
    {
        return new Card\CFM_712_t15;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_712_t15';
    }
}