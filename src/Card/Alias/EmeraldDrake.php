<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EmeraldDrake
{
    public static function globalId() : string
    {
        return 'DREAM_03';
    }

    public static function create()
    {
        return new Card\DREAM_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\DREAM_03';
    }
}