<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CorruptedHealbot
{
    public static function globalId() : string
    {
        return 'OG_147';
    }

    public static function create()
    {
        return new Card\OG_147;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_147';
    }
}