<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FrozenClone
{
    public static function globalId() : string
    {
        return 'ICC_082';
    }

    public static function create()
    {
        return new Card\ICC_082;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_082';
    }
}