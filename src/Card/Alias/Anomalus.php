<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Anomalus
{
    public static function globalId() : string
    {
        return 'OG_120';
    }

    public static function create()
    {
        return new Card\OG_120;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_120';
    }
}