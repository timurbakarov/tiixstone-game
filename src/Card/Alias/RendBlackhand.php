<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RendBlackhand
{
    public static function globalId() : string
    {
        return 'BRM_029';
    }

    public static function create()
    {
        return new Card\BRM_029;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_029';
    }
}