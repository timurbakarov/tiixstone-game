<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TwilightSummoner
{
    public static function globalId() : string
    {
        return 'OG_272';
    }

    public static function create()
    {
        return new Card\OG_272;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_272';
    }
}