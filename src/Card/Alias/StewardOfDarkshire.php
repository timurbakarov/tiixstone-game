<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StewardOfDarkshire
{
    public static function globalId() : string
    {
        return 'OG_310';
    }

    public static function create()
    {
        return new Card\OG_310;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_310';
    }
}