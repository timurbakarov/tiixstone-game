<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Electron
{
    public static function globalId() : string
    {
        return 'BRMA14_7';
    }

    public static function create()
    {
        return new Card\BRMA14_7;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA14_7';
    }
}