<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WintersVeilGift
{
    public static function globalId() : string
    {
        return 'TB_GiftExchange_Treasure';
    }

    public static function create()
    {
        return new Card\TB_GiftExchange_Treasure;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_GiftExchange_Treasure';
    }
}