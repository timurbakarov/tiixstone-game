<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WalterKong
{
    public static function globalId() : string
    {
        return 'CRED_44';
    }

    public static function create()
    {
        return new Card\CRED_44;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_44';
    }
}