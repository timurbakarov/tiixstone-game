<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OgreNinja
{
    public static function globalId() : string
    {
        return 'GVG_088';
    }

    public static function create()
    {
        return new Card\GVG_088;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_088';
    }
}