<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StevenGabriel
{
    public static function globalId() : string
    {
        return 'CRED_04';
    }

    public static function create()
    {
        return new Card\CRED_04;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_04';
    }
}