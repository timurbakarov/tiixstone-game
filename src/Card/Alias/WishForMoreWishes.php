<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WishForMoreWishes
{
    public static function globalId() : string
    {
        return 'LOEA02_06';
    }

    public static function create()
    {
        return new Card\LOEA02_06;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA02_06';
    }
}