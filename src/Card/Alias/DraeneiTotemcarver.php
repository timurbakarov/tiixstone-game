<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DraeneiTotemcarver
{
    public static function globalId() : string
    {
        return 'AT_047';
    }

    public static function create()
    {
        return new Card\AT_047;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_047';
    }
}