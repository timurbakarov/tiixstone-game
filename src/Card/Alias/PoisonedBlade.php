<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PoisonedBlade
{
    public static function globalId() : string
    {
        return 'AT_034';
    }

    public static function create()
    {
        return new Card\AT_034;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_034';
    }
}