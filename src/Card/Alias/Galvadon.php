<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Galvadon
{
    public static function globalId() : string
    {
        return 'UNG_954t1';
    }

    public static function create()
    {
        return new Card\UNG_954t1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_954t1';
    }
}