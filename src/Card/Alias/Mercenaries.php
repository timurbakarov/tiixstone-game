<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Mercenaries
{
    public static function globalId() : string
    {
        return 'GILA_592';
    }

    public static function create()
    {
        return new Card\GILA_592;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_592';
    }
}