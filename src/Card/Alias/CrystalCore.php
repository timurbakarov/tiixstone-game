<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CrystalCore
{
    public static function globalId() : string
    {
        return 'UNG_067t1';
    }

    public static function create()
    {
        return new Card\UNG_067t1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_067t1';
    }
}