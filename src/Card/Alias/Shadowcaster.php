<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Shadowcaster
{
    public static function globalId() : string
    {
        return 'OG_291';
    }

    public static function create()
    {
        return new Card\OG_291;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_291';
    }
}