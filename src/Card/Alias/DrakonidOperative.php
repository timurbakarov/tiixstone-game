<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DrakonidOperative
{
    public static function globalId() : string
    {
        return 'CFM_605';
    }

    public static function create()
    {
        return new Card\CFM_605;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_605';
    }
}