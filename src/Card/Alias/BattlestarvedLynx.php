<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BattlestarvedLynx
{
    public static function globalId() : string
    {
        return 'TRLA_165';
    }

    public static function create()
    {
        return new Card\TRLA_165;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_165';
    }
}