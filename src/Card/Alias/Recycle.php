<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Recycle
{
    public static function globalId() : string
    {
        return 'GVG_031';
    }

    public static function create()
    {
        return new Card\GVG_031;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_031';
    }
}