<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class QueenCarnassa
{
    public static function globalId() : string
    {
        return 'UNG_920t1';
    }

    public static function create()
    {
        return new Card\UNG_920t1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_920t1';
    }
}