<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StreetwiseInvestigator
{
    public static function globalId() : string
    {
        return 'CFM_656';
    }

    public static function create()
    {
        return new Card\CFM_656;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_656';
    }
}