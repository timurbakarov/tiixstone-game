<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Venomancer
{
    public static function globalId() : string
    {
        return 'ICC_032';
    }

    public static function create()
    {
        return new Card\ICC_032;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_032';
    }
}