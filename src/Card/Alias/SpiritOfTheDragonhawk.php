<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpiritOfTheDragonhawk
{
    public static function globalId() : string
    {
        return 'TRL_319';
    }

    public static function create()
    {
        return new Card\TRL_319;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_319';
    }
}