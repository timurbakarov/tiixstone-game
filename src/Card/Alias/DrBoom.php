<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DrBoom
{
    public static function globalId() : string
    {
        return 'GVG_110';
    }

    public static function create()
    {
        return new Card\GVG_110;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_110';
    }
}