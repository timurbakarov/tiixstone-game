<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IceBreaker
{
    public static function globalId() : string
    {
        return 'ICC_236';
    }

    public static function create()
    {
        return new Card\ICC_236;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_236';
    }
}