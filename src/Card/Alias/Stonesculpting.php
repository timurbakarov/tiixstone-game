<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Stonesculpting
{
    public static function globalId() : string
    {
        return 'LOEA06_02';
    }

    public static function create()
    {
        return new Card\LOEA06_02;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA06_02';
    }
}