<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SilithidSwarmer
{
    public static function globalId() : string
    {
        return 'OG_034';
    }

    public static function create()
    {
        return new Card\OG_034;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_034';
    }
}