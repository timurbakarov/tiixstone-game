<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Cragtorr
{
    public static function globalId() : string
    {
        return 'GILA_BOSS_54h';
    }

    public static function create()
    {
        return new Card\GILA_BOSS_54h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_BOSS_54h';
    }
}