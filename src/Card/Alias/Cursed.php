<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Cursed
{
    public static function globalId() : string
    {
        return 'LOE_007t';
    }

    public static function create()
    {
        return new Card\LOE_007t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_007t';
    }
}