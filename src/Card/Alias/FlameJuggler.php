<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FlameJuggler
{
    public static function globalId() : string
    {
        return 'AT_094';
    }

    public static function create()
    {
        return new Card\AT_094;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_094';
    }
}