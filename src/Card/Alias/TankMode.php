<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TankMode
{
    public static function globalId() : string
    {
        return 'GVG_030b';
    }

    public static function create()
    {
        return new Card\GVG_030b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_030b';
    }
}