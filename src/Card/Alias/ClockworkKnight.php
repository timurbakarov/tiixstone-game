<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ClockworkKnight
{
    public static function globalId() : string
    {
        return 'AT_096';
    }

    public static function create()
    {
        return new Card\AT_096;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_096';
    }
}