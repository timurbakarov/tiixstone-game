<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Likkim
{
    public static function globalId() : string
    {
        return 'TRL_352';
    }

    public static function create()
    {
        return new Card\TRL_352;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_352';
    }
}