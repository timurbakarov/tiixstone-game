<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GlitterMoth
{
    public static function globalId() : string
    {
        return 'GIL_837';
    }

    public static function create()
    {
        return new Card\GIL_837;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_837';
    }
}