<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FacelessShambler
{
    public static function globalId() : string
    {
        return 'OG_174';
    }

    public static function create()
    {
        return new Card\OG_174;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_174';
    }
}