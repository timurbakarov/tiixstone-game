<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GiftOfMana
{
    public static function globalId() : string
    {
        return 'GVG_032a';
    }

    public static function create()
    {
        return new Card\GVG_032a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_032a';
    }
}