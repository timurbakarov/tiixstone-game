<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OrcWarrior
{
    public static function globalId() : string
    {
        return 'KARA_13_03H';
    }

    public static function create()
    {
        return new Card\KARA_13_03H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_13_03H';
    }
}