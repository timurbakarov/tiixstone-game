<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Shieldmaiden
{
    public static function globalId() : string
    {
        return 'GVG_053';
    }

    public static function create()
    {
        return new Card\GVG_053;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_053';
    }
}