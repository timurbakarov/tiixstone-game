<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GluttonousOoze
{
    public static function globalId() : string
    {
        return 'UNG_946';
    }

    public static function create()
    {
        return new Card\UNG_946;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_946';
    }
}