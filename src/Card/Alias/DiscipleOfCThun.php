<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DiscipleOfCThun
{
    public static function globalId() : string
    {
        return 'OG_162';
    }

    public static function create()
    {
        return new Card\OG_162;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_162';
    }
}