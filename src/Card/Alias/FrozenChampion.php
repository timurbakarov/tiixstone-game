<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FrozenChampion
{
    public static function globalId() : string
    {
        return 'ICC_838t';
    }

    public static function create()
    {
        return new Card\ICC_838t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_838t';
    }
}