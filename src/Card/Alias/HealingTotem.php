<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HealingTotem
{
    public static function globalId() : string
    {
        return 'NEW1_009';
    }

    public static function create()
    {
        return new Card\NEW1_009;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NEW1_009';
    }
}