<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CorruptingMist
{
    public static function globalId() : string
    {
        return 'UNG_831';
    }

    public static function create()
    {
        return new Card\UNG_831;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_831';
    }
}