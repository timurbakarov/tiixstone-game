<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BoneStorm
{
    public static function globalId() : string
    {
        return 'ICCA06_004';
    }

    public static function create()
    {
        return new Card\ICCA06_004;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA06_004';
    }
}