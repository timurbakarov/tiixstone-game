<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BeastCaller
{
    public static function globalId() : string
    {
        return 'TRLA_Hunter_04';
    }

    public static function create()
    {
        return new Card\TRLA_Hunter_04;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Hunter_04';
    }
}