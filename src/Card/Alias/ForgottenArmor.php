<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ForgottenArmor
{
    public static function globalId() : string
    {
        return 'CFM_308a';
    }

    public static function create()
    {
        return new Card\CFM_308a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_308a';
    }
}