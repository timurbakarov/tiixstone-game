<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Shadowmourne
{
    public static function globalId() : string
    {
        return 'ICC_834w';
    }

    public static function create()
    {
        return new Card\ICC_834w;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_834w';
    }
}