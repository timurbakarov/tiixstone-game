<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Duskboar
{
    public static function globalId() : string
    {
        return 'OG_326';
    }

    public static function create()
    {
        return new Card\OG_326;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_326';
    }
}