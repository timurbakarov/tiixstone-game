<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GiantMastodon
{
    public static function globalId() : string
    {
        return 'UNG_071';
    }

    public static function create()
    {
        return new Card\UNG_071;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_071';
    }
}