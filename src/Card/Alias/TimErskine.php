<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TimErskine
{
    public static function globalId() : string
    {
        return 'CRED_42';
    }

    public static function create()
    {
        return new Card\CRED_42;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_42';
    }
}