<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BlackWhelp
{
    public static function globalId() : string
    {
        return 'BRM_022t';
    }

    public static function create()
    {
        return new Card\BRM_022t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_022t';
    }
}