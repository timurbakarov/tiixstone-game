<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RumbletuskBreaker
{
    public static function globalId() : string
    {
        return 'TRL_531t';
    }

    public static function create()
    {
        return new Card\TRL_531t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_531t';
    }
}