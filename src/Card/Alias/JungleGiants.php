<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JungleGiants
{
    public static function globalId() : string
    {
        return 'UNG_116';
    }

    public static function create()
    {
        return new Card\UNG_116;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_116';
    }
}