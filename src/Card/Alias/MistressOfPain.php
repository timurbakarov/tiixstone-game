<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MistressOfPain
{
    public static function globalId() : string
    {
        return 'GVG_018';
    }

    public static function create()
    {
        return new Card\GVG_018;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_018';
    }
}