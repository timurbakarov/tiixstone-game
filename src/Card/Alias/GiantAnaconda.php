<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GiantAnaconda
{
    public static function globalId() : string
    {
        return 'UNG_086';
    }

    public static function create()
    {
        return new Card\UNG_086;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_086';
    }
}