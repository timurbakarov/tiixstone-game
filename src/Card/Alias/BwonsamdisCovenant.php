<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BwonsamdisCovenant
{
    public static function globalId() : string
    {
        return 'TRLA_147';
    }

    public static function create()
    {
        return new Card\TRLA_147;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_147';
    }
}