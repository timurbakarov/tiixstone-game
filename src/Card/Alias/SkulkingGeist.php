<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SkulkingGeist
{
    public static function globalId() : string
    {
        return 'ICC_701';
    }

    public static function create()
    {
        return new Card\ICC_701;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_701';
    }
}