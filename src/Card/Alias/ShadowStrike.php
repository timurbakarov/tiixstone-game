<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShadowStrike
{
    public static function globalId() : string
    {
        return 'OG_176';
    }

    public static function create()
    {
        return new Card\OG_176;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_176';
    }
}