<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WitchwoodsTouch
{
    public static function globalId() : string
    {
        return 'GILA_BOSS_99t3';
    }

    public static function create()
    {
        return new Card\GILA_BOSS_99t3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_BOSS_99t3';
    }
}