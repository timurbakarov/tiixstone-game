<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SoupVendor
{
    public static function globalId() : string
    {
        return 'TRL_570';
    }

    public static function create()
    {
        return new Card\TRL_570;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_570';
    }
}