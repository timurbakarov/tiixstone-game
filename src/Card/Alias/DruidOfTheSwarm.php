<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DruidOfTheSwarm
{
    public static function globalId() : string
    {
        return 'ICC_051t3';
    }

    public static function create()
    {
        return new Card\ICC_051t3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_051t3';
    }
}