<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MikeDonais
{
    public static function globalId() : string
    {
        return 'CRED_36';
    }

    public static function create()
    {
        return new Card\CRED_36;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_36';
    }
}