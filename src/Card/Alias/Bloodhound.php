<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Bloodhound
{
    public static function globalId() : string
    {
        return 'GILA_400t';
    }

    public static function create()
    {
        return new Card\GILA_400t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_400t';
    }
}