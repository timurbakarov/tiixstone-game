<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BradCrusco
{
    public static function globalId() : string
    {
        return 'CRED_59';
    }

    public static function create()
    {
        return new Card\CRED_59;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_59';
    }
}