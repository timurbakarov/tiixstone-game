<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GrimestreetSmuggler
{
    public static function globalId() : string
    {
        return 'CFM_853';
    }

    public static function create()
    {
        return new Card\CFM_853;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_853';
    }
}