<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Chupacabran
{
    public static function globalId() : string
    {
        return 'GILA_BOSS_35h';
    }

    public static function create()
    {
        return new Card\GILA_BOSS_35h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_BOSS_35h';
    }
}