<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SaltyDog
{
    public static function globalId() : string
    {
        return 'GVG_070';
    }

    public static function create()
    {
        return new Card\GVG_070;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_070';
    }
}