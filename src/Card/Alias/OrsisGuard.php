<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OrsisGuard
{
    public static function globalId() : string
    {
        return 'LOEA04_13bt';
    }

    public static function create()
    {
        return new Card\LOEA04_13bt;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA04_13bt';
    }
}