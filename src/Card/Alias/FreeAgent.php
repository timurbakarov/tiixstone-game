<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FreeAgent
{
    public static function globalId() : string
    {
        return 'TRL_363t';
    }

    public static function create()
    {
        return new Card\TRL_363t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_363t';
    }
}