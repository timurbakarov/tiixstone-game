<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NalaaTheRedeemer
{
    public static function globalId() : string
    {
        return 'GILA_826';
    }

    public static function create()
    {
        return new Card\GILA_826;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_826';
    }
}