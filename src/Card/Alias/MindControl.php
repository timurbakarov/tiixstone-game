<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MindControl
{
    public static function globalId() : string
    {
        return 'CS1_113';
    }

    public static function create()
    {
        return new Card\CS1_113;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS1_113';
    }
}