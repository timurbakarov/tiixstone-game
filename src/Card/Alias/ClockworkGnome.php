<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ClockworkGnome
{
    public static function globalId() : string
    {
        return 'GVG_082';
    }

    public static function create()
    {
        return new Card\GVG_082;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_082';
    }
}