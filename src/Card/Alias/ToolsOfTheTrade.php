<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ToolsOfTheTrade
{
    public static function globalId() : string
    {
        return 'GILA_510';
    }

    public static function create()
    {
        return new Card\GILA_510;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_510';
    }
}