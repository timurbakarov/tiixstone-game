<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HauntedCurio
{
    public static function globalId() : string
    {
        return 'GILA_818';
    }

    public static function create()
    {
        return new Card\GILA_818;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_818';
    }
}