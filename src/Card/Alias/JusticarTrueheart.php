<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JusticarTrueheart
{
    public static function globalId() : string
    {
        return 'AT_132';
    }

    public static function create()
    {
        return new Card\AT_132;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_132';
    }
}