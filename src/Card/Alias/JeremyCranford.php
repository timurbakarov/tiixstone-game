<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JeremyCranford
{
    public static function globalId() : string
    {
        return 'CRED_31';
    }

    public static function create()
    {
        return new Card\CRED_31;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_31';
    }
}