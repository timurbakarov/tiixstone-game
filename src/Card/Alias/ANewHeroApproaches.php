<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ANewHeroApproaches
{
    public static function globalId() : string
    {
        return 'FB_LK_ClearBoard';
    }

    public static function create()
    {
        return new Card\FB_LK_ClearBoard;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK_ClearBoard';
    }
}