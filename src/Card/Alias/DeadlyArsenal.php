<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DeadlyArsenal
{
    public static function globalId() : string
    {
        return 'GIL_537';
    }

    public static function create()
    {
        return new Card\GIL_537;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_537';
    }
}