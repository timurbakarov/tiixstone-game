<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BoneConstruct
{
    public static function globalId() : string
    {
        return 'BRMA17_6H';
    }

    public static function create()
    {
        return new Card\BRMA17_6H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA17_6H';
    }
}