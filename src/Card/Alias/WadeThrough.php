<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WadeThrough
{
    public static function globalId() : string
    {
        return 'LOEA04_28b';
    }

    public static function create()
    {
        return new Card\LOEA04_28b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA04_28b';
    }
}