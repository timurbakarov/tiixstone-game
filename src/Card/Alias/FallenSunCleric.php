<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FallenSunCleric
{
    public static function globalId() : string
    {
        return 'ICC_094';
    }

    public static function create()
    {
        return new Card\ICC_094;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_094';
    }
}