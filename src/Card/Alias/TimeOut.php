<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TimeOut
{
    public static function globalId() : string
    {
        return 'TRL_302';
    }

    public static function create()
    {
        return new Card\TRL_302;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_302';
    }
}