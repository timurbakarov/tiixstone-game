<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WarpStalker
{
    public static function globalId() : string
    {
        return 'TRLA_183';
    }

    public static function create()
    {
        return new Card\TRLA_183;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_183';
    }
}