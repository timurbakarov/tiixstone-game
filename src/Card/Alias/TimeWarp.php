<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TimeWarp
{
    public static function globalId() : string
    {
        return 'UNG_028t';
    }

    public static function create()
    {
        return new Card\UNG_028t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_028t';
    }
}