<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ScourgelordGarrosh
{
    public static function globalId() : string
    {
        return 'ICC_834';
    }

    public static function create()
    {
        return new Card\ICC_834;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_834';
    }
}