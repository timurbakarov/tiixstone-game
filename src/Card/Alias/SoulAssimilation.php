<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SoulAssimilation
{
    public static function globalId() : string
    {
        return 'GILA_BOSS_55t2';
    }

    public static function create()
    {
        return new Card\GILA_BOSS_55t2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_BOSS_55t2';
    }
}