<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FoolsBane
{
    public static function globalId() : string
    {
        return 'KAR_028';
    }

    public static function create()
    {
        return new Card\KAR_028;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_028';
    }
}