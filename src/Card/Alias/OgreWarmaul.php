<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OgreWarmaul
{
    public static function globalId() : string
    {
        return 'GVG_054';
    }

    public static function create()
    {
        return new Card\GVG_054;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_054';
    }
}