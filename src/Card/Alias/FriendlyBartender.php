<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FriendlyBartender
{
    public static function globalId() : string
    {
        return 'CFM_654';
    }

    public static function create()
    {
        return new Card\CFM_654;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_654';
    }
}