<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PartyArmory
{
    public static function globalId() : string
    {
        return 'TB_SPT_MTH_BossWeapon';
    }

    public static function create()
    {
        return new Card\TB_SPT_MTH_BossWeapon;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_SPT_MTH_BossWeapon';
    }
}