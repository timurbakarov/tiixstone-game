<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WorthlessImp
{
    public static function globalId() : string
    {
        return 'EX1_317t';
    }

    public static function create()
    {
        return new Card\EX1_317t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_317t';
    }
}