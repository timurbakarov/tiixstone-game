<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DarkBargain
{
    public static function globalId() : string
    {
        return 'AT_025';
    }

    public static function create()
    {
        return new Card\AT_025;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_025';
    }
}