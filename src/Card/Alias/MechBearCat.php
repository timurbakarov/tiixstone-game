<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MechBearCat
{
    public static function globalId() : string
    {
        return 'GVG_034';
    }

    public static function create()
    {
        return new Card\GVG_034;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_034';
    }
}