<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OverchargedTotem
{
    public static function globalId() : string
    {
        return 'TRLA_158';
    }

    public static function create()
    {
        return new Card\TRLA_158;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_158';
    }
}