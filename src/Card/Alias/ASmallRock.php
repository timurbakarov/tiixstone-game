<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ASmallRock
{
    public static function globalId() : string
    {
        return 'GILA_500p2t';
    }

    public static function create()
    {
        return new Card\GILA_500p2t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_500p2t';
    }
}