<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VampiricBite
{
    public static function globalId() : string
    {
        return 'ICCA05_002p';
    }

    public static function create()
    {
        return new Card\ICCA05_002p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA05_002p';
    }
}