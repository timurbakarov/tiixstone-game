<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShandosLesson
{
    public static function globalId() : string
    {
        return 'EX1_573b';
    }

    public static function create()
    {
        return new Card\EX1_573b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_573b';
    }
}