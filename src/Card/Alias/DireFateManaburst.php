<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DireFateManaburst
{
    public static function globalId() : string
    {
        return 'TB_PickYourFate_7_2nd';
    }

    public static function create()
    {
        return new Card\TB_PickYourFate_7_2nd;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_PickYourFate_7_2nd';
    }
}