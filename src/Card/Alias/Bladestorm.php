<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Bladestorm
{
    public static function globalId() : string
    {
        return 'ICC_834h';
    }

    public static function create()
    {
        return new Card\ICC_834h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_834h';
    }
}