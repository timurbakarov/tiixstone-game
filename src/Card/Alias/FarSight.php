<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FarSight
{
    public static function globalId() : string
    {
        return 'CS2_053';
    }

    public static function create()
    {
        return new Card\CS2_053;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_053';
    }
}