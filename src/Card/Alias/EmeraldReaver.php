<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EmeraldReaver
{
    public static function globalId() : string
    {
        return 'UNG_803';
    }

    public static function create()
    {
        return new Card\UNG_803;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_803';
    }
}