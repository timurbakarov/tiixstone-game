<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BuildABeast
{
    public static function globalId() : string
    {
        return 'ICC_828p';
    }

    public static function create()
    {
        return new Card\ICC_828p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_828p';
    }
}