<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Candle
{
    public static function globalId() : string
    {
        return 'KAR_025a';
    }

    public static function create()
    {
        return new Card\KAR_025a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_025a';
    }
}