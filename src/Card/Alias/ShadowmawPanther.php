<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShadowmawPanther
{
    public static function globalId() : string
    {
        return 'TRLA_164';
    }

    public static function create()
    {
        return new Card\TRLA_164;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_164';
    }
}