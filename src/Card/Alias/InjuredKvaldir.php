<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class InjuredKvaldir
{
    public static function globalId() : string
    {
        return 'AT_105';
    }

    public static function create()
    {
        return new Card\AT_105;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_105';
    }
}