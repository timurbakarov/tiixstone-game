<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GreaterPotion
{
    public static function globalId() : string
    {
        return 'CFM_621t12';
    }

    public static function create()
    {
        return new Card\CFM_621t12;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_621t12';
    }
}