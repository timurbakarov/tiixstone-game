<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BattleAxe
{
    public static function globalId() : string
    {
        return 'EX1_398t';
    }

    public static function create()
    {
        return new Card\EX1_398t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_398t';
    }
}