<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IronhornDrummer
{
    public static function globalId() : string
    {
        return 'TRLA_174';
    }

    public static function create()
    {
        return new Card\TRLA_174;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_174';
    }
}