<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BraveArcher
{
    public static function globalId() : string
    {
        return 'AT_059';
    }

    public static function create()
    {
        return new Card\AT_059;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_059';
    }
}