<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FlyingMonkey
{
    public static function globalId() : string
    {
        return 'KARA_04_05h';
    }

    public static function create()
    {
        return new Card\KARA_04_05h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_04_05h';
    }
}