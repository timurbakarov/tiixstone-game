<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DunemaulShaman
{
    public static function globalId() : string
    {
        return 'GVG_066';
    }

    public static function create()
    {
        return new Card\GVG_066;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_066';
    }
}