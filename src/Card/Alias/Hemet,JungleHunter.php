<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HemetJungleHunter
{
    public static function globalId() : string
    {
        return 'UNG_840';
    }

    public static function create()
    {
        return new Card\UNG_840;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_840';
    }
}