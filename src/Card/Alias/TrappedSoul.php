<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TrappedSoul
{
    public static function globalId() : string
    {
        return 'ICCA08_033';
    }

    public static function create()
    {
        return new Card\ICCA08_033;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA08_033';
    }
}