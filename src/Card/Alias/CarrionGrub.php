<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CarrionGrub
{
    public static function globalId() : string
    {
        return 'OG_325';
    }

    public static function create()
    {
        return new Card\OG_325;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_325';
    }
}