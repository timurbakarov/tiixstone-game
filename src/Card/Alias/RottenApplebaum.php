<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RottenApplebaum
{
    public static function globalId() : string
    {
        return 'GIL_667';
    }

    public static function create()
    {
        return new Card\GIL_667;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_667';
    }
}