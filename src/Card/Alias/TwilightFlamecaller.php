<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TwilightFlamecaller
{
    public static function globalId() : string
    {
        return 'OG_083';
    }

    public static function create()
    {
        return new Card\OG_083;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_083';
    }
}