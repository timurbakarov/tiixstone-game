<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ActivateMagmatron
{
    public static function globalId() : string
    {
        return 'BRMA14_8';
    }

    public static function create()
    {
        return new Card\BRMA14_8;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA14_8';
    }
}