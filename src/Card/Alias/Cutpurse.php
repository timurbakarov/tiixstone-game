<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Cutpurse
{
    public static function globalId() : string
    {
        return 'AT_031';
    }

    public static function create()
    {
        return new Card\AT_031;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_031';
    }
}