<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LesserHeal
{
    public static function globalId() : string
    {
        return 'CS1h_001';
    }

    public static function create()
    {
        return new Card\CS1h_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS1h_001';
    }
}