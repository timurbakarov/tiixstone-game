<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShadowReflection
{
    public static function globalId() : string
    {
        return 'ICC_827t';
    }

    public static function create()
    {
        return new Card\ICC_827t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_827t';
    }
}