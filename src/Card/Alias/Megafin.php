<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Megafin
{
    public static function globalId() : string
    {
        return 'UNG_942t';
    }

    public static function create()
    {
        return new Card\UNG_942t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_942t';
    }
}