<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Zentimo
{
    public static function globalId() : string
    {
        return 'TRL_085';
    }

    public static function create()
    {
        return new Card\TRL_085;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_085';
    }
}