<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Continue
{
    public static function globalId() : string
    {
        return 'TB_LethalSetup001a';
    }

    public static function create()
    {
        return new Card\TB_LethalSetup001a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_LethalSetup001a';
    }
}