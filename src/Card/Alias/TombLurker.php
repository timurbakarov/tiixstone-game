<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TombLurker
{
    public static function globalId() : string
    {
        return 'ICC_098';
    }

    public static function create()
    {
        return new Card\ICC_098;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_098';
    }
}