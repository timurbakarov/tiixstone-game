<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HappyGhoul
{
    public static function globalId() : string
    {
        return 'ICC_700';
    }

    public static function create()
    {
        return new Card\ICC_700;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_700';
    }
}