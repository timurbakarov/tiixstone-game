<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CrowdFavorite
{
    public static function globalId() : string
    {
        return 'AT_121';
    }

    public static function create()
    {
        return new Card\AT_121;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_121';
    }
}