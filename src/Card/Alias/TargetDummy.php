<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TargetDummy
{
    public static function globalId() : string
    {
        return 'GVG_093';
    }

    public static function create()
    {
        return new Card\GVG_093;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_093';
    }
}