<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BigWisps
{
    public static function globalId() : string
    {
        return 'OG_195b';
    }

    public static function create()
    {
        return new Card\OG_195b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_195b';
    }
}