<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HadidjahChamberlin
{
    public static function globalId() : string
    {
        return 'CRED_54';
    }

    public static function create()
    {
        return new Card\CRED_54;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_54';
    }
}