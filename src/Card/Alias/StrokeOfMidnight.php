<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StrokeOfMidnight
{
    public static function globalId() : string
    {
        return 'GILA_904';
    }

    public static function create()
    {
        return new Card\GILA_904;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_904';
    }
}