<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RemorselessWinter
{
    public static function globalId() : string
    {
        return 'FB_LK005';
    }

    public static function create()
    {
        return new Card\FB_LK005;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK005';
    }
}