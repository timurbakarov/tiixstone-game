<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LightsChampion
{
    public static function globalId() : string
    {
        return 'AT_106';
    }

    public static function create()
    {
        return new Card\AT_106;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_106';
    }
}