<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CurioCollector
{
    public static function globalId() : string
    {
        return 'GIL_640';
    }

    public static function create()
    {
        return new Card\GIL_640;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_640';
    }
}