<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TransmuteSpirit
{
    public static function globalId() : string
    {
        return 'ICC_481p';
    }

    public static function create()
    {
        return new Card\ICC_481p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_481p';
    }
}