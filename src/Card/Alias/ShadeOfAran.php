<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShadeOfAran
{
    public static function globalId() : string
    {
        return 'KARA_12_01H';
    }

    public static function create()
    {
        return new Card\KARA_12_01H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_12_01H';
    }
}