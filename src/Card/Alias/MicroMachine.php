<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MicroMachine
{
    public static function globalId() : string
    {
        return 'GVG_103';
    }

    public static function create()
    {
        return new Card\GVG_103;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_103';
    }
}