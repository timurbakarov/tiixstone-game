<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WitchsCauldron
{
    public static function globalId() : string
    {
        return 'GIL_819';
    }

    public static function create()
    {
        return new Card\GIL_819;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_819';
    }
}