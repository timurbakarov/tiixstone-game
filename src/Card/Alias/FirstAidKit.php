<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FirstAidKit
{
    public static function globalId() : string
    {
        return 'GILA_506';
    }

    public static function create()
    {
        return new Card\GILA_506;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_506';
    }
}