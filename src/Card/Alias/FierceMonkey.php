<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FierceMonkey
{
    public static function globalId() : string
    {
        return 'LOE_022';
    }

    public static function create()
    {
        return new Card\LOE_022;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_022';
    }
}