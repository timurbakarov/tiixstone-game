<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Enhanceamatic
{
    public static function globalId() : string
    {
        return 'GILA_903';
    }

    public static function create()
    {
        return new Card\GILA_903;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_903';
    }
}