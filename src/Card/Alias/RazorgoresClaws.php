<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RazorgoresClaws
{
    public static function globalId() : string
    {
        return 'BRMA10_6';
    }

    public static function create()
    {
        return new Card\BRMA10_6;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA10_6';
    }
}