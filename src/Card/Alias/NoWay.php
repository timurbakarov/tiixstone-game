<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NoWay
{
    public static function globalId() : string
    {
        return 'LOEA04_31b';
    }

    public static function create()
    {
        return new Card\LOEA04_31b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA04_31b';
    }
}