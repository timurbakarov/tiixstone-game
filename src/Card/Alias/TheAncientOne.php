<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheAncientOne
{
    public static function globalId() : string
    {
        return 'OG_173a';
    }

    public static function create()
    {
        return new Card\OG_173a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_173a';
    }
}