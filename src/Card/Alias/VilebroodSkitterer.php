<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VilebroodSkitterer
{
    public static function globalId() : string
    {
        return 'ICC_828t6';
    }

    public static function create()
    {
        return new Card\ICC_828t6;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_828t6';
    }
}