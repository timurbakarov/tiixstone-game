<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ForkedLightning
{
    public static function globalId() : string
    {
        return 'EX1_251';
    }

    public static function create()
    {
        return new Card\EX1_251;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_251';
    }
}