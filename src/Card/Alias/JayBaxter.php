<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JayBaxter
{
    public static function globalId() : string
    {
        return 'CRED_11';
    }

    public static function create()
    {
        return new Card\CRED_11;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_11';
    }
}