<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BalefulBanker
{
    public static function globalId() : string
    {
        return 'GIL_815';
    }

    public static function create()
    {
        return new Card\GIL_815;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_815';
    }
}