<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShadowVolley
{
    public static function globalId() : string
    {
        return 'KARA_09_06';
    }

    public static function create()
    {
        return new Card\KARA_09_06;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_09_06';
    }
}