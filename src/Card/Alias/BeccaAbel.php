<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BeccaAbel
{
    public static function globalId() : string
    {
        return 'CRED_18';
    }

    public static function create()
    {
        return new Card\CRED_18;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_18';
    }
}