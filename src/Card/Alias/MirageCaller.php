<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MirageCaller
{
    public static function globalId() : string
    {
        return 'UNG_022';
    }

    public static function create()
    {
        return new Card\UNG_022;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_022';
    }
}