<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OneeyedCheat
{
    public static function globalId() : string
    {
        return 'GVG_025';
    }

    public static function create()
    {
        return new Card\GVG_025;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_025';
    }
}