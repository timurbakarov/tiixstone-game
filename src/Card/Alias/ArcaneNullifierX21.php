<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArcaneNullifierX21
{
    public static function globalId() : string
    {
        return 'GVG_091';
    }

    public static function create()
    {
        return new Card\GVG_091;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_091';
    }
}