<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RottenBanana
{
    public static function globalId() : string
    {
        return 'TB_008';
    }

    public static function create()
    {
        return new Card\TB_008;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_008';
    }
}