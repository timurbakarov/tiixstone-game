<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MistressOfMixtures
{
    public static function globalId() : string
    {
        return 'CFM_120';
    }

    public static function create()
    {
        return new Card\CFM_120;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_120';
    }
}