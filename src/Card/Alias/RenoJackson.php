<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RenoJackson
{
    public static function globalId() : string
    {
        return 'LOE_011';
    }

    public static function create()
    {
        return new Card\LOE_011;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_011';
    }
}