<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HolyWater
{
    public static function globalId() : string
    {
        return 'GIL_134';
    }

    public static function create()
    {
        return new Card\GIL_134;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_134';
    }
}