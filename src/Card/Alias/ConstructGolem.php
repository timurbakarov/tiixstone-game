<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ConstructGolem
{
    public static function globalId() : string
    {
        return 'TB_BossRumble_002hp';
    }

    public static function create()
    {
        return new Card\TB_BossRumble_002hp;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_BossRumble_002hp';
    }
}