<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AmaraWardenOfHope
{
    public static function globalId() : string
    {
        return 'UNG_940t8';
    }

    public static function create()
    {
        return new Card\UNG_940t8;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_940t8';
    }
}