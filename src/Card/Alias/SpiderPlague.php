<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpiderPlague
{
    public static function globalId() : string
    {
        return 'ICC_832b';
    }

    public static function create()
    {
        return new Card\ICC_832b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_832b';
    }
}