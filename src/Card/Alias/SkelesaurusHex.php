<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SkelesaurusHex
{
    public static function globalId() : string
    {
        return 'LOEA16_26H';
    }

    public static function create()
    {
        return new Card\LOEA16_26H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA16_26H';
    }
}