<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BlackKing
{
    public static function globalId() : string
    {
        return 'KAR_a10_Boss2H_TB';
    }

    public static function create()
    {
        return new Card\KAR_a10_Boss2H_TB;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_a10_Boss2H_TB';
    }
}