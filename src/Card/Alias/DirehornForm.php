<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DirehornForm
{
    public static function globalId() : string
    {
        return 'UNG_101b';
    }

    public static function create()
    {
        return new Card\UNG_101b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_101b';
    }
}