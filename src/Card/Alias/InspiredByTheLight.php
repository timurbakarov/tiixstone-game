<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class InspiredByTheLight
{
    public static function globalId() : string
    {
        return 'TRLA_Priest_03';
    }

    public static function create()
    {
        return new Card\TRLA_Priest_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Priest_03';
    }
}