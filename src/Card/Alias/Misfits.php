<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Misfits
{
    public static function globalId() : string
    {
        return 'TRLA_Rogue_10';
    }

    public static function create()
    {
        return new Card\TRLA_Rogue_10;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Rogue_10';
    }
}