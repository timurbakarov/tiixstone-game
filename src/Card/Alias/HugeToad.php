<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HugeToad
{
    public static function globalId() : string
    {
        return 'LOE_046';
    }

    public static function create()
    {
        return new Card\LOE_046;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_046';
    }
}