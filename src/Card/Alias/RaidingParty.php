<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RaidingParty
{
    public static function globalId() : string
    {
        return 'TRL_124';
    }

    public static function create()
    {
        return new Card\TRL_124;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_124';
    }
}