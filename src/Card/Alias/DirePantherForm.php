<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DirePantherForm
{
    public static function globalId() : string
    {
        return 'GIL_188a';
    }

    public static function create()
    {
        return new Card\GIL_188a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_188a';
    }
}