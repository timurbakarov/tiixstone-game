<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheMistcaller
{
    public static function globalId() : string
    {
        return 'AT_054';
    }

    public static function create()
    {
        return new Card\AT_054;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_054';
    }
}