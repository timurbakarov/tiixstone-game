<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShakyZipgunner
{
    public static function globalId() : string
    {
        return 'CFM_336';
    }

    public static function create()
    {
        return new Card\CFM_336;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_336';
    }
}