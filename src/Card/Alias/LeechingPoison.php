<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LeechingPoison
{
    public static function globalId() : string
    {
        return 'ICC_221';
    }

    public static function create()
    {
        return new Card\ICC_221;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_221';
    }
}