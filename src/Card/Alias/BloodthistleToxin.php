<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodthistleToxin
{
    public static function globalId() : string
    {
        return 'OG_080c';
    }

    public static function create()
    {
        return new Card\OG_080c;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_080c';
    }
}