<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Flamewaker
{
    public static function globalId() : string
    {
        return 'BRM_002';
    }

    public static function create()
    {
        return new Card\BRM_002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_002';
    }
}