<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class UtherOfTheEbonBlade
{
    public static function globalId() : string
    {
        return 'ICC_829';
    }

    public static function create()
    {
        return new Card\ICC_829;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_829';
    }
}