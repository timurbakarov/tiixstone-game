<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EliseTheTrailblazer
{
    public static function globalId() : string
    {
        return 'UNG_851';
    }

    public static function create()
    {
        return new Card\UNG_851;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_851';
    }
}