<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SharkfinFan
{
    public static function globalId() : string
    {
        return 'TRL_507';
    }

    public static function create()
    {
        return new Card\TRL_507;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_507';
    }
}