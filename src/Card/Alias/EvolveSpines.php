<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EvolveSpines
{
    public static function globalId() : string
    {
        return 'OG_047a';
    }

    public static function create()
    {
        return new Card\OG_047a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_047a';
    }
}