<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DecoratedStormwind
{
    public static function globalId() : string
    {
        return 'TB_SPT_MTH_Boss';
    }

    public static function create()
    {
        return new Card\TB_SPT_MTH_Boss;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_SPT_MTH_Boss';
    }
}