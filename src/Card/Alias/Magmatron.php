<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Magmatron
{
    public static function globalId() : string
    {
        return 'BRMA14_9';
    }

    public static function create()
    {
        return new Card\BRMA14_9;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA14_9';
    }
}