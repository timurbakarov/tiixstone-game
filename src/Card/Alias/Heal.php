<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Heal
{
    public static function globalId() : string
    {
        return 'CS1h_001_H1_AT_132';
    }

    public static function create()
    {
        return new Card\CS1h_001_H1_AT_132;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS1h_001_H1_AT_132';
    }
}