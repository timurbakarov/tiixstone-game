<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CobaltScalebane
{
    public static function globalId() : string
    {
        return 'ICC_029';
    }

    public static function create()
    {
        return new Card\ICC_029;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_029';
    }
}