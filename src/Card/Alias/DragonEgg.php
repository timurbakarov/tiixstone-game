<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DragonEgg
{
    public static function globalId() : string
    {
        return 'BRM_022';
    }

    public static function create()
    {
        return new Card\BRM_022;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_022';
    }
}