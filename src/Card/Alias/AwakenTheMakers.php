<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AwakenTheMakers
{
    public static function globalId() : string
    {
        return 'UNG_940';
    }

    public static function create()
    {
        return new Card\UNG_940;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_940';
    }
}