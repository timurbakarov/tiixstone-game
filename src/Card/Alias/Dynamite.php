<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Dynamite
{
    public static function globalId() : string
    {
        return 'LOEA07_18';
    }

    public static function create()
    {
        return new Card\LOEA07_18;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA07_18';
    }
}