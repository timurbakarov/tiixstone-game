<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SackOfCoins
{
    public static function globalId() : string
    {
        return 'GILA_816b';
    }

    public static function create()
    {
        return new Card\GILA_816b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_816b';
    }
}