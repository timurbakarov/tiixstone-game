<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShadowOfNothing
{
    public static function globalId() : string
    {
        return 'EX1_345t';
    }

    public static function create()
    {
        return new Card\EX1_345t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_345t';
    }
}