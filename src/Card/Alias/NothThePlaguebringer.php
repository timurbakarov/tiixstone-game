<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NothThePlaguebringer
{
    public static function globalId() : string
    {
        return 'TB_KTRAF_10';
    }

    public static function create()
    {
        return new Card\TB_KTRAF_10;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_KTRAF_10';
    }
}