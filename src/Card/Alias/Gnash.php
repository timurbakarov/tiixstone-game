<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Gnash
{
    public static function globalId() : string
    {
        return 'ICC_079';
    }

    public static function create()
    {
        return new Card\ICC_079;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_079';
    }
}