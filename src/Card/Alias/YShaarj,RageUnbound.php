<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class YShaarjRageUnbound
{
    public static function globalId() : string
    {
        return 'OG_042';
    }

    public static function create()
    {
        return new Card\OG_042;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_042';
    }
}