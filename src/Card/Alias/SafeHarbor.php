<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SafeHarbor
{
    public static function globalId() : string
    {
        return 'GILA_608';
    }

    public static function create()
    {
        return new Card\GILA_608;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_608';
    }
}