<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PrimordialDrake
{
    public static function globalId() : string
    {
        return 'UNG_848';
    }

    public static function create()
    {
        return new Card\UNG_848;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_848';
    }
}