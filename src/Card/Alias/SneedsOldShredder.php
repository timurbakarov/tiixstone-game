<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SneedsOldShredder
{
    public static function globalId() : string
    {
        return 'GVG_114';
    }

    public static function create()
    {
        return new Card\GVG_114;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_114';
    }
}