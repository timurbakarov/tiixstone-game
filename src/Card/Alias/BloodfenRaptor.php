<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodfenRaptor
{
    public static function globalId() : string
    {
        return 'CS2_172';
    }

    public static function create()
    {
        return new Card\CS2_172;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_172';
    }
}