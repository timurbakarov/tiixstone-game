<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Plant
{
    public static function globalId() : string
    {
        return 'UNG_999t2t1';
    }

    public static function create()
    {
        return new Card\UNG_999t2t1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_999t2t1';
    }
}