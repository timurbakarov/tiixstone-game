<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HoggerSMASH
{
    public static function globalId() : string
    {
        return 'TU4a_004';
    }

    public static function create()
    {
        return new Card\TU4a_004;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TU4a_004';
    }
}