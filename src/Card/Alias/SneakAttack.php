<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SneakAttack
{
    public static function globalId() : string
    {
        return 'GILA_810';
    }

    public static function create()
    {
        return new Card\GILA_810;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_810';
    }
}