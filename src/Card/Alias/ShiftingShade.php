<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShiftingShade
{
    public static function globalId() : string
    {
        return 'OG_335';
    }

    public static function create()
    {
        return new Card\OG_335;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_335';
    }
}