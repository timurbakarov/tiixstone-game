<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ChargedHammer
{
    public static function globalId() : string
    {
        return 'AT_050';
    }

    public static function create()
    {
        return new Card\AT_050;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_050';
    }
}