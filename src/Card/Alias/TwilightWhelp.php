<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TwilightWhelp
{
    public static function globalId() : string
    {
        return 'BRM_004';
    }

    public static function create()
    {
        return new Card\BRM_004;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_004';
    }
}