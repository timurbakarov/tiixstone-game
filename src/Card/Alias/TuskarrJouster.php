<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TuskarrJouster
{
    public static function globalId() : string
    {
        return 'AT_104';
    }

    public static function create()
    {
        return new Card\AT_104;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_104';
    }
}