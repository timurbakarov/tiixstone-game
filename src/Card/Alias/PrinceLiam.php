<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PrinceLiam
{
    public static function globalId() : string
    {
        return 'GIL_694';
    }

    public static function create()
    {
        return new Card\GIL_694;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_694';
    }
}