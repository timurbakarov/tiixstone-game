<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BrianFarr
{
    public static function globalId() : string
    {
        return 'CRED_50';
    }

    public static function create()
    {
        return new Card\CRED_50;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_50';
    }
}