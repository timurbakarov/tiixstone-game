<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NightscaleWhelp
{
    public static function globalId() : string
    {
        return 'GIL_190t';
    }

    public static function create()
    {
        return new Card\GIL_190t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_190t';
    }
}