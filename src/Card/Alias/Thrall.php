<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Thrall
{
    public static function globalId() : string
    {
        return 'HERO_02';
    }

    public static function create()
    {
        return new Card\HERO_02;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HERO_02';
    }
}