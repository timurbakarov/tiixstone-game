<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AstralRaptor
{
    public static function globalId() : string
    {
        return 'TRLA_127';
    }

    public static function create()
    {
        return new Card\TRLA_127;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_127';
    }
}