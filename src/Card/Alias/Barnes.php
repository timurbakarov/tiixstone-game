<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Barnes
{
    public static function globalId() : string
    {
        return 'KAR_114';
    }

    public static function create()
    {
        return new Card\KAR_114;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_114';
    }
}