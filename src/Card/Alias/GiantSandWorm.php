<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GiantSandWorm
{
    public static function globalId() : string
    {
        return 'OG_308';
    }

    public static function create()
    {
        return new Card\OG_308;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_308';
    }
}