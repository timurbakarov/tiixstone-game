<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PhantomFreebooter
{
    public static function globalId() : string
    {
        return 'ICC_018';
    }

    public static function create()
    {
        return new Card\ICC_018;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_018';
    }
}