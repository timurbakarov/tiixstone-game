<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheCowKing
{
    public static function globalId() : string
    {
        return 'TB_SPT_DPromo_Hero2';
    }

    public static function create()
    {
        return new Card\TB_SPT_DPromo_Hero2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_SPT_DPromo_Hero2';
    }
}