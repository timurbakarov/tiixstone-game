<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodsailCultist
{
    public static function globalId() : string
    {
        return 'OG_315';
    }

    public static function create()
    {
        return new Card\OG_315;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_315';
    }
}