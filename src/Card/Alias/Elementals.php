<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Elementals
{
    public static function globalId() : string
    {
        return 'TRLA_Priest_05';
    }

    public static function create()
    {
        return new Card\TRLA_Priest_05;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Priest_05';
    }
}