<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SkullOfTheManari
{
    public static function globalId() : string
    {
        return 'LOOT_420';
    }

    public static function create()
    {
        return new Card\LOOT_420;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOOT_420';
    }
}