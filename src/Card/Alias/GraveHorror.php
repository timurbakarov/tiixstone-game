<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GraveHorror
{
    public static function globalId() : string
    {
        return 'TRL_408';
    }

    public static function create()
    {
        return new Card\TRL_408;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_408';
    }
}