<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Scarab
{
    public static function globalId() : string
    {
        return 'TRL_503t';
    }

    public static function create()
    {
        return new Card\TRL_503t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_503t';
    }
}