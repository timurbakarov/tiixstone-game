<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FanOfFlames
{
    public static function globalId() : string
    {
        return 'TRLA_135';
    }

    public static function create()
    {
        return new Card\TRLA_135;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_135';
    }
}