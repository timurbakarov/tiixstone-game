<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KnightOfTheWild
{
    public static function globalId() : string
    {
        return 'AT_041';
    }

    public static function create()
    {
        return new Card\AT_041;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_041';
    }
}