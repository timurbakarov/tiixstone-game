<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AnimatedBerserker
{
    public static function globalId() : string
    {
        return 'ICC_238';
    }

    public static function create()
    {
        return new Card\ICC_238;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_238';
    }
}