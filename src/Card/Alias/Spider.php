<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Spider
{
    public static function globalId() : string
    {
        return 'OG_216a';
    }

    public static function create()
    {
        return new Card\OG_216a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_216a';
    }
}