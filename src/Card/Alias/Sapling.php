<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Sapling
{
    public static function globalId() : string
    {
        return 'AT_037t';
    }

    public static function create()
    {
        return new Card\AT_037t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_037t';
    }
}