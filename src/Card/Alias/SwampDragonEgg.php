<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SwampDragonEgg
{
    public static function globalId() : string
    {
        return 'GIL_816';
    }

    public static function create()
    {
        return new Card\GIL_816;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_816';
    }
}