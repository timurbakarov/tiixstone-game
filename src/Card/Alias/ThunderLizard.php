<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ThunderLizard
{
    public static function globalId() : string
    {
        return 'UNG_082';
    }

    public static function create()
    {
        return new Card\UNG_082;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_082';
    }
}