<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WorgenTracker
{
    public static function globalId() : string
    {
        return 'GILA_851c';
    }

    public static function create()
    {
        return new Card\GILA_851c;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_851c';
    }
}