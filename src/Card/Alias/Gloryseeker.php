<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Gloryseeker
{
    public static function globalId() : string
    {
        return 'TRLA_143';
    }

    public static function create()
    {
        return new Card\TRLA_143;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_143';
    }
}