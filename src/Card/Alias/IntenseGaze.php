<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IntenseGaze
{
    public static function globalId() : string
    {
        return 'BRMA08_2';
    }

    public static function create()
    {
        return new Card\BRMA08_2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA08_2';
    }
}