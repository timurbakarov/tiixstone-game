<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HireeksLegion
{
    public static function globalId() : string
    {
        return 'TRLA_Warlock_01';
    }

    public static function create()
    {
        return new Card\TRLA_Warlock_01;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Warlock_01';
    }
}