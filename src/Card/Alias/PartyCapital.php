<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PartyCapital
{
    public static function globalId() : string
    {
        return 'TB_SPT_MTH_Boss3';
    }

    public static function create()
    {
        return new Card\TB_SPT_MTH_Boss3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_SPT_MTH_Boss3';
    }
}