<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GrimestreetEnforcer
{
    public static function globalId() : string
    {
        return 'CFM_639';
    }

    public static function create()
    {
        return new Card\CFM_639;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_639';
    }
}