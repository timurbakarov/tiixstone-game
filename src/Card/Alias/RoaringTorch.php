<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RoaringTorch
{
    public static function globalId() : string
    {
        return 'LOE_002t';
    }

    public static function create()
    {
        return new Card\LOE_002t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_002t';
    }
}