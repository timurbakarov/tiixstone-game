<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BlackCat
{
    public static function globalId() : string
    {
        return 'GIL_838';
    }

    public static function create()
    {
        return new Card\GIL_838;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_838';
    }
}