<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Powermace
{
    public static function globalId() : string
    {
        return 'GVG_036';
    }

    public static function create()
    {
        return new Card\GVG_036;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_036';
    }
}