<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VilefinInquisitor
{
    public static function globalId() : string
    {
        return 'OG_006';
    }

    public static function create()
    {
        return new Card\OG_006;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_006';
    }
}