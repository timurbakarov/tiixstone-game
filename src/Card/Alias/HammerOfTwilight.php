<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HammerOfTwilight
{
    public static function globalId() : string
    {
        return 'OG_031';
    }

    public static function create()
    {
        return new Card\OG_031;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_031';
    }
}