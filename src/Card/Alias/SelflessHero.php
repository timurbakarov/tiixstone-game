<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SelflessHero
{
    public static function globalId() : string
    {
        return 'OG_221';
    }

    public static function create()
    {
        return new Card\OG_221;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_221';
    }
}