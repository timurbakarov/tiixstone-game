<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HeadlessHorseman(noHead)
{
    public static function globalId() : string
    {
        return 'TB_HeadlessHorseman_H1a';
    }

    public static function create()
    {
        return new Card\TB_HeadlessHorseman_H1a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_HeadlessHorseman_H1a';
    }
}