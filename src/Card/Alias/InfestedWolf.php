<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class InfestedWolf
{
    public static function globalId() : string
    {
        return 'OG_216';
    }

    public static function create()
    {
        return new Card\OG_216;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_216';
    }
}