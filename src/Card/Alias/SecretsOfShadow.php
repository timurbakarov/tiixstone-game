<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SecretsOfShadow
{
    public static function globalId() : string
    {
        return 'TB_Coopv3_102a';
    }

    public static function create()
    {
        return new Card\TB_Coopv3_102a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_Coopv3_102a';
    }
}