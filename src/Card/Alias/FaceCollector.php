<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FaceCollector
{
    public static function globalId() : string
    {
        return 'GIL_677';
    }

    public static function create()
    {
        return new Card\GIL_677;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_677';
    }
}