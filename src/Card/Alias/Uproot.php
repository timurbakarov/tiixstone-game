<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Uproot
{
    public static function globalId() : string
    {
        return 'EX1_178b';
    }

    public static function create()
    {
        return new Card\EX1_178b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_178b';
    }
}