<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StampedingKodo
{
    public static function globalId() : string
    {
        return 'NEW1_041';
    }

    public static function create()
    {
        return new Card\NEW1_041;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NEW1_041';
    }
}