<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KabalSongstealer
{
    public static function globalId() : string
    {
        return 'CFM_657';
    }

    public static function create()
    {
        return new Card\CFM_657;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_657';
    }
}