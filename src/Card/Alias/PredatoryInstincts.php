<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PredatoryInstincts
{
    public static function globalId() : string
    {
        return 'TRL_244';
    }

    public static function create()
    {
        return new Card\TRL_244;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_244';
    }
}