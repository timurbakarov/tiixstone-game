<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BenThompson
{
    public static function globalId() : string
    {
        return 'CRED_09';
    }

    public static function create()
    {
        return new Card\CRED_09;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_09';
    }
}