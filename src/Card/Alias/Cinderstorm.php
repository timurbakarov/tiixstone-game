<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Cinderstorm
{
    public static function globalId() : string
    {
        return 'GIL_147';
    }

    public static function create()
    {
        return new Card\GIL_147;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_147';
    }
}