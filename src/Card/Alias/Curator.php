<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Curator
{
    public static function globalId() : string
    {
        return 'KARA_07_01heroic';
    }

    public static function create()
    {
        return new Card\KARA_07_01heroic;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_07_01heroic';
    }
}