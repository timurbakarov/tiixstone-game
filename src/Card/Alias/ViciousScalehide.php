<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ViciousScalehide
{
    public static function globalId() : string
    {
        return 'ICC_828t7';
    }

    public static function create()
    {
        return new Card\ICC_828t7;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_828t7';
    }
}