<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BrannBronzebeard
{
    public static function globalId() : string
    {
        return 'LOE_077';
    }

    public static function create()
    {
        return new Card\LOE_077;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_077';
    }
}