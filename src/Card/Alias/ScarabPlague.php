<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ScarabPlague
{
    public static function globalId() : string
    {
        return 'ICC_832a';
    }

    public static function create()
    {
        return new Card\ICC_832a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_832a';
    }
}