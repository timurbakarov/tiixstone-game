<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SwiftMessenger
{
    public static function globalId() : string
    {
        return 'GIL_528t';
    }

    public static function create()
    {
        return new Card\GIL_528t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_528t';
    }
}