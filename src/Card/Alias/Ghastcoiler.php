<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Ghastcoiler
{
    public static function globalId() : string
    {
        return 'TRLA_149';
    }

    public static function create()
    {
        return new Card\TRLA_149;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_149';
    }
}