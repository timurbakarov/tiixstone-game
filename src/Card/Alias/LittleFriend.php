<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class "LittleFriend"
{
    public static function globalId() : string
    {
        return 'CFM_648t';
    }

    public static function create()
    {
        return new Card\CFM_648t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_648t';
    }
}