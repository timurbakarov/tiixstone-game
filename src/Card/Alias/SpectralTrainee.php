<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpectralTrainee
{
    public static function globalId() : string
    {
        return 'NAX8_03t';
    }

    public static function create()
    {
        return new Card\NAX8_03t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX8_03t';
    }
}