<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GalleryProtection
{
    public static function globalId() : string
    {
        return 'KARA_07_02';
    }

    public static function create()
    {
        return new Card\KARA_07_02;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_07_02';
    }
}