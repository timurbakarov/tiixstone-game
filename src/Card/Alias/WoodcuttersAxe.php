<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WoodcuttersAxe
{
    public static function globalId() : string
    {
        return 'GIL_653';
    }

    public static function create()
    {
        return new Card\GIL_653;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_653';
    }
}