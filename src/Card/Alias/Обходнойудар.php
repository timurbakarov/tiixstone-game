<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Обходнойудар
{
    public static function globalId() : string
    {
        return 'LOOT_077';
    }

    public static function create()
    {
        return new Card\LOOT_077;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOOT_077';
    }
}