<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ExplorersHat
{
    public static function globalId() : string
    {
        return 'LOE_105';
    }

    public static function create()
    {
        return new Card\LOE_105;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_105';
    }
}