<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AstralPortal
{
    public static function globalId() : string
    {
        return 'KARA_00_07';
    }

    public static function create()
    {
        return new Card\KARA_00_07;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_00_07';
    }
}