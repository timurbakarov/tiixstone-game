<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KingMosh
{
    public static function globalId() : string
    {
        return 'UNG_933';
    }

    public static function create()
    {
        return new Card\UNG_933;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_933';
    }
}