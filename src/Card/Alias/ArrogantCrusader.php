<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArrogantCrusader
{
    public static function globalId() : string
    {
        return 'ICC_034';
    }

    public static function create()
    {
        return new Card\ICC_034;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_034';
    }
}