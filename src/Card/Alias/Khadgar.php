<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Khadgar
{
    public static function globalId() : string
    {
        return 'HERO_08b';
    }

    public static function create()
    {
        return new Card\HERO_08b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HERO_08b';
    }
}