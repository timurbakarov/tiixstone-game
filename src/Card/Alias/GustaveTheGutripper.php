<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GustaveTheGutripper
{
    public static function globalId() : string
    {
        return 'GILA_BOSS_62h';
    }

    public static function create()
    {
        return new Card\GILA_BOSS_62h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_BOSS_62h';
    }
}