<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GuardianOfIcecrown
{
    public static function globalId() : string
    {
        return 'NAX15_03t';
    }

    public static function create()
    {
        return new Card\NAX15_03t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX15_03t';
    }
}