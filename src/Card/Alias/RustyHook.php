<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RustyHook
{
    public static function globalId() : string
    {
        return 'OG_058';
    }

    public static function create()
    {
        return new Card\OG_058;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_058';
    }
}