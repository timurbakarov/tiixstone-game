<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WhirlingZapomatic
{
    public static function globalId() : string
    {
        return 'GVG_037';
    }

    public static function create()
    {
        return new Card\GVG_037;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_037';
    }
}