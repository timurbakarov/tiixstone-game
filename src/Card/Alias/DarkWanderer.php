<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DarkWanderer
{
    public static function globalId() : string
    {
        return 'TB_SPT_DPromo_Hero';
    }

    public static function create()
    {
        return new Card\TB_SPT_DPromo_Hero;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_SPT_DPromo_Hero';
    }
}