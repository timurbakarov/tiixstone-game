<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DarionMograine
{
    public static function globalId() : string
    {
        return 'ICC_829t5';
    }

    public static function create()
    {
        return new Card\ICC_829t5;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_829t5';
    }
}