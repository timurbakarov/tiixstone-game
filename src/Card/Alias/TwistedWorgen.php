<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TwistedWorgen
{
    public static function globalId() : string
    {
        return 'OG_247';
    }

    public static function create()
    {
        return new Card\OG_247;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_247';
    }
}