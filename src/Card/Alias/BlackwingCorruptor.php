<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BlackwingCorruptor
{
    public static function globalId() : string
    {
        return 'BRM_034';
    }

    public static function create()
    {
        return new Card\BRM_034;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_034';
    }
}