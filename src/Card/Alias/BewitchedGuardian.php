<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BewitchedGuardian
{
    public static function globalId() : string
    {
        return 'GIL_507';
    }

    public static function create()
    {
        return new Card\GIL_507;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_507';
    }
}