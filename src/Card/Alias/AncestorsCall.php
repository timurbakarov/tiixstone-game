<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AncestorsCall
{
    public static function globalId() : string
    {
        return 'GVG_029';
    }

    public static function create()
    {
        return new Card\GVG_029;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_029';
    }
}