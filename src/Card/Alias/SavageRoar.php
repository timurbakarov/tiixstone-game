<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SavageRoar
{
    public static function globalId() : string
    {
        return 'CS2_011';
    }

    public static function create()
    {
        return new Card\CS2_011;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_011';
    }
}