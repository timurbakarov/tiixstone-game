<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WrathOfTheLoas
{
    public static function globalId() : string
    {
        return 'TRLA_806';
    }

    public static function create()
    {
        return new Card\TRLA_806;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_806';
    }
}