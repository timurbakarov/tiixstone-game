<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GrizzledReinforcement
{
    public static function globalId() : string
    {
        return 'GILA_610';
    }

    public static function create()
    {
        return new Card\GILA_610;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_610';
    }
}