<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MalfurionThePestilent
{
    public static function globalId() : string
    {
        return 'ICC_832';
    }

    public static function create()
    {
        return new Card\ICC_832;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_832';
    }
}