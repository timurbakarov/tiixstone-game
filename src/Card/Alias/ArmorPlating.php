<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArmorPlating
{
    public static function globalId() : string
    {
        return 'PART_001';
    }

    public static function create()
    {
        return new Card\PART_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\PART_001';
    }
}