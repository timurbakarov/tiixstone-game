<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CoreRager
{
    public static function globalId() : string
    {
        return 'BRM_014';
    }

    public static function create()
    {
        return new Card\BRM_014;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_014';
    }
}