<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Shatter
{
    public static function globalId() : string
    {
        return 'OG_081';
    }

    public static function create()
    {
        return new Card\OG_081;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_081';
    }
}