<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class InfiniteToki
{
    public static function globalId() : string
    {
        return 'GILA_BOSS_48h';
    }

    public static function create()
    {
        return new Card\GILA_BOSS_48h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_BOSS_48h';
    }
}