<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Lightbomb
{
    public static function globalId() : string
    {
        return 'GVG_008';
    }

    public static function create()
    {
        return new Card\GVG_008;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_008';
    }
}