<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Jaws
{
    public static function globalId() : string
    {
        return 'NAX12_03';
    }

    public static function create()
    {
        return new Card\NAX12_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX12_03';
    }
}