<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodPact
{
    public static function globalId() : string
    {
        return 'TRLA_113';
    }

    public static function create()
    {
        return new Card\TRLA_113;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_113';
    }
}