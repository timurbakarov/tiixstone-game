<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DIEINSECTS
{
    public static function globalId() : string
    {
        return 'BRM_027pH';
    }

    public static function create()
    {
        return new Card\BRM_027pH;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_027pH';
    }
}