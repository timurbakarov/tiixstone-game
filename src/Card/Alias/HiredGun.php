<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HiredGun
{
    public static function globalId() : string
    {
        return 'CFM_653';
    }

    public static function create()
    {
        return new Card\CFM_653;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_653';
    }
}