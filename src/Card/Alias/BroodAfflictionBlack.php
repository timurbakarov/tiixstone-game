<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BroodAfflictionBlack
{
    public static function globalId() : string
    {
        return 'BRMA12_6H';
    }

    public static function create()
    {
        return new Card\BRMA12_6H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA12_6H';
    }
}