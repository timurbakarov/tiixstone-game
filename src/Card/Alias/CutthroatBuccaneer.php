<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CutthroatBuccaneer
{
    public static function globalId() : string
    {
        return 'GIL_902';
    }

    public static function create()
    {
        return new Card\GIL_902;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_902';
    }
}