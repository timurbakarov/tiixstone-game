<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RockpoolHunter
{
    public static function globalId() : string
    {
        return 'UNG_073';
    }

    public static function create()
    {
        return new Card\UNG_073;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_073';
    }
}