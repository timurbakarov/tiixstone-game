<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CThunsChosen
{
    public static function globalId() : string
    {
        return 'OG_283';
    }

    public static function create()
    {
        return new Card\OG_283;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_283';
    }
}