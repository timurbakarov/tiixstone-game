<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DarkIronSpectator
{
    public static function globalId() : string
    {
        return 'BRMA02_2t';
    }

    public static function create()
    {
        return new Card\BRMA02_2t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA02_2t';
    }
}