<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SoulReaper
{
    public static function globalId() : string
    {
        return 'ICCA08_026';
    }

    public static function create()
    {
        return new Card\ICCA08_026;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA08_026';
    }
}