<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShadowVisions
{
    public static function globalId() : string
    {
        return 'UNG_029';
    }

    public static function create()
    {
        return new Card\UNG_029;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_029';
    }
}