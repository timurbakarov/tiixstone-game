<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GorillabotA3
{
    public static function globalId() : string
    {
        return 'LOE_039';
    }

    public static function create()
    {
        return new Card\LOE_039;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_039';
    }
}