<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Expedite
{
    public static function globalId() : string
    {
        return 'GILA_813';
    }

    public static function create()
    {
        return new Card\GILA_813;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_813';
    }
}