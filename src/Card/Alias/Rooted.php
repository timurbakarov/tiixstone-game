<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Rooted
{
    public static function globalId() : string
    {
        return 'EX1_178a';
    }

    public static function create()
    {
        return new Card\EX1_178a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_178a';
    }
}