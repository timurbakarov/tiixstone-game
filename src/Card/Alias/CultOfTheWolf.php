<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CultOfTheWolf
{
    public static function globalId() : string
    {
        return 'GILA_513';
    }

    public static function create()
    {
        return new Card\GILA_513;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_513';
    }
}