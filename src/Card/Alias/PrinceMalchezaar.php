<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PrinceMalchezaar
{
    public static function globalId() : string
    {
        return 'KAR_096';
    }

    public static function create()
    {
        return new Card\KAR_096;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_096';
    }
}