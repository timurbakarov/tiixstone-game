<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpreadingPlague
{
    public static function globalId() : string
    {
        return 'ICC_054';
    }

    public static function create()
    {
        return new Card\ICC_054;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_054';
    }
}