<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AmaniWarBear
{
    public static function globalId() : string
    {
        return 'TRL_550';
    }

    public static function create()
    {
        return new Card\TRL_550;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_550';
    }
}