<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Volcano
{
    public static function globalId() : string
    {
        return 'UNG_025';
    }

    public static function create()
    {
        return new Card\UNG_025;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_025';
    }
}