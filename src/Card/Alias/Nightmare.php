<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Nightmare
{
    public static function globalId() : string
    {
        return 'DREAM_05';
    }

    public static function create()
    {
        return new Card\DREAM_05;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\DREAM_05';
    }
}