<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RaptorPack
{
    public static function globalId() : string
    {
        return 'TRL_254b';
    }

    public static function create()
    {
        return new Card\TRL_254b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_254b';
    }
}