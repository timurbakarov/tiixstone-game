<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DispatchKodo
{
    public static function globalId() : string
    {
        return 'CFM_335';
    }

    public static function create()
    {
        return new Card\CFM_335;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_335';
    }
}