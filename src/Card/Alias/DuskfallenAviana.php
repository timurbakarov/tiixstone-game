<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DuskfallenAviana
{
    public static function globalId() : string
    {
        return 'GIL_800';
    }

    public static function create()
    {
        return new Card\GIL_800;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_800';
    }
}