<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BoonReaper
{
    public static function globalId() : string
    {
        return 'TRLA_807';
    }

    public static function create()
    {
        return new Card\TRLA_807;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_807';
    }
}