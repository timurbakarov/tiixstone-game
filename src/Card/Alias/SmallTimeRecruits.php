<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SmallTimeRecruits
{
    public static function globalId() : string
    {
        return 'CFM_905';
    }

    public static function create()
    {
        return new Card\CFM_905;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_905';
    }
}