<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Webweave
{
    public static function globalId() : string
    {
        return 'ICC_050';
    }

    public static function create()
    {
        return new Card\ICC_050;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_050';
    }
}