<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HighJusticeGrimstone
{
    public static function globalId() : string
    {
        return 'BRMC_96';
    }

    public static function create()
    {
        return new Card\BRMC_96;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_96';
    }
}