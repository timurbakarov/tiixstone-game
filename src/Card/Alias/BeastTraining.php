<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BeastTraining
{
    public static function globalId() : string
    {
        return 'TRLA_Hunter_06';
    }

    public static function create()
    {
        return new Card\TRLA_Hunter_06;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Hunter_06';
    }
}