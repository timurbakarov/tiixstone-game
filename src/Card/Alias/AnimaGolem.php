<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AnimaGolem
{
    public static function globalId() : string
    {
        return 'GVG_077';
    }

    public static function create()
    {
        return new Card\GVG_077;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_077';
    }
}