<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ValithriaDreamwalker
{
    public static function globalId() : string
    {
        return 'ICCA10_001';
    }

    public static function create()
    {
        return new Card\ICCA10_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA10_001';
    }
}