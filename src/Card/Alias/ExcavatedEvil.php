<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ExcavatedEvil
{
    public static function globalId() : string
    {
        return 'LOE_111';
    }

    public static function create()
    {
        return new Card\LOE_111;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_111';
    }
}