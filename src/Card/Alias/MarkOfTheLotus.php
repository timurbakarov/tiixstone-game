<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MarkOfTheLotus
{
    public static function globalId() : string
    {
        return 'CFM_614';
    }

    public static function create()
    {
        return new Card\CFM_614;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_614';
    }
}