<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BogSlosher
{
    public static function globalId() : string
    {
        return 'TRL_059';
    }

    public static function create()
    {
        return new Card\TRL_059;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_059';
    }
}