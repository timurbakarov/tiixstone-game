<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PeterWhalen
{
    public static function globalId() : string
    {
        return 'CRED_68';
    }

    public static function create()
    {
        return new Card\CRED_68;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_68';
    }
}