<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Spyglass
{
    public static function globalId() : string
    {
        return 'GILA_811';
    }

    public static function create()
    {
        return new Card\GILA_811;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_811';
    }
}