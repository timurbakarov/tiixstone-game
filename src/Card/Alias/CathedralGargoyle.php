<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CathedralGargoyle
{
    public static function globalId() : string
    {
        return 'GIL_635';
    }

    public static function create()
    {
        return new Card\GIL_635;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_635';
    }
}