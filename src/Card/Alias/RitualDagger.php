<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RitualDagger
{
    public static function globalId() : string
    {
        return 'GILA_BOSS_61t';
    }

    public static function create()
    {
        return new Card\GILA_BOSS_61t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_BOSS_61t';
    }
}