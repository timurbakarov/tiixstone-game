<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Sabotage
{
    public static function globalId() : string
    {
        return 'GVG_047';
    }

    public static function create()
    {
        return new Card\GVG_047;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_047';
    }
}