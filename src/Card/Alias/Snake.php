<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Snake
{
    public static function globalId() : string
    {
        return 'EX1_554t';
    }

    public static function create()
    {
        return new Card\EX1_554t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_554t';
    }
}