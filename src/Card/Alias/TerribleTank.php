<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TerribleTank
{
    public static function globalId() : string
    {
        return 'ICCA01_008';
    }

    public static function create()
    {
        return new Card\ICCA01_008;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA01_008';
    }
}