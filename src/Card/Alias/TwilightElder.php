<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TwilightElder
{
    public static function globalId() : string
    {
        return 'OG_286';
    }

    public static function create()
    {
        return new Card\OG_286;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_286';
    }
}