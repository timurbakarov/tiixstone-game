<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StubbornGastropod
{
    public static function globalId() : string
    {
        return 'UNG_808';
    }

    public static function create()
    {
        return new Card\UNG_808;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_808';
    }
}