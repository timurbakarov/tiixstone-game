<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Rhonin
{
    public static function globalId() : string
    {
        return 'AT_009';
    }

    public static function create()
    {
        return new Card\AT_009;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_009';
    }
}