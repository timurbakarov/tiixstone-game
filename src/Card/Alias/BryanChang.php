<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BryanChang
{
    public static function globalId() : string
    {
        return 'CRED_21';
    }

    public static function create()
    {
        return new Card\CRED_21;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_21';
    }
}