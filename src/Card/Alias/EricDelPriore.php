<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EricDelPriore
{
    public static function globalId() : string
    {
        return 'CRED_26';
    }

    public static function create()
    {
        return new Card\CRED_26;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_26';
    }
}