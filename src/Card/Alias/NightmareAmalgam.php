<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NightmareAmalgam
{
    public static function globalId() : string
    {
        return 'GIL_681';
    }

    public static function create()
    {
        return new Card\GIL_681;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_681';
    }
}