<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HowlingCommander
{
    public static function globalId() : string
    {
        return 'ICC_801';
    }

    public static function create()
    {
        return new Card\ICC_801;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_801';
    }
}