<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CobraShot
{
    public static function globalId() : string
    {
        return 'GVG_073';
    }

    public static function create()
    {
        return new Card\GVG_073;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_073';
    }
}