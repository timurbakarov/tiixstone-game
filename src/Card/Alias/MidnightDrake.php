<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MidnightDrake
{
    public static function globalId() : string
    {
        return 'OG_320';
    }

    public static function create()
    {
        return new Card\OG_320;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_320';
    }
}