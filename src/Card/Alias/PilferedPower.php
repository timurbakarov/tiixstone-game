<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PilferedPower
{
    public static function globalId() : string
    {
        return 'CFM_616';
    }

    public static function create()
    {
        return new Card\CFM_616;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_616';
    }
}