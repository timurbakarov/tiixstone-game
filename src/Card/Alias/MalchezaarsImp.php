<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MalchezaarsImp
{
    public static function globalId() : string
    {
        return 'KAR_089';
    }

    public static function create()
    {
        return new Card\KAR_089;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_089';
    }
}