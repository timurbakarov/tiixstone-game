<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LordJaraxxusHero
{
    public static function globalId() : string
    {
        return 'EX1_323h';
    }

    public static function create()
    {
        return new Card\EX1_323h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_323h';
    }
}