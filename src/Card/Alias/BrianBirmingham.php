<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BrianBirmingham
{
    public static function globalId() : string
    {
        return 'CRED_20';
    }

    public static function create()
    {
        return new Card\CRED_20;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_20';
    }
}