<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BobFitch
{
    public static function globalId() : string
    {
        return 'CRED_03';
    }

    public static function create()
    {
        return new Card\CRED_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_03';
    }
}