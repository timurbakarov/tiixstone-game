<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NagaRepellent
{
    public static function globalId() : string
    {
        return 'LOEA09_9H';
    }

    public static function create()
    {
        return new Card\LOEA09_9H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA09_9H';
    }
}