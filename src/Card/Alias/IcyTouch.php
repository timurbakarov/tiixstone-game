<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IcyTouch
{
    public static function globalId() : string
    {
        return 'ICC_833h';
    }

    public static function create()
    {
        return new Card\ICC_833h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_833h';
    }
}