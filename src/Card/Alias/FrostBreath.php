<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FrostBreath
{
    public static function globalId() : string
    {
        return 'ICCA04_008p';
    }

    public static function create()
    {
        return new Card\ICCA04_008p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA04_008p';
    }
}