<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SlitheringGuard
{
    public static function globalId() : string
    {
        return 'LOEA09_8';
    }

    public static function create()
    {
        return new Card\LOEA09_8;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA09_8';
    }
}