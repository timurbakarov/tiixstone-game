<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RallyingQuilboar
{
    public static function globalId() : string
    {
        return 'TRLA_172';
    }

    public static function create()
    {
        return new Card\TRLA_172;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_172';
    }
}