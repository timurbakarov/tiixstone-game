<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EmergencyCoolant
{
    public static function globalId() : string
    {
        return 'PART_005';
    }

    public static function create()
    {
        return new Card\PART_005;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\PART_005';
    }
}