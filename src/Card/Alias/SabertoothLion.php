<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SabertoothLion
{
    public static function globalId() : string
    {
        return 'AT_042t';
    }

    public static function create()
    {
        return new Card\AT_042t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_042t';
    }
}