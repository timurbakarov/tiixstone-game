<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PourARound
{
    public static function globalId() : string
    {
        return 'KAR_A02_10';
    }

    public static function create()
    {
        return new Card\KAR_A02_10;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_A02_10';
    }
}