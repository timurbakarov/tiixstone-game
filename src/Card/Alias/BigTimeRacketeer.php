<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BigTimeRacketeer
{
    public static function globalId() : string
    {
        return 'CFM_648';
    }

    public static function create()
    {
        return new Card\CFM_648;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_648';
    }
}