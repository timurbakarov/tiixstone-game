<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Tabbycat
{
    public static function globalId() : string
    {
        return 'CFM_315t';
    }

    public static function create()
    {
        return new Card\CFM_315t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_315t';
    }
}