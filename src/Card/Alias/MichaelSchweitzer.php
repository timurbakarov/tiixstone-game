<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MichaelSchweitzer
{
    public static function globalId() : string
    {
        return 'CRED_10';
    }

    public static function create()
    {
        return new Card\CRED_10;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_10';
    }
}