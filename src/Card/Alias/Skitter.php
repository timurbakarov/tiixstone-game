<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Skitter
{
    public static function globalId() : string
    {
        return 'NAX1_04';
    }

    public static function create()
    {
        return new Card\NAX1_04;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX1_04';
    }
}