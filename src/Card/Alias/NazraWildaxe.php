<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NazraWildaxe
{
    public static function globalId() : string
    {
        return 'KARA_13_01H';
    }

    public static function create()
    {
        return new Card\KARA_13_01H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_13_01H';
    }
}