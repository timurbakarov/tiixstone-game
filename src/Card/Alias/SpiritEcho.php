<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpiritEcho
{
    public static function globalId() : string
    {
        return 'UNG_956';
    }

    public static function create()
    {
        return new Card\UNG_956;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_956';
    }
}