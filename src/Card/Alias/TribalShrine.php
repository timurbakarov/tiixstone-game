<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TribalShrine
{
    public static function globalId() : string
    {
        return 'TRLA_107';
    }

    public static function create()
    {
        return new Card\TRLA_107;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_107';
    }
}