<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WhiteQueen
{
    public static function globalId() : string
    {
        return 'KAR_A10_09';
    }

    public static function create()
    {
        return new Card\KAR_A10_09;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_A10_09';
    }
}