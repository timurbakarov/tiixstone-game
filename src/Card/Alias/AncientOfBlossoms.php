<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AncientOfBlossoms
{
    public static function globalId() : string
    {
        return 'CFM_854';
    }

    public static function create()
    {
        return new Card\CFM_854;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_854';
    }
}