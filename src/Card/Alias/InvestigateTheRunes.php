<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class InvestigateTheRunes
{
    public static function globalId() : string
    {
        return 'LOEA04_29b';
    }

    public static function create()
    {
        return new Card\LOEA04_29b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA04_29b';
    }
}