<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RadiantElemental
{
    public static function globalId() : string
    {
        return 'UNG_034';
    }

    public static function create()
    {
        return new Card\UNG_034;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_034';
    }
}