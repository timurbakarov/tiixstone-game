<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheCrone
{
    public static function globalId() : string
    {
        return 'KARA_04_01h';
    }

    public static function create()
    {
        return new Card\KARA_04_01h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_04_01h';
    }
}