<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PactWithTheLoa
{
    public static function globalId() : string
    {
        return 'TRLA_805';
    }

    public static function create()
    {
        return new Card\TRLA_805;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_805';
    }
}