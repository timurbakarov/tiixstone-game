<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Simulacrum
{
    public static function globalId() : string
    {
        return 'ICC_823';
    }

    public static function create()
    {
        return new Card\ICC_823;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_823';
    }
}