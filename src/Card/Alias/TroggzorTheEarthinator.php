<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TroggzorTheEarthinator
{
    public static function globalId() : string
    {
        return 'GVG_118';
    }

    public static function create()
    {
        return new Card\GVG_118;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_118';
    }
}