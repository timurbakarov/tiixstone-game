<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DarkArakkoa
{
    public static function globalId() : string
    {
        return 'OG_293';
    }

    public static function create()
    {
        return new Card\OG_293;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_293';
    }
}