<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NadiaMankriksWife
{
    public static function globalId() : string
    {
        return 'TB_MnkWf01';
    }

    public static function create()
    {
        return new Card\TB_MnkWf01;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_MnkWf01';
    }
}