<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SightlessRanger
{
    public static function globalId() : string
    {
        return 'TRL_020';
    }

    public static function create()
    {
        return new Card\TRL_020;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_020';
    }
}