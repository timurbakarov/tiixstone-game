<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HealingWave
{
    public static function globalId() : string
    {
        return 'AT_048';
    }

    public static function create()
    {
        return new Card\AT_048;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_048';
    }
}