<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SerratedTooth
{
    public static function globalId() : string
    {
        return 'TRL_074';
    }

    public static function create()
    {
        return new Card\TRL_074;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_074';
    }
}