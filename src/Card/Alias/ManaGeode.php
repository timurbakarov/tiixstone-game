<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ManaGeode
{
    public static function globalId() : string
    {
        return 'CFM_606';
    }

    public static function create()
    {
        return new Card\CFM_606;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_606';
    }
}