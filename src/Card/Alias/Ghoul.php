<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Ghoul
{
    public static function globalId() : string
    {
        return 'ICCA01_004t';
    }

    public static function create()
    {
        return new Card\ICCA01_004t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA01_004t';
    }
}