<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RavenousFamiliar
{
    public static function globalId() : string
    {
        return 'TRLA_181';
    }

    public static function create()
    {
        return new Card\TRLA_181;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_181';
    }
}