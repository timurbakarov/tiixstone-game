<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ForbiddenFlame
{
    public static function globalId() : string
    {
        return 'OG_086';
    }

    public static function create()
    {
        return new Card\OG_086;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_086';
    }
}