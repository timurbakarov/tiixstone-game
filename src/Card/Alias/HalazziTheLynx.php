<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HalazziTheLynx
{
    public static function globalId() : string
    {
        return 'TRL_900';
    }

    public static function create()
    {
        return new Card\TRL_900;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_900';
    }
}