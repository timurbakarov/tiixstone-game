<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CorruptedBlood
{
    public static function globalId() : string
    {
        return 'TRL_541t';
    }

    public static function create()
    {
        return new Card\TRL_541t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_541t';
    }
}