<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Hellfire
{
    public static function globalId() : string
    {
        return 'CS2_062';
    }

    public static function create()
    {
        return new Card\CS2_062;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_062';
    }
}