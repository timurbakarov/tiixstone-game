<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BigGameHunter
{
    public static function globalId() : string
    {
        return 'EX1_005';
    }

    public static function create()
    {
        return new Card\EX1_005;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_005';
    }
}