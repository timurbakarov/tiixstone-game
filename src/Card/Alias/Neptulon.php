<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Neptulon
{
    public static function globalId() : string
    {
        return 'GVG_042';
    }

    public static function create()
    {
        return new Card\GVG_042;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_042';
    }
}