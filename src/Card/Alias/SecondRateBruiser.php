<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SecondRateBruiser
{
    public static function globalId() : string
    {
        return 'CFM_652';
    }

    public static function create()
    {
        return new Card\CFM_652;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_652';
    }
}