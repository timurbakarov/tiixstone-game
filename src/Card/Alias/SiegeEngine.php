<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SiegeEngine
{
    public static function globalId() : string
    {
        return 'GVG_086';
    }

    public static function create()
    {
        return new Card\GVG_086;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_086';
    }
}