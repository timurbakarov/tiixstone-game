<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpiderFangs
{
    public static function globalId() : string
    {
        return 'ICC_832pb';
    }

    public static function create()
    {
        return new Card\ICC_832pb;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_832pb';
    }
}