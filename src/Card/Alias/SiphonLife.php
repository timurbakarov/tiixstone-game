<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SiphonLife
{
    public static function globalId() : string
    {
        return 'ICC_831p';
    }

    public static function create()
    {
        return new Card\ICC_831p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_831p';
    }
}