<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArgentWatchman
{
    public static function globalId() : string
    {
        return 'AT_109';
    }

    public static function create()
    {
        return new Card\AT_109;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_109';
    }
}