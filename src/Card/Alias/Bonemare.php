<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Bonemare
{
    public static function globalId() : string
    {
        return 'ICC_705';
    }

    public static function create()
    {
        return new Card\ICC_705;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_705';
    }
}