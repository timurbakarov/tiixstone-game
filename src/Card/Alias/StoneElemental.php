<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StoneElemental
{
    public static function globalId() : string
    {
        return 'UNG_211aa';
    }

    public static function create()
    {
        return new Card\UNG_211aa;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_211aa';
    }
}