<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SabertoothTiger
{
    public static function globalId() : string
    {
        return 'OG_044c';
    }

    public static function create()
    {
        return new Card\OG_044c;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_044c';
    }
}