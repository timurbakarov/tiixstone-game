<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HallowedWater
{
    public static function globalId() : string
    {
        return 'GILA_850b';
    }

    public static function create()
    {
        return new Card\GILA_850b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_850b';
    }
}