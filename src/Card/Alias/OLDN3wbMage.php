<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OLDN3wbMage
{
    public static function globalId() : string
    {
        return 'TBST_002';
    }

    public static function create()
    {
        return new Card\TBST_002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TBST_002';
    }
}