<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PlatedBeetle
{
    public static function globalId() : string
    {
        return 'LOOT_413';
    }

    public static function create()
    {
        return new Card\LOOT_413;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOOT_413';
    }
}