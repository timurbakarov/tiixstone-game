<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AzalinaSoulthief
{
    public static function globalId() : string
    {
        return 'GIL_198';
    }

    public static function create()
    {
        return new Card\GIL_198;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_198';
    }
}