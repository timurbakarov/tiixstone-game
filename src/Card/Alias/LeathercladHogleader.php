<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LeathercladHogleader
{
    public static function globalId() : string
    {
        return 'CFM_810';
    }

    public static function create()
    {
        return new Card\CFM_810;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_810';
    }
}