<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Saboteur
{
    public static function globalId() : string
    {
        return 'AT_086';
    }

    public static function create()
    {
        return new Card\AT_086;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_086';
    }
}