<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ChitteringTunneler
{
    public static function globalId() : string
    {
        return 'UNG_835';
    }

    public static function create()
    {
        return new Card\UNG_835;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_835';
    }
}