<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Toxmonger
{
    public static function globalId() : string
    {
        return 'GIL_607';
    }

    public static function create()
    {
        return new Card\GIL_607;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_607';
    }
}