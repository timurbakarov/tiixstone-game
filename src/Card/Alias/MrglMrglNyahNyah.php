<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MrglMrglNyahNyah
{
    public static function globalId() : string
    {
        return 'LOEA10_5';
    }

    public static function create()
    {
        return new Card\LOEA10_5;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA10_5';
    }
}