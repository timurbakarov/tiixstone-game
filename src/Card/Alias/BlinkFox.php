<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BlinkFox
{
    public static function globalId() : string
    {
        return 'GIL_827';
    }

    public static function create()
    {
        return new Card\GIL_827;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_827';
    }
}