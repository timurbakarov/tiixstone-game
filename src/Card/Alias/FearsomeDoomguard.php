<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FearsomeDoomguard
{
    public static function globalId() : string
    {
        return 'AT_020';
    }

    public static function create()
    {
        return new Card\AT_020;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_020';
    }
}