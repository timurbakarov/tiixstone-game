<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Avalanche
{
    public static function globalId() : string
    {
        return 'ICC_078';
    }

    public static function create()
    {
        return new Card\ICC_078;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_078';
    }
}