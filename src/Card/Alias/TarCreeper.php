<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TarCreeper
{
    public static function globalId() : string
    {
        return 'UNG_928';
    }

    public static function create()
    {
        return new Card\UNG_928;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_928';
    }
}