<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MirrorEntity
{
    public static function globalId() : string
    {
        return 'EX1_294';
    }

    public static function create()
    {
        return new Card\EX1_294;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_294';
    }
}