<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HuntersInsight
{
    public static function globalId() : string
    {
        return 'GILA_503';
    }

    public static function create()
    {
        return new Card\GILA_503;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_503';
    }
}