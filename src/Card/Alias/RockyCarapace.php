<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RockyCarapace
{
    public static function globalId() : string
    {
        return 'UNG_999t4';
    }

    public static function create()
    {
        return new Card\UNG_999t4;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_999t4';
    }
}