<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GentlemanlyReequipEffectDummy
{
    public static function globalId() : string
    {
        return 'GILA_825d';
    }

    public static function create()
    {
        return new Card\GILA_825d;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_825d';
    }
}