<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HomingChicken
{
    public static function globalId() : string
    {
        return 'Mekka1';
    }

    public static function create()
    {
        return new Card\Mekka1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\Mekka1';
    }
}