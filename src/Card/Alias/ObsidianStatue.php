<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ObsidianStatue
{
    public static function globalId() : string
    {
        return 'ICC_214';
    }

    public static function create()
    {
        return new Card\ICC_214;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_214';
    }
}