<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ZandalariStriker
{
    public static function globalId() : string
    {
        return 'TRLA_124';
    }

    public static function create()
    {
        return new Card\TRLA_124;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_124';
    }
}