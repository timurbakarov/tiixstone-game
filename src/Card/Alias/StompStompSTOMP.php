<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StompStompSTOMP
{
    public static function globalId() : string
    {
        return 'GILA_497';
    }

    public static function create()
    {
        return new Card\GILA_497;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_497';
    }
}