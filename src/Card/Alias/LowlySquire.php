<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LowlySquire
{
    public static function globalId() : string
    {
        return 'AT_082';
    }

    public static function create()
    {
        return new Card\AT_082;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_082';
    }
}