<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ParagonOfLight
{
    public static function globalId() : string
    {
        return 'GIL_685';
    }

    public static function create()
    {
        return new Card\GIL_685;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_685';
    }
}