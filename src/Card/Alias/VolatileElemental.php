<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VolatileElemental
{
    public static function globalId() : string
    {
        return 'UNG_818';
    }

    public static function create()
    {
        return new Card\UNG_818;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_818';
    }
}