<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class InfestedTauren
{
    public static function globalId() : string
    {
        return 'OG_249';
    }

    public static function create()
    {
        return new Card\OG_249;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_249';
    }
}