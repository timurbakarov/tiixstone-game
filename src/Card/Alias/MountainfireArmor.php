<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MountainfireArmor
{
    public static function globalId() : string
    {
        return 'ICC_062';
    }

    public static function create()
    {
        return new Card\ICC_062;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_062';
    }
}