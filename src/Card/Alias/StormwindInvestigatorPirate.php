<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StormwindInvestigatorPirate
{
    public static function globalId() : string
    {
        return 'TB_HeadlessHorseman_H2c';
    }

    public static function create()
    {
        return new Card\TB_HeadlessHorseman_H2c;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_HeadlessHorseman_H2c';
    }
}