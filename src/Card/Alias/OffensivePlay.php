<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OffensivePlay
{
    public static function globalId() : string
    {
        return 'TB_Superfriends001';
    }

    public static function create()
    {
        return new Card\TB_Superfriends001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_Superfriends001';
    }
}