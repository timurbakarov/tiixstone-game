<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ObsidianDestroyer
{
    public static function globalId() : string
    {
        return 'LOE_009';
    }

    public static function create()
    {
        return new Card\LOE_009;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_009';
    }
}