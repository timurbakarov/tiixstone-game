<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GiantWasp
{
    public static function globalId() : string
    {
        return 'UNG_814';
    }

    public static function create()
    {
        return new Card\UNG_814;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_814';
    }
}