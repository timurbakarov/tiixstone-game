<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Gnomeferatu
{
    public static function globalId() : string
    {
        return 'ICC_407';
    }

    public static function create()
    {
        return new Card\ICC_407;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_407';
    }
}