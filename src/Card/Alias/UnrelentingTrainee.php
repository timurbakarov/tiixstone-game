<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class UnrelentingTrainee
{
    public static function globalId() : string
    {
        return 'NAX8_03';
    }

    public static function create()
    {
        return new Card\NAX8_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX8_03';
    }
}