<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PossessedLackey
{
    public static function globalId() : string
    {
        return 'LOOT_306';
    }

    public static function create()
    {
        return new Card\LOOT_306;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOOT_306';
    }
}