<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GrimestreetPawnbroker
{
    public static function globalId() : string
    {
        return 'CFM_755';
    }

    public static function create()
    {
        return new Card\CFM_755;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_755';
    }
}