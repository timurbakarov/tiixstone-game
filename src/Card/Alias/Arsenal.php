<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Arsenal
{
    public static function globalId() : string
    {
        return 'GILA_597';
    }

    public static function create()
    {
        return new Card\GILA_597;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_597';
    }
}