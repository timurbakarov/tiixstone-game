<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CharlèneLeScanff
{
    public static function globalId() : string
    {
        return 'CRED_61';
    }

    public static function create()
    {
        return new Card\CRED_61;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_61';
    }
}