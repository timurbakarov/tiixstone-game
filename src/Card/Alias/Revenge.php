<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Revenge
{
    public static function globalId() : string
    {
        return 'BRM_015';
    }

    public static function create()
    {
        return new Card\BRM_015;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_015';
    }
}