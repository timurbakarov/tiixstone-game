<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BreathOfSindragosa
{
    public static function globalId() : string
    {
        return 'ICC_836';
    }

    public static function create()
    {
        return new Card\ICC_836;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_836';
    }
}