<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Supercharge
{
    public static function globalId() : string
    {
        return 'NAX13_03';
    }

    public static function create()
    {
        return new Card\NAX13_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX13_03';
    }
}