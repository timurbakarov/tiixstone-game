<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AaronGutierrez
{
    public static function globalId() : string
    {
        return 'CRED_79';
    }

    public static function create()
    {
        return new Card\CRED_79;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_79';
    }
}