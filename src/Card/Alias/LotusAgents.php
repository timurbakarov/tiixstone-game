<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LotusAgents
{
    public static function globalId() : string
    {
        return 'CFM_852';
    }

    public static function create()
    {
        return new Card\CFM_852;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_852';
    }
}