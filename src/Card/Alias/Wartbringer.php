<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Wartbringer
{
    public static function globalId() : string
    {
        return 'TRL_522';
    }

    public static function create()
    {
        return new Card\TRL_522;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_522';
    }
}