<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BlackwingTechnician
{
    public static function globalId() : string
    {
        return 'BRM_033';
    }

    public static function create()
    {
        return new Card\BRM_033;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_033';
    }
}