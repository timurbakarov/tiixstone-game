<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CallInTheFinishers
{
    public static function globalId() : string
    {
        return 'CFM_310';
    }

    public static function create()
    {
        return new Card\CFM_310;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_310';
    }
}