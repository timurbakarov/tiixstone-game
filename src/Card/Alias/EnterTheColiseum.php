<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EnterTheColiseum
{
    public static function globalId() : string
    {
        return 'AT_078';
    }

    public static function create()
    {
        return new Card\AT_078;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_078';
    }
}