<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DeathAndDecay
{
    public static function globalId() : string
    {
        return 'ICC_314t8';
    }

    public static function create()
    {
        return new Card\ICC_314t8;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_314t8';
    }
}