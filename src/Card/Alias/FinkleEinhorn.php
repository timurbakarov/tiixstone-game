<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FinkleEinhorn
{
    public static function globalId() : string
    {
        return 'EX1_finkle';
    }

    public static function create()
    {
        return new Card\EX1_finkle;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_finkle';
    }
}