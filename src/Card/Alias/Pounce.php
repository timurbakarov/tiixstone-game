<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Pounce
{
    public static function globalId() : string
    {
        return 'TRL_243';
    }

    public static function create()
    {
        return new Card\TRL_243;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_243';
    }
}