<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MadScience
{
    public static function globalId() : string
    {
        return 'ICCA07_002p';
    }

    public static function create()
    {
        return new Card\ICCA07_002p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA07_002p';
    }
}