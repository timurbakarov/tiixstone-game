<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Warbot
{
    public static function globalId() : string
    {
        return 'GVG_051';
    }

    public static function create()
    {
        return new Card\GVG_051;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_051';
    }
}