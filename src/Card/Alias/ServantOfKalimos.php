<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ServantOfKalimos
{
    public static function globalId() : string
    {
        return 'UNG_816';
    }

    public static function create()
    {
        return new Card\UNG_816;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_816';
    }
}