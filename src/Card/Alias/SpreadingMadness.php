<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpreadingMadness
{
    public static function globalId() : string
    {
        return 'OG_116';
    }

    public static function create()
    {
        return new Card\OG_116;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_116';
    }
}