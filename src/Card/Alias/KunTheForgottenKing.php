<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KunTheForgottenKing
{
    public static function globalId() : string
    {
        return 'CFM_308';
    }

    public static function create()
    {
        return new Card\CFM_308;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_308';
    }
}