<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AmgamRager
{
    public static function globalId() : string
    {
        return 'OG_248';
    }

    public static function create()
    {
        return new Card\OG_248;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_248';
    }
}