<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Caltrops
{
    public static function globalId() : string
    {
        return 'GILA_814';
    }

    public static function create()
    {
        return new Card\GILA_814;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_814';
    }
}