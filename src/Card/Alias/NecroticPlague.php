<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NecroticPlague
{
    public static function globalId() : string
    {
        return 'ICCA08_023';
    }

    public static function create()
    {
        return new Card\ICCA08_023;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA08_023';
    }
}