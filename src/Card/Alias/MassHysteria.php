<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MassHysteria
{
    public static function globalId() : string
    {
        return 'TRL_258';
    }

    public static function create()
    {
        return new Card\TRL_258;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_258';
    }
}