<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PirateCostume
{
    public static function globalId() : string
    {
        return 'TB_HeadlessHorseman_s001c';
    }

    public static function create()
    {
        return new Card\TB_HeadlessHorseman_s001c;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_HeadlessHorseman_s001c';
    }
}