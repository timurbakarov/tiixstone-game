<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VoodooDoctor
{
    public static function globalId() : string
    {
        return 'EX1_011';
    }

    public static function create()
    {
        return new Card\EX1_011;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_011';
    }
}