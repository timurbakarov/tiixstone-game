<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FandralStaghelm
{
    public static function globalId() : string
    {
        return 'OG_044';
    }

    public static function create()
    {
        return new Card\OG_044;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_044';
    }
}