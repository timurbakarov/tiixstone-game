<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PterrordaxHatchling
{
    public static function globalId() : string
    {
        return 'UNG_001';
    }

    public static function create()
    {
        return new Card\UNG_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_001';
    }
}