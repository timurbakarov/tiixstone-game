<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Hunter
{
    public static function globalId() : string
    {
        return 'FB_LK_Hunter_copy';
    }

    public static function create()
    {
        return new Card\FB_LK_Hunter_copy;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK_Hunter_copy';
    }
}