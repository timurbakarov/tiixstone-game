<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Boneraptor
{
    public static function globalId() : string
    {
        return 'LOEA15_3';
    }

    public static function create()
    {
        return new Card\LOEA15_3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA15_3';
    }
}