<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Convert
{
    public static function globalId() : string
    {
        return 'AT_015';
    }

    public static function create()
    {
        return new Card\AT_015;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_015';
    }
}