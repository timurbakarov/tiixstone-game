<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SabertoothPanther
{
    public static function globalId() : string
    {
        return 'AT_042t2';
    }

    public static function create()
    {
        return new Card\AT_042t2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_042t2';
    }
}