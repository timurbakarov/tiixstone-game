<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DragonsFree
{
    public static function globalId() : string
    {
        return 'KARA_07_08';
    }

    public static function create()
    {
        return new Card\KARA_07_08;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_07_08';
    }
}