<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Springpaw
{
    public static function globalId() : string
    {
        return 'TRL_348';
    }

    public static function create()
    {
        return new Card\TRL_348;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_348';
    }
}