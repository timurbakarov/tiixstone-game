<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VilespineSlayer
{
    public static function globalId() : string
    {
        return 'UNG_064';
    }

    public static function create()
    {
        return new Card\UNG_064;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_064';
    }
}