<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DaUndatakah
{
    public static function globalId() : string
    {
        return 'TRL_537';
    }

    public static function create()
    {
        return new Card\TRL_537;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_537';
    }
}