<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MassiveRuneblade
{
    public static function globalId() : string
    {
        return 'TB_KTRAF_08w';
    }

    public static function create()
    {
        return new Card\TB_KTRAF_08w;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_KTRAF_08w';
    }
}