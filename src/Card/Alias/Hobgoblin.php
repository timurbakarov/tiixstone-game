<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Hobgoblin
{
    public static function globalId() : string
    {
        return 'GVG_104';
    }

    public static function create()
    {
        return new Card\GVG_104;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_104';
    }
}