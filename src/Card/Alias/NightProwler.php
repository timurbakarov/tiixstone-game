<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NightProwler
{
    public static function globalId() : string
    {
        return 'GIL_624';
    }

    public static function create()
    {
        return new Card\GIL_624;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_624';
    }
}