<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ChromaticDrake
{
    public static function globalId() : string
    {
        return 'BRMA10_5H';
    }

    public static function create()
    {
        return new Card\BRMA10_5H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA10_5H';
    }
}