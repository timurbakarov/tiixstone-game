<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MayorNoggenfogger
{
    public static function globalId() : string
    {
        return 'CFM_670';
    }

    public static function create()
    {
        return new Card\CFM_670;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_670';
    }
}