<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GralTheShark
{
    public static function globalId() : string
    {
        return 'TRL_409';
    }

    public static function create()
    {
        return new Card\TRL_409;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_409';
    }
}