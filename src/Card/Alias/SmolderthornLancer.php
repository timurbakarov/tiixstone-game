<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SmolderthornLancer
{
    public static function globalId() : string
    {
        return 'TRL_326';
    }

    public static function create()
    {
        return new Card\TRL_326;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_326';
    }
}