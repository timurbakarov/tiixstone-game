<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VulgarHomunculus
{
    public static function globalId() : string
    {
        return 'LOOT_013';
    }

    public static function create()
    {
        return new Card\LOOT_013;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOOT_013';
    }
}