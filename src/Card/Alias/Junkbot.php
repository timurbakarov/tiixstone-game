<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Junkbot
{
    public static function globalId() : string
    {
        return 'GVG_106';
    }

    public static function create()
    {
        return new Card\GVG_106;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_106';
    }
}