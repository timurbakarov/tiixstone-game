<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EverlastingDevotion
{
    public static function globalId() : string
    {
        return 'TRLA_804';
    }

    public static function create()
    {
        return new Card\TRLA_804;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_804';
    }
}