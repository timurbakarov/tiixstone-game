<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LivingBomb
{
    public static function globalId() : string
    {
        return 'BRMC_100';
    }

    public static function create()
    {
        return new Card\BRMC_100;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_100';
    }
}