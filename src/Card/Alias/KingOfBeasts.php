<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KingOfBeasts
{
    public static function globalId() : string
    {
        return 'GVG_046';
    }

    public static function create()
    {
        return new Card\GVG_046;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_046';
    }
}