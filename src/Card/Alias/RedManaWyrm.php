<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RedManaWyrm
{
    public static function globalId() : string
    {
        return 'CFM_060';
    }

    public static function create()
    {
        return new Card\CFM_060;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_060';
    }
}