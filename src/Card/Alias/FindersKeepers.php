<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FindersKeepers
{
    public static function globalId() : string
    {
        return 'CFM_313';
    }

    public static function create()
    {
        return new Card\CFM_313;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_313';
    }
}