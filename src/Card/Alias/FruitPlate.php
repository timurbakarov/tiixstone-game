<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FruitPlate
{
    public static function globalId() : string
    {
        return 'TB_MammothParty_s101a';
    }

    public static function create()
    {
        return new Card\TB_MammothParty_s101a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_MammothParty_s101a';
    }
}