<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FrostGiant
{
    public static function globalId() : string
    {
        return 'AT_120';
    }

    public static function create()
    {
        return new Card\AT_120;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_120';
    }
}