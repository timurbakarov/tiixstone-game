<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DeviateBanana
{
    public static function globalId() : string
    {
        return 'TB_007';
    }

    public static function create()
    {
        return new Card\TB_007;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_007';
    }
}