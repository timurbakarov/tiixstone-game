<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ThingFromBelow
{
    public static function globalId() : string
    {
        return 'OG_028';
    }

    public static function create()
    {
        return new Card\OG_028;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_028';
    }
}