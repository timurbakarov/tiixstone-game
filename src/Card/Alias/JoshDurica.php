<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JoshDurica
{
    public static function globalId() : string
    {
        return 'CRED_83';
    }

    public static function create()
    {
        return new Card\CRED_83;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_83';
    }
}