<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Shrinkmeister
{
    public static function globalId() : string
    {
        return 'GVG_011';
    }

    public static function create()
    {
        return new Card\GVG_011;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_011';
    }
}