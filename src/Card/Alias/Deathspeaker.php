<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Deathspeaker
{
    public static function globalId() : string
    {
        return 'ICC_467';
    }

    public static function create()
    {
        return new Card\ICC_467;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_467';
    }
}