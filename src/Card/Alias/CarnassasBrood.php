<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CarnassasBrood
{
    public static function globalId() : string
    {
        return 'UNG_920t2';
    }

    public static function create()
    {
        return new Card\UNG_920t2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_920t2';
    }
}