<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AuchenaiPhantasm
{
    public static function globalId() : string
    {
        return 'TRL_501';
    }

    public static function create()
    {
        return new Card\TRL_501;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_501';
    }
}