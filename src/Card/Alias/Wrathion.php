<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Wrathion
{
    public static function globalId() : string
    {
        return 'CFM_806';
    }

    public static function create()
    {
        return new Card\CFM_806;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_806';
    }
}