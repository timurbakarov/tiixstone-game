<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ReversingSwitch
{
    public static function globalId() : string
    {
        return 'PART_006';
    }

    public static function create()
    {
        return new Card\PART_006;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\PART_006';
    }
}