<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Companions
{
    public static function globalId() : string
    {
        return 'GILA_495';
    }

    public static function create()
    {
        return new Card\GILA_495;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_495';
    }
}