<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GilneanRoyalGuard
{
    public static function globalId() : string
    {
        return 'GIL_202t';
    }

    public static function create()
    {
        return new Card\GIL_202t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_202t';
    }
}