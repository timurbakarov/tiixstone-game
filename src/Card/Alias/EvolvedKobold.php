<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EvolvedKobold
{
    public static function globalId() : string
    {
        return 'OG_082';
    }

    public static function create()
    {
        return new Card\OG_082;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_082';
    }
}