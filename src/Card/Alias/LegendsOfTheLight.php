<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LegendsOfTheLight
{
    public static function globalId() : string
    {
        return 'TRLA_Priest_10';
    }

    public static function create()
    {
        return new Card\TRLA_Priest_10;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Priest_10';
    }
}