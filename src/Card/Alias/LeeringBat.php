<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LeeringBat
{
    public static function globalId() : string
    {
        return 'TRLA_180';
    }

    public static function create()
    {
        return new Card\TRLA_180;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_180';
    }
}