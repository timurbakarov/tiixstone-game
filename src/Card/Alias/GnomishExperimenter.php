<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GnomishExperimenter
{
    public static function globalId() : string
    {
        return 'GVG_092';
    }

    public static function create()
    {
        return new Card\GVG_092;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_092';
    }
}