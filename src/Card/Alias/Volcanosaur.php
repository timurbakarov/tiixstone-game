<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Volcanosaur
{
    public static function globalId() : string
    {
        return 'UNG_002';
    }

    public static function create()
    {
        return new Card\UNG_002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_002';
    }
}