<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MojomasterZihi
{
    public static function globalId() : string
    {
        return 'TRL_564';
    }

    public static function create()
    {
        return new Card\TRL_564;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_564';
    }
}