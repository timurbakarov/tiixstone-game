<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Emeriss
{
    public static function globalId() : string
    {
        return 'GIL_128';
    }

    public static function create()
    {
        return new Card\GIL_128;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_128';
    }
}