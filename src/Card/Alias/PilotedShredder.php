<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PilotedShredder
{
    public static function globalId() : string
    {
        return 'GVG_096';
    }

    public static function create()
    {
        return new Card\GVG_096;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_096';
    }
}