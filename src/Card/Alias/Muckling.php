<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Muckling
{
    public static function globalId() : string
    {
        return 'GIL_682t';
    }

    public static function create()
    {
        return new Card\GIL_682t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_682t';
    }
}