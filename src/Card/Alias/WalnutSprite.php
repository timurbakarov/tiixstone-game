<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WalnutSprite
{
    public static function globalId() : string
    {
        return 'GIL_680';
    }

    public static function create()
    {
        return new Card\GIL_680;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_680';
    }
}