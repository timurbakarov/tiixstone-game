<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DrakkisathsCommand
{
    public static function globalId() : string
    {
        return 'BRMA08_3';
    }

    public static function create()
    {
        return new Card\BRMA08_3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA08_3';
    }
}