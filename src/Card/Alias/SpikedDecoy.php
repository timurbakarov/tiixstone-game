<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpikedDecoy
{
    public static function globalId() : string
    {
        return 'LOEA07_24';
    }

    public static function create()
    {
        return new Card\LOEA07_24;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA07_24';
    }
}