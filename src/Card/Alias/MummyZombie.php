<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MummyZombie
{
    public static function globalId() : string
    {
        return 'LOEA16_5t';
    }

    public static function create()
    {
        return new Card\LOEA16_5t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA16_5t';
    }
}