<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Thaddius
{
    public static function globalId() : string
    {
        return 'FP1_014t';
    }

    public static function create()
    {
        return new Card\FP1_014t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FP1_014t';
    }
}