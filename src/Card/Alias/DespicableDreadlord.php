<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DespicableDreadlord
{
    public static function globalId() : string
    {
        return 'ICC_075';
    }

    public static function create()
    {
        return new Card\ICC_075;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_075';
    }
}