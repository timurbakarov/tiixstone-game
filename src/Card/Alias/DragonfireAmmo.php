<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DragonfireAmmo
{
    public static function globalId() : string
    {
        return 'GILA_605';
    }

    public static function create()
    {
        return new Card\GILA_605;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_605';
    }
}