<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DerekDupras
{
    public static function globalId() : string
    {
        return 'CRED_80';
    }

    public static function create()
    {
        return new Card\CRED_80;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_80';
    }
}