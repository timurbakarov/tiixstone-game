<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IceCreamPeddler
{
    public static function globalId() : string
    {
        return 'TRL_533';
    }

    public static function create()
    {
        return new Card\TRL_533;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_533';
    }
}