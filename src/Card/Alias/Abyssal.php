<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Abyssal
{
    public static function globalId() : string
    {
        return 'KARA_00_02a';
    }

    public static function create()
    {
        return new Card\KARA_00_02a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_00_02a';
    }
}