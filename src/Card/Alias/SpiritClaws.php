<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpiritClaws
{
    public static function globalId() : string
    {
        return 'KAR_063';
    }

    public static function create()
    {
        return new Card\KAR_063;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_063';
    }
}