<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SojinHwang
{
    public static function globalId() : string
    {
        return 'CRED_81';
    }

    public static function create()
    {
        return new Card\CRED_81;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_81';
    }
}