<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MasterJouster
{
    public static function globalId() : string
    {
        return 'AT_112';
    }

    public static function create()
    {
        return new Card\AT_112;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_112';
    }
}