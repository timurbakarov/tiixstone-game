<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IceRager
{
    public static function globalId() : string
    {
        return 'AT_092';
    }

    public static function create()
    {
        return new Card\AT_092;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_092';
    }
}