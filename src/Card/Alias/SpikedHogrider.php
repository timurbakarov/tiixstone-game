<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpikedHogrider
{
    public static function globalId() : string
    {
        return 'CFM_688';
    }

    public static function create()
    {
        return new Card\CFM_688;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_688';
    }
}