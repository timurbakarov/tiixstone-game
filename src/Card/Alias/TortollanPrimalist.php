<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TortollanPrimalist
{
    public static function globalId() : string
    {
        return 'UNG_088';
    }

    public static function create()
    {
        return new Card\UNG_088;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_088';
    }
}