<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Decay
{
    public static function globalId() : string
    {
        return 'ICC_047b';
    }

    public static function create()
    {
        return new Card\ICC_047b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_047b';
    }
}