<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HolySmite
{
    public static function globalId() : string
    {
        return 'CS1_130';
    }

    public static function create()
    {
        return new Card\CS1_130;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS1_130';
    }
}