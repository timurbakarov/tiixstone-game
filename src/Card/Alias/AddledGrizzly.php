<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AddledGrizzly
{
    public static function globalId() : string
    {
        return 'OG_313';
    }

    public static function create()
    {
        return new Card\OG_313;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_313';
    }
}