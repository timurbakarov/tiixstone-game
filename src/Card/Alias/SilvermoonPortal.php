<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SilvermoonPortal
{
    public static function globalId() : string
    {
        return 'KAR_077';
    }

    public static function create()
    {
        return new Card\KAR_077;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_077';
    }
}