<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HeraldVolazj
{
    public static function globalId() : string
    {
        return 'OG_316';
    }

    public static function create()
    {
        return new Card\OG_316;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_316';
    }
}