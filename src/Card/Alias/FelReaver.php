<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FelReaver
{
    public static function globalId() : string
    {
        return 'GVG_016';
    }

    public static function create()
    {
        return new Card\GVG_016;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_016';
    }
}