<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SunRaiderPhaerix
{
    public static function globalId() : string
    {
        return 'LOEA16_19H';
    }

    public static function create()
    {
        return new Card\LOEA16_19H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA16_19H';
    }
}