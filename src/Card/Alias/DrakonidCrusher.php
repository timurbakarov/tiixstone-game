<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DrakonidCrusher
{
    public static function globalId() : string
    {
        return 'BRM_024';
    }

    public static function create()
    {
        return new Card\BRM_024;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_024';
    }
}