<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HolyBook
{
    public static function globalId() : string
    {
        return 'GILA_804';
    }

    public static function create()
    {
        return new Card\GILA_804;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_804';
    }
}