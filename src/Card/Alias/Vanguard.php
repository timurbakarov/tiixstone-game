<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Vanguard
{
    public static function globalId() : string
    {
        return 'GILA_Darius_05';
    }

    public static function create()
    {
        return new Card\GILA_Darius_05;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_Darius_05';
    }
}