<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DragonRoar
{
    public static function globalId() : string
    {
        return 'TRL_362';
    }

    public static function create()
    {
        return new Card\TRL_362;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_362';
    }
}