<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EndlessHunger
{
    public static function globalId() : string
    {
        return 'LOEA09_3H';
    }

    public static function create()
    {
        return new Card\LOEA09_3H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA09_3H';
    }
}