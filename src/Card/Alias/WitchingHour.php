<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WitchingHour
{
    public static function globalId() : string
    {
        return 'GIL_571';
    }

    public static function create()
    {
        return new Card\GIL_571;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_571';
    }
}