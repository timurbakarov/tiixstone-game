<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShipsCannon
{
    public static function globalId() : string
    {
        return 'GVG_075';
    }

    public static function create()
    {
        return new Card\GVG_075;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_075';
    }
}