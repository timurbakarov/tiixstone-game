<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StoneclawTotem
{
    public static function globalId() : string
    {
        return 'CS2_051';
    }

    public static function create()
    {
        return new Card\CS2_051;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_051';
    }
}