<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Chogall
{
    public static function globalId() : string
    {
        return 'OG_121';
    }

    public static function create()
    {
        return new Card\OG_121;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_121';
    }
}