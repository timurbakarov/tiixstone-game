<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BrightEyedScout
{
    public static function globalId() : string
    {
        return 'UNG_113';
    }

    public static function create()
    {
        return new Card\UNG_113;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_113';
    }
}