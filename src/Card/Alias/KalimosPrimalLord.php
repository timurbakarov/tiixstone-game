<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KalimosPrimalLord
{
    public static function globalId() : string
    {
        return 'UNG_211';
    }

    public static function create()
    {
        return new Card\UNG_211;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_211';
    }
}