<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShadoPanRider
{
    public static function globalId() : string
    {
        return 'AT_028';
    }

    public static function create()
    {
        return new Card\AT_028;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_028';
    }
}