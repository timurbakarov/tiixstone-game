<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OpenTheGates
{
    public static function globalId() : string
    {
        return 'BRMC_83';
    }

    public static function create()
    {
        return new Card\BRMC_83;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_83';
    }
}