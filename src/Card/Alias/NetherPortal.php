<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NetherPortal
{
    public static function globalId() : string
    {
        return 'UNG_829t2';
    }

    public static function create()
    {
        return new Card\UNG_829t2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_829t2';
    }
}