<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DarkConviction
{
    public static function globalId() : string
    {
        return 'ICC_039';
    }

    public static function create()
    {
        return new Card\ICC_039;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_039';
    }
}