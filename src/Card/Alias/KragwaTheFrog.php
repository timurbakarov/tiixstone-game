<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KragwaTheFrog
{
    public static function globalId() : string
    {
        return 'TRL_345';
    }

    public static function create()
    {
        return new Card\TRL_345;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_345';
    }
}