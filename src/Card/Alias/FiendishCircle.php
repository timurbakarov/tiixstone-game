<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FiendishCircle
{
    public static function globalId() : string
    {
        return 'GIL_191';
    }

    public static function create()
    {
        return new Card\GIL_191;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_191';
    }
}