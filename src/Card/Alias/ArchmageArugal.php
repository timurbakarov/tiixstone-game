<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArchmageArugal
{
    public static function globalId() : string
    {
        return 'GIL_691';
    }

    public static function create()
    {
        return new Card\GIL_691;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_691';
    }
}