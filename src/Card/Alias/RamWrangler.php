<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RamWrangler
{
    public static function globalId() : string
    {
        return 'AT_010';
    }

    public static function create()
    {
        return new Card\AT_010;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_010';
    }
}