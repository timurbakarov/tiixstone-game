<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JerryMascho
{
    public static function globalId() : string
    {
        return 'CRED_32';
    }

    public static function create()
    {
        return new Card\CRED_32;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_32';
    }
}