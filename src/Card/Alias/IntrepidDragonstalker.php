<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IntrepidDragonstalker
{
    public static function globalId() : string
    {
        return 'TB_Coopv3_103';
    }

    public static function create()
    {
        return new Card\TB_Coopv3_103;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_Coopv3_103';
    }
}