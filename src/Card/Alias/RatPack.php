<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RatPack
{
    public static function globalId() : string
    {
        return 'CFM_316';
    }

    public static function create()
    {
        return new Card\CFM_316;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_316';
    }
}