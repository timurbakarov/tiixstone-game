<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LivingMana
{
    public static function globalId() : string
    {
        return 'UNG_111';
    }

    public static function create()
    {
        return new Card\UNG_111;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_111';
    }
}