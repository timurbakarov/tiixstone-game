<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VenomstrikeTrap
{
    public static function globalId() : string
    {
        return 'ICC_200';
    }

    public static function create()
    {
        return new Card\ICC_200;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_200';
    }
}