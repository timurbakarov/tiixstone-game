<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OmnotronDefenseSystem
{
    public static function globalId() : string
    {
        return 'BRMC_93';
    }

    public static function create()
    {
        return new Card\BRMC_93;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_93';
    }
}