<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class UnbalancingStrike
{
    public static function globalId() : string
    {
        return 'NAX7_03';
    }

    public static function create()
    {
        return new Card\NAX7_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX7_03';
    }
}