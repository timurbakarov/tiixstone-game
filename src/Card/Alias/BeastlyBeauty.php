<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BeastlyBeauty
{
    public static function globalId() : string
    {
        return 'GILA_854';
    }

    public static function create()
    {
        return new Card\GILA_854;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_854';
    }
}