<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CityOfStormwind
{
    public static function globalId() : string
    {
        return 'TB_SPT_MTH_Boss0';
    }

    public static function create()
    {
        return new Card\TB_SPT_MTH_Boss0;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_SPT_MTH_Boss0';
    }
}