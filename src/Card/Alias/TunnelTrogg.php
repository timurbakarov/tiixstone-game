<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TunnelTrogg
{
    public static function globalId() : string
    {
        return 'LOE_018';
    }

    public static function create()
    {
        return new Card\LOE_018;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_018';
    }
}