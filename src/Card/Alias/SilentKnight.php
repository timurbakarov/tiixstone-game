<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SilentKnight
{
    public static function globalId() : string
    {
        return 'AT_095';
    }

    public static function create()
    {
        return new Card\AT_095;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_095';
    }
}