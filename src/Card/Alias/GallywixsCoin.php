<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GallywixsCoin
{
    public static function globalId() : string
    {
        return 'GVG_028t';
    }

    public static function create()
    {
        return new Card\GVG_028t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_028t';
    }
}