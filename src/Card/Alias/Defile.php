<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Defile
{
    public static function globalId() : string
    {
        return 'ICC_041';
    }

    public static function create()
    {
        return new Card\ICC_041;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_041';
    }
}