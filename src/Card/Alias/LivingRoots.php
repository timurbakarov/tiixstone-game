<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LivingRoots
{
    public static function globalId() : string
    {
        return 'AT_037';
    }

    public static function create()
    {
        return new Card\AT_037;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_037';
    }
}