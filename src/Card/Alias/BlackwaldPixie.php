<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BlackwaldPixie
{
    public static function globalId() : string
    {
        return 'GIL_561';
    }

    public static function create()
    {
        return new Card\GIL_561;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_561';
    }
}