<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KrulTheUnshackled
{
    public static function globalId() : string
    {
        return 'CFM_750';
    }

    public static function create()
    {
        return new Card\CFM_750;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_750';
    }
}