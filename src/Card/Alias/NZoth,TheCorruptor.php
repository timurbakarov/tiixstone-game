<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NZothTheCorruptor
{
    public static function globalId() : string
    {
        return 'OG_133';
    }

    public static function create()
    {
        return new Card\OG_133;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_133';
    }
}