<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BringItOn
{
    public static function globalId() : string
    {
        return 'ICC_837';
    }

    public static function create()
    {
        return new Card\ICC_837;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_837';
    }
}