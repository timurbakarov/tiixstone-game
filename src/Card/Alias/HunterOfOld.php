<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HunterOfOld
{
    public static function globalId() : string
    {
        return 'GILA_507';
    }

    public static function create()
    {
        return new Card\GILA_507;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_507';
    }
}