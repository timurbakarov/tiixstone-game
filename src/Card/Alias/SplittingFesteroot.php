<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SplittingFesteroot
{
    public static function globalId() : string
    {
        return 'GIL_616';
    }

    public static function create()
    {
        return new Card\GIL_616;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_616';
    }
}