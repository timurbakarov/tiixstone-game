<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FadeleafToxin
{
    public static function globalId() : string
    {
        return 'OG_080e';
    }

    public static function create()
    {
        return new Card\OG_080e;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_080e';
    }
}