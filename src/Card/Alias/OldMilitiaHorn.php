<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OldMilitiaHorn
{
    public static function globalId() : string
    {
        return 'GILA_852a';
    }

    public static function create()
    {
        return new Card\GILA_852a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_852a';
    }
}