<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheFinalBattle
{
    public static function globalId() : string
    {
        return 'FB_LK010';
    }

    public static function create()
    {
        return new Card\FB_LK010;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK010';
    }
}