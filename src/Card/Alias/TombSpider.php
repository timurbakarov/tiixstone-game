<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TombSpider
{
    public static function globalId() : string
    {
        return 'LOE_047';
    }

    public static function create()
    {
        return new Card\LOE_047;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_047';
    }
}