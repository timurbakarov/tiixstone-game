<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JadeClaws
{
    public static function globalId() : string
    {
        return 'CFM_717';
    }

    public static function create()
    {
        return new Card\CFM_717;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_717';
    }
}