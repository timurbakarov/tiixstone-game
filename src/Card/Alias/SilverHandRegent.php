<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SilverHandRegent
{
    public static function globalId() : string
    {
        return 'AT_100';
    }

    public static function create()
    {
        return new Card\AT_100;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_100';
    }
}