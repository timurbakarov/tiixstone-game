<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IAmMurloc
{
    public static function globalId() : string
    {
        return 'PRO_001a';
    }

    public static function create()
    {
        return new Card\PRO_001a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\PRO_001a';
    }
}