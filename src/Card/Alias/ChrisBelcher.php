<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ChrisBelcher
{
    public static function globalId() : string
    {
        return 'CRED_69';
    }

    public static function create()
    {
        return new Card\CRED_69;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_69';
    }
}