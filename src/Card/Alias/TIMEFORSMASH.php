<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TIMEFORSMASH
{
    public static function globalId() : string
    {
        return 'BRMA07_3';
    }

    public static function create()
    {
        return new Card\BRMA07_3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA07_3';
    }
}