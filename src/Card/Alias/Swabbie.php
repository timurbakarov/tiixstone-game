<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Swabbie
{
    public static function globalId() : string
    {
        return 'TRL_507t';
    }

    public static function create()
    {
        return new Card\TRL_507t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_507t';
    }
}