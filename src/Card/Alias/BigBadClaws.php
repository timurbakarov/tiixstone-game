<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BigBadClaws
{
    public static function globalId() : string
    {
        return 'KARA_05_02';
    }

    public static function create()
    {
        return new Card\KARA_05_02;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_05_02';
    }
}