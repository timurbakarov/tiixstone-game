<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DustinEscoffery
{
    public static function globalId() : string
    {
        return 'CRED_56';
    }

    public static function create()
    {
        return new Card\CRED_56;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_56';
    }
}