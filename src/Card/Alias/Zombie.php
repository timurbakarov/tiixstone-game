<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Zombie
{
    public static function globalId() : string
    {
        return 'TRL_131t';
    }

    public static function create()
    {
        return new Card\TRL_131t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_131t';
    }
}