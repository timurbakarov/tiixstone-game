<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class UnwillingSacrifice
{
    public static function globalId() : string
    {
        return 'ICC_469';
    }

    public static function create()
    {
        return new Card\ICC_469;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_469';
    }
}