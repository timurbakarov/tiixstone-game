<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ThickHide
{
    public static function globalId() : string
    {
        return 'GILA_498';
    }

    public static function create()
    {
        return new Card\GILA_498;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_498';
    }
}