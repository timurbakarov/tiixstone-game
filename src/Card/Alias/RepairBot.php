<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RepairBot
{
    public static function globalId() : string
    {
        return 'Mekka2';
    }

    public static function create()
    {
        return new Card\Mekka2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\Mekka2';
    }
}