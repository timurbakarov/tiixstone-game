<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WebWrap
{
    public static function globalId() : string
    {
        return 'NAX3_02_TB';
    }

    public static function create()
    {
        return new Card\NAX3_02_TB;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX3_02_TB';
    }
}