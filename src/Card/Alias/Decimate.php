<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Decimate
{
    public static function globalId() : string
    {
        return 'NAX12_02H_2_TB';
    }

    public static function create()
    {
        return new Card\NAX12_02H_2_TB;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX12_02H_2_TB';
    }
}