<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Woodchip
{
    public static function globalId() : string
    {
        return 'GIL_616t2';
    }

    public static function create()
    {
        return new Card\GIL_616t2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_616t2';
    }
}