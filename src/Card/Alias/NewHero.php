<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NewHero
{
    public static function globalId() : string
    {
        return 'FB_LK_NewHeroCards';
    }

    public static function create()
    {
        return new Card\FB_LK_NewHeroCards;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK_NewHeroCards';
    }
}