<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Dream
{
    public static function globalId() : string
    {
        return 'DREAM_04';
    }

    public static function create()
    {
        return new Card\DREAM_04;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\DREAM_04';
    }
}