<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GlindaCrowskin
{
    public static function globalId() : string
    {
        return 'GIL_618';
    }

    public static function create()
    {
        return new Card\GIL_618;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_618';
    }
}