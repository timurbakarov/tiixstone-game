<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Devastate
{
    public static function globalId() : string
    {
        return 'TRL_321';
    }

    public static function create()
    {
        return new Card\TRL_321;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_321';
    }
}