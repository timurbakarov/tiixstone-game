<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AnnoyoTron
{
    public static function globalId() : string
    {
        return 'TB_MechWar_Boss1';
    }

    public static function create()
    {
        return new Card\TB_MechWar_Boss1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_MechWar_Boss1';
    }
}