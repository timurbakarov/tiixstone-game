<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class InvocationOfEarth
{
    public static function globalId() : string
    {
        return 'UNG_211a';
    }

    public static function create()
    {
        return new Card\UNG_211a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_211a';
    }
}