<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BeneathTheGrounds
{
    public static function globalId() : string
    {
        return 'AT_035';
    }

    public static function create()
    {
        return new Card\AT_035;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_035';
    }
}