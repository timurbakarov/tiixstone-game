<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GrimPatron
{
    public static function globalId() : string
    {
        return 'BRM_019';
    }

    public static function create()
    {
        return new Card\BRM_019;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_019';
    }
}