<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Aviana
{
    public static function globalId() : string
    {
        return 'AT_045';
    }

    public static function create()
    {
        return new Card\AT_045;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_045';
    }
}