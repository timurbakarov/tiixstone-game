<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LyraTheSunshard
{
    public static function globalId() : string
    {
        return 'UNG_963';
    }

    public static function create()
    {
        return new Card\UNG_963;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_963';
    }
}