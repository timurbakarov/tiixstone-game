<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Chains
{
    public static function globalId() : string
    {
        return 'NAX15_04';
    }

    public static function create()
    {
        return new Card\NAX15_04;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX15_04';
    }
}