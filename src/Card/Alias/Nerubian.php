<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Nerubian
{
    public static function globalId() : string
    {
        return 'AT_036t';
    }

    public static function create()
    {
        return new Card\AT_036t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_036t';
    }
}