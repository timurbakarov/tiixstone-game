<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArenaFanatic
{
    public static function globalId() : string
    {
        return 'TRL_517';
    }

    public static function create()
    {
        return new Card\TRL_517;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_517';
    }
}