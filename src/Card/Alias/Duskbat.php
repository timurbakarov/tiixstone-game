<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Duskbat
{
    public static function globalId() : string
    {
        return 'GIL_508';
    }

    public static function create()
    {
        return new Card\GIL_508;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_508';
    }
}