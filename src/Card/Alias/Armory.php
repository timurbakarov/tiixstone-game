<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Armory
{
    public static function globalId() : string
    {
        return 'GILA_Darius_06';
    }

    public static function create()
    {
        return new Card\GILA_Darius_06;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_Darius_06';
    }
}