<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LilExorcist
{
    public static function globalId() : string
    {
        return 'GVG_097';
    }

    public static function create()
    {
        return new Card\GVG_097;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_097';
    }
}