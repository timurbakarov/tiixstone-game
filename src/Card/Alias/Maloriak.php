<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Maloriak
{
    public static function globalId() : string
    {
        return 'BRMA15_1';
    }

    public static function create()
    {
        return new Card\BRMA15_1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA15_1';
    }
}