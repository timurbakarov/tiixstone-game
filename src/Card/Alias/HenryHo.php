<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HenryHo
{
    public static function globalId() : string
    {
        return 'CRED_27';
    }

    public static function create()
    {
        return new Card\CRED_27;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_27';
    }
}