<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class InquisitorWhitemane
{
    public static function globalId() : string
    {
        return 'ICC_829t4';
    }

    public static function create()
    {
        return new Card\ICC_829t4;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_829t4';
    }
}