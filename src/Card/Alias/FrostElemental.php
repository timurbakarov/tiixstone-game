<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FrostElemental
{
    public static function globalId() : string
    {
        return 'EX1_283';
    }

    public static function create()
    {
        return new Card\EX1_283;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_283';
    }
}