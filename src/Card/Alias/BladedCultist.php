<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BladedCultist
{
    public static function globalId() : string
    {
        return 'OG_070';
    }

    public static function create()
    {
        return new Card\OG_070;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_070';
    }
}