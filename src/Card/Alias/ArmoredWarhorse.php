<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArmoredWarhorse
{
    public static function globalId() : string
    {
        return 'AT_108';
    }

    public static function create()
    {
        return new Card\AT_108;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_108';
    }
}