<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Rummage
{
    public static function globalId() : string
    {
        return 'LOEA16_16H';
    }

    public static function create()
    {
        return new Card\LOEA16_16H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA16_16H';
    }
}