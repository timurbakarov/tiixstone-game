<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RazorpetalVolley
{
    public static function globalId() : string
    {
        return 'UNG_057';
    }

    public static function create()
    {
        return new Card\UNG_057;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_057';
    }
}