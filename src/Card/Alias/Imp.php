<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Imp
{
    public static function globalId() : string
    {
        return 'GIL_191t';
    }

    public static function create()
    {
        return new Card\GIL_191t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_191t';
    }
}