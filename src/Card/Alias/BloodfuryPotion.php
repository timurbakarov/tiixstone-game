<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodfuryPotion
{
    public static function globalId() : string
    {
        return 'CFM_611';
    }

    public static function create()
    {
        return new Card\CFM_611;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_611';
    }
}