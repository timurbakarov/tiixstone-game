<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FrozenCrusher
{
    public static function globalId() : string
    {
        return 'UNG_079';
    }

    public static function create()
    {
        return new Card\UNG_079;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_079';
    }
}