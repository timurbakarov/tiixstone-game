<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HamiltonChu
{
    public static function globalId() : string
    {
        return 'CRED_16';
    }

    public static function create()
    {
        return new Card\CRED_16;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_16';
    }
}