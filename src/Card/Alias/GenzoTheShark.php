<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GenzoTheShark
{
    public static function globalId() : string
    {
        return 'CFM_808';
    }

    public static function create()
    {
        return new Card\CFM_808;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_808';
    }
}