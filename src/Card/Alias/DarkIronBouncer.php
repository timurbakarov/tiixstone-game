<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DarkIronBouncer
{
    public static function globalId() : string
    {
        return 'BRMA01_3';
    }

    public static function create()
    {
        return new Card\BRMA01_3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA01_3';
    }
}