<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ScaledNightmare
{
    public static function globalId() : string
    {
        return 'OG_271';
    }

    public static function create()
    {
        return new Card\OG_271;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_271';
    }
}