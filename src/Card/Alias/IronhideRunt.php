<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IronhideRunt
{
    public static function globalId() : string
    {
        return 'TRL_232t';
    }

    public static function create()
    {
        return new Card\TRL_232t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_232t';
    }
}