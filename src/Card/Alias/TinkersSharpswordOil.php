<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TinkersSharpswordOil
{
    public static function globalId() : string
    {
        return 'GVG_022';
    }

    public static function create()
    {
        return new Card\GVG_022;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_022';
    }
}