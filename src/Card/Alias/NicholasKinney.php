<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NicholasKinney
{
    public static function globalId() : string
    {
        return 'CRED_76';
    }

    public static function create()
    {
        return new Card\CRED_76;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_76';
    }
}