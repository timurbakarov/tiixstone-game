<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KabalCourier
{
    public static function globalId() : string
    {
        return 'CFM_649';
    }

    public static function create()
    {
        return new Card\CFM_649;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_649';
    }
}