<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DavidPendergrast
{
    public static function globalId() : string
    {
        return 'CRED_73';
    }

    public static function create()
    {
        return new Card\CRED_73;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_73';
    }
}