<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HeavyMetal
{
    public static function globalId() : string
    {
        return 'TRL_324';
    }

    public static function create()
    {
        return new Card\TRL_324;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_324';
    }
}