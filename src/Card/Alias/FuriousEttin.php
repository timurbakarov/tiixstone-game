<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FuriousEttin
{
    public static function globalId() : string
    {
        return 'GIL_120';
    }

    public static function create()
    {
        return new Card\GIL_120;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_120';
    }
}