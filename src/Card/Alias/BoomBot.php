<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BoomBot
{
    public static function globalId() : string
    {
        return 'TB_MechWar_Boss2';
    }

    public static function create()
    {
        return new Card\TB_MechWar_Boss2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_MechWar_Boss2';
    }
}