<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GiantInsect
{
    public static function globalId() : string
    {
        return 'LOEA04_23';
    }

    public static function create()
    {
        return new Card\LOEA04_23;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA04_23';
    }
}