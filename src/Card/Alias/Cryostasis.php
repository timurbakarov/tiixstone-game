<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Cryostasis
{
    public static function globalId() : string
    {
        return 'ICC_056';
    }

    public static function create()
    {
        return new Card\ICC_056;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_056';
    }
}