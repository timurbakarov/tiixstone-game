<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NetherImp
{
    public static function globalId() : string
    {
        return 'UNG_829t3';
    }

    public static function create()
    {
        return new Card\UNG_829t3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_829t3';
    }
}