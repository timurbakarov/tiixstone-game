<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TradePrinceGallywix
{
    public static function globalId() : string
    {
        return 'GVG_028';
    }

    public static function create()
    {
        return new Card\GVG_028;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_028';
    }
}