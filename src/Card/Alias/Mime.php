<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Mime
{
    public static function globalId() : string
    {
        return 'KARA_13_22';
    }

    public static function create()
    {
        return new Card\KARA_13_22;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_13_22';
    }
}