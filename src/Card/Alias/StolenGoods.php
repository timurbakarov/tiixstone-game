<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StolenGoods
{
    public static function globalId() : string
    {
        return 'CFM_752';
    }

    public static function create()
    {
        return new Card\CFM_752;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_752';
    }
}