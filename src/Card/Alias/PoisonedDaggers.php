<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PoisonedDaggers
{
    public static function globalId() : string
    {
        return 'AT_132_ROGUE_H1';
    }

    public static function create()
    {
        return new Card\AT_132_ROGUE_H1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_132_ROGUE_H1';
    }
}