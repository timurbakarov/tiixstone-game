<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NagaTonguelasher
{
    public static function globalId() : string
    {
        return 'TRLA_160';
    }

    public static function create()
    {
        return new Card\TRLA_160;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_160';
    }
}