<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PitFighter
{
    public static function globalId() : string
    {
        return 'AT_101';
    }

    public static function create()
    {
        return new Card\AT_101;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_101';
    }
}