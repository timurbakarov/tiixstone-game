<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HardpackedSnowballs
{
    public static function globalId() : string
    {
        return 'TB_GiftExchange_Snowball';
    }

    public static function create()
    {
        return new Card\TB_GiftExchange_Snowball;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_GiftExchange_Snowball';
    }
}