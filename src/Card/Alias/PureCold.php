<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PureCold
{
    public static function globalId() : string
    {
        return 'NAX14_04';
    }

    public static function create()
    {
        return new Card\NAX14_04;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX14_04';
    }
}