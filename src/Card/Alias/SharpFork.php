<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SharpFork
{
    public static function globalId() : string
    {
        return 'KAR_094a';
    }

    public static function create()
    {
        return new Card\KAR_094a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_094a';
    }
}