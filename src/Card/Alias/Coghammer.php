<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Coghammer
{
    public static function globalId() : string
    {
        return 'GVG_059';
    }

    public static function create()
    {
        return new Card\GVG_059;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_059';
    }
}