<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FiretreeWitchdoctor
{
    public static function globalId() : string
    {
        return 'TRL_523';
    }

    public static function create()
    {
        return new Card\TRL_523;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_523';
    }
}