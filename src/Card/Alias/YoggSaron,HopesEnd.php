<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class YoggSaronHopesEnd
{
    public static function globalId() : string
    {
        return 'OG_134';
    }

    public static function create()
    {
        return new Card\OG_134;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_134';
    }
}