<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FlamewreathedFaceless
{
    public static function globalId() : string
    {
        return 'OG_024';
    }

    public static function create()
    {
        return new Card\OG_024;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_024';
    }
}