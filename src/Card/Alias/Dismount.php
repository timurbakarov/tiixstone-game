<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Dismount
{
    public static function globalId() : string
    {
        return 'BRMA09_5';
    }

    public static function create()
    {
        return new Card\BRMA09_5;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA09_5';
    }
}