<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Fatespinner
{
    public static function globalId() : string
    {
        return 'ICC_047';
    }

    public static function create()
    {
        return new Card\ICC_047;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_047';
    }
}