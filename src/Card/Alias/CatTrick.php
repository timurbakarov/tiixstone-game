<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CatTrick
{
    public static function globalId() : string
    {
        return 'KAR_004';
    }

    public static function create()
    {
        return new Card\KAR_004;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_004';
    }
}