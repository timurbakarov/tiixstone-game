<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RavenousPterrordax
{
    public static function globalId() : string
    {
        return 'UNG_047';
    }

    public static function create()
    {
        return new Card\UNG_047;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_047';
    }
}