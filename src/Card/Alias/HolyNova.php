<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HolyNova
{
    public static function globalId() : string
    {
        return 'CS1_112';
    }

    public static function create()
    {
        return new Card\CS1_112;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS1_112';
    }
}