<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class YoggSaronsMagic
{
    public static function globalId() : string
    {
        return 'OG_202b';
    }

    public static function create()
    {
        return new Card\OG_202b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_202b';
    }
}