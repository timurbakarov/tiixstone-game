<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Razorgore
{
    public static function globalId() : string
    {
        return 'BRMC_98';
    }

    public static function create()
    {
        return new Card\BRMC_98;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_98';
    }
}