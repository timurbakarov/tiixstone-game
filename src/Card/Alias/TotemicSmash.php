<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TotemicSmash
{
    public static function globalId() : string
    {
        return 'TRL_012';
    }

    public static function create()
    {
        return new Card\TRL_012;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_012';
    }
}