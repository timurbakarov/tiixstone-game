<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MattWyble
{
    public static function globalId() : string
    {
        return 'CRED_57';
    }

    public static function create()
    {
        return new Card\CRED_57;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_57';
    }
}