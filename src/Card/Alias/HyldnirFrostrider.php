<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HyldnirFrostrider
{
    public static function globalId() : string
    {
        return 'ICC_855';
    }

    public static function create()
    {
        return new Card\ICC_855;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_855';
    }
}