<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Recruiter
{
    public static function globalId() : string
    {
        return 'AT_113';
    }

    public static function create()
    {
        return new Card\AT_113;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_113';
    }
}