<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GangUp
{
    public static function globalId() : string
    {
        return 'BRM_007';
    }

    public static function create()
    {
        return new Card\BRM_007;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_007';
    }
}