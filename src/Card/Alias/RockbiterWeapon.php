<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RockbiterWeapon
{
    public static function globalId() : string
    {
        return 'CS2_045';
    }

    public static function create()
    {
        return new Card\CS2_045;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_045';
    }
}