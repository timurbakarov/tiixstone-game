<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Blingtron3000
{
    public static function globalId() : string
    {
        return 'GVG_119';
    }

    public static function create()
    {
        return new Card\GVG_119;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_119';
    }
}