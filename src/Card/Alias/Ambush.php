<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Ambush
{
    public static function globalId() : string
    {
        return 'AT_035t';
    }

    public static function create()
    {
        return new Card\AT_035t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_035t';
    }
}