<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BananaBuffoon
{
    public static function globalId() : string
    {
        return 'TRL_509';
    }

    public static function create()
    {
        return new Card\TRL_509;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_509';
    }
}