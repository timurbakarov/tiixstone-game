<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MogorsChampion
{
    public static function globalId() : string
    {
        return 'AT_088';
    }

    public static function create()
    {
        return new Card\AT_088;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_088';
    }
}