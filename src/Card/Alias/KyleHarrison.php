<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KyleHarrison
{
    public static function globalId() : string
    {
        return 'CRED_05';
    }

    public static function create()
    {
        return new Card\CRED_05;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_05';
    }
}