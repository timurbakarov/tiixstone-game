<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Chicken
{
    public static function globalId() : string
    {
        return 'Mekka4t';
    }

    public static function create()
    {
        return new Card\Mekka4t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\Mekka4t';
    }
}