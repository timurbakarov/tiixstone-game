<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BurlyRockjawTrogg
{
    public static function globalId() : string
    {
        return 'GVG_068';
    }

    public static function create()
    {
        return new Card\GVG_068;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_068';
    }
}