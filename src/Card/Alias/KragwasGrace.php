<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KragwasGrace
{
    public static function globalId() : string
    {
        return 'TRLA_155t';
    }

    public static function create()
    {
        return new Card\TRLA_155t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_155t';
    }
}