<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BoomBotJr
{
    public static function globalId() : string
    {
        return 'TB_MechWar_Boss2_HeroPower';
    }

    public static function create()
    {
        return new Card\TB_MechWar_Boss2_HeroPower;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_MechWar_Boss2_HeroPower';
    }
}