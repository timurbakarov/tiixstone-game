<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KragwasLure
{
    public static function globalId() : string
    {
        return 'TRLA_109';
    }

    public static function create()
    {
        return new Card\TRLA_109;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_109';
    }
}