<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Shadowboxer
{
    public static function globalId() : string
    {
        return 'GVG_072';
    }

    public static function create()
    {
        return new Card\GVG_072;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_072';
    }
}