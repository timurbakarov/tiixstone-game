<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SunborneValkyr
{
    public static function globalId() : string
    {
        return 'ICC_028';
    }

    public static function create()
    {
        return new Card\ICC_028;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_028';
    }
}