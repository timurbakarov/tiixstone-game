<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AnimalCompanion
{
    public static function globalId() : string
    {
        return 'NEW1_031';
    }

    public static function create()
    {
        return new Card\NEW1_031;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NEW1_031';
    }
}