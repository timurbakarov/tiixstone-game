<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Sandbinder
{
    public static function globalId() : string
    {
        return 'GIL_581';
    }

    public static function create()
    {
        return new Card\GIL_581;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_581';
    }
}