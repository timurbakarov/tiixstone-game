<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArcaneExplosion
{
    public static function globalId() : string
    {
        return 'CS2_025';
    }

    public static function create()
    {
        return new Card\CS2_025;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_025';
    }
}