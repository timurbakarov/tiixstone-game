<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RicochetShot
{
    public static function globalId() : string
    {
        return 'GILA_820';
    }

    public static function create()
    {
        return new Card\GILA_820;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_820';
    }
}