<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Voidform
{
    public static function globalId() : string
    {
        return 'ICC_830p';
    }

    public static function create()
    {
        return new Card\ICC_830p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_830p';
    }
}