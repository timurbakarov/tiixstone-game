<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AncientHarbinger
{
    public static function globalId() : string
    {
        return 'OG_290';
    }

    public static function create()
    {
        return new Card\OG_290;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_290';
    }
}