<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DragonhawkRider
{
    public static function globalId() : string
    {
        return 'AT_083';
    }

    public static function create()
    {
        return new Card\AT_083;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_083';
    }
}