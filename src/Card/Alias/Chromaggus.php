<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Chromaggus
{
    public static function globalId() : string
    {
        return 'BRM_031';
    }

    public static function create()
    {
        return new Card\BRM_031;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_031';
    }
}