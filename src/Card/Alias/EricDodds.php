<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EricDodds
{
    public static function globalId() : string
    {
        return 'CRED_02';
    }

    public static function create()
    {
        return new Card\CRED_02;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_02';
    }
}