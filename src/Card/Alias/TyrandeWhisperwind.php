<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TyrandeWhisperwind
{
    public static function globalId() : string
    {
        return 'HERO_09a';
    }

    public static function create()
    {
        return new Card\HERO_09a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HERO_09a';
    }
}