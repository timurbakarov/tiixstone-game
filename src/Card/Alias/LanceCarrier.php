<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LanceCarrier
{
    public static function globalId() : string
    {
        return 'AT_084';
    }

    public static function create()
    {
        return new Card\AT_084;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_084';
    }
}