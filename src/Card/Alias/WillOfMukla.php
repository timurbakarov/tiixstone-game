<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WillOfMukla
{
    public static function globalId() : string
    {
        return 'TU4c_008';
    }

    public static function create()
    {
        return new Card\TU4c_008;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TU4c_008';
    }
}