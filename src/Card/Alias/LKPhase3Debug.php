<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LKPhase3Debug
{
    public static function globalId() : string
    {
        return 'FB_LKDebug002';
    }

    public static function create()
    {
        return new Card\FB_LKDebug002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LKDebug002';
    }
}