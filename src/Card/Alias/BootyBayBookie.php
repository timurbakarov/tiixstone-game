<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BootyBayBookie
{
    public static function globalId() : string
    {
        return 'TRL_504';
    }

    public static function create()
    {
        return new Card\TRL_504;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_504';
    }
}