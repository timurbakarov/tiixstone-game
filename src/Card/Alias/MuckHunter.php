<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MuckHunter
{
    public static function globalId() : string
    {
        return 'GIL_682';
    }

    public static function create()
    {
        return new Card\GIL_682;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_682';
    }
}