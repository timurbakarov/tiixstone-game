<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LakkariFelhound
{
    public static function globalId() : string
    {
        return 'UNG_833';
    }

    public static function create()
    {
        return new Card\UNG_833;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_833';
    }
}