<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SavageCombatant
{
    public static function globalId() : string
    {
        return 'AT_039';
    }

    public static function create()
    {
        return new Card\AT_039;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_039';
    }
}