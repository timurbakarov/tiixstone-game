<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NZothsFirstMate
{
    public static function globalId() : string
    {
        return 'OG_312';
    }

    public static function create()
    {
        return new Card\OG_312;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_312';
    }
}