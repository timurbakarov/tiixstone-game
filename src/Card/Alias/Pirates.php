<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Pirates
{
    public static function globalId() : string
    {
        return 'GILA_Darius_02';
    }

    public static function create()
    {
        return new Card\GILA_Darius_02;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_Darius_02';
    }
}