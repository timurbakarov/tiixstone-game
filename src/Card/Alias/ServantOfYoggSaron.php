<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ServantOfYoggSaron
{
    public static function globalId() : string
    {
        return 'OG_087';
    }

    public static function create()
    {
        return new Card\OG_087;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_087';
    }
}