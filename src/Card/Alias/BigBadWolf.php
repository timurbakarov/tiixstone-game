<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BigBadWolf
{
    public static function globalId() : string
    {
        return 'KAR_005a';
    }

    public static function create()
    {
        return new Card\KAR_005a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_005a';
    }
}