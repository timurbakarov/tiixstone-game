<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DeathbringerSaurfang
{
    public static function globalId() : string
    {
        return 'ICCA09_002';
    }

    public static function create()
    {
        return new Card\ICCA09_002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA09_002';
    }
}