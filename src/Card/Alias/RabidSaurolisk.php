<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RabidSaurolisk
{
    public static function globalId() : string
    {
        return 'TRLA_167';
    }

    public static function create()
    {
        return new Card\TRLA_167;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_167';
    }
}