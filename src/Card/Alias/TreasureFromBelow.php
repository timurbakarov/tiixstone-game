<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TreasureFromBelow
{
    public static function globalId() : string
    {
        return 'TRLA_186';
    }

    public static function create()
    {
        return new Card\TRLA_186;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_186';
    }
}