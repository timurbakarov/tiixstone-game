<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KelThuzad
{
    public static function globalId() : string
    {
        return 'TB_KTRAF_H_1';
    }

    public static function create()
    {
        return new Card\TB_KTRAF_H_1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_KTRAF_H_1';
    }
}