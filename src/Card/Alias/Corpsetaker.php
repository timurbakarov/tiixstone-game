<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Corpsetaker
{
    public static function globalId() : string
    {
        return 'ICC_912';
    }

    public static function create()
    {
        return new Card\ICC_912;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_912';
    }
}