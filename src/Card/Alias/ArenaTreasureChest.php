<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArenaTreasureChest
{
    public static function globalId() : string
    {
        return 'TRL_525';
    }

    public static function create()
    {
        return new Card\TRL_525;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_525';
    }
}