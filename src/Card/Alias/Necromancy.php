<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Necromancy
{
    public static function globalId() : string
    {
        return 'TB_KTRAF_HP_KT_3';
    }

    public static function create()
    {
        return new Card\TB_KTRAF_HP_KT_3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_KTRAF_HP_KT_3';
    }
}