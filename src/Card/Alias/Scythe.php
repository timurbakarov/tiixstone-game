<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Scythe
{
    public static function globalId() : string
    {
        return 'GILA_BOSS_33t';
    }

    public static function create()
    {
        return new Card\GILA_BOSS_33t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_BOSS_33t';
    }
}