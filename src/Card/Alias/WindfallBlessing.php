<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WindfallBlessing
{
    public static function globalId() : string
    {
        return 'TRLA_809';
    }

    public static function create()
    {
        return new Card\TRLA_809;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_809';
    }
}