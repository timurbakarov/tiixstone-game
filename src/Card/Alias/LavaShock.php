<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LavaShock
{
    public static function globalId() : string
    {
        return 'BRM_011';
    }

    public static function create()
    {
        return new Card\BRM_011;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_011';
    }
}