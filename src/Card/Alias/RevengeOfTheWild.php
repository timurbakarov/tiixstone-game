<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RevengeOfTheWild
{
    public static function globalId() : string
    {
        return 'TRL_566';
    }

    public static function create()
    {
        return new Card\TRL_566;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_566';
    }
}