<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheSteelSentinel
{
    public static function globalId() : string
    {
        return 'LOEA16_27H';
    }

    public static function create()
    {
        return new Card\LOEA16_27H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA16_27H';
    }
}