<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AcherusVeteran
{
    public static function globalId() : string
    {
        return 'ICC_092';
    }

    public static function create()
    {
        return new Card\ICC_092;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_092';
    }
}