<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GearmasterMechazod
{
    public static function globalId() : string
    {
        return 'HRW02_2';
    }

    public static function create()
    {
        return new Card\HRW02_2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HRW02_2';
    }
}