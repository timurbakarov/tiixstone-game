<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BerserkerThrow
{
    public static function globalId() : string
    {
        return 'TRL_065h';
    }

    public static function create()
    {
        return new Card\TRL_065h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_065h';
    }
}