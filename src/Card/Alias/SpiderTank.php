<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpiderTank
{
    public static function globalId() : string
    {
        return 'GVG_044';
    }

    public static function create()
    {
        return new Card\GVG_044;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_044';
    }
}