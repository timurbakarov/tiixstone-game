<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TrollHarbinger
{
    public static function globalId() : string
    {
        return 'TRLA_166';
    }

    public static function create()
    {
        return new Card\TRLA_166;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_166';
    }
}