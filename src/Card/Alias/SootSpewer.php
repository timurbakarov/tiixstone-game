<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SootSpewer
{
    public static function globalId() : string
    {
        return 'GVG_123';
    }

    public static function create()
    {
        return new Card\GVG_123;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_123';
    }
}