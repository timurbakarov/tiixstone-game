<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BigSpells
{
    public static function globalId() : string
    {
        return 'TRLA_Mage_03';
    }

    public static function create()
    {
        return new Card\TRLA_Mage_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Mage_03';
    }
}