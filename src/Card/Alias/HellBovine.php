<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HellBovine
{
    public static function globalId() : string
    {
        return 'TB_SPT_DPromoMinionInit';
    }

    public static function create()
    {
        return new Card\TB_SPT_DPromoMinionInit;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_SPT_DPromoMinionInit';
    }
}