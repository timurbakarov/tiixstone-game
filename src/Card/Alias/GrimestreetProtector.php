<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GrimestreetProtector
{
    public static function globalId() : string
    {
        return 'CFM_062';
    }

    public static function create()
    {
        return new Card\CFM_062;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_062';
    }
}