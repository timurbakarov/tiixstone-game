<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Knuckles
{
    public static function globalId() : string
    {
        return 'CFM_333';
    }

    public static function create()
    {
        return new Card\CFM_333;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_333';
    }
}