<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodWitch
{
    public static function globalId() : string
    {
        return 'GIL_693';
    }

    public static function create()
    {
        return new Card\GIL_693;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_693';
    }
}