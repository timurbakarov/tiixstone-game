<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Meteor
{
    public static function globalId() : string
    {
        return 'UNG_955';
    }

    public static function create()
    {
        return new Card\UNG_955;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_955';
    }
}