<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TimepieceOfHorror
{
    public static function globalId() : string
    {
        return 'LOEA16_4';
    }

    public static function create()
    {
        return new Card\LOEA16_4;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA16_4';
    }
}