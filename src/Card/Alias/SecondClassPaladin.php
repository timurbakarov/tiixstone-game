<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SecondClassPaladin
{
    public static function globalId() : string
    {
        return 'TB_ClassRandom_Paladin';
    }

    public static function create()
    {
        return new Card\TB_ClassRandom_Paladin;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_ClassRandom_Paladin';
    }
}