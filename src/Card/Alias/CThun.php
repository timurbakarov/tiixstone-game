<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CThun
{
    public static function globalId() : string
    {
        return 'OG_280';
    }

    public static function create()
    {
        return new Card\OG_280;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_280';
    }
}