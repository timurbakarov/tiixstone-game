<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RabbleBouncer
{
    public static function globalId() : string
    {
        return 'TRL_515';
    }

    public static function create()
    {
        return new Card\TRL_515;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_515';
    }
}