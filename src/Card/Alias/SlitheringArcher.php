<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SlitheringArcher
{
    public static function globalId() : string
    {
        return 'LOEA09_6';
    }

    public static function create()
    {
        return new Card\LOEA09_6;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA09_6';
    }
}