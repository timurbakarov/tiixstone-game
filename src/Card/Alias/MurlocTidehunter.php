<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MurlocTidehunter
{
    public static function globalId() : string
    {
        return 'EX1_506';
    }

    public static function create()
    {
        return new Card\EX1_506;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_506';
    }
}