<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Ultrasaur
{
    public static function globalId() : string
    {
        return 'UNG_806';
    }

    public static function create()
    {
        return new Card\UNG_806;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_806';
    }
}