<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MarkOfYShaarj
{
    public static function globalId() : string
    {
        return 'OG_048';
    }

    public static function create()
    {
        return new Card\OG_048;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_048';
    }
}