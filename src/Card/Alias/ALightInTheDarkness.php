<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ALightInTheDarkness
{
    public static function globalId() : string
    {
        return 'OG_311';
    }

    public static function create()
    {
        return new Card\OG_311;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_311';
    }
}