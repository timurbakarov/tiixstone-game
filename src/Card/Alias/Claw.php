<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Claw
{
    public static function globalId() : string
    {
        return 'CS2_005';
    }

    public static function create()
    {
        return new Card\CS2_005;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_005';
    }
}