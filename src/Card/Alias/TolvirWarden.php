<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TolvirWarden
{
    public static function globalId() : string
    {
        return 'UNG_913';
    }

    public static function create()
    {
        return new Card\UNG_913;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_913';
    }
}