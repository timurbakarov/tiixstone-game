<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WhirlingAsh
{
    public static function globalId() : string
    {
        return 'BRMC_89';
    }

    public static function create()
    {
        return new Card\BRMC_89;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_89';
    }
}