<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ManicSoulcaster
{
    public static function globalId() : string
    {
        return 'CFM_660';
    }

    public static function create()
    {
        return new Card\CFM_660;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_660';
    }
}