<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GiftOfCards
{
    public static function globalId() : string
    {
        return 'GVG_032b';
    }

    public static function create()
    {
        return new Card\GVG_032b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_032b';
    }
}