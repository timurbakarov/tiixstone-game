<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BombLobber
{
    public static function globalId() : string
    {
        return 'GVG_099';
    }

    public static function create()
    {
        return new Card\GVG_099;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_099';
    }
}