<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HatefulStrike
{
    public static function globalId() : string
    {
        return 'NAX10_03';
    }

    public static function create()
    {
        return new Card\NAX10_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX10_03';
    }
}