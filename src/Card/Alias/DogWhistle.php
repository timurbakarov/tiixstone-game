<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DogWhistle
{
    public static function globalId() : string
    {
        return 'GILA_400p';
    }

    public static function create()
    {
        return new Card\GILA_400p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_400p';
    }
}