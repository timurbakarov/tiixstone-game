<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class InvocationOfAir
{
    public static function globalId() : string
    {
        return 'UNG_211d';
    }

    public static function create()
    {
        return new Card\UNG_211d;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_211d';
    }
}