<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Sacrifice
{
    public static function globalId() : string
    {
        return 'TRLA_Warlock_07';
    }

    public static function create()
    {
        return new Card\TRLA_Warlock_07;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Warlock_07';
    }
}