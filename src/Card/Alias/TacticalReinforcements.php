<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TacticalReinforcements
{
    public static function globalId() : string
    {
        return 'GILA_603';
    }

    public static function create()
    {
        return new Card\GILA_603;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_603';
    }
}