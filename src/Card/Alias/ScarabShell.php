<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ScarabShell
{
    public static function globalId() : string
    {
        return 'ICC_832pa';
    }

    public static function create()
    {
        return new Card\ICC_832pa;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_832pa';
    }
}