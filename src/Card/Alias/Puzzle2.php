<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Puzzle2
{
    public static function globalId() : string
    {
        return 'TB_Lethal003';
    }

    public static function create()
    {
        return new Card\TB_Lethal003;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_Lethal003';
    }
}