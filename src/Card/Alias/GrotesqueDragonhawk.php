<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GrotesqueDragonhawk
{
    public static function globalId() : string
    {
        return 'OG_152';
    }

    public static function create()
    {
        return new Card\OG_152;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_152';
    }
}