<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Power(Spell)
{
    public static function globalId() : string
    {
        return 'TB_CoopHero_002';
    }

    public static function create()
    {
        return new Card\TB_CoopHero_002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_CoopHero_002';
    }
}