<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EvilHeckler
{
    public static function globalId() : string
    {
        return 'AT_114';
    }

    public static function create()
    {
        return new Card\AT_114;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_114';
    }
}