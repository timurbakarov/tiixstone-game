<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HalazzisHunt
{
    public static function globalId() : string
    {
        return 'TRLA_162';
    }

    public static function create()
    {
        return new Card\TRLA_162;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_162';
    }
}