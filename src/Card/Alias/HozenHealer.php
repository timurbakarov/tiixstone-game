<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HozenHealer
{
    public static function globalId() : string
    {
        return 'CFM_067';
    }

    public static function create()
    {
        return new Card\CFM_067;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_067';
    }
}