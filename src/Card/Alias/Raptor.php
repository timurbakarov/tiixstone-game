<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Raptor
{
    public static function globalId() : string
    {
        return 'TRL_254t';
    }

    public static function create()
    {
        return new Card\TRL_254t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_254t';
    }
}