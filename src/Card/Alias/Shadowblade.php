<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Shadowblade
{
    public static function globalId() : string
    {
        return 'ICC_850';
    }

    public static function create()
    {
        return new Card\ICC_850;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_850';
    }
}