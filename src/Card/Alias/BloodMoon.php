<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodMoon
{
    public static function globalId() : string
    {
        return 'GILA_412';
    }

    public static function create()
    {
        return new Card\GILA_412;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_412';
    }
}