<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SerpentWard
{
    public static function globalId() : string
    {
        return 'TRL_057';
    }

    public static function create()
    {
        return new Card\TRL_057;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_057';
    }
}