<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JourneyBelow
{
    public static function globalId() : string
    {
        return 'OG_072';
    }

    public static function create()
    {
        return new Card\OG_072;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_072';
    }
}