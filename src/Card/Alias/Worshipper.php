<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Worshipper
{
    public static function globalId() : string
    {
        return 'NAX2_05';
    }

    public static function create()
    {
        return new Card\NAX2_05;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX2_05';
    }
}