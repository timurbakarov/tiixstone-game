<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Hotshot
{
    public static function globalId() : string
    {
        return 'TRL_151t';
    }

    public static function create()
    {
        return new Card\TRL_151t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_151t';
    }
}