<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NorthSeaKraken
{
    public static function globalId() : string
    {
        return 'AT_103';
    }

    public static function create()
    {
        return new Card\AT_103;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_103';
    }
}