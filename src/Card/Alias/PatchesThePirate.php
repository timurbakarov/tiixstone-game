<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PatchesThePirate
{
    public static function globalId() : string
    {
        return 'CFM_637';
    }

    public static function create()
    {
        return new Card\CFM_637;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_637';
    }
}