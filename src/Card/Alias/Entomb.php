<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Entomb
{
    public static function globalId() : string
    {
        return 'LOE_104';
    }

    public static function create()
    {
        return new Card\LOE_104;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_104';
    }
}