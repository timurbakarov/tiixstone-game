<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DartTrap
{
    public static function globalId() : string
    {
        return 'LOE_021';
    }

    public static function create()
    {
        return new Card\LOE_021;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_021';
    }
}