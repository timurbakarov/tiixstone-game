<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NormalDifficulty
{
    public static function globalId() : string
    {
        return 'FB_LK_BossSetup001c';
    }

    public static function create()
    {
        return new Card\FB_LK_BossSetup001c;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK_BossSetup001c';
    }
}