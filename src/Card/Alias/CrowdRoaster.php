<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CrowdRoaster
{
    public static function globalId() : string
    {
        return 'TRL_569';
    }

    public static function create()
    {
        return new Card\TRL_569;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_569';
    }
}