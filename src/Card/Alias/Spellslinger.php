<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Spellslinger
{
    public static function globalId() : string
    {
        return 'AT_007';
    }

    public static function create()
    {
        return new Card\AT_007;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_007';
    }
}