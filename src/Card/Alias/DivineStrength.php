<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DivineStrength
{
    public static function globalId() : string
    {
        return 'OG_223';
    }

    public static function create()
    {
        return new Card\OG_223;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_223';
    }
}