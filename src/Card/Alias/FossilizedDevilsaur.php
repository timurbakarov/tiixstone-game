<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FossilizedDevilsaur
{
    public static function globalId() : string
    {
        return 'LOE_073';
    }

    public static function create()
    {
        return new Card\LOE_073;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_073';
    }
}