<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Doomerang
{
    public static function globalId() : string
    {
        return 'ICC_233';
    }

    public static function create()
    {
        return new Card\ICC_233;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_233';
    }
}