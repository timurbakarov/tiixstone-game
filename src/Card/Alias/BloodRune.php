<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodRune
{
    public static function globalId() : string
    {
        return 'ICCA09_001p';
    }

    public static function create()
    {
        return new Card\ICCA09_001p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA09_001p';
    }
}