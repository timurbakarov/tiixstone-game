<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GilneanTracker
{
    public static function globalId() : string
    {
        return 'GILA_851a';
    }

    public static function create()
    {
        return new Card\GILA_851a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_851a';
    }
}