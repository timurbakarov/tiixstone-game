<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PompousThespian
{
    public static function globalId() : string
    {
        return 'KAR_011';
    }

    public static function create()
    {
        return new Card\KAR_011;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_011';
    }
}