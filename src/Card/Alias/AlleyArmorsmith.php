<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AlleyArmorsmith
{
    public static function globalId() : string
    {
        return 'CFM_756';
    }

    public static function create()
    {
        return new Card\CFM_756;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_756';
    }
}