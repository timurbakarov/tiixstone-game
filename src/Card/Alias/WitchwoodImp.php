<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WitchwoodImp
{
    public static function globalId() : string
    {
        return 'GIL_608';
    }

    public static function create()
    {
        return new Card\GIL_608;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_608';
    }
}