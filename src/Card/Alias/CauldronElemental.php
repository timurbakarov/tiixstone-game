<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CauldronElemental
{
    public static function globalId() : string
    {
        return 'GIL_119';
    }

    public static function create()
    {
        return new Card\GIL_119;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_119';
    }
}