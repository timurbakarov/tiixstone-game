<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BoneguardLieutenant
{
    public static function globalId() : string
    {
        return 'AT_089';
    }

    public static function create()
    {
        return new Card\AT_089;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_089';
    }
}