<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShakuTheCollector
{
    public static function globalId() : string
    {
        return 'CFM_781';
    }

    public static function create()
    {
        return new Card\CFM_781;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_781';
    }
}