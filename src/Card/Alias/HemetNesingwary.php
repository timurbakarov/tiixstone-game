<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HemetNesingwary
{
    public static function globalId() : string
    {
        return 'TU4d_001';
    }

    public static function create()
    {
        return new Card\TU4d_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TU4d_001';
    }
}