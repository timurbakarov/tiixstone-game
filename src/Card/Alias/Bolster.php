<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Bolster
{
    public static function globalId() : string
    {
        return 'AT_068';
    }

    public static function create()
    {
        return new Card\AT_068;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_068';
    }
}