<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Adaptation
{
    public static function globalId() : string
    {
        return 'UNG_961';
    }

    public static function create()
    {
        return new Card\UNG_961;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_961';
    }
}