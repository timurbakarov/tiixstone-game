<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ForbiddenShaping
{
    public static function globalId() : string
    {
        return 'OG_101';
    }

    public static function create()
    {
        return new Card\OG_101;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_101';
    }
}