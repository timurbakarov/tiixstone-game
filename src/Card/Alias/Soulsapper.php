<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Soulsapper
{
    public static function globalId() : string
    {
        return 'TRLA_152';
    }

    public static function create()
    {
        return new Card\TRLA_152;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_152';
    }
}