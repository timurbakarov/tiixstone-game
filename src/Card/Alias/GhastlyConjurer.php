<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GhastlyConjurer
{
    public static function globalId() : string
    {
        return 'ICC_069';
    }

    public static function create()
    {
        return new Card\ICC_069;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_069';
    }
}