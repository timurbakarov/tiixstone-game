<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TerestianIllhoof
{
    public static function globalId() : string
    {
        return 'KARA_09_01heroic';
    }

    public static function create()
    {
        return new Card\KARA_09_01heroic;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_09_01heroic';
    }
}