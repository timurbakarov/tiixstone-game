<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SergeantSally
{
    public static function globalId() : string
    {
        return 'CFM_341';
    }

    public static function create()
    {
        return new Card\CFM_341;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_341';
    }
}