<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EmbraceDarkness
{
    public static function globalId() : string
    {
        return 'ICC_849';
    }

    public static function create()
    {
        return new Card\ICC_849;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_849';
    }
}