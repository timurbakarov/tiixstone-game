<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HandOfProtection
{
    public static function globalId() : string
    {
        return 'EX1_371';
    }

    public static function create()
    {
        return new Card\EX1_371;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_371';
    }
}