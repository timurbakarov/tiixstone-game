<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PartyPortal
{
    public static function globalId() : string
    {
        return 'TB_KaraPortal_001';
    }

    public static function create()
    {
        return new Card\TB_KaraPortal_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_KaraPortal_001';
    }
}