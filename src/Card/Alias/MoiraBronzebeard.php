<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MoiraBronzebeard
{
    public static function globalId() : string
    {
        return 'BRMC_87';
    }

    public static function create()
    {
        return new Card\BRMC_87;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_87';
    }
}