<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DjinniOfZephyrs
{
    public static function globalId() : string
    {
        return 'LOE_053';
    }

    public static function create()
    {
        return new Card\LOE_053;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_053';
    }
}