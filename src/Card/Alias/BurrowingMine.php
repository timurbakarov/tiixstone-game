<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BurrowingMine
{
    public static function globalId() : string
    {
        return 'GVG_056t';
    }

    public static function create()
    {
        return new Card\GVG_056t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_056t';
    }
}