<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MogorTheOgre
{
    public static function globalId() : string
    {
        return 'GVG_112';
    }

    public static function create()
    {
        return new Card\GVG_112;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_112';
    }
}