<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GarrisonCommander
{
    public static function globalId() : string
    {
        return 'AT_080';
    }

    public static function create()
    {
        return new Card\AT_080;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_080';
    }
}