<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MaievShadowsong
{
    public static function globalId() : string
    {
        return 'HERO_03a';
    }

    public static function create()
    {
        return new Card\HERO_03a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HERO_03a';
    }
}