<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheFanFavorite
{
    public static function globalId() : string
    {
        return 'TRLA_125';
    }

    public static function create()
    {
        return new Card\TRLA_125;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_125';
    }
}