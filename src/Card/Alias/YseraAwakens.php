<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class YseraAwakens
{
    public static function globalId() : string
    {
        return 'DREAM_02';
    }

    public static function create()
    {
        return new Card\DREAM_02;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\DREAM_02';
    }
}