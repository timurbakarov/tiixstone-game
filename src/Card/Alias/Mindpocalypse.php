<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Mindpocalypse
{
    public static function globalId() : string
    {
        return 'NAX5_03';
    }

    public static function create()
    {
        return new Card\NAX5_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX5_03';
    }
}