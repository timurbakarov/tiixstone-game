<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KabalChemist
{
    public static function globalId() : string
    {
        return 'CFM_619';
    }

    public static function create()
    {
        return new Card\CFM_619;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_619';
    }
}