<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SwampLeech
{
    public static function globalId() : string
    {
        return 'GIL_558';
    }

    public static function create()
    {
        return new Card\GIL_558;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_558';
    }
}