<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BarnabusTheStomper
{
    public static function globalId() : string
    {
        return 'UNG_116t';
    }

    public static function create()
    {
        return new Card\UNG_116t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_116t';
    }
}