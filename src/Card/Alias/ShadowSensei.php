<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShadowSensei
{
    public static function globalId() : string
    {
        return 'CFM_694';
    }

    public static function create()
    {
        return new Card\CFM_694;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_694';
    }
}