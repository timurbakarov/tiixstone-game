<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ReliquarySeeker
{
    public static function globalId() : string
    {
        return 'LOE_116';
    }

    public static function create()
    {
        return new Card\LOE_116;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_116';
    }
}