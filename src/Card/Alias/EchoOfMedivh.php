<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EchoOfMedivh
{
    public static function globalId() : string
    {
        return 'GVG_005';
    }

    public static function create()
    {
        return new Card\GVG_005;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_005';
    }
}