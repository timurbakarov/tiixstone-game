<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JonBankard
{
    public static function globalId() : string
    {
        return 'CRED_43';
    }

    public static function create()
    {
        return new Card\CRED_43;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_43';
    }
}