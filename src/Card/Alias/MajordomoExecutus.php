<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MajordomoExecutus
{
    public static function globalId() : string
    {
        return 'BRM_027';
    }

    public static function create()
    {
        return new Card\BRM_027;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_027';
    }
}