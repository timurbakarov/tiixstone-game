<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShutUpPriest
{
    public static function globalId() : string
    {
        return 'ICCA08_029';
    }

    public static function create()
    {
        return new Card\ICCA08_029;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA08_029';
    }
}