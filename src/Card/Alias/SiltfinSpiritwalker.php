<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SiltfinSpiritwalker
{
    public static function globalId() : string
    {
        return 'GVG_040';
    }

    public static function create()
    {
        return new Card\GVG_040;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_040';
    }
}