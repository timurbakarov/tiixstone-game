<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SmugglersCrate
{
    public static function globalId() : string
    {
        return 'CFM_334';
    }

    public static function create()
    {
        return new Card\CFM_334;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_334';
    }
}