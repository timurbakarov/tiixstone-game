<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CrystallineOracle
{
    public static function globalId() : string
    {
        return 'UNG_032';
    }

    public static function create()
    {
        return new Card\UNG_032;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_032';
    }
}