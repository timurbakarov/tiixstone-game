<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Restart
{
    public static function globalId() : string
    {
        return 'TB_LethalSetup001b';
    }

    public static function create()
    {
        return new Card\TB_LethalSetup001b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_LethalSetup001b';
    }
}