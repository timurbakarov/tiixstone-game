<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FlameBurst
{
    public static function globalId() : string
    {
        return 'TU4e_005';
    }

    public static function create()
    {
        return new Card\TU4e_005;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TU4e_005';
    }
}