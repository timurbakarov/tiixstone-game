<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class UndercityHuckster
{
    public static function globalId() : string
    {
        return 'OG_330';
    }

    public static function create()
    {
        return new Card\OG_330;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_330';
    }
}