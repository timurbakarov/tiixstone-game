<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PrinceValanar
{
    public static function globalId() : string
    {
        return 'ICC_853';
    }

    public static function create()
    {
        return new Card\ICC_853;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_853';
    }
}