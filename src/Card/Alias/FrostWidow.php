<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FrostWidow
{
    public static function globalId() : string
    {
        return 'ICC_832t3';
    }

    public static function create()
    {
        return new Card\ICC_832t3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_832t3';
    }
}