<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VioletIllusionist
{
    public static function globalId() : string
    {
        return 'KAR_712';
    }

    public static function create()
    {
        return new Card\KAR_712;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_712';
    }
}