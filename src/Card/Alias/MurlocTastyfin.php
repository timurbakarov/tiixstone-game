<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MurlocTastyfin
{
    public static function globalId() : string
    {
        return 'TRL_520';
    }

    public static function create()
    {
        return new Card\TRL_520;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_520';
    }
}