<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SeabreakerGoliath
{
    public static function globalId() : string
    {
        return 'GILA_612';
    }

    public static function create()
    {
        return new Card\GILA_612;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_612';
    }
}