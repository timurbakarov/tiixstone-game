<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JanalaisFlame
{
    public static function globalId() : string
    {
        return 'TRLA_128t';
    }

    public static function create()
    {
        return new Card\TRLA_128t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_128t';
    }
}