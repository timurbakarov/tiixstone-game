<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NetherspiteHistorian
{
    public static function globalId() : string
    {
        return 'KAR_062';
    }

    public static function create()
    {
        return new Card\KAR_062;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_062';
    }
}