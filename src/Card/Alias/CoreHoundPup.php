<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CoreHoundPup
{
    public static function globalId() : string
    {
        return 'BRMC_95he';
    }

    public static function create()
    {
        return new Card\BRMC_95he;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_95he';
    }
}