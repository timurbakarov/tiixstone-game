<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IceClaw
{
    public static function globalId() : string
    {
        return 'ICCA04_011p';
    }

    public static function create()
    {
        return new Card\ICCA04_011p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA04_011p';
    }
}