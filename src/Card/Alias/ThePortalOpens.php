<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ThePortalOpens
{
    public static function globalId() : string
    {
        return 'TB_SPT_DPromoSpellPortal';
    }

    public static function create()
    {
        return new Card\TB_SPT_DPromoSpellPortal;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_SPT_DPromoSpellPortal';
    }
}