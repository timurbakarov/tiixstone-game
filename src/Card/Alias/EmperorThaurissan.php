<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EmperorThaurissan
{
    public static function globalId() : string
    {
        return 'BRM_028';
    }

    public static function create()
    {
        return new Card\BRM_028;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_028';
    }
}