<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Bulldoze
{
    public static function globalId() : string
    {
        return 'GILA_BOSS_42p';
    }

    public static function create()
    {
        return new Card\GILA_BOSS_42p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_BOSS_42p';
    }
}