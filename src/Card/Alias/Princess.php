<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Princess
{
    public static function globalId() : string
    {
        return 'GILA_411';
    }

    public static function create()
    {
        return new Card\GILA_411;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_411';
    }
}