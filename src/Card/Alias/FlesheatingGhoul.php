<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FlesheatingGhoul
{
    public static function globalId() : string
    {
        return 'tt_004';
    }

    public static function create()
    {
        return new Card\tt_004;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\tt_004';
    }
}