<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DragonConsort
{
    public static function globalId() : string
    {
        return 'BRM_018';
    }

    public static function create()
    {
        return new Card\BRM_018;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_018';
    }
}