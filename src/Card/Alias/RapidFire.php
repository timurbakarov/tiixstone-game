<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RapidFire
{
    public static function globalId() : string
    {
        return 'GILA_594';
    }

    public static function create()
    {
        return new Card\GILA_594;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_594';
    }
}