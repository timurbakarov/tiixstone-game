<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Nightbane
{
    public static function globalId() : string
    {
        return 'KARA_11_01heroic';
    }

    public static function create()
    {
        return new Card\KARA_11_01heroic;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_11_01heroic';
    }
}