<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CorruptedEgg
{
    public static function globalId() : string
    {
        return 'BRMA10_4H';
    }

    public static function create()
    {
        return new Card\BRMA10_4H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA10_4H';
    }
}