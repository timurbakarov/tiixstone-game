<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OminousFog
{
    public static function globalId() : string
    {
        return 'GILA_BOSS_61t3';
    }

    public static function create()
    {
        return new Card\GILA_BOSS_61t3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_BOSS_61t3';
    }
}