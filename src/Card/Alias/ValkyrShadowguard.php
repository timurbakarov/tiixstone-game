<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ValkyrShadowguard
{
    public static function globalId() : string
    {
        return 'ICCA08_017';
    }

    public static function create()
    {
        return new Card\ICCA08_017;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA08_017';
    }
}