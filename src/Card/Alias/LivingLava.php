<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LivingLava
{
    public static function globalId() : string
    {
        return 'BRMC_90';
    }

    public static function create()
    {
        return new Card\BRMC_90;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_90';
    }
}