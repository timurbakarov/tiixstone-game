<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OnyxBishop
{
    public static function globalId() : string
    {
        return 'KAR_204';
    }

    public static function create()
    {
        return new Card\KAR_204;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_204';
    }
}