<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Tracking
{
    public static function globalId() : string
    {
        return 'DS1_184';
    }

    public static function create()
    {
        return new Card\DS1_184;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\DS1_184';
    }
}