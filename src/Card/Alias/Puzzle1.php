<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Puzzle1
{
    public static function globalId() : string
    {
        return 'TB_Lethal002';
    }

    public static function create()
    {
        return new Card\TB_Lethal002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_Lethal002';
    }
}