<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GroveTender
{
    public static function globalId() : string
    {
        return 'GVG_032';
    }

    public static function create()
    {
        return new Card\GVG_032;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_032';
    }
}