<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GrievousBite
{
    public static function globalId() : string
    {
        return 'UNG_910';
    }

    public static function create()
    {
        return new Card\UNG_910;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_910';
    }
}