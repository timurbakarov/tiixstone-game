<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SacredTrial
{
    public static function globalId() : string
    {
        return 'LOE_027';
    }

    public static function create()
    {
        return new Card\LOE_027;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_027';
    }
}