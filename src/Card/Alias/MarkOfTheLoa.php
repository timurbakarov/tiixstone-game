<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MarkOfTheLoa
{
    public static function globalId() : string
    {
        return 'TRL_254';
    }

    public static function create()
    {
        return new Card\TRL_254;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_254';
    }
}