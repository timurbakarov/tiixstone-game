<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SeadevilStinger
{
    public static function globalId() : string
    {
        return 'CFM_699';
    }

    public static function create()
    {
        return new Card\CFM_699;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_699';
    }
}