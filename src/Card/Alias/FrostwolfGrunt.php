<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FrostwolfGrunt
{
    public static function globalId() : string
    {
        return 'CS2_121';
    }

    public static function create()
    {
        return new Card\CS2_121;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_121';
    }
}