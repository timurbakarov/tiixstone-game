<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PowerOfTheHorde
{
    public static function globalId() : string
    {
        return 'PRO_001c';
    }

    public static function create()
    {
        return new Card\PRO_001c;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\PRO_001c';
    }
}