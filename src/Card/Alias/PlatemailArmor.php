<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PlatemailArmor
{
    public static function globalId() : string
    {
        return 'LOEA14_2';
    }

    public static function create()
    {
        return new Card\LOEA14_2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA14_2';
    }
}