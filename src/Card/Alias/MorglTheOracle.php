<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MorglTheOracle
{
    public static function globalId() : string
    {
        return 'HERO_02a';
    }

    public static function create()
    {
        return new Card\HERO_02a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HERO_02a';
    }
}