<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SurrenderToMadness
{
    public static function globalId() : string
    {
        return 'TRL_500';
    }

    public static function create()
    {
        return new Card\TRL_500;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_500';
    }
}