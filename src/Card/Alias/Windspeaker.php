<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Windspeaker
{
    public static function globalId() : string
    {
        return 'EX1_587';
    }

    public static function create()
    {
        return new Card\EX1_587;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_587';
    }
}