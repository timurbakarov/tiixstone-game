<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VirmenSensei
{
    public static function globalId() : string
    {
        return 'CFM_816';
    }

    public static function create()
    {
        return new Card\CFM_816;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_816';
    }
}