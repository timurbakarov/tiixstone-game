<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class UnpoweredSteambot
{
    public static function globalId() : string
    {
        return 'GIL_809';
    }

    public static function create()
    {
        return new Card\GIL_809;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_809';
    }
}