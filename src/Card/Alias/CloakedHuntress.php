<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CloakedHuntress
{
    public static function globalId() : string
    {
        return 'KAR_006';
    }

    public static function create()
    {
        return new Card\KAR_006;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_006';
    }
}