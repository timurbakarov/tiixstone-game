<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LotusIllusionist
{
    public static function globalId() : string
    {
        return 'CFM_697';
    }

    public static function create()
    {
        return new Card\CFM_697;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_697';
    }
}