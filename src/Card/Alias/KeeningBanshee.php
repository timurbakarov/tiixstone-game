<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KeeningBanshee
{
    public static function globalId() : string
    {
        return 'ICC_911';
    }

    public static function create()
    {
        return new Card\ICC_911;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_911';
    }
}