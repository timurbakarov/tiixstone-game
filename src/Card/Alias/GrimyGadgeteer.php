<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GrimyGadgeteer
{
    public static function globalId() : string
    {
        return 'CFM_754';
    }

    public static function create()
    {
        return new Card\CFM_754;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_754';
    }
}