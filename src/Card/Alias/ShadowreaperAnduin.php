<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShadowreaperAnduin
{
    public static function globalId() : string
    {
        return 'ICC_830';
    }

    public static function create()
    {
        return new Card\ICC_830;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_830';
    }
}