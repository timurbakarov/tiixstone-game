<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Shadowfiend
{
    public static function globalId() : string
    {
        return 'AT_014';
    }

    public static function create()
    {
        return new Card\AT_014;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_014';
    }
}