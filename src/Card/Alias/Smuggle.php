<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Smuggle
{
    public static function globalId() : string
    {
        return 'TB_BossRumble_001hp';
    }

    public static function create()
    {
        return new Card\TB_BossRumble_001hp;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_BossRumble_001hp';
    }
}