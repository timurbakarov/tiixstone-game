<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TouchIt
{
    public static function globalId() : string
    {
        return 'LOEA04_29a';
    }

    public static function create()
    {
        return new Card\LOEA04_29a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA04_29a';
    }
}