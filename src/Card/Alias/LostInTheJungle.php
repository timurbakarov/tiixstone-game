<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LostInTheJungle
{
    public static function globalId() : string
    {
        return 'UNG_960';
    }

    public static function create()
    {
        return new Card\UNG_960;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_960';
    }
}