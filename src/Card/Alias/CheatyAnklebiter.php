<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CheatyAnklebiter
{
    public static function globalId() : string
    {
        return 'TRL_512';
    }

    public static function create()
    {
        return new Card\TRL_512;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_512';
    }
}