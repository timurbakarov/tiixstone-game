<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OLDN3wbHealer
{
    public static function globalId() : string
    {
        return 'TBST_003';
    }

    public static function create()
    {
        return new Card\TBST_003;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TBST_003';
    }
}