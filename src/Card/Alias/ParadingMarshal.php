<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ParadingMarshal
{
    public static function globalId() : string
    {
        return 'TRLA_139';
    }

    public static function create()
    {
        return new Card\TRLA_139;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_139';
    }
}