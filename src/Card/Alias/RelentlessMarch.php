<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RelentlessMarch
{
    public static function globalId() : string
    {
        return 'FB_LK002';
    }

    public static function create()
    {
        return new Card\FB_LK002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK002';
    }
}