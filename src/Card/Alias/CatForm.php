<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CatForm
{
    public static function globalId() : string
    {
        return 'EX1_165a';
    }

    public static function create()
    {
        return new Card\EX1_165a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_165a';
    }
}