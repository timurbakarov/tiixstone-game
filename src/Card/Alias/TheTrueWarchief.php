<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheTrueWarchief
{
    public static function globalId() : string
    {
        return 'BRMA09_6';
    }

    public static function create()
    {
        return new Card\BRMA09_6;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA09_6';
    }
}