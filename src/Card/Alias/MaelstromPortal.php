<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MaelstromPortal
{
    public static function globalId() : string
    {
        return 'KAR_073';
    }

    public static function create()
    {
        return new Card\KAR_073;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_073';
    }
}