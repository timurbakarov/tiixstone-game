<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MoshOggEnforcer
{
    public static function globalId() : string
    {
        return 'TRL_513';
    }

    public static function create()
    {
        return new Card\TRL_513;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_513';
    }
}