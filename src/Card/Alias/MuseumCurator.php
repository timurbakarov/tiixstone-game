<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MuseumCurator
{
    public static function globalId() : string
    {
        return 'LOE_006';
    }

    public static function create()
    {
        return new Card\LOE_006;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_006';
    }
}