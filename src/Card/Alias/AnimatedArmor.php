<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AnimatedArmor
{
    public static function globalId() : string
    {
        return 'LOE_119';
    }

    public static function create()
    {
        return new Card\LOE_119;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_119';
    }
}