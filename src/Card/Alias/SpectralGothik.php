<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpectralGothik
{
    public static function globalId() : string
    {
        return 'TB_KTRAF_4m';
    }

    public static function create()
    {
        return new Card\TB_KTRAF_4m;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_KTRAF_4m';
    }
}