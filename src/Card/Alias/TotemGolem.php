<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TotemGolem
{
    public static function globalId() : string
    {
        return 'AT_052';
    }

    public static function create()
    {
        return new Card\AT_052;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_052';
    }
}