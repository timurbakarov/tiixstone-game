<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShifterZerus
{
    public static function globalId() : string
    {
        return 'OG_123';
    }

    public static function create()
    {
        return new Card\OG_123;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_123';
    }
}