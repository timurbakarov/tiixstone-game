<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MilitiaCommander
{
    public static function globalId() : string
    {
        return 'GIL_803';
    }

    public static function create()
    {
        return new Card\GIL_803;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_803';
    }
}