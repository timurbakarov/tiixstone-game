<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NightHowler
{
    public static function globalId() : string
    {
        return 'ICC_031';
    }

    public static function create()
    {
        return new Card\ICC_031;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_031';
    }
}