<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Linecracker
{
    public static function globalId() : string
    {
        return 'TRL_528';
    }

    public static function create()
    {
        return new Card\TRL_528;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_528';
    }
}