<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JungleMoonkin
{
    public static function globalId() : string
    {
        return 'LOE_051';
    }

    public static function create()
    {
        return new Card\LOE_051;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_051';
    }
}