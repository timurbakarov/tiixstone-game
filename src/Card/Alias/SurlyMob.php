<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SurlyMob
{
    public static function globalId() : string
    {
        return 'GILA_821a';
    }

    public static function create()
    {
        return new Card\GILA_821a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_821a';
    }
}