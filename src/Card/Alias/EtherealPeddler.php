<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EtherealPeddler
{
    public static function globalId() : string
    {
        return 'KAR_070';
    }

    public static function create()
    {
        return new Card\KAR_070;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_070';
    }
}