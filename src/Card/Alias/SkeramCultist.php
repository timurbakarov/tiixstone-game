<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SkeramCultist
{
    public static function globalId() : string
    {
        return 'OG_339';
    }

    public static function create()
    {
        return new Card\OG_339;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_339';
    }
}