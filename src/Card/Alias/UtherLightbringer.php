<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class UtherLightbringer
{
    public static function globalId() : string
    {
        return 'HERO_04';
    }

    public static function create()
    {
        return new Card\HERO_04;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HERO_04';
    }
}