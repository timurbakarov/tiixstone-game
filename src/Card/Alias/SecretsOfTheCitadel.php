<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SecretsOfTheCitadel
{
    public static function globalId() : string
    {
        return 'ICCA03_001';
    }

    public static function create()
    {
        return new Card\ICCA03_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA03_001';
    }
}