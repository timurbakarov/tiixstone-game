<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TaintedZealot
{
    public static function globalId() : string
    {
        return 'ICC_913';
    }

    public static function create()
    {
        return new Card\ICC_913;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_913';
    }
}