<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SkeletalKnight
{
    public static function globalId() : string
    {
        return 'ICCA11_001';
    }

    public static function create()
    {
        return new Card\ICCA11_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA11_001';
    }
}