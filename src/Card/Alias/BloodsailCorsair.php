<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodsailCorsair
{
    public static function globalId() : string
    {
        return 'NEW1_025';
    }

    public static function create()
    {
        return new Card\NEW1_025;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NEW1_025';
    }
}