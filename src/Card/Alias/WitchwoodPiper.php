<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WitchwoodPiper
{
    public static function globalId() : string
    {
        return 'GIL_584';
    }

    public static function create()
    {
        return new Card\GIL_584;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_584';
    }
}