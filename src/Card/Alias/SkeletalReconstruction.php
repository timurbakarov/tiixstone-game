<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SkeletalReconstruction
{
    public static function globalId() : string
    {
        return 'ICCA06_002p';
    }

    public static function create()
    {
        return new Card\ICCA06_002p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA06_002p';
    }
}