<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FlameElemental
{
    public static function globalId() : string
    {
        return 'UNG_809t1';
    }

    public static function create()
    {
        return new Card\UNG_809t1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_809t1';
    }
}