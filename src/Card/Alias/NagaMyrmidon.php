<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NagaMyrmidon
{
    public static function globalId() : string
    {
        return 'TU4e_003';
    }

    public static function create()
    {
        return new Card\TU4e_003;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TU4e_003';
    }
}