<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TroggHateSpells
{
    public static function globalId() : string
    {
        return 'LOEA05_03';
    }

    public static function create()
    {
        return new Card\LOEA05_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA05_03';
    }
}