<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodTrollSapper
{
    public static function globalId() : string
    {
        return 'TRL_257';
    }

    public static function create()
    {
        return new Card\TRL_257;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_257';
    }
}