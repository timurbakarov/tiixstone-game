<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CallMediva
{
    public static function globalId() : string
    {
        return 'TB_KaraPortal_002';
    }

    public static function create()
    {
        return new Card\TB_KaraPortal_002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_KaraPortal_002';
    }
}