<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SoulTap
{
    public static function globalId() : string
    {
        return 'AT_132_WARLOCK';
    }

    public static function create()
    {
        return new Card\AT_132_WARLOCK;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_132_WARLOCK';
    }
}