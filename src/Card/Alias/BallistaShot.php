<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BallistaShot
{
    public static function globalId() : string
    {
        return 'DS1h_292_H1_AT_132';
    }

    public static function create()
    {
        return new Card\DS1h_292_H1_AT_132;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\DS1h_292_H1_AT_132';
    }
}