<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Shudderwock
{
    public static function globalId() : string
    {
        return 'GIL_820';
    }

    public static function create()
    {
        return new Card\GIL_820;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_820';
    }
}