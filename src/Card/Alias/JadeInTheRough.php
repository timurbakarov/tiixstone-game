<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JadeInTheRough
{
    public static function globalId() : string
    {
        return 'GILA_599';
    }

    public static function create()
    {
        return new Card\GILA_599;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_599';
    }
}