<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FrigidSnobold
{
    public static function globalId() : string
    {
        return 'AT_093';
    }

    public static function create()
    {
        return new Card\AT_093;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_093';
    }
}