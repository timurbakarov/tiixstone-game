<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Flamecannon
{
    public static function globalId() : string
    {
        return 'GVG_001';
    }

    public static function create()
    {
        return new Card\GVG_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_001';
    }
}