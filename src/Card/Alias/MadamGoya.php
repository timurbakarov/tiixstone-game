<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MadamGoya
{
    public static function globalId() : string
    {
        return 'CFM_672';
    }

    public static function create()
    {
        return new Card\CFM_672;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_672';
    }
}