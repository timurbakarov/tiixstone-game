<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PlayDead
{
    public static function globalId() : string
    {
        return 'ICC_052';
    }

    public static function create()
    {
        return new Card\ICC_052;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_052';
    }
}