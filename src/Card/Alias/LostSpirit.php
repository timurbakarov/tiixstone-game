<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LostSpirit
{
    public static function globalId() : string
    {
        return 'GIL_513';
    }

    public static function create()
    {
        return new Card\GIL_513;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_513';
    }
}