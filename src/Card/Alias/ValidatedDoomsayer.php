<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ValidatedDoomsayer
{
    public static function globalId() : string
    {
        return 'OG_200';
    }

    public static function create()
    {
        return new Card\OG_200;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_200';
    }
}