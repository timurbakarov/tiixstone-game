<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Gahzrilla
{
    public static function globalId() : string
    {
        return 'GVG_049';
    }

    public static function create()
    {
        return new Card\GVG_049;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_049';
    }
}