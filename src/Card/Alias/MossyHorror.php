<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MossyHorror
{
    public static function globalId() : string
    {
        return 'GIL_124';
    }

    public static function create()
    {
        return new Card\GIL_124;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_124';
    }
}