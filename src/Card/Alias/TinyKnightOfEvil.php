<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TinyKnightOfEvil
{
    public static function globalId() : string
    {
        return 'AT_021';
    }

    public static function create()
    {
        return new Card\AT_021;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_021';
    }
}