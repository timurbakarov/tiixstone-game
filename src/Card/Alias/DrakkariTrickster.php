<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DrakkariTrickster
{
    public static function globalId() : string
    {
        return 'TRL_527';
    }

    public static function create()
    {
        return new Card\TRL_527;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_527';
    }
}