<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AndyBrock
{
    public static function globalId() : string
    {
        return 'CRED_15';
    }

    public static function create()
    {
        return new Card\CRED_15;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_15';
    }
}