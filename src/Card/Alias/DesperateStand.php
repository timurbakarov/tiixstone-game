<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DesperateStand
{
    public static function globalId() : string
    {
        return 'ICC_244';
    }

    public static function create()
    {
        return new Card\ICC_244;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_244';
    }
}