<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FerociousHowl
{
    public static function globalId() : string
    {
        return 'GIL_637';
    }

    public static function create()
    {
        return new Card\GIL_637;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_637';
    }
}