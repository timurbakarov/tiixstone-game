<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HireekTheBat
{
    public static function globalId() : string
    {
        return 'TRL_253';
    }

    public static function create()
    {
        return new Card\TRL_253;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_253';
    }
}