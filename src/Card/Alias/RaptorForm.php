<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RaptorForm
{
    public static function globalId() : string
    {
        return 'UNG_101a';
    }

    public static function create()
    {
        return new Card\UNG_101a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_101a';
    }
}