<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ChiefInspector
{
    public static function globalId() : string
    {
        return 'GIL_648';
    }

    public static function create()
    {
        return new Card\GIL_648;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_648';
    }
}