<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SwingAcross
{
    public static function globalId() : string
    {
        return 'LOEA04_06a';
    }

    public static function create()
    {
        return new Card\LOEA04_06a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA04_06a';
    }
}