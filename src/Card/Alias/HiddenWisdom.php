<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HiddenWisdom
{
    public static function globalId() : string
    {
        return 'GIL_903';
    }

    public static function create()
    {
        return new Card\GIL_903;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_903';
    }
}