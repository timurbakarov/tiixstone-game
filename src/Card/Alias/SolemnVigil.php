<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SolemnVigil
{
    public static function globalId() : string
    {
        return 'BRM_001';
    }

    public static function create()
    {
        return new Card\BRM_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_001';
    }
}