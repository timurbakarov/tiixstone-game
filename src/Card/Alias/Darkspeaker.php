<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Darkspeaker
{
    public static function globalId() : string
    {
        return 'OG_102';
    }

    public static function create()
    {
        return new Card\OG_102;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_102';
    }
}