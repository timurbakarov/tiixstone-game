<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Resourceful
{
    public static function globalId() : string
    {
        return 'GILA_593';
    }

    public static function create()
    {
        return new Card\GILA_593;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_593';
    }
}