<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KoboldLibrarian
{
    public static function globalId() : string
    {
        return 'LOOT_014';
    }

    public static function create()
    {
        return new Card\LOOT_014;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOOT_014';
    }
}