<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GattlingGunner
{
    public static function globalId() : string
    {
        return 'GILA_801';
    }

    public static function create()
    {
        return new Card\GILA_801;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_801';
    }
}