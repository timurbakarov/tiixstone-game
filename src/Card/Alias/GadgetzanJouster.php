<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GadgetzanJouster
{
    public static function globalId() : string
    {
        return 'AT_133';
    }

    public static function create()
    {
        return new Card\AT_133;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_133';
    }
}