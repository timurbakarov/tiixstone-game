<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TwilightDrake
{
    public static function globalId() : string
    {
        return 'EX1_043';
    }

    public static function create()
    {
        return new Card\EX1_043;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_043';
    }
}