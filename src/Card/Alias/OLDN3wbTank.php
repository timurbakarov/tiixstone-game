<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OLDN3wbTank
{
    public static function globalId() : string
    {
        return 'TBST_001';
    }

    public static function create()
    {
        return new Card\TBST_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TBST_001';
    }
}