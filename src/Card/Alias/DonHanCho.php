<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DonHanCho
{
    public static function globalId() : string
    {
        return 'CFM_685';
    }

    public static function create()
    {
        return new Card\CFM_685;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_685';
    }
}