<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FoeReaper4000
{
    public static function globalId() : string
    {
        return 'GVG_113';
    }

    public static function create()
    {
        return new Card\GVG_113;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_113';
    }
}