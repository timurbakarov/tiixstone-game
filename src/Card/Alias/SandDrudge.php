<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SandDrudge
{
    public static function globalId() : string
    {
        return 'TRL_131';
    }

    public static function create()
    {
        return new Card\TRL_131;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_131';
    }
}