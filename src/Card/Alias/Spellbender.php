<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Spellbender
{
    public static function globalId() : string
    {
        return 'tt_010';
    }

    public static function create()
    {
        return new Card\tt_010;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\tt_010';
    }
}