<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GrimNecromancer
{
    public static function globalId() : string
    {
        return 'ICC_026';
    }

    public static function create()
    {
        return new Card\ICC_026;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_026';
    }
}