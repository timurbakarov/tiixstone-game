<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EadricThePure
{
    public static function globalId() : string
    {
        return 'AT_081';
    }

    public static function create()
    {
        return new Card\AT_081;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_081';
    }
}