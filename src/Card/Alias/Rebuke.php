<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Rebuke
{
    public static function globalId() : string
    {
        return 'GIL_203';
    }

    public static function create()
    {
        return new Card\GIL_203;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_203';
    }
}