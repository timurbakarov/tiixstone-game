<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Rogue
{
    public static function globalId() : string
    {
        return 'FB_LK_Rogue_copy';
    }

    public static function create()
    {
        return new Card\FB_LK_Rogue_copy;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK_Rogue_copy';
    }
}