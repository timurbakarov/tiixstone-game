<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DruidOfTheScythe
{
    public static function globalId() : string
    {
        return 'GIL_188';
    }

    public static function create()
    {
        return new Card\GIL_188;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_188';
    }
}