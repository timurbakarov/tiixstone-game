<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Leokk
{
    public static function globalId() : string
    {
        return 'NEW1_033';
    }

    public static function create()
    {
        return new Card\NEW1_033;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NEW1_033';
    }
}