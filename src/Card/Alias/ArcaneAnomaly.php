<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArcaneAnomaly
{
    public static function globalId() : string
    {
        return 'KAR_036';
    }

    public static function create()
    {
        return new Card\KAR_036;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_036';
    }
}