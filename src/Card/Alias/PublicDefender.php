<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PublicDefender
{
    public static function globalId() : string
    {
        return 'CFM_300';
    }

    public static function create()
    {
        return new Card\CFM_300;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_300';
    }
}