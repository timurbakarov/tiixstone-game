<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PlagueOfMurlocs
{
    public static function globalId() : string
    {
        return 'GILA_BOSS_37t';
    }

    public static function create()
    {
        return new Card\GILA_BOSS_37t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_BOSS_37t';
    }
}