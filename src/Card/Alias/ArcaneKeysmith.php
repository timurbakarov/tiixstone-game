<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArcaneKeysmith
{
    public static function globalId() : string
    {
        return 'GIL_116';
    }

    public static function create()
    {
        return new Card\GIL_116;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_116';
    }
}