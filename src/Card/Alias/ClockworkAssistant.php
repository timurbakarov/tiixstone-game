<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ClockworkAssistant
{
    public static function globalId() : string
    {
        return 'GILA_907';
    }

    public static function create()
    {
        return new Card\GILA_907;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_907';
    }
}