<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ScarabForm
{
    public static function globalId() : string
    {
        return 'ICC_051b';
    }

    public static function create()
    {
        return new Card\ICC_051b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_051b';
    }
}