<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FireguardDestroyer
{
    public static function globalId() : string
    {
        return 'BRM_012';
    }

    public static function create()
    {
        return new Card\BRM_012;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_012';
    }
}