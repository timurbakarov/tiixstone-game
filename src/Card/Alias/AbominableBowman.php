<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AbominableBowman
{
    public static function globalId() : string
    {
        return 'ICC_825';
    }

    public static function create()
    {
        return new Card\ICC_825;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_825';
    }
}