<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PrimalfinTotem
{
    public static function globalId() : string
    {
        return 'UNG_201';
    }

    public static function create()
    {
        return new Card\UNG_201;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_201';
    }
}