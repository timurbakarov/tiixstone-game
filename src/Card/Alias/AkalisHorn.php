<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AkalisHorn
{
    public static function globalId() : string
    {
        return 'TRLA_171';
    }

    public static function create()
    {
        return new Card\TRLA_171;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_171';
    }
}