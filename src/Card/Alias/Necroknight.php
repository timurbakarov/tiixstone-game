<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Necroknight
{
    public static function globalId() : string
    {
        return 'NAXM_001';
    }

    public static function create()
    {
        return new Card\NAXM_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAXM_001';
    }
}