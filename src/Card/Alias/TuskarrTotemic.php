<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TuskarrTotemic
{
    public static function globalId() : string
    {
        return 'AT_046';
    }

    public static function create()
    {
        return new Card\AT_046;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_046';
    }
}