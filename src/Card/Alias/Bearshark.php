<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Bearshark
{
    public static function globalId() : string
    {
        return 'ICC_419';
    }

    public static function create()
    {
        return new Card\ICC_419;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_419';
    }
}