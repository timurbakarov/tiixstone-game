<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Brawl
{
    public static function globalId() : string
    {
        return 'EX1_407';
    }

    public static function create()
    {
        return new Card\EX1_407;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_407';
    }
}