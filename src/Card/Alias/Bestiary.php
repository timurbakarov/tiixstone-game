<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Bestiary
{
    public static function globalId() : string
    {
        return 'GILA_812';
    }

    public static function create()
    {
        return new Card\GILA_812;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_812';
    }
}