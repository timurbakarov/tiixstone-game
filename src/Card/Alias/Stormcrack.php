<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Stormcrack
{
    public static function globalId() : string
    {
        return 'OG_206';
    }

    public static function create()
    {
        return new Card\OG_206;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_206';
    }
}