<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ImpetuousCompanion
{
    public static function globalId() : string
    {
        return 'GILA_906';
    }

    public static function create()
    {
        return new Card\GILA_906;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_906';
    }
}