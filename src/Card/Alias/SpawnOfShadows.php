<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpawnOfShadows
{
    public static function globalId() : string
    {
        return 'AT_012';
    }

    public static function create()
    {
        return new Card\AT_012;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_012';
    }
}