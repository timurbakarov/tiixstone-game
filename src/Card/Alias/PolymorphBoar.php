<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PolymorphBoar
{
    public static function globalId() : string
    {
        return 'AT_005';
    }

    public static function create()
    {
        return new Card\AT_005;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_005';
    }
}