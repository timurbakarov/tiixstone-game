<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AnduinWrynn
{
    public static function globalId() : string
    {
        return 'HERO_09';
    }

    public static function create()
    {
        return new Card\HERO_09;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HERO_09';
    }
}