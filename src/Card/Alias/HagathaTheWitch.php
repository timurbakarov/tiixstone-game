<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HagathaTheWitch
{
    public static function globalId() : string
    {
        return 'GIL_504';
    }

    public static function create()
    {
        return new Card\GIL_504;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_504';
    }
}