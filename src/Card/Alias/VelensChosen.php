<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VelensChosen
{
    public static function globalId() : string
    {
        return 'GVG_010';
    }

    public static function create()
    {
        return new Card\GVG_010;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_010';
    }
}