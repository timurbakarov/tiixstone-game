<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShadowWordPain
{
    public static function globalId() : string
    {
        return 'CS2_234';
    }

    public static function create()
    {
        return new Card\CS2_234;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_234';
    }
}