<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EssenceOfTheRed
{
    public static function globalId() : string
    {
        return 'BRMA11_2H_2_TB';
    }

    public static function create()
    {
        return new Card\BRMA11_2H_2_TB;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA11_2H_2_TB';
    }
}