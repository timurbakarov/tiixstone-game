<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodToIchor
{
    public static function globalId() : string
    {
        return 'OG_314';
    }

    public static function create()
    {
        return new Card\OG_314;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_314';
    }
}