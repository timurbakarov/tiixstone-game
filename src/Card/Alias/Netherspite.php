<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Netherspite
{
    public static function globalId() : string
    {
        return 'KARA_08_01H';
    }

    public static function create()
    {
        return new Card\KARA_08_01H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_08_01H';
    }
}