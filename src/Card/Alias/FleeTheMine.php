<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FleeTheMine
{
    public static function globalId() : string
    {
        return 'LOEA07_03h';
    }

    public static function create()
    {
        return new Card\LOEA07_03h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA07_03h';
    }
}