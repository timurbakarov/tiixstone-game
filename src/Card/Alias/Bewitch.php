<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Bewitch
{
    public static function globalId() : string
    {
        return 'GIL_504h';
    }

    public static function create()
    {
        return new Card\GIL_504h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_504h';
    }
}