<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Evolve
{
    public static function globalId() : string
    {
        return 'OG_027';
    }

    public static function create()
    {
        return new Card\OG_027;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_027';
    }
}