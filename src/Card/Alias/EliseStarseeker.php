<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EliseStarseeker
{
    public static function globalId() : string
    {
        return 'LOE_079';
    }

    public static function create()
    {
        return new Card\LOE_079;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_079';
    }
}