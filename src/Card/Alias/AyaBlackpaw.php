<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AyaBlackpaw
{
    public static function globalId() : string
    {
        return 'TB_BossRumble_002';
    }

    public static function create()
    {
        return new Card\TB_BossRumble_002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_BossRumble_002';
    }
}