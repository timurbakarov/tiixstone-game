<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Oondasta
{
    public static function globalId() : string
    {
        return 'TRL_542';
    }

    public static function create()
    {
        return new Card\TRL_542;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_542';
    }
}