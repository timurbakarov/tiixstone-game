<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CapturedJormungar
{
    public static function globalId() : string
    {
        return 'AT_102';
    }

    public static function create()
    {
        return new Card\AT_102;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_102';
    }
}