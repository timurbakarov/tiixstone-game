<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Squashling
{
    public static function globalId() : string
    {
        return 'GIL_835';
    }

    public static function create()
    {
        return new Card\GIL_835;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_835';
    }
}