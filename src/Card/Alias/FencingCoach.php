<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FencingCoach
{
    public static function globalId() : string
    {
        return 'AT_115';
    }

    public static function create()
    {
        return new Card\AT_115;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_115';
    }
}