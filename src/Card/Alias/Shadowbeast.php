<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Shadowbeast
{
    public static function globalId() : string
    {
        return 'OG_241a';
    }

    public static function create()
    {
        return new Card\OG_241a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_241a';
    }
}