<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BrianSchwab
{
    public static function globalId() : string
    {
        return 'CRED_13';
    }

    public static function create()
    {
        return new Card\CRED_13;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_13';
    }
}