<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShirvallahsVengeance
{
    public static function globalId() : string
    {
        return 'TRLA_137t';
    }

    public static function create()
    {
        return new Card\TRLA_137t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_137t';
    }
}