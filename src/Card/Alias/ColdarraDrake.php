<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ColdarraDrake
{
    public static function globalId() : string
    {
        return 'AT_008';
    }

    public static function create()
    {
        return new Card\AT_008;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_008';
    }
}