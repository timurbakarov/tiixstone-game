<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShimmeringTempest
{
    public static function globalId() : string
    {
        return 'UNG_846';
    }

    public static function create()
    {
        return new Card\UNG_846;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_846';
    }
}