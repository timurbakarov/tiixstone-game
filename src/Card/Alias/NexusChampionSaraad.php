<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NexusChampionSaraad
{
    public static function globalId() : string
    {
        return 'AT_127';
    }

    public static function create()
    {
        return new Card\AT_127;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_127';
    }
}