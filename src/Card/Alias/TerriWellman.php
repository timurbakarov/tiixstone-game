<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TerriWellman
{
    public static function globalId() : string
    {
        return 'CRED_87';
    }

    public static function create()
    {
        return new Card\CRED_87;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_87';
    }
}