<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FirePlumePhoenix
{
    public static function globalId() : string
    {
        return 'UNG_084';
    }

    public static function create()
    {
        return new Card\UNG_084;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_084';
    }
}