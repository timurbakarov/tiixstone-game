<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Gyth
{
    public static function globalId() : string
    {
        return 'BRMA09_5t';
    }

    public static function create()
    {
        return new Card\BRMA09_5t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA09_5t';
    }
}