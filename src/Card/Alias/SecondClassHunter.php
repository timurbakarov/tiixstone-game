<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SecondClassHunter
{
    public static function globalId() : string
    {
        return 'TB_ClassRandom_Hunter';
    }

    public static function create()
    {
        return new Card\TB_ClassRandom_Hunter;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_ClassRandom_Hunter';
    }
}