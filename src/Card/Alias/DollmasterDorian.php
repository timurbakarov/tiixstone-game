<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DollmasterDorian
{
    public static function globalId() : string
    {
        return 'GIL_620';
    }

    public static function create()
    {
        return new Card\GIL_620;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_620';
    }
}