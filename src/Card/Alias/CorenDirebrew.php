<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CorenDirebrew
{
    public static function globalId() : string
    {
        return 'BRMC_92';
    }

    public static function create()
    {
        return new Card\BRMC_92;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_92';
    }
}