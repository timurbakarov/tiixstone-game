<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Piranha
{
    public static function globalId() : string
    {
        return 'CFM_337t';
    }

    public static function create()
    {
        return new Card\CFM_337t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_337t';
    }
}