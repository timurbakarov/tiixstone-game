<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SonOfTheFlame
{
    public static function globalId() : string
    {
        return 'BRMC_91';
    }

    public static function create()
    {
        return new Card\BRMC_91;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_91';
    }
}