<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpectralWarrior
{
    public static function globalId() : string
    {
        return 'NAX8_04t';
    }

    public static function create()
    {
        return new Card\NAX8_04t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX8_04t';
    }
}