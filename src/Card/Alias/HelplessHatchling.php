<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HelplessHatchling
{
    public static function globalId() : string
    {
        return 'TRL_505';
    }

    public static function create()
    {
        return new Card\TRL_505;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_505';
    }
}