<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Decorate
{
    public static function globalId() : string
    {
        return 'TB_MammothParty_hp001';
    }

    public static function create()
    {
        return new Card\TB_MammothParty_hp001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_MammothParty_hp001';
    }
}