<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GhoulInfestor
{
    public static function globalId() : string
    {
        return 'ICC_085t';
    }

    public static function create()
    {
        return new Card\ICC_085t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_085t';
    }
}