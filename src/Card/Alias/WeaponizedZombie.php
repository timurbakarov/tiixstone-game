<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WeaponizedZombie
{
    public static function globalId() : string
    {
        return 'TRLA_148';
    }

    public static function create()
    {
        return new Card\TRLA_148;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_148';
    }
}