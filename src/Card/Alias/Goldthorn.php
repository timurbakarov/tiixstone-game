<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Goldthorn
{
    public static function globalId() : string
    {
        return 'CFM_621t32';
    }

    public static function create()
    {
        return new Card\CFM_621t32;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_621t32';
    }
}