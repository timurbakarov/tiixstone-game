<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CurseOfWeakness
{
    public static function globalId() : string
    {
        return 'GIL_665';
    }

    public static function create()
    {
        return new Card\GIL_665;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_665';
    }
}