<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TroggBeastrager
{
    public static function globalId() : string
    {
        return 'CFM_338';
    }

    public static function create()
    {
        return new Card\CFM_338;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_338';
    }
}