<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BaitedArrow
{
    public static function globalId() : string
    {
        return 'TRL_347';
    }

    public static function create()
    {
        return new Card\TRL_347;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_347';
    }
}