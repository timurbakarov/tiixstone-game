<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BlessedOne
{
    public static function globalId() : string
    {
        return 'TRLA_140';
    }

    public static function create()
    {
        return new Card\TRLA_140;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_140';
    }
}