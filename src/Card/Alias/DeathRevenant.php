<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DeathRevenant
{
    public static function globalId() : string
    {
        return 'ICC_450';
    }

    public static function create()
    {
        return new Card\ICC_450;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_450';
    }
}