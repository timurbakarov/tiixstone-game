<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Wrathguard
{
    public static function globalId() : string
    {
        return 'AT_026';
    }

    public static function create()
    {
        return new Card\AT_026;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_026';
    }
}