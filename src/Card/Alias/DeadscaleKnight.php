<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DeadscaleKnight
{
    public static function globalId() : string
    {
        return 'ICC_220';
    }

    public static function create()
    {
        return new Card\ICC_220;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_220';
    }
}