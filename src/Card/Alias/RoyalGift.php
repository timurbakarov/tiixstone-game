<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RoyalGift
{
    public static function globalId() : string
    {
        return 'GILA_824';
    }

    public static function create()
    {
        return new Card\GILA_824;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_824';
    }
}