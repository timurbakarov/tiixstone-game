<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MoltenRage
{
    public static function globalId() : string
    {
        return 'TBA01_6';
    }

    public static function create()
    {
        return new Card\TBA01_6;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TBA01_6';
    }
}