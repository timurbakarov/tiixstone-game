<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GhostlyCharger
{
    public static function globalId() : string
    {
        return 'GIL_545';
    }

    public static function create()
    {
        return new Card\GIL_545;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_545';
    }
}