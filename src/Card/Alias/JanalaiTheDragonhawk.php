<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JanalaiTheDragonhawk
{
    public static function globalId() : string
    {
        return 'TRL_316';
    }

    public static function create()
    {
        return new Card\TRL_316;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_316';
    }
}