<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Cannon
{
    public static function globalId() : string
    {
        return 'GILA_601';
    }

    public static function create()
    {
        return new Card\GILA_601;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_601';
    }
}