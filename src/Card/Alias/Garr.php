<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Garr
{
    public static function globalId() : string
    {
        return 'BRMC_99';
    }

    public static function create()
    {
        return new Card\BRMC_99;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_99';
    }
}