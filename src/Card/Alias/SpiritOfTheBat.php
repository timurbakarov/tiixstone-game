<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpiritOfTheBat
{
    public static function globalId() : string
    {
        return 'TRL_251';
    }

    public static function create()
    {
        return new Card\TRL_251;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_251';
    }
}