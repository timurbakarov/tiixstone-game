<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GothikTheHarvester
{
    public static function globalId() : string
    {
        return 'TB_KTRAF_4';
    }

    public static function create()
    {
        return new Card\TB_KTRAF_4;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_KTRAF_4';
    }
}