<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MichaelAltuna
{
    public static function globalId() : string
    {
        return 'CRED_60';
    }

    public static function create()
    {
        return new Card\CRED_60;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_60';
    }
}