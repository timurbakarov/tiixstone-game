<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HungryNaga
{
    public static function globalId() : string
    {
        return 'LOEA09_13';
    }

    public static function create()
    {
        return new Card\LOEA09_13;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA09_13';
    }
}