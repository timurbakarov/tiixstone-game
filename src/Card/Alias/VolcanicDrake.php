<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VolcanicDrake
{
    public static function globalId() : string
    {
        return 'BRM_025';
    }

    public static function create()
    {
        return new Card\BRM_025;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_025';
    }
}