<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodBeast
{
    public static function globalId() : string
    {
        return 'ICCA09_001t1';
    }

    public static function create()
    {
        return new Card\ICCA09_001t1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA09_001t1';
    }
}