<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CultSorcerer
{
    public static function globalId() : string
    {
        return 'OG_303';
    }

    public static function create()
    {
        return new Card\OG_303;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_303';
    }
}