<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NightbaneTemplar
{
    public static function globalId() : string
    {
        return 'KAR_010';
    }

    public static function create()
    {
        return new Card\KAR_010;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_010';
    }
}