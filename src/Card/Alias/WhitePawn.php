<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WhitePawn
{
    public static function globalId() : string
    {
        return 'KAR_A10_02';
    }

    public static function create()
    {
        return new Card\KAR_A10_02;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_A10_02';
    }
}