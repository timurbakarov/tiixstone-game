<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TrueLove
{
    public static function globalId() : string
    {
        return 'KARA_06_03hp';
    }

    public static function create()
    {
        return new Card\KARA_06_03hp;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_06_03hp';
    }
}