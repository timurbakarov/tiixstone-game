<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ChargedDevilsaur
{
    public static function globalId() : string
    {
        return 'UNG_099';
    }

    public static function create()
    {
        return new Card\UNG_099;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_099';
    }
}