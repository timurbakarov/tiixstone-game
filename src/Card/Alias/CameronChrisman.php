<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CameronChrisman
{
    public static function globalId() : string
    {
        return 'CRED_22';
    }

    public static function create()
    {
        return new Card\CRED_22;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_22';
    }
}