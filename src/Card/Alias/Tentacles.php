<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Tentacles
{
    public static function globalId() : string
    {
        return 'ICCA07_020';
    }

    public static function create()
    {
        return new Card\ICCA07_020;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA07_020';
    }
}