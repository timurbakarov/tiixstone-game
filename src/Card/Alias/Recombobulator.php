<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Recombobulator
{
    public static function globalId() : string
    {
        return 'GVG_108';
    }

    public static function create()
    {
        return new Card\GVG_108;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_108';
    }
}