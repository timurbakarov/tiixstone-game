<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArmyOfTheDead
{
    public static function globalId() : string
    {
        return 'TRLA_Priest_08';
    }

    public static function create()
    {
        return new Card\TRLA_Priest_08;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Priest_08';
    }
}