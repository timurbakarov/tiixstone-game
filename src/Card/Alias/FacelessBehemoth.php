<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FacelessBehemoth
{
    public static function globalId() : string
    {
        return 'OG_141';
    }

    public static function create()
    {
        return new Card\OG_141;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_141';
    }
}