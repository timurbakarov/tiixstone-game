<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OLDPvPRogue
{
    public static function globalId() : string
    {
        return 'TBST_005';
    }

    public static function create()
    {
        return new Card\TBST_005;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TBST_005';
    }
}