<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NetherBreath
{
    public static function globalId() : string
    {
        return 'KARA_08_03';
    }

    public static function create()
    {
        return new Card\KARA_08_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_08_03';
    }
}