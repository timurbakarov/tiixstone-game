<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PitSnake
{
    public static function globalId() : string
    {
        return 'LOE_010';
    }

    public static function create()
    {
        return new Card\LOE_010;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_010';
    }
}