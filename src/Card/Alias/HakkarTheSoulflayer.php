<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HakkarTheSoulflayer
{
    public static function globalId() : string
    {
        return 'TRL_541';
    }

    public static function create()
    {
        return new Card\TRL_541;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_541';
    }
}