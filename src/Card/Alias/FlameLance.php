<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FlameLance
{
    public static function globalId() : string
    {
        return 'AT_001';
    }

    public static function create()
    {
        return new Card\AT_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_001';
    }
}