<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Infatuation
{
    public static function globalId() : string
    {
        return 'GILA_BOSS_29p';
    }

    public static function create()
    {
        return new Card\GILA_BOSS_29p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_BOSS_29p';
    }
}