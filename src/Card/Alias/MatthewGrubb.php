<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MatthewGrubb
{
    public static function globalId() : string
    {
        return 'CRED_78';
    }

    public static function create()
    {
        return new Card\CRED_78;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_78';
    }
}