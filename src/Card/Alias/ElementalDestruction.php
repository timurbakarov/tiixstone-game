<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ElementalDestruction
{
    public static function globalId() : string
    {
        return 'AT_051';
    }

    public static function create()
    {
        return new Card\AT_051;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_051';
    }
}