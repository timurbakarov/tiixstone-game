<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JacobJarecki
{
    public static function globalId() : string
    {
        return 'CRED_82';
    }

    public static function create()
    {
        return new Card\CRED_82;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_82';
    }
}