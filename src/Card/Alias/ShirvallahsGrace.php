<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShirvallahsGrace
{
    public static function globalId() : string
    {
        return 'TRLA_138t';
    }

    public static function create()
    {
        return new Card\TRLA_138t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_138t';
    }
}