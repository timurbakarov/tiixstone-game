<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MuklaTyrantOfTheVale
{
    public static function globalId() : string
    {
        return 'OG_122';
    }

    public static function create()
    {
        return new Card\OG_122;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_122';
    }
}