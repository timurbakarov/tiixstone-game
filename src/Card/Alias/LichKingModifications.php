<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LichKingModifications
{
    public static function globalId() : string
    {
        return 'FB_LKStats002';
    }

    public static function create()
    {
        return new Card\FB_LKStats002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LKStats002';
    }
}