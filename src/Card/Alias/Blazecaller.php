<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Blazecaller
{
    public static function globalId() : string
    {
        return 'UNG_847';
    }

    public static function create()
    {
        return new Card\UNG_847;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_847';
    }
}