<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Houndmaster
{
    public static function globalId() : string
    {
        return 'DS1_070';
    }

    public static function create()
    {
        return new Card\DS1_070;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\DS1_070';
    }
}