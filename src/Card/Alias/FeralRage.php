<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FeralRage
{
    public static function globalId() : string
    {
        return 'OG_047';
    }

    public static function create()
    {
        return new Card\OG_047;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_047';
    }
}