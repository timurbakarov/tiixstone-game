<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TortollanForager
{
    public static function globalId() : string
    {
        return 'UNG_078';
    }

    public static function create()
    {
        return new Card\UNG_078;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_078';
    }
}