<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IronforgePortal
{
    public static function globalId() : string
    {
        return 'KAR_091';
    }

    public static function create()
    {
        return new Card\KAR_091;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_091';
    }
}