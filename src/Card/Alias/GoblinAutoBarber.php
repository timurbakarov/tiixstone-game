<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GoblinAutoBarber
{
    public static function globalId() : string
    {
        return 'GVG_023';
    }

    public static function create()
    {
        return new Card\GVG_023;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_023';
    }
}