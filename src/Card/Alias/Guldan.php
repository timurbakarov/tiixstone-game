<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Guldan
{
    public static function globalId() : string
    {
        return 'HERO_07';
    }

    public static function create()
    {
        return new Card\HERO_07;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HERO_07';
    }
}