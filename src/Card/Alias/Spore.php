<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Spore
{
    public static function globalId() : string
    {
        return 'NAX6_03t';
    }

    public static function create()
    {
        return new Card\NAX6_03t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX6_03t';
    }
}