<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RumbletuskShaker
{
    public static function globalId() : string
    {
        return 'TRL_531';
    }

    public static function create()
    {
        return new Card\TRL_531;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_531';
    }
}