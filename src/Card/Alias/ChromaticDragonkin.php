<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ChromaticDragonkin
{
    public static function globalId() : string
    {
        return 'BRMA12_8t';
    }

    public static function create()
    {
        return new Card\BRMA12_8t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA12_8t';
    }
}