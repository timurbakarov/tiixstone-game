<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PartyElemental
{
    public static function globalId() : string
    {
        return 'TB_KaraPortals_003';
    }

    public static function create()
    {
        return new Card\TB_KaraPortals_003;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_KaraPortals_003';
    }
}