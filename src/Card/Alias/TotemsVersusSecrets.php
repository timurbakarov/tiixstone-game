<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TotemsVersusSecrets
{
    public static function globalId() : string
    {
        return 'TB_TagTeam_Shaman';
    }

    public static function create()
    {
        return new Card\TB_TagTeam_Shaman;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_TagTeam_Shaman';
    }
}