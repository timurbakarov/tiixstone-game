<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Shellshifter
{
    public static function globalId() : string
    {
        return 'UNG_101t3';
    }

    public static function create()
    {
        return new Card\UNG_101t3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_101t3';
    }
}