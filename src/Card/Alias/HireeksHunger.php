<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HireeksHunger
{
    public static function globalId() : string
    {
        return 'TRLA_179';
    }

    public static function create()
    {
        return new Card\TRLA_179;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_179';
    }
}