<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WeepingGhost
{
    public static function globalId() : string
    {
        return 'GILA_819t';
    }

    public static function create()
    {
        return new Card\GILA_819t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_819t';
    }
}