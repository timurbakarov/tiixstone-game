<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Rikkar
{
    public static function globalId() : string
    {
        return 'TRLA_209h_Warlock';
    }

    public static function create()
    {
        return new Card\TRLA_209h_Warlock;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_209h_Warlock';
    }
}