<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TwinEmperorVeklor
{
    public static function globalId() : string
    {
        return 'OG_131';
    }

    public static function create()
    {
        return new Card\OG_131;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_131';
    }
}