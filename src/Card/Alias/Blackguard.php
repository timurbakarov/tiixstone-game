<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Blackguard
{
    public static function globalId() : string
    {
        return 'ICC_245';
    }

    public static function create()
    {
        return new Card\ICC_245;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_245';
    }
}