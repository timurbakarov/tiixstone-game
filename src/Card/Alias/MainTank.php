<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MainTank
{
    public static function globalId() : string
    {
        return 'TB_Coopv3_104_NewClasses';
    }

    public static function create()
    {
        return new Card\TB_Coopv3_104_NewClasses;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_Coopv3_104_NewClasses';
    }
}