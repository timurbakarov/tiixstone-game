<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FirePlumesHeart
{
    public static function globalId() : string
    {
        return 'UNG_934';
    }

    public static function create()
    {
        return new Card\UNG_934;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_934';
    }
}