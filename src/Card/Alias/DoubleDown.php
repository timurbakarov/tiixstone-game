<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DoubleDown
{
    public static function globalId() : string
    {
        return 'TRLA_Priest_04';
    }

    public static function create()
    {
        return new Card\TRLA_Priest_04;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Priest_04';
    }
}