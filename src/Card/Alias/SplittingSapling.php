<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SplittingSapling
{
    public static function globalId() : string
    {
        return 'GIL_616t';
    }

    public static function create()
    {
        return new Card\GIL_616t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_616t';
    }
}