<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LobstrokTastetester
{
    public static function globalId() : string
    {
        return 'TRLA_192';
    }

    public static function create()
    {
        return new Card\TRLA_192;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_192';
    }
}