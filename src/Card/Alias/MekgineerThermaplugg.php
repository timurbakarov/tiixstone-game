<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MekgineerThermaplugg
{
    public static function globalId() : string
    {
        return 'GVG_116';
    }

    public static function create()
    {
        return new Card\GVG_116;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_116';
    }
}