<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ImpGangBoss
{
    public static function globalId() : string
    {
        return 'BRM_006';
    }

    public static function create()
    {
        return new Card\BRM_006;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_006';
    }
}