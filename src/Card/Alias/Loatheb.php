<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Loatheb
{
    public static function globalId() : string
    {
        return 'FP1_030';
    }

    public static function create()
    {
        return new Card\FP1_030;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FP1_030';
    }
}