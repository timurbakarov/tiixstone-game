<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FlyingMachine
{
    public static function globalId() : string
    {
        return 'GVG_084';
    }

    public static function create()
    {
        return new Card\GVG_084;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_084';
    }
}