<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BigBadVoodoo
{
    public static function globalId() : string
    {
        return 'TRL_082';
    }

    public static function create()
    {
        return new Card\TRL_082;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_082';
    }
}