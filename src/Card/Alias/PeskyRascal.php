<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PeskyRascal
{
    public static function globalId() : string
    {
        return 'TRLA_188';
    }

    public static function create()
    {
        return new Card\TRLA_188;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_188';
    }
}