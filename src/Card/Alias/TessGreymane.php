<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TessGreymane
{
    public static function globalId() : string
    {
        return 'GIL_598';
    }

    public static function create()
    {
        return new Card\GIL_598;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_598';
    }
}