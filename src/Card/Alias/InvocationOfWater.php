<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class InvocationOfWater
{
    public static function globalId() : string
    {
        return 'UNG_211b';
    }

    public static function create()
    {
        return new Card\UNG_211b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_211b';
    }
}