<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Ooze
{
    public static function globalId() : string
    {
        return 'OG_156a';
    }

    public static function create()
    {
        return new Card\OG_156a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_156a';
    }
}