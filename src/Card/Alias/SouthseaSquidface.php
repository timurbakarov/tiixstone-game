<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SouthseaSquidface
{
    public static function globalId() : string
    {
        return 'OG_267';
    }

    public static function create()
    {
        return new Card\OG_267;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_267';
    }
}