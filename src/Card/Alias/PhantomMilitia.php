<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PhantomMilitia
{
    public static function globalId() : string
    {
        return 'GIL_207';
    }

    public static function create()
    {
        return new Card\GIL_207;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_207';
    }
}