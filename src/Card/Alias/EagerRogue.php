<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EagerRogue
{
    public static function globalId() : string
    {
        return 'ICCA01_007';
    }

    public static function create()
    {
        return new Card\ICCA01_007;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA01_007';
    }
}