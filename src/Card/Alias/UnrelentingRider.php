<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class UnrelentingRider
{
    public static function globalId() : string
    {
        return 'NAX8_05';
    }

    public static function create()
    {
        return new Card\NAX8_05;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX8_05';
    }
}