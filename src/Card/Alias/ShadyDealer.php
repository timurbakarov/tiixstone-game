<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShadyDealer
{
    public static function globalId() : string
    {
        return 'AT_032';
    }

    public static function create()
    {
        return new Card\AT_032;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_032';
    }
}