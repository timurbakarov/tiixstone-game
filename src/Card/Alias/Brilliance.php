<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Brilliance
{
    public static function globalId() : string
    {
        return 'KARA_00_04H';
    }

    public static function create()
    {
        return new Card\KARA_00_04H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_00_04H';
    }
}