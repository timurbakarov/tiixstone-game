<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MasterOfCeremonies
{
    public static function globalId() : string
    {
        return 'AT_117';
    }

    public static function create()
    {
        return new Card\AT_117;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_117';
    }
}