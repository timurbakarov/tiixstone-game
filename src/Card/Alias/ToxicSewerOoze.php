<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ToxicSewerOoze
{
    public static function globalId() : string
    {
        return 'CFM_655';
    }

    public static function create()
    {
        return new Card\CFM_655;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_655';
    }
}