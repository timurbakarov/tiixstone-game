<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ZealousInitiate
{
    public static function globalId() : string
    {
        return 'OG_158';
    }

    public static function create()
    {
        return new Card\OG_158;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_158';
    }
}