<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShirvallahTheTiger
{
    public static function globalId() : string
    {
        return 'TRL_300';
    }

    public static function create()
    {
        return new Card\TRL_300;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_300';
    }
}