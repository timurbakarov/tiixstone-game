<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GoblinBlastmage
{
    public static function globalId() : string
    {
        return 'GVG_004';
    }

    public static function create()
    {
        return new Card\GVG_004;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_004';
    }
}