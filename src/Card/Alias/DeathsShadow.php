<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DeathsShadow
{
    public static function globalId() : string
    {
        return 'ICC_827p';
    }

    public static function create()
    {
        return new Card\ICC_827p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_827p';
    }
}