<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FreeFromAmber
{
    public static function globalId() : string
    {
        return 'UNG_854';
    }

    public static function create()
    {
        return new Card\UNG_854;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_854';
    }
}