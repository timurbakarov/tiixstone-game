<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DivideAndConquer
{
    public static function globalId() : string
    {
        return 'GILA_494';
    }

    public static function create()
    {
        return new Card\GILA_494;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_494';
    }
}