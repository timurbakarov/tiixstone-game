<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodreaverGuldan
{
    public static function globalId() : string
    {
        return 'ICC_831';
    }

    public static function create()
    {
        return new Card\ICC_831;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_831';
    }
}