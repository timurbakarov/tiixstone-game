<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SirFinleyMrrgglton
{
    public static function globalId() : string
    {
        return 'LOE_076';
    }

    public static function create()
    {
        return new Card\LOE_076;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_076';
    }
}