<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AkalisChampion
{
    public static function globalId() : string
    {
        return 'TRLA_104t';
    }

    public static function create()
    {
        return new Card\TRLA_104t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_104t';
    }
}