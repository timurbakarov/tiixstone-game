<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EnchantedRaven
{
    public static function globalId() : string
    {
        return 'KAR_300';
    }

    public static function create()
    {
        return new Card\KAR_300;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_300';
    }
}