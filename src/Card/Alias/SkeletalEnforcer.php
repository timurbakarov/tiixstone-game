<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SkeletalEnforcer
{
    public static function globalId() : string
    {
        return 'ICC_025t';
    }

    public static function create()
    {
        return new Card\ICC_025t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_025t';
    }
}