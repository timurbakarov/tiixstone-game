<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CracklingRazormaw
{
    public static function globalId() : string
    {
        return 'UNG_915';
    }

    public static function create()
    {
        return new Card\UNG_915;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_915';
    }
}