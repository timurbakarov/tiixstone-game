<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ActivateElectron
{
    public static function globalId() : string
    {
        return 'BRMA14_6';
    }

    public static function create()
    {
        return new Card\BRMA14_6;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA14_6';
    }
}