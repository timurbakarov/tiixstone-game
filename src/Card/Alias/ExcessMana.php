<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ExcessMana
{
    public static function globalId() : string
    {
        return 'CS2_013t';
    }

    public static function create()
    {
        return new Card\CS2_013t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_013t';
    }
}