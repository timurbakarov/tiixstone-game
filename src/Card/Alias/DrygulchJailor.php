<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DrygulchJailor
{
    public static function globalId() : string
    {
        return 'LOOT_363';
    }

    public static function create()
    {
        return new Card\LOOT_363;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOOT_363';
    }
}