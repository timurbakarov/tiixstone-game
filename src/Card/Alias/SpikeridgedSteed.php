<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpikeridgedSteed
{
    public static function globalId() : string
    {
        return 'UNG_952';
    }

    public static function create()
    {
        return new Card\UNG_952;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_952';
    }
}