<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DireFateMurlocs
{
    public static function globalId() : string
    {
        return 'TB_PickYourFate_11rand';
    }

    public static function create()
    {
        return new Card\TB_PickYourFate_11rand;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_PickYourFate_11rand';
    }
}