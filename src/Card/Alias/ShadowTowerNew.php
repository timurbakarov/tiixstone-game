<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShadowTowerNew
{
    public static function globalId() : string
    {
        return 'TB_GP_03';
    }

    public static function create()
    {
        return new Card\TB_GP_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_GP_03';
    }
}