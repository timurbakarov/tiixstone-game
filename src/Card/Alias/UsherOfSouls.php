<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class UsherOfSouls
{
    public static function globalId() : string
    {
        return 'OG_302';
    }

    public static function create()
    {
        return new Card\OG_302;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_302';
    }
}