<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SkycapnKragg
{
    public static function globalId() : string
    {
        return 'AT_070';
    }

    public static function create()
    {
        return new Card\AT_070;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_070';
    }
}