<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Legion
{
    public static function globalId() : string
    {
        return 'KARA_00_02H';
    }

    public static function create()
    {
        return new Card\KARA_00_02H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_00_02H';
    }
}