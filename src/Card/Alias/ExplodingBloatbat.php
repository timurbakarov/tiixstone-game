<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ExplodingBloatbat
{
    public static function globalId() : string
    {
        return 'ICC_021';
    }

    public static function create()
    {
        return new Card\ICC_021;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_021';
    }
}