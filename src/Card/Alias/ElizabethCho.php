<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ElizabethCho
{
    public static function globalId() : string
    {
        return 'CRED_25';
    }

    public static function create()
    {
        return new Card\CRED_25;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_25';
    }
}