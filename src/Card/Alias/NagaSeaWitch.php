<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NagaSeaWitch
{
    public static function globalId() : string
    {
        return 'LOE_038';
    }

    public static function create()
    {
        return new Card\LOE_038;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_038';
    }
}