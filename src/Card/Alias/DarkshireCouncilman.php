<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DarkshireCouncilman
{
    public static function globalId() : string
    {
        return 'OG_113';
    }

    public static function create()
    {
        return new Card\OG_113;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_113';
    }
}