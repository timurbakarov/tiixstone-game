<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class UndercityValiant
{
    public static function globalId() : string
    {
        return 'AT_030';
    }

    public static function create()
    {
        return new Card\AT_030;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_030';
    }
}