<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DanEmmons
{
    public static function globalId() : string
    {
        return 'CRED_47';
    }

    public static function create()
    {
        return new Card\CRED_47;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_47';
    }
}