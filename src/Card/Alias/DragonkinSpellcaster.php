<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DragonkinSpellcaster
{
    public static function globalId() : string
    {
        return 'BRMC_84';
    }

    public static function create()
    {
        return new Card\BRMC_84;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_84';
    }
}