<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Doppelgangster
{
    public static function globalId() : string
    {
        return 'CFM_668';
    }

    public static function create()
    {
        return new Card\CFM_668;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_668';
    }
}