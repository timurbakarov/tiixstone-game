<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FateArmor
{
    public static function globalId() : string
    {
        return 'TB_PickYourFate_8rand';
    }

    public static function create()
    {
        return new Card\TB_PickYourFate_8rand;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_PickYourFate_8rand';
    }
}