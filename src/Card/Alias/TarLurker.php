<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TarLurker
{
    public static function globalId() : string
    {
        return 'UNG_049';
    }

    public static function create()
    {
        return new Card\UNG_049;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_049';
    }
}