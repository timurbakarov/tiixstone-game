<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CorneredSentry
{
    public static function globalId() : string
    {
        return 'UNG_926';
    }

    public static function create()
    {
        return new Card\UNG_926;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_926';
    }
}