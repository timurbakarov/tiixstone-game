<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RoguesDoIt
{
    public static function globalId() : string
    {
        return 'PRO_001b';
    }

    public static function create()
    {
        return new Card\PRO_001b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\PRO_001b';
    }
}