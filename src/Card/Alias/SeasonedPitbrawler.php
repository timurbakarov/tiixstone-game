<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SeasonedPitbrawler
{
    public static function globalId() : string
    {
        return 'TRLA_175';
    }

    public static function create()
    {
        return new Card\TRLA_175;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_175';
    }
}