<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Dragonkin
{
    public static function globalId() : string
    {
        return 'BRMA09_4t';
    }

    public static function create()
    {
        return new Card\BRMA09_4t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA09_4t';
    }
}