<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BlackhowlGunspire
{
    public static function globalId() : string
    {
        return 'GIL_152';
    }

    public static function create()
    {
        return new Card\GIL_152;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_152';
    }
}