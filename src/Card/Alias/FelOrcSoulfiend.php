<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FelOrcSoulfiend
{
    public static function globalId() : string
    {
        return 'CFM_609';
    }

    public static function create()
    {
        return new Card\CFM_609;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_609';
    }
}