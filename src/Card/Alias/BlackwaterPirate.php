<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BlackwaterPirate
{
    public static function globalId() : string
    {
        return 'OG_322';
    }

    public static function create()
    {
        return new Card\OG_322;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_322';
    }
}