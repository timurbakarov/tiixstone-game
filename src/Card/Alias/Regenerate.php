<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Regenerate
{
    public static function globalId() : string
    {
        return 'TRL_128';
    }

    public static function create()
    {
        return new Card\TRL_128;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_128';
    }
}