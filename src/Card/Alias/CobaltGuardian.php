<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CobaltGuardian
{
    public static function globalId() : string
    {
        return 'GVG_062';
    }

    public static function create()
    {
        return new Card\GVG_062;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_062';
    }
}