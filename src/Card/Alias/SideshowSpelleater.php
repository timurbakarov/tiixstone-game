<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SideshowSpelleater
{
    public static function globalId() : string
    {
        return 'AT_098';
    }

    public static function create()
    {
        return new Card\AT_098;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_098';
    }
}