<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Swashburglar
{
    public static function globalId() : string
    {
        return 'KAR_069';
    }

    public static function create()
    {
        return new Card\KAR_069;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_069';
    }
}