<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HolyChampion
{
    public static function globalId() : string
    {
        return 'AT_011';
    }

    public static function create()
    {
        return new Card\AT_011;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_011';
    }
}