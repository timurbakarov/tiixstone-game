<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HeadhuntersHatchet
{
    public static function globalId() : string
    {
        return 'TRL_111';
    }

    public static function create()
    {
        return new Card\TRL_111;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_111';
    }
}