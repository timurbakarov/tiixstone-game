<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Unload
{
    public static function globalId() : string
    {
        return 'GILA_499';
    }

    public static function create()
    {
        return new Card\GILA_499;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_499';
    }
}