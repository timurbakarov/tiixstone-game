<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EyeOfOrsis
{
    public static function globalId() : string
    {
        return 'LOEA16_13';
    }

    public static function create()
    {
        return new Card\LOEA16_13;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA16_13';
    }
}