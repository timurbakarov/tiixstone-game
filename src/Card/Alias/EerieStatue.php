<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EerieStatue
{
    public static function globalId() : string
    {
        return 'LOE_107';
    }

    public static function create()
    {
        return new Card\LOE_107;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_107';
    }
}