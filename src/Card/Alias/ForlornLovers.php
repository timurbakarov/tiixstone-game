<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ForlornLovers
{
    public static function globalId() : string
    {
        return 'GILA_BOSS_39h';
    }

    public static function create()
    {
        return new Card\GILA_BOSS_39h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_BOSS_39h';
    }
}