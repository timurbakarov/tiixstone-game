<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Rock
{
    public static function globalId() : string
    {
        return 'LOE_016t';
    }

    public static function create()
    {
        return new Card\LOE_016t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_016t';
    }
}