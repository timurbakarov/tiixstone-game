<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BogCreeper
{
    public static function globalId() : string
    {
        return 'OG_153';
    }

    public static function create()
    {
        return new Card\OG_153;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_153';
    }
}