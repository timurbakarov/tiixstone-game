<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NerubianSoldier
{
    public static function globalId() : string
    {
        return 'OG_270a';
    }

    public static function create()
    {
        return new Card\OG_270a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_270a';
    }
}