<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WilfredFizzlebang
{
    public static function globalId() : string
    {
        return 'AT_027';
    }

    public static function create()
    {
        return new Card\AT_027;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_027';
    }
}