<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AstralCommunion
{
    public static function globalId() : string
    {
        return 'AT_043';
    }

    public static function create()
    {
        return new Card\AT_043;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_043';
    }
}