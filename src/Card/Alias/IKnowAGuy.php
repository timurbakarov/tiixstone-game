<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IKnowAGuy
{
    public static function globalId() : string
    {
        return 'CFM_940';
    }

    public static function create()
    {
        return new Card\CFM_940;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_940';
    }
}