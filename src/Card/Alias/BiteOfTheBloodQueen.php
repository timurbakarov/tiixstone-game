<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BiteOfTheBloodQueen
{
    public static function globalId() : string
    {
        return 'ICCA05_020';
    }

    public static function create()
    {
        return new Card\ICCA05_020;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA05_020';
    }
}