<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Stegodon
{
    public static function globalId() : string
    {
        return 'UNG_810';
    }

    public static function create()
    {
        return new Card\UNG_810;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_810';
    }
}