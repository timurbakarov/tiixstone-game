<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Voidlord
{
    public static function globalId() : string
    {
        return 'LOOT_368';
    }

    public static function create()
    {
        return new Card\LOOT_368;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOOT_368';
    }
}