<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AcolyteOfAgony
{
    public static function globalId() : string
    {
        return 'ICC_212';
    }

    public static function create()
    {
        return new Card\ICC_212;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_212';
    }
}