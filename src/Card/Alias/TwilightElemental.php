<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TwilightElemental
{
    public static function globalId() : string
    {
        return 'OG_031a';
    }

    public static function create()
    {
        return new Card\OG_031a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_031a';
    }
}