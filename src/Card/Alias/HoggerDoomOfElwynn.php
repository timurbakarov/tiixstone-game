<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HoggerDoomOfElwynn
{
    public static function globalId() : string
    {
        return 'OG_318';
    }

    public static function create()
    {
        return new Card\OG_318;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_318';
    }
}