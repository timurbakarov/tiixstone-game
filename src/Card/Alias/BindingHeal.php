<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BindingHeal
{
    public static function globalId() : string
    {
        return 'UNG_030';
    }

    public static function create()
    {
        return new Card\UNG_030;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_030';
    }
}