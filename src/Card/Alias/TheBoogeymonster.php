<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheBoogeymonster
{
    public static function globalId() : string
    {
        return 'OG_300';
    }

    public static function create()
    {
        return new Card\OG_300;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_300';
    }
}