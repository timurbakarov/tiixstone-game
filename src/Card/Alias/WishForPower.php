<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WishForPower
{
    public static function globalId() : string
    {
        return 'LOEA02_03';
    }

    public static function create()
    {
        return new Card\LOEA02_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA02_03';
    }
}