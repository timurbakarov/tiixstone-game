<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CreepyCurio
{
    public static function globalId() : string
    {
        return 'GILA_817';
    }

    public static function create()
    {
        return new Card\GILA_817;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_817';
    }
}