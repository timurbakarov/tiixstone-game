<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodRazor
{
    public static function globalId() : string
    {
        return 'ICC_064';
    }

    public static function create()
    {
        return new Card\ICC_064;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_064';
    }
}