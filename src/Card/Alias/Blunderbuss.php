<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Blunderbuss
{
    public static function globalId() : string
    {
        return 'GILA_500t3';
    }

    public static function create()
    {
        return new Card\GILA_500t3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_500t3';
    }
}