<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EyeOfHakkar
{
    public static function globalId() : string
    {
        return 'LOE_008';
    }

    public static function create()
    {
        return new Card\LOE_008;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_008';
    }
}