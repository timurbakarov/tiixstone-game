<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpectralCutlass
{
    public static function globalId() : string
    {
        return 'GIL_672';
    }

    public static function create()
    {
        return new Card\GIL_672;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_672';
    }
}