<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EnvenomWeapon
{
    public static function globalId() : string
    {
        return 'UNG_823';
    }

    public static function create()
    {
        return new Card\UNG_823;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_823';
    }
}