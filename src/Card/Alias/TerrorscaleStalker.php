<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TerrorscaleStalker
{
    public static function globalId() : string
    {
        return 'UNG_800';
    }

    public static function create()
    {
        return new Card\UNG_800;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_800';
    }
}