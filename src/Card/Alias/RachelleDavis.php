<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RachelleDavis
{
    public static function globalId() : string
    {
        return 'CRED_12';
    }

    public static function create()
    {
        return new Card\CRED_12;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_12';
    }
}