<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class UnholyShadow
{
    public static function globalId() : string
    {
        return 'NAX9_06';
    }

    public static function create()
    {
        return new Card\NAX9_06;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX9_06';
    }
}