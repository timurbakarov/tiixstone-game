<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EvolveScales
{
    public static function globalId() : string
    {
        return 'OG_047b';
    }

    public static function create()
    {
        return new Card\OG_047b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_047b';
    }
}