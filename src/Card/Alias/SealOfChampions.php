<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SealOfChampions
{
    public static function globalId() : string
    {
        return 'AT_074';
    }

    public static function create()
    {
        return new Card\AT_074;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_074';
    }
}