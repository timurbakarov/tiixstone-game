<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PrincessTalanji
{
    public static function globalId() : string
    {
        return 'TRL_259';
    }

    public static function create()
    {
        return new Card\TRL_259;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_259';
    }
}