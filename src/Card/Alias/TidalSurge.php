<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TidalSurge
{
    public static function globalId() : string
    {
        return 'UNG_817';
    }

    public static function create()
    {
        return new Card\UNG_817;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_817';
    }
}