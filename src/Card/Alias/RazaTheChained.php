<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RazaTheChained
{
    public static function globalId() : string
    {
        return 'CFM_020';
    }

    public static function create()
    {
        return new Card\CFM_020;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_020';
    }
}