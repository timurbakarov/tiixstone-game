<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WANTED
{
    public static function globalId() : string
    {
        return 'GIL_687';
    }

    public static function create()
    {
        return new Card\GIL_687;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_687';
    }
}