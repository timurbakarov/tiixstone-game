<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShatteringSpree
{
    public static function globalId() : string
    {
        return 'LOEA06_04h';
    }

    public static function create()
    {
        return new Card\LOEA06_04h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA06_04h';
    }
}