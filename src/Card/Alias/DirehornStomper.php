<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DirehornStomper
{
    public static function globalId() : string
    {
        return 'TRLA_122';
    }

    public static function create()
    {
        return new Card\TRLA_122;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_122';
    }
}