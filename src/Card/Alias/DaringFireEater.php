<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DaringFireEater
{
    public static function globalId() : string
    {
        return 'TRL_390';
    }

    public static function create()
    {
        return new Card\TRL_390;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_390';
    }
}