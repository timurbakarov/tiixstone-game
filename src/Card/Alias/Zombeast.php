<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Zombeast
{
    public static function globalId() : string
    {
        return 'ICC_800h3t';
    }

    public static function create()
    {
        return new Card\ICC_800h3t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_800h3t';
    }
}