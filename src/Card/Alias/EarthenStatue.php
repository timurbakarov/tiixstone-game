<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EarthenStatue
{
    public static function globalId() : string
    {
        return 'LOEA06_02t';
    }

    public static function create()
    {
        return new Card\LOEA06_02t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA06_02t';
    }
}