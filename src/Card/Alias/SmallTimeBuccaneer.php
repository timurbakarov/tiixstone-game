<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SmallTimeBuccaneer
{
    public static function globalId() : string
    {
        return 'CFM_325';
    }

    public static function create()
    {
        return new Card\CFM_325;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_325';
    }
}