<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Butch
{
    public static function globalId() : string
    {
        return 'GILA_403';
    }

    public static function create()
    {
        return new Card\GILA_403;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_403';
    }
}