<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IncreaseHealth
{
    public static function globalId() : string
    {
        return 'FB_LKStats002a';
    }

    public static function create()
    {
        return new Card\FB_LKStats002a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LKStats002a';
    }
}