<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ProtectTheKing
{
    public static function globalId() : string
    {
        return 'KAR_026';
    }

    public static function create()
    {
        return new Card\KAR_026;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_026';
    }
}