<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VaelastraszTheCorrupt
{
    public static function globalId() : string
    {
        return 'BRMA11_1';
    }

    public static function create()
    {
        return new Card\BRMA11_1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA11_1';
    }
}