<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Howlfiend
{
    public static function globalId() : string
    {
        return 'ICC_218';
    }

    public static function create()
    {
        return new Card\ICC_218;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_218';
    }
}