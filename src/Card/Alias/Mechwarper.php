<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Mechwarper
{
    public static function globalId() : string
    {
        return 'GVG_006';
    }

    public static function create()
    {
        return new Card\GVG_006;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_006';
    }
}