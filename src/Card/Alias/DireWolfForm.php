<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DireWolfForm
{
    public static function globalId() : string
    {
        return 'GIL_188b';
    }

    public static function create()
    {
        return new Card\GIL_188b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_188b';
    }
}