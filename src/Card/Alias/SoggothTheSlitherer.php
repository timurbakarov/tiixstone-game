<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SoggothTheSlitherer
{
    public static function globalId() : string
    {
        return 'OG_340';
    }

    public static function create()
    {
        return new Card\OG_340;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_340';
    }
}