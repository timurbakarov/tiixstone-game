<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DirtyRat
{
    public static function globalId() : string
    {
        return 'CFM_790';
    }

    public static function create()
    {
        return new Card\CFM_790;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_790';
    }
}