<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Streetsmuggler
{
    public static function globalId() : string
    {
        return 'TRLA_168';
    }

    public static function create()
    {
        return new Card\TRLA_168;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_168';
    }
}