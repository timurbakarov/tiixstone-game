<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Slime
{
    public static function globalId() : string
    {
        return 'FP1_012t';
    }

    public static function create()
    {
        return new Card\FP1_012t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FP1_012t';
    }
}