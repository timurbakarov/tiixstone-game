<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MichaelReynaga
{
    public static function globalId() : string
    {
        return 'CRED_71';
    }

    public static function create()
    {
        return new Card\CRED_71;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_71';
    }
}