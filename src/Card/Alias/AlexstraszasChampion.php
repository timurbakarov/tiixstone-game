<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AlexstraszasChampion
{
    public static function globalId() : string
    {
        return 'AT_071';
    }

    public static function create()
    {
        return new Card\AT_071;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_071';
    }
}