<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AnnoyopTron
{
    public static function globalId() : string
    {
        return 'TB_FW_ImbaTron';
    }

    public static function create()
    {
        return new Card\TB_FW_ImbaTron;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_FW_ImbaTron';
    }
}