<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OldHorde
{
    public static function globalId() : string
    {
        return 'BRMA09_3';
    }

    public static function create()
    {
        return new Card\BRMA09_3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA09_3';
    }
}