<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Zap
{
    public static function globalId() : string
    {
        return 'GIL_600';
    }

    public static function create()
    {
        return new Card\GIL_600;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_600';
    }
}