<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Priest
{
    public static function globalId() : string
    {
        return 'FB_LK_Priest_copy';
    }

    public static function create()
    {
        return new Card\FB_LK_Priest_copy;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK_Priest_copy';
    }
}