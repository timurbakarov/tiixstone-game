<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CogmastersWrench
{
    public static function globalId() : string
    {
        return 'GVG_024';
    }

    public static function create()
    {
        return new Card\GVG_024;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_024';
    }
}