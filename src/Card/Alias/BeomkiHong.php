<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BeomkiHong
{
    public static function globalId() : string
    {
        return 'CRED_19';
    }

    public static function create()
    {
        return new Card\CRED_19;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_19';
    }
}