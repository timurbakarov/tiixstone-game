<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BurglyBully
{
    public static function globalId() : string
    {
        return 'CFM_669';
    }

    public static function create()
    {
        return new Card\CFM_669;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_669';
    }
}