<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PileOn
{
    public static function globalId() : string
    {
        return 'BRMA01_2H_2_TB';
    }

    public static function create()
    {
        return new Card\BRMA01_2H_2_TB;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA01_2H_2_TB';
    }
}