<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheScourge
{
    public static function globalId() : string
    {
        return 'ICCA08_002p';
    }

    public static function create()
    {
        return new Card\ICCA08_002p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA08_002p';
    }
}