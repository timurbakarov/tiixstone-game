<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IgneousElemental
{
    public static function globalId() : string
    {
        return 'UNG_845';
    }

    public static function create()
    {
        return new Card\UNG_845;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_845';
    }
}