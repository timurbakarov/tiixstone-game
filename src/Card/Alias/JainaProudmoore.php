<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JainaProudmoore
{
    public static function globalId() : string
    {
        return 'HERO_08';
    }

    public static function create()
    {
        return new Card\HERO_08;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HERO_08';
    }
}