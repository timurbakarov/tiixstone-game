<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AuctionmasterBeardo
{
    public static function globalId() : string
    {
        return 'CFM_807';
    }

    public static function create()
    {
        return new Card\CFM_807;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_807';
    }
}