<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TolvirStoneshaper
{
    public static function globalId() : string
    {
        return 'UNG_070';
    }

    public static function create()
    {
        return new Card\UNG_070;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_070';
    }
}