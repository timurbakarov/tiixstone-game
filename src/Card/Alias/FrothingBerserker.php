<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FrothingBerserker
{
    public static function globalId() : string
    {
        return 'EX1_604';
    }

    public static function create()
    {
        return new Card\EX1_604;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_604';
    }
}