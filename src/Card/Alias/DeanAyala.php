<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DeanAyala
{
    public static function globalId() : string
    {
        return 'CRED_24';
    }

    public static function create()
    {
        return new Card\CRED_24;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_24';
    }
}