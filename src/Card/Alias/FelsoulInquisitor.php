<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FelsoulInquisitor
{
    public static function globalId() : string
    {
        return 'GIL_527';
    }

    public static function create()
    {
        return new Card\GIL_527;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_527';
    }
}