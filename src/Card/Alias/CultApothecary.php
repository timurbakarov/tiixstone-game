<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CultApothecary
{
    public static function globalId() : string
    {
        return 'OG_295';
    }

    public static function create()
    {
        return new Card\OG_295;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_295';
    }
}