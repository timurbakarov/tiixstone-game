<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheMajordomo
{
    public static function globalId() : string
    {
        return 'BRMA06_2H_TB';
    }

    public static function create()
    {
        return new Card\BRMA06_2H_TB;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA06_2H_TB';
    }
}