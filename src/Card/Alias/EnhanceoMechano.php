<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EnhanceoMechano
{
    public static function globalId() : string
    {
        return 'GVG_107';
    }

    public static function create()
    {
        return new Card\GVG_107;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_107';
    }
}