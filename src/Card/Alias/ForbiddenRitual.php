<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ForbiddenRitual
{
    public static function globalId() : string
    {
        return 'OG_114';
    }

    public static function create()
    {
        return new Card\OG_114;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_114';
    }
}