<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BolvarFordragon
{
    public static function globalId() : string
    {
        return 'GVG_063';
    }

    public static function create()
    {
        return new Card\GVG_063;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_063';
    }
}