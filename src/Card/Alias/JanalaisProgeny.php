<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JanalaisProgeny
{
    public static function globalId() : string
    {
        return 'TRLA_129t';
    }

    public static function create()
    {
        return new Card\TRLA_129t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_129t';
    }
}