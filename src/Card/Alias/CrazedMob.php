<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CrazedMob
{
    public static function globalId() : string
    {
        return 'GILA_821c';
    }

    public static function create()
    {
        return new Card\GILA_821c;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_821c';
    }
}