<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LilianVoss
{
    public static function globalId() : string
    {
        return 'ICC_811';
    }

    public static function create()
    {
        return new Card\ICC_811;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_811';
    }
}