<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Resurrect
{
    public static function globalId() : string
    {
        return 'BRM_017';
    }

    public static function create()
    {
        return new Card\BRM_017;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_017';
    }
}