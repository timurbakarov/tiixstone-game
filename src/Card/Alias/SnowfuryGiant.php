<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SnowfuryGiant
{
    public static function globalId() : string
    {
        return 'ICC_090';
    }

    public static function create()
    {
        return new Card\ICC_090;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_090';
    }
}