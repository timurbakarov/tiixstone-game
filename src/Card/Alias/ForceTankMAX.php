<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ForceTankMAX
{
    public static function globalId() : string
    {
        return 'GVG_079';
    }

    public static function create()
    {
        return new Card\GVG_079;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_079';
    }
}