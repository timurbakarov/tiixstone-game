<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Eggnapper
{
    public static function globalId() : string
    {
        return 'UNG_076';
    }

    public static function create()
    {
        return new Card\UNG_076;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_076';
    }
}