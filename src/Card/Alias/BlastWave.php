<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BlastWave
{
    public static function globalId() : string
    {
        return 'TRL_317';
    }

    public static function create()
    {
        return new Card\TRL_317;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_317';
    }
}