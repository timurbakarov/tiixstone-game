<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ParrotMascot
{
    public static function globalId() : string
    {
        return 'TRLA_189';
    }

    public static function create()
    {
        return new Card\TRLA_189;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_189';
    }
}