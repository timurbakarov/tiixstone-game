<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JinyuWaterspeaker
{
    public static function globalId() : string
    {
        return 'CFM_061';
    }

    public static function create()
    {
        return new Card\CFM_061;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_061';
    }
}