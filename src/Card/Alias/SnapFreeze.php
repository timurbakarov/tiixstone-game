<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SnapFreeze
{
    public static function globalId() : string
    {
        return 'GIL_801';
    }

    public static function create()
    {
        return new Card\GIL_801;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_801';
    }
}