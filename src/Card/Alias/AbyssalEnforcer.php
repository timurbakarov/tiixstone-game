<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AbyssalEnforcer
{
    public static function globalId() : string
    {
        return 'CFM_751';
    }

    public static function create()
    {
        return new Card\CFM_751;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_751';
    }
}