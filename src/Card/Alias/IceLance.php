<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IceLance
{
    public static function globalId() : string
    {
        return 'CS2_031';
    }

    public static function create()
    {
        return new Card\CS2_031;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_031';
    }
}