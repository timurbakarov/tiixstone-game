<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Medivh
{
    public static function globalId() : string
    {
        return 'HERO_08a';
    }

    public static function create()
    {
        return new Card\HERO_08a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HERO_08a';
    }
}