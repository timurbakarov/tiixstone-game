<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Boar
{
    public static function globalId() : string
    {
        return 'AT_005t';
    }

    public static function create()
    {
        return new Card\AT_005t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_005t';
    }
}