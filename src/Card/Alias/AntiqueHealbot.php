<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AntiqueHealbot
{
    public static function globalId() : string
    {
        return 'GVG_069';
    }

    public static function create()
    {
        return new Card\GVG_069;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_069';
    }
}