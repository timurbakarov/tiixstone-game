<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NerubianProphet
{
    public static function globalId() : string
    {
        return 'OG_138';
    }

    public static function create()
    {
        return new Card\OG_138;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_138';
    }
}