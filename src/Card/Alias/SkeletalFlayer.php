<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SkeletalFlayer
{
    public static function globalId() : string
    {
        return 'ICC_019t';
    }

    public static function create()
    {
        return new Card\ICC_019t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_019t';
    }
}