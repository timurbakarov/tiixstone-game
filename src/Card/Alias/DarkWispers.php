<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DarkWispers
{
    public static function globalId() : string
    {
        return 'GVG_041';
    }

    public static function create()
    {
        return new Card\GVG_041;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_041';
    }
}