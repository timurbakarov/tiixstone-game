<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ChillbladeChampion
{
    public static function globalId() : string
    {
        return 'ICC_820';
    }

    public static function create()
    {
        return new Card\ICC_820;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_820';
    }
}