<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Kodorider
{
    public static function globalId() : string
    {
        return 'AT_099';
    }

    public static function create()
    {
        return new Card\AT_099;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_099';
    }
}