<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheGlassKnight
{
    public static function globalId() : string
    {
        return 'GIL_817';
    }

    public static function create()
    {
        return new Card\GIL_817;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_817';
    }
}