<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OLDLegitHealer
{
    public static function globalId() : string
    {
        return 'TBST_004';
    }

    public static function create()
    {
        return new Card\TBST_004;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TBST_004';
    }
}