<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DrainSoul
{
    public static function globalId() : string
    {
        return 'ICC_055';
    }

    public static function create()
    {
        return new Card\ICC_055;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_055';
    }
}