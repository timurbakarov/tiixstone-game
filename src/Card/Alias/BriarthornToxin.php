<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BriarthornToxin
{
    public static function globalId() : string
    {
        return 'OG_080d';
    }

    public static function create()
    {
        return new Card\OG_080d;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_080d';
    }
}