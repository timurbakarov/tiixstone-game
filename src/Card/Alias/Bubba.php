<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Bubba
{
    public static function globalId() : string
    {
        return 'GILA_410';
    }

    public static function create()
    {
        return new Card\GILA_410;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_410';
    }
}