<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MutatingInjection
{
    public static function globalId() : string
    {
        return 'NAX11_04';
    }

    public static function create()
    {
        return new Card\NAX11_04;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX11_04';
    }
}