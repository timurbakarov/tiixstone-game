<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VoodooDoll
{
    public static function globalId() : string
    {
        return 'GIL_614';
    }

    public static function create()
    {
        return new Card\GIL_614;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_614';
    }
}