<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Stormwatcher
{
    public static function globalId() : string
    {
        return 'UNG_813';
    }

    public static function create()
    {
        return new Card\UNG_813;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_813';
    }
}