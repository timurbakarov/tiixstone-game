<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Anubarak
{
    public static function globalId() : string
    {
        return 'AT_036';
    }

    public static function create()
    {
        return new Card\AT_036;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_036';
    }
}