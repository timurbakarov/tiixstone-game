<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HallazealTheAscended
{
    public static function globalId() : string
    {
        return 'OG_209';
    }

    public static function create()
    {
        return new Card\OG_209;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_209';
    }
}