<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BackroomBouncer
{
    public static function globalId() : string
    {
        return 'CFM_658';
    }

    public static function create()
    {
        return new Card\CFM_658;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_658';
    }
}