<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BlackQueen
{
    public static function globalId() : string
    {
        return 'KAR_A10_10';
    }

    public static function create()
    {
        return new Card\KAR_A10_10;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_A10_10';
    }
}