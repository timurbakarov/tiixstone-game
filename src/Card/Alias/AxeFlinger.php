<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AxeFlinger
{
    public static function globalId() : string
    {
        return 'BRM_016';
    }

    public static function create()
    {
        return new Card\BRM_016;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_016';
    }
}