<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FirecatForm
{
    public static function globalId() : string
    {
        return 'BRM_010a';
    }

    public static function create()
    {
        return new Card\BRM_010a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_010a';
    }
}