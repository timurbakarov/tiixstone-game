<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AnubisathSentinel
{
    public static function globalId() : string
    {
        return 'LOE_061';
    }

    public static function create()
    {
        return new Card\LOE_061;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_061';
    }
}