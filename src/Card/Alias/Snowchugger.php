<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Snowchugger
{
    public static function globalId() : string
    {
        return 'GVG_002';
    }

    public static function create()
    {
        return new Card\GVG_002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_002';
    }
}