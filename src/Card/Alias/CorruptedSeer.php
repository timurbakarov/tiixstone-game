<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CorruptedSeer
{
    public static function globalId() : string
    {
        return 'OG_161';
    }

    public static function create()
    {
        return new Card\OG_161;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_161';
    }
}