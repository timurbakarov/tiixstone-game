<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Moorabi
{
    public static function globalId() : string
    {
        return 'ICC_289';
    }

    public static function create()
    {
        return new Card\ICC_289;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_289';
    }
}