<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CoffinCrasher
{
    public static function globalId() : string
    {
        return 'GIL_805';
    }

    public static function create()
    {
        return new Card\GIL_805;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_805';
    }
}