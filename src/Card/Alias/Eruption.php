<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Eruption
{
    public static function globalId() : string
    {
        return 'NAX5_02';
    }

    public static function create()
    {
        return new Card\NAX5_02;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX5_02';
    }
}