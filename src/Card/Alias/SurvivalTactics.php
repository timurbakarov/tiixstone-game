<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SurvivalTactics
{
    public static function globalId() : string
    {
        return 'TRLA_Hunter_11';
    }

    public static function create()
    {
        return new Card\TRLA_Hunter_11;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Hunter_11';
    }
}