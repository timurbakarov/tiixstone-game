<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DancingSwords
{
    public static function globalId() : string
    {
        return 'FP1_029';
    }

    public static function create()
    {
        return new Card\FP1_029;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FP1_029';
    }
}