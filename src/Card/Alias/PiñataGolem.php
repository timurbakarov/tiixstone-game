<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PiñataGolem
{
    public static function globalId() : string
    {
        return 'TB_MammothParty_Boss002';
    }

    public static function create()
    {
        return new Card\TB_MammothParty_Boss002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_MammothParty_Boss002';
    }
}