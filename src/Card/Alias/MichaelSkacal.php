<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MichaelSkacal
{
    public static function globalId() : string
    {
        return 'CRED_67';
    }

    public static function create()
    {
        return new Card\CRED_67;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_67';
    }
}