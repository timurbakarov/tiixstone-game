<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VioletApprentice
{
    public static function globalId() : string
    {
        return 'NEW1_026t';
    }

    public static function create()
    {
        return new Card\NEW1_026t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NEW1_026t';
    }
}