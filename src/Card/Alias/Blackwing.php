<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Blackwing
{
    public static function globalId() : string
    {
        return 'BRMA09_4';
    }

    public static function create()
    {
        return new Card\BRMA09_4;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA09_4';
    }
}