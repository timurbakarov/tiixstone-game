<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TuskarrRaider
{
    public static function globalId() : string
    {
        return 'GILA_611';
    }

    public static function create()
    {
        return new Card\GILA_611;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_611';
    }
}