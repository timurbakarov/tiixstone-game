<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FlashHeal
{
    public static function globalId() : string
    {
        return 'AT_055';
    }

    public static function create()
    {
        return new Card\AT_055;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_055';
    }
}