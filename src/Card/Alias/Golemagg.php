<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Golemagg
{
    public static function globalId() : string
    {
        return 'BRMC_95';
    }

    public static function create()
    {
        return new Card\BRMC_95;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_95';
    }
}