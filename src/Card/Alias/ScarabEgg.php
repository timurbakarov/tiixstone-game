<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ScarabEgg
{
    public static function globalId() : string
    {
        return 'TRL_503';
    }

    public static function create()
    {
        return new Card\TRL_503;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_503';
    }
}