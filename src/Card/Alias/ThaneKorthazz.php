<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ThaneKorthazz
{
    public static function globalId() : string
    {
        return 'NAX9_03';
    }

    public static function create()
    {
        return new Card\NAX9_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX9_03';
    }
}