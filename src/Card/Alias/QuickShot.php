<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class QuickShot
{
    public static function globalId() : string
    {
        return 'BRM_013';
    }

    public static function create()
    {
        return new Card\BRM_013;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_013';
    }
}