<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PowerOfTheFirelord
{
    public static function globalId() : string
    {
        return 'BRMA03_2';
    }

    public static function create()
    {
        return new Card\BRMA03_2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA03_2';
    }
}