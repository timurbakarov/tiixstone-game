<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HuntingMastiff
{
    public static function globalId() : string
    {
        return 'GIL_607t';
    }

    public static function create()
    {
        return new Card\GIL_607t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_607t';
    }
}