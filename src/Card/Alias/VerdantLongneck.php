<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VerdantLongneck
{
    public static function globalId() : string
    {
        return 'UNG_100';
    }

    public static function create()
    {
        return new Card\UNG_100;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_100';
    }
}