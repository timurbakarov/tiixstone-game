<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PrincessHuhuran
{
    public static function globalId() : string
    {
        return 'OG_309';
    }

    public static function create()
    {
        return new Card\OG_309;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_309';
    }
}