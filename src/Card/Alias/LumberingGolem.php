<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LumberingGolem
{
    public static function globalId() : string
    {
        return 'LOEA07_14';
    }

    public static function create()
    {
        return new Card\LOEA07_14;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA07_14';
    }
}