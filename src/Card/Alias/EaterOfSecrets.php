<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EaterOfSecrets
{
    public static function globalId() : string
    {
        return 'OG_254';
    }

    public static function create()
    {
        return new Card\OG_254;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_254';
    }
}