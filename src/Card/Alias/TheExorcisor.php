<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheExorcisor
{
    public static function globalId() : string
    {
        return 'GILA_508';
    }

    public static function create()
    {
        return new Card\GILA_508;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_508';
    }
}