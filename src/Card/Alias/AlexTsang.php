<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AlexTsang
{
    public static function globalId() : string
    {
        return 'CRED_84';
    }

    public static function create()
    {
        return new Card\CRED_84;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_84';
    }
}