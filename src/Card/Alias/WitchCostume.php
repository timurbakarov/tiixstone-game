<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WitchCostume
{
    public static function globalId() : string
    {
        return 'TB_HeadlessHorseman_s001a';
    }

    public static function create()
    {
        return new Card\TB_HeadlessHorseman_s001a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_HeadlessHorseman_s001a';
    }
}