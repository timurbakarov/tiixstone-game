<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Ankylodon
{
    public static function globalId() : string
    {
        return 'TRL_343at1';
    }

    public static function create()
    {
        return new Card\TRL_343at1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_343at1';
    }
}