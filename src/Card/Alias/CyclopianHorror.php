<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CyclopianHorror
{
    public static function globalId() : string
    {
        return 'OG_337';
    }

    public static function create()
    {
        return new Card\OG_337;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_337';
    }
}