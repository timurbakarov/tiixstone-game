<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Pyromaniac
{
    public static function globalId() : string
    {
        return 'TRL_315';
    }

    public static function create()
    {
        return new Card\TRL_315;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_315';
    }
}