<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SparringPartner
{
    public static function globalId() : string
    {
        return 'AT_069';
    }

    public static function create()
    {
        return new Card\AT_069;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_069';
    }
}