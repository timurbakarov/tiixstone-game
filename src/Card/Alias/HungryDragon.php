<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HungryDragon
{
    public static function globalId() : string
    {
        return 'BRM_026';
    }

    public static function create()
    {
        return new Card\BRM_026;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_026';
    }
}