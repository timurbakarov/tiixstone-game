<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Scrapyard
{
    public static function globalId() : string
    {
        return 'TRLA_Hunter_12';
    }

    public static function create()
    {
        return new Card\TRLA_Hunter_12;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Hunter_12';
    }
}