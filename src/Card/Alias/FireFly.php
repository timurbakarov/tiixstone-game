<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FireFly
{
    public static function globalId() : string
    {
        return 'UNG_809';
    }

    public static function create()
    {
        return new Card\UNG_809;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_809';
    }
}