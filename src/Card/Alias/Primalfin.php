<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Primalfin
{
    public static function globalId() : string
    {
        return 'UNG_201t';
    }

    public static function create()
    {
        return new Card\UNG_201t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_201t';
    }
}