<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MimicPod
{
    public static function globalId() : string
    {
        return 'UNG_060';
    }

    public static function create()
    {
        return new Card\UNG_060;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_060';
    }
}