<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VoodooHexxer
{
    public static function globalId() : string
    {
        return 'ICC_088';
    }

    public static function create()
    {
        return new Card\ICC_088;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_088';
    }
}