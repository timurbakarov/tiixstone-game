<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Ragnaros
{
    public static function globalId() : string
    {
        return 'TB_Frost_Rag';
    }

    public static function create()
    {
        return new Card\TB_Frost_Rag;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_Frost_Rag';
    }
}