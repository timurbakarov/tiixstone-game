<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TentaclesForArms
{
    public static function globalId() : string
    {
        return 'OG_033';
    }

    public static function create()
    {
        return new Card\OG_033;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_033';
    }
}