<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LanternOfPower
{
    public static function globalId() : string
    {
        return 'LOEA16_3';
    }

    public static function create()
    {
        return new Card\LOEA16_3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA16_3';
    }
}