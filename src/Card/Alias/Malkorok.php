<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Malkorok
{
    public static function globalId() : string
    {
        return 'OG_220';
    }

    public static function create()
    {
        return new Card\OG_220;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_220';
    }
}