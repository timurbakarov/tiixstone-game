<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArchbishopBenedictus
{
    public static function globalId() : string
    {
        return 'ICC_215';
    }

    public static function create()
    {
        return new Card\ICC_215;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_215';
    }
}