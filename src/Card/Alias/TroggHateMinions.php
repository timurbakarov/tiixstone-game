<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TroggHateMinions
{
    public static function globalId() : string
    {
        return 'LOEA05_02';
    }

    public static function create()
    {
        return new Card\LOEA05_02;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA05_02';
    }
}