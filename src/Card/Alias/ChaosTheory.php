<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ChaosTheory
{
    public static function globalId() : string
    {
        return 'GILA_901';
    }

    public static function create()
    {
        return new Card\GILA_901;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_901';
    }
}