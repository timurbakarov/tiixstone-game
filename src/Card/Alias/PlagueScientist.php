<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PlagueScientist
{
    public static function globalId() : string
    {
        return 'ICC_809';
    }

    public static function create()
    {
        return new Card\ICC_809;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_809';
    }
}