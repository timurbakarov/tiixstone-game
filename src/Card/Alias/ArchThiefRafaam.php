<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArchThiefRafaam
{
    public static function globalId() : string
    {
        return 'LOE_092';
    }

    public static function create()
    {
        return new Card\LOE_092;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_092';
    }
}