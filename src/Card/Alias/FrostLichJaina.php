<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FrostLichJaina
{
    public static function globalId() : string
    {
        return 'ICC_833';
    }

    public static function create()
    {
        return new Card\ICC_833;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_833';
    }
}