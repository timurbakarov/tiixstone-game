<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HiddenCache
{
    public static function globalId() : string
    {
        return 'CFM_026';
    }

    public static function create()
    {
        return new Card\CFM_026;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_026';
    }
}