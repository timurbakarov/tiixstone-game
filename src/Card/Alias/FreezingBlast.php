<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FreezingBlast
{
    public static function globalId() : string
    {
        return 'FB_LK007p';
    }

    public static function create()
    {
        return new Card\FB_LK007p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK007p';
    }
}