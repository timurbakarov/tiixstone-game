<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpiritOfTheRaptor
{
    public static function globalId() : string
    {
        return 'TRL_223';
    }

    public static function create()
    {
        return new Card\TRL_223;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_223';
    }
}