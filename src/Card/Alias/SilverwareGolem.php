<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SilverwareGolem
{
    public static function globalId() : string
    {
        return 'KAR_205';
    }

    public static function create()
    {
        return new Card\KAR_205;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_205';
    }
}