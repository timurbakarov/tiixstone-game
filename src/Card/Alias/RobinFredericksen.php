<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RobinFredericksen
{
    public static function globalId() : string
    {
        return 'CRED_38';
    }

    public static function create()
    {
        return new Card\CRED_38;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_38';
    }
}