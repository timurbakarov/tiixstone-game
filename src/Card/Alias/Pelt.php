<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Pelt
{
    public static function globalId() : string
    {
        return 'TB_MammothParty_hp002';
    }

    public static function create()
    {
        return new Card\TB_MammothParty_hp002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_MammothParty_hp002';
    }
}