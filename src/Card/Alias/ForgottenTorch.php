<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ForgottenTorch
{
    public static function globalId() : string
    {
        return 'LOE_002';
    }

    public static function create()
    {
        return new Card\LOE_002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_002';
    }
}