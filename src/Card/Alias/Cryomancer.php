<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Cryomancer
{
    public static function globalId() : string
    {
        return 'CFM_671';
    }

    public static function create()
    {
        return new Card\CFM_671;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_671';
    }
}