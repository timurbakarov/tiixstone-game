<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VexCrow
{
    public static function globalId() : string
    {
        return 'GIL_664';
    }

    public static function create()
    {
        return new Card\GIL_664;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_664';
    }
}