<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CompetitiveSpirit
{
    public static function globalId() : string
    {
        return 'AT_073';
    }

    public static function create()
    {
        return new Card\AT_073;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_073';
    }
}