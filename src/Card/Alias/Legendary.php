<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Legendary
{
    public static function globalId() : string
    {
        return 'TRLA_Warrior_03';
    }

    public static function create()
    {
        return new Card\TRLA_Warrior_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Warrior_03';
    }
}