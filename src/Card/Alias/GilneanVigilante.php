<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GilneanVigilante
{
    public static function globalId() : string
    {
        return 'GILA_803';
    }

    public static function create()
    {
        return new Card\GILA_803;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_803';
    }
}