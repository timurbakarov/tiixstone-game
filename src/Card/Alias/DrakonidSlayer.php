<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DrakonidSlayer
{
    public static function globalId() : string
    {
        return 'BRMC_88';
    }

    public static function create()
    {
        return new Card\BRMC_88;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_88';
    }
}