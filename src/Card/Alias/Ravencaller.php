<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Ravencaller
{
    public static function globalId() : string
    {
        return 'GIL_212';
    }

    public static function create()
    {
        return new Card\GIL_212;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_212';
    }
}