<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OldBones
{
    public static function globalId() : string
    {
        return 'GILA_589';
    }

    public static function create()
    {
        return new Card\GILA_589;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_589';
    }
}