<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Gazlowe
{
    public static function globalId() : string
    {
        return 'GVG_117';
    }

    public static function create()
    {
        return new Card\GVG_117;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_117';
    }
}