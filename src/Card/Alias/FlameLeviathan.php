<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FlameLeviathan
{
    public static function globalId() : string
    {
        return 'GVG_007';
    }

    public static function create()
    {
        return new Card\GVG_007;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_007';
    }
}