<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AncientSecrets
{
    public static function globalId() : string
    {
        return 'NEW1_008b';
    }

    public static function create()
    {
        return new Card\NEW1_008b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NEW1_008b';
    }
}