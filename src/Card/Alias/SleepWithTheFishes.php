<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SleepWithTheFishes
{
    public static function globalId() : string
    {
        return 'CFM_716';
    }

    public static function create()
    {
        return new Card\CFM_716;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_716';
    }
}