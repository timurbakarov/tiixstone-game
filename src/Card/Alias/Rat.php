<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Rat
{
    public static function globalId() : string
    {
        return 'CFM_316t';
    }

    public static function create()
    {
        return new Card\CFM_316t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_316t';
    }
}