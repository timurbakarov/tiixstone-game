<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FightPromoter
{
    public static function globalId() : string
    {
        return 'CFM_328';
    }

    public static function create()
    {
        return new Card\CFM_328;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_328';
    }
}