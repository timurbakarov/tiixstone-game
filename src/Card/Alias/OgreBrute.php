<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OgreBrute
{
    public static function globalId() : string
    {
        return 'GVG_065';
    }

    public static function create()
    {
        return new Card\GVG_065;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_065';
    }
}