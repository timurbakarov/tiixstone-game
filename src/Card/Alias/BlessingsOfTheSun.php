<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BlessingsOfTheSun
{
    public static function globalId() : string
    {
        return 'LOEA01_02';
    }

    public static function create()
    {
        return new Card\LOEA01_02;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA01_02';
    }
}