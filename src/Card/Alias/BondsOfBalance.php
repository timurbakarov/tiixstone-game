<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BondsOfBalance
{
    public static function globalId() : string
    {
        return 'TRLA_116t';
    }

    public static function create()
    {
        return new Card\TRLA_116t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_116t';
    }
}