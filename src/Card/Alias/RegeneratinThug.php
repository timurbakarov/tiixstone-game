<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RegeneratinThug
{
    public static function globalId() : string
    {
        return 'TRL_508';
    }

    public static function create()
    {
        return new Card\TRL_508;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_508';
    }
}