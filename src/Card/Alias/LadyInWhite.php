<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LadyInWhite
{
    public static function globalId() : string
    {
        return 'GIL_840';
    }

    public static function create()
    {
        return new Card\GIL_840;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_840';
    }
}