<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MasterOfEvolution
{
    public static function globalId() : string
    {
        return 'OG_328';
    }

    public static function create()
    {
        return new Card\OG_328;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_328';
    }
}