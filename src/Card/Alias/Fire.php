<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Fire
{
    public static function globalId() : string
    {
        return 'GILA_600p';
    }

    public static function create()
    {
        return new Card\GILA_600p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_600p';
    }
}