<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WyrmrestAgent
{
    public static function globalId() : string
    {
        return 'AT_116';
    }

    public static function create()
    {
        return new Card\AT_116;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_116';
    }
}