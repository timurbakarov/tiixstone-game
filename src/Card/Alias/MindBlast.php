<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MindBlast
{
    public static function globalId() : string
    {
        return 'DS1_233';
    }

    public static function create()
    {
        return new Card\DS1_233;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\DS1_233';
    }
}