<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LesserAmethystSpellstone
{
    public static function globalId() : string
    {
        return 'LOOT_043';
    }

    public static function create()
    {
        return new Card\LOOT_043;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOOT_043';
    }
}