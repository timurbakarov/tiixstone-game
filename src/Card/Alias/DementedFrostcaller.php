<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DementedFrostcaller
{
    public static function globalId() : string
    {
        return 'OG_085';
    }

    public static function create()
    {
        return new Card\OG_085;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_085';
    }
}