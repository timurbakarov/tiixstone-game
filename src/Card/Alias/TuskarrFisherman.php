<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TuskarrFisherman
{
    public static function globalId() : string
    {
        return 'ICC_093';
    }

    public static function create()
    {
        return new Card\ICC_093;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_093';
    }
}