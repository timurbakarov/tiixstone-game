<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GoblinSapper
{
    public static function globalId() : string
    {
        return 'GVG_095';
    }

    public static function create()
    {
        return new Card\GVG_095;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_095';
    }
}