<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MattPlace
{
    public static function globalId() : string
    {
        return 'CRED_65';
    }

    public static function create()
    {
        return new Card\CRED_65;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_65';
    }
}