<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Teambuilding
{
    public static function globalId() : string
    {
        return 'TRLA_Paladin_05';
    }

    public static function create()
    {
        return new Card\TRLA_Paladin_05;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Paladin_05';
    }
}