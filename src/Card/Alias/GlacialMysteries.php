<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GlacialMysteries
{
    public static function globalId() : string
    {
        return 'ICC_086';
    }

    public static function create()
    {
        return new Card\ICC_086;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_086';
    }
}