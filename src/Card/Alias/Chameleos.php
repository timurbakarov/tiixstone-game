<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Chameleos
{
    public static function globalId() : string
    {
        return 'GIL_142';
    }

    public static function create()
    {
        return new Card\GIL_142;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_142';
    }
}