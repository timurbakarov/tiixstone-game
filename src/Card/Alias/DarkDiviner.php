<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DarkDiviner
{
    public static function globalId() : string
    {
        return 'TRLA_184';
    }

    public static function create()
    {
        return new Card\TRLA_184;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_184';
    }
}