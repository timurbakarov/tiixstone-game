<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WhirlingBlades
{
    public static function globalId() : string
    {
        return 'PART_007';
    }

    public static function create()
    {
        return new Card\PART_007;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\PART_007';
    }
}