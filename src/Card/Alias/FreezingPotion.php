<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FreezingPotion
{
    public static function globalId() : string
    {
        return 'CFM_021';
    }

    public static function create()
    {
        return new Card\CFM_021;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_021';
    }
}