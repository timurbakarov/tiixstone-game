<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BrewPotion
{
    public static function globalId() : string
    {
        return 'TB_BossRumble_003hp';
    }

    public static function create()
    {
        return new Card\TB_BossRumble_003hp;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_BossRumble_003hp';
    }
}