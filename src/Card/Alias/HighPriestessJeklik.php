<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HighPriestessJeklik
{
    public static function globalId() : string
    {
        return 'TRL_252';
    }

    public static function create()
    {
        return new Card\TRL_252;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_252';
    }
}