<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CancellingTheApocalypse
{
    public static function globalId() : string
    {
        return 'FB_LK011';
    }

    public static function create()
    {
        return new Card\FB_LK011;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK011';
    }
}