<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CoinPouch
{
    public static function globalId() : string
    {
        return 'GILA_816a';
    }

    public static function create()
    {
        return new Card\GILA_816a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_816a';
    }
}