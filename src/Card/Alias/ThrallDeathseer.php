<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ThrallDeathseer
{
    public static function globalId() : string
    {
        return 'ICC_481';
    }

    public static function create()
    {
        return new Card\ICC_481;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_481';
    }
}