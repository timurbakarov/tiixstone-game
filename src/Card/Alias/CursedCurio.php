<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CursedCurio
{
    public static function globalId() : string
    {
        return 'GILA_819';
    }

    public static function create()
    {
        return new Card\GILA_819;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_819';
    }
}