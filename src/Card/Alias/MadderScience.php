<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MadderScience
{
    public static function globalId() : string
    {
        return 'ICCA07_003p';
    }

    public static function create()
    {
        return new Card\ICCA07_003p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA07_003p';
    }
}