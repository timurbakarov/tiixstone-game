<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DarkPeddler
{
    public static function globalId() : string
    {
        return 'LOE_023';
    }

    public static function create()
    {
        return new Card\LOE_023;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_023';
    }
}