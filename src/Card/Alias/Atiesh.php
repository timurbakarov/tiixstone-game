<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Atiesh
{
    public static function globalId() : string
    {
        return 'KAR_097t';
    }

    public static function create()
    {
        return new Card\KAR_097t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_097t';
    }
}