<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KillObjectiveAnubarak
{
    public static function globalId() : string
    {
        return 'HRW02_3';
    }

    public static function create()
    {
        return new Card\HRW02_3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HRW02_3';
    }
}