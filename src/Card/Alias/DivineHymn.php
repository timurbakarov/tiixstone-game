<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DivineHymn
{
    public static function globalId() : string
    {
        return 'GIL_661';
    }

    public static function create()
    {
        return new Card\GIL_661;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_661';
    }
}