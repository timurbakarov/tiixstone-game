<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HeadlessHorseman
{
    public static function globalId() : string
    {
        return 'TB_HeadlessHorseman_H1';
    }

    public static function create()
    {
        return new Card\TB_HeadlessHorseman_H1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_HeadlessHorseman_H1';
    }
}