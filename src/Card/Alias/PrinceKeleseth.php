<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PrinceKeleseth
{
    public static function globalId() : string
    {
        return 'ICC_851';
    }

    public static function create()
    {
        return new Card\ICC_851;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_851';
    }
}