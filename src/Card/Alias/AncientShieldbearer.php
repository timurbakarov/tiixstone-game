<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AncientShieldbearer
{
    public static function globalId() : string
    {
        return 'OG_301';
    }

    public static function create()
    {
        return new Card\OG_301;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_301';
    }
}