<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DevilsaurEgg
{
    public static function globalId() : string
    {
        return 'UNG_083';
    }

    public static function create()
    {
        return new Card\UNG_083;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_083';
    }
}