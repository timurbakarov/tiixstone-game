<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Firesworn
{
    public static function globalId() : string
    {
        return 'BRMA04_3';
    }

    public static function create()
    {
        return new Card\BRMA04_3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA04_3';
    }
}