<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FeignDeath
{
    public static function globalId() : string
    {
        return 'GVG_026';
    }

    public static function create()
    {
        return new Card\GVG_026;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_026';
    }
}