<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Sabertusk
{
    public static function globalId() : string
    {
        return 'TRL_343bt1';
    }

    public static function create()
    {
        return new Card\TRL_343bt1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_343bt1';
    }
}