<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Demonheart
{
    public static function globalId() : string
    {
        return 'GVG_019';
    }

    public static function create()
    {
        return new Card\GVG_019;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_019';
    }
}