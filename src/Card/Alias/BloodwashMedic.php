<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodwashMedic
{
    public static function globalId() : string
    {
        return 'TRLA_150';
    }

    public static function create()
    {
        return new Card\TRLA_150;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_150';
    }
}