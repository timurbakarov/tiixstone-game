<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MaxMcCall
{
    public static function globalId() : string
    {
        return 'CRED_35';
    }

    public static function create()
    {
        return new Card\CRED_35;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_35';
    }
}