<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StrongshellScavenger
{
    public static function globalId() : string
    {
        return 'ICC_807';
    }

    public static function create()
    {
        return new Card\ICC_807;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_807';
    }
}