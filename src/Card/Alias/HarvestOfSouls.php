<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HarvestOfSouls
{
    public static function globalId() : string
    {
        return 'ICCA08_032p';
    }

    public static function create()
    {
        return new Card\ICCA08_032p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA08_032p';
    }
}