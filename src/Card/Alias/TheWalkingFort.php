<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheWalkingFort
{
    public static function globalId() : string
    {
        return 'TRLA_142';
    }

    public static function create()
    {
        return new Card\TRLA_142;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_142';
    }
}