<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Understudy
{
    public static function globalId() : string
    {
        return 'TB_KTRAF_Under';
    }

    public static function create()
    {
        return new Card\TB_KTRAF_Under;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_KTRAF_Under';
    }
}