<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Mechanical
{
    public static function globalId() : string
    {
        return 'GILA_Toki_10';
    }

    public static function create()
    {
        return new Card\GILA_Toki_10;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_Toki_10';
    }
}