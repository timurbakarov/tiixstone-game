<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Demonbolt
{
    public static function globalId() : string
    {
        return 'TRL_555';
    }

    public static function create()
    {
        return new Card\TRL_555;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_555';
    }
}