<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ForbiddenAncient
{
    public static function globalId() : string
    {
        return 'OG_051';
    }

    public static function create()
    {
        return new Card\OG_051;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_051';
    }
}