<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Sindragosa
{
    public static function globalId() : string
    {
        return 'FB_LK_014h';
    }

    public static function create()
    {
        return new Card\FB_LK_014h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK_014h';
    }
}