<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BoneMinions
{
    public static function globalId() : string
    {
        return 'BRMA17_5_TB';
    }

    public static function create()
    {
        return new Card\BRMA17_5_TB;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA17_5_TB';
    }
}