<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TournamentMedic
{
    public static function globalId() : string
    {
        return 'AT_091';
    }

    public static function create()
    {
        return new Card\AT_091;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_091';
    }
}