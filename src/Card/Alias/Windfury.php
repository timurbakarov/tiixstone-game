<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Windfury
{
    public static function globalId() : string
    {
        return 'CS2_039';
    }

    public static function create()
    {
        return new Card\CS2_039;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_039';
    }
}