<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AlAkirTheWindlord
{
    public static function globalId() : string
    {
        return 'NEW1_010';
    }

    public static function create()
    {
        return new Card\NEW1_010;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NEW1_010';
    }
}