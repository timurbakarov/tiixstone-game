<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RedbandWasp
{
    public static function globalId() : string
    {
        return 'GIL_155';
    }

    public static function create()
    {
        return new Card\GIL_155;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_155';
    }
}