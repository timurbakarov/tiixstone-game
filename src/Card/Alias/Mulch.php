<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Mulch
{
    public static function globalId() : string
    {
        return 'AT_044';
    }

    public static function create()
    {
        return new Card\AT_044;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_044';
    }
}