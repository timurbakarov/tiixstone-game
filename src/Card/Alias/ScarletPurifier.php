<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ScarletPurifier
{
    public static function globalId() : string
    {
        return 'GVG_101';
    }

    public static function create()
    {
        return new Card\GVG_101;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_101';
    }
}