<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KingsElekk
{
    public static function globalId() : string
    {
        return 'AT_058';
    }

    public static function create()
    {
        return new Card\AT_058;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_058';
    }
}