<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Fireworks
{
    public static function globalId() : string
    {
        return 'TB_FireFestival_Fireworks';
    }

    public static function create()
    {
        return new Card\TB_FireFestival_Fireworks;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_FireFestival_Fireworks';
    }
}