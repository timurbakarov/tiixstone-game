<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FireHawkForm
{
    public static function globalId() : string
    {
        return 'BRM_010b';
    }

    public static function create()
    {
        return new Card\BRM_010b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_010b';
    }
}