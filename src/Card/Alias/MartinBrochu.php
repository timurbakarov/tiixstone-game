<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MartinBrochu
{
    public static function globalId() : string
    {
        return 'CRED_62';
    }

    public static function create()
    {
        return new Card\CRED_62;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_62';
    }
}