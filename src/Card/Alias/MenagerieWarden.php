<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MenagerieWarden
{
    public static function globalId() : string
    {
        return 'KAR_065';
    }

    public static function create()
    {
        return new Card\KAR_065;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_065';
    }
}