<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ExtraPowder
{
    public static function globalId() : string
    {
        return 'GILA_604';
    }

    public static function create()
    {
        return new Card\GILA_604;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_604';
    }
}