<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CallPet
{
    public static function globalId() : string
    {
        return 'GVG_017';
    }

    public static function create()
    {
        return new Card\GVG_017;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_017';
    }
}