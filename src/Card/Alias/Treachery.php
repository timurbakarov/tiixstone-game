<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Treachery
{
    public static function globalId() : string
    {
        return 'ICC_206';
    }

    public static function create()
    {
        return new Card\ICC_206;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_206';
    }
}