<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PlagueLord
{
    public static function globalId() : string
    {
        return 'ICC_832p';
    }

    public static function create()
    {
        return new Card\ICC_832p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_832p';
    }
}