<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RaiseDead
{
    public static function globalId() : string
    {
        return 'NAX4_04';
    }

    public static function create()
    {
        return new Card\NAX4_04;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX4_04';
    }
}