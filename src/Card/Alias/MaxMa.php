<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MaxMa
{
    public static function globalId() : string
    {
        return 'CRED_34';
    }

    public static function create()
    {
        return new Card\CRED_34;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_34';
    }
}