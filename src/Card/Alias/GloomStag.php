<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GloomStag
{
    public static function globalId() : string
    {
        return 'GIL_130';
    }

    public static function create()
    {
        return new Card\GIL_130;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_130';
    }
}