<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MurlocBonus
{
    public static function globalId() : string
    {
        return 'TB_PickYourFate_11b';
    }

    public static function create()
    {
        return new Card\TB_PickYourFate_11b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_PickYourFate_11b';
    }
}