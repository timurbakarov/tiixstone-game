<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShadowAscendant
{
    public static function globalId() : string
    {
        return 'ICC_210';
    }

    public static function create()
    {
        return new Card\ICC_210;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_210';
    }
}