<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JCPark
{
    public static function globalId() : string
    {
        return 'CRED_30';
    }

    public static function create()
    {
        return new Card\CRED_30;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_30';
    }
}