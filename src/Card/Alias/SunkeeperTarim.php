<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SunkeeperTarim
{
    public static function globalId() : string
    {
        return 'UNG_015';
    }

    public static function create()
    {
        return new Card\UNG_015;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_015';
    }
}