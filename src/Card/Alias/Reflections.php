<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Reflections
{
    public static function globalId() : string
    {
        return 'KAR_A01_02H';
    }

    public static function create()
    {
        return new Card\KAR_A01_02H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_A01_02H';
    }
}