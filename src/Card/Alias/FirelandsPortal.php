<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FirelandsPortal
{
    public static function globalId() : string
    {
        return 'KAR_076';
    }

    public static function create()
    {
        return new Card\KAR_076;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_076';
    }
}