<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShadowWordHorror
{
    public static function globalId() : string
    {
        return 'OG_100';
    }

    public static function create()
    {
        return new Card\OG_100;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_100';
    }
}