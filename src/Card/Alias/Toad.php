<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Toad
{
    public static function globalId() : string
    {
        return 'TRL_351t';
    }

    public static function create()
    {
        return new Card\TRL_351t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_351t';
    }
}