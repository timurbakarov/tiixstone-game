<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EternalServitude
{
    public static function globalId() : string
    {
        return 'ICC_213';
    }

    public static function create()
    {
        return new Card\ICC_213;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_213';
    }
}