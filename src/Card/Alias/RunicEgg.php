<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RunicEgg
{
    public static function globalId() : string
    {
        return 'KAR_029';
    }

    public static function create()
    {
        return new Card\KAR_029;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_029';
    }
}