<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RagingContender
{
    public static function globalId() : string
    {
        return 'TRLA_193';
    }

    public static function create()
    {
        return new Card\TRLA_193;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_193';
    }
}