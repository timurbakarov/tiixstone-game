<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DirehornMatriarch
{
    public static function globalId() : string
    {
        return 'UNG_957t1';
    }

    public static function create()
    {
        return new Card\UNG_957t1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_957t1';
    }
}