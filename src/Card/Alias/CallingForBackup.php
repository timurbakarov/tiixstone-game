<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CallingForBackup
{
    public static function globalId() : string
    {
        return 'FB_LK004';
    }

    public static function create()
    {
        return new Card\FB_LK004;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK004';
    }
}