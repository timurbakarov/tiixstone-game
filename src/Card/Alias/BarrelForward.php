<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BarrelForward
{
    public static function globalId() : string
    {
        return 'LOEA07_21';
    }

    public static function create()
    {
        return new Card\LOEA07_21;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA07_21';
    }
}