<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Bat
{
    public static function globalId() : string
    {
        return 'GIL_508t';
    }

    public static function create()
    {
        return new Card\GIL_508t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_508t';
    }
}