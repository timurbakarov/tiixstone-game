<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Ratcatcher
{
    public static function globalId() : string
    {
        return 'GIL_515';
    }

    public static function create()
    {
        return new Card\GIL_515;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_515';
    }
}