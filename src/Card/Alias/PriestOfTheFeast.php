<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PriestOfTheFeast
{
    public static function globalId() : string
    {
        return 'KAR_035';
    }

    public static function create()
    {
        return new Card\KAR_035;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_035';
    }
}