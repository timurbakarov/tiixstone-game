<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PuzzleBox
{
    public static function globalId() : string
    {
        return 'GILA_910';
    }

    public static function create()
    {
        return new Card\GILA_910;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_910';
    }
}