<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HelloHelloHello
{
    public static function globalId() : string
    {
        return 'TB_MechWar_Boss1_HeroPower';
    }

    public static function create()
    {
        return new Card\TB_MechWar_Boss1_HeroPower;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_MechWar_Boss1_HeroPower';
    }
}