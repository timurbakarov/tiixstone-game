<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BlessingOfMight
{
    public static function globalId() : string
    {
        return 'CS2_087';
    }

    public static function create()
    {
        return new Card\CS2_087;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_087';
    }
}