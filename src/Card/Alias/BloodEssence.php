<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodEssence
{
    public static function globalId() : string
    {
        return 'ICCA05_021';
    }

    public static function create()
    {
        return new Card\ICCA05_021;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA05_021';
    }
}