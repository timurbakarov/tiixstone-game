<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ForestGuide
{
    public static function globalId() : string
    {
        return 'GIL_833';
    }

    public static function create()
    {
        return new Card\GIL_833;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_833';
    }
}