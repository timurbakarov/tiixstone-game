<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ThunderBluffValiant
{
    public static function globalId() : string
    {
        return 'AT_049';
    }

    public static function create()
    {
        return new Card\AT_049;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_049';
    }
}