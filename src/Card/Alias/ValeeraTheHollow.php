<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ValeeraTheHollow
{
    public static function globalId() : string
    {
        return 'ICC_827';
    }

    public static function create()
    {
        return new Card\ICC_827;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_827';
    }
}