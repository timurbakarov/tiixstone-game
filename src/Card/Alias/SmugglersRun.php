<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SmugglersRun
{
    public static function globalId() : string
    {
        return 'CFM_305';
    }

    public static function create()
    {
        return new Card\CFM_305;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_305';
    }
}