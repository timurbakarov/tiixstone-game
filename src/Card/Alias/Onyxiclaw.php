<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Onyxiclaw
{
    public static function globalId() : string
    {
        return 'BRMA17_9';
    }

    public static function create()
    {
        return new Card\BRMA17_9;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA17_9';
    }
}