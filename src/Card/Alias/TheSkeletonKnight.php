<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheSkeletonKnight
{
    public static function globalId() : string
    {
        return 'AT_128';
    }

    public static function create()
    {
        return new Card\AT_128;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_128';
    }
}