<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ScarabBeetle
{
    public static function globalId() : string
    {
        return 'ICC_832t4';
    }

    public static function create()
    {
        return new Card\ICC_832t4;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_832t4';
    }
}