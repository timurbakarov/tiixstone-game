<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Ozruk
{
    public static function globalId() : string
    {
        return 'UNG_907';
    }

    public static function create()
    {
        return new Card\UNG_907;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_907';
    }
}