<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HumongousRazorleaf
{
    public static function globalId() : string
    {
        return 'UNG_844';
    }

    public static function create()
    {
        return new Card\UNG_844;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_844';
    }
}