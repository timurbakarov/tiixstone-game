<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WitchwoodApple
{
    public static function globalId() : string
    {
        return 'GIL_663';
    }

    public static function create()
    {
        return new Card\GIL_663;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_663';
    }
}