<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RaptorPatriarch
{
    public static function globalId() : string
    {
        return 'UNG_914t1';
    }

    public static function create()
    {
        return new Card\UNG_914t1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_914t1';
    }
}