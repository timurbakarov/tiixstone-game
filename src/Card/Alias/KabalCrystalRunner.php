<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KabalCrystalRunner
{
    public static function globalId() : string
    {
        return 'CFM_760';
    }

    public static function create()
    {
        return new Card\CFM_760;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_760';
    }
}