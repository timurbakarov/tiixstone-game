<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KabalLackey
{
    public static function globalId() : string
    {
        return 'CFM_066';
    }

    public static function create()
    {
        return new Card\CFM_066;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_066';
    }
}