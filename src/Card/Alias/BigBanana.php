<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BigBanana
{
    public static function globalId() : string
    {
        return 'TB_006';
    }

    public static function create()
    {
        return new Card\TB_006;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_006';
    }
}