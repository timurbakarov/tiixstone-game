<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodscalpStrategist
{
    public static function globalId() : string
    {
        return 'TRL_349';
    }

    public static function create()
    {
        return new Card\TRL_349;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_349';
    }
}