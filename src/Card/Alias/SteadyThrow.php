<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SteadyThrow
{
    public static function globalId() : string
    {
        return 'TRLA_065p';
    }

    public static function create()
    {
        return new Card\TRLA_065p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_065p';
    }
}