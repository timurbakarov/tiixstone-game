<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Illuminator
{
    public static function globalId() : string
    {
        return 'GVG_089';
    }

    public static function create()
    {
        return new Card\GVG_089;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_089';
    }
}