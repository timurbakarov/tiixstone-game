<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SeethingStatue
{
    public static function globalId() : string
    {
        return 'LOEA04_25';
    }

    public static function create()
    {
        return new Card\LOEA04_25;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA04_25';
    }
}