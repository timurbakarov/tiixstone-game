<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheScarecrow
{
    public static function globalId() : string
    {
        return 'GILA_BOSS_33h';
    }

    public static function create()
    {
        return new Card\GILA_BOSS_33h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_BOSS_33h';
    }
}