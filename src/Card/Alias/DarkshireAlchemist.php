<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DarkshireAlchemist
{
    public static function globalId() : string
    {
        return 'OG_234';
    }

    public static function create()
    {
        return new Card\OG_234;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_234';
    }
}