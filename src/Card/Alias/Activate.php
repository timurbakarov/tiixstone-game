<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Activate
{
    public static function globalId() : string
    {
        return 'BRMA14_10H_TB';
    }

    public static function create()
    {
        return new Card\BRMA14_10H_TB;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA14_10H_TB';
    }
}