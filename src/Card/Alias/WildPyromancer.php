<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WildPyromancer
{
    public static function globalId() : string
    {
        return 'NEW1_020';
    }

    public static function create()
    {
        return new Card\NEW1_020;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NEW1_020';
    }
}