<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Peruse
{
    public static function globalId() : string
    {
        return 'TB_DiscoverMyDeck_Discovery';
    }

    public static function create()
    {
        return new Card\TB_DiscoverMyDeck_Discovery;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_DiscoverMyDeck_Discovery';
    }
}