<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AkalisWarDrum
{
    public static function globalId() : string
    {
        return 'TRLA_170';
    }

    public static function create()
    {
        return new Card\TRLA_170;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_170';
    }
}