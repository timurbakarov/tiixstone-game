<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FortifiedSpirit
{
    public static function globalId() : string
    {
        return 'TRLA_803';
    }

    public static function create()
    {
        return new Card\TRLA_803;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_803';
    }
}