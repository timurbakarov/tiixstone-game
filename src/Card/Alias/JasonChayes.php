<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JasonChayes
{
    public static function globalId() : string
    {
        return 'CRED_01';
    }

    public static function create()
    {
        return new Card\CRED_01;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_01';
    }
}