<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GraveVengeance
{
    public static function globalId() : string
    {
        return 'ICC_829t';
    }

    public static function create()
    {
        return new Card\ICC_829t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_829t';
    }
}