<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DrakkariDefender
{
    public static function globalId() : string
    {
        return 'ICC_081';
    }

    public static function create()
    {
        return new Card\ICC_081;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_081';
    }
}