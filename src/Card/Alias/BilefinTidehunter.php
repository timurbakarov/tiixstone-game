<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BilefinTidehunter
{
    public static function globalId() : string
    {
        return 'OG_156';
    }

    public static function create()
    {
        return new Card\OG_156;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_156';
    }
}