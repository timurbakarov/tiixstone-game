<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ChooseANewCard
{
    public static function globalId() : string
    {
        return 'TB_012';
    }

    public static function create()
    {
        return new Card\TB_012;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_012';
    }
}