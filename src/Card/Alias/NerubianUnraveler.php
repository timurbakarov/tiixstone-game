<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NerubianUnraveler
{
    public static function globalId() : string
    {
        return 'ICC_706';
    }

    public static function create()
    {
        return new Card\ICC_706;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_706';
    }
}