<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StonehillDefender
{
    public static function globalId() : string
    {
        return 'UNG_072';
    }

    public static function create()
    {
        return new Card\UNG_072;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_072';
    }
}