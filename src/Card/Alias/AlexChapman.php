<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AlexChapman
{
    public static function globalId() : string
    {
        return 'CRED_52';
    }

    public static function create()
    {
        return new Card\CRED_52;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_52';
    }
}