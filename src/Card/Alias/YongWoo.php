<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class YongWoo
{
    public static function globalId() : string
    {
        return 'CRED_14';
    }

    public static function create()
    {
        return new Card\CRED_14;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_14';
    }
}