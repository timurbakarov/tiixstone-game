<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CorpseWidow
{
    public static function globalId() : string
    {
        return 'ICC_243';
    }

    public static function create()
    {
        return new Card\ICC_243;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_243';
    }
}