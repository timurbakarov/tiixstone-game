<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DruidOfTheSaber
{
    public static function globalId() : string
    {
        return 'AT_042';
    }

    public static function create()
    {
        return new Card\AT_042;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_042';
    }
}