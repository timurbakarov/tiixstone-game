<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TicketScalper
{
    public static function globalId() : string
    {
        return 'TRL_015';
    }

    public static function create()
    {
        return new Card\TRL_015;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_015';
    }
}