<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TransformingTracker
{
    public static function globalId() : string
    {
        return 'GILA_851b';
    }

    public static function create()
    {
        return new Card\GILA_851b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_851b';
    }
}