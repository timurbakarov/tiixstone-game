<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ReverberatingGong
{
    public static function globalId() : string
    {
        return 'BRMA16_4';
    }

    public static function create()
    {
        return new Card\BRMA16_4;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA16_4';
    }
}