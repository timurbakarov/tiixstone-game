<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OrgrimmarAspirant
{
    public static function globalId() : string
    {
        return 'AT_066';
    }

    public static function create()
    {
        return new Card\AT_066;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_066';
    }
}