<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DevourMind
{
    public static function globalId() : string
    {
        return 'ICC_207';
    }

    public static function create()
    {
        return new Card\ICC_207;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_207';
    }
}