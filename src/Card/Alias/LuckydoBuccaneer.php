<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LuckydoBuccaneer
{
    public static function globalId() : string
    {
        return 'CFM_342';
    }

    public static function create()
    {
        return new Card\CFM_342;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_342';
    }
}