<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MarshDrake
{
    public static function globalId() : string
    {
        return 'GIL_683';
    }

    public static function create()
    {
        return new Card\GIL_683;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_683';
    }
}