<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RavasaurRunt
{
    public static function globalId() : string
    {
        return 'UNG_009';
    }

    public static function create()
    {
        return new Card\UNG_009;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_009';
    }
}