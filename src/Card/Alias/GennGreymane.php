<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GennGreymane
{
    public static function globalId() : string
    {
        return 'GIL_692';
    }

    public static function create()
    {
        return new Card\GIL_692;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_692';
    }
}