<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FlashOfLight
{
    public static function globalId() : string
    {
        return 'TRL_307';
    }

    public static function create()
    {
        return new Card\TRL_307;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_307';
    }
}