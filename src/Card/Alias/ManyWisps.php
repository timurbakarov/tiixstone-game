<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ManyWisps
{
    public static function globalId() : string
    {
        return 'OG_195a';
    }

    public static function create()
    {
        return new Card\OG_195a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_195a';
    }
}