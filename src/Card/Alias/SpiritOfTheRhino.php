<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpiritOfTheRhino
{
    public static function globalId() : string
    {
        return 'TRL_327';
    }

    public static function create()
    {
        return new Card\TRL_327;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_327';
    }
}