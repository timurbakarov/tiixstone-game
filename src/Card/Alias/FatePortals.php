<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FatePortals
{
    public static function globalId() : string
    {
        return 'TB_PickYourFate_6';
    }

    public static function create()
    {
        return new Card\TB_PickYourFate_6;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_PickYourFate_6';
    }
}