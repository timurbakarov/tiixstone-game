<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PickPocket
{
    public static function globalId() : string
    {
        return 'GIL_696';
    }

    public static function create()
    {
        return new Card\GIL_696;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_696';
    }
}