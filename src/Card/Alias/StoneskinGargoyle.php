<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StoneskinGargoyle
{
    public static function globalId() : string
    {
        return 'FP1_027';
    }

    public static function create()
    {
        return new Card\FP1_027;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FP1_027';
    }
}