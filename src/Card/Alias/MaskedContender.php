<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MaskedContender
{
    public static function globalId() : string
    {
        return 'TRL_530';
    }

    public static function create()
    {
        return new Card\TRL_530;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_530';
    }
}