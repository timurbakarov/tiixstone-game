<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TentacleOfNZoth
{
    public static function globalId() : string
    {
        return 'OG_151';
    }

    public static function create()
    {
        return new Card\OG_151;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_151';
    }
}