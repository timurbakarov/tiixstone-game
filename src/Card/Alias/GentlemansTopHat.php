<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GentlemansTopHat
{
    public static function globalId() : string
    {
        return 'GILA_825';
    }

    public static function create()
    {
        return new Card\GILA_825;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_825';
    }
}