<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KrisZierhut
{
    public static function globalId() : string
    {
        return 'CRED_49';
    }

    public static function create()
    {
        return new Card\CRED_49;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_49';
    }
}