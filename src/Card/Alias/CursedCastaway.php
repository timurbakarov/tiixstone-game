<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CursedCastaway
{
    public static function globalId() : string
    {
        return 'GIL_557';
    }

    public static function create()
    {
        return new Card\GIL_557;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_557';
    }
}