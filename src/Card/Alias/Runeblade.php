<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Runeblade
{
    public static function globalId() : string
    {
        return 'NAX9_05';
    }

    public static function create()
    {
        return new Card\NAX9_05;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX9_05';
    }
}