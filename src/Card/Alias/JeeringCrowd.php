<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JeeringCrowd
{
    public static function globalId() : string
    {
        return 'BRMA02_2_2_TB';
    }

    public static function create()
    {
        return new Card\BRMA02_2_2_TB;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA02_2_2_TB';
    }
}