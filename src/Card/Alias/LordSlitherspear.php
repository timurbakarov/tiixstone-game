<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LordSlitherspear
{
    public static function globalId() : string
    {
        return 'LOEA16_23H';
    }

    public static function create()
    {
        return new Card\LOEA16_23H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA16_23H';
    }
}