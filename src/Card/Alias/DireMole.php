<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DireMole
{
    public static function globalId() : string
    {
        return 'LOOT_258';
    }

    public static function create()
    {
        return new Card\LOOT_258;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOOT_258';
    }
}