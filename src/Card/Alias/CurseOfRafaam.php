<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CurseOfRafaam
{
    public static function globalId() : string
    {
        return 'LOE_007';
    }

    public static function create()
    {
        return new Card\LOE_007;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_007';
    }
}