<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WhiteEyes
{
    public static function globalId() : string
    {
        return 'CFM_324';
    }

    public static function create()
    {
        return new Card\CFM_324;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_324';
    }
}