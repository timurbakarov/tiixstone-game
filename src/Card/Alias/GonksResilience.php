<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GonksResilience
{
    public static function globalId() : string
    {
        return 'TRL_254a';
    }

    public static function create()
    {
        return new Card\TRL_254a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_254a';
    }
}