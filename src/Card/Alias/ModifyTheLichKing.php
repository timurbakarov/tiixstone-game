<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ModifyTheLichKing
{
    public static function globalId() : string
    {
        return 'FB_LKStats001d';
    }

    public static function create()
    {
        return new Card\FB_LKStats001d;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LKStats001d';
    }
}