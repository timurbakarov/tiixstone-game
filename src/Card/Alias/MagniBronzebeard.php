<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MagniBronzebeard
{
    public static function globalId() : string
    {
        return 'HERO_01a';
    }

    public static function create()
    {
        return new Card\HERO_01a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HERO_01a';
    }
}