<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Rush
{
    public static function globalId() : string
    {
        return 'TRLA_Warrior_06';
    }

    public static function create()
    {
        return new Card\TRLA_Warrior_06;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Warrior_06';
    }
}