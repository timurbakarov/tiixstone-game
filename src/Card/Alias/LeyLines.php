<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LeyLines
{
    public static function globalId() : string
    {
        return 'KARA_12_02H';
    }

    public static function create()
    {
        return new Card\KARA_12_02H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_12_02H';
    }
}