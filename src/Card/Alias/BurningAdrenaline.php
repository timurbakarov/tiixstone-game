<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BurningAdrenaline
{
    public static function globalId() : string
    {
        return 'BRMA11_3';
    }

    public static function create()
    {
        return new Card\BRMA11_3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA11_3';
    }
}