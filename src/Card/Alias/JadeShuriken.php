<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JadeShuriken
{
    public static function globalId() : string
    {
        return 'CFM_690';
    }

    public static function create()
    {
        return new Card\CFM_690;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_690';
    }
}