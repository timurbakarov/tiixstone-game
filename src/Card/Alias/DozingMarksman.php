<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DozingMarksman
{
    public static function globalId() : string
    {
        return 'TRL_406';
    }

    public static function create()
    {
        return new Card\TRL_406;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_406';
    }
}