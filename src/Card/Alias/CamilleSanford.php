<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CamilleSanford
{
    public static function globalId() : string
    {
        return 'CRED_53';
    }

    public static function create()
    {
        return new Card\CRED_53;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_53';
    }
}