<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RecklessDiretroll
{
    public static function globalId() : string
    {
        return 'TRL_551';
    }

    public static function create()
    {
        return new Card\TRL_551;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_551';
    }
}