<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HeadlessHorsemansHead
{
    public static function globalId() : string
    {
        return 'TB_HeadlessHorseman_001';
    }

    public static function create()
    {
        return new Card\TB_HeadlessHorseman_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_HeadlessHorseman_001';
    }
}