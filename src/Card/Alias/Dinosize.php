<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Dinosize
{
    public static function globalId() : string
    {
        return 'UNG_004';
    }

    public static function create()
    {
        return new Card\UNG_004;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_004';
    }
}