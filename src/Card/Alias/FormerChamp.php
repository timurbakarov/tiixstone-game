<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FormerChamp
{
    public static function globalId() : string
    {
        return 'TRL_151';
    }

    public static function create()
    {
        return new Card\TRL_151;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_151';
    }
}