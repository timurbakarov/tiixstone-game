<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KabalDemon
{
    public static function globalId() : string
    {
        return 'CFM_621_m3';
    }

    public static function create()
    {
        return new Card\CFM_621_m3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_621_m3';
    }
}