<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MalfurionStormrage
{
    public static function globalId() : string
    {
        return 'HERO_06';
    }

    public static function create()
    {
        return new Card\HERO_06;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HERO_06';
    }
}