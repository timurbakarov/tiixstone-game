<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CannonBarrage
{
    public static function globalId() : string
    {
        return 'TRL_127';
    }

    public static function create()
    {
        return new Card\TRL_127;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_127';
    }
}