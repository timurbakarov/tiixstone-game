<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Demonwrath
{
    public static function globalId() : string
    {
        return 'BRM_005';
    }

    public static function create()
    {
        return new Card\BRM_005;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_005';
    }
}