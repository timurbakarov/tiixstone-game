<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Waterboy
{
    public static function globalId() : string
    {
        return 'TRL_407';
    }

    public static function create()
    {
        return new Card\TRL_407;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_407';
    }
}