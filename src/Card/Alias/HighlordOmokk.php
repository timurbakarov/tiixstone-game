<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HighlordOmokk
{
    public static function globalId() : string
    {
        return 'BRMA07_1';
    }

    public static function create()
    {
        return new Card\BRMA07_1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA07_1';
    }
}