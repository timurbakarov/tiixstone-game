<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DerangedDoctor
{
    public static function globalId() : string
    {
        return 'GIL_118';
    }

    public static function create()
    {
        return new Card\GIL_118;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_118';
    }
}