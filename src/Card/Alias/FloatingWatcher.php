<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FloatingWatcher
{
    public static function globalId() : string
    {
        return 'GVG_100';
    }

    public static function create()
    {
        return new Card\GVG_100;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_100';
    }
}