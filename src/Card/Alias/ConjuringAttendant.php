<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ConjuringAttendant
{
    public static function globalId() : string
    {
        return 'TRLA_153';
    }

    public static function create()
    {
        return new Card\TRLA_153;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_153';
    }
}