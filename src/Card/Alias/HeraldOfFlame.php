<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HeraldOfFlame
{
    public static function globalId() : string
    {
        return 'TRLA_176';
    }

    public static function create()
    {
        return new Card\TRLA_176;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_176';
    }
}