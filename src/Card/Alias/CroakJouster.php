<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CroakJouster
{
    public static function globalId() : string
    {
        return 'TRLA_159';
    }

    public static function create()
    {
        return new Card\TRLA_159;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_159';
    }
}