<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TemporalLoop
{
    public static function globalId() : string
    {
        return 'GILA_900p';
    }

    public static function create()
    {
        return new Card\GILA_900p;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_900p';
    }
}