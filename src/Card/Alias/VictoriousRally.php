<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VictoriousRally
{
    public static function globalId() : string
    {
        return 'GILA_602';
    }

    public static function create()
    {
        return new Card\GILA_602;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_602';
    }
}