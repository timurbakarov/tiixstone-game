<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TownCrier
{
    public static function globalId() : string
    {
        return 'GIL_580';
    }

    public static function create()
    {
        return new Card\GIL_580;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_580';
    }
}