<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BwonsamdisKeeper
{
    public static function globalId() : string
    {
        return 'TRLA_151';
    }

    public static function create()
    {
        return new Card\TRLA_151;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_151';
    }
}