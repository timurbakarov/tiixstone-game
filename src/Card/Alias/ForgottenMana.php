<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ForgottenMana
{
    public static function globalId() : string
    {
        return 'CFM_308b';
    }

    public static function create()
    {
        return new Card\CFM_308b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_308b';
    }
}