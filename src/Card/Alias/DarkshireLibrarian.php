<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DarkshireLibrarian
{
    public static function globalId() : string
    {
        return 'OG_109';
    }

    public static function create()
    {
        return new Card\OG_109;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_109';
    }
}