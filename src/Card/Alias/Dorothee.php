<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Dorothee
{
    public static function globalId() : string
    {
        return 'TB_Dorothee_001';
    }

    public static function create()
    {
        return new Card\TB_Dorothee_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_Dorothee_001';
    }
}