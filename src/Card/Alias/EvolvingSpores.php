<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EvolvingSpores
{
    public static function globalId() : string
    {
        return 'UNG_103';
    }

    public static function create()
    {
        return new Card\UNG_103;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_103';
    }
}