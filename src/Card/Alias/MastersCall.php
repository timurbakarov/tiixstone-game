<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MastersCall
{
    public static function globalId() : string
    {
        return 'TRL_339';
    }

    public static function create()
    {
        return new Card\TRL_339;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_339';
    }
}