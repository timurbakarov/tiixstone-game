<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MoltenBlade
{
    public static function globalId() : string
    {
        return 'UNG_929';
    }

    public static function create()
    {
        return new Card\UNG_929;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_929';
    }
}