<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VeteransMilitiaHorn
{
    public static function globalId() : string
    {
        return 'GILA_852c';
    }

    public static function create()
    {
        return new Card\GILA_852c;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_852c';
    }
}