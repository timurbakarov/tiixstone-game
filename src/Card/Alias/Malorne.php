<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Malorne
{
    public static function globalId() : string
    {
        return 'GVG_035';
    }

    public static function create()
    {
        return new Card\GVG_035;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_035';
    }
}