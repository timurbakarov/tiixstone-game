<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LeaderOfThePack
{
    public static function globalId() : string
    {
        return 'EX1_160b';
    }

    public static function create()
    {
        return new Card\EX1_160b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_160b';
    }
}