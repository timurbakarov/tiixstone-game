<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class UnGoroPack
{
    public static function globalId() : string
    {
        return 'UNG_851t1';
    }

    public static function create()
    {
        return new Card\UNG_851t1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_851t1';
    }
}