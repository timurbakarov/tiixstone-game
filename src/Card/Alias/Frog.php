<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Frog
{
    public static function globalId() : string
    {
        return 'hexfrog';
    }

    public static function create()
    {
        return new Card\hexfrog;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\hexfrog';
    }
}