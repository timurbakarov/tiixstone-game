<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LikeASoreThumb
{
    public static function globalId() : string
    {
        return 'TB_MammothParty_s101b';
    }

    public static function create()
    {
        return new Card\TB_MammothParty_s101b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_MammothParty_s101b';
    }
}