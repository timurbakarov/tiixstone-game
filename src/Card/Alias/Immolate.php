<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Immolate
{
    public static function globalId() : string
    {
        return 'TB_CoOpv3_013';
    }

    public static function create()
    {
        return new Card\TB_CoOpv3_013;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_CoOpv3_013';
    }
}