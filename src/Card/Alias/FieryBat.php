<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FieryBat
{
    public static function globalId() : string
    {
        return 'OG_179';
    }

    public static function create()
    {
        return new Card\OG_179;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_179';
    }
}