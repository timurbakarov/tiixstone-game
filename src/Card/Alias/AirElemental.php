<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AirElemental
{
    public static function globalId() : string
    {
        return 'UNG_019';
    }

    public static function create()
    {
        return new Card\UNG_019;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_019';
    }
}