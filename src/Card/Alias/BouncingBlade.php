<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BouncingBlade
{
    public static function globalId() : string
    {
        return 'GVG_050';
    }

    public static function create()
    {
        return new Card\GVG_050;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_050';
    }
}