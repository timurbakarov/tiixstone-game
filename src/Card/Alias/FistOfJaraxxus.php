<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FistOfJaraxxus
{
    public static function globalId() : string
    {
        return 'AT_022';
    }

    public static function create()
    {
        return new Card\AT_022;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_022';
    }
}