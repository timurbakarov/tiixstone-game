<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Quartermaster
{
    public static function globalId() : string
    {
        return 'GVG_060';
    }

    public static function create()
    {
        return new Card\GVG_060;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_060';
    }
}