<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Arcanologist
{
    public static function globalId() : string
    {
        return 'UNG_020';
    }

    public static function create()
    {
        return new Card\UNG_020;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_020';
    }
}