<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DesertCamel
{
    public static function globalId() : string
    {
        return 'LOE_020';
    }

    public static function create()
    {
        return new Card\LOE_020;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_020';
    }
}