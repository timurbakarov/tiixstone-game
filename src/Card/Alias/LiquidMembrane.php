<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LiquidMembrane
{
    public static function globalId() : string
    {
        return 'UNG_999t5';
    }

    public static function create()
    {
        return new Card\UNG_999t5;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_999t5';
    }
}