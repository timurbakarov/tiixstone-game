<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Dreadsteed
{
    public static function globalId() : string
    {
        return 'AT_019';
    }

    public static function create()
    {
        return new Card\AT_019;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_019';
    }
}