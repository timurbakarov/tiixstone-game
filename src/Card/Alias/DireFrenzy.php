<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DireFrenzy
{
    public static function globalId() : string
    {
        return 'GIL_828';
    }

    public static function create()
    {
        return new Card\GIL_828;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_828';
    }
}