<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Shieldbreaker
{
    public static function globalId() : string
    {
        return 'TRL_524';
    }

    public static function create()
    {
        return new Card\TRL_524;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_524';
    }
}