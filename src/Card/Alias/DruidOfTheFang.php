<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DruidOfTheFang
{
    public static function globalId() : string
    {
        return 'GVG_080';
    }

    public static function create()
    {
        return new Card\GVG_080;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_080';
    }
}