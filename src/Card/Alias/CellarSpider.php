<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CellarSpider
{
    public static function globalId() : string
    {
        return 'KAR_030';
    }

    public static function create()
    {
        return new Card\KAR_030;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_030';
    }
}