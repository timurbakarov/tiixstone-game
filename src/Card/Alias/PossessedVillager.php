<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PossessedVillager
{
    public static function globalId() : string
    {
        return 'OG_241';
    }

    public static function create()
    {
        return new Card\OG_241;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_241';
    }
}