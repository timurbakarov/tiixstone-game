<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ChristopherYim
{
    public static function globalId() : string
    {
        return 'CRED_23';
    }

    public static function create()
    {
        return new Card\CRED_23;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_23';
    }
}