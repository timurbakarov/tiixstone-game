<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Hydrologist
{
    public static function globalId() : string
    {
        return 'UNG_011';
    }

    public static function create()
    {
        return new Card\UNG_011;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_011';
    }
}