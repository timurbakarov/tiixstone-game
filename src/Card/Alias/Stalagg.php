<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Stalagg
{
    public static function globalId() : string
    {
        return 'NAX13_05H';
    }

    public static function create()
    {
        return new Card\NAX13_05H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX13_05H';
    }
}