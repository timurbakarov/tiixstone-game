<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Treant
{
    public static function globalId() : string
    {
        return 'GIL_663t';
    }

    public static function create()
    {
        return new Card\GIL_663t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_663t';
    }
}