<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SilverHandRecruit
{
    public static function globalId() : string
    {
        return 'CS2_101t';
    }

    public static function create()
    {
        return new Card\CS2_101t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_101t';
    }
}