<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DemigodsFavor
{
    public static function globalId() : string
    {
        return 'EX1_573a';
    }

    public static function create()
    {
        return new Card\EX1_573a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_573a';
    }
}