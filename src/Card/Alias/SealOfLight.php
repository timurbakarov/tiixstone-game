<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SealOfLight
{
    public static function globalId() : string
    {
        return 'GVG_057';
    }

    public static function create()
    {
        return new Card\GVG_057;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_057';
    }
}