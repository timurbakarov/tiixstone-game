<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VoidContract
{
    public static function globalId() : string
    {
        return 'TRL_246';
    }

    public static function create()
    {
        return new Card\TRL_246;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_246';
    }
}