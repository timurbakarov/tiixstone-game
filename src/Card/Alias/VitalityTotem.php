<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VitalityTotem
{
    public static function globalId() : string
    {
        return 'GVG_039';
    }

    public static function create()
    {
        return new Card\GVG_039;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_039';
    }
}