<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SeaReaver
{
    public static function globalId() : string
    {
        return 'AT_130';
    }

    public static function create()
    {
        return new Card\AT_130;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_130';
    }
}