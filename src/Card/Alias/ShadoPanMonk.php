<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShadoPanMonk
{
    public static function globalId() : string
    {
        return 'TU4f_003';
    }

    public static function create()
    {
        return new Card\TU4f_003;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TU4f_003';
    }
}