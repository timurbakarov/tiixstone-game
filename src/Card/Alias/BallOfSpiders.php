<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BallOfSpiders
{
    public static function globalId() : string
    {
        return 'AT_062';
    }

    public static function create()
    {
        return new Card\AT_062;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_062';
    }
}