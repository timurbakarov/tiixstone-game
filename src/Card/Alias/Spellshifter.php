<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Spellshifter
{
    public static function globalId() : string
    {
        return 'GIL_529t';
    }

    public static function create()
    {
        return new Card\GIL_529t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_529t';
    }
}