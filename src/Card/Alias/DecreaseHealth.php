<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DecreaseHealth
{
    public static function globalId() : string
    {
        return 'FB_LKStats002b';
    }

    public static function create()
    {
        return new Card\FB_LKStats002b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LKStats002b';
    }
}