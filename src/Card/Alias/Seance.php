<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Seance
{
    public static function globalId() : string
    {
        return 'TRL_097';
    }

    public static function create()
    {
        return new Card\TRL_097;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_097';
    }
}