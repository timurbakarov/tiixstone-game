<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BrassKnuckles
{
    public static function globalId() : string
    {
        return 'CFM_631';
    }

    public static function create()
    {
        return new Card\CFM_631;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_631';
    }
}