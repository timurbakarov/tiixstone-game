<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Poultryizer
{
    public static function globalId() : string
    {
        return 'Mekka4';
    }

    public static function create()
    {
        return new Card\Mekka4;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\Mekka4';
    }
}