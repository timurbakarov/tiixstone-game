<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodsailRaider
{
    public static function globalId() : string
    {
        return 'NEW1_018';
    }

    public static function create()
    {
        return new Card\NEW1_018;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NEW1_018';
    }
}