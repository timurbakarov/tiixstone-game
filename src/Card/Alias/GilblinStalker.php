<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GilblinStalker
{
    public static function globalId() : string
    {
        return 'GVG_081';
    }

    public static function create()
    {
        return new Card\GVG_081;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_081';
    }
}