<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JeweledMacaw
{
    public static function globalId() : string
    {
        return 'UNG_912';
    }

    public static function create()
    {
        return new Card\UNG_912;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_912';
    }
}