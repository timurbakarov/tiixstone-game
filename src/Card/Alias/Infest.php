<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Infest
{
    public static function globalId() : string
    {
        return 'OG_045';
    }

    public static function create()
    {
        return new Card\OG_045;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_045';
    }
}