<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FlamewakerAcolyte
{
    public static function globalId() : string
    {
        return 'BRMA06_4H';
    }

    public static function create()
    {
        return new Card\BRMA06_4H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA06_4H';
    }
}