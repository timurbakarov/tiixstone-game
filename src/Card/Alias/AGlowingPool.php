<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AGlowingPool
{
    public static function globalId() : string
    {
        return 'LOEA04_28';
    }

    public static function create()
    {
        return new Card\LOEA04_28;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA04_28';
    }
}