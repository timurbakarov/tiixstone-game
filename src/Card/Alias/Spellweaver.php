<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Spellweaver
{
    public static function globalId() : string
    {
        return 'ICC_856';
    }

    public static function create()
    {
        return new Card\ICC_856;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_856';
    }
}