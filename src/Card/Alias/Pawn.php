<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Pawn
{
    public static function globalId() : string
    {
        return 'KAR_026t';
    }

    public static function create()
    {
        return new Card\KAR_026t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_026t';
    }
}