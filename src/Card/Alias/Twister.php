<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Twister
{
    public static function globalId() : string
    {
        return 'KARA_04_02hp';
    }

    public static function create()
    {
        return new Card\KARA_04_02hp;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_04_02hp';
    }
}