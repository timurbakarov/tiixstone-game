<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BellringerSentry
{
    public static function globalId() : string
    {
        return 'GIL_634';
    }

    public static function create()
    {
        return new Card\GIL_634;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_634';
    }
}