<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StakeThrower
{
    public static function globalId() : string
    {
        return 'GILA_500t';
    }

    public static function create()
    {
        return new Card\GILA_500t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_500t';
    }
}