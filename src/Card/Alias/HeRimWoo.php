<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HeRimWoo
{
    public static function globalId() : string
    {
        return 'CRED_28';
    }

    public static function create()
    {
        return new Card\CRED_28;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_28';
    }
}