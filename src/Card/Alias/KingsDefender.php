<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KingsDefender
{
    public static function globalId() : string
    {
        return 'AT_065';
    }

    public static function create()
    {
        return new Card\AT_065;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_065';
    }
}