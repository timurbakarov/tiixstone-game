<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HeftySackOfCoins
{
    public static function globalId() : string
    {
        return 'GILA_816c';
    }

    public static function create()
    {
        return new Card\GILA_816c;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_816c';
    }
}