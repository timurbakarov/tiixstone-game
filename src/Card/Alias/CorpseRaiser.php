<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CorpseRaiser
{
    public static function globalId() : string
    {
        return 'ICC_257';
    }

    public static function create()
    {
        return new Card\ICC_257;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_257';
    }
}