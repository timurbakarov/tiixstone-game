<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SquirmingTentacle
{
    public static function globalId() : string
    {
        return 'OG_327';
    }

    public static function create()
    {
        return new Card\OG_327;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_327';
    }
}