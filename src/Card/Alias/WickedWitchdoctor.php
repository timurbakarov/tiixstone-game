<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WickedWitchdoctor
{
    public static function globalId() : string
    {
        return 'KAR_021';
    }

    public static function create()
    {
        return new Card\KAR_021;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_021';
    }
}