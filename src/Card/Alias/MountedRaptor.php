<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MountedRaptor
{
    public static function globalId() : string
    {
        return 'LOE_050';
    }

    public static function create()
    {
        return new Card\LOE_050;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_050';
    }
}