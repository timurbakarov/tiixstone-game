<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FacelessSummoner
{
    public static function globalId() : string
    {
        return 'OG_207';
    }

    public static function create()
    {
        return new Card\OG_207;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_207';
    }
}