<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BoneDrake
{
    public static function globalId() : string
    {
        return 'ICC_027';
    }

    public static function create()
    {
        return new Card\ICC_027;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_027';
    }
}