<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AncientTeachings
{
    public static function globalId() : string
    {
        return 'NEW1_008a';
    }

    public static function create()
    {
        return new Card\NEW1_008a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NEW1_008a';
    }
}