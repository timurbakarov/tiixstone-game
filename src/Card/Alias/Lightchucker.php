<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Lightchucker
{
    public static function globalId() : string
    {
        return 'TRLA_141';
    }

    public static function create()
    {
        return new Card\TRLA_141;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_141';
    }
}