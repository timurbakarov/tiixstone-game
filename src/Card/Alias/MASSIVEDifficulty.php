<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MASSIVEDifficulty
{
    public static function globalId() : string
    {
        return 'FB_LK_BossSetup001a';
    }

    public static function create()
    {
        return new Card\FB_LK_BossSetup001a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK_BossSetup001a';
    }
}