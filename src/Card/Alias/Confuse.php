<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Confuse
{
    public static function globalId() : string
    {
        return 'AT_016';
    }

    public static function create()
    {
        return new Card\AT_016;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_016';
    }
}