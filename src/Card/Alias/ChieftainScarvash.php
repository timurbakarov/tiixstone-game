<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ChieftainScarvash
{
    public static function globalId() : string
    {
        return 'LOEA16_21H';
    }

    public static function create()
    {
        return new Card\LOEA16_21H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA16_21H';
    }
}