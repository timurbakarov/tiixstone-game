<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Julianne
{
    public static function globalId() : string
    {
        return 'KARA_06_02heroic';
    }

    public static function create()
    {
        return new Card\KARA_06_02heroic;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_06_02heroic';
    }
}