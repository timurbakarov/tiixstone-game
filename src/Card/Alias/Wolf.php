<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Wolf
{
    public static function globalId() : string
    {
        return 'LOOT_077t';
    }

    public static function create()
    {
        return new Card\LOOT_077t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOOT_077t';
    }
}