<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PollutedHoarder
{
    public static function globalId() : string
    {
        return 'OG_323';
    }

    public static function create()
    {
        return new Card\OG_323;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_323';
    }
}