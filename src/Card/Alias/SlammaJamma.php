<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SlammaJamma
{
    public static function globalId() : string
    {
        return 'TRLA_169';
    }

    public static function create()
    {
        return new Card\TRLA_169;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_169';
    }
}