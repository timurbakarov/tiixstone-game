<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Coldwraith
{
    public static function globalId() : string
    {
        return 'ICC_252';
    }

    public static function create()
    {
        return new Card\ICC_252;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_252';
    }
}