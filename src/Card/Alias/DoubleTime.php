<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DoubleTime
{
    public static function globalId() : string
    {
        return 'GILA_913';
    }

    public static function create()
    {
        return new Card\GILA_913;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_913';
    }
}