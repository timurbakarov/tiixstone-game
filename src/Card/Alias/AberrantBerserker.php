<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AberrantBerserker
{
    public static function globalId() : string
    {
        return 'OG_150';
    }

    public static function create()
    {
        return new Card\OG_150;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_150';
    }
}