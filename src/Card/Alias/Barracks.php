<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Barracks
{
    public static function globalId() : string
    {
        return 'TB_SPT_BossHeroPower';
    }

    public static function create()
    {
        return new Card\TB_SPT_BossHeroPower;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_SPT_BossHeroPower';
    }
}