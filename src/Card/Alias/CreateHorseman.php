<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CreateHorseman
{
    public static function globalId() : string
    {
        return 'TB_HeadlessHorseman_XXX';
    }

    public static function create()
    {
        return new Card\TB_HeadlessHorseman_XXX;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_HeadlessHorseman_XXX';
    }
}