<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class YShaarjsStrength
{
    public static function globalId() : string
    {
        return 'OG_202a';
    }

    public static function create()
    {
        return new Card\OG_202a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_202a';
    }
}