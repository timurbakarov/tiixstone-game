<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SummonAPanther
{
    public static function globalId() : string
    {
        return 'EX1_160a';
    }

    public static function create()
    {
        return new Card\EX1_160a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_160a';
    }
}