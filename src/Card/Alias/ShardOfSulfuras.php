<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShardOfSulfuras
{
    public static function globalId() : string
    {
        return 'LOEA16_6';
    }

    public static function create()
    {
        return new Card\LOEA16_6;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA16_6';
    }
}