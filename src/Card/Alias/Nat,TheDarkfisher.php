<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NatTheDarkfisher
{
    public static function globalId() : string
    {
        return 'OG_338';
    }

    public static function create()
    {
        return new Card\OG_338;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_338';
    }
}