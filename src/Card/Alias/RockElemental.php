<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RockElemental
{
    public static function globalId() : string
    {
        return 'BRMC_99e';
    }

    public static function create()
    {
        return new Card\BRMC_99e;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_99e';
    }
}