<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PoisonCloud
{
    public static function globalId() : string
    {
        return 'NAX11_02H_2_TB';
    }

    public static function create()
    {
        return new Card\NAX11_02H_2_TB;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX11_02H_2_TB';
    }
}