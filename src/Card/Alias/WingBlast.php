<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WingBlast
{
    public static function globalId() : string
    {
        return 'GIL_518';
    }

    public static function create()
    {
        return new Card\GIL_518;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_518';
    }
}