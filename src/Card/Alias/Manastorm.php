<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Manastorm
{
    public static function globalId() : string
    {
        return 'KARA_11_02';
    }

    public static function create()
    {
        return new Card\KARA_11_02;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_11_02';
    }
}