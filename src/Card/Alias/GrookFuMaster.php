<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GrookFuMaster
{
    public static function globalId() : string
    {
        return 'CFM_666';
    }

    public static function create()
    {
        return new Card\CFM_666;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_666';
    }
}