<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WalkAcrossGingerly
{
    public static function globalId() : string
    {
        return 'LOEA04_06b';
    }

    public static function create()
    {
        return new Card\LOEA04_06b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA04_06b';
    }
}