<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DarkIronSkulker
{
    public static function globalId() : string
    {
        return 'BRM_008';
    }

    public static function create()
    {
        return new Card\BRM_008;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_008';
    }
}