<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MeanstreetMarshal
{
    public static function globalId() : string
    {
        return 'CFM_759';
    }

    public static function create()
    {
        return new Card\CFM_759;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_759';
    }
}