<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PrinceTaldaram
{
    public static function globalId() : string
    {
        return 'ICC_852';
    }

    public static function create()
    {
        return new Card\ICC_852;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_852';
    }
}