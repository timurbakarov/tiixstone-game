<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HoundmasterShaw
{
    public static function globalId() : string
    {
        return 'GIL_650';
    }

    public static function create()
    {
        return new Card\GIL_650;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_650';
    }
}