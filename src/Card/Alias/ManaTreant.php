<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ManaTreant
{
    public static function globalId() : string
    {
        return 'UNG_111t1';
    }

    public static function create()
    {
        return new Card\UNG_111t1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_111t1';
    }
}