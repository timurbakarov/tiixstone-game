<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Moroes
{
    public static function globalId() : string
    {
        return 'KAR_044';
    }

    public static function create()
    {
        return new Card\KAR_044;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_044';
    }
}