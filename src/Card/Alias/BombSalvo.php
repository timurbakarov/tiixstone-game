<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BombSalvo
{
    public static function globalId() : string
    {
        return 'TB_CoOpBossSpell_2';
    }

    public static function create()
    {
        return new Card\TB_CoOpBossSpell_2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_CoOpBossSpell_2';
    }
}