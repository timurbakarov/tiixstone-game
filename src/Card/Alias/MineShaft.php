<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MineShaft
{
    public static function globalId() : string
    {
        return 'LOEA07_02h';
    }

    public static function create()
    {
        return new Card\LOEA07_02h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA07_02h';
    }
}