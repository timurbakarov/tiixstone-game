<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NecroticPoison
{
    public static function globalId() : string
    {
        return 'NAX3_03';
    }

    public static function create()
    {
        return new Card\NAX3_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX3_03';
    }
}