<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class UniteTheMurlocs
{
    public static function globalId() : string
    {
        return 'UNG_942';
    }

    public static function create()
    {
        return new Card\UNG_942;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_942';
    }
}