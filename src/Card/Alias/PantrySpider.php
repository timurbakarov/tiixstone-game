<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PantrySpider
{
    public static function globalId() : string
    {
        return 'KAR_030a';
    }

    public static function create()
    {
        return new Card\KAR_030a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_030a';
    }
}