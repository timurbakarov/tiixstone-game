<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DeadlyFork
{
    public static function globalId() : string
    {
        return 'KAR_094';
    }

    public static function create()
    {
        return new Card\KAR_094;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_094';
    }
}