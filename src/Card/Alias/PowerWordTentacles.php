<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PowerWordTentacles
{
    public static function globalId() : string
    {
        return 'OG_094';
    }

    public static function create()
    {
        return new Card\OG_094;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_094';
    }
}