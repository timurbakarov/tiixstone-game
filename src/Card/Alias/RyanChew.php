<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RyanChew
{
    public static function globalId() : string
    {
        return 'CRED_39';
    }

    public static function create()
    {
        return new Card\CRED_39;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_39';
    }
}