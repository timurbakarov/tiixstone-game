<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NightscaleMatriarch
{
    public static function globalId() : string
    {
        return 'GIL_190';
    }

    public static function create()
    {
        return new Card\GIL_190;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_190';
    }
}