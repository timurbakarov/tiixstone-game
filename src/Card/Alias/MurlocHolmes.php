<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MurlocHolmes
{
    public static function globalId() : string
    {
        return 'GILA_827';
    }

    public static function create()
    {
        return new Card\GILA_827;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_827';
    }
}