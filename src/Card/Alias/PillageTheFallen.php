<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PillageTheFallen
{
    public static function globalId() : string
    {
        return 'GILA_607';
    }

    public static function create()
    {
        return new Card\GILA_607;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_607';
    }
}