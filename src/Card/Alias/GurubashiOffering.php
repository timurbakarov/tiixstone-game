<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GurubashiOffering
{
    public static function globalId() : string
    {
        return 'TRL_516';
    }

    public static function create()
    {
        return new Card\TRL_516;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_516';
    }
}