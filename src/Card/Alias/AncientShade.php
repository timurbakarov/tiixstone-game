<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AncientShade
{
    public static function globalId() : string
    {
        return 'LOE_110';
    }

    public static function create()
    {
        return new Card\LOE_110;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_110';
    }
}