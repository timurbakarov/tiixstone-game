<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArgentHorserider
{
    public static function globalId() : string
    {
        return 'AT_087';
    }

    public static function create()
    {
        return new Card\AT_087;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_087';
    }
}