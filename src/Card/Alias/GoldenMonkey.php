<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GoldenMonkey
{
    public static function globalId() : string
    {
        return 'LOE_019t2';
    }

    public static function create()
    {
        return new Card\LOE_019t2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_019t2';
    }
}