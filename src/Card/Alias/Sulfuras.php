<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Sulfuras
{
    public static function globalId() : string
    {
        return 'BRMC_94';
    }

    public static function create()
    {
        return new Card\BRMC_94;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_94';
    }
}