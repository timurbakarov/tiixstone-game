<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CabalistsTome
{
    public static function globalId() : string
    {
        return 'OG_090';
    }

    public static function create()
    {
        return new Card\OG_090;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_090';
    }
}