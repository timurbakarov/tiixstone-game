<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SilverHandMurloc
{
    public static function globalId() : string
    {
        return 'OG_006a';
    }

    public static function create()
    {
        return new Card\OG_006a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_006a';
    }
}