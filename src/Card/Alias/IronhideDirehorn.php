<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IronhideDirehorn
{
    public static function globalId() : string
    {
        return 'TRL_232';
    }

    public static function create()
    {
        return new Card\TRL_232;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_232';
    }
}