<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PrinceArthas
{
    public static function globalId() : string
    {
        return 'HERO_04b';
    }

    public static function create()
    {
        return new Card\HERO_04b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HERO_04b';
    }
}