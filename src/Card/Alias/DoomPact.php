<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DoomPact
{
    public static function globalId() : string
    {
        return 'ICC_314t3';
    }

    public static function create()
    {
        return new Card\ICC_314t3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_314t3';
    }
}