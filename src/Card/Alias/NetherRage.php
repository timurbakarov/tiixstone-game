<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NetherRage
{
    public static function globalId() : string
    {
        return 'KARA_08_02';
    }

    public static function create()
    {
        return new Card\KARA_08_02;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KARA_08_02';
    }
}