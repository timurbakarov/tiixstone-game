<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Crush
{
    public static function globalId() : string
    {
        return 'GVG_052';
    }

    public static function create()
    {
        return new Card\GVG_052;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_052';
    }
}