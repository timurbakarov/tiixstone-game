<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Warpath
{
    public static function globalId() : string
    {
        return 'GIL_654';
    }

    public static function create()
    {
        return new Card\GIL_654;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_654';
    }
}