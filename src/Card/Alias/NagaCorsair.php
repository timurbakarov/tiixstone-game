<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class NagaCorsair
{
    public static function globalId() : string
    {
        return 'CFM_651';
    }

    public static function create()
    {
        return new Card\CFM_651;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_651';
    }
}