<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MetaltoothLeaper
{
    public static function globalId() : string
    {
        return 'GVG_048';
    }

    public static function create()
    {
        return new Card\GVG_048;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_048';
    }
}