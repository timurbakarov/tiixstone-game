<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LivBreeden
{
    public static function globalId() : string
    {
        return 'CRED_64';
    }

    public static function create()
    {
        return new Card\CRED_64;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_64';
    }
}