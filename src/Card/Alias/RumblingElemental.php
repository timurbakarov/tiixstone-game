<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RumblingElemental
{
    public static function globalId() : string
    {
        return 'LOE_016';
    }

    public static function create()
    {
        return new Card\LOE_016;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_016';
    }
}