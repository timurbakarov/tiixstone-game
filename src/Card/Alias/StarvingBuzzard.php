<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StarvingBuzzard
{
    public static function globalId() : string
    {
        return 'CS2_237';
    }

    public static function create()
    {
        return new Card\CS2_237;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CS2_237';
    }
}