<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Guzzler
{
    public static function globalId() : string
    {
        return 'BRMA01_4t';
    }

    public static function create()
    {
        return new Card\BRMA01_4t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA01_4t';
    }
}