<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DragonmawScorcher
{
    public static function globalId() : string
    {
        return 'TRL_526';
    }

    public static function create()
    {
        return new Card\TRL_526;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_526';
    }
}