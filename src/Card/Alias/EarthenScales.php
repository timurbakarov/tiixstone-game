<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EarthenScales
{
    public static function globalId() : string
    {
        return 'UNG_108';
    }

    public static function create()
    {
        return new Card\UNG_108;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_108';
    }
}