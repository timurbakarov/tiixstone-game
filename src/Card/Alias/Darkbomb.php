<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Darkbomb
{
    public static function globalId() : string
    {
        return 'GVG_015';
    }

    public static function create()
    {
        return new Card\GVG_015;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_015';
    }
}