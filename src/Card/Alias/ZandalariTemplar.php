<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ZandalariTemplar
{
    public static function globalId() : string
    {
        return 'TRL_545';
    }

    public static function create()
    {
        return new Card\TRL_545;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_545';
    }
}