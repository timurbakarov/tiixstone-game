<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Atramedes
{
    public static function globalId() : string
    {
        return 'BRMC_86';
    }

    public static function create()
    {
        return new Card\BRMC_86;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMC_86';
    }
}