<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DragonscaleWarrior
{
    public static function globalId() : string
    {
        return 'TB_Coopv3_100';
    }

    public static function create()
    {
        return new Card\TB_Coopv3_100;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_Coopv3_100';
    }
}