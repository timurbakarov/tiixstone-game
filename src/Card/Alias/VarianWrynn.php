<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VarianWrynn
{
    public static function globalId() : string
    {
        return 'AT_072';
    }

    public static function create()
    {
        return new Card\AT_072;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_072';
    }
}