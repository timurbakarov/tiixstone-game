<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WitchsApprentice
{
    public static function globalId() : string
    {
        return 'GIL_531';
    }

    public static function create()
    {
        return new Card\GIL_531;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_531';
    }
}