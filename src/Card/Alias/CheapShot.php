<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CheapShot
{
    public static function globalId() : string
    {
        return 'GIL_506';
    }

    public static function create()
    {
        return new Card\GIL_506;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_506';
    }
}