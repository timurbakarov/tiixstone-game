<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShadowEssence
{
    public static function globalId() : string
    {
        return 'ICC_235';
    }

    public static function create()
    {
        return new Card\ICC_235;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_235';
    }
}