<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DireFateCard
{
    public static function globalId() : string
    {
        return 'TB_PickYourFate_4';
    }

    public static function create()
    {
        return new Card\TB_PickYourFate_4;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_PickYourFate_4';
    }
}