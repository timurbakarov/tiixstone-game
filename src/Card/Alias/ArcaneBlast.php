<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArcaneBlast
{
    public static function globalId() : string
    {
        return 'AT_004';
    }

    public static function create()
    {
        return new Card\AT_004;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_004';
    }
}