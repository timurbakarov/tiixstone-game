<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GetawayKodo
{
    public static function globalId() : string
    {
        return 'CFM_800';
    }

    public static function create()
    {
        return new Card\CFM_800;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_800';
    }
}