<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Pirate
{
    public static function globalId() : string
    {
        return 'TB_015';
    }

    public static function create()
    {
        return new Card\TB_015;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_015';
    }
}