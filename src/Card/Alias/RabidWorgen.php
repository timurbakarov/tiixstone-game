<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RabidWorgen
{
    public static function globalId() : string
    {
        return 'GIL_113';
    }

    public static function create()
    {
        return new Card\GIL_113;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_113';
    }
}