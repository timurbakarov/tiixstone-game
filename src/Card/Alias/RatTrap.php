<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RatTrap
{
    public static function globalId() : string
    {
        return 'GIL_577';
    }

    public static function create()
    {
        return new Card\GIL_577;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_577';
    }
}