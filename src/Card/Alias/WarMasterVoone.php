<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WarMasterVoone
{
    public static function globalId() : string
    {
        return 'TRL_328';
    }

    public static function create()
    {
        return new Card\TRL_328;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_328';
    }
}