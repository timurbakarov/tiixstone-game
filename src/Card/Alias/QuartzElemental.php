<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class QuartzElemental
{
    public static function globalId() : string
    {
        return 'GIL_156';
    }

    public static function create()
    {
        return new Card\GIL_156;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_156';
    }
}