<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CarnivorousCube
{
    public static function globalId() : string
    {
        return 'LOOT_161';
    }

    public static function create()
    {
        return new Card\LOOT_161;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOOT_161';
    }
}