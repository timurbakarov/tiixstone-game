<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PearlOfTheTides
{
    public static function globalId() : string
    {
        return 'LOEA12_2';
    }

    public static function create()
    {
        return new Card\LOEA12_2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA12_2';
    }
}