<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MedivhTheGuardian
{
    public static function globalId() : string
    {
        return 'KAR_097';
    }

    public static function create()
    {
        return new Card\KAR_097;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_097';
    }
}