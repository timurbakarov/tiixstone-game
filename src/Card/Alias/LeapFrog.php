<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LeapFrog
{
    public static function globalId() : string
    {
        return 'TRLA_157';
    }

    public static function create()
    {
        return new Card\TRLA_157;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_157';
    }
}