<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LordVictorNefarius
{
    public static function globalId() : string
    {
        return 'BRMA13_1';
    }

    public static function create()
    {
        return new Card\BRMA13_1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA13_1';
    }
}