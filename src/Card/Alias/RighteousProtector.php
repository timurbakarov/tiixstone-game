<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RighteousProtector
{
    public static function globalId() : string
    {
        return 'ICC_038';
    }

    public static function create()
    {
        return new Card\ICC_038;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_038';
    }
}