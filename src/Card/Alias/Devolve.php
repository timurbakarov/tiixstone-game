<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Devolve
{
    public static function globalId() : string
    {
        return 'CFM_696';
    }

    public static function create()
    {
        return new Card\CFM_696;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_696';
    }
}