<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PiranhaLauncher
{
    public static function globalId() : string
    {
        return 'CFM_337';
    }

    public static function create()
    {
        return new Card\CFM_337;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_337';
    }
}