<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LockAndLoad
{
    public static function globalId() : string
    {
        return 'AT_061';
    }

    public static function create()
    {
        return new Card\AT_061;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_061';
    }
}