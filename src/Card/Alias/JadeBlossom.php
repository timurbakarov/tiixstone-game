<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JadeBlossom
{
    public static function globalId() : string
    {
        return 'CFM_713';
    }

    public static function create()
    {
        return new Card\CFM_713;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_713';
    }
}