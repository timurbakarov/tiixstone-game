<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RyanMasterson
{
    public static function globalId() : string
    {
        return 'CRED_40';
    }

    public static function create()
    {
        return new Card\CRED_40;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_40';
    }
}