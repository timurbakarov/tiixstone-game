<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CursedBlade
{
    public static function globalId() : string
    {
        return 'LOE_118';
    }

    public static function create()
    {
        return new Card\LOE_118;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_118';
    }
}