<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WispsOfTheOldGods
{
    public static function globalId() : string
    {
        return 'OG_195';
    }

    public static function create()
    {
        return new Card\OG_195;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_195';
    }
}