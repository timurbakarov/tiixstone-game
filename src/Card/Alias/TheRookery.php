<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheRookery
{
    public static function globalId() : string
    {
        return 'TB_BRMA10_3H';
    }

    public static function create()
    {
        return new Card\TB_BRMA10_3H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_BRMA10_3H';
    }
}