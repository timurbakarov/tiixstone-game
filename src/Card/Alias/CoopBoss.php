<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CoopBoss
{
    public static function globalId() : string
    {
        return 'TB_CoopHero_H_001';
    }

    public static function create()
    {
        return new Card\TB_CoopHero_H_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_CoopHero_H_001';
    }
}