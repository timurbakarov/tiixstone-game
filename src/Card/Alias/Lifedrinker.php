<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Lifedrinker
{
    public static function globalId() : string
    {
        return 'GIL_622';
    }

    public static function create()
    {
        return new Card\GIL_622;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_622';
    }
}