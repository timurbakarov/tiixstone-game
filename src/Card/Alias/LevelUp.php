<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LevelUp
{
    public static function globalId() : string
    {
        return 'TB_LevelUp_001';
    }

    public static function create()
    {
        return new Card\TB_LevelUp_001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_LevelUp_001';
    }
}