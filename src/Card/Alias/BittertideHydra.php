<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BittertideHydra
{
    public static function globalId() : string
    {
        return 'UNG_087';
    }

    public static function create()
    {
        return new Card\UNG_087;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_087';
    }
}