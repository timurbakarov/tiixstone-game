<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VolcanicLumberer
{
    public static function globalId() : string
    {
        return 'BRM_009';
    }

    public static function create()
    {
        return new Card\BRM_009;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_009';
    }
}