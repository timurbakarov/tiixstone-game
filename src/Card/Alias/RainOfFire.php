<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RainOfFire
{
    public static function globalId() : string
    {
        return 'NAX2_03';
    }

    public static function create()
    {
        return new Card\NAX2_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX2_03';
    }
}