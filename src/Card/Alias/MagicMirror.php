<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MagicMirror
{
    public static function globalId() : string
    {
        return 'KAR_A01_01H';
    }

    public static function create()
    {
        return new Card\KAR_A01_01H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_A01_01H';
    }
}