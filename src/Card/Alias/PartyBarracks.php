<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PartyBarracks
{
    public static function globalId() : string
    {
        return 'TB_SPT_MTH_BossHeroPower';
    }

    public static function create()
    {
        return new Card\TB_SPT_MTH_BossHeroPower;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_SPT_MTH_BossHeroPower';
    }
}