<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ArcaneGiant
{
    public static function globalId() : string
    {
        return 'KAR_711';
    }

    public static function create()
    {
        return new Card\KAR_711;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_711';
    }
}