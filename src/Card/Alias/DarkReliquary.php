<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DarkReliquary
{
    public static function globalId() : string
    {
        return 'TRLA_178';
    }

    public static function create()
    {
        return new Card\TRLA_178;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_178';
    }
}