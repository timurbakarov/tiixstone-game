<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Brewmaster
{
    public static function globalId() : string
    {
        return 'TU4f_005';
    }

    public static function create()
    {
        return new Card\TU4f_005;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TU4f_005';
    }
}