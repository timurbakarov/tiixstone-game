<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HotSpringGuardian
{
    public static function globalId() : string
    {
        return 'UNG_938';
    }

    public static function create()
    {
        return new Card\UNG_938;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_938';
    }
}