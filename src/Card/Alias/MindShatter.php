<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MindShatter
{
    public static function globalId() : string
    {
        return 'EX1_625t2';
    }

    public static function create()
    {
        return new Card\EX1_625t2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_625t2';
    }
}