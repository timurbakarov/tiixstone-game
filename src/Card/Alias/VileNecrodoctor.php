<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VileNecrodoctor
{
    public static function globalId() : string
    {
        return 'TRLA_182';
    }

    public static function create()
    {
        return new Card\TRLA_182;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_182';
    }
}