<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ChromaticMutation
{
    public static function globalId() : string
    {
        return 'BRMA12_8';
    }

    public static function create()
    {
        return new Card\BRMA12_8;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA12_8';
    }
}