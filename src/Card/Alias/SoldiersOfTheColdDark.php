<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SoldiersOfTheColdDark
{
    public static function globalId() : string
    {
        return 'FB_LK001';
    }

    public static function create()
    {
        return new Card\FB_LK001;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK001';
    }
}