<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LightOfTheNaaru
{
    public static function globalId() : string
    {
        return 'GVG_012';
    }

    public static function create()
    {
        return new Card\GVG_012;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_012';
    }
}