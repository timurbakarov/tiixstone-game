<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LotusAssassin
{
    public static function globalId() : string
    {
        return 'CFM_634';
    }

    public static function create()
    {
        return new Card\CFM_634;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_634';
    }
}