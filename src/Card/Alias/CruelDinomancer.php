<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CruelDinomancer
{
    public static function globalId() : string
    {
        return 'UNG_830';
    }

    public static function create()
    {
        return new Card\UNG_830;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_830';
    }
}