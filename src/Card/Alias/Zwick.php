<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Zwick
{
    public static function globalId() : string
    {
        return 'CRED_07';
    }

    public static function create()
    {
        return new Card\CRED_07;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_07';
    }
}