<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Griftah
{
    public static function globalId() : string
    {
        return 'TRL_096';
    }

    public static function create()
    {
        return new Card\TRL_096;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_096';
    }
}