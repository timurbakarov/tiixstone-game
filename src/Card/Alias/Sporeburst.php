<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Sporeburst
{
    public static function globalId() : string
    {
        return 'NAX6_04';
    }

    public static function create()
    {
        return new Card\NAX6_04;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX6_04';
    }
}