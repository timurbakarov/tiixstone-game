<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FacelessManipulator
{
    public static function globalId() : string
    {
        return 'EX1_564';
    }

    public static function create()
    {
        return new Card\EX1_564;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_564';
    }
}