<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StitchedTracker
{
    public static function globalId() : string
    {
        return 'ICC_415';
    }

    public static function create()
    {
        return new Card\ICC_415;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_415';
    }
}