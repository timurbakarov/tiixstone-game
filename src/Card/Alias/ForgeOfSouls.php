<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ForgeOfSouls
{
    public static function globalId() : string
    {
        return 'ICC_281';
    }

    public static function create()
    {
        return new Card\ICC_281;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_281';
    }
}