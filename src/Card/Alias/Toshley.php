<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Toshley
{
    public static function globalId() : string
    {
        return 'GVG_115';
    }

    public static function create()
    {
        return new Card\GVG_115;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_115';
    }
}