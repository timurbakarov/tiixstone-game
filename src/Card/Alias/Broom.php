<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Broom
{
    public static function globalId() : string
    {
        return 'KAR_025b';
    }

    public static function create()
    {
        return new Card\KAR_025b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_025b';
    }
}