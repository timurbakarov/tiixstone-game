<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CallOfTheWild
{
    public static function globalId() : string
    {
        return 'OG_211';
    }

    public static function create()
    {
        return new Card\OG_211;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_211';
    }
}