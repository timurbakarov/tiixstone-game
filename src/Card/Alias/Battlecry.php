<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Battlecry
{
    public static function globalId() : string
    {
        return 'TRLA_Rogue_03';
    }

    public static function create()
    {
        return new Card\TRLA_Rogue_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Rogue_03';
    }
}