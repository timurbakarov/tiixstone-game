<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PurgeTheWeak
{
    public static function globalId() : string
    {
        return 'ICCA08_025';
    }

    public static function create()
    {
        return new Card\ICCA08_025;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA08_025';
    }
}