<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PowerWordGlory
{
    public static function globalId() : string
    {
        return 'AT_013';
    }

    public static function create()
    {
        return new Card\AT_013;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_013';
    }
}