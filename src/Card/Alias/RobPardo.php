<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RobPardo
{
    public static function globalId() : string
    {
        return 'CRED_17';
    }

    public static function create()
    {
        return new Card\CRED_17;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_17';
    }
}