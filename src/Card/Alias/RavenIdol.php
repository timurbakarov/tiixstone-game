<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RavenIdol
{
    public static function globalId() : string
    {
        return 'LOE_115';
    }

    public static function create()
    {
        return new Card\LOE_115;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_115';
    }
}