<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Stablemaster
{
    public static function globalId() : string
    {
        return 'AT_057';
    }

    public static function create()
    {
        return new Card\AT_057;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_057';
    }
}