<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ThistleTea
{
    public static function globalId() : string
    {
        return 'OG_073';
    }

    public static function create()
    {
        return new Card\OG_073;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_073';
    }
}