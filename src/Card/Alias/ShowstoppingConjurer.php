<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ShowstoppingConjurer
{
    public static function globalId() : string
    {
        return 'TRLA_134';
    }

    public static function create()
    {
        return new Card\TRLA_134;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_134';
    }
}