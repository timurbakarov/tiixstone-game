<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Mutations
{
    public static function globalId() : string
    {
        return 'TRLA_Shaman_04';
    }

    public static function create()
    {
        return new Card\TRLA_Shaman_04;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Shaman_04';
    }
}