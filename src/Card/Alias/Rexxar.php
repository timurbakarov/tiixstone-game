<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Rexxar
{
    public static function globalId() : string
    {
        return 'HERO_05';
    }

    public static function create()
    {
        return new Card\HERO_05;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HERO_05';
    }
}