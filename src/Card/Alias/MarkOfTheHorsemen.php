<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MarkOfTheHorsemen
{
    public static function globalId() : string
    {
        return 'NAX9_07';
    }

    public static function create()
    {
        return new Card\NAX9_07;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX9_07';
    }
}