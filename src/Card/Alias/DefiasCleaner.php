<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DefiasCleaner
{
    public static function globalId() : string
    {
        return 'CFM_855';
    }

    public static function create()
    {
        return new Card\CFM_855;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_855';
    }
}