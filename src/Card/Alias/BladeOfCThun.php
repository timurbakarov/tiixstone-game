<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BladeOfCThun
{
    public static function globalId() : string
    {
        return 'OG_282';
    }

    public static function create()
    {
        return new Card\OG_282;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_282';
    }
}