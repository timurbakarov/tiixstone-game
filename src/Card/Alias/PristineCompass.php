<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PristineCompass
{
    public static function globalId() : string
    {
        return 'GILA_853b';
    }

    public static function create()
    {
        return new Card\GILA_853b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_853b';
    }
}