<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EmeraldHiveQueen
{
    public static function globalId() : string
    {
        return 'UNG_085';
    }

    public static function create()
    {
        return new Card\UNG_085;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_085';
    }
}