<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Razorpetal
{
    public static function globalId() : string
    {
        return 'UNG_057t1';
    }

    public static function create()
    {
        return new Card\UNG_057t1;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_057t1';
    }
}