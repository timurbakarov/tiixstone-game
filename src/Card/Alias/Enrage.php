<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Enrage
{
    public static function globalId() : string
    {
        return 'NAX12_04';
    }

    public static function create()
    {
        return new Card\NAX12_04;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX12_04';
    }
}