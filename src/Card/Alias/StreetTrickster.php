<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StreetTrickster
{
    public static function globalId() : string
    {
        return 'CFM_039';
    }

    public static function create()
    {
        return new Card\CFM_039;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_039';
    }
}