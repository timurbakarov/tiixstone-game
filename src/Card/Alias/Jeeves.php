<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Jeeves
{
    public static function globalId() : string
    {
        return 'GVG_094';
    }

    public static function create()
    {
        return new Card\GVG_094;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_094';
    }
}