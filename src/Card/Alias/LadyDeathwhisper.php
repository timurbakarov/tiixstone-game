<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LadyDeathwhisper
{
    public static function globalId() : string
    {
        return 'ICCA10_009';
    }

    public static function create()
    {
        return new Card\ICCA10_009;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA10_009';
    }
}