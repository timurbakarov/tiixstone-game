<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TheCurator
{
    public static function globalId() : string
    {
        return 'KAR_061';
    }

    public static function create()
    {
        return new Card\KAR_061;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_061';
    }
}