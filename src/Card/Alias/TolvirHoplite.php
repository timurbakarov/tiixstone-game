<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TolvirHoplite
{
    public static function globalId() : string
    {
        return 'LOEA01_12h';
    }

    public static function create()
    {
        return new Card\LOEA01_12h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA01_12h';
    }
}