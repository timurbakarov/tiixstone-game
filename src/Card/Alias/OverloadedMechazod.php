<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class OverloadedMechazod
{
    public static function globalId() : string
    {
        return 'TB_CoOp_Mechazod2';
    }

    public static function create()
    {
        return new Card\TB_CoOp_Mechazod2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_CoOp_Mechazod2';
    }
}