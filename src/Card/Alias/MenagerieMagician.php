<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MenagerieMagician
{
    public static function globalId() : string
    {
        return 'KAR_702';
    }

    public static function create()
    {
        return new Card\KAR_702;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_702';
    }
}