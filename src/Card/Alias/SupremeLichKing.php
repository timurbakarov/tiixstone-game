<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SupremeLichKing
{
    public static function globalId() : string
    {
        return 'FB_LK_Raid_Hero';
    }

    public static function create()
    {
        return new Card\FB_LK_Raid_Hero;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK_Raid_Hero';
    }
}