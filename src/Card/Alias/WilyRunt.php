<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WilyRunt
{
    public static function globalId() : string
    {
        return 'LOE_089t2';
    }

    public static function create()
    {
        return new Card\LOE_089t2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_089t2';
    }
}