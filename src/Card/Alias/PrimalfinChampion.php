<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PrimalfinChampion
{
    public static function globalId() : string
    {
        return 'UNG_953';
    }

    public static function create()
    {
        return new Card\UNG_953;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_953';
    }
}