<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class StandAgainstDarkness
{
    public static function globalId() : string
    {
        return 'OG_273';
    }

    public static function create()
    {
        return new Card\OG_273;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_273';
    }
}