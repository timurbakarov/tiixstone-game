<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MuklasChampion
{
    public static function globalId() : string
    {
        return 'AT_090';
    }

    public static function create()
    {
        return new Card\AT_090;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_090';
    }
}