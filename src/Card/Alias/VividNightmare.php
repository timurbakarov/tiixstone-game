<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VividNightmare
{
    public static function globalId() : string
    {
        return 'GIL_813';
    }

    public static function create()
    {
        return new Card\GIL_813;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_813';
    }
}