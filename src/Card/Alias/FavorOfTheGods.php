<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FavorOfTheGods
{
    public static function globalId() : string
    {
        return 'TRLA_802';
    }

    public static function create()
    {
        return new Card\TRLA_802;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_802';
    }
}