<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SteadyShot
{
    public static function globalId() : string
    {
        return 'DS1h_292';
    }

    public static function create()
    {
        return new Card\DS1h_292;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\DS1h_292';
    }
}