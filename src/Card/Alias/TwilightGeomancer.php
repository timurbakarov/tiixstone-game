<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TwilightGeomancer
{
    public static function globalId() : string
    {
        return 'OG_284';
    }

    public static function create()
    {
        return new Card\OG_284;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_284';
    }
}