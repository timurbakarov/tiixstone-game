<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MoltenReflection
{
    public static function globalId() : string
    {
        return 'UNG_948';
    }

    public static function create()
    {
        return new Card\UNG_948;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_948';
    }
}