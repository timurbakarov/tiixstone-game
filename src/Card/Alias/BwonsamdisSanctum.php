<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BwonsamdisSanctum
{
    public static function globalId() : string
    {
        return 'TRLA_114t';
    }

    public static function create()
    {
        return new Card\TRLA_114t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_114t';
    }
}