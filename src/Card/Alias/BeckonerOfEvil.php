<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BeckonerOfEvil
{
    public static function globalId() : string
    {
        return 'OG_281';
    }

    public static function create()
    {
        return new Card\OG_281;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_281';
    }
}