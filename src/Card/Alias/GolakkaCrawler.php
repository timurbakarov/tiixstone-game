<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GolakkaCrawler
{
    public static function globalId() : string
    {
        return 'UNG_807';
    }

    public static function create()
    {
        return new Card\UNG_807;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_807';
    }
}