<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AvatarOfTheCoin
{
    public static function globalId() : string
    {
        return 'GAME_002';
    }

    public static function create()
    {
        return new Card\GAME_002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GAME_002';
    }
}