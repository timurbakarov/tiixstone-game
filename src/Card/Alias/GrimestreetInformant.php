<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GrimestreetInformant
{
    public static function globalId() : string
    {
        return 'CFM_321';
    }

    public static function create()
    {
        return new Card\CFM_321;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_321';
    }
}