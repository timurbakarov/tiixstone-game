<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SatedThreshadon
{
    public static function globalId() : string
    {
        return 'UNG_010';
    }

    public static function create()
    {
        return new Card\UNG_010;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_010';
    }
}