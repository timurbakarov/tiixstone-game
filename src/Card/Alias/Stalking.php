<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Stalking
{
    public static function globalId() : string
    {
        return 'GILA_489';
    }

    public static function create()
    {
        return new Card\GILA_489;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_489';
    }
}