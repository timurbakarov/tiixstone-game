<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Bloodbloom
{
    public static function globalId() : string
    {
        return 'UNG_832';
    }

    public static function create()
    {
        return new Card\UNG_832;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_832';
    }
}