<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AmethystSpellstone
{
    public static function globalId() : string
    {
        return 'LOOT_043t2';
    }

    public static function create()
    {
        return new Card\LOOT_043t2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOOT_043t2';
    }
}