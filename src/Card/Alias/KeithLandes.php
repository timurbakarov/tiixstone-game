<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class KeithLandes
{
    public static function globalId() : string
    {
        return 'CRED_46';
    }

    public static function create()
    {
        return new Card\CRED_46;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_46';
    }
}