<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BolfRamshield
{
    public static function globalId() : string
    {
        return 'AT_124';
    }

    public static function create()
    {
        return new Card\AT_124;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_124';
    }
}