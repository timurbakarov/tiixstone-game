<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Burgle
{
    public static function globalId() : string
    {
        return 'TRLA_Rogue_01';
    }

    public static function create()
    {
        return new Card\TRLA_Rogue_01;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Rogue_01';
    }
}