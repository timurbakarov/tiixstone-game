<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JasonShattuck
{
    public static function globalId() : string
    {
        return 'CRED_58';
    }

    public static function create()
    {
        return new Card\CRED_58;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_58';
    }
}