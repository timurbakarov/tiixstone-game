<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FurnacefireColossus
{
    public static function globalId() : string
    {
        return 'ICC_096';
    }

    public static function create()
    {
        return new Card\ICC_096;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_096';
    }
}