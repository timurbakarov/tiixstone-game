<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BaneOfDoom
{
    public static function globalId() : string
    {
        return 'EX1_320';
    }

    public static function create()
    {
        return new Card\EX1_320;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\EX1_320';
    }
}