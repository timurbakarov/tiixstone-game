<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class EvanPolekoff
{
    public static function globalId() : string
    {
        return 'CRED_63';
    }

    public static function create()
    {
        return new Card\CRED_63;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_63';
    }
}