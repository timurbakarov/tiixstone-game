<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class UpgradedRepairBot
{
    public static function globalId() : string
    {
        return 'GVG_083';
    }

    public static function create()
    {
        return new Card\GVG_083;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_083';
    }
}