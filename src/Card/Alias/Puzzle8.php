<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Puzzle8
{
    public static function globalId() : string
    {
        return 'TB_Lethal010';
    }

    public static function create()
    {
        return new Card\TB_Lethal010;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_Lethal010';
    }
}