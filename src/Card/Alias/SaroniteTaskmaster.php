<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SaroniteTaskmaster
{
    public static function globalId() : string
    {
        return 'TRL_363';
    }

    public static function create()
    {
        return new Card\TRL_363;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_363';
    }
}