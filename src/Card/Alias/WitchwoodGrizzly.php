<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WitchwoodGrizzly
{
    public static function globalId() : string
    {
        return 'GIL_623';
    }

    public static function create()
    {
        return new Card\GIL_623;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_623';
    }
}