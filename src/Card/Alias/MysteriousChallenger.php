<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MysteriousChallenger
{
    public static function globalId() : string
    {
        return 'AT_079';
    }

    public static function create()
    {
        return new Card\AT_079;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_079';
    }
}