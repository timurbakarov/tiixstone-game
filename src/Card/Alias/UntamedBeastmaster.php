<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class UntamedBeastmaster
{
    public static function globalId() : string
    {
        return 'TRL_405';
    }

    public static function create()
    {
        return new Card\TRL_405;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_405';
    }
}