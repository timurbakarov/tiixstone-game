<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WalkThePlank
{
    public static function globalId() : string
    {
        return 'TRL_157';
    }

    public static function create()
    {
        return new Card\TRL_157;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_157';
    }
}