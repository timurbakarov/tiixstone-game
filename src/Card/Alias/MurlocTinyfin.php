<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MurlocTinyfin
{
    public static function globalId() : string
    {
        return 'LOEA10_3';
    }

    public static function create()
    {
        return new Card\LOEA10_3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA10_3';
    }
}