<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AnimateEarthen
{
    public static function globalId() : string
    {
        return 'LOEA06_03';
    }

    public static function create()
    {
        return new Card\LOEA06_03;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOEA06_03';
    }
}