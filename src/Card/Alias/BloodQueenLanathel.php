<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodQueenLanathel
{
    public static function globalId() : string
    {
        return 'FB_LK008h';
    }

    public static function create()
    {
        return new Card\FB_LK008h;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK008h';
    }
}