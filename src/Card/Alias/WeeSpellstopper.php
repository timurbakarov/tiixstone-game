<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WeeSpellstopper
{
    public static function globalId() : string
    {
        return 'GVG_122';
    }

    public static function create()
    {
        return new Card\GVG_122;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_122';
    }
}