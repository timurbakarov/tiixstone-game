<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BroodAffliction
{
    public static function globalId() : string
    {
        return 'BRMA12_2';
    }

    public static function create()
    {
        return new Card\BRMA12_2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA12_2';
    }
}