<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SwampKingDred
{
    public static function globalId() : string
    {
        return 'UNG_919';
    }

    public static function create()
    {
        return new Card\UNG_919;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_919';
    }
}