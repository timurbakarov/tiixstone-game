<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PilotedSkyGolem
{
    public static function globalId() : string
    {
        return 'GVG_105';
    }

    public static function create()
    {
        return new Card\GVG_105;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_105';
    }
}