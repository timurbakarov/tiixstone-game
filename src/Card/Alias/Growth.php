<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Growth
{
    public static function globalId() : string
    {
        return 'ICC_047a';
    }

    public static function create()
    {
        return new Card\ICC_047a;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_047a';
    }
}