<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DoubleZap
{
    public static function globalId() : string
    {
        return 'TB_CoOpBossSpell_5';
    }

    public static function create()
    {
        return new Card\TB_CoOpBossSpell_5;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_CoOpBossSpell_5';
    }
}