<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Wisp
{
    public static function globalId() : string
    {
        return 'GIL_553t';
    }

    public static function create()
    {
        return new Card\GIL_553t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_553t';
    }
}