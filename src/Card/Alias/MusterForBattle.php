<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MusterForBattle
{
    public static function globalId() : string
    {
        return 'GVG_061';
    }

    public static function create()
    {
        return new Card\GVG_061;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_061';
    }
}