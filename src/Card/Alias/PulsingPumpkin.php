<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PulsingPumpkin
{
    public static function globalId() : string
    {
        return 'TB_HeadlessHorseman_003';
    }

    public static function create()
    {
        return new Card\TB_HeadlessHorseman_003;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TB_HeadlessHorseman_003';
    }
}