<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class PantherForm
{
    public static function globalId() : string
    {
        return 'AT_042b';
    }

    public static function create()
    {
        return new Card\AT_042b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_042b';
    }
}