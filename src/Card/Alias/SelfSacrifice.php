<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SelfSacrifice
{
    public static function globalId() : string
    {
        return 'TRLA_Paladin_04';
    }

    public static function create()
    {
        return new Card\TRLA_Paladin_04;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_Paladin_04';
    }
}