<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ChristianScharling
{
    public static function globalId() : string
    {
        return 'CRED_48';
    }

    public static function create()
    {
        return new Card\CRED_48;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_48';
    }
}