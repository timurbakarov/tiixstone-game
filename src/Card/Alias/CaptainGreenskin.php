<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class CaptainGreenskin
{
    public static function globalId() : string
    {
        return 'NEW1_024';
    }

    public static function create()
    {
        return new Card\NEW1_024;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NEW1_024';
    }
}