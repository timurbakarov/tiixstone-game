<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ExoticBeasts
{
    public static function globalId() : string
    {
        return 'GILA_496';
    }

    public static function create()
    {
        return new Card\GILA_496;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_496';
    }
}