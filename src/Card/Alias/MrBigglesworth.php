<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class MrBigglesworth
{
    public static function globalId() : string
    {
        return 'NAX15_05';
    }

    public static function create()
    {
        return new Card\NAX15_05;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\NAX15_05';
    }
}