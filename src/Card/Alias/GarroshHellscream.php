<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GarroshHellscream
{
    public static function globalId() : string
    {
        return 'HERO_01';
    }

    public static function create()
    {
        return new Card\HERO_01;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\HERO_01';
    }
}