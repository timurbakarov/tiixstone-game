<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Fireslinger
{
    public static function globalId() : string
    {
        return 'TRLA_133';
    }

    public static function create()
    {
        return new Card\TRLA_133;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_133';
    }
}