<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Warrior
{
    public static function globalId() : string
    {
        return 'FB_LK_Warrior_copy';
    }

    public static function create()
    {
        return new Card\FB_LK_Warrior_copy;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK_Warrior_copy';
    }
}