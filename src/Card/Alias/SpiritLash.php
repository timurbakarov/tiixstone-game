<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SpiritLash
{
    public static function globalId() : string
    {
        return 'ICC_802';
    }

    public static function create()
    {
        return new Card\ICC_802;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_802';
    }
}