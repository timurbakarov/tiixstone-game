<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DalaranAspirant
{
    public static function globalId() : string
    {
        return 'AT_006';
    }

    public static function create()
    {
        return new Card\AT_006;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_006';
    }
}