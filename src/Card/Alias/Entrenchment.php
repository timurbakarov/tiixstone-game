<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Entrenchment
{
    public static function globalId() : string
    {
        return 'GILA_805';
    }

    public static function create()
    {
        return new Card\GILA_805;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_805';
    }
}