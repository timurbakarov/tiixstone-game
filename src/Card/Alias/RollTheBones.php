<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class RollTheBones
{
    public static function globalId() : string
    {
        return 'ICC_201';
    }

    public static function create()
    {
        return new Card\ICC_201;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_201';
    }
}