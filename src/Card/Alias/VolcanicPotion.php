<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class VolcanicPotion
{
    public static function globalId() : string
    {
        return 'CFM_065';
    }

    public static function create()
    {
        return new Card\CFM_065;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_065';
    }
}