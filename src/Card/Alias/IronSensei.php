<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class IronSensei
{
    public static function globalId() : string
    {
        return 'GVG_027';
    }

    public static function create()
    {
        return new Card\GVG_027;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_027';
    }
}