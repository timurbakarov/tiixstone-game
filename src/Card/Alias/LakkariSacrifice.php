<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LakkariSacrifice
{
    public static function globalId() : string
    {
        return 'UNG_829';
    }

    public static function create()
    {
        return new Card\UNG_829;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\UNG_829';
    }
}