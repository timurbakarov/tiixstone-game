<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BloodOfTheAncientOne
{
    public static function globalId() : string
    {
        return 'OG_173';
    }

    public static function create()
    {
        return new Card\OG_173;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_173';
    }
}