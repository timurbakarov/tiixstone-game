<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Drakeslayer
{
    public static function globalId() : string
    {
        return 'GIL_683t';
    }

    public static function create()
    {
        return new Card\GIL_683t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_683t';
    }
}