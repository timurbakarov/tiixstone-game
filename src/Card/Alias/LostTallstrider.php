<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class LostTallstrider
{
    public static function globalId() : string
    {
        return 'GVG_071';
    }

    public static function create()
    {
        return new Card\GVG_071;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_071';
    }
}