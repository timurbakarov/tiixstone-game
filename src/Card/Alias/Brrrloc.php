<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Brrrloc
{
    public static function globalId() : string
    {
        return 'ICC_058';
    }

    public static function create()
    {
        return new Card\ICC_058;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_058';
    }
}