<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Icehowl
{
    public static function globalId() : string
    {
        return 'AT_125';
    }

    public static function create()
    {
        return new Card\AT_125;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_125';
    }
}