<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GonksArmament
{
    public static function globalId() : string
    {
        return 'TRLA_112t';
    }

    public static function create()
    {
        return new Card\TRLA_112t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_112t';
    }
}