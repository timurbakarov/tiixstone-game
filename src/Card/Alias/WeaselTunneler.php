<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WeaselTunneler
{
    public static function globalId() : string
    {
        return 'CFM_095';
    }

    public static function create()
    {
        return new Card\CFM_095;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CFM_095';
    }
}