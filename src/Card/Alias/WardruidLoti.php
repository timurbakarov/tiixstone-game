<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WardruidLoti
{
    public static function globalId() : string
    {
        return 'TRL_343dt2';
    }

    public static function create()
    {
        return new Card\TRL_343dt2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_343dt2';
    }
}