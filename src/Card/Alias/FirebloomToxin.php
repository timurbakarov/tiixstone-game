<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class FirebloomToxin
{
    public static function globalId() : string
    {
        return 'OG_080f';
    }

    public static function create()
    {
        return new Card\OG_080f;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\OG_080f';
    }
}