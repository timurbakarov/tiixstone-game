<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JeweledScarab
{
    public static function globalId() : string
    {
        return 'LOE_029';
    }

    public static function create()
    {
        return new Card\LOE_029;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_029';
    }
}