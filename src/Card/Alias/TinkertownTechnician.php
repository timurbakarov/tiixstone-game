<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class TinkertownTechnician
{
    public static function globalId() : string
    {
        return 'GVG_102';
    }

    public static function create()
    {
        return new Card\GVG_102;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_102';
    }
}