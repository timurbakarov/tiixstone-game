<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BearTrap
{
    public static function globalId() : string
    {
        return 'AT_060';
    }

    public static function create()
    {
        return new Card\AT_060;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_060';
    }
}