<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class JasonDeFord
{
    public static function globalId() : string
    {
        return 'CRED_66';
    }

    public static function create()
    {
        return new Card\CRED_66;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\CRED_66';
    }
}