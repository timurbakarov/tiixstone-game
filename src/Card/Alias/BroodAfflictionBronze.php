<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BroodAfflictionBronze
{
    public static function globalId() : string
    {
        return 'BRMA12_7H';
    }

    public static function create()
    {
        return new Card\BRMA12_7H;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRMA12_7H';
    }
}