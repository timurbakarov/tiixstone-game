<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Implosion
{
    public static function globalId() : string
    {
        return 'GVG_045';
    }

    public static function create()
    {
        return new Card\GVG_045;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GVG_045';
    }
}