<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DruidOfTheFlame
{
    public static function globalId() : string
    {
        return 'BRM_010t2';
    }

    public static function create()
    {
        return new Card\BRM_010t2;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\BRM_010t2';
    }
}