<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class BoneSpike
{
    public static function globalId() : string
    {
        return 'ICCA06_005';
    }

    public static function create()
    {
        return new Card\ICCA06_005;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICCA06_005';
    }
}