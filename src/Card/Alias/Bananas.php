<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Bananas
{
    public static function globalId() : string
    {
        return 'TRL_509t';
    }

    public static function create()
    {
        return new Card\TRL_509t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRL_509t';
    }
}