<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class DoomRat
{
    public static function globalId() : string
    {
        return 'GIL_577t';
    }

    public static function create()
    {
        return new Card\GIL_577t;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_577t';
    }
}