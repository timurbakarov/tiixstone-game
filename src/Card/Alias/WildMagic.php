<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class WildMagic
{
    public static function globalId() : string
    {
        return 'GILA_Toki_02';
    }

    public static function create()
    {
        return new Card\GILA_Toki_02;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GILA_Toki_02';
    }
}