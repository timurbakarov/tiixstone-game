<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class AvianWatcher
{
    public static function globalId() : string
    {
        return 'KAR_037';
    }

    public static function create()
    {
        return new Card\KAR_037;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\KAR_037';
    }
}