<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class GrumblyRunt
{
    public static function globalId() : string
    {
        return 'LOE_089t3';
    }

    public static function create()
    {
        return new Card\LOE_089t3;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\LOE_089t3';
    }
}