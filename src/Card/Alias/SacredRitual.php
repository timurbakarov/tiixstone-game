<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SacredRitual
{
    public static function globalId() : string
    {
        return 'TRLA_801';
    }

    public static function create()
    {
        return new Card\TRLA_801;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\TRLA_801';
    }
}