<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class HeroicDifficulty
{
    public static function globalId() : string
    {
        return 'FB_LK_BossSetup001b';
    }

    public static function create()
    {
        return new Card\FB_LK_BossSetup001b;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\FB_LK_BossSetup001b';
    }
}