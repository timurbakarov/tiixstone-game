<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class ValkyrSoulclaimer
{
    public static function globalId() : string
    {
        return 'ICC_408';
    }

    public static function create()
    {
        return new Card\ICC_408;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\ICC_408';
    }
}