<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class Effigy
{
    public static function globalId() : string
    {
        return 'AT_002';
    }

    public static function create()
    {
        return new Card\AT_002;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\AT_002';
    }
}