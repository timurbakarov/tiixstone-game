<?php

namespace Tiixstone\Card\Alias;

use Tiixstone\Card;

class SilverSword
{
    public static function globalId() : string
    {
        return 'GIL_596';
    }

    public static function create()
    {
        return new Card\GIL_596;
    }
    
    public static function className()
    {
        return 'Tiixstone\Card\GIL_596';
    }
}