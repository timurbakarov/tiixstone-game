<?php

namespace Tiixstone\Card;

/**
 * Raging Contender
 */
class TRLA_193 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_193';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}