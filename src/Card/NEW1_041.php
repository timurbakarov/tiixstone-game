<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Stampeding Kodo
 */
class NEW1_041 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_041';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}