<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Wee Spellstopper
 */
class GVG_122 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_122';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}