<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Witching Hour
 */
class GIL_571 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_571';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}