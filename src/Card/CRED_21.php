<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Bryan Chang
 */
class CRED_21 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_21';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}