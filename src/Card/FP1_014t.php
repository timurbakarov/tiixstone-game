<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Thaddius
 */
class FP1_014t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_014t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 11;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 11;
    }
}