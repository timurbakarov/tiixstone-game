<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * One-eyed Cheat
 */
class GVG_025 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_025';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}