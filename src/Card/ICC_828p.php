<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Build-A-Beast
 */
class ICC_828p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'ICC_828p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}