<?php

namespace Tiixstone\Card;

/**
 * Wisp
 */
class GILA_BOSS_55t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_BOSS_55t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}