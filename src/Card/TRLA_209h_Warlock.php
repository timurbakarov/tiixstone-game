<?php

namespace Tiixstone\Card;

/**
 * Rikkar
 */
class TRLA_209h_Warlock extends Hero
{
    public function globalId() : string
    {
        return 'TRLA_209h_Warlock';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}