<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Pterrordax Hatchling
 */
class UNG_001 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_001';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}