<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Battlecryable;
use Tiixstone\Block\TakeDamage;
use Tiixstone\Effect\TauntEffect;

/**
 * Witchwood Grizzly
 */
class GIL_623 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_623';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 12;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [TauntEffect::class];
    }

    /**
     * @param Game $game
     * @param Attackable|Character $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        return [
            new TakeDamage($attackable, $attackable->getPlayer()->getOpponent($game)->hand->count(), $attackable),
        ];
    }
}