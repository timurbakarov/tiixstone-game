<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * The Horde
 */
class KARA_13_02H extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'KARA_13_02H';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}