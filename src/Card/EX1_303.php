<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Condition;
use Tiixstone\PlayCardCondition;
use Tiixstone\Manager\TargetManager;

/**
 * Shadowflame
 */
class EX1_303 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_303';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new Block\DealSplashDamage(TargetManager::ANY_OPPONENT_MINION, $target->attack->total(), $this),
            new Block\MarkDestroyed($target),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Friendly,
            new Condition\Target\Minion,
        ]);
    }
}