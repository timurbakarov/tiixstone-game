<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect;
use Tiixstone\Card\Minion;

/**
 * Stubborn Gastropod
 */
class UNG_808 extends Minion
{
	protected $race = Race::BEAST;
	
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_808';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
	public function boardEffects()
	{
		return [Effect\PoisonousEffect::class, Effect\TauntEffect::class];
	}
}