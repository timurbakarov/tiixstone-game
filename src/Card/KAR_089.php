<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Malchezaar's Imp
 */
class KAR_089 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_089';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}