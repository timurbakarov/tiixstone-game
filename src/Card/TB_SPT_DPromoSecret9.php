<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * Visions of Knowledge
 */
class TB_SPT_DPromoSecret9 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_SPT_DPromoSecret9';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}