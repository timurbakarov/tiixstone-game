<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Cenarius
 */
class EX1_573 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_573';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}