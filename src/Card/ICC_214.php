<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Obsidian Statue
 */
class ICC_214 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_214';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}