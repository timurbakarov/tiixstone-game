<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Captain Greenskin
 */
class NEW1_024 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_024';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}