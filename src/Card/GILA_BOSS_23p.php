<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Chuck
 */
class GILA_BOSS_23p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_23p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}