<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Siege Engine
 */
class GVG_086 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_086';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}