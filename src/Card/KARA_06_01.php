<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Romulo
 */
class KARA_06_01 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KARA_06_01';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}