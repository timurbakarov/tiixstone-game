<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Valithria Dreamwalker
 */
class ICCA10_001 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA10_001';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 30;
    }
}