<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Gnoll
 */
class OG_318t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_318t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}