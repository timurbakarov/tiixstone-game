<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect;

/**
 * Grimscale Oracle
 */
class EX1_508 extends Minion
{
    /**
     * @var int
     */
    protected $race = Race::MURLOC;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_508';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    public function boardEffects()
    {
        return [
            new Effect\AuraAttackChangerEffect(
                1,
                new Effect\Target\Aggregate([
                    new Effect\Target\Friendly,
                    new Effect\Target\Race(Race::murloc()),
                    new Effect\Target\NotSelfTarget,
                ])
            ),
        ];
    }
}