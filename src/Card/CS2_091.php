<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Light's Justice
 */
class CS2_091 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_091';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}