<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * King Mukla
 */
class TU4c_001 extends Hero
{
    public function globalId() : string
    {
        return 'TU4c_001';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}