<?php

namespace Tiixstone\Card;

/**
 * Chupacabran
 */
class GILA_BOSS_35h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_35h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}