<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tol'vir Stoneshaper
 */
class UNG_070 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_070';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}