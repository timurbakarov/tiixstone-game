<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Silverware Golem
 */
class KAR_A02_12H extends Hero
{
    public function globalId() : string
    {
        return 'KAR_A02_12H';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}