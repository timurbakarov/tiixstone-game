<?php

namespace Tiixstone\Card;

/**
 * Bottled Terror
 */
class TRLA_106t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_106t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}