<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Sabertooth Tiger
 */
class OG_044c extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_044c';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}