<?php

namespace Tiixstone\Card;

/**
 * Duskhaven Hunter
 */
class GIL_200 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_200';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}