<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Obsidian Destroyer
 */
class LOE_009 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_009';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}