<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Bananas
 */
class TRL_509t extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_509t';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}