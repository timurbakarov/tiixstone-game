<?php

namespace Tiixstone\Card;

/**
 * War Master Voone
 */
class TRLA_200h extends Hero
{
    public function globalId() : string
    {
        return 'TRLA_200h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}