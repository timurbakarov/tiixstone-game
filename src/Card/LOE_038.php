<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Naga Sea Witch
 */
class LOE_038 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_038';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}