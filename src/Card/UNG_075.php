<?php

namespace Tiixstone\Card;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Block\Adapt;
use Tiixstone\Effect\TriggerEffect;

/**
 * Vicious Fledgling
 */
class UNG_075 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_075';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [
            $this->afterAttackHero(),
        ];
    }

    /**
     * @return TriggerEffect
     */
    private function afterAttackHero()
    {
        return new class extends TriggerEffect {
            /**
             * @return string
             */
            public function triggerAtEvent(): string
            {
                return Event\AfterAttack::class;
            }

            /**
             * @param Game $game
             * @param Card $card
             * @param Event|Event\AfterAttack $event
             * @return bool
             */
            public function condition(Game $game, Card $card, Event $event): bool
            {
                return $card->isSelf($event->attacker) && $event->defender->isHero();
            }

            /**
             * @param Game $game
             * @param Card $card
             * @param Event $event
             * @return array
             */
            public function action(Game $game, Card $card, Event $event): array
            {
                return [
                    new Adapt($card),
                ];
            }
        };
    }
}