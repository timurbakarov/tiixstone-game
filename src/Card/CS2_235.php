<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\Trigger\NorthshireClericEffect;

/**
 * Northshire Cleric
 */
class CS2_235 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_235';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [NorthshireClericEffect::class];
    }
}