<?php

namespace Tiixstone\Card;

/**
 * Gattling Gunner
 */
class GILA_801 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_801';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}