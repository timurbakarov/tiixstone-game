<?php

namespace Tiixstone\Card;

/**
 * Cutthroat Buccaneer
 */
class GIL_902 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_902';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}