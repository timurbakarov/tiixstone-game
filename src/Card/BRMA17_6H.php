<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Bone Construct
 */
class BRMA17_6H extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA17_6H';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}