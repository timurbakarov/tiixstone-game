<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\TakeDamage;
use Tiixstone\PlayCardCondition;
use Tiixstone\Condition\ClosureCondition;

/**
 * Multi-Shot
 */
class DS1_183 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'DS1_183';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $availableTargets = $game->targetManager->allEnemyMinions($game, $this->getPlayer());

        return array_map(function(Minion $minion) use($game) {
            return new TakeDamage($minion, $game->withSpellDamage(3), $this);
        }, $game->RNG->randomFromArray($availableTargets, 2));
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new ClosureCondition(function(Game $game) {
                return $this->getPlayer()->getOpponent($game)->board->count() >= 2;
            }, 'There should be two minions on enemy board at least'),
        ]);
    }
}