<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mike Donais
 */
class CRED_36 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_36';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}