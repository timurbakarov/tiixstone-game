<?php

namespace Tiixstone\Card;

/**
 * Helpless Hatchling
 */
class TRL_505 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_505';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}