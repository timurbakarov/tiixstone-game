<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Light's Sorrow
 */
class ICC_071 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_071';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}