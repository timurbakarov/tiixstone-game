<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dust Devil
 */
class EX1_243 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_243';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}