<?php

namespace Tiixstone\Card;

/**
 * Farraki Battleaxe
 */
class TRL_304 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_304';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 5;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}