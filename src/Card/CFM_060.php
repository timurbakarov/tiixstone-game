<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Red Mana Wyrm
 */
class CFM_060 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_060';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}