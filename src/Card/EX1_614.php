<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Illidan Stormrage
 */
class EX1_614 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_614';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}