<?php

namespace Tiixstone\Card;

/**
 * Worgen Tracker
 */
class GILA_851c extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_851c';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}