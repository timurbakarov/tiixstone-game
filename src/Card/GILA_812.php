<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Bestiary
 */
class GILA_812 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_812';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 8;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}