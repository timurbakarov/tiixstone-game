<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Onyxiclaw
 */
class BRMA17_9 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA17_9';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 6;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}