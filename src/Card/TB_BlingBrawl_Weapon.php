<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Foam Sword
 */
class TB_BlingBrawl_Weapon extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_BlingBrawl_Weapon';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}