<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Lance Carrier
 */
class AT_084 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_084';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}