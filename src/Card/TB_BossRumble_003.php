<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Kazakus
 */
class TB_BossRumble_003 extends Hero
{
    public function globalId() : string
    {
        return 'TB_BossRumble_003';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}