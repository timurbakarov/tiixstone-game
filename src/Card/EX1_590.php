<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Blood Knight
 */
class EX1_590 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_590';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}