<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Brian Schwab
 */
class CRED_13 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_13';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 10;
    }
}