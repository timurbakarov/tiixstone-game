<?php

namespace Tiixstone\Card;

/**
 * Night Prowler
 */
class GIL_624 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_624';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}