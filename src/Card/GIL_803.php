<?php

namespace Tiixstone\Card;

/**
 * Militia Commander
 */
class GIL_803 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_803';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}