<?php

namespace Tiixstone\Card;

/**
 * Bogshaper
 */
class GIL_807 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_807';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}