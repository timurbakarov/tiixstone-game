<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Robin Fredericksen
 */
class CRED_38 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_38';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}