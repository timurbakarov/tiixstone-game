<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Holy Book
 */
class GILA_804 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_804';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 6;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}