<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\Trigger\WrathGuardEffect;

/**
 * Wrathguard
 */
class AT_026 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_026';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [WrathGuardEffect::class];
    }
}