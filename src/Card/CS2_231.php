<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Wisp
 */
class CS2_231 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_231';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}