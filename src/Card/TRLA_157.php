<?php

namespace Tiixstone\Card;

/**
 * Leap Frog
 */
class TRLA_157 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_157';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}