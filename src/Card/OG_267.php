<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Southsea Squidface
 */
class OG_267 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_267';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}