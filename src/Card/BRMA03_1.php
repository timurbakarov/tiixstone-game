<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Emperor Thaurissan
 */
class BRMA03_1 extends Hero
{
    public function globalId() : string
    {
        return 'BRMA03_1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}