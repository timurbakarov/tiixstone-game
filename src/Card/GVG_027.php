<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Iron Sensei
 */
class GVG_027 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_027';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}