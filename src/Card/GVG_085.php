<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Annoy-o-Tron
 */
class GVG_085 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_085';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}