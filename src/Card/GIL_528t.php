<?php

namespace Tiixstone\Card;

/**
 * Swift Messenger
 */
class GIL_528t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_528t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}