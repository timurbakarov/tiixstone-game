<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Doomguard
 */
class EX1_310 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_310';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}