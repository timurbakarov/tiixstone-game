<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Alias\ArmorUp;

/**
 * Garrosh Hellscream
 */
class HERO_01 extends Hero
{
    public function globalId() : string
    {
        return 'HERO_01';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return ArmorUp::create();
    }
}