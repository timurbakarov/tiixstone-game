<?php

namespace Tiixstone\Card;

/**
 * Grave Horror
 */
class TRL_408 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_408';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 12;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}