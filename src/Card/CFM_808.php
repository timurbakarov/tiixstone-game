<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Genzo, the Shark
 */
class CFM_808 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_808';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}