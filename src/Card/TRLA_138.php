<?php

namespace Tiixstone\Card;

/**
 * Shirvallah's Grace
 */
class TRLA_138 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_138';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}