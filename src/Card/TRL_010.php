<?php

namespace Tiixstone\Card;

/**
 * Half-Time Scavenger
 */
class TRL_010 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_010';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}