<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Vilespine Slayer
 */
class UNG_064 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_064';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}