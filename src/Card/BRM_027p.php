<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * DIE, INSECT!
 */
class BRM_027p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'BRM_027p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}