<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;
use Tiixstone\Effect\LightwellEffect;

/**
 * Lightwell
 */
class EX1_341 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_341';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [LightwellEffect::class];
    }
}