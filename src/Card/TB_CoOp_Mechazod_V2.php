<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Gearmaster Mechazod
 */
class TB_CoOp_Mechazod_V2 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_CoOp_Mechazod_V2';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 95;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}