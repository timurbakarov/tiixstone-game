<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Gearmaster Mechazod
 */
class TB_CoOp_Mechazod_OLD extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_CoOp_Mechazod_OLD';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 95;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}