<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Chieftain Scarvash
 */
class LOEA05_01h extends Hero
{
    public function globalId() : string
    {
        return 'LOEA05_01h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}