<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\Inspire\SpawnOfShadowsEffect;

/**
 * Spawn of Shadows
 */
class AT_012 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_012';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [SpawnOfShadowsEffect::class];
    }
}