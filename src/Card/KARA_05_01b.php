<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Kindly Grandmother
 */
class KARA_05_01b extends Hero
{
    public function globalId() : string
    {
        return 'KARA_05_01b';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}