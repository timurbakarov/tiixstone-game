<?php

namespace Tiixstone\Card;

/**
 * Smolderthorn Lancer
 */
class TRL_326 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_326';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}