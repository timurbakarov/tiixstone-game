<?php

namespace Tiixstone\Card;

/**
 * Hotshot
 */
class TRL_151t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_151t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}