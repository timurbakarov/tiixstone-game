<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * C'Thun's Chosen
 */
class OG_283 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_283';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}