<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Spirit Claws
 */
class KAR_063 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_063';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}