<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Anub'Rekhan
 */
class NAX1_01 extends Hero
{
    public function globalId() : string
    {
        return 'NAX1_01';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}