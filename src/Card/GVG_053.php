<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Shieldmaiden
 */
class GVG_053 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_053';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}