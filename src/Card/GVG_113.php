<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Foe Reaper 4000
 */
class GVG_113 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_113';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}