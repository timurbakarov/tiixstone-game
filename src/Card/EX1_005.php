<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Big Game Hunter
 */
class EX1_005 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_005';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}