<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\Combo\SabotageEffect;
use Tiixstone\Game;
use Tiixstone\Block\MarkDestroyed;

/**
 * Sabotage
 */
class GVG_047 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_047';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $targets = $game->targetManager->allEnemyMinions($game, $game->currentPlayer());

        if(!$targets) {
            return [];
        }

        $target = $game->RNG->randomFromArray($targets);

        return [new MarkDestroyed($target)];
    }

    /**
     * @return array
     */
    public function handEffects()
    {
        return [SabotageEffect::class];
    }
}