<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Glaivezooka
 */
class GVG_043 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_043';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}