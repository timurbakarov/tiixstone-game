<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Rat Pack
 */
class CFM_316 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_316';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}