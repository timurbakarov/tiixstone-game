<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mire Keeper
 */
class OG_202 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_202';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}