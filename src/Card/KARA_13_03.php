<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Orc Warrior
 */
class KARA_13_03 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KARA_13_03';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}