<?php

namespace Tiixstone\Card;

/**
 * Nightscale Matriarch
 */
class GIL_190 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_190';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}