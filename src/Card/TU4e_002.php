<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Flames of Azzinoth
 */
class TU4e_002 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TU4e_002';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}