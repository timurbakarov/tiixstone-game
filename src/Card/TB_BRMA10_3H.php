<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * The Rookery
 */
class TB_BRMA10_3H extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TB_BRMA10_3H';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}