<?php

namespace Tiixstone\Card;

/**
 * Overcharged Totem
 */
class TRLA_158 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_158';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}