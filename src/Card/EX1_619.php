<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;

/**
 * Equality
 */
class EX1_619 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_619';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
		$minions = $game->targetManager->allMinions($game);

		return array_map(function(Minion $minion) {
		    return new Block\GiveEffect($minion, new Effect\HealthChangerEffect(1, true));
        }, $minions);
    }
}