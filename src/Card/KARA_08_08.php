<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Red Portal
 */
class KARA_08_08 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KARA_08_08';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 11;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}