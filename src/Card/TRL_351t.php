<?php

namespace Tiixstone\Card;

/**
 * Toad
 */
class TRL_351t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_351t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}