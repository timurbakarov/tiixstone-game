<?php

namespace Tiixstone\Card;

/**
 * Akali, the Rhino
 */
class TRL_329 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_329';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}