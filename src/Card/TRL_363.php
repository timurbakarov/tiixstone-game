<?php

namespace Tiixstone\Card;

/**
 * Saronite Taskmaster
 */
class TRL_363 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_363';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}