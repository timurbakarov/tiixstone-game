<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Explosive Sheep
 */
class GVG_076 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_076';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}