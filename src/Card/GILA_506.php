<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * First Aid Kit
 */
class GILA_506 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_506';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}