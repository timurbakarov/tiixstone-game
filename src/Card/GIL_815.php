<?php

namespace Tiixstone\Card;

/**
 * Baleful Banker
 */
class GIL_815 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_815';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}