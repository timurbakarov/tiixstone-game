<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Sharp Fork
 */
class KAR_094a extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_094a';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}