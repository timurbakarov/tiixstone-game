<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Coop Boss
 */
class TB_CoopHero_H_001 extends Hero
{
    public function globalId() : string
    {
        return 'TB_CoopHero_H_001';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}