<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect\AttackChangerEffect;
use Tiixstone\Effect\Trigger\EndOfTheTurn;

/**
 * Spider Fangs
 */
class ICC_832pb extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_832pb';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new GiveEffect($game->currentPlayer()->hero, new EndOfTheTurn(new AttackChangerEffect(3))),
        ];
    }
}