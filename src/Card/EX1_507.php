<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Murloc Warleader
 */
class EX1_507 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_507';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}