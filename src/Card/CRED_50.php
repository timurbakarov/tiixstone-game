<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Brian Farr
 */
class CRED_50 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_50';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 11;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 11;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 11;
    }
}