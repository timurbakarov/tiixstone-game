<?php

namespace Tiixstone\Card;

/**
 * Fan of Flames
 */
class TRLA_135 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_135';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}