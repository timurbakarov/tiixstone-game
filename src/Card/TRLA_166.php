<?php

namespace Tiixstone\Card;

/**
 * Troll Harbinger
 */
class TRLA_166 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_166';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}