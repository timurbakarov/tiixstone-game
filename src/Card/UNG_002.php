<?php

namespace Tiixstone\Card;

use Tiixstone\Block\Adapt;
use Tiixstone\Effect\AdaptEffect;
use Tiixstone\Game;
use Tiixstone\Battlecryable;
use Tiixstone\Block\GiveEffect;

/**
 * Volcanosaur
 */
class UNG_002 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_002';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }

    /**
     * @param Game $game
     * @param Attackable|Minion $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        return [new Adapt($attackable, 2)];
    }
}