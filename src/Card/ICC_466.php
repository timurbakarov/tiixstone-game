<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Battlecryable;

/**
 * Saronite Chain Gang
 */
class ICC_466 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_466';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
	
	public function boardEffects()
	{
		return [Effect\TauntEffect::class];
	}

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|null $positionMinion
     * @param Character|null $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = null, Character $target = null)
    {
        $game->logBlock('saronite-chain-gang-battlecry');
		
        return [
            new Block\SummonMinion($game->currentPlayer(), $attackable->copy(), $attackable),
        ];
    }
}