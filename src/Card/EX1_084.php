<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\AuraAttackChangerEffect;
use Tiixstone\Effect\ChargeEffect;
use Tiixstone\Effect\Target\Aggregate;
use Tiixstone\Effect\Target\Friendly;
use Tiixstone\Effect\Target\HasEffect;
use Tiixstone\Effect\Trigger\Condition\ClosureCondition;

/**
 * Warsong Commander
 */
class EX1_084 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_084';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }


    /**
     * @return array
     */
    public function boardEffects()
    {
        return [new AuraAttackChangerEffect(1, new Aggregate([
            new HasEffect(ChargeEffect::class),
            new Friendly(),
            new \Tiixstone\Effect\Target\Minion
        ]))];
    }
}