<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Morgl the Oracle
 */
class HERO_02a extends Hero
{
    public function globalId() : string
    {
        return 'HERO_02a';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}