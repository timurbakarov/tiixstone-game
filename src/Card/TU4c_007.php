<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mukla's Big Brother
 */
class TU4c_007 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TU4c_007';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 10;
    }
}