<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Rallying Blade
 */
class OG_222 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_222';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}