<?php

namespace Tiixstone\Card;

/**
 * Dire Wolf Form
 */
class GIL_188b extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_188b';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}