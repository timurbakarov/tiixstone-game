<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Skelemancer
 */
class ICC_019 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_019';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}