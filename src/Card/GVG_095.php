<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Goblin Sapper
 */
class GVG_095 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_095';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}