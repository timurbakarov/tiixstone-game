<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hydrologist
 */
class UNG_011 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_011';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}