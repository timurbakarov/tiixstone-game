<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Han'Cho
 */
class TB_BossRumble_001 extends Hero
{
    public function globalId() : string
    {
        return 'TB_BossRumble_001';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}