<?php

namespace Tiixstone\Card;

/**
 * Jan'alai's Flame
 */
class TRLA_128t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_128t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}