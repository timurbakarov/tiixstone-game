<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Instructor Razuvious
 */
class NAX7_01 extends Hero
{
    public function globalId() : string
    {
        return 'NAX7_01';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}