<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Matt Place
 */
class CRED_65 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_65';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}