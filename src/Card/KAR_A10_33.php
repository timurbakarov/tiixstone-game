<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Cheat
 */
class KAR_A10_33 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'KAR_A10_33';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}