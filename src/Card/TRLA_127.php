<?php

namespace Tiixstone\Card;

/**
 * Astral Raptor
 */
class TRLA_127 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_127';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}