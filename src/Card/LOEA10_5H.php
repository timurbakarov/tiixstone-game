<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * Mrgl Mrgl Nyah Nyah
 */
class LOEA10_5H extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA10_5H';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}