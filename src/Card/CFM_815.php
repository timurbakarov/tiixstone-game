<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Wickerflame Burnbristle
 */
class CFM_815 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_815';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}