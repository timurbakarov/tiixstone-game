<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Golemagg
 */
class BRMC_95 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMC_95';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 50;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 20;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 20;
    }
}