<?php

namespace Tiixstone\Card;

/**
 * Vitus the Exiled
 */
class GILA_BOSS_46h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_46h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}