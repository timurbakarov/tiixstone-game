<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Mind Spike
 */
class EX1_625t extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'EX1_625t';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}