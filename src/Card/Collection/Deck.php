<?php declare(strict_types=1);

namespace Tiixstone\Card\Collection;

use Tiixstone\Card\Collection;

/**
 * Class Deck
 *
 * Колода карт
 *
 * @package Tiixstone\Card\Collection
 */
class Deck extends Collection
{
    /**
     * @return Deck
     */
    public function shuffle() : self
    {
        shuffle($this->cards);

        return $this;
    }
}