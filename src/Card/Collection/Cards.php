<?php

namespace Tiixstone\Card\Collection;

use Tiixstone\Card\Collection;

/**
 * Collection of all cards
 *
 * Class Cards
 * @package Tiixstone\Card\Collection
 */
class Cards extends Collection
{

}