<?php

namespace Tiixstone\Card\Collection;

use Tiixstone\Exception;
use Tiixstone\Card\Minion;
use Tiixstone\Card\Collection;
use Tiixstone\Game;

class Board extends Collection
{
    /**
     * @param Game $game
     * @return bool
     */
    public function isFull(Game $game)
    {
        return $this->count() >= $game->settings->maxPlacesOnBoard();
    }

    /**
     * @param Minion $minion
     * @return null|Minion
     */
    public function minionToPlaceNear(Minion $minion)
    {
        /** @var Minion $minion */
        while($minion = $this->next($minion->id())) {
            if(!$minion OR $minion->health->isAlive()) {
                break;
            }
        }

        return $minion ? $minion : null;
    }

    /**
     * @param Minion $minion
     * @return int
     * @throws Exception
     */
    public function position(Minion $minion)
    {
        if(!$this->has($minion->id())) {
            throw new Exception("Minion is not in board");
        }

        $i=1;
        foreach($this->all() as $item) {
            if($item->id() == $minion->id()) {
                return $i;
            }

            $i++;
        }

        throw new Exception("Minion is not in board");
    }
}