<?php

namespace Tiixstone\Card;

/**
 * Gral, the Shark
 */
class TRL_409 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_409';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}