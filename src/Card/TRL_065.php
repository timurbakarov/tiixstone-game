<?php

namespace Tiixstone\Card;

/**
 * Zul'jin
 */
class TRL_065 extends Hero
{
    public function globalId() : string
    {
        return 'TRL_065';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}