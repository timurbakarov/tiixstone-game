<?php

namespace Tiixstone\Card;

/**
 * Greater Amethyst Spellstone
 */
class LOOT_043t3 extends LOOT_043
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOOT_043t3';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function damage()
    {
        return 7;
    }
}