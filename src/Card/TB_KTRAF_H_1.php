<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Kel'Thuzad
 */
class TB_KTRAF_H_1 extends Hero
{
    public function globalId() : string
    {
        return 'TB_KTRAF_H_1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}