<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Temple Escape
 */
class LOEA04_01h extends Hero
{
    public function globalId() : string
    {
        return 'LOEA04_01h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}