<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Al'Akir the Windlord
 */
class NEW1_010 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_010';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}