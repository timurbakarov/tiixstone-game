<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Annoy-o-Tron
 */
class TB_MechWar_Boss1 extends Hero
{
    public function globalId() : string
    {
        return 'TB_MechWar_Boss1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}