<?php

namespace Tiixstone\Card;

/**
 * Free Agent
 */
class TRL_363t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_363t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}