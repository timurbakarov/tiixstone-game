<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Haunted Creeper
 */
class FP1_002 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_002';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}