<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * The Crone
 */
class KARA_04_01heroic extends Hero
{
    public function globalId() : string
    {
        return 'KARA_04_01heroic';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}