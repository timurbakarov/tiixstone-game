<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Volcanic Drake
 */
class BRM_025 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_025';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}