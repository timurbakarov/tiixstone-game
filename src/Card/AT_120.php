<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Frost Giant
 */
class AT_120 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_120';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}