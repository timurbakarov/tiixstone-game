<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Tricks of the Trade
 */
class TRLA_Rogue_05 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_Rogue_05';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}