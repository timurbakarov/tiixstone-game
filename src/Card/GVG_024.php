<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Cogmaster's Wrench
 */
class GVG_024 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_024';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}