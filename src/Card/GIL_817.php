<?php

namespace Tiixstone\Card;

/**
 * The Glass Knight
 */
class GIL_817 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_817';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}