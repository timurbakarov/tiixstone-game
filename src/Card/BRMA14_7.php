<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Electron
 */
class BRMA14_7 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA14_7';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}