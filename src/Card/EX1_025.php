<?php

namespace Tiixstone\Card;

use Tiixstone\Block\SummonMinion;
use Tiixstone\Card\Alias\MechanicalDragonling;
use Tiixstone\Game;
use Tiixstone\Battlecryable;

/**
 * Dragonling Mechanic
 */
class EX1_025 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_025';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        return [new SummonMinion($attackable->getPlayer(), MechanicalDragonling::create(), $attackable, 'right')];
    }
}