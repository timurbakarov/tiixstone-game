<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Black Rook
 */
class KAR_A10_03 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_A10_03';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}