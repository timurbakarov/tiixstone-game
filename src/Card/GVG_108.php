<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Recombobulator
 */
class GVG_108 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_108';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}