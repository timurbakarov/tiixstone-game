<?php

namespace Tiixstone\Card;

/**
 * Spirit of the Frog
 */
class TRL_060 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_060';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}