<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * SI:7 Agent
 */
class EX1_134 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_134';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}