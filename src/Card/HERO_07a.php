<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Nemsy Necrofizzle
 */
class HERO_07a extends Hero
{
    public function globalId() : string
    {
        return 'HERO_07a';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}