<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Elise Starseeker
 */
class LOE_079 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_079';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}