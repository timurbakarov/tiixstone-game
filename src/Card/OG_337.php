<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Cyclopian Horror
 */
class OG_337 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_337';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}