<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Battlecryable;
use Tiixstone\Card\Alias\LordJaraxxusHero;
use Tiixstone\Race;

/**
 * Lord Jaraxxus
 */
class EX1_323 extends Minion implements Battlecryable
{
    /**
     * @var int
     */
    protected $race = Race::DEMON;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_323';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 15;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        return [
            new Block\Hero\Replace(LordJaraxxusHero::create()),
            new Block\Battlefield\RemoveFromBattlefield($this),
        ];
    }
}