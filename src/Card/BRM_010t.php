<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Druid of the Flame
 */
class BRM_010t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_010t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}