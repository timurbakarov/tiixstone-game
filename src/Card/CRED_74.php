<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Drew Korfe
 */
class CRED_74 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_74';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}