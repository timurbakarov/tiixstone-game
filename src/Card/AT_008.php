<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Coldarra Drake
 */
class AT_008 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_008';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}