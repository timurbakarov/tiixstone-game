<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Piloted Sky Golem
 */
class GVG_105 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_105';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}