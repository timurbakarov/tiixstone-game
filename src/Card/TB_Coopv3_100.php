<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dragonscale Warrior
 */
class TB_Coopv3_100 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_Coopv3_100';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}