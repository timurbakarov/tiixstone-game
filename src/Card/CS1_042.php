<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;
use Tiixstone\Effect\TauntEffect;

/**
 * Goldshire Footman
 */
class CS1_042 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS1_042';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [TauntEffect::class];
    }
}