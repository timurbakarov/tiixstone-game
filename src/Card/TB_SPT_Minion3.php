<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Swordsman
 */
class TB_SPT_Minion3 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_SPT_Minion3';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}