<?php

namespace Tiixstone\Card;

/**
 * Ravenous Familiar
 */
class TRLA_181 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_181';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}