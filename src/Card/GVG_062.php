<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Cobalt Guardian
 */
class GVG_062 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_062';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}