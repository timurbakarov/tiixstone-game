<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Throw Rocks
 */
class LOEA07_29 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'LOEA07_29';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}