<?php

namespace Tiixstone\Card;

/**
 * Vile Necrodoctor
 */
class TRLA_182 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_182';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}