<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Stoneskin Gargoyle
 */
class FP1_027 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_027';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}