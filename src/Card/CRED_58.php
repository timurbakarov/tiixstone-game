<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jason Shattuck
 */
class CRED_58 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_58';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}