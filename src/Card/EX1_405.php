<?php

namespace Tiixstone\Card;

use Tiixstone\Effect;
use Tiixstone\Card\Minion;

/**
 * Shieldbearer
 */
class EX1_405 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_405';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }

	public function boardEffects()
	{
		return [Effect\TauntEffect::class];
	}
}