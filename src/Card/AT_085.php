<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;
use Tiixstone\Effect\AuraCostChangerEffect;
use Tiixstone\Effect\Target\Aggregate;
use Tiixstone\Effect\Target\Friendly;
use Tiixstone\Effect\Target\HeroPower;
use Tiixstone\Manager\TargetManager;

/**
 * Maiden of the Lake
 */
class AT_085 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_085';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [new AuraCostChangerEffect(-1, new Aggregate([new Friendly(), new HeroPower()]))];
    }
}