<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * A New Hero Approaches
 */
class FB_LK_ClearBoard extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FB_LK_ClearBoard';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 10;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}