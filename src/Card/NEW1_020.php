<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;
use Tiixstone\Effect\WildPyromancerEffect;

/**
 * Wild Pyromancer
 */
class NEW1_020 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_020';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [WildPyromancerEffect::class];
    }
}