<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Crazed Hunter
 */
class TU4d_002 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TU4d_002';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}