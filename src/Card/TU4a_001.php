<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Hogger
 */
class TU4a_001 extends Hero
{
    public function globalId() : string
    {
        return 'TU4a_001';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}