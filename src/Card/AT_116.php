<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Wyrmrest Agent
 */
class AT_116 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_116';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}