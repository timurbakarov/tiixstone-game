<?php

namespace Tiixstone\Card;

/**
 * Princess Talanji
 */
class TRL_259 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_259';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}