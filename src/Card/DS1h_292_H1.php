<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Steady Shot
 */
class DS1h_292_H1 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'DS1h_292_H1';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}