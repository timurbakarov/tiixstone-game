<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Transmute Spirit
 */
class ICC_481p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'ICC_481p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}