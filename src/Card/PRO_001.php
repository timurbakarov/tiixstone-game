<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Elite Tauren Chieftain
 */
class PRO_001 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'PRO_001';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}