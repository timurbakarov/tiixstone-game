<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Hateful Strike
 */
class NAX10_03 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'NAX10_03';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}