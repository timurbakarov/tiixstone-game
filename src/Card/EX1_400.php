<?php

namespace Tiixstone\Card;

use Tiixstone\Block\DealSplashDamage;
use Tiixstone\Game;
use Tiixstone\Manager\TargetManager;

/**
 * Whirlwind
 */
class EX1_400 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_400';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new DealSplashDamage(TargetManager::ANY_MINION, $game->withSpellDamage(1), $this)];
    }
}