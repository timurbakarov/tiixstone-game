<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Prince Arthas
 */
class HERO_04b extends Hero
{
    public function globalId() : string
    {
        return 'HERO_04b';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}