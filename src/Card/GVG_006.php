<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mechwarper
 */
class GVG_006 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_006';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}