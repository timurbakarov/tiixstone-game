<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tomb Pillager
 */
class LOE_012 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_012';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}