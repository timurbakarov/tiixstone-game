<?php

namespace Tiixstone\Card;

/**
 * Am'gam Rager
 */
class OG_248 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_248';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}