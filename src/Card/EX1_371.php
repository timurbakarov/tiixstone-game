<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect\DivineShieldEffect;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Hand of Protection
 */
class EX1_371 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_371';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new GiveEffect($target, DivineShieldEffect::class)];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
        ]);
    }
}