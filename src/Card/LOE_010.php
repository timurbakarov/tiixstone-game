<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Pit Snake
 */
class LOE_010 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_010';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}