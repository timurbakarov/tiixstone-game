<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Malfurion the Pestilent
 */
class ICC_832 extends Hero
{
    public function globalId() : string
    {
        return 'ICC_832';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}