<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Wailing Soul
 */
class FP1_016 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_016';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}