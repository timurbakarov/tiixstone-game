<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Block;
use Tiixstone\Stats;
use Tiixstone\Block\Trigger;

abstract class Weapon extends Attackable
{
    /**
     * @var Stats\Durability
     */
    public $durability;

    /**
     * @return mixed
     */
    abstract public function defaultDurability() : int;

    public function __construct()
    {
        parent::__construct();

        $this->durability = new Stats\Durability($this->defaultDurability());
    }

    /**
     * @param Game $game
     * @param string|NULL $positionCardId
     * @param Character|NULL $target
     * @return array
     */
    public function play(Game $game, Minion $positionMinion = null, Character $target = null) : array
    {
        return [
            new Trigger(new Event\BeforePlayWeapon($this)),
            new Block\Weapon\EquipWeapon($game->currentPlayer(), $this),
            new Trigger(new Event\AfterPlayWeapon($this)),
        ];
    }

    /**
     * @return array
     */
    public function equippedEffects() : array
    {
        return [];
    }

    /**
     * @param Game $game
     * @return array
     */
    public function destroy(Game $game): array
    {
        $game->logBlock('weapon-destroyed', ['weapon' => $this]);

        $this->getPlayer()->hero->setWeapon(null);

        $game->effects->removeByCard($this);

        $game->cardsList->remove($this->id());

        return [];
    }

    /**
     * @return bool
     */
    public function isDestroyed(): bool
    {
        return $this->durability->total() < 1 || $this->destroyed;
    }
}