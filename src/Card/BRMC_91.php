<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Son of the Flame
 */
class BRMC_91 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMC_91';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}