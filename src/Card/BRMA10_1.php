<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Razorgore the Untamed
 */
class BRMA10_1 extends Hero
{
    public function globalId() : string
    {
        return 'BRMA10_1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}