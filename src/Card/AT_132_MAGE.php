<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Fireblast Rank 2
 */
class AT_132_MAGE extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'AT_132_MAGE';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}