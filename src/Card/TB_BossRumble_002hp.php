<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Construct Golem
 */
class TB_BossRumble_002hp extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TB_BossRumble_002hp';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}