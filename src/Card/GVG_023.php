<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Goblin Auto-Barber
 */
class GVG_023 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_023';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}