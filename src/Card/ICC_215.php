<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Archbishop Benedictus
 */
class ICC_215 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_215';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}