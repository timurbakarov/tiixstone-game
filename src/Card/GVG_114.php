<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Sneed's Old Shredder
 */
class GVG_114 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_114';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}