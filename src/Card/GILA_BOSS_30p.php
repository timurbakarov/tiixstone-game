<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Blood Red Apple
 */
class GILA_BOSS_30p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_30p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}