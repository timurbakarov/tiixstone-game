<?php

namespace Tiixstone\Card;

/**
 * Splitting Sapling
 */
class GIL_616t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_616t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}