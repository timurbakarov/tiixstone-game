<?php

namespace Tiixstone\Card;

/**
 * Water Spirit
 */
class TRLA_121 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_121';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}