<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Rebuke
 */
class GIL_203 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_203';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}