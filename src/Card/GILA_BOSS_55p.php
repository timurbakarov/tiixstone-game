<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Unfinished Business
 */
class GILA_BOSS_55p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_55p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}