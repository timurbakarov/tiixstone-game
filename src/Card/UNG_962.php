<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Lightfused Stegodon
 */
class UNG_962 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_962';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}