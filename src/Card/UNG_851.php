<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Elise the Trailblazer
 */
class UNG_851 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_851';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}