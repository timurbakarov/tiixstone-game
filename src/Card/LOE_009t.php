<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Scarab
 */
class LOE_009t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_009t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}