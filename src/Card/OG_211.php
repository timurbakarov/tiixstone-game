<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * Call of the Wild
 */
class OG_211 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_211';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 9;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}