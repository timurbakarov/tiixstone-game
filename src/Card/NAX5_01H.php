<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Heigan the Unclean
 */
class NAX5_01H extends Hero
{
    public function globalId() : string
    {
        return 'NAX5_01H';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}