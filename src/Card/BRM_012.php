<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Fireguard Destroyer
 */
class BRM_012 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_012';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}