<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Flame Juggler
 */
class AT_094 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_094';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}