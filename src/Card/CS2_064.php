<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Battlecryable;
use Tiixstone\Manager\TargetManager;
use Tiixstone\Block\DealSplashDamage;

/**
 * Dread Infernal
 */
class CS2_064 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_064';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        return [new DealSplashDamage(TargetManager::ANY_CHARACTER, 1, $this, new Effect\Target\Not(new Effect\Target\SelfTarget))];
    }
}