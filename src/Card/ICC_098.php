<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tomb Lurker
 */
class ICC_098 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_098';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}