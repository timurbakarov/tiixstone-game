<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Core Rager
 */
class BRM_014 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_014';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}