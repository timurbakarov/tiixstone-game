<?php

namespace Tiixstone\Card;

/**
 * Halazzi, the Lynx
 */
class TRL_900 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_900';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}