<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Cauldron
 */
class LOEA09_7H extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA09_7H';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}