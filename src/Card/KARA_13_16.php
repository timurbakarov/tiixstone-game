<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Susie Sizzlesong
 */
class KARA_13_16 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KARA_13_16';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}