<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Big Bad Claws
 */
class KARA_05_02 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KARA_05_02';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}