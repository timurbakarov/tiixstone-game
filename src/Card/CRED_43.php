<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jon Bankard
 */
class CRED_43 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_43';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}