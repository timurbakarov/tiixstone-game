<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Celestial Dreamer
 */
class CFM_617 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_617';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}