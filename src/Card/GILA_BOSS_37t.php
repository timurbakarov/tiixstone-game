<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Plague of Murlocs
 */
class GILA_BOSS_37t extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_BOSS_37t';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}