<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Stablemaster
 */
class AT_057 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_057';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}