<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hungry Naga
 */
class LOEA09_13 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA09_13';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}