<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Nether Imp
 */
class UNG_829t3 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_829t3';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}