<?php

namespace Tiixstone\Card;

/**
 * Ancient
 */
class TRL_341t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_341t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}