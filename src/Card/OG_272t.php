<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Faceless Destroyer
 */
class OG_272t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_272t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}