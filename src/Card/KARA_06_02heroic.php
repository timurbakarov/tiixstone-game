<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Julianne
 */
class KARA_06_02heroic extends Hero
{
    public function globalId() : string
    {
        return 'KARA_06_02heroic';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}