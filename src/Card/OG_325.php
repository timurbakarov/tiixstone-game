<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Carrion Grub
 */
class OG_325 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_325';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}