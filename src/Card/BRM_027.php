<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Majordomo Executus
 */
class BRM_027 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_027';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 9;
    }
}