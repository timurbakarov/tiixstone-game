<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\DrawCard;
use Tiixstone\Block\TakeDamage;
use Tiixstone\PlayCardCondition;
use Tiixstone\Block\ClosureBlock;

/**
 * Slam
 */
class EX1_391 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_391';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null|Minion $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new TakeDamage($target, 2, $this),
            new ClosureBlock(function() use($target) {
                if($target->health->isAlive()) {
                    return [new DrawCard($this->getPlayer())];
                }

                return [];
            }),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion(),
        ]);
    }
}