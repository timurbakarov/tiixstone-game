<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Raptor Hatchling
 */
class UNG_914 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_914';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}