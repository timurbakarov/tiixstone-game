<?php

namespace Tiixstone\Card;

/**
 * Treant
 */
class GIL_663t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_663t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}