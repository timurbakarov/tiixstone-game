<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Stonesplinter Trogg
 */
class GVG_067 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_067';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}