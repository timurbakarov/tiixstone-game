<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Emerald Hive Queen
 */
class UNG_085 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_085';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}