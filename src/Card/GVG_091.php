<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\TauntEffect;
use Tiixstone\Effect\ElusiveEffect;

/**
 * Arcane Nullifier X-21
 */
class GVG_091 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_091';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [TauntEffect::class, ElusiveEffect::class];
    }
}