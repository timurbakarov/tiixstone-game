<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Grim Rally
 */
class TRL_249 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_249';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}