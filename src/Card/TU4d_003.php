<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Shotgun Blast
 */
class TU4d_003 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TU4d_003';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}