<?php

namespace Tiixstone\Card;

/**
 * Halazzi's Guise
 */
class TRLA_163 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_163';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}