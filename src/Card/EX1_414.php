<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Grommash Hellscream
 */
class EX1_414 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_414';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}