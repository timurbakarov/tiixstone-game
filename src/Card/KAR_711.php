<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Arcane Giant
 */
class KAR_711 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_711';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 12;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}