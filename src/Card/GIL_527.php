<?php

namespace Tiixstone\Card;

/**
 * Felsoul Inquisitor
 */
class GIL_527 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_527';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}