<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Faceless Summoner
 */
class OG_207 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_207';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}