<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Buccaneer
 */
class AT_029 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_029';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}