<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Black Pawn
 */
class KAR_A10_01 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_A10_01';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}