<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tar Lord
 */
class UNG_838 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_838';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 11;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}