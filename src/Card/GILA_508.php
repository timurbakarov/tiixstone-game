<?php

namespace Tiixstone\Card;

/**
 * The Exorcisor
 */
class GILA_508 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_508';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}