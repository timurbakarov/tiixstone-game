<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\MountainGiantEffect;

/**
 * Mountain Giant
 */
class EX1_105 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_105';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 12;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }

    /**
     * @return array
     */
    public function handEffects()
    {
        return [MountainGiantEffect::class];
    }
}