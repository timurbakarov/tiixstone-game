<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;
use Tiixstone\Effect\MistressOfMixturesDeathrattle;

/**
 * Mistress of Mixtures
 */
class CFM_120 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_120';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [MistressOfMixturesDeathrattle::class];
    }
}