<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;
use Tiixstone\Effect\ChargeEffect;

/**
 * Stormwind Knight
 */
class CS2_131 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_131';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [ChargeEffect::class];
    }
}