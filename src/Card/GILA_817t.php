<?php

namespace Tiixstone\Card;

/**
 * Weeping Ghost
 */
class GILA_817t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_817t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}