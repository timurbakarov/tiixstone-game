<?php declare(strict_types=1);

namespace Tiixstone\Card;

use Tiixstone\Card;
use Tiixstone\Exception;

abstract class Collection
{
    /**
     * @var Card[]
     */
    protected $cards = [];

    /**
     * Collection constructor.
     *
     * @param Card[] $cards
     */
    public function __construct(array $cards = [])
    {
        foreach($cards as $card) {
            $this->append($card);
        }
    }

    /**
     * @return mixed
     */
    public function random(int $num = null)
    {
        return array_random($this->cards, $num);
    }

    /**
     * @param Card $card
     * @param string $id
     * @return $this
     */
    public function addBefore(Card $card, string $id)
    {
        $targetCard = $this->get($id);

        $position = 0;
        foreach($this->all() as $item) {
            if($item->id() == $targetCard->id()) {
                break;
            }

            $position++;
        }

        $this->cards = array_replace(
            array_slice($this->cards, 0, $position, true),
            [$card->id() => $card],
            array_slice($this->cards, $position, null, true)
        );

        reset($this->cards);

        return $this;
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return !$this->count();
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->cards);
    }

    /**
     * @param Card $card
     * @return Collection
     */
    public function append(Card $card)
    {
        if($this->has($card->id())) {
            throw new Exception(sprintf("Card with key [%s] exists", $card->id()));
        }

        $this->cards[$card->id()] = $card;

        return $this;
    }

    /**
     * @param Card[] $cards
     * @return $this
     */
    public function appendMany(array $cards)
    {
        foreach($cards as $card) {
            $this->append($card);
        }

        return $this;
    }

    /**
     * @param int $position
     * @return Card
     * @throws Exception
     */
    public function getByPosition(int $position) : Card
    {
        $i=1;
        foreach($this->all() as $card) {
            if($position == $i) {
                return $card;
            }

            $i++;
        }

        throw new Exception(sprintf("Card with position [%s] does not exist", $position));
    }

    /**
     * @return Card
     */
    public function shift() : Card
    {
        if(!$this->count()) {
            throw new Exception("No more cards to shift");
        }

        foreach($this->cards as $key => $card) {
            unset($this->cards[$key]);
            return $card;
        }
    }

    /**
     * @return Card
     */
    public function first() : Card
    {
        if(!$this->count()) {
            throw new Exception("Collection is empty");
        }

        return reset($this->cards);
    }

    /**
     * @return Card
     */
    public function last() : Card
    {
        if(!$this->count()) {
            throw new Exception("Collection is empty");
        }

        return end($this->cards);
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has(string $key) : bool
    {
        return isset($this->cards[$key]);
    }

    /**
     * @param string $key
     * @return Card
     * @throws Exception
     */
    public function get(string $key) : Card
    {
        if(!$this->has($key)) {
            throw new Exception(sprintf("Card with key %s does not exist", $key));
        }

        return $this->cards[$key];
    }

    /**
     * @param string $key
     * @return Card
     */
    public function pull(string $key) : Card
    {
        $card = $this->get($key);

        $this->remove($key);

        return $card;
    }

    /**
     * @param string $key
     * @return $this
     * @throws Exception
     */
    public function remove(string $key)
    {
        if(!$this->has($key)) {
            throw new Exception(sprintf("Card with key %s does not exist", $key));
        }

        unset($this->cards[$key]);

        return $this;
    }

    /**
     * @return array|\Tiixstone\Card[]
     */
    public function all()
    {
        return $this->cards;
    }

    /**
     * @param string $key
     * @return bool|mixed|Card
     */
    public function prev(string $key)
    {
        $position = false;

        $i=0;
        foreach($this->all() as $card) {
            if($card->id() == $key) {
                $position = $i;
                break;
            }

            $i++;
        }

        $i=0;
        foreach($this->all() as $card) {
            if($i == ($position - 1)) {
                return $card;
            }

            $i++;
        }

        return false;
    }

    /**
     * @param string $key
     * @return bool|mixed|Card
     */
    public function next(string $key)
    {
        $position = false;

        $i=0;
        foreach($this->all() as $card) {
            if($card->id() == $key) {
                $position = $i;
                break;
            }

            $i++;
        }

        if($position === false OR $position == ($this->count() - 1)) {
            return false;
        }

        $i=0;
        foreach($this->all() as $card) {
            if($i == ($position + 1)) {
                return $card;
            }

            $i++;
        }

        return false;
    }
}