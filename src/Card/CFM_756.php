<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Alley Armorsmith
 */
class CFM_756 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_756';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}