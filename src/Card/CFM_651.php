<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Naga Corsair
 */
class CFM_651 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_651';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}