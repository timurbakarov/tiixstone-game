<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Baron Geddon
 */
class BRMA05_1H extends Hero
{
    public function globalId() : string
    {
        return 'BRMA05_1H';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}