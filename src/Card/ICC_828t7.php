<?php

namespace Tiixstone\Card;

/**
 * Vicious Scalehide
 */
class ICC_828t7 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_828t7';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}