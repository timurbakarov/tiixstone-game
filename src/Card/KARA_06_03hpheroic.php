<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * True Love
 */
class KARA_06_03hpheroic extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'KARA_06_03hpheroic';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}