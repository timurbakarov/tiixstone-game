<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Coghammer
 */
class GVG_059 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_059';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}