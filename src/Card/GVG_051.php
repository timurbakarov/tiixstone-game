<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Warbot
 */
class GVG_051 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_051';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}