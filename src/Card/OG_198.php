<?php

namespace Tiixstone\Card;

use Tiixstone\Block\HealCharacter;
use Tiixstone\Block\SetManaAvailable;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Forbidden Healing
 */
class OG_198 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_198';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $mana = $this->getPlayer()->availableMana();

        return [
            new SetManaAvailable($this->getPlayer(), 0),
            new HealCharacter($target, $mana * 2, $this),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Character(),
        ]);
    }
}