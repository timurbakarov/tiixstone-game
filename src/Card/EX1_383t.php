<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Ashbringer
 */
class EX1_383t extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_383t';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 5;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}