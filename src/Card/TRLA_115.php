<?php

namespace Tiixstone\Card;

/**
 * Gonk's Mark
 */
class TRLA_115 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_115';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}