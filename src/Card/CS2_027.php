<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\SummonMinion;

/**
 * Mirror Image
 */
class CS2_027 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_027';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new SummonMinion($game->currentPlayer(), new CS2_mirror()),
            new SummonMinion($game->currentPlayer(), new CS2_mirror()),
        ];
    }
}