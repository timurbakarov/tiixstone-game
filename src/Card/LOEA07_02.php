<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Mine Shaft
 */
class LOEA07_02 extends Hero
{
    public function globalId() : string
    {
        return 'LOEA07_02';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}