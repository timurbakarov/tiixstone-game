<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Growing Ooze
 */
class ICCA07_004 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA07_004';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}