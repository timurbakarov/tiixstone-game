<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Justicar Trueheart
 */
class AT_132 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_132';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}