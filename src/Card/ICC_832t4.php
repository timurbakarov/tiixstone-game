<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Scarab Beetle
 */
class ICC_832t4 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_832t4';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}