<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Damaged Golem
 */
class skele21 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'skele21';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}