<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Manager\TargetManager;

/**
 * Arcane Missiles
 */
class EX1_277 extends Spell
{
    const DEFAULT_MISSILES_COUNT = 3;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_277';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new Block\RandomMissilesDamage(
            TargetManager::ANY_OPPONENT_CHARACTER,
            1,
            $game->withSpellDamage(self::DEFAULT_MISSILES_COUNT),
            $this
        )];
    }
}