<?php

namespace Tiixstone\Card;

/**
 * Sephira Dusktalon
 */
class GILA_BOSS_63h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_63h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}