<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Alex Chapman
 */
class CRED_52 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_52';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}