<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\PlayCardCondition;

/**
 * Earth Shock
 */
class EX1_245 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_245';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new Block\GiveEffect($target, Effect\SilenceEffect::class),
            new Block\TakeDamage($target, $game->withSpellDamage(1)),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
        ]);
    }
}