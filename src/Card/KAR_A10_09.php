<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * White Queen
 */
class KAR_A10_09 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_A10_09';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}