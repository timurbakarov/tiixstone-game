<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Alias\DaggerMastery;
use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Valeera Sanguinar
 */
class HERO_03 extends Hero
{
    public function globalId() : string
    {
        return 'HERO_03';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return DaggerMastery::create();
    }
}