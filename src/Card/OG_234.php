<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Darkshire Alchemist
 */
class OG_234 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_234';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}