<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Backstreet Leper
 */
class CFM_646 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_646';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}