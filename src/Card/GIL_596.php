<?php

namespace Tiixstone\Card;

/**
 * Silver Sword
 */
class GIL_596 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_596';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 8;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}