<?php

namespace Tiixstone\Card;

/**
 * Cursed Castaway
 */
class GIL_557 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_557';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}