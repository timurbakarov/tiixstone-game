<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Reliquary Seeker
 */
class LOE_116 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_116';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}