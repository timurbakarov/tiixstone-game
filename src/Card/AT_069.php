<?php

namespace Tiixstone\Card;
use Tiixstone\Battlecryable;
use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect\TauntEffect;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Sparring Partner
 */
class AT_069 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_069';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [TauntEffect::class];
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        if(!$target) {
            return [];
        }

        return [new GiveEffect($target, TauntEffect::class)];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
        ]);
    }
}