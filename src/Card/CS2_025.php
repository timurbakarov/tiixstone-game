<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Manager\TargetManager;
use Tiixstone\Block\DealSplashDamage;

/**
 * Arcane Explosion
 */
class CS2_025 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_025';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new DealSplashDamage(
            $this->damageTargets(),
            $game->withSpellDamage($this->damage()),
            $this
        )];
    }

    /**
     * @return int
     */
    private function damageTargets()
    {
        return TargetManager::ANY_OPPONENT_MINION;
    }

    /**
     * @return int
     */
    private function damage() : int
    {
        return 1;
    }
}