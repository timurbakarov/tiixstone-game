<?php

namespace Tiixstone\Card;

/**
 * Serrated Tooth
 */
class TRL_074 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_074';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}