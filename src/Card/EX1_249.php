<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Baron Geddon
 */
class EX1_249 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_249';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}