<?php

namespace Tiixstone\Card;

/**
 * Mojomaster Zihi
 */
class TRL_564 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_564';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}