<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\Deathrattle\ZombieChowDeathrattle;

/**
 * Zombie Chow
 */
class FP1_001 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_001';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [ZombieChowDeathrattle::class];
    }
}