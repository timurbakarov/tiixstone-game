<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Chromatic Drake
 */
class BRMA10_5H extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA10_5H';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}