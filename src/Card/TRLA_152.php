<?php

namespace Tiixstone\Card;

/**
 * Soulsapper
 */
class TRLA_152 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_152';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}