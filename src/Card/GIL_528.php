<?php

namespace Tiixstone\Card;

/**
 * Swift Messenger
 */
class GIL_528 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_528';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}