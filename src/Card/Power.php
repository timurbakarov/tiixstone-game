<?php

namespace Tiixstone\Card;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block\UsePower;
use Tiixstone\Block\ClosureBlock;

abstract class Power extends Card
{
    /**
     * @var
     */
    protected $usedThisTurn = false;

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    abstract public function use(Game $game, Character $target = null);

    /**
     * @param Game $game
     * @param Minion|null $positionMinion
     * @param Character|null $target
     * @return array
     */
    final public function play(Game $game, Minion $positionMinion = null, Character $target = null) : array
    {
        return [
            new ClosureBlock(function(Game $game) use($target) {
                $game->logBlock('hero-power-played', [
                    'player' => $game->currentPlayer(),
                    'power' => $this,
                    'target' => $target,
                ]);

                return [];
            }),
            new UsePower($this, $target),
        ];
    }

    /**
     * @return bool
     */
    public function usedThisTurn() : bool
    {
        return $this->usedThisTurn;
    }

    /**
     * @param bool $value
     * @return $this
     */
    public function setUsedThisTurn(bool $value)
    {
        $this->usedThisTurn = $value;

        return $this;
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function effects() : array
    {
        return [];
    }
}