<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Brass Knuckles
 */
class CFM_631 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_631';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}