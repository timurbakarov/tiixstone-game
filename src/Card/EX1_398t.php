<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Battle Axe
 */
class EX1_398t extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_398t';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}