<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Yong Woo
 */
class CRED_14 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_14';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}