<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Understudy
 */
class NAX7_02 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX7_02';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}