<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Shadowreaper Anduin
 */
class ICC_830 extends Hero
{
    public function globalId() : string
    {
        return 'ICC_830';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}