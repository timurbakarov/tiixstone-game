<?php

namespace Tiixstone\Card;

/**
 * War Master Voone
 */
class TRL_328 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_328';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}