<?php

namespace Tiixstone\Card;

/**
 * Ravencaller
 */
class GIL_212 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_212';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}