<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect\PowerWordGloryEffect;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Power Word: Glory
 */
class AT_013 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_013';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new GiveEffect($target, new PowerWordGloryEffect($game->currentPlayer()))];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
        ]);
    }
}