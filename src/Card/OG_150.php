<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Aberrant Berserker
 */
class OG_150 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_150';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}