<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Validated Doomsayer
 */
class OG_200 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_200';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}