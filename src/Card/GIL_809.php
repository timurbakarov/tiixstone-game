<?php

namespace Tiixstone\Card;

/**
 * Unpowered Steambot
 */
class GIL_809 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_809';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}