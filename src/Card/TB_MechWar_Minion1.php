<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mech Fan
 */
class TB_MechWar_Minion1 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_MechWar_Minion1';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}