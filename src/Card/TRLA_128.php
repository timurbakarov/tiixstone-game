<?php

namespace Tiixstone\Card;

/**
 * Jan'alai's Flame
 */
class TRLA_128 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_128';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}