<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Leatherclad Hogleader
 */
class CFM_810 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_810';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}