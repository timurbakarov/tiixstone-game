<?php

namespace Tiixstone\Card;

/**
 * Face Collector
 */
class GIL_677 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_677';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}