<?php

namespace Tiixstone\Card;

/**
 * Bwonsamdi's Covenant
 */
class TRLA_147 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_147';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}