<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;
use Tiixstone\Effect\TauntEffect;

/**
 * Mogu'shan Warden
 */
class EX1_396 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_396';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [TauntEffect::class];
    }
}