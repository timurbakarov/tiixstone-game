<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Gallery Protection
 */
class KARA_07_02 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'KARA_07_02';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}