<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Cash In
 */
class TP_Bling_HP2 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TP_Bling_HP2';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}