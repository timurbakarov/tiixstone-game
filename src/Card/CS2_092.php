<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Condition;
use Tiixstone\Block\GiveEffect;
use Tiixstone\PlayCardCondition;

/**
 * Blessing of Kings
 */
class CS2_092 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_092';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
			new GiveEffect($target, new Effect\AttackChangerEffect(4)),
			new GiveEffect($target, new Effect\HealthChangerEffect(4)),
		];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Minion,
        ]);
    }
}