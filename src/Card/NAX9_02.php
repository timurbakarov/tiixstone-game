<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Lady Blaumeux
 */
class NAX9_02 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX9_02';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}