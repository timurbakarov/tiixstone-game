<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\Overload;
use Tiixstone\Block\TakeDamage;
use Tiixstone\PlayCardCondition;

/**
 * Lightning Bolt
 */
class EX1_238 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_238';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new TakeDamage($target, $game->withSpellDamage(3)),
            new Overload($game->currentPlayer(), 1),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Character,
        ]);
    }
}