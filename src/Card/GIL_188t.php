<?php

namespace Tiixstone\Card;

/**
 * Druid of the Scythe
 */
class GIL_188t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_188t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}