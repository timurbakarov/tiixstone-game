<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\Inspire\LowlySquireEffect;

/**
 * Lowly Squire
 */
class AT_082 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_082';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [LowlySquireEffect::class];
    }
}