<?php

namespace Tiixstone\Card;

use Tiixstone\Block\Adapt;
use Tiixstone\Condition\Target\Friendly;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Adaptation
 */
class UNG_961 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_961';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new Adapt($target),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Friendly(),
            new \Tiixstone\Condition\Target\Minion(),
        ]);
    }
}