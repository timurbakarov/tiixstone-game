<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\IncreaseArmor;

/**
 * Armor Up!
 */
class CS2_102 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'CS2_102';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [new IncreaseArmor($game->currentPlayer()->hero, $this->defaultArmorAmount())];
    }

    /**
     * @return int
     */
    private function defaultArmorAmount()
    {
        return 2;
    }
}