<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Creepy Curio
 */
class GILA_817 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_817';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}