<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Lord Marrowgar
 */
class FB_LK_012h extends Hero
{
    public function globalId() : string
    {
        return 'FB_LK_012h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}