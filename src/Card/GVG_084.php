<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Flying Machine
 */
class GVG_084 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_084';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}