<?php

namespace Tiixstone\Card;

/**
 * Princess
 */
class GILA_411 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_411';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}