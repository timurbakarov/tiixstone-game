<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Rend Blackhand
 */
class BRM_029 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_029';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}