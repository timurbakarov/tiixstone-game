<?php

namespace Tiixstone\Card;

use Tiixstone\Block\PutCardInHand;
use Tiixstone\Card;
use Tiixstone\Game;

/**
 * Mind Vision
 */
class CS2_003 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_003';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        /** @var Card[] $enemyHandCards */
        $enemyHandCards = $this->getPlayer()->getOpponent($game)->hand->all();

        if(!$enemyHandCards) {
            return [];
        }

        /** @var Card $randomCard */
        $randomCard = $game->RNG->randomFromArray($enemyHandCards);

        return [new PutCardInHand($this->getPlayer(), $randomCard->copy())];
    }
}