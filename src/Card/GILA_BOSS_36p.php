<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Witch's Kiss
 */
class GILA_BOSS_36p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_36p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}