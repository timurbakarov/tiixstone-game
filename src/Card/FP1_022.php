<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Voidcaller
 */
class FP1_022 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_022';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}