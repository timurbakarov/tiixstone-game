<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Andy Brock
 */
class CRED_15 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_15';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}