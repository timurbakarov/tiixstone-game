<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\MoltenGiantEffect;

/**
 * Molten Giant
 */
class EX1_620 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_620';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 25;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }

    /**
     * @return array
     */
    public function handEffects()
    {
        return [MoltenGiantEffect::class];
    }
}