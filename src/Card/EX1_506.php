<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Race;
use Tiixstone\Battlecryable;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Card\Alias\MurlocScout;

/**
 * Murloc Tidehunter
 */
class EX1_506 extends Minion implements Battlecryable
{
    /**
     * @var int
     */
    protected $race = Race::MURLOC;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_506';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        return [new SummonMinion($attackable->getPlayer(), MurlocScout::create(), $attackable, 'right')];
    }
}