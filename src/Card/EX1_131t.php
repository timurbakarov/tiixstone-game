<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Defias Bandit
 */
class EX1_131t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_131t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}