<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Nightbane
 */
class KARA_11_01heroic extends Hero
{
    public function globalId() : string
    {
        return 'KARA_11_01heroic';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}