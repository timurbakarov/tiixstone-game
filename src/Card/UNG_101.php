<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Shellshifter
 */
class UNG_101 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_101';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}