<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Condition\Target\Friendly;
use Tiixstone\Effect\AttackChangerEffect;
use Tiixstone\Effect\HealthChangerEffect;
use Tiixstone\Game;
use Tiixstone\Battlecryable;
use Tiixstone\PlayCardCondition;

/**
 * Shattered Sun Cleric
 */
class EX1_019 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_019';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        if(!$target) {
            return [];
        }

        return [
            new GiveEffect($target, new AttackChangerEffect(1)),
            new GiveEffect($target, new HealthChangerEffect(1)),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Friendly(),
            new \Tiixstone\Condition\Target\Minion(),
        ]);
    }
}