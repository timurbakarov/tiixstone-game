<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Weapon Rack
 */
class TB_SPT_DPromoCrate1 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_SPT_DPromoCrate1';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}