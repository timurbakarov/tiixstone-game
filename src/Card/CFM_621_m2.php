<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Kabal Demon
 */
class CFM_621_m2 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_621_m2';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}