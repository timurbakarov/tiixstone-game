<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Drakonid Crusher
 */
class BRM_024 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_024';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}