<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Spoon
 */
class KAR_A02_02H extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_A02_02H';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}