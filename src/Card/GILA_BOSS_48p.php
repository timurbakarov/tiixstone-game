<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Chronoacceleration
 */
class GILA_BOSS_48p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_48p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}