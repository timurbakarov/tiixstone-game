<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ogre Brute
 */
class GVG_065 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_065';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}