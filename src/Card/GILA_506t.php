<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Bandage
 */
class GILA_506t extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_506t';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}