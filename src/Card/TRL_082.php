<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Big Bad Voodoo
 */
class TRL_082 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_082';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}