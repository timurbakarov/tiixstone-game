<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Cutpurse
 */
class AT_031 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_031';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}