<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dunemaul Shaman
 */
class GVG_066 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_066';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}