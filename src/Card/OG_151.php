<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\Deathrattle\TentacleOfNZothDeathrattle;

/**
 * Tentacle of N'Zoth
 */
class OG_151 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_151';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [TentacleOfNZothDeathrattle::class];
    }
}