<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Michael Altuna
 */
class CRED_60 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_60';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}