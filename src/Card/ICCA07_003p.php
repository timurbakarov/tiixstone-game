<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Madder Science
 */
class ICCA07_003p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'ICCA07_003p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}