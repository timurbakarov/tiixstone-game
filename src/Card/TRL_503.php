<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Alias\Scarab;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Effect\DeathrattleEffect;

/**
 * Scarab Egg
 */
class TRL_503 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_503';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [
            $this->deathrattle(),
        ];
    }

    /**
     * @return DeathrattleEffect
     */
    private function deathrattle()
    {
        return new class extends DeathrattleEffect {

            /**
             * @param Game $game
             * @param Minion $minion
             * @param Minion|NULL $nextMinion
             * @return mixed
             */
            public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL): array
            {
                return [
                    new SummonMinion($minion->getPlayer(), $minion1 = Scarab::create(), $nextMinion),
                    new SummonMinion($minion->getPlayer(), $minion2 = Scarab::create(), $minion1),
                    new SummonMinion($minion->getPlayer(), Scarab::create(), $minion2),
                ];
            }
        };
    }
}