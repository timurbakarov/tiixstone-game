<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Siltfin Spiritwalker
 */
class GVG_040 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_040';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}