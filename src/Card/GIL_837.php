<?php

namespace Tiixstone\Card;

/**
 * Glitter Moth
 */
class GIL_837 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_837';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}