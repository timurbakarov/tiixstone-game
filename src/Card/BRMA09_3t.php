<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Old Horde Orc
 */
class BRMA09_3t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA09_3t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}