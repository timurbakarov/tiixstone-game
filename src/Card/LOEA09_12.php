<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hungry Naga
 */
class LOEA09_12 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA09_12';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}