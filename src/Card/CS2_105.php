<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect\AttackChangerEffect;
use Tiixstone\Effect\Trigger\EndOfTheTurn;
use Tiixstone\Game;

/**
 * Heroic Strike
 */
class CS2_105 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_105';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new GiveEffect($game->currentPlayer()->hero, new EndOfTheTurn(new AttackChangerEffect(4)))];
    }
}