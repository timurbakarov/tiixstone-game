<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Drygulch Jailor
 */
class LOOT_363 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOOT_363';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
	
	/**
     * @return array
     */
	public function boardEffects()
	{
		return [\Tiixstone\Effect\DrygulchJailorDeathrattle::class];
	}
}