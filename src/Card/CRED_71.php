<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Michael Reynaga
 */
class CRED_71 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_71';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}