<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Onyxia
 */
class EX1_562 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_562';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}