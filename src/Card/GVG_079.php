<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Force-Tank MAX
 */
class GVG_079 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_079';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}