<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Slithering Archer
 */
class LOEA09_6 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOEA09_6';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}