<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect\HealthChangerEffect;
use Tiixstone\Game;

/**
 * Totemic Might
 */
class EX1_244 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_244';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return array_map(function(Minion $minion) {
            if(!$minion->race()->isTotem()) {
                return [];
            }

            return [new GiveEffect($minion, new HealthChangerEffect(2))];
        }, $game->targetManager->allPlayerMinions($game, $game->currentPlayer()));
    }
}