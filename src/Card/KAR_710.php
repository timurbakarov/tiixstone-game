<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Arcanosmith
 */
class KAR_710 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_710';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}