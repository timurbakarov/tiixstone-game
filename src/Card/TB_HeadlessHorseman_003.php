<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Pulsing Pumpkin
 */
class TB_HeadlessHorseman_003 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_HeadlessHorseman_003';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}