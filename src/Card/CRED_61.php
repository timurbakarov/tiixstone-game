<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Charlène Le Scanff
 */
class CRED_61 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_61';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}