<?php

namespace Tiixstone\Card;

/**
 * Azalina Soulthief
 */
class GIL_198 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_198';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}