<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Sindragosa
 */
class ICC_838 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_838';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}