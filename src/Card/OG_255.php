<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Doomcaller
 */
class OG_255 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_255';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}