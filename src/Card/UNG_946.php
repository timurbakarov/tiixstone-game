<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Gluttonous Ooze
 */
class UNG_946 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_946';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}