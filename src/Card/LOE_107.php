<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Eerie Statue
 */
class LOE_107 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_107';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}