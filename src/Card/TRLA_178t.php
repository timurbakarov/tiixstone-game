<?php

namespace Tiixstone\Card;

/**
 * Dark Reliquary
 */
class TRLA_178t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_178t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return ;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}