<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Menagerie Warden
 */
class KAR_065 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_065';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}