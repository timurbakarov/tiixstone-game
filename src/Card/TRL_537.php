<?php

namespace Tiixstone\Card;

/**
 * Da Undatakah
 */
class TRL_537 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_537';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}