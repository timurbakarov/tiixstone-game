<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Swashburglar
 */
class KAR_069 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_069';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}