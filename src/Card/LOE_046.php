<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Huge Toad
 */
class LOE_046 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_046';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}