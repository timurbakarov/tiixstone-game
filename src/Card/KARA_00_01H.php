<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Prince Malchezaar
 */
class KARA_00_01H extends Hero
{
    public function globalId() : string
    {
        return 'KARA_00_01H';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}