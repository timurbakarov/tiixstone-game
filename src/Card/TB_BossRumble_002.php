<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Aya Blackpaw
 */
class TB_BossRumble_002 extends Hero
{
    public function globalId() : string
    {
        return 'TB_BossRumble_002';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}