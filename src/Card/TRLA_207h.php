<?php

namespace Tiixstone\Card;

/**
 * Hex Lord Malacrass
 */
class TRLA_207h extends Hero
{
    public function globalId() : string
    {
        return 'TRLA_207h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}