<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\Battlecryable;
use Tiixstone\Block\TakeDamage;
use Tiixstone\PlayCardCondition;

/**
 * Elven Archer
 */
class CS2_189 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_189';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = null, Character $target = null)
    {
        $game->logBlock('elven-archer-battlecry');

        return [
            new TakeDamage($target, 1),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Character,
        ]);
    }
}