<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Dire Shapeshift
 */
class AT_132_DRUID extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'AT_132_DRUID';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}