<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Happy Ghoul
 */
class ICC_700 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_700';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}