<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Eydis Darkbane
 */
class AT_131 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_131';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}