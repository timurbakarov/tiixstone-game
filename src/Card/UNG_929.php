<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Molten Blade
 */
class UNG_929 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_929';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}