<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Unbalancing Strike
 */
class NAX7_03H extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'NAX7_03H';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}