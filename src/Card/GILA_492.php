<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Go for the Throat
 */
class GILA_492 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_492';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}