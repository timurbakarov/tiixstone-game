<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Arcanotron
 */
class BRMA14_3 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA14_3';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}