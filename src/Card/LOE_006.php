<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Museum Curator
 */
class LOE_006 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_006';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}