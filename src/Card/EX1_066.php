<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Battlecryable;
use Tiixstone\Block\MarkDestroyed;

/**
 * Acidic Swamp Ooze
 */
class EX1_066 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_066';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = null, Character $target = null)
    {
        if(!$game->idlePlayer()->hero->hasWeapon()) {
            return [];
        }

        $game->logBlock('destroy-weapon-battlecry', [
            'minion' => $attackable,
            'weapon' => $game->idlePlayer()->hero->weapon,
        ]);

        return [new MarkDestroyed($game->idlePlayer()->hero->weapon)];
    }
}