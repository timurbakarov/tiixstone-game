<?php

namespace Tiixstone\Card;

/**
 * Dollmaster Dorian
 */
class GIL_620 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_620';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}