<?php

namespace Tiixstone\Card;

/**
 * Bloodhound
 */
class GILA_400t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_400t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}