<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Jaws
 */
class NAX12_03H extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NAX12_03H';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 5;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}