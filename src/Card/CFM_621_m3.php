<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Kabal Demon
 */
class CFM_621_m3 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_621_m3';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 8;
    }
}