<?php

namespace Tiixstone\Card;

/**
 * Prince Liam
 */
class GIL_694 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_694';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}