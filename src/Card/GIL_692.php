<?php

namespace Tiixstone\Card;

/**
 * Genn Greymane
 */
class GIL_692 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_692';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}