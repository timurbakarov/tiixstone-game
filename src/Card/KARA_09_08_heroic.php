<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Kil'rek
 */
class KARA_09_08_heroic extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KARA_09_08_heroic';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}