<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Silverware Golem
 */
class KAR_205 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_205';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}