<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * OLDN3wb Tank
 */
class TBST_001 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TBST_001';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}