<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hadronox
 */
class ICC_835 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_835';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}