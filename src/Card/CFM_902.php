<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Aya Blackpaw
 */
class CFM_902 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_902';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}