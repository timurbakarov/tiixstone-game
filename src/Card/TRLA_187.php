<?php

namespace Tiixstone\Card;

/**
 * Pirate's Mark
 */
class TRLA_187 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_187';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}