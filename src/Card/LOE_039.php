<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Gorillabot A-3
 */
class LOE_039 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_039';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}