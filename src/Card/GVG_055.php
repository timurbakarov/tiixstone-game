<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Screwjank Clunker
 */
class GVG_055 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_055';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}