<?php

namespace Tiixstone\Card;

/**
 * Hunter of Old
 */
class GILA_507 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_507';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}