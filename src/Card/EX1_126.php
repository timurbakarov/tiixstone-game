<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\Block\TakeDamage;
use Tiixstone\PlayCardCondition;

/**
 * Betrayal
 */
class EX1_126 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_126';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        /** @var Minion[] $adjacentMinions */
        $adjacentMinions = $game->targetManager->adjacentMinions($game, $target);

        $blocks = [];
        foreach ($adjacentMinions as $minion) {
            if($minion) {
                $blocks[] = new TakeDamage($minion, $target->attack->total());
            }
        }

        return $blocks;
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Enemy,
            new Condition\Target\Minion,
        ]);
    }
}