<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Val'kyr Shadowguard
 */
class ICCA08_017 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA08_017';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}