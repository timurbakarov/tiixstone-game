<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Lady Deathwhisper
 */
class ICCA10_009 extends Hero
{
    public function globalId() : string
    {
        return 'ICCA10_009';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}