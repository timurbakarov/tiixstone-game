<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Michael Skacal
 */
class CRED_67 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_67';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}