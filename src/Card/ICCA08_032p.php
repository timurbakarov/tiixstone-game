<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Harvest of Souls
 */
class ICCA08_032p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'ICCA08_032p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}