<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Sharpen
 */
class TB_BlingBrawl_Hero1p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TB_BlingBrawl_Hero1p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}