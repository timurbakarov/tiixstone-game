<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tortollan Forager
 */
class UNG_078 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_078';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}