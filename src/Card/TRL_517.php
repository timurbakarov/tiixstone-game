<?php

namespace Tiixstone\Card;

/**
 * Arena Fanatic
 */
class TRL_517 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_517';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}