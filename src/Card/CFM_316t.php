<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Rat
 */
class CFM_316t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_316t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}