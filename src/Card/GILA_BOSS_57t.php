<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Shallow Graves
 */
class GILA_BOSS_57t extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_BOSS_57t';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 9;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}