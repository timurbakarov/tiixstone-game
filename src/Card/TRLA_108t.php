<?php

namespace Tiixstone\Card;

/**
 * Jan'alai's Mantle
 */
class TRLA_108t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_108t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}