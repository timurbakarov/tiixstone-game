<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Moira Bronzebeard
 */
class BRMC_87 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMC_87';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}