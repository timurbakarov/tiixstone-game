<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * The Ancient One
 */
class OG_173a extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_173a';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 30;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 30;
    }
}