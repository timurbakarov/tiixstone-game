<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Trogg Hate Spells!
 */
class LOEA05_03 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'LOEA05_03';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}