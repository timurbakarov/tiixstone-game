<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Berserker
 */
class GILA_Darius_01 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_Darius_01';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}