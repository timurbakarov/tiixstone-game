<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Grumbly Runt
 */
class LOE_089t3 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_089t3';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}