<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Blood-Queen Lana'thel
 */
class ICCA05_001 extends Hero
{
    public function globalId() : string
    {
        return 'ICCA05_001';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}