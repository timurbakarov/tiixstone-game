<?php

namespace Tiixstone\Card;

/**
 * Cultist S'thara
 */
class GILA_BOSS_45h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_45h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}