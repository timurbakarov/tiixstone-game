<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Frost Breath
 */
class ICCA04_008p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'ICCA04_008p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}