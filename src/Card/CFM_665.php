<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Worgen Greaser
 */
class CFM_665 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_665';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}