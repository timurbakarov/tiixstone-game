<?php

namespace Tiixstone\Card;

/**
 * Headhunter's Hatchet
 */
class TRL_111 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_111';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}