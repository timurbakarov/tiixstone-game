<?php

namespace Tiixstone\Card;

use Tiixstone\Block\DealSplashDamage;
use Tiixstone\Block\TakeDamage;
use Tiixstone\Effect\Target\Aggregate;
use Tiixstone\Effect\Target\Not;
use Tiixstone\Effect\Target\SelfTarget;
use Tiixstone\Effect\Target\SpecificCharacter;
use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\Manager\TargetManager;
use Tiixstone\PlayCardCondition;

/**
 * Swipe
 */
class CS2_012 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_012';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 4;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new TakeDamage($target, $game->withSpellDamage(4), $this),
            new DealSplashDamage(
                TargetManager::ANY_OPPONENT_CHARACTER,
                $game->withSpellDamage(1),
                $this,
                new Not(new SpecificCharacter($target))
            ),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Character,
            new Condition\Target\Enemy,
        ]);
    }
}