<?php

namespace Tiixstone\Card;

/**
 * Furious Ettin
 */
class GIL_120 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_120';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}