<?php

namespace Tiixstone\Card;

/**
 * Captain Hooktusk
 */
class TRL_126 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_126';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}