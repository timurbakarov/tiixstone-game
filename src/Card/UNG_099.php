<?php

namespace Tiixstone\Card;

use Tiixstone\Effect\RushEffect;

/**
 * Charged Devilsaur
 */
class UNG_099 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_099';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [RushEffect::class];
    }
}