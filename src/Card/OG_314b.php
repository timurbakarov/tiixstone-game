<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\TauntEffect;

/**
 * Slime
 */
class OG_314b extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_314b';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [TauntEffect::class];
    }
}