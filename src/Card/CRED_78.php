<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Matthew Grubb
 */
class CRED_78 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_78';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}