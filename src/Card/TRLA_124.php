<?php

namespace Tiixstone\Card;

/**
 * Zandalari Striker
 */
class TRLA_124 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_124';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}