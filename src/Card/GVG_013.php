<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Cogmaster
 */
class GVG_013 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_013';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}