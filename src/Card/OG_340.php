<?php

namespace Tiixstone\Card;

use Tiixstone\Effect;

/**
 * Soggoth the Slitherer
 */
class OG_340 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_340';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [Effect\TauntEffect::class, Effect\ElusiveEffect::class];
    }
}