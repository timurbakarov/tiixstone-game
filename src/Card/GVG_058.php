<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Shielded Minibot
 */
class GVG_058 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_058';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}