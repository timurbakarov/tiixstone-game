<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Blue Portal
 */
class KARA_08_06 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KARA_08_06';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}