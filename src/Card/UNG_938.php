<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Hot Spring Guardian
 */
class UNG_938 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_938';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}