<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\PlayCardCondition;
use Tiixstone\Block\MarkDestroyed;

/**
 * Assassinate
 */
class CS2_076 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_076';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 5;
    }

    /**
     * @param Game $game
     * @param Character|null|Minion $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $game->logBlock('assasinate-spell');

        return [new MarkDestroyed($target)];
    }

    /**
     * @return int
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Enemy,
            new Condition\Target\Minion,
        ]);
    }
}