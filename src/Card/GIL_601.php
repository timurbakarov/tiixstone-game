<?php

namespace Tiixstone\Card;

/**
 * Scaleworm
 */
class GIL_601 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_601';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}