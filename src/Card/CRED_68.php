<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Peter Whalen
 */
class CRED_68 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_68';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}