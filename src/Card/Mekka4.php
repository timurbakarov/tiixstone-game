<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Poultryizer
 */
class Mekka4 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'Mekka4';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}