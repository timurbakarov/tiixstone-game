<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dark Iron Dwarf
 */
class EX1_046 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_046';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}