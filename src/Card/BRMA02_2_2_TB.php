<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Jeering Crowd
 */
class BRMA02_2_2_TB extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'BRMA02_2_2_TB';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}