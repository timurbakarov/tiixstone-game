<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Nazra Wildaxe
 */
class KARA_13_01H extends Hero
{
    public function globalId() : string
    {
        return 'KARA_13_01H';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}