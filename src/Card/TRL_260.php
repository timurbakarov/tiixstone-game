<?php

namespace Tiixstone\Card;

/**
 * Bwonsamdi, the Dead
 */
class TRL_260 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_260';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}