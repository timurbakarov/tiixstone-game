<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Doppelgangster
 */
class CFM_668t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_668t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}