<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Magni Bronzebeard
 */
class HERO_01a extends Hero
{
    public function globalId() : string
    {
        return 'HERO_01a';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}