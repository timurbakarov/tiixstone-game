<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Zealous Initiate
 */
class OG_158 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_158';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}