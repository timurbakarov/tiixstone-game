<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Icy Touch
 */
class ICC_833h extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'ICC_833h';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}