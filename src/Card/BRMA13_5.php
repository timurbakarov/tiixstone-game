<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Son of the Flame
 */
class BRMA13_5 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA13_5';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}