<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Black King
 */
class KAR_a10_Boss2H extends Hero
{
    public function globalId() : string
    {
        return 'KAR_a10_Boss2H';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}