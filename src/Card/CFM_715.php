<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jade Spirit
 */
class CFM_715 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_715';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}