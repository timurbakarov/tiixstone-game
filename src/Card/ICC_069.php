<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ghastly Conjurer
 */
class ICC_069 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_069';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}