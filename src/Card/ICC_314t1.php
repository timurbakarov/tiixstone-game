<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Frostmourne
 */
class ICC_314t1 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_314t1';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 7;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}