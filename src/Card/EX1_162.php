<?php

namespace Tiixstone\Card;

use Tiixstone\Effect;

/**
 * Dire Wolf Alpha
 */
class EX1_162 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_162';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
	
	/**
     * @return array
     */
    public function boardEffects()
    {
        return [new Effect\AuraAttackChangerEffect(1, new Effect\Target\AdjacentTarget)];
    }
}