<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Ominous Fog
 */
class GILA_BOSS_61t3 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_BOSS_61t3';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}