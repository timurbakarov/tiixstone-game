<?php

namespace Tiixstone\Card;

/**
 * Chillwind Yeti
 */
class CS2_182 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_182';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}