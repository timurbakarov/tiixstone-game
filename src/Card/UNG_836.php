<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Clutchmother Zavas
 */
class UNG_836 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_836';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}