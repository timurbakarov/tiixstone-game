<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Headless Horseman
 */
class TB_HeadlessHorseman_H1 extends Hero
{
    public function globalId() : string
    {
        return 'TB_HeadlessHorseman_H1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}