<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Bryn'troll, the Bone Arbiter
 */
class ICCA06_003 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA06_003';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 6;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 6;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}