<?php

namespace Tiixstone\Card;

use Tiixstone\Effect;

/**
 * Ice Block
 */
class EX1_295 extends MageSecret
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_295';
    }

    /**
     * @return mixed
     */
    public function effect(): Effect\Secret
    {
        return new Effect\Secret\IceBlock();
    }
}