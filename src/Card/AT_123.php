<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Chillmaw
 */
class AT_123 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_123';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}