<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect\AttackChangerEffect;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Humility
 */
class EX1_360 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_360';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new GiveEffect($target, new AttackChangerEffect(1, true))];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
        ]);
    }
}