<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Wilfred Fizzlebang
 */
class AT_027 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_027';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}