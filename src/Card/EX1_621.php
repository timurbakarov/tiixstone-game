<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Manager\TargetManager;

/**
 * Circle of Healing
 */
class EX1_621 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_621';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new Block\Heal\MassHeal(TargetManager::ANY_MINION, $game->withSpellDamage(4), $this)];
    }
}