<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Silver Hand Murloc
 */
class OG_006a extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_006a';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}