<?php

namespace Tiixstone\Card;

/**
 * Gilnean Vigilante
 */
class GILA_803 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_803';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}