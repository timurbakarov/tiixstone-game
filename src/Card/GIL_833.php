<?php

namespace Tiixstone\Card;

/**
 * Forest Guide
 */
class GIL_833 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_833';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}