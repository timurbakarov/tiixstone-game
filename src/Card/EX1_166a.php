<?php

namespace Tiixstone\Card;

use Tiixstone\Block\TakeDamage;
use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;
use Tiixstone\PlayCardCondition;

/**
 * Moonfire
 */
class EX1_166a extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_166a';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new TakeDamage($target, $game->withSpellDamage(2))];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Character(),
        ]);
    }
}