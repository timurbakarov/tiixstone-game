<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * Ice Barrier
 */
class EX1_289 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_289';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}