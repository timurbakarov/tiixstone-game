<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Block\SummonMinion;

/**
 * Animal Companion
 */
class NEW1_031 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_031';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null): array
    {
        $minion = $this->randomMinion($game);

        $game->logBlock('animal-companion', ['minion' => $minion]);

        return [new SummonMinion($game->currentPlayer(), $minion)];
    }

    /**
     * @return mixed
     */
    private function randomMinion(Game $game)
    {
        $randomMinion = $game->RNG->randomFromArray($this->minions());

        return new $randomMinion;
    }

    /**
     * @return array
     */
    private function minions()
    {
        return [
            Card\Alias\Misha::className(),
            Card\Alias\Huffer::className(),
            Card\Alias\Leokk::className(),
        ];
    }
}