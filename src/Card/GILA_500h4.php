<?php

namespace Tiixstone\Card;

/**
 * Tess Greymane
 */
class GILA_500h4 extends Hero
{
    public function globalId() : string
    {
        return 'GILA_500h4';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}