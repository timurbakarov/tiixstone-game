<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Gruul
 */
class NEW1_038 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_038';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}