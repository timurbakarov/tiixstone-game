<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dragonkin
 */
class BRMA09_4Ht extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA09_4Ht';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}