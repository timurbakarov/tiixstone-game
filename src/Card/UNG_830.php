<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Cruel Dinomancer
 */
class UNG_830 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_830';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}