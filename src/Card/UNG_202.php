<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Fire Plume Harbinger
 */
class UNG_202 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_202';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}