<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\Block\TakeDamage;
use Tiixstone\PlayCardCondition;

/**
 * Backstab
 */
class CS2_072 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_072';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        $game->logBlock('backstab-spell');

        return [
            new TakeDamage($target, $game->withSpellDamage(2)),
        ];
    }

    /**
     * @return int
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Minion,
            new Condition\Target\FullHealth,
        ]);
    }
}