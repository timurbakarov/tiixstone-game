<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect\SavannahHighmaneDeathrattle;

/**
 * Savannah Highmane
 */
class EX1_534 extends Minion
{
    protected $race = Race::BEAST;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_534';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [SavannahHighmaneDeathrattle::class];
    }
}