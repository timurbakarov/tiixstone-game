<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Living Lava
 */
class BRMA13_6 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA13_6';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}