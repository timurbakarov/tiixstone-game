<?php

namespace Tiixstone\Card;

/**
 * Zentimo
 */
class TRLA_201h extends Hero
{
    public function globalId() : string
    {
        return 'TRLA_201h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}