<?php

namespace Tiixstone\Card;

/**
 * Grum
 */
class GILA_BOSS_58h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_58h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}