<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Curious Glimmerroot
 */
class UNG_035 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_035';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}