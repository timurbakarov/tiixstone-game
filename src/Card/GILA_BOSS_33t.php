<?php

namespace Tiixstone\Card;

/**
 * Scythe
 */
class GILA_BOSS_33t extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_BOSS_33t';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}