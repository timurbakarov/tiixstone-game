<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Totemic Slam
 */
class CS2_049_H1_AT_132 extends CS2_049
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'CS2_049_H1_AT_132';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}