<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Angry Mob
 */
class GILA_821b extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_821b';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}