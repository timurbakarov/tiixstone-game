<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Be Our Guest
 */
class KAR_A02_13H extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'KAR_A02_13H';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}