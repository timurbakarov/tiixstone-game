<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Master of Ceremonies
 */
class AT_117 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_117';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}