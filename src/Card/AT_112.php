<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Master Jouster
 */
class AT_112 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_112';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}