<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Chromaggus
 */
class BRM_031 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRM_031';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}