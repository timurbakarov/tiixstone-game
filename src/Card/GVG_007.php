<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Flame Leviathan
 */
class GVG_007 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_007';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}