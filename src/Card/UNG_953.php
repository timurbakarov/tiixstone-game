<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Primalfin Champion
 */
class UNG_953 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_953';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}