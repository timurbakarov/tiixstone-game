<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Twilight Geomancer
 */
class OG_284 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_284';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}