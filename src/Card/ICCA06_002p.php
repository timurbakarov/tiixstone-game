<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Skeletal Reconstruction
 */
class ICCA06_002p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'ICCA06_002p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}