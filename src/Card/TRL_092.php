<?php

namespace Tiixstone\Card;

/**
 * Spirit of the Shark
 */
class TRL_092 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_092';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}