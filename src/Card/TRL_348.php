<?php

namespace Tiixstone\Card;

/**
 * Springpaw
 */
class TRL_348 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_348';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}