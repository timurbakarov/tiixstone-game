<?php

namespace Tiixstone\Card;

/**
 * Archmage Arugal
 */
class GIL_691 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_691';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}