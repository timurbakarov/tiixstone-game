<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect\TauntEffect;

/**
 * Massive
 */
class UNG_999t6 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_999t6';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [new GiveEffect($target, TauntEffect::class)];
    }
}