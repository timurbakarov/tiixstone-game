<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Dog Whistle
 */
class GILA_400p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_400p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}