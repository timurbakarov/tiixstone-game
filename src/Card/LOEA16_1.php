<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Rafaam
 */
class LOEA16_1 extends Hero
{
    public function globalId() : string
    {
        return 'LOEA16_1';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}