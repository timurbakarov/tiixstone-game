<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Cat in a Hat
 */
class KAR_004a extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_004a';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}