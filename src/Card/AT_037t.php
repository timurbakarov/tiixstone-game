<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Sapling
 */
class AT_037t extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_037t';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}