<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Sense Weakness
 */
class TB_HeadlessHorseman_HP5 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TB_HeadlessHorseman_HP5';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}