<?php

namespace Tiixstone\Card;

/**
 * Grizzled Reinforcement
 */
class GILA_610 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_610';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}