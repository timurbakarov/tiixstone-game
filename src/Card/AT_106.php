<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Light's Champion
 */
class AT_106 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_106';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}