<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Ravenous Pterrordax
 */
class UNG_047 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_047';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}