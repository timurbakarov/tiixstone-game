<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Trade Prince Gallywix
 */
class GVG_028 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_028';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}