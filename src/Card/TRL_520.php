<?php

namespace Tiixstone\Card;

/**
 * Murloc Tastyfin
 */
class TRL_520 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_520';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}