<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Freezing Blast
 */
class FB_LK007p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'FB_LK007p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}