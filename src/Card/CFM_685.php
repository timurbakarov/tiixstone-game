<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Don Han'Cho
 */
class CFM_685 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_685';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}