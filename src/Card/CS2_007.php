<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\PlayCardCondition;
use Tiixstone\Block\HealCharacter;
use Tiixstone\Manager\TargetManager;

/**
 * Healing Touch
 */
class CS2_007 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_007';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new HealCharacter($target, 8),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Condition\Target\Character,
        ]);
    }
}