<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Stone Sentinel
 */
class UNG_208 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_208';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}