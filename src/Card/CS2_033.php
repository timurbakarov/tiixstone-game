<?php

namespace Tiixstone\Card;

use Tiixstone\Race;
use Tiixstone\Effect\WaterElementEffect;

/**
 * Water Elemental
 */
class CS2_033 extends Minion
{
    protected $race = Race::ELEMENTAL;

    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_033';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
    /**
     * @return array
     */
    public function boardEffects()
    {
        return [WaterElementEffect::class];
    }
}