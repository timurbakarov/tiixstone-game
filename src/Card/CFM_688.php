<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Spiked Hogrider
 */
class CFM_688 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_688';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}