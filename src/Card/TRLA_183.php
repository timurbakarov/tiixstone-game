<?php

namespace Tiixstone\Card;

/**
 * Warp Stalker
 */
class TRLA_183 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_183';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}