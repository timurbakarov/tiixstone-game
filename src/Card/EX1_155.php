<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\CastSpell;
use Tiixstone\Block\ChooseOne;
use Tiixstone\PlayCardCondition;

/**
 * Mark of Nature
 */
class EX1_155 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_155';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 3;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new ChooseOne(
                function(Spell $spell) use($game, $target) {
                    return new CastSpell($spell, $target);
                },
                ...$this->chooseCards()
            ),
        ];
    }

    /**
     * @return array
     */
    private function chooseCards()
    {
        return [
            new EX1_155a(),
            new EX1_155b(),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion(),
        ]);
    }
}