<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Fearsome Doomguard
 */
class AT_020 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_020';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}