<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Fire Plume Phoenix
 */
class UNG_084 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_084';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}