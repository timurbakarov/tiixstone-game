<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Humongous Razorleaf
 */
class UNG_844 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_844';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}