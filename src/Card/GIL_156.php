<?php

namespace Tiixstone\Card;

/**
 * Quartz Elemental
 */
class GIL_156 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_156';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}