<?php

namespace Tiixstone\Card;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Block\TakeDamage;
use Tiixstone\Effect\AttackChangerEffect;
use Tiixstone\Game;
use Tiixstone\Battlecryable;
use Tiixstone\PlayCardCondition;

/**
 * Cruel Taskmaster
 */
class EX1_603 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_603';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        if(!$target) {
            return [];
        }

        return [
            new TakeDamage($target, 1, $this),
            new GiveEffect($target, new AttackChangerEffect(2)),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Minion,
        ]);
    }
}