<?php

namespace Tiixstone\Card;

/**
 * Herald of Flame
 */
class TRLA_176 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_176';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}