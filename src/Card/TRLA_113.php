<?php

namespace Tiixstone\Card;

/**
 * Blood Pact
 */
class TRLA_113 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_113';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}