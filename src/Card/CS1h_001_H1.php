<?php

namespace Tiixstone\Card;

use Tiixstone\Block\HealCharacter;
use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;
use Tiixstone\Manager\TargetManager;

/**
 * Lesser Heal Skin
 */
class CS1h_001_H1 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'CS1h_001_H1';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [new HealCharacter($target, $this->defaultHealAmount())];
    }

    /**
     * @return int
     */
    public function target()
    {
        return TargetManager::ANY_CHARACTER;
    }

    /**
     * @return int
     */
    private function defaultHealAmount()
    {
        return 2;
    }
}