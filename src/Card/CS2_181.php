<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Injured Blademaster
 */
class CS2_181 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_181';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}