<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Blood of The Ancient One
 */
class OG_173 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_173';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 9;
    }
}