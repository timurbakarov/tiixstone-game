<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Surly Mob
 */
class GILA_821a extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_821a';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}