<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Onyx Bishop
 */
class KAR_204 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_204';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}