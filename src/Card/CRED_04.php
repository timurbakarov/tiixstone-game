<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Steven Gabriel
 */
class CRED_04 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_04';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}