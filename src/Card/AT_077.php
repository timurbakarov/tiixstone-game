<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Weapon;

/**
 * Argent Lance
 */
class AT_077 extends Weapon
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_077';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultDurability(): int
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}