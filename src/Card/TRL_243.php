<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Pounce
 */
class TRL_243 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_243';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}