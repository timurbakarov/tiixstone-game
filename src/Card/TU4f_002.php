<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Pandaren Scout
 */
class TU4f_002 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TU4f_002';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}