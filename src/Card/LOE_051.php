<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Jungle Moonkin
 */
class LOE_051 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'LOE_051';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}