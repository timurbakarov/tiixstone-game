<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Skulking Geist
 */
class ICC_701 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICC_701';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}