<?php

namespace Tiixstone\Card;

/**
 * Bellringer Sentry
 */
class GIL_634 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_634';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}