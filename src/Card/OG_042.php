<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Y'Shaarj, Rage Unbound
 */
class OG_042 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_042';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 10;
    }
}