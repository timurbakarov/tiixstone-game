<?php

namespace Tiixstone\Card;

/**
 * Frostweaver
 */
class TRLA_132 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_132';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}