<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Emerald Drake
 */
class DREAM_03 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'DREAM_03';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 7;
    }
}