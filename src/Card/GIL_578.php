<?php

namespace Tiixstone\Card;

/**
 * Countess Ashmore
 */
class GIL_578 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_578';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 6;
    }
}