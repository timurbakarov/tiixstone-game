<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Arcane Anomaly
 */
class KAR_036 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'KAR_036';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}