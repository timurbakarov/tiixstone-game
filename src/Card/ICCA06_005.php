<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Bone Spike
 */
class ICCA06_005 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'ICCA06_005';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}