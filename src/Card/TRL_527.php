<?php

namespace Tiixstone\Card;

/**
 * Drakkari Trickster
 */
class TRL_527 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_527';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}