<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Aberration
 */
class BRMA15_4 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'BRMA15_4';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}