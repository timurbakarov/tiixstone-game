<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Patient Assassin
 */
class EX1_522 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_522';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}