<?php

namespace Tiixstone\Card;

/**
 * Darius Crowley
 */
class GIL_547 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_547';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}