<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Blood Rune
 */
class ICCA09_001p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'ICCA09_001p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}