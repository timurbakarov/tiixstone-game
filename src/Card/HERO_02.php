<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;
use Tiixstone\Card\Alias\TotemicCall;

/**
 * Thrall
 */
class HERO_02 extends Hero
{
    public function globalId() : string
    {
        return 'HERO_02';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return TotemicCall::create();
    }
}