<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Street Trickster
 */
class CFM_039 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_039';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}