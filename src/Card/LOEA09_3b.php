<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Getting Hungry
 */
class LOEA09_3b extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'LOEA09_3b';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}