<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * N'Zoth, the Corruptor
 */
class OG_133 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_133';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 7;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}