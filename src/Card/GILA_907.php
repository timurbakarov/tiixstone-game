<?php

namespace Tiixstone\Card;

/**
 * Clockwork Assistant
 */
class GILA_907 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_907';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}