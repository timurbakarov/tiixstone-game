<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Molten Rage
 */
class TBA01_6 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TBA01_6';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}