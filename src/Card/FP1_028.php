<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Undertaker
 */
class FP1_028 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_028';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 1;
    }
}