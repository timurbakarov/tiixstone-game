<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Archaedas
 */
class LOEA08_01h extends Hero
{
    public function globalId() : string
    {
        return 'LOEA08_01h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}