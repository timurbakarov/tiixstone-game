<?php

namespace Tiixstone\Card;

use Tiixstone\Block\Discard\DiscardOne;
use Tiixstone\Block\MarkDestroyed;
use Tiixstone\Game;
use Tiixstone\Battlecryable;

/**
 * Deathwing
 */
class NEW1_030 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_030';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 12;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 12;
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        $minions = $game->battlefield->minions();

        $blocks = [];

        foreach($minions as $minion) {
            if($minion->id() == $attackable->id()) {
                continue;
            }

            $blocks[] = new MarkDestroyed($minion);
        }

        $handCards = $attackable->getPlayer()->hand->all();

        foreach($handCards as $card) {
            $blocks[] = new DiscardOne($card);
        }

        return $blocks;
    }
}