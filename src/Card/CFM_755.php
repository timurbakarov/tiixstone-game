<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Grimestreet Pawnbroker
 */
class CFM_755 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_755';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}