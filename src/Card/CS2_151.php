<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Silver Hand Knight
 */
class CS2_151 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CS2_151';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}