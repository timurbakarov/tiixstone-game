<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Blingtron 3000
 */
class GVG_119 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GVG_119';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}