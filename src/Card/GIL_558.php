<?php

namespace Tiixstone\Card;

/**
 * Swamp Leech
 */
class GIL_558 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GIL_558';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}