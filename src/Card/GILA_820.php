<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Ricochet Shot
 */
class GILA_820 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'GILA_820';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 5;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}