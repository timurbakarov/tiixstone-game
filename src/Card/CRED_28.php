<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * He-Rim Woo
 */
class CRED_28 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_28';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}