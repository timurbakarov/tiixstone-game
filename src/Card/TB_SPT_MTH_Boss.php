<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

/**
 * Decorated Stormwind
 */
class TB_SPT_MTH_Boss extends Hero
{
    public function globalId() : string
    {
        return 'TB_SPT_MTH_Boss';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}