<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Bloodhoof Brave
 */
class OG_218 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'OG_218';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}