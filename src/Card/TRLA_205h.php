<?php

namespace Tiixstone\Card;

/**
 * Wardruid Loti
 */
class TRLA_205h extends Hero
{
    public function globalId() : string
    {
        return 'TRLA_205h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}