<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Necromancy
 */
class TB_KTRAF_HP_KT_3 extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'TB_KTRAF_HP_KT_3';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}