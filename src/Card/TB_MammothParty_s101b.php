<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * Like a Sore Thumb
 */
class TB_MammothParty_s101b extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_MammothParty_s101b';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 0;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}