<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Dalaran Aspirant
 */
class AT_006 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_006';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 4;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}