<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Nether Portal
 */
class UNG_829t2 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_829t2';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 11;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 0;
    }
}