<?php

namespace Tiixstone\Card;

/**
 * Lorewalker Cho
 */
class TU4f_001 extends Hero
{
    public function globalId() : string
    {
        return 'TU4f_001';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}