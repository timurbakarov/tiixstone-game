<?php

namespace Tiixstone\Card;

use Tiixstone\Game;

/**
 * Survival of the Fittest
 */
class GILA_BOSS_41p extends Power
{
    /**
     * @return string
     */
    public function globalId(): string
    {
        return 'GILA_BOSS_41p';
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return mixed
     */
    public function use (Game $game, Character $target = null)
    {
        return [];
    }
}