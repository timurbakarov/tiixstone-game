<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Mukla's Champion
 */
class AT_090 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'AT_090';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }
}