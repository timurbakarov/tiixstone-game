<?php

namespace Tiixstone\Card;

/**
 * Banana Buffoon
 */
class TRL_509 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRL_509';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }
}