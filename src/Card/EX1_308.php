<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Block\TakeDamage;
use Tiixstone\PlayCardCondition;
use Tiixstone\Block\Discard\DiscardRandom;

/**
 * Soulfire
 */
class EX1_308 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_308';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 1;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [
            new TakeDamage($target, $game->withSpellDamage(4), $this),
            new DiscardRandom($this->getPlayer()),
        ];
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new \Tiixstone\Condition\Target\Character,
        ]);
    }
}