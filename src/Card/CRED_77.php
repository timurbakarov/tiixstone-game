<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Lorenzo Minaca
 */
class CRED_77 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CRED_77';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}