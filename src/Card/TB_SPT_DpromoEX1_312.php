<?php

namespace Tiixstone\Card;

use Tiixstone\Game;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

/**
 * Twisting Nether?
 */
class TB_SPT_DpromoEX1_312 extends Spell
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TB_SPT_DpromoEX1_312';
    }

    /**
     * @return int
     */
    public function defaultCost() : int
    {
        return 8;
    }

    /**
     * @param Game $game
     * @param Character|null $target
     * @return array
     */
    public function cast(Game $game, Character $target = null) : array
    {
        return [];
    }
}