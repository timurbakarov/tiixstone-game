<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Thunder Lizard
 */
class UNG_082 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_082';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}