<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Luckydo Buccaneer
 */
class CFM_342 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'CFM_342';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 5;
    }
}