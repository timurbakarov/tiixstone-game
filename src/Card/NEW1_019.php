<?php

namespace Tiixstone\Card;

use Tiixstone\Effect;

/**
 * Knife Juggler
 */
class NEW1_019 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'NEW1_019';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 2;
    }

    /**
     * @return array
     */
	public function boardEffects() : array
	{
		return [Effect\KnifeJugglerEffect::class];
	}
}