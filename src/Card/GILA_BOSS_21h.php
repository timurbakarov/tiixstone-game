<?php

namespace Tiixstone\Card;

/**
 * Rottooth
 */
class GILA_BOSS_21h extends Hero
{
    public function globalId() : string
    {
        return 'GILA_BOSS_21h';
    }

    /**
     * @return Power
     */
    public function defaultPower() : Power
    {
        return new DS1h_292;
    }
}