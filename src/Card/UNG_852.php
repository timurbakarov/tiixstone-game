<?php

namespace Tiixstone\Card;

use Tiixstone\Card\Minion;

/**
 * Tyrantus
 */
class UNG_852 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'UNG_852';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 12;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 12;
    }
}