<?php

namespace Tiixstone\Card;

/**
 * Parading Marshal
 */
class TRLA_139 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'TRLA_139';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }
}