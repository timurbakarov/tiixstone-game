<?php

namespace Tiixstone\Card;
use Tiixstone\Effect\Deathrattle\SludgeBelcherDeathrattle;
use Tiixstone\Effect\TauntEffect;

/**
 * Sludge Belcher
 */
class FP1_012 extends Minion
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'FP1_012';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 3;
    }

    /**
     * @return array
     */
    public function boardEffects()
    {
        return [TauntEffect::class, SludgeBelcherDeathrattle::class];
    }
}