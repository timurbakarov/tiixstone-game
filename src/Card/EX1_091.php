<?php

namespace Tiixstone\Card;
use Tiixstone\Battlecryable;
use Tiixstone\Block\MindControl;
use Tiixstone\Condition\Target\AttackLowerThan;
use Tiixstone\Condition\Target\Enemy;
use Tiixstone\Game;
use Tiixstone\PlayCardCondition;

/**
 * Cabal Shadow Priest
 */
class EX1_091 extends Minion implements Battlecryable
{
    /**
     * @return string
     */
    public function globalId() : string
    {
        return 'EX1_091';
    }

    /**
     * @return int
     */
    public function defaultCost(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function defaultMaximumHealth(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function defaultAttackRate(): int
    {
        return 4;
    }

    /**
     * @return PlayCardCondition
     * @throws \Tiixstone\Exception
     */
    public function playCondition() : PlayCardCondition
    {
        return new PlayCardCondition([
            new Enemy(),
            new \Tiixstone\Condition\Target\Minion,
            new AttackLowerThan(3),
        ]);
    }

    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = NULL, Character $target = NULL)
    {
        if(!$target) {
            return [];
        }

        return [new MindControl($target)];
    }
}