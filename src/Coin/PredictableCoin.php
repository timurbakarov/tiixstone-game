<?php

namespace Tiixstone\Coin;

use Tiixstone\Player;

class PredictableCoin extends \Tiixstone\Coin
{
    /**
     * @return $this
     */
    public function toss(Player $player1, Player $player2)
    {
        $this->winner = $player1;
        $this->loser = $player2;

        return $this;
    }
}