<?php

namespace Tiixstone;

class SpellDamageTracker
{
    /**
     * @var
     */
    private $data;

    /**
     * @param Player $player
     * @param int $amount
     * @return SpellDamageTracker
     */
    public function set(Player $player, int $amount) : self
    {
        $this->data[$player->id()] = $amount;

        return $this;
    }

    /**
     * @param Player $player
     * @return int
     */
    public function get(Player $player) : int
    {
        return $this->data[$player->id()] ?? 0;
    }
}