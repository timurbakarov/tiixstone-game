<?php

namespace Tiixstone;

use Tiixstone\Card\Alias\Thrall;
use Tiixstone\Card\Alias\JainaProudmoore;
use Tiixstone\Card\Collection\Board;
use Tiixstone\Card\Collection\Deck;
use Tiixstone\Card\Collection\Hand;
use Tiixstone\Coin\PredictableCoin;
use Tiixstone\Manager\TargetManager;

class Factory
{
    public static function createForTest(
        $player1Deck = [],
        $player2Deck = [],
        $player1Hand = [],
        $player2Hand = [],
        $player1Board = [],
        $player2Board = [],
        $hero1 = null,
        $hero2 = null,
        array $globalEffects = []
    )
    {
        if(!$hero1) {
            $hero1 = JainaProudmoore::className();
        }

        if(!$hero2) {
            $hero2 = Thrall::className();
        }

        $player1Hero = new $hero1;
        $player2Hero = new $hero2;

        $player1Deck = new Deck($player1Deck);
        $player1Hand = new Hand($player1Hand);
        $player1Board = new Board($player1Board);

        $player2Deck = new Deck($player2Deck);
        $player2Hand = new Hand($player2Hand);
        $player2Board = new Board($player2Board);

        $player1 = new \Tiixstone\Player('Jonh Doe', $player1Hero, $player1Deck, $player1Hand, $player1Board);
        $player2 = new \Tiixstone\Player('Agent Smith', $player2Hero, $player2Deck, $player2Hand, $player2Board);

        $targetManager = new TargetManager();

        return new \Tiixstone\Game(
            $player1,
            $player2,
            $targetManager,
            new PredictableCoin(),
            $globalEffects
        );
    }

    /**
     * @return Game
     */
    public static function createForTestWithDecks()
    {
        return Factory::createForTest(self::createCardsArray(30), self::createCardsArray(30));
    }

    public static function createCardsArray(int $amount, $card = null)
    {
        if(!$card) {
            $card = \Tiixstone\Card\Alias\Sheep::className();
        }

        $cards = [];

        for($i=0; $i<$amount; $i++) {
            $cards[] = new $card;
        }

        return $cards;
    }
}