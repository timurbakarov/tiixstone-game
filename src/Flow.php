<?php

namespace Tiixstone;

abstract class Flow implements Subscriber
{
    use Identifier;

    public function __construct()
    {
        $this->id = Game::uuid();
    }
}