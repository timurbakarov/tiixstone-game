<?php

namespace Tiixstone;

use Tiixstone\Card\Character;
use Tiixstone\Condition;

class PlayCardCondition
{
    const DEFAULT_ERROR_MESSAGE = 'Play conditions does not fullfilled';

    /**
     * @var Condition[]
     */
    private $conditions;

    public function  __construct(array $conditions)
    {
        foreach($conditions as $condition) {
            if(!$condition instanceof Condition) {
                throw new Exception("TargetCondition should be instance of Tiixstone\Condition");
            }
        }

        $this->conditions = $conditions;
    }

    /**
     * @param Game $game
     * @param Card $initiator
     * @param Character|NULL $target
     * @return Condition\Result
     */
    public function check(Game $game, Card $initiator, Character $target = null) : Condition\Result
    {
        /**
         * Если battlecry то не показываем ошибку если нет валидной цели
         *
         * Если выбранная цель не валидна по какому-то критерию(condition), то мы получаем
         * все цели соответствующие этому критерию. И затем проверяем, что среди них всех
         * есть общая валидная цель. Если есть - выдаем ошибку, если нет - значит на поле
         * нет валидной цели и можно разыграть существо без бэттлкрая
         *
         * Возможные варианты:
         *
         * 1. Выбрана валидная цель - не будет $results
         * 2. Выбрана невалидная цель - возвращаем ошибку сразу
         * 3. Не выбрана цель - будут $results
         *  3.1 На поле нет валидной цели в принципе
         *  3.2 Не выбрана валидная цель
         */
        if($initiator->isMinion()) {
            $results = [];
            $targetConditionsCount = 0;
            $conditionTargets = [];

            foreach($this->conditions as $condition) {
                if($condition instanceof Condition\Target) {
                    $targetConditionsCount++;

                    if(!$condition->isSatisfiedBy($game, $initiator, $target)) {
                        if(!is_null($target)) {
                            return new Condition\Result(true, $condition);
                        }

                        $results[] = new Condition\Result(true, $condition);
                        $conditionTargets[] = $condition->targets($game, $initiator);
                    }
                } else {
                    if(!$condition->isSatisfiedBy($game, $initiator, $target)) {
                        return new Condition\Result(true, $condition);
                    }
                }
            }

            // если у бэттлкрая была должны быть выбрана цель,
            // но она не была выбрана игроком, то проверяем,
            // есть ли в принципе валидные цели
            if($results) {
                $targetsIds = [];

                foreach($conditionTargets as $key => $conditionTarget) {
                    /** @var Character $target */
                    foreach ($conditionTarget as $target) {
                        $targetsIds[$key][] = $target->id();
                    }
                }

                //
                if(count($results) == count($targetsIds)) {
                    if(count($targetsIds) > 1) {
                        $validTarget = call_user_func_array('array_intersect', $targetsIds);

                        // если есть общие валидные цели, то возвращаем ошибку
                        if($validTarget) {
                            return $results[0];
                        }
                    } elseif(count($targetsIds) == 1) {
                        if($targetsIds[0]) {
                            return $results[0];
                        }
                    }
                }
            }
        } else {
            foreach($this->conditions as $condition) {
                if(!$condition->isSatisfiedBy($game, $initiator, $target)) {
                    return new Condition\Result(true, $condition);
                }
            }
        }

        return new Condition\Result(false);
    }

    /**
     * @param Game $game
     * @param Card\Minion $card
     * @param Condition\Target $condition
     * @return bool
     */
    private function hasValidTargetForBattlecry(Game $game, Card\Minion $card, Condition $condition)
    {
        return count($condition->targets($game, $card)) > 0;
    }

    /**
     * @return array|Condition[]
     */
    public function conditions()
    {
        return $this->conditions;
    }
}