<?php

namespace Tiixstone;

class RNG
{
    /**
     * @param int $min
     * @param int $max
     * @return int
     * @throws \Exception
     */
    public function int(int $min, int $max)
    {
        return random_int($min, $max);
    }

    /**
     * @param array $data
     * @param int $count
     * @return mixed
     * @throws \Exception
     */
    public function randomFromArray(array $data, $count = 1)
    {
        $result = [];

        for($i=0; $i<$count; $i++) {
            $data = $this->shuffleArray($data);
            $rand = $this->int(0, count($data) - 1);
            $result[] =$data[$rand];
            unset($data[$rand]);
        }

        return $count > 1 ? $result : $result[0];
    }

    /**
     * @param array $data
     * @return array
     */
    public function shuffleArray(array $data)
    {
        shuffle($data);

        return $data;
    }
}