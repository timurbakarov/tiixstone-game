<?php

namespace Tiixstone;

trait Identifier
{
    /**
     * @var
     */
    protected $id;

    /**
     * @return string
     */
    public function id() : string
    {
        return $this->id;
    }

    /**
     * @return $this
     */
    public function assignId()
    {
        $this->id = Game::uuid();

        return $this;
    }
}