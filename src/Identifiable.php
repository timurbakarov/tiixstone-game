<?php

namespace Tiixstone;

interface Identifiable
{
    /**
     * @return string
     */
    public function id() : string;
}