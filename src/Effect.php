<?php

namespace Tiixstone;

abstract class Effect implements Identifiable
{
    use Identifier;

    /**
     * @var Card
     */
    protected $owner;

    /**
     * @param Game $game
     * @param Card $card
     * @return array
     */
    public function init(Game $game, Card $card) : array
    {
        return [];
    }

    /**
     * @param Game $game
     * @return array
     */
    public function onRemove(Game $game) : array
    {
        return [];
    }

    /**
     * @param Identifiable $card
     * @return $this
     */
    public function setOwner(Identifiable $card)
    {
        $this->owner = $card;

        return $this;
    }

    /**
     * @return Card|Identifiable
     */
    public function owner()
    {
        return $this->owner;
    }

    /**
     * @return array
     */
    public function events()
    {
        return [];
    }
	
	/**
     * @return bool
     */
	public function isAura()
	{
		return false;
	}
}