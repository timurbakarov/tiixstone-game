<?php

namespace Tiixstone\Manager;

use Tiixstone\Card\Destroyable;
use Tiixstone\Game;
use Tiixstone\Player;
use Tiixstone\Exception;
use Tiixstone\Card\Minion;
use Tiixstone\Card\Character;

class TargetManager
{
    const ANY_CHARACTER = 1;
    const ANY_HERO = 2;
    const ANY_MINION = 3;
    const ANY_OPPONENT_MINION = 4;
    const ANY_FRIENDLY_MINION = 5;
    const ANY_OPPONENT_CHARACTER = 6;
    const ANY_FRIENDLY_CHARACTER = 7;

    /**
     * @param Player $player
     * @return Minion[]
     */
    public function allPlayerMinions(Game $game, Player $player) : array
    {
        return $game->battlefield->minionsByPlayer($player);

        return $player->board->all();

    }

    /**
     * @param Game $game
     * @param Player $player
     * @return Minion[]
     */
    public function allEnemyMinions(Game $game, Player $player) : array
    {
        return $this->allPlayerMinions($game, $player->getOpponent($game));
    }

    /**
     * @param Game $game
     * @param Player $player
     * @return Character[]
     */
    public function allEnemyCharacters(Game $game, Player $player) : array
    {
        return $this->allPlayerCharacters($game, $player->getOpponent($game));
    }

    /**
     * @param Game $game
     * @param Player $player
     * @return Character[]
     */
    public function allFriendlyCharacters(Game $game, Player $player) : array
    {
        return $this->allPlayerCharacters($game, $player);
    }

    /**
     * @param Game $game
     * @param Player $player
     * @return Character[]
     */
    public function allPlayerCharacters(Game $game, Player $player) : array
    {
        return array_merge($this->allPlayerMinions($game, $player), [$player->hero]);
    }

    /**
     * @param Game $game
     * @return Minion[]
     */
    public function allMinions(Game $game)
    {
        return $game->battlefield->minions();
    }

    /**
     * @param Game $game
     * @return Character[]
     */
    public function allCharacters(Game $game) : array
    {
        return array_merge(
            $this->allHeroes($game),
            $this->allMinions($game)
        );
    }

    /**
     * @param Game $game
     * @return Character[]
     */
    public function allHeroes(Game $game) : array
    {
        return [$game->currentPlayer()->hero, $game->idlePlayer()->hero];
    }

    /**
     * @param Game $game
     * @param string $targetId
     * @param int $targetsType
     * @return Character
     * @throws Exception
     */
    public function getTarget(Game $game, string $targetId, int $targetType)
    {
        foreach($this->targets($game, $targetType) as $target) {
            if($target->id() == $targetId) {
                return $target;
            }
        }

        throw new Exception("Invalid target");
    }

    /**
     * @param Game $game
     * @param Minion $minion
     * @return array
     */
    public function adjacentMinions(Game $game, Minion $minion)
    {
        $player = $minion->getPlayer();

        $leftMinion = $player->board->prev($minion->id());
        $rightMinion = $player->board->next($minion->id());

        return [$leftMinion, $rightMinion];
    }

    /**
     * @param Game $game
     * @return Destroyable[]
     */
    public function destroyable(Game $game)
    {
        $items =  array_merge(
            $this->allMinions($game),
            [
                $game->currentPlayer()->hero,
                $game->idlePlayer()->hero,
            ]
        );

        if($game->currentPlayer()->hero->hasWeapon()) {
            $items[] = $game->currentPlayer()->hero->weapon;
        }

        if($game->idlePlayer()->hero->hasWeapon()) {
            $items[] = $game->idlePlayer()->hero->weapon;
        }

        return $items;
    }

    /**
     * @param Game $game
     * @param int $targetType
     * @param Player|NULL $player
     * @return array|Character[]|Minion[]|\Tiixstone\Card\Power
     * @throws Exception
     */
    public function targets(Game $game, int $targetType, Player $player = null)
    {
        switch ($targetType)
        {
            case self::ANY_CHARACTER:
                return $this->allCharacters($game);

            case self::ANY_MINION:
                return $this->allMinions($game);

            case self::ANY_OPPONENT_MINION:
                return $this->allEnemyMinions($game, $player);

            case self::ANY_FRIENDLY_MINION:
                return $this->allPlayerMinions($game, $player);

            case self::ANY_HERO:
                return $this->allHeroes($game);

            case self::ANY_OPPONENT_CHARACTER:
                return $this->allEnemyCharacters($game, $player);

            case self::ANY_FRIENDLY_CHARACTER:
                return $this->allFriendlyCharacters($game, $player);

            default:
                throw new Exception("Invalid target type " . $targetType);
        }
    }
}