<?php

namespace Tiixstone;

use Tiixstone\Card\Character;

class AttackCondition
{
    /**
     * @var Condition[]
     */
    private $conditions;

    /**
     * @param array $conditions
     */
    public function  __construct(array $conditions)
    {
        foreach($conditions as $condition) {
            if(!$condition instanceof Condition) {
                throw new Exception("TargetCondition should be instance of Tiixstone\Condition");
            }
        }

        $this->conditions = $conditions;
    }

    /**
     * @return array|Condition[]
     */
    public function conditions()
    {
        return $this->conditions;
    }

    /**
     * @param Game $game
     * @param Card $initiator
     * @param Character|NULL $target
     * @return Condition\Result
     */
    public function check(Game $game, Card $initiator, Character $target = null) : Condition\Result
    {
        foreach($this->conditions as $condition) {
            if(!$condition->isSatisfiedBy($game, $initiator, $target)) {
                return new Condition\Result(true, $condition);
            }
        }

        return new Condition\Result(false);
    }
}