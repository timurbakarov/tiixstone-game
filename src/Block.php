<?php declare(strict_types=1);

namespace Tiixstone;

abstract class Block
{
    abstract public function run(Game $game) : array;
}