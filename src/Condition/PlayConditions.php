<?php

namespace Tiixstone\Condition;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Condition;

class PlayConditions extends Condition
{
    /**
     * @var
     */
    private $errorMessage;

    /**
     * @param Game $game
     * @param \Tiixstone\Card $initiator
     * @param Card\Character|NULL $target
     * @return bool
     */
    public function isSatisfiedBy(Game $game, Card $initiator, Card\Character $target = NULL): bool
    {
        if($initiator->isMinion()) {
            if(!$this->vacantPlaceOnBoard($game, $initiator, $target)) {
                $this->errorMessage = 'There is no vacant place on board';

                return false;
            }
        }

        if($initiator->isSpell()) {
            if(is_a($initiator, Card\Secret::class)) {
                if(!$this->checkMaximumSecretsCount($game, $initiator, $target)) {
                    $this->errorMessage = 'There are 5 secrets maximum available';

                    return false;
                }

                if(!$this->checkDoubleSecrets($game, $initiator, $target)) {
                    $this->errorMessage = 'Secret is already in use';

                    return false;
                }
            }

            if($target AND $this->targetHasStealth($target) AND $this->targetIsEnemy($initiator, $target)) {
                $this->errorMessage = 'Нельзя использовать заклинание на персонажа c маскировкой';

                return false;
            }
        }

        if($target AND $target->hasEffect('immune') AND $this->targetIsEnemy($initiator, $target)) {
            $this->errorMessage = 'Нельзя использовать заклинание на персонажа c неуязвимостью';

            return false;
        }

        if($target AND $target->hasEffect('elusive') AND $this->targetIsEnemy($initiator, $target)) {
            $this->errorMessage = 'Нельзя использовать заклинание на этого персонажа';

            return false;
        }

        if($result = $initiator->playCondition()->check($game, $initiator, $target)) {
            if($result->hasError()) {
                $this->errorMessage = $result->message();

                return false;
            }
        }

        return true;
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return $this->errorMessage;
    }

    /**
     * @param Game $game
     * @param Card $initiator
     * @param Card\Character|NULL $target
     * @return bool
     */
    private function vacantPlaceOnBoard(Game $game, Card $initiator, Card\Character $target = null)
    {
        return $game->currentPlayer()->board->count() < $game->settings->maxPlacesOnBoard();
    }

    /**
     * @param Game $game
     * @param Card $initiator
     * @param Card\Character|NULL $target
     * @return bool
     */
    private function checkMaximumSecretsCount(Game $game, Card $initiator, Card\Character $target = null)
    {
        $player = $initiator->getPlayer();

        return $player->secrets->count() < 5;
    }

    /**
     * @param Game $game
     * @param Card $initiator
     * @param Card\Character|NULL $target
     * @return bool
     */
    private function checkDoubleSecrets(Game $game, Card $initiator, Card\Character $target = null)
    {
        $secrets = $initiator->getPlayer()->secrets->all();

        foreach($secrets as $secret) {
            if($secret instanceof $initiator) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param Card\Character $target
     * @return bool
     */
    private function targetHasStealth(Card\Character $target)
    {
        return $target->hasEffect('stealth');
    }

    /**
     * @param Card $initiator
     * @param Card\Character $target
     * @return bool
     */
    private function targetIsEnemy(Card $initiator, Card\Character $target)
    {
        return $initiator->getPlayer()->id() != $target->getPlayer()->id();
    }
}