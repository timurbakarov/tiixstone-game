<?php

namespace Tiixstone\Condition;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Condition;

abstract class Target extends Condition
{
    /**
     * Нужно для того, чтобы проверить есть ли валидные цели при бэттлкрае
     * Если нет, то существо можно разыграть без цели
     *
     * @param Game $game
     * @return array
     */
    abstract public function targets(Game $game, Card $card) : array;
}