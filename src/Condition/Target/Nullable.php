<?php

namespace Tiixstone\Condition\Target;

use Tiixstone\Card;
use Tiixstone\Game;

class Nullable extends \Tiixstone\Condition\Target
{
    /**
     * @param Game $game
     * @param Card $initiator
     * @param \Tiixstone\Card\Character $target
     * @return bool
     */
    public function isSatisfiedBy(Game $game, Card $initiator, Card\Character $target = null): bool
    {
        return $target === null;
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return 'Target should be null';
    }

    /**
     * Нужно для того, чтобы проверить есть ли валидные цели при бэттлкрае
     * Если нет, то существо можно разыграть без цели
     *
     * @param Game $game
     * @return array
     */
    public function targets(Game $game, Card $card): array
    {
        return [];
    }
}