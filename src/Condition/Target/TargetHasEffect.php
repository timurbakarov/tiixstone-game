<?php

namespace Tiixstone\Condition\Target;

use Tiixstone\Card;
use Tiixstone\Game;
use Illuminate\Support\Arr;
use Tiixstone\Condition\Target;
use Tiixstone\Effect\DeathrattleEffect;

class TargetHasEffect extends Target
{
    private $effects;

    /**
     * @param $effects
     */
    public function __construct($effects)
    {
        $this->effects = Arr::wrap($effects);
    }

    /**
     * @param Game $game
     * @param \Tiixstone\Card $initiator
     * @param Card\Character|NULL $target
     * @return bool
     */
    public function isSatisfiedBy(Game $game, Card $initiator, Card\Character $target = NULL): bool
    {
        if(!$target) {
            return false;
        }

        foreach($this->effects as $effect) {
            if($game->effects->cardHasEffect($target, $effect)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return 'Target should have ' . implode(', ', $this->effectName($this->effects));
    }

    /**
     * Нужно для того, чтобы проверить есть ли валидные цели при бэттлкрае
     * Если нет, то существо можно разыграть без цели
     *
     * @param Game $game
     * @return array
     */
    public function targets(Game $game, Card $card): array
    {
        return $game->targetManager->allCharacters($game);
    }

    /**
     * @param array $effects
     * @return array
     */
    private function effectName(array $effects)
    {
        $effectsNames = [];

        foreach($effects as $effect) {
            $effectName = $effect;

            if(is_a($effect, DeathrattleEffect::class, true)) {
                $effectName = 'deathrattle';
            }

            $effectsNames[] = $effectName;
        }

        return $effectsNames;
    }
}