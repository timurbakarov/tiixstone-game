<?php

namespace Tiixstone\Condition\Target;

use Tiixstone\Card;
use Tiixstone\Game;

class AttackLowerThan extends \Tiixstone\Condition\Target
{
    /**
     * @var int
     */
    private $attack;

    public function __construct(int $attack)
    {
        $this->attack = $attack;
    }

    /**
     * @param Game $game
     * @param Card $initiator
     * @param \Tiixstone\Card\Character $target
     * @return bool
     */
    public function isSatisfiedBy(Game $game, Card $initiator, Card\Character $target = null): bool
    {
        if(!$target) {
            return false;
        }

        return $target->attack->total() < $this->attack;
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return 'Attack should be lower than ' . $this->attack;
    }

    /**
     * Нужно для того, чтобы проверить есть ли валидные цели при бэттлкрае
     * Если нет, то существо можно разыграть без цели
     *
     * @param Game $game
     * @return array
     */
    public function targets(Game $game, Card $card): array
    {
        return array_filter($game->targetManager->allEnemyMinions($game, $card->getPlayer()->getOpponent($game)), function(Card\Minion $minion) {
            return $minion->health->total() < $this->attack;
        });
    }
}