<?php

namespace Tiixstone\Condition\Target;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Card\Character;

class Race extends \Tiixstone\Condition\Target
{
    /**
     * @var \Tiixstone\Race
     */
    private $race;

    public function __construct(\Tiixstone\Race $race)
    {
        $this->race = $race;
    }

    /**
     * @param Game $game
     * @param Card $initiator
     * @param Character $target
     * @return bool
     */
    public function isSatisfiedBy(Game $game, Card $initiator, Character $target = null): bool
    {
        if(!$target) {
            return false;
        }

        return$target->race()->is($this->race);
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return 'Target should be ' . $this->race->label();
    }

    /**
     * @param Game $game
     * @return array
     */
    public function targets(Game $game, Card $card): array
    {
        return array_filter($game->targetManager->allCharacters($game), function(Character $character) {
            return $character->race()->is($this->race);
        });
    }
}