<?php

namespace Tiixstone\Condition\Target;

use Tiixstone\Card;
use Tiixstone\Game;

class Hero extends \Tiixstone\Condition\Target
{
    /**
     * @param Game $game
     * @param Card $initiator
     * @param \Tiixstone\Card\Character $target
     * @return bool
     */
    public function isSatisfiedBy(Game $game, Card $initiator, Card\Character $target = null): bool
    {
        if(!$target) {
            return false;
        }

        return $target->isHero();
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return 'Target should be a hero';
    }
}