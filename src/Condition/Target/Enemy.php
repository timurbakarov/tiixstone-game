<?php

namespace Tiixstone\Condition\Target;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Card\Character;

class Enemy extends \Tiixstone\Condition\Target
{
    /**
     * @param Game $game
     * @param Card $initiator
     * @param Character $target
     * @return bool
     */
    public function isSatisfiedBy(Game $game, Card $initiator, Character $target = NULL): bool
    {
        if(!$target) {
            return false;
        }

        if(!$initiator->getPlayer()) {
            return false;
        }

        if(!$target->getPlayer()) {
            return false;
        }

        return $initiator->getPlayer()->id() != $target->getPlayer()->id();
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return 'Target should be an enemy';
    }

    /**
     * @param Game $game
     * @return array
     */
    public function targets(Game $game, Card $card): array
    {
        return $game->targetManager->allEnemyCharacters($game, $card->getPlayer());
    }
}