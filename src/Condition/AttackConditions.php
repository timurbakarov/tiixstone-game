<?php

namespace Tiixstone\Condition;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Condition;

class AttackConditions extends Condition
{
    /**
     * @var
     */
    private $errorMessage;

    /**
     * @param Game $game
     * @param \Tiixstone\Card|Card\Character $initiator
     * @param Card\Character|NULL $target
     * @return bool
     */
    public function isSatisfiedBy(Game $game, Card $initiator, Card\Character $target = null): bool
    {
        if(!$this->validAttacker($game, $initiator)) {
            $this->errorMessage = 'Invalid attacker character';

            return false;
        }

        if(!$this->validTarget($game, $target)) {
            $this->errorMessage = 'Target should be an enemy';

            return false;
        }

        if($this->attackerAttackIsZero($initiator)) {
            $this->errorMessage = 'Attack should be greater than 0';

            return false;
        }

        if($target->hasEffect('immune')) {
            $this->errorMessage = 'Нельзя атаковать персонажа с неуязвимостью';

            return false;
        }

        if($initiator->hasEffect('freeze')) {
            $this->errorMessage = 'Frozen character can not attack';

            return false;
        }

        if($game->attackTracker->isExhausted($initiator)) {
            $this->errorMessage = 'Attacker is exhausted';

            return false;
        }

        if(!$this->checkTaunt($game, $initiator, $target)) {
            $this->errorMessage = 'You should attack taunt';

            return false;
        }

        if($target->hasEffect('stealth')) {
            $this->errorMessage = 'Нельзя атаковать персонажа с маскировкой';

            return false;
        }

        if($result = $initiator->attackCondition()->check($game, $initiator, $target)) {
            if($result->hasError()) {
                $this->errorMessage = $result->message();

                return false;
            }
        }

        return true;
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return $this->errorMessage;
    }

    /**
     * @param Game $game
     * @param Card\Character $attacker
     * @param Card\Character $target
     * @return bool
     */
    public function checkTaunt(Game $game, Card\Character $attacker, Card\Character $target)
    {
        if($this->characterHaveTaunt($target)) {
            return true;
        }

        if($this->noTauntsOnEnemyBoard($game)) {
            return true;
        }

        return false;
    }

    /**
     * @param Card\Character $defender
     * @return bool
     */
    private function characterHaveTaunt(Card\Character $defender)
    {
        return $defender->hasEffect('taunt')
            && !$defender->hasEffect('stealth')
            && !$defender->hasEffect('immune')
        ;
    }

    /**
     * @param Game $game
     * @return bool
     */
    private function noTauntsOnEnemyBoard(Game $game) : bool
    {
        $enemyCharacters = $game->targetManager->allEnemyCharacters($game, $game->currentPlayer());

        foreach($enemyCharacters as $character) {
            if($this->characterHaveTaunt($character)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param Game $game
     * @param Card $initiator
     * @return bool
     */
    private function validAttacker(Game $game, Card $initiator)
    {
        return $game->currentPlayer()->board->has($initiator->id()) || $game->currentPlayer()->hero->isSelf($initiator);
    }

    /**
     * @param Game $game
     * @param Card $target
     * @return bool
     */
    private function validTarget(Game $game, Card $target)
    {
        return $game->idlePlayer()->board->has($target->id()) || $game->idlePlayer()->hero->isSelf($target);
    }

    /**
     * @param Card\Character $initiator
     * @return bool
     */
    private function attackerAttackIsZero(Card\Character $initiator)
    {
        return $initiator->attack->total() <= 0;
    }

}