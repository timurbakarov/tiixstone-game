<?php

namespace Tiixstone\Condition;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Condition;
use Tiixstone\Card\Character;

class FriendlyHeroHasWeapon extends Condition
{
    /**
     * @param Game $game
     * @param Card $initiator
     * @param Character|NULL $target
     * @return bool
     */
    public function isSatisfiedBy(Game $game, Card $initiator, Character $target = NULL): bool
    {
        return $initiator->getPlayer()->hero->hasWeapon();
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return 'Your hero should have weapon';
    }
}