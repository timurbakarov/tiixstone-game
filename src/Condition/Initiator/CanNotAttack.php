<?php

namespace Tiixstone\Condition\Initiator;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Condition;

class CanNotAttack extends Condition
{
    /**
     * @param Game $game
     * @param \Tiixstone\Card $initiator
     * @param Card\Character|NULL $target
     * @return bool
     */
    public function isSatisfiedBy(Game $game, Card $initiator, Card\Character $target = NULL): bool
    {
        return false;
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return 'Character can not attack';
    }
}