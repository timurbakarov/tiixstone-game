<?php

namespace Tiixstone\Condition\Initiator;

use Tiixstone\Card;
use Tiixstone\Game;

class Character extends \Tiixstone\Condition\Target
{
    /**
     * @param Game $game
     * @param Card $initiator
     * @param \Tiixstone\Card\Character $target
     * @return bool
     */
    public function isSatisfiedBy(Game $game, Card $initiator, Card\Character $target = null): bool
    {
        return $initiator->isMinion() || $initiator->isHero();
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return 'Attacker is not character';
    }
}