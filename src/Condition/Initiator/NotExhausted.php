<?php

namespace Tiixstone\Condition\Initiator;

use Tiixstone\Card;
use Tiixstone\Game;

class NotExhausted extends \Tiixstone\Condition
{
    /**
     * @param Game $game
     * @param Card $initiator
     * @param \Tiixstone\Card\Character $target
     * @return bool
     */
    public function isSatisfiedBy(Game $game, Card $initiator, Card\Character $target = null): bool
    {
        $isExhausted = $game->attackTracker->isExhausted($initiator);
        $canAttackAnyway = $game->attackTracker->canAttackAnyway($initiator);

        return !$isExhausted || $canAttackAnyway;
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return 'Attacker is exhausted';
    }
}