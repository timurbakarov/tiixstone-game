<?php

namespace Tiixstone\Condition\Initiator;

use Tiixstone\Card;
use Tiixstone\Game;

class Damaged extends \Tiixstone\Condition\Target
{
    /**
     * @param Game $game
     * @param Card $initiator
     * @param \Tiixstone\Card\Character $target
     * @return bool
     */
    public function isSatisfiedBy(Game $game, Card $initiator, Card\Character $target = null): bool
    {
        return $initiator->health->isDamaged();
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return 'Initiator should be damaged';
    }
}