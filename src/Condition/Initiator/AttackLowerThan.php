<?php

namespace Tiixstone\Condition\Initiator;

use Tiixstone\Card;
use Tiixstone\Game;

class AttackLowerThan extends \Tiixstone\Condition
{
    /**
     * @var int
     */
    private $attack;

    public function __construct(int $attack)
    {
        $this->attack = $attack;
    }

    /**
     * @param Game $game
     * @param Card $initiator
     * @param \Tiixstone\Card\Character $target
     * @return bool
     */
    public function isSatisfiedBy(Game $game, Card $initiator, Card\Character $target = null): bool
    {
        return $target->attack->total() < $this->attack;
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return 'Attack should be lower than ' . $this->attack;
    }
}