<?php

namespace Tiixstone\Condition;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Condition;

class ClosureCondition extends Condition
{
    /**
     * @var \Closure
     */
    private $closure;

    /**
     * @var string
     */
    private $errorMessage;

    public function __construct(\Closure $closure, string $errorMessage)
    {
        $this->closure = $closure;
        $this->errorMessage = $errorMessage;
    }

    /**
     * @param Game $game
     * @param Card $initiator
     * @param \Tiixstone\Card\Character $target
     * @return bool
     */
    public function isSatisfiedBy(Game $game, Card $initiator, Card\Character $target = null): bool
    {
        $closure = $this->closure;

        return $closure($game, $initiator, $target);
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return $this->errorMessage;
    }
}