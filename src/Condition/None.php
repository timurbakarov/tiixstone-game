<?php

namespace Tiixstone\Condition;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Condition;

class None extends Condition
{
    /**
     * @param Game $game
     * @param Card $initiator
     * @param \Tiixstone\Card\Character $target
     * @return bool
     */
    public function isSatisfiedBy(Game $game, Card $initiator, Card\Character $target = null): bool
    {
        return $target === null;
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return 'There should not be a target';
    }
}