<?php

namespace Tiixstone\Condition;

use Tiixstone\Condition;

class Result
{
    /**
     * @var bool
     */
    private $hasError;

    /**
     * @var Condition
     */
    public $condition;

    public function __construct(bool $hasError, Condition $condition = null)
    {
        $this->hasError = $hasError;
        $this->condition = $condition;
    }

    /**
     * @return bool
     */
    public function hasError() : bool
    {
        return $this->hasError;
    }

    /**
     * @return bool
     */
    public function success()
    {
        return !$this->hasError;
    }

    /**
     * @return string
     */
    public function message() : string
    {
        return $this->condition->errorMessage();
    }
}