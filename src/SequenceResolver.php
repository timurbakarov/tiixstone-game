<?php declare(strict_types=1);

namespace Tiixstone;

use Tiixstone\Block\ClosureBlock;
use Tiixstone\Block\Trigger;
use Tiixstone\Card\Destroyable;
use Tiixstone\Event\InspireEvent;
use Tiixstone\Effect\Changer\BlockChanger;

class SequenceResolver
{
    /**
     * @var
     */
    private $stopBlockResolving = false;

    /**
     * @var array
     */
    private $leftBlocks = [];

    /**
     * @var
     */
    private $stopBlockResolvingClosure;

    /**
     * @param Game $game
     * @param array $blocks
     * @throws \Tiixstone\Exception
     */
    public function resolveSequence(Game $game, array $blocks)
    {
        $this->stopBlockResolving = false;

        $blocks = array_merge(
            [new Block\Trigger(new Event\BeforeMove())],
            $blocks,
            [new Block\Trigger(new Event\AfterMove())],
            [new ClosureBlock(function() use($game) {
                $this->afterSequenceBlocksResolve($game);

                return [];
            })]
        );

        $this->resolveBlock($game, $blocks);

        if($this->stopBlockResolving) {
            $closure = $this->stopBlockResolvingClosure;

            $closure($this->leftBlocks);
        }

        if($game->isOver()) {
            $game->status->set(Game\Status::STATUS_OVER);

            $game->logBlock('game-over', [
                'isDraw' => $game->isDraw(),
                'winner' => $game->winner(),
                'loser' => $game->loser(),
            ]);
        }
    }

    /**
     * @param Game $game
     * @param $block
     * @return $this|SequenceResolver
     */
    public function resolveBlock(Game $game, $block)
    {
        if(is_array($block)) {
            foreach($block as $subBlocks) {
                if($subBlocks === false) {
                    break;
                }

                $this->resolveBlock($game, $subBlocks);
            }

            return $this;
        } else {
            /**
             * рекурсивное изменение блоков, как например в ситуации с аукейнакой и божественным щитом
             * аукенайка меняет лечение на атаку, а щит меняет атаку на удаление щита
             */
            while(true) {
                if($block instanceof Block\Trigger) {
                    break;
                }

                $newBlock = null;

                /** @var BlockChanger $effect */
                foreach($game->effects->getByEffect(BlockChanger::class) as $effect) {
                    $newBlock = $effect->changeBlock($game, $block);

                    if(get_class($newBlock) != get_class($block)) {
                        break;
                    }
                }

                if(is_null($newBlock) OR get_class($newBlock) == get_class($block)) {
                    break;
                } else {
                    $block = clone $newBlock;
                }
            }

            if($this->stopBlockResolving) {
                $this->leftBlocks[] = $block;

                $blocks = [];
            } else {
                $blocks = $block->run($game);
            }

            return $this->resolveBlock($game, $blocks);
        }
    }

    /**
     * @param Game $game
     */
	public function afterSequenceBlocksResolve(Game $game)
	{
		$hasDestroyed = true;
		
		while($hasDestroyed) {
            $hasDestroyed = false;

			foreach($this->afterSequenceBlocks(true) as $block) {
				$this->resolveBlock($game, $block);
			}

			/** @var Destroyable $char */
            foreach($game->targetManager->destroyable($game) as $char) {
				if($char->isDestroyed() AND !$char->isHero()) {
                    $hasDestroyed = true;
				}
			}
		}
	}

    /**
     * @return array
     */
    public function afterSequenceBlocks(bool $triggerInspire = false)
    {
        return [
            new Block\CheckDestroyed(),
            $triggerInspire ? new Trigger(new InspireEvent()) : [],
            new Block\RecalculateBuffs(),
        ];
    }

    /**
     * @param \Closure $closure
     */
    public function setStopBlockResolvingClosure(\Closure $closure)
    {
        $this->stopBlockResolvingClosure = $closure;
    }

    /**
     * @return $this
     */
    public function stopBlockResolving()
    {
        $this->stopBlockResolving = true;

        return $this;
    }
}