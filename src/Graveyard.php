<?php

namespace Tiixstone;

class Graveyard
{
    /**
     * @var array
     */
    private $playedItems = [];

    /**
     * @param Game $game
     * @param Card $card
     */
    public function addPlayedCard(Game $game, Card $card)
    {
        $this->playedItems[] = [
            'card' => get_class($card),
            'turn' => $game->turnNumber(),
            'player' => $game->currentPlayer()->id(),
        ];
    }

    /**
     * @param int $turn
     * @return array
     */
    public function playedCardsByTurn(int $turn)
    {
        $playedItems = [];

        foreach($this->playedItems as $playedItem) {
            if($playedItem['turn'] == $turn) {
                $playedItems[] = $playedItem;
            }
        }

        return $playedItems;
    }
}