<?php

namespace Tiixstone;

use Tiixstone\Card\Hero;
use Tiixstone\Card\Minion;
use Tiixstone\Card\Character;
use Tiixstone\Effect\WindfuryEffect;

/**
 * Управляет состоянием существ на столе
 *
 * Class AttackTracker
 * @package Tiixstone
 */
class AttackTracker
{
    const JUST_SUMMONED = 1;
    const ATTACK_TIMES = 2;

    /**
     * @var array
     */
    private $characters = [];

    /**
     * @param Character|Minion $attacker
     * @return bool
     */
    public function isExhausted(Character $attacker) : bool
    {
        if($attacker instanceof Hero) {
            return $this->attackTimesGreaterThanZero($attacker);
        }

        if($attacker->isMinion() AND $this->isJustSummoned($attacker) AND !$attacker->hasEffect('charge')) {
            return true;
        }

        if($this->attackTimes($attacker) >= 1 AND !$attacker->hasEffect(WindfuryEffect::class)) {
            return true;
        }

        if($this->attackTimes($attacker) >= 2) {
            return true;
        }

        return false;
    }

    /**
     * @param Minion $minion
     * @param bool $justSummoned
     * @return $this
     */
    public function setJustSummoned(Minion $minion, bool $justSummoned)
    {
        $this->setProperty($minion, self::JUST_SUMMONED, $justSummoned);

        return $this;
    }

    /**
     * @param Character $character
     * @param int $attackTimes
     * @return $this
     */
    public function setAttackTimes(Character $character, int $attackTimes)
    {
        $this->setProperty($character, self::ATTACK_TIMES, $attackTimes);

        return $this;
    }

    /**
     * @param Character $character
     * @return $this
     */
    public function resetAttackTimes(Character $character)
    {
        $this->setProperty($character, self::ATTACK_TIMES, 0);

        return $this;
    }

    /**
     * @param Character $character
     * @return mixed
     */
    public function attackTimes(Character $character)
    {
        return $this->getProperty($character, self::ATTACK_TIMES);
    }

    /**
     * @param Character $character
     * @return $this
     */
    public function incrementAttackTimes(Character $character)
    {
        $this->setProperty($character, self::ATTACK_TIMES, $this->getProperty($character, self::ATTACK_TIMES) + 1);

        return $this;
    }

    /**
     * @param Character $character
     * @param string $property
     * @param $value
     * @return $this
     */
    private function setProperty(Character $character, string $property, $value)
    {
        $this->characters[$character->id()][$property] = $value;

        return $this;
    }

    /**
     * @param Character $character
     * @param string $property
     * @return mixed
     */
    private function getProperty(Character $character, string $property)
    {
        return $this->characters[$character->id()][$property] ?? null;
    }

    /**
     * @return $this
     */
    public function clear()
    {
        $this->characters = [];

        return $this;
    }

    /**
     * @param Minion $attacker
     * @return mixed
     */
    public function isJustSummoned(Minion $attacker)
    {
        return $this->getProperty($attacker, self::JUST_SUMMONED);
    }

    /**
     * @param Character $attacker
     * @return mixed
     */
    private function attackTimesGreaterThanZero(Character $attacker)
    {
        return $this->getProperty($attacker, self::ATTACK_TIMES) > 0;
    }
}