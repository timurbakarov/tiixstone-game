<?php

namespace Tiixstone;

use Tiixstone\Card\Minion;

class Battlefield
{
    /**
     * @var Minion[]
     */
    private $minions = [];

    /**
     * @return array
     */
    public function minions() : array
    {
        return $this->minions;
    }

    /**
     * @param Player $player
     * @return array
     */
    public function minionsByPlayer(Player $player) : array
    {
        return array_filter($this->minions(), function(Minion $minion) use($player) {
            return $player->id() == $minion->getPlayer()->id();
        });
    }

    /**
     * @param Minion $minion
     * @return $this
     */
    public function add(Minion $minion)
    {
        $this->minions[$minion->id()] = $minion;

        return $this;
    }

    /**
     * @param Minion $minion
     * @return $this
     */
    public function remove(Minion $minion)
    {
        unset($this->minions[$minion->id()]);

        return $this;
    }
}