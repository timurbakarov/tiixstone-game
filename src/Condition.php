<?php

namespace Tiixstone;

abstract class Condition
{
    /**
     * @param Game $game
     * @param \Tiixstone\Card $initiator
     * @param Card\Character|NULL $target
     * @return bool
     */
    abstract public function isSatisfiedBy(Game $game, Card $initiator, Card\Character $target = null) : bool;

    /**
     * @return string
     */
    abstract public function errorMessage() : string;
}