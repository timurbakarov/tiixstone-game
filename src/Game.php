<?php declare(strict_types=1);

namespace Tiixstone;

use Ramsey\Uuid\Uuid;
use Tiixstone\Event\AfterAction;
use Tiixstone\Event\BeforeAction;
use Tiixstone\Game\Status;
use Tiixstone\Game\Settings;
use Tiixstone\Block\Trigger;
use Tiixstone\Coin\RandomCoin;
use Tiixstone\Event\GameStarted;
use Tiixstone\Event\TurnStarted;
use Tiixstone\Effect\GlobalEffects;
use Tiixstone\Card\Collection\Cards;
use Tiixstone\Manager\TargetManager;

class Game
{
    /**
     * @var Effects
     */
    public $effects;

    /**
     * @var int
     */
    private static $uuid = 1;

    /**
     * @var int
     */
    protected $turnNumber = 1;

    /**
     * @var Player
     */
    public $firstToGoPlayer;

    /**
     * @var Player
     */
    public $secondToGoPlayer;

    /**
     * @var Player
     */
    public $player1;

    /**
     * @var Player
     */
    public $player2;

    /**
     * @var TargetManager
     */
    public $targetManager;

    /**
     * @var SequenceResolver
     */
    public $resolver;

    /**
     * @var Settings
     */
    public $settings;

    /**
     * @var Coin
     */
    public $coin;

    /**
     * @var Status
     */
    public $status;

    /**
     * @var array
     */
    private $logAction;

    /**
     * @var
     */
    private $logBlocks;

    /**
     * @var Card\Collection\
     */
    public $cardsList;

    /**
     * @var array
     */
    private $globalEffects = [];

    /**
     * @var Graveyard
     */
    public $graveyard;

    /**
     * @var AttackTracker
     */
    public $attackTracker;

    /**
     * @var SpellDamageTracker
     */
    public $spellDamageTracker;

    /**
     * @var RNG
     */
    public $RNG;

    /**
     * @var Battlefield
     */
    public $battlefield;

    public function __construct(
        Player $player1,
        Player $player2,
        TargetManager $targetManager,
        Coin $coin = null,
        array $globalEffects = []
    ) {
        $this->battlefield = new Battlefield();
        $this->player1 = $player1;
        $this->player2 = $player2;
        $this->targetManager = $targetManager;
        $this->resolver = new SequenceResolver();
        $this->graveyard = new Graveyard();
        $this->settings = new Settings();
        $this->coin = $coin ?? new RandomCoin();
        $this->status = new Status();
        $this->effects = new Effects();
        $this->attackTracker = new AttackTracker();
        $this->globalEffects = $globalEffects ?: self::defaultGlobalEffects();
        $this->cardsList = new Cards();
        $this->spellDamageTracker = new SpellDamageTracker();
        $this->RNG = new RNG();
    }

    /**
     * @return array
     */
    public static function defaultGlobalEffects()
    {
        return [
            GlobalEffects\GameFlow::class,
            GlobalEffects\AttackFlow::class,
            GlobalEffects\ManaRoutine::class,
            GlobalEffects\CardsDrawingRoutine::class,
            GlobalEffects\HeroPowerRoutine::class,
            GlobalEffects\WeaponRoutine::class,
            GlobalEffects\EffectsFlow::class,
            GlobalEffects\PlayCardsFlow::class,
        ];
    }

    /**
     * @param RNG $RNG
     * @return $this
     */
    public function setRNG(RNG $RNG)
    {
        $this->RNG = $RNG;

        return $this;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function restart()
    {
        $this->status->set(Status::STATUS_PRE_START);

        self::resetUuid();

        return $this->start();
    }

    /**
     * @return array
     * @throws Exception
     */
    public function start()
    {
        if(!$this->status->is(Status::STATUS_PRE_START)) {
            throw new Exception("Game is not in pre started status");
        }

        $this->status->set(Status::STATUS_STARTED);

        $this->clearLogs();
        $this->logAction('start', []);

        $this->coin->toss($this->player1, $this->player2);
        $this->firstToGoPlayer = $this->coin->winner();
        $this->secondToGoPlayer = $this->coin->loser();

        $this->currentPlayer()->deck->shuffle();
        $this->idlePlayer()->deck->shuffle();

        $this->effects->addGlobalEffects($this, $this->globalEffects);

        $this->resolver->resolveSequence($this, [
            new Block\HeroPower\Replace($this->player1, $this->player1->hero->defaultPower()),
            new Block\HeroPower\Replace($this->player2, $this->player2->hero->defaultPower()),
            new Trigger(new GameStarted),
            new Trigger(new TurnStarted($this->currentPlayer())),
        ]);

        $this->cardsList->append($this->player1->hero);
        $this->cardsList->append($this->player2->hero);

        foreach($this->player1->deck->all() as $card) {
            $this->cardsList->append($card);
            $card->setPlayer($this->player1);
        }

        foreach($this->player2->deck->all() as $card) {
            $this->cardsList->append($card);
            $card->setPlayer($this->player2);
        }

        foreach($this->player1->deck->all() as $card) {
            $this->effects->addMany($this, $card, $card->defaultEffects());
        }

        foreach($this->player2->deck->all() as $card) {
            $this->effects->addMany($this, $card, $card->defaultEffects());
        }

        return $this->logs();
    }

    /**
     * @param Action $action
     * @return array
     * @throws Exception
     */
	public function action(Action $action)
	{
		if(!$this->status->is(Status::STATUS_STARTED)) {
            throw new Exception("Game should be in started status");
        }

		$blocks = $action->process($this);

		$blocks = array_merge(
		    [new Trigger(new BeforeAction($action))],
		    $blocks,
            [new Trigger(new AfterAction($action))]
        );

		$this->resolver->resolveSequence($this, $blocks);

		return $this->logs();
	}

    /**
     * @return Player
     */
    public function currentPlayer() : Player
    {
        return $this->turnNumber % 2 ? $this->firstToGoPlayer : $this->secondToGoPlayer;
    }

    /**
     * @return Player
     */
    public function idlePlayer() : Player
    {
        return $this->turnNumber % 2 ? $this->secondToGoPlayer : $this->firstToGoPlayer;
    }

    /**
     * @return int
     */
    public function turnNumber() : int
    {
        return $this->turnNumber;
    }

    /**
     * @return $this
     */
    public function setTurnNumber(int $turnNumber)
    {
        $this->turnNumber = $turnNumber;
        
        return $this;
    }

    /**
     * @return bool
     */
    public function isOver() : bool
    {
        return $this->player1->hero->isDestroyed() || $this->player2->hero->isDestroyed();
    }

    /**
     * @return bool|Player
     * @throws Exception
     */
    public function winner()
    {
        if(!$this->status->is(Status::STATUS_OVER)) {
            throw new Exception("Game is in progress");
        }

        if(!$this->player1->hero->isDestroyed()) {
            return $this->player1;
        } elseif(!$this->player2->hero->isDestroyed()) {
            return $this->player2;
        } else {
            return false;
        }
    }

    /**
     * @return bool|Player
     * @throws Exception
     */
    public function loser()
    {
        if(!$this->status->is(Status::STATUS_OVER)) {
            throw new Exception("Game is in progress");
        }

        if(!$this->winner()) {
            return false;
        }

        return $this->winner()->id() == $this->player1->id() ? $this->player2 : $this->player1;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function isDraw()
    {
        if(!$this->status->is(Status::STATUS_OVER)) {
            throw new Exception("Game is in progress");
        }

        return !$this->winner() AND !$this->loser();
    }

    /**
     * @return string
     */
    public static function uuid()
    {
        return Uuid::uuid4()->toString();
    }

    /**
     * @return $this
     */
    public static function resetUuid()
    {
        self::$uuid = 1;
    }

    /**
     * @param $action
     * @param array $data
     * @return $this
     */
    public function logAction($action, array $data)
    {
        if($this->logAction) {
            $this->clearLogs();
            //throw new Exception("LogAction already set. Return first");
        }

        $this->logAction = [
            'name' => $action,
            'data' => $data,
        ];

        return $this;
    }

    /**
     * @param $name
     * @param array $data
     * @return $this
     */
    public function logBlock($name, array $data = [])
    {
        $this->logBlocks[] = [
            'name' => $name,
            'data' => $data,
        ];

        return $this;
    }

    /**
     * @return array
     */
    public function logs() : array
    {
        $logs = [
            'action' => $this->logAction,
            'blocks' => $this->logBlocks,
        ];

        $this->clearLogs();

        return $logs;
    }

    /**
     * @return $this
     */
    public function clearLogs()
    {
        $this->logAction = null;
        $this->logBlocks = [];

        return $this;
    }

    /**
     * @deprecated
     * @param Card $card
     * @return Player
     */
    public function getPlayerByCard(Card $card) : Player
    {
		return $card->getPlayer();
    }

    /**
     * @param int $damage
     * @return int
     */
    public function withSpellDamage(int $damage)
    {
        return $this->spellDamageTracker->get($this->currentPlayer()) + $damage;
    }
}