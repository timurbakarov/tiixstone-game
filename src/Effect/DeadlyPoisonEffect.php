<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Character;
use Tiixstone\Card\Attackable;

class DeadlyPoisonEffect extends Effect implements Effect\Changer\AttackChanger
{
    /**
     * @param Game $game
     * @param Character $character
     * @param int $attackRate
     * @return int
     */
    public function changeAttack(Game $game, Attackable $character, int $attackRate): int
    {
        if($this->owner()->id() == $character->id()) {
            return $attackRate + 2;
        }

        return $attackRate;
    }
}