<?php

namespace Tiixstone\Effect;

use Tiixstone\Block\CastSpell;
use Tiixstone\Card;
use Tiixstone\Card\Spell;
use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Minion;
use Tiixstone\Block\Adapt;
use Tiixstone\Action\ChooseOne;
use Tiixstone\Block\ChooseOne as ChooseOneBlock;
use Tiixstone\Event\AfterAction;
use Tiixstone\Block\Effect\RemoveEffect;

class AdaptEffect extends Effect
{
    /**
     * @var Minion
     */
    private $minion;

    /**
     * @var int
     */
    private $times;

    /**
     * @param int $times
     */
    public function __construct(int $times = 1)
    {
        $this->times = $times;
    }

    /**
     * @return array
     */
    public function events()
    {
        return [
            AfterAction::class => 'afterAction',
        ];
    }

    /**
     * @param Game $game
     * @param Card $card
     * @return array
     */
    public function init(Game $game, Card $card) : array
    {
        $this->minion = $card;

        return $this->chooseOneBlock($game);
    }

    /**
     * @param Game $game
     * @param AfterAction $event
     * @return array
     */
    public function afterAction(Game $game, AfterAction $event)
    {
        if($event->action() instanceof ChooseOne) {
            return [];
        }

        $this->times--;

        if($this->times <= 0) {
            return [new RemoveEffect([$this])];
        }

        return $this->chooseOneBlock($game);
    }

    /**
     * @param Game $game
     * @return array
     * @throws \Exception
     */
    private function chooseOneBlock(Game $game)
    {
        $options = $game->RNG->randomFromArray($game->RNG->shuffleArray($this->options()), 3);

        return [
            new ChooseOneBlock(function(Spell $spell) {
                return new CastSpell($spell, $this->minion);
            }, ...$options),
        ];
    }

    /**
     * @return array
     */
    private function options()
    {
        return [
            Card\Alias\LightningSpeed::create(),
            Card\Alias\CracklingShield::create(),
            Card\Alias\FlamingClaws::create(),
            Card\Alias\Massive::create(),
            Card\Alias\VolcanicMight::create(),
            Card\Alias\PoisonSpit::create(),
            Card\Alias\ShroudingMist::create(),
            Card\Alias\RockyCarapace::create(),
            Card\Alias\LivingSpores::create(),
            Card\Alias\LiquidMembrane::create(),
        ];
    }
}