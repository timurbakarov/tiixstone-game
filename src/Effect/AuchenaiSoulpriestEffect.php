<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;

class AuchenaiSoulpriestEffect extends Effect implements Effect\Changer\BlockChanger
{
    /**
     * @param Game $game
     * @param Block $block
     * @return array
     */
    public function changeBlock(Game $game, Block $block): Block
    {
        if($block instanceof Block\HealCharacter AND $this->ourPlayerTurn($game, $block)) {
            $game->logBlock('auchenai-soulpriest-effect', ['card' => $this->owner()]);

            return new Block\TakeDamage($block->character(), $block->amount(), $block->source());
        }

        return $block;
    }

    /**
     * @param Game $game
     * @param Block\HealCharacter $block
     * @return bool
     */
    private function ourPlayerTurn(Game $game, Block\HealCharacter $block)
    {
        return $this->owner()->getPlayer()->id() == $game->currentPlayer()->id();
    }
}