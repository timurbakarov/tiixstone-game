<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Block;

/**
 * Class LifestealEffect
 * @package Tiixstone\Effect
 */
class LifestealEffect extends \Tiixstone\Effect
{
    /**
     * @var Target|Target\SelfTarget
     */
	private $target;

    /**
     * LifestealEffect constructor.
     * @param Target|NULL $target
     */
	public function __construct(Target $target = null)
	{
		$this->target = $target ?? new Target\SelfTarget;
	}

    /**
     * @return array
     */
	public function events()
	{
		return [
			Event\AfterTakeDamage::class => 'afterTakeDamage',
		];
	}

    /**
     * @param Game $game
     * @param Event\AfterTakeDamage $event
     * @return array
     */
	public function afterTakeDamage(Game $game, Event\AfterTakeDamage $event)
	{
		if(!$event->source) {
			return [];
		}
		
		if(!$this->target->condition($game, $this->owner(), $event->source)) {
			return [];
		}
		
		return [new Block\HealCharacter($this->owner()->getPlayer()->hero, $event->amount)];
	}
}