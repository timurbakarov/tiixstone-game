<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Event;
use Tiixstone\Effect;

/**
 * Class Secret
 * @implements Secret\Eventable|Secret\BlockChangeable
 * @package Tiixstone\Effect
 */
abstract class Secret extends Effect implements Effect\Changer\BlockChanger
{
    /**
     * @return array
     */
    public function events()
    {
        if($this instanceof Effect\Secret\Eventable) {
            return [$this->triggerAt() => 'triggerAtEvent'];
        } else {
            return [];
        }
    }

    /**
     * @param Game $game
     * @param Event $event
     * @return array
     */
    public final function triggerAtEvent(Game $game, Event $event)
    {
        if($this->condition($game, $event, $this->owner())) {
            $game->logBlock('secret-trigger', ['card' => $this->owner()]);

            return array_merge(
                $this->action($game, $event, $this->owner()),
                [
                    new Block\Trigger(new Event\SecretTriggered($this->owner())),
                    new Block\Effect\RemoveEffect([$this]),
                    new Block\ClosureBlock(function(Game $game) {
                        $this->owner()->getPlayer()->secrets->remove($this->owner()->id());
                        return [];
                    }),
                ]
            );
        }

        return [];
    }

    /**
     * @param Game $game
     * @param Block $block
     * @return array
     */
    public function changeBlock(Game $game, Block $block): Block
    {
        if($this instanceof Effect\Secret\BlockChangeable) {
            if(get_class($block) != $this->triggerAt()) {
                return $block;
            }

            if(!$this->condition($game, $block, $this->owner())) {
                return $block;
            }

            $game->logBlock('secret-trigger', ['card' => $this->owner()]);

            return new Block\ClosureBlock(function (Game $game) use($block) {
                return array_merge(
                    $this->action($game, $block, $this->owner()),
                    [
                        new Block\Trigger(new Event\SecretTriggered($this->owner())),
                        new Block\Effect\RemoveEffect([$this]),
                        new Block\ClosureBlock(function(Game $game) {
                            $this->owner()->getPlayer()->secrets->remove($this->owner()->id());
                            return [];
                        }),
                    ]
                );
            });
        }

        return $block;
    }
}