<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Effect;

class OnePlusCostEffect extends Effect implements Effect\Changer\ManaCostChanger
{
    /**
     * @param Game $game
     * @param Card $card
     * @param int $cost
     * @return int
     */
    public function changeManaCost(Game $game, Card $card, int $cost): int
    {
        return $this->owner()->isSelf($card) ? $cost - 1 : $cost;
    }
}