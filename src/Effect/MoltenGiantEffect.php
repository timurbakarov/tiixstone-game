<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Effect;

class MoltenGiantEffect extends Effect implements Effect\Changer\ManaCostChanger
{
    /**
     * @param Game $game
     * @param Card $card
     * @param int $cost
     * @return int
     */
    public function changeManaCost(Game $game, Card $card, int $cost): int
    {
        if($card->isSelf($this->owner())) {
            $hero = $card->getPlayer()->hero;
            $heroDamage = $hero->health->maximum->get() - $hero->health->current->get();

            return $cost - $heroDamage;
        }

        return $cost;
    }
}