<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Player;
use Tiixstone\Card\Minion;
use Tiixstone\Block\Recruit;
use Tiixstone\Event\AfterDestroyMinion;

class PossessedLackeyDeathrattle extends Effect
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            AfterDestroyMinion::class => 'deathrattle',
        ];
    }

    /**
     * @param Game $game
     * @param AfterDestroyMinion $event
     * @return array
     */
    public function deathrattle(Game $game, AfterDestroyMinion $event): array
    {
        return [new Recruit($event->minion->getPlayer(), $this->recruitMinion($event->minion->getPlayer()))];
    }

    /**
     * @param Player $player
     * @return \Closure
     */
    private function recruitMinion(Player $player)
    {
        return function(Game $game) use($player) {
            foreach($player->deck->all() as $card) {
                if($card instanceof Minion && $card->race()->isDemon()) {
                    return $card;
                }
            }

            return false;
        };
    }
}