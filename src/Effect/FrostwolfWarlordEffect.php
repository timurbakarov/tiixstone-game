<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Card\Attackable;
use Tiixstone\Card\Character;
use Tiixstone\Effect;
use Tiixstone\Game;

class FrostwolfWarlordEffect extends Effect implements Effect\Changer\AttackChanger, Effect\Changer\HealthChanger
{
    private $amount;

    public function init(Game $game, Card $card)
    {
        $player = $game->getPlayerByCard($card);

        $this->amount = $player->board->count() - 1;

        $game->logBlock('frostwolf-warlord-effect-init-' . $this->amount);
    }

    /**
     * @param Game $game
     * @param Attackable $character
     * @param int $attackRate
     * @return int
     */
    public function changeAttack(Game $game, Attackable $character, int $attackRate): int
    {
        if($character->isSelf($this->owner)) {
            return $attackRate + $this->amount;
        }

        return 0;
    }

    /**
     * @param Game $game
     * @param Attackable $character
     * @param int $attackRate
     * @return int
     */
    public function changeHealth(Game $game, Character $character, int $health): int
    {
        if($character->isSelf($this->owner)) {
            return $health + $this->amount;
        }

        return 0;
    }
}