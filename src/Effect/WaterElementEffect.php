<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Block\GiveEffect;
use Tiixstone\Event\AfterTakeDamage;

class WaterElementEffect extends Effect
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            AfterTakeDamage::class => 'afterTakeDamage',
        ];
    }

    /**
     * @param Game $game
     * @param AfterTakeDamage $event
     * @return array
     */
    public function afterTakeDamage(Game $game, AfterTakeDamage $event)
    {
        if(!$event->source OR $event->source->id() != $this->owner()->id()) {
            return [];
        }

        return [new GiveEffect($event->character, FreezeEffect::class)];
    }
}