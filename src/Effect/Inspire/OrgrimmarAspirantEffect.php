<?php

namespace Tiixstone\Effect\Inspire;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect\InspireEffect;
use Tiixstone\Effect\AttackChangerEffect;

class OrgrimmarAspirantEffect extends InspireEffect
{
    /**
     * @return array
     */
    public function inspire(Game $game): array
    {
        if(!$this->owner->getPlayer()->hero->hasWeapon()) {
            return [];
        }

        return [new Block\GiveEffect($this->owner()->getPlayer()->hero->weapon, new AttackChangerEffect(1))];
    }
}