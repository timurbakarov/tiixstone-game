<?php

namespace Tiixstone\Effect\Inspire;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect\InspireEffect;
use Tiixstone\Effect\AttackChangerEffect;

class LowlySquireEffect extends InspireEffect
{
    /**
     * @return array
     */
    public function inspire(Game $game): array
    {
        return [new Block\GiveEffect($this->owner(), new AttackChangerEffect(1))];
    }
}