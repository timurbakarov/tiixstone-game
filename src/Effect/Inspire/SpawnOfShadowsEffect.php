<?php

namespace Tiixstone\Effect\Inspire;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect\InspireEffect;

class SpawnOfShadowsEffect extends InspireEffect
{
    /**
     * @return array
     */
    public function inspire(Game $game): array
    {
        return [
            new Block\TakeDamage($game->idlePlayer()->hero, 4, $this->owner),
            new Block\TakeDamage($game->currentPlayer()->hero, 4, $this->owner),
        ];
    }
}