<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Effect;

/**
 * Class TauntEffect
 * @package Tiixstone\Effect
 */
class TauntEffect extends Effect implements Targetable
{
    /**
     * @param Game $game
     * @param Card|Card\Character $target
     * @return mixed
     */
    public function apply(Game $game, Card $target)
    {
        if($game->effects->cardHasEffect($target, TauntEffect::class)) {
            $target->addEffect('taunt');
        }
    }
}