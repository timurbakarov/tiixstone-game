<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Character;
use Tiixstone\Card\Attackable;

class OneHealthEffect extends Effect implements Effect\Changer\HealthChanger, BoardEffect
{
    /**
     * @param Game $game
     * @param Attackable $character
     * @param int $attackRate
     * @return int
     */
    public function changeHealth(Game $game, Character $character, int $health): int
    {
        return $this->owner()->isSelf($character) ? $health + 1 : $health;
    }
}