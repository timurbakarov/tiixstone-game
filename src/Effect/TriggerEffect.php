<?php

namespace Tiixstone\Effect;

use Tiixstone\Block\ClosureBlock;
use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;

abstract class TriggerEffect extends Effect
{
    /**
     * @return string
     */
    abstract public function triggerAtEvent() : string;

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return bool
     */
    abstract public function condition(Game $game, Card $card, Event $event) : bool;

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    abstract public function action(Game $game, Card $card, Event $event) : array;

    /**
     * @return array
     */
    final public function events()
    {
        return [
            $this->triggerAtEvent() => 'triggerAt',
        ];
    }

    /**
     * @param Game $game
     * @param Event $event
     * @return array
     */
    final public function triggerAt(Game $game, Event $event)
    {
        if(!$this->condition($game, $this->owner(), $event)) {
            return [];
        }

        return array_merge([$this->logBlock()], $this->action($game, $this->owner(), $event), $this->afterActionBlocks($game));
    }

    /**
     * @return ClosureBlock
     */
    private function logBlock()
    {
        return new ClosureBlock(function(Game $game) {
            $game->logBlock('trigger-effect', ['card' => $this->owner()]);

            return [];
        });
    }

    /**
     * @return array
     */
    protected function afterActionBlocks(Game $game) : array
    {
        return [];
    }
}