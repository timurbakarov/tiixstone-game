<?php

namespace Tiixstone\Effect;

use Tiixstone\Block\Effect\RemoveEffect;
use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Condition;
use Tiixstone\AttackCondition;
use Tiixstone\Card\Attackable;
use Tiixstone\Event\BeforeEndTurn;

class RushEffect extends ChargeEffect implements Effect\Changer\AttackConditionChanger
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            BeforeEndTurn::class => 'beforeEndTurn',
        ];
    }

    /**
     * @param Game $game
     * @param BeforeEndTurn $event
     * @return array
     */
    public function beforeEndTurn(Game $game, BeforeEndTurn $event)
    {
        return [new RemoveEffect([$this])];
    }

    /**
     * @param Game $game
     * @param Attackable $character
     * @return AttackCondition
     */
    public function changeAttackCondition(Game $game, Attackable $character): AttackCondition
    {
        return new AttackCondition([new Condition\Target\Minion]);
    }

    /**
     * @param Game $game
     * @param Attackable $character
     * @return bool
     */
    public function isTarget(Game $game, Attackable $character): bool
    {
        return $this->targetCondition->condition($game, $this->owner(), $character);
    }
}