<?php

namespace Tiixstone\Effect;

use Tiixstone\Block\Effect\RemoveEffect;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;
use Tiixstone\Block\Trigger;

class DefileEffect extends Effect
{
    /**
     * @var int
     */
    private $maxLoops = 15;

    /**
     * @var int
     */
    private $currentLoop = 1;

    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\AfterPlaySpell::class => 'afterPlaySpell',
        ];
    }

    /**
     * @param Game $game
     * @param Event\AfterPlaySpell $afterPlaySpell
     * @return array
     */
    public function afterPlaySpell(Game $game, Event\AfterPlaySpell $afterPlaySpell)
    {
        $blocks = [];

        if($this->currentLoop < $this->maxLoops) {
            foreach($game->targetManager->allMinions($game) as $minion) {
                var_dump(get_class($minion), $minion->isDestroyed());

                if ($minion->isDestroyed()) {
                    $blocks = array_merge($blocks, $game->resolver->afterSequenceBlocks());

                    $blocks[] = $afterPlaySpell->spell->cast($game);
                    $blocks[] = new Trigger(new Event\AfterPlaySpell($afterPlaySpell->spell));
                }
            }

            $this->currentLoop++;

            var_dump('-----------------------------------------');
        }

        $blocks[] = new RemoveEffect([$this]);

        return $blocks;
    }
}