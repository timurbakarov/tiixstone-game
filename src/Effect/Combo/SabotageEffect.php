<?php

namespace Tiixstone\Effect\Combo;

use Tiixstone\Block\MarkDestroyed;
use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Game;

class SabotageEffect extends Effect\ComboEffect
{
    /**
     * @return array
     */
    public function combo(Game $game, Card $owner, Card\Character $target = NULL): Block
    {
        $blocks[] = $this->destroyRandomMinion($game, $owner);

        $opponentPlayer = $this->owner()->getPlayer()->getOpponent($game);

        if($opponentPlayer->hero->hasWeapon()) {
            $blocks[] = new Block\MarkDestroyed($opponentPlayer->hero->weapon);
        }

        return new Block\AggregateBlock($blocks);
    }

    /**
     * @param Game $game
     * @param Card $owner
     * @return array|MarkDestroyed
     */
    public function destroyRandomMinion(Game $game, Card $owner)
    {
        $targets = $game->targetManager->allEnemyMinions($game, $owner->getPlayer());

        if(!$targets) {
            return [];
        }

        $target = $game->RNG->randomFromArray($targets);

        return new MarkDestroyed($target);
    }
}