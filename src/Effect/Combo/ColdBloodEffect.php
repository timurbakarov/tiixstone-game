<?php

namespace Tiixstone\Effect\Combo;

use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Game;

class ColdBloodEffect extends Effect\ComboEffect
{
    /**
     * @return array
     */
    public function combo(Game $game, Card $owner, Card\Character $target = NULL): Block
    {
        return new Block\GiveEffect($target, new Effect\AttackChangerEffect(4));
    }
}