<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;
use Tiixstone\Exception;

class EnragedEffect extends Effect
{
    /**
     * @var array
     */
    private $enragedEffects;

    /**
     * @var bool
     */
    private $enragedApplied = false;

    public function __construct(array $enragedEffects)
    {
        /** @var Effect $enragedEffect */
        foreach($enragedEffects as $enragedEffect) {
            $enragedEffect = is_object($enragedEffect) ? $enragedEffect : new $enragedEffect;

            if(!$enragedEffect instanceof Effect) {
                throw new Exception("Effect should instance of Tiixstone\Effect. Instance of ". get_class($enragedEffect) . " given");
            }

            $this->enragedEffects[] = $enragedEffect;
        }
    }

    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\BeforeMove::class => 'beforeMove',
            Event\AfterTakeDamage::class => 'afterTakeDamage',
            Event\AfterHeal::class => 'afterHeal',
        ];
    }

    /**
     * @param Game $game
     * @param Event\BeforeMove $event
     * @return array
     */
    public function beforeMove(Game $game, Event\BeforeMove $event)
    {
        $this->checkEnrage($game);

        return [];
    }

    /**
     * @param Game $game
     * @param Event\AfterTakeDamage $event
     * @return array
     */
    public function afterTakeDamage(Game $game, Event\AfterTakeDamage $event)
    {
        if($event->character->isSelf($this->owner())) {
            $this->checkEnrage($game);
        }

        return [];
    }

    /**
     * @param Game $game
     * @param Event\AfterHeal $event
     * @return array
     */
    public function afterHeal(Game $game, Event\AfterHeal $event)
    {
        if($event->character->isSelf($this->owner())) {
            $this->checkEnrage($game);
        }

        return [];
    }

    /**
     * @return bool
     */
    private function isEnraged() : bool
    {
        return $this->owner()->health->isDamaged();
    }

    /**
     * @param Game $game
     */
    private function applyEnragedEffects(Game $game)
    {
        $game->effects->addMany($game, $this->owner(), $this->enragedEffects);

        $this->enragedApplied = true;
    }

    /**
     * @param Game $game
     */
    private function removeEnragedEffects(Game $game)
    {
        foreach($this->enragedEffects as $enragedEffect) {
            $game->effects->removeByEffect($enragedEffect);
        }

        $this->enragedApplied = false;
    }

    /**
     * @param Game $game
     */
    public function checkEnrage(Game $game)
    {
        if(!$this->isEnraged() AND $this->enragedApplied) {
            $this->removeEnragedEffects($game);
        } else if($this->isEnraged() AND !$this->enragedApplied) {
            $this->applyEnragedEffects($game);
        }
    }
}