<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;

class DivineShieldEffect extends Effect implements Effect\Changer\BlockChanger
{
    /**
     * @param Game $game
     * @param Block $block
     * @return Block
     */
    public function changeBlock(Game $game, Block $block): Block
    {
        if($block instanceof Block\TakeDamage AND $this->owner()->isSelf($block->character())) {
            return new Block\PopupDivineShield($block->character());
        }

        return $block;
    }
}