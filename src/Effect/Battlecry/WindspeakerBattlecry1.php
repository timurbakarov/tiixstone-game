<?php

namespace Tiixstone\Effect\Battlecry;

use Tiixstone\Game;
use Tiixstone\Card\Minion;
use Tiixstone\Card\Character;
use Tiixstone\Card\Attackable;
use Tiixstone\Event\BeforeSummon;
use Tiixstone\Effect\WindfuryEffect;
use Tiixstone\Effect\BattlecryEffect1;

class WindspeakerBattlecry1 extends BattlecryEffect1
{
    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = null, Character $target = null): array
    {
        $game->logBlock('windspeaker-battlecry');

        $game->effects->add($game, $target, WindfuryEffect::class);

        return [];
    }
}