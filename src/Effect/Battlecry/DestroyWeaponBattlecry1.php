<?php

namespace Tiixstone\Effect\Battlecry;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Minion;
use Tiixstone\Card\Character;
use Tiixstone\Card\Attackable;
use Tiixstone\Event\BeforeSummon;
use Tiixstone\Block\Weapon\DestroyWeapon;

class DestroyWeaponBattlecry1 extends Effect\BattlecryEffect1
{
    /**
     * @param Game $game
     * @param BeforeSummon $event
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = null, Character $target = null) : array
    {
        if(!$this->owner()->isSelf($attackable)) {
            return [];
        }

        $weapon = null;

        $blocks = [];
        if($game->idlePlayer()->hero->hasWeapon()) {
            $blocks[] = new DestroyWeapon($game->idlePlayer()->hero);
        }

        $game->logBlock('destroy-weapon-battlecry', ['minion' => $attackable, 'weapon' => $weapon]);

        return $blocks;
    }
}