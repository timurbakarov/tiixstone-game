<?php

namespace Tiixstone\Effect\Battlecry;

use Tiixstone\Cards\Item\MurlocScout;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Effect\BattlecryEffect1;
use Tiixstone\Event\BeforePlayMinion;
use Tiixstone\Game;

class MurlocTidehunterBattlecry1 extends BattlecryEffect1
{
    /**
     * @param Game $game
     * @param BeforePlayMinion $event
     * @return array
     */
    public function battlecry(Game $game, BeforePlayMinion $event): array
    {
        return [new SummonMinion($game->currentPlayer(), new MurlocScout(), $event->positionMinion)];
    }
}