<?php

namespace Tiixstone\Effect\Battlecry;

use Tiixstone\Cards\Item\Boar;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Card\Attackable;
use Tiixstone\Card\Character;
use Tiixstone\Card\Minion;
use Tiixstone\Effect\BattlecryEffect1;
use Tiixstone\Event\BeforeSummon;
use Tiixstone\Game;

class RazorfenHunterBattlecry1 extends BattlecryEffect1
{
    /**
     * @param Game $game
     * @param BeforeSummon $event
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = null, Character $target = null): array
    {
        $game->logBlock('razorfen-hunter-battlecry');

        $nextMinion = $game->currentPlayer()->board->next($attackable->id());

        return [new SummonMinion($game->currentPlayer(), new Boar(), $nextMinion ? $nextMinion : null)];
    }
}