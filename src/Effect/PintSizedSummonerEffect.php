<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;

class PintSizedSummonerEffect extends Effect implements Effect\Changer\ManaCostChanger
{
    /**
     * @var bool
     */
    private $wasUsed = false;

    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\AfterPlayMinion::class => 'afterPlayMinion',
            Event\TurnStarted::class => 'turnStarted',
        ];
    }

    /**
     * @param Game $game
     * @param Event\AfterPlayMinion $event
     * @return array
     */
    public function afterPlayMinion(Game $game, Event\AfterPlayMinion $event)
    {
        if($this->owner()->getPlayer()->isCurrent($game)) {
            $this->wasUsed = true;
        }

        return [];
    }

    /**
     * @param Game $game
     * @param Event\TurnStarted $event
     * @return array
     */
    public function turnStarted(Game $game, Event\TurnStarted $event)
    {
        if($this->owner()->getPlayer()->isCurrent($game)) {
            $this->wasUsed = false;
        }

        return [];
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param int $cost
     * @return int
     */
    public function changeManaCost(Game $game, Card $card, int $cost): int
    {
        if($this->wasUsed) {
            return $cost;
        }

        if(!$this->owner()->getPlayer()->hand->has($card->id())) {
            return $cost;
        }

        if(!$card instanceof Card\Minion) {
            return $cost;
        }

        return $cost - 1;
    }
}