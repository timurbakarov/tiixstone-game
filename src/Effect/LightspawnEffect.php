<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Attackable;

class LightspawnEffect extends Aura implements Effect\Changer\AttackChanger
{
    /**
     * @param Game $game
     * @param Attackable $character
     * @param int $attackRate
     * @return int
     */
    public function changeAttack(Game $game, Attackable $character, int $attackRate): int
    {
        return $character->isSelf($this->owner()) ? $character->health->total() : $attackRate;
    }
}