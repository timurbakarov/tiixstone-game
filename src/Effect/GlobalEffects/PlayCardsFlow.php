<?php

namespace Tiixstone\Effect\GlobalEffects;

use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;
use Tiixstone\Condition;
use Tiixstone\Exception;

class PlayCardsFlow extends Effect
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\AfterPlayCard::class => 'afterPlayCard',
            Event\ValidatePlayCard::class => 'validatePlayCard',
        ];
    }

    /**
     * @param Game $game
     * @param Event\AfterPlayCard $event
     * @return array
     */
    public function afterPlayCard(Game $game, Event\AfterPlayCard $event)
    {
        $game->graveyard->addPlayedCard($game, $event->card);

        return [];
    }

    /**
     * @param Game $game
     * @param Event\ValidatePlayCard $event
     * @return array
     * @throws Exception
     */
    public function validatePlayCard(Game $game, Event\ValidatePlayCard $event)
    {
        $playCardConditions = new Condition\PlayConditions();

        $success = $playCardConditions->isSatisfiedBy($game, $event->card, $event->target);

        if(!$success) {
            throw new Exception($playCardConditions->errorMessage());
        }

        return [];
    }
}