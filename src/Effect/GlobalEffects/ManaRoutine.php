<?php

namespace Tiixstone\Effect\GlobalEffects;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Exception;

class ManaRoutine extends Effect
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\TurnStarted::class => 'turnStarted',
            Event\BeforePlayCard::class => 'beforePlayCard',
            Event\ValidatePlayCard::class => 'validatePlayCard',
            Event\ValidateUseHeroPower::class => 'validateUseHeroPower',
            Event\BeforeUseHeroPower::class => 'beforeUseHeroPower',
        ];
    }

    /**
     * @param Game $game
     * @param Event\BeforeUseHeroPower $event
     * @return array
     */
    public function beforeUseHeroPower(Game $game, Event\BeforeUseHeroPower $event)
    {
        return [new Block\ReduceMana($game->currentPlayer(), $event->heroPower->cost->total())];
    }

    /**
     * @param Game $game
     * @param Event\ValidatePlayCard $event
     * @return array
     * @throws Exception
     */
    public function validatePlayCard(Game $game, Event\ValidatePlayCard $event)
    {
        if(!$this->playerHasEnoughMana($game, $event->card)) {
            throw new Exception(
                sprintf("Player does not have enough mana"),
                Exception::PLAYER_DOESNT_HAVE_ENOUGH_MANA_TO_PLAY_CARD
            );
        }

        return [];
    }

    /**
     * @param Game $game
     * @param Event\ValidateUseHeroPower $event
     * @return array
     * @throws Exception
     */
    public function validateUseHeroPower(Game $game, Event\ValidateUseHeroPower $event)
    {
        if(!$this->playerHasEnoughMana($game, $event->heroPower)) {
            throw new Exception(
                sprintf("Player does not have enough mana"),
                Exception::PLAYER_DOESNT_HAVE_ENOUGH_MANA_TO_PLAY_CARD
            );
        }

        return [];
    }

    /**
     * @param Game $game
     * @param Event\BeforePlayCard $event
     * @return array
     */
    public function beforePlayCard(Game $game, Event\BeforePlayCard $event)
    {
        return [new Block\ReduceMana($game->currentPlayer(), $event->card->cost->total())];
    }

    /**
     * @param Game $game
     * @return array
     */
    public function turnStarted(Game $game)
    {
        return $this->increaseMana($game);
    }

    /**
     * @param Game $game
     * @return array
     */
    private function increaseMana(Game $game)
    {
        $amount = $game->currentPlayer()->maximumMana() + 1;

        if($amount > $game->settings->playerMaximumManaLimit()) {
            $amount = $game->settings->playerMaximumManaLimit();
        }

        return [
            new Block\SetManaCrystals($game->currentPlayer(), $amount),
            new Block\RefillMana($game->currentPlayer()),
        ];
    }

    /**
     * @param Game $game
     * @param Card $card
     * @return bool
     */
    private function playerHasEnoughMana(Game $game, Card $card)
    {
        return $game->currentPlayer()->availableMana() >= $card->cost->total();
    }
}