<?php

namespace Tiixstone\Effect\GlobalEffects;

use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Block;
use Tiixstone\Effect;

class EchoGlobalEffect extends Effect
{
    /**
     * @var array
     */
	private $playedEchoCardsIds = [];

    /**
     * @var
     */
	private $copyCard;

    /**
     * @return array
     */
	public function events()
	{
		return [
			Event\BeforePlayCard::class => 'beforePlayCard',
			Event\AfterPlayCard::class => 'afterPlayCard',
			Event\BeforeEndTurn::class => 'beforeEndTurn',
		];
	}

    /**
     * @param Game $game
     * @param Event\BeforePlayCard $event
     * @return array
     */
	public function beforePlayCard(Game $game, Event\BeforePlayCard $event)
	{
		if(!$game->effects->cardHasEffect($event->card, Effect\EchoEffect::class)) {
			return [];
		}

		unset($this->playedEchoCardsIds[$event->card->id()]);

		$this->copyCard = $event->card->copy();

		return [];
	}

    /**
     * @param Game $game
     * @param Event\AfterPlayCard $event
     * @return array
     */
	public function afterPlayCard(Game $game, Event\AfterPlayCard $event)
	{
		if(!$this->copyCard) {
			return [];
		}

		$this->playedEchoCardsIds[$this->copyCard->id()] = $this->copyCard;

		$copyCard = $this->copyCard;

		$this->copyCard = null;

		return [new Block\PutCardInHand($game->currentPlayer(), $copyCard)];
	}

    /**
     * @param Game $game
     * @param Event\BeforeEndTurn $event
     * @return array
     */
	public function beforeEndTurn(Game $game, Event\BeforeEndTurn $event)
	{
		$blocks = [];
		foreach($this->playedEchoCardsIds as $card) {
		    if($game->currentPlayer()->hand->has($card->id())) {
                $blocks[] = new Block\RetrieveCardFromHand($game->currentPlayer(), $card);
            }
		}

		return $blocks;
	}
}
