<?php

namespace Tiixstone\Effect\GlobalEffects;

use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;

class EffectsFlow extends Effect
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\AfterDestroyMinion::class => 'afterDestroyMinion',
            Event\AfterPlaySpell::class => 'afterPlaySpell',
        ];
    }

    /**
     * @param Game $game
     * @param Event\AfterDestroyMinion $event
     * @return array
     */
    public function afterDestroyMinion(Game $game, Event\AfterDestroyMinion $event)
    {
        $game->effects->removeByCard($event->minion);

        return [];
    }

    /**
     * @param Game $game
     * @param Event\AfterPlaySpell $event
     * @return array
     */
    public function afterPlaySpell(Game $game, Event\AfterPlaySpell $event)
    {
        //$game->effects->removeByCard($event->spell);

        return [];
    }
}