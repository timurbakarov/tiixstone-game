<?php

namespace Tiixstone\Effect\GlobalEffects;

use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;
use Tiixstone\Card\Alias\TheCoin;
use Tiixstone\Block\DrawCard;
use Tiixstone\Block\PutCardInHand;

class CardsDrawingRoutine extends Effect
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\GameStarted::class => 'gameStarted',
            Event\TurnStarted::class => 'turnStarted',
        ];
    }

    /**
     * @param Game $game
     * @return array
     */
    public function gameStarted(Game $game)
    {
        return [
            new DrawCard($game->currentPlayer()),
            new DrawCard($game->currentPlayer()),
            new DrawCard($game->currentPlayer()),

            new DrawCard($game->idlePlayer()),
            new DrawCard($game->idlePlayer()),
            new DrawCard($game->idlePlayer()),
            new DrawCard($game->idlePlayer()),

            new PutCardInHand($game->idlePlayer(), TheCoin::create()),
        ];
    }

    /**
     * @param Game $game
     * @return array
     */
    public function turnStarted(Game $game)
    {
        return [
            new DrawCard($game->currentPlayer()),
        ];
    }
}