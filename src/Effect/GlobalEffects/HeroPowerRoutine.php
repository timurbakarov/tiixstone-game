<?php

namespace Tiixstone\Effect\GlobalEffects;

use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;
use Tiixstone\Exception;
use Tiixstone\Card\Hero;
use Tiixstone\Card\Power;

class HeroPowerRoutine extends Effect
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\TurnStarted::class => 'turnStarted',
            Event\ValidateUseHeroPower::class => 'validateUseHeroPower',
            Event\BeforeUseHeroPower::class => 'beforeUseHeroPower',
        ];
    }

    /**
     * @param Game $game
     * @param Event\BeforeUseHeroPower $event
     * @return array
     */
    public function beforeUseHeroPower(Game $game, Event\BeforeUseHeroPower $event)
    {
        $event->heroPower->setUsedThisTurn(true);

        return [];
    }

    /**
     * @param Game $game
     * @return array
     */
    public function turnStarted(Game $game)
    {
        $game->currentPlayer()->hero->power->setUsedThisTurn(false);

        return [];
    }

    /**
     * @param Game $game
     * @param Event\ValidateUseHeroPower $event
     * @return array
     */
    public function validateUseHeroPower(Game $game, Event\ValidateUseHeroPower $event)
    {
        if($event->heroPower->usedThisTurn()) {
            throw new Exception(sprintf("Can not use hero power twice a turn"));
        }

        $playCondition = $event->heroPower->playCondition();

        $result = $playCondition->check($game, $event->heroPower, $event->target);

        if($result->hasError()) {
            throw new Exception($result->message());
        }

        return [];
    }
}