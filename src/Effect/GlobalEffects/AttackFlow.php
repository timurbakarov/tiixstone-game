<?php

namespace Tiixstone\Effect\GlobalEffects;

use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;
use Tiixstone\Condition;
use Tiixstone\Exception;
use Tiixstone\Card\Minion;

class AttackFlow extends Effect
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\TurnStarted::class => 'turnStarted',
            Event\AfterAttack::class => 'afterAttack',
            Event\AfterSummon::class => 'afterSummon',
            Event\ValidateAttack::class => 'validateAttack',
            Event\AfterDestroyMinion::class => 'afterDestroyMinion',
            Event\BeforeDestroyMinion::class => 'beforeDestroyMinion',
        ];
    }

    /**
     * @param Game $game
     * @param Event\ValidateAttack $event
     * @return array
     * @throws Exception
     */
    public function validateAttack(Game $game, Event\ValidateAttack $event)
    {
        $attackCondition = new Condition\AttackConditions();

        $success = $attackCondition->isSatisfiedBy($game, $event->attacker, $event->defender);

        if(!$success) {
            throw new Exception($attackCondition->errorMessage());
        }

        return [];
    }

    /**
     * @param Game $game
     * @param Event\TurnStarted $event
     * @return array
     */
    public function turnStarted(Game $game, Event\TurnStarted $event)
    {
        $game->attackTracker->clear();

        foreach($game->targetManager->allPlayerCharacters($game, $game->currentPlayer()) as $character) {
            if($character instanceof Minion) {
                $game->attackTracker->setJustSummoned($character, false);
            }

            $game->attackTracker->resetAttackTimes($character, 0);
        }

        return [];
    }

    /**
     * @param Game $game
     * @param Event\AfterAttack $event
     * @return array
     */
    public function afterAttack(Game $game, Event\AfterAttack $event)
    {
        $game->attackTracker->incrementAttackTimes($event->attacker);

        return [];
    }

    /**
     * @param Game $game
     * @param Event\AfterSummon $event
     * @return array
     */
    public function afterSummon(Game $game, Event\AfterSummon $event)
    {
        $game->attackTracker->setJustSummoned($event->minion, true);
        $game->attackTracker->resetAttackTimes($event->minion);

        return [];
    }

    /**
     * @param Game $game
     * @param Event\AfterDestroyMinion $event
     * @return array
     */
    public function afterDestroyMinion(Game $game, Event\AfterDestroyMinion $event)
    {
        $game->effects->removeByCard($event->minion);

        return [];
    }

    /**
     * @param Game $game
     * @param Event\BeforeDestroyMinion $event
     * @return array
     */
    public function beforeDestroyMinion(Game $game, Event\BeforeDestroyMinion $event)
    {
        $game->logBlock('minion-destroyed', ['minion' => $event->minion]);

        return [];
    }
}