<?php

namespace Tiixstone\Effect\GlobalEffects;

use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;

class WeaponRoutine extends Effect
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\BeforeEquipWeapon::class => 'beforeEquipWeapon',
        ];
    }

    /**
     * @param Game $game
     * @param Event\BeforeEquipWeapon $event
     * @return array
     */
    public function beforeEquipWeapon(Game $game, Event\BeforeEquipWeapon $event)
    {
        if($event->player->hero->hasWeapon()) {
            return $event->player->hero->weapon->destroy($game);
        }

        return [];
    }
}