<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Character;

class AuraHealthChangerEffect extends Aura implements Effect\Changer\HealthChanger
{
    /**
     * @var int
     */
    private $value;

    /**
     * @var Target
     */
	private $target;

    /**
     * @var bool
     */
	private $fixed;

    public function __construct(int $value, Target $target, bool $fixed = false)
    {
        $this->value = $value;
        $this->target = new Effect\Target\Aggregate([$target, new Effect\Target\Battlefield()]);
		$this->fixed = $fixed;
    }

    /**
     * @param Game $game
     * @param Character $character
     * @param int $health
     * @return int
     */
    public function changeHealth(Game $game, Character $character, int $health): int
    {
		if($this->target->condition($game, $this->owner(), $character)) {
			return $this->fixed ? $this->value - $character->health->current->get() : $health + $this->value;
		} else {
			return $health;
		}
    }
}