<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Effect;

class WindfuryEffect extends Effect implements Targetable
{
    /**
     * @var Target
     */
    private $target;

    /**
     * WindfuryEffect constructor.
     * @param Target|NULL $target
     */
    public function __construct(Target $target = null)
    {
        $this->target = $target ?: new Effect\Target\SelfTarget();
    }

    /**
     * @param Game $game
     * @param Card|Card\Minion $target
     * @return mixed|void
     */
    public function apply(Game $game, Card $target)
    {
        if($this->target->condition($game, $this->owner, $target)) {
            $target->addEffect(WindfuryEffect::class);
        }
    }
}