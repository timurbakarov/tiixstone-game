<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Event;
use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Block\MarkDestroyed;

class DoomsayerEffect extends Effect\Trigger\StartOfYourTurn
{
    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        return array_map(function(Card\Minion $minion) {
            return new MarkDestroyed($minion);
        }, $game->targetManager->allMinions($game));
    }
}