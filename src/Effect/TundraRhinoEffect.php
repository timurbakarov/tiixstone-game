<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Minion;
use Tiixstone\Event\BeforeMove;
use Tiixstone\Event\AfterDestroyMinion;

class TundraRhinoEffect extends Effect
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            BeforeMove::class => 'BeforeMove',
            AfterDestroyMinion::class => 'afterDestroy',
        ];
    }

    /**
     * @param Game $game
     * @param BeforeMove $event
     * @return array
     */
    public function BeforeMove(Game $game, BeforeMove $event)
    {
        $player = $this->owner()->getPlayer();

        if(!$player->isCurrent($game)) {
            return [];
        }

        /** @var Minion $minion */
        foreach($player->board->all() as $minion) {
            if($minion->race()->isBeast() AND $game->attackTracker->attackTimes($minion) == 0) {
                $game->attackTracker->setCanAttackAnyway($minion, true);
            }
        }

        return [];
    }

    /**
     * @param Game $game
     * @param AfterDestroyMinion $event
     * @return array
     */
    public function afterDestroy(Game $game, AfterDestroyMinion $event)
    {
//        if($this->owner()->isSelf($event->minion)) {
//            $game->effects->removeByEffect($this);
//        }

        return [];
    }
}