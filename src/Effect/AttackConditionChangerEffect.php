<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Condition;
use Tiixstone\AttackCondition;
use Tiixstone\Card\Attackable;

class AttackConditionChangerEffect extends Effect implements Effect\Changer\AttackConditionChanger
{
    /**
     * @var Condition[]
     */
    private $conditions;
    /**
     * @var Target
     */
    private $target;

    public function __construct(array $conditions, Target $target)
    {
        $this->conditions = $conditions;
        $this->target = $target;
    }

    /**
     * @param Game $game
     * @param Attackable $character
     * @return AttackCondition
     */
    public function changeAttackCondition(Game $game, Attackable $character): AttackCondition
    {
        $conditions = $character->attackCondition()->conditions();

        $conditions = array_merge($conditions, $this->conditions);

        return new AttackCondition($conditions);
    }

    /**
     * @param Game $game
     * @param Attackable $character
     * @return bool
     */
    public function isTarget(Game $game, Attackable $character): bool
    {
        return $this->target->condition($game, $this->owner, $character);
    }
}