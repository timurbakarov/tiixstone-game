<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Card\Minion;
use Tiixstone\Card\Alias\Hyena;
use Tiixstone\Block\SummonMinion;

class SavannahHighmaneDeathrattle extends DeathrattleEffect
{
    /**
     * @param Game $game
     * @param Minion $minion
     * @param Minion|NULL $nextMinion
     * @return mixed
     */
    public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL) : array
    {
        $game->logBlock('savannah-highmane-deathrattle');

        $hyena1 = Hyena::create();
        $hyena2 = Hyena::create();

        return [
            new SummonMinion($minion->getPlayer(), $hyena1, $nextMinion),
            new SummonMinion($minion->getPlayer(), $hyena2, $hyena1),
        ];
    }
}