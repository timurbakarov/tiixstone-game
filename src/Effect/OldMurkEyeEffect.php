<?php

namespace Tiixstone\Effect;

use Tiixstone\Card\Attackable;
use Tiixstone\Card\Minion;
use Tiixstone\Effect;
use Tiixstone\Game;
use Tiixstone\Race;

class OldMurkEyeEffect extends Effect implements Effect\Changer\AttackChanger
{
    /**
     * @param Game $game
     * @param Attackable $character
     * @param int $attackRate
     * @return int
     */
    public function changeAttack(Game $game, Attackable $character, int $attackRate): int
    {
        if(!$character->isSelf($this->owner())) {
            return $attackRate;
        }

        $murlocs = array_filter($game->targetManager->allMinions($game), function(Minion $minion) {
            if($minion->isSelf($this->owner())) {
                return false;
            }

            return $minion->race()->is(Race::murloc());
        });

        return $attackRate + count($murlocs);
    }
}