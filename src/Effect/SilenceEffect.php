<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Block\Effect\RemoveEffect;

/**
 * Class SilenceEffect
 * @package Tiixstone\Effect
 */
class SilenceEffect extends Effect
{
    /**
     * @param Game $game
     * @param Card $card
     * @return array
     */
    public function init(Game $game, Card $card) : array
    {
        $game->logBlock('silence-minion', ['minion' => $this->owner()]);

        $this->setCurrentHealthAfterSilence();

        return [new RemoveEffect($game->effects->getByCard($this->owner()), $this)];
    }

    /**
     *
     *
     * @return int
     */
    private function setCurrentHealthAfterSilence()
    {
        /** @var Card\Minion $minion */
        $minion = $this->owner();

        if($minion->health->current->get() <= 0) {
            $currentHealth = $minion->health->total() > $minion->health->maximum->get()
                ? $minion->health->maximum->get()
                : $minion->health->total();

            $minion->health->current->set($currentHealth);
        }
    }
}