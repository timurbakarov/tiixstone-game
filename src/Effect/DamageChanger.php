<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Card\Character;

interface DamageChanger
{
    /**
     * @param Game $game
     * @return int
     */
    public function changeDamage(Game $game, Character $target, int $damage) : int;
}