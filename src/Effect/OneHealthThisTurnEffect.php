<?php

namespace Tiixstone\Effect;

use Tiixstone\Card\Attackable;
use Tiixstone\Card\Character;
use Tiixstone\Effect;
use Tiixstone\Game;

class OneHealthThisTurnEffect extends Effect\Buff\OneTurnBuff implements Effect\Changer\HealthChanger, BoardEffect
{
    /**
     * @param Game $game
     * @param Attackable $character
     * @param int $attackRate
     * @return int
     */
    public function changeHealth(Game $game, Character $character, int $health): int
    {
        return $this->owner()->isSelf($character) ? $health + 1 : $health;
    }
}