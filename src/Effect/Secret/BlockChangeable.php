<?php

namespace Tiixstone\Effect\Secret;

use Tiixstone\Block;
use Tiixstone\Card\Secret;
use Tiixstone\Game;

interface BlockChangeable
{
    /**
     * @return string
     */
    public function triggerAt() : string;

    /**
     * @param Game $game
     * @param Block $block
     * @param Secret $secret
     * @return bool
     */
    public function condition(Game $game, Block $block, Secret $secret) : bool;

    /**
     * @param Game $game
     * @param Block $block
     * @param Secret $secret
     * @return array
     */
    public function action(Game $game, Block $block, Secret $secret) : array;
}