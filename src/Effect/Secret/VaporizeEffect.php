<?php

namespace Tiixstone\Effect\Secret;

use Tiixstone\Block;
use Tiixstone\Block\Combat;
use Tiixstone\Effect\Secret;
use Tiixstone\Event;
use Tiixstone\Game;

class VaporizeEffect extends Secret
{
    /**
     * @return string
     */
    public function triggerAt(): string
    {
        return Combat::class;
    }

    /**
     * Event or Block
     * @return string
     */
    public function triggerType(): string
    {
        return 'block';
    }

    /**
     * @param Game $game
     * @param Event|Block $entity
     * @param \Tiixstone\Card\Secret $secret
     * @return bool
     */
    public function condition(Game $game, $entity, \Tiixstone\Card\Secret $secret): bool
    {
        return false;
    }

    /**
     * @param Game $game
     * @param Event|Block $event
     * @param \Tiixstone\Card\Secret $secret
     * @return mixed
     */
    public function action(Game $game, $entity, \Tiixstone\Card\Secret $secret)
    {
        return [];
    }
}