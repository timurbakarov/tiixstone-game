<?php

namespace Tiixstone\Effect\Secret;

use Tiixstone\Block\SummonMinion;
use Tiixstone\Effect\Secret;
use Tiixstone\Event;
use Tiixstone\Game;

class MirrorEntityEffect extends Secret implements Eventable
{
    /**
     * @return string
     */
    public function triggerAt(): string
    {
        return Event\AfterPlayMinion::class;
    }

    /**
     * @param Game $game
     * @param Event|Event\AfterPlayMinion $event
     * @param \Tiixstone\Card\Secret $secret
     * @return bool
     */
    public function condition(Game $game, Event $event, \Tiixstone\Card\Secret $secret): bool
    {
        return $event->minion->getPlayer()->id() != $this->owner()->getPlayer()->id();
    }

    /**
     * @param Game $game
     * @param Event|Event\AfterPlayMinion $event
     * @param \Tiixstone\Card\Secret $secret
     * @return array
     */
    public function action(Game $game, Event $event, \Tiixstone\Card\Secret $secret): array
    {
        return [new SummonMinion($this->owner()->getPlayer(), $event->minion->copy())];
    }
}