<?php

namespace Tiixstone\Effect\Secret;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect\Secret;
use Tiixstone\Effect\ImmuneEffect;
use Tiixstone\Effect\Trigger\EndOfTheTurn;

class IceBlock extends Secret implements BlockChangeable
{
    /**
     * @return string
     */
    public function triggerAt(): string
    {
        return Block\TakeDamage::class;
    }

    /**
     * @param Game $game
     * @param Block|Block\TakeDamage $block
     * @param \Tiixstone\Card\Secret $secret
     * @return bool
     */
    public function condition(Game $game, Block $block, \Tiixstone\Card\Secret $secret): bool
    {
        $hero = $this->owner()->getPlayer()->hero;

        return $block->character()->isSelf($hero) && $hero->health->total() <= $block->amount();
    }

    /**
     * @param Game $game
     * @param Block $block
     * @param \Tiixstone\Card\Secret $secret
     * @return array
     */
    public function action(Game $game, Block $block, \Tiixstone\Card\Secret $secret): array
    {
        $removeEffect = new Block\Effect\RemoveEffect([$this]);
        $giveEffect = new Block\GiveEffect($this->owner()->getPlayer()->hero, new EndOfTheTurn(new ImmuneEffect));

        return [$giveEffect, $removeEffect];
    }
}