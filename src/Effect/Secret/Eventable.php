<?php

namespace Tiixstone\Effect\Secret;

use Tiixstone\Card\Secret;
use Tiixstone\Event;
use Tiixstone\Game;

interface Eventable
{
    /**
     * @return string
     */
    public function triggerAt() : string;

    /**
     * @param Game $game
     * @param Event $event
     * @param Secret $secret
     * @return bool
     */
    public function condition(Game $game, Event $event, Secret $secret) : bool;

    /**
     * @param Game $game
     * @param Event $event
     * @param Secret $secret
     * @return array
     */
    public function action(Game $game, Event $event, Secret $secret) : array;
}