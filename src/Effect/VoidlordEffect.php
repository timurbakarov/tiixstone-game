<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Card\Minion;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Card\Alias\Voidwalker;

class VoidlordEffect extends DeathrattleEffect
{
    /**
     * @param Game $game
     * @param Minion $minion
     * @param Minion|NULL $nextMinion
     * @return mixed
     */
    public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL) : array
    {
        $game->logBlock('voidlord-deathrattle');

        return [
            new SummonMinion($minion->getPlayer(), Voidwalker::create(), $nextMinion),
            new SummonMinion($minion->getPlayer(), Voidwalker::create(), $nextMinion),
            new SummonMinion($minion->getPlayer(), Voidwalker::create(), $nextMinion),
        ];
    }
}