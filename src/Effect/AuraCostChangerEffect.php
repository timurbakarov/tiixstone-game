<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Effect;

class AuraCostChangerEffect extends Aura implements Effect\Changer\ManaCostChanger
{
    /**
     * @var int
     */
    private $value;
	
	/**
     * @var Target
     */
	private $target;

    /**
     * @var bool
     */
	private $fixed;

    /**
     * @param int $value
     * @param Target|NULL $target
     * @param bool $fixed
     */
    public function __construct(int $value, Target $target = null, bool $fixed = false)
    {
        $this->value = $value;
		$this->target = new Effect\Target\Aggregate([$target, $this->defaultConditions()]);
		$this->fixed = $fixed;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param int $cost
     * @return int
     */
    public function changeManaCost(Game $game, Card $card, int $cost): int
    {
		if($this->target->condition($game, $this->owner(), $card)) {
			return $this->fixed ? $this->value - $card->cost->current->get() : $cost + $this->value;
		} else {
			return $cost;
		}
    }

    /**
     * @return Target\OrCondition
     */
    private function defaultConditions()
    {
        return new Effect\Target\OrCondition([new Effect\Target\Hand(), new Effect\Target\HeroPower()]);
    }
}