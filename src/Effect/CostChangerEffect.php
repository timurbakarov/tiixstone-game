<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Effect;

class CostChangerEffect extends Effect implements Effect\Changer\ManaCostChanger
{
    /**
     * @var int
     */
    private $value;

    /**
     * @var bool
     */
	private $fixed;

    public function __construct(int $value, bool $fixed = false)
    {
        $this->value = $value;
		$this->fixed = $fixed;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param int $cost
     * @return int
     */
    public function changeManaCost(Game $game, Card $card, int $cost): int
    {
		if($this->owner->isSelf($card)) {
			return $this->fixed ? $this->value - $card->cost->current->get() : $cost + $this->value;
		} else {
			return $cost;
		}
    }
}