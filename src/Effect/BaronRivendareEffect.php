<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;

class BaronRivendareEffect extends Effect implements Effect\Changer\BlockChanger
{
    /**
     * @param Game $game
     * @param Block|Block\Deathrattle $block
     * @return array
     */
    public function changeBlock(Game $game, Block $block): Block
    {
        if($block instanceof Block\Deathrattle) {
            if($block->minion->getPlayer()->id() != $this->owner()->getPlayer()->id()) {
                return $block;
            }

            return $this->doubleDeathrattle($game, $block);
        }

        return $block;
    }

    /**
     * @param Game $game
     * @param Block $block
     * @return Block
     */
    private function doubleDeathrattle(Game $game, Block $block) : Block
    {
        return new Block\AggregateBlock([
            new Block\ClosureBlock($this->deathrattleClosure($block)),
            new Block\ClosureBlock($this->logBlock()),
            new Block\ClosureBlock($this->deathrattleClosure($block)),
        ]);
    }

    /**
     * @return \Closure
     */
    private function logBlock()
    {
        return function(Game $game) {
            $game->logBlock('baron-rivendare-effect', ['char' => $this->owner()]);

            return [];
        };
    }

    /**
     * @return \Closure
     */
    private function deathrattleClosure(Block $block)
    {
        return function(Game $game) use($block) {
            return $block->run($game);
        };
    }
}