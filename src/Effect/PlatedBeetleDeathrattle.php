<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Minion;

class PlatedBeetleDeathrattle extends DeathrattleEffect
{
    /**
     * @param Game $game
     * @param Minion $minion
     * @param Minion|NULL $nextMinion
     * @return array
     */
    public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL): array
    {
        return [
			new Block\IncreaseArmor($minion->getPlayer()->hero, 3),
		];
    }
}