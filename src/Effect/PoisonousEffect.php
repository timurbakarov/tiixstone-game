<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Block\MarkDestroyed;
use Tiixstone\Event\AfterTakeDamage;

class PoisonousEffect extends Effect
{
	public function events()
	{
		return [
			AfterTakeDamage::class => 'afterTakeDamage',
		];
	}
	
	public function afterTakeDamage(Game $game, AfterTakeDamage $event)
	{
		if($event->source AND $event->source->id() == $this->owner()->id() AND $event->character->isMinion()) {
			return [new MarkDestroyed($event->character)];
		}
		
		return [];
	}
}