<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Effect;
use Tiixstone\Game;

class SeaGiantEffect extends Effect implements Effect\Changer\ManaCostChanger
{
    /**
     * @param Game $game
     * @param Card $card
     * @param int $cost
     * @return int
     */
    public function changeManaCost(Game $game, Card $card, int $cost): int
    {
        if($card->isSelf($this->owner())) {
            return $cost - $game->player1->board->count() - $game->player2->board->count();
        }

        return $cost;
    }
}