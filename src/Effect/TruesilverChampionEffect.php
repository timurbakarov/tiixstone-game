<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Event\BeforeAttack;
use Tiixstone\Block\HealCharacter;

class TruesilverChampionEffect extends Effect
{
	public function events()
	{
		return [
			BeforeAttack::class => 'beforeAttack',
		];
	}
	
	public function beforeAttack(Game $game, BeforeAttack $event)
	{
		if($event->attacker->id() == $this->owner()->getPlayer()->hero->id()) {
			return [new HealCharacter($event->attacker, 2)];
		}
		
		return [];
	}
}