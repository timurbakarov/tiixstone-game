<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;
use Tiixstone\Card\Power;

abstract class InspireEffect extends Effect
{
    /**
     * @var bool
     */
    private $activated = false;

    /**
     * @var Power
     */
    private $heroPower;

    /**
     * @return array
     */
    abstract public function inspire(Game $game) : array;

    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\AfterUseHeroPower::class => 'afterUseHeroPower',
            Event\InspireEvent::class => 'inspireEvent',
        ];
    }

    /**
     * @param Game $game
     * @param Event\AfterUseHeroPower $event
     * @return array
     */
    public function afterUseHeroPower(Game $game, Event\AfterUseHeroPower $event)
    {
        $this->activated = true;

        $this->heroPower = $event->heroPower;

        return [];
    }

    /**
     * @param Game $game
     * @param Event\InspireEvent $event
     * @return array
     */
    public function inspireEvent(Game $game, Event\InspireEvent $event)
    {
        if(!$this->activated) {
            return [];
        }

        if($this->heroPower->getPlayer()->id() != $this->owner()->getPlayer()->id()) {
            return [];
        }

        $game->logBlock('inspire-effect', ['card' => $this->owner]);

        $this->activated = false;
        $this->heroPower = null;

        return $this->inspire($game);
    }
}