<?php

namespace Tiixstone\Effect;

use Tiixstone\Block\Effect\RemoveEffect;
use Tiixstone\Card;
use Tiixstone\Event\AfterMove;
use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Exception;
use Tiixstone\Action\ChooseOne;
use Tiixstone\Event\BeforeAction;

class ChooseCardEffect extends Effect
{
    /**
     * @var \Closure
     */
    private $closure;

    /**
     * @var Card[]
     */
    private $cards;

    /**
     * @var
     */
    private $leftBlocks;

    /**
     * @param \Closure $closure
     * @param Card[] ...$cards
     */
    public function __construct(\Closure $closure, Card ...$cards)
    {
        $this->closure = $closure;
        $this->cards = $cards;
    }

    /**
     * @return array
     */
    public function events()
    {
        return [
            BeforeAction::class => 'beforeAction',
            AfterMove::class => 'afterMove',
        ];
    }

    /**
     * @param Game $game
     * @param BeforeAction $event
     * @return array
     * @throws Exception
     */
    public function beforeAction(Game $game, BeforeAction $event)
    {
        if(!($event->action() instanceof ChooseOne)) {
            throw new Exception("Action should be 'Choose one'");
        }

        return [];
    }

    /**
     * @param Game $game
     * @param AfterMove $event
     * @return array
     */
    public function afterMove(Game $game, AfterMove $event)
    {
        return [
            new RemoveEffect([$this]),
            $this->leftBlocks(),
        ];
    }

    /**
     * @param Game $game
     * @param Card $card
     * @return array
     */
    public function init(Game $game, Card $card) : array
    {
        $game->cardsList->appendMany($this->cards);

        return [];
    }

    /**
     * @return \Closure
     */
    public function closure(): \Closure
    {
        return $this->closure;
    }

    /**
     * @return Card[]
     */
    public function cards()
    {
        return $this->cards;
    }

    /**
     * @return mixed
     */
    public function leftBlocks()
    {
        return $this->leftBlocks;
    }

    /**
     * @param $leftBlocks
     * @return $this
     */
    public function setLeftBlocks($leftBlocks)
    {
        $this->leftBlocks = $leftBlocks;

        return $this;
    }
}