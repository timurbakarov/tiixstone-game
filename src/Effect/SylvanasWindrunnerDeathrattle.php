<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Player;
use Tiixstone\Card\Minion;
use Tiixstone\Block\MindControl;

class SylvanasWindrunnerDeathrattle extends DeathrattleEffect
{
    /**
     * @param Game $game
     * @param Minion $minion
     * @param Minion|NULL $nextMinion
     * @return mixed
     */
    public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL) : array
    {
        if(!$this->owner()->isSelf($minion)) {
            return [];
        }

        $minion = $this->getRandomEnemyMinion($game, $minion->getPlayer());

        return $minion ? [new MindControl($minion)] : [];
    }

    /**
     * @param Game $game
     * @param Player $player
     * @return bool|mixed|Minion
     */
    private function getRandomEnemyMinion(Game $game, Player $player)
    {
        $enemyMinions = $game->targetManager->allEnemyMinions($game, $player);

        $enemyMinions = array_filter($enemyMinions, function(Minion $minion) {
            return !$minion->isDestroyed();
        });

        if(!$enemyMinions) {
            return false;
        }

        shuffle($enemyMinions);
        shuffle($enemyMinions);
        shuffle($enemyMinions);

        return array_shift($enemyMinions);
    }
}