<?php

namespace Tiixstone\Effect\Deathrattle;

use Tiixstone\Game;
use Tiixstone\Card\Minion;
use Tiixstone\Card\Alias\Slime;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Effect\DeathrattleEffect;

class SludgeBelcherDeathrattle extends DeathrattleEffect
{
    /**
     * @param Game $game
     * @param Minion $minion
     * @param Minion|NULL $nextMinion
     * @return mixed
     */
    public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL) : array
    {
        return [new SummonMinion($minion->getPlayer(), Slime::create(), $nextMinion)];
    }
}