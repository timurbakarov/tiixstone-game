<?php

namespace Tiixstone\Effect\Deathrattle;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Card\Minion;

class DeathwingDragonlordDeathrattle extends Effect\DeathrattleEffect
{
    /**
     * @param Game $game
     * @param Minion $minion
     * @param Minion|NULL $nextMinion
     * @return mixed
     */
    public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL) : array
    {
        $dragons = array_filter($minion->getPlayer()->hand->all(), function(Card $card) {
            /** @var Card|Minion $card */
            return $card->isMinion() && $card->race()->isDragon();
        });

        return array_map(function(Card $dragon) {
            return [
                new Block\RetrieveCardFromHand($dragon->getPlayer(), $dragon),
                new Block\SummonMinion($dragon->getPlayer(), $dragon),
            ];
        }, $dragons);
    }
}