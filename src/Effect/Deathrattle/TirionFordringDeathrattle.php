<?php

namespace Tiixstone\Effect\Deathrattle;

use Tiixstone\Game;
use Tiixstone\Card\Minion;
use Tiixstone\Card\Alias\Ashbringer;
use Tiixstone\Manager\TargetManager;
use Tiixstone\Block\DealSplashDamage;
use Tiixstone\Block\Weapon\EquipWeapon;
use Tiixstone\Effect\DeathrattleEffect;

class TirionFordringDeathrattle extends DeathrattleEffect
{
    /**
     * @param Game $game
     * @param Minion $minion
     * @param Minion|NULL $nextMinion
     * @return mixed
     */
    public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL) : array
    {
        return [new EquipWeapon($minion->getPlayer(), Ashbringer::create())];
    }
}