<?php

namespace Tiixstone\Effect\Deathrattle;

use Tiixstone\Game;
use Tiixstone\Card\Minion;
use Tiixstone\Block\ReturnMinionToHand;
use Tiixstone\Effect\DeathrattleEffect;

class AnubarAmbusherDeathrattle extends DeathrattleEffect
{
    /**
     * @param Game $game
     * @param Minion $minion
     * @param Minion|NULL $nextMinion
     * @return mixed
     */
    public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL) : array
    {
        $targets = array_map(function(Minion $item) use($minion) {
            if($item->isDestroyed()) {
                return [];
            }

            return $item;
        }, $game->targetManager->allPlayerMinions($game, $minion->getPlayer()));

        $targets = array_filter($targets);

        if(!$targets) {
            return [];
        }

        $target = $game->RNG->randomFromArray($targets);

        return [new ReturnMinionToHand($target)];
    }
}