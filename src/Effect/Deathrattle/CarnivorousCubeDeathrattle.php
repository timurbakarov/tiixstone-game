<?php

namespace Tiixstone\Effect\Deathrattle;

use Tiixstone\Game;
use Tiixstone\Card\Minion;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Effect\DeathrattleEffect;

class CarnivorousCubeDeathrattle extends DeathrattleEffect
{
    /**
     * @var
     */
    private $minionToResummon;

    /**
     * @param Minion|NULL $minion
     */
    public function __construct(Minion $minion = null)
    {
        $this->minionToResummon = $minion ? get_class($minion) : null;
    }

    /**
     * @param Game $game
     * @param Minion $minion
     * @param Minion|NULL $nextMinion
     * @return mixed
     */
    public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL): array
    {
        if(!$this->minionToResummon) {
            return [];
        }

        return [
            new SummonMinion($minion->getPlayer(), new $this->minionToResummon, $nextMinion),
            new SummonMinion($minion->getPlayer(), new $this->minionToResummon, $nextMinion),
        ];
    }
}