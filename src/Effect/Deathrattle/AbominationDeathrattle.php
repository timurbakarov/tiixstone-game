<?php

namespace Tiixstone\Effect\Deathrattle;

use Tiixstone\Game;
use Tiixstone\Card\Minion;
use Tiixstone\Manager\TargetManager;
use Tiixstone\Block\DealSplashDamage;
use Tiixstone\Effect\DeathrattleEffect;

class AbominationDeathrattle extends DeathrattleEffect
{
    /**
     * @param Game $game
     * @param Minion $minion
     * @param Minion|NULL $nextMinion
     * @return mixed
     */
    public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL) : array
    {
        return [new DealSplashDamage(TargetManager::ANY_CHARACTER, 2, $minion)];
    }
}