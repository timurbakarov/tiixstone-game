<?php

namespace Tiixstone\Effect\Deathrattle;

use Tiixstone\Block\DealSplashDamage;
use Tiixstone\Card\Minion;
use Tiixstone\Effect\DeathrattleEffect;
use Tiixstone\Game;
use Tiixstone\Manager\TargetManager;

class TentacleOfNZothDeathrattle extends DeathrattleEffect
{
    /**
     * @param Game $game
     * @param Minion $minion
     * @param Minion|NULL $nextMinion
     * @return mixed
     */
    public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL): array
    {
        return [new DealSplashDamage(TargetManager::ANY_MINION, 1, $this->owner())];
    }
}