<?php

namespace Tiixstone\Effect\Deathrattle;

use Tiixstone\Block\SummonMinion;
use Tiixstone\Card\Alias\DamagedGolem;
use Tiixstone\Card\Minion;
use Tiixstone\Effect\DeathrattleEffect;
use Tiixstone\Game;

class HarvestGolemDeathrattle extends DeathrattleEffect
{
    /**
     * @param Game $game
     * @param Minion $minion
     * @param Minion|NULL $nextMinion
     * @return mixed
     */
    public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL): array
    {
        return [new SummonMinion($minion->getPlayer(), DamagedGolem::create(), $nextMinion)];
    }
}