<?php

namespace Tiixstone\Effect\Deathrattle;

use Tiixstone\Game;
use Tiixstone\Card\Minion;
use Tiixstone\Block\TakeDamage;
use Tiixstone\Effect\DeathrattleEffect;

class LeperGnomeDeathrattle extends DeathrattleEffect
{
    /**
     * @param Game $game
     * @param Minion $minion
     * @param Minion|NULL $nextMinion
     * @return mixed
     */
    public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL) : array
    {
        return [new TakeDamage($minion->getPlayer()->getOpponent($game)->hero, 2, $minion)];
    }
}