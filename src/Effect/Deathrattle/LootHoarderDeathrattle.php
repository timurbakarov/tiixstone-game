<?php

namespace Tiixstone\Effect\Deathrattle;

use Tiixstone\Block\DrawCard;
use Tiixstone\Card\Minion;
use Tiixstone\Effect\DeathrattleEffect;
use Tiixstone\Game;

class LootHoarderDeathrattle extends DeathrattleEffect
{

    /**
     * @param Game $game
     * @param Minion $minion
     * @param Minion|NULL $nextMinion
     * @return mixed
     */
    public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL): array
    {
        return [new DrawCard($minion->getPlayer())];
    }
}