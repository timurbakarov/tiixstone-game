<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Event\AfterDiscard;
use Tiixstone\Manager\TargetManager;
use Tiixstone\Block\Damage\DealRandomDamage;

class FistOfJaraxxusEffect extends Effect
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            AfterDiscard::class => 'afterDiscard',
        ];
    }

    public function afterDiscard(Game $game, AfterDiscard $event)
    {
        if(!$event->card()->isSelf($this->owner())) {
            return [];
        }

        return [new DealRandomDamage(TargetManager::ANY_OPPONENT_CHARACTER, $game->withSpellDamage(4), $this->owner())];
    }
}