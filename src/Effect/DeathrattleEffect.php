<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Minion;

abstract class DeathrattleEffect extends Effect
{
    /**
     * @param Game $game
     * @param Minion $minion
     * @param Minion|NULL $nextMinion
     * @return mixed
     */
    abstract public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = null) : array;
}