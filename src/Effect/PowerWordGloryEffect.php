<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Player;
use Tiixstone\Event\BeforeAttack;
use Tiixstone\Block\HealCharacter;

class PowerWordGloryEffect extends Effect
{
    /**
     * @var Player
     */
    protected $player;

    public function __construct(Player $player)
    {
        $this->player = $player;
    }

    /**
     * @return array|string
     */
    public function events()
    {
        return [
            BeforeAttack::class => 'beforeAttack',
        ];
    }

    /**
     * @param Game $game
     * @param BeforeAttack $event
     * @return array
     */
    public function beforeAttack(Game $game, BeforeAttack $event)
    {
        if(!$event->attacker->isSelf($this->owner())) {
            return [];
        }

        return [new HealCharacter($this->player->hero, 4)];
    }
}