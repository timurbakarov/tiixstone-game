<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;
use Tiixstone\Exception;

class StealthEffect extends Effect implements Targetable
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\ValidateUseHeroPower::class => 'validateUseHeroPower',
            Event\AfterTakeDamage::class => 'afterTakeDamage',
        ];
    }

    /**
     * @param Game $game
     * @param Event\ValidateUseHeroPower $event
     * @return array
     * @throws Exception
     */
    public function validateUseHeroPower(Game $game, Event\ValidateUseHeroPower $event)
    {
        if(!$event->target) {
            return [];
        }

        if(!$this->owner->isSelf($event->target)) {
            return [];
        }

        if($event->heroPower->getPlayer()->id() == $this->owner->getPlayer()->id()) {
            return [];
        }

        throw new Exception('Нельзя использовать cилу героя на персонажа с маскировкой');
    }

    /**
     * @param Game $game
     * @param Event\AfterTakeDamage $event
     * @return array
     */
    public function afterTakeDamage(Game $game, Event\AfterTakeDamage $event)
    {
        if(!$event->source) {
            return [];
        }

        if(!$event->source->isSelf($this->owner())) {
            return [];
        }

        $game->effects->removeByEffect($this);

        return [];
    }

    /**
     * @param Game $game
     * @param Card|Card\Character $target
     * @return mixed
     */
    public function apply(Game $game, Card $target)
    {
        if($game->effects->cardHasEffect($target, StealthEffect::class)) {
            $target->addEffect('stealth');
        }
    }
}