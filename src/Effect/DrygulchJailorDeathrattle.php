<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Player;
use Tiixstone\Card\Alias;
use Tiixstone\Card\Minion;
use Tiixstone\Block;
use Tiixstone\Event\DeathrattleEvent;
use Tiixstone\Effect\DeathrattleEffect;

class DrygulchJailorDeathrattle extends DeathrattleEffect
{
    public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL): array
    {
        return [
			new Block\PutCardInHand($game->currentPlayer(), Alias\SilverHandRecruit::create()),
			new Block\PutCardInHand($game->currentPlayer(), Alias\SilverHandRecruit::create()),
			new Block\PutCardInHand($game->currentPlayer(), Alias\SilverHandRecruit::create()),
		];
    }
}