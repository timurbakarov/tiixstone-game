<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Event;
use Tiixstone\Block;
use Tiixstone\Effect;

class ManaAddictEffect extends TriggerEffect
{
    /**
     * @return string
     */
    public function triggerAtEvent(): string
    {
        return Event\BeforePlaySpell::class;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event\BeforePlaySpell $event
     * @return bool
     */
    public function condition(Game $game, Card $card, Event $event): bool
    {
        return $this->yourPlayer($card, $event->spell);
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        return [new Block\GiveEffect($card, new Effect\Trigger\EndOfTheTurn(new AttackChangerEffect(2)))];
    }

    /**
     * @param Card $owner
     * @param Card\Spell $spell
     * @return bool
     */
    private function yourPlayer(Card $owner, Card\Spell $spell)
    {
        return $owner->getPlayer()->id() == $spell->getPlayer()->id();
    }
}