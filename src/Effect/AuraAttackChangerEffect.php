<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Attackable;

class AuraAttackChangerEffect extends Aura implements Effect\Changer\AttackChanger
{
    /**
     * @var int
     */
    private $value;
	
	/**
     * @var Target
     */
	private $target;

    /**
     * @var bool
     */
	private $fixed;

    public function __construct(int $value, Target $target, bool $fixed = false)
    {
        $this->value = $value;
		$this->target = new Effect\Target\Aggregate([$target, new Effect\Target\Battlefield()]);
		$this->fixed = $fixed;
    }

    /**
     * @param Game $game
     * @param Attackable $character
     * @param int $attackRate
     * @return int
     */
    public function changeAttack(Game $game, Attackable $character, int $attackRate): int
    {
		if($this->target->condition($game, $this->owner(), $character)) {
			return $this->fixed ? $this->value - $character->attack->current->get() : $attackRate + $this->value;
		} else {
			return $attackRate;
		}
    }
}