<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Block;

class SkullOfTheManariEffect extends TriggerEffect
{
    /**
     * @return string
     */
    public function triggerAtEvent(): string
    {
        return Event\TurnStarted::class;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event|Event\TurnStarted $event
     * @return bool
     */
    public function condition(Game $game, Card $card, Event $event): bool
    {
        return $this->owner()->getPlayer()->id() == $event->player->id();
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        $player = $this->owner()->getPlayer();

        $demons = array_filter($player->hand->all(), function(Card $card) {
            /** @var Card\Minion $card */
            return $card->isMinion() && $card->race()->isDemon();
        });

        if(!$demons) {
            return [];
        }

        shuffle($demons);

        $randomDemon = $demons[rand(0, count($demons) - 1)];

        return [
            new Block\RetrieveCardFromHand($player, $randomDemon),
            new Block\SummonMinion($player, $randomDemon),
        ];
    }
}