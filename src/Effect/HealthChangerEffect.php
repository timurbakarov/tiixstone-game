<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Character;

class HealthChangerEffect extends Effect implements Effect\Changer\HealthChanger
{
    /**
     * @var int
     */
    private $value;

    /**
     * @var bool
     */
	private $fixed;

    public function __construct(int $value, bool $fixed = false)
    {
        $this->value = $value;
		$this->fixed = $fixed;
    }

    /**
     * @param Game $game
     * @param Card|Card\Minion $card
     * @return array
     */
    public function init(Game $game, Card $card) : array
    {
        if(!$this->fixed) {
            $card->health->buff->increase($this->value);
        } else {
            $card->health->buff->set($this->value - $card->health->current->get());
        }

        $game->logBlock('health-change-effect', [
            'char' => $card,
            'value' => $this->value,
        ]);

        return [];
    }

    /**
     * @param Game $game
     * @param Character $character
     * @param int $health
     * @return int
     */
    public function changeHealth(Game $game, Character $character, int $health): int
    {
        if($this->owner->isSelf($character)) {
            return $this->fixed ? $this->value - $character->health->current->get() : $health + $this->value;
        } else {
            return $health;
        }
    }
}