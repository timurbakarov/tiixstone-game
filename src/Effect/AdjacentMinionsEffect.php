<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Minion;
use Tiixstone\Card\Attackable;

abstract class AdjacentMinionsEffect extends Effect
{
    /**
     * @param Attackable $character
     * @param Minion|null $leftMinion
     * @param Minion|null $rightMinion
     * @return bool
     */
    protected function adjacentMinion(Game $game, Attackable $character)
    {
        if($this->owner->isSelf($character)) {
            return false;
        }

        $player = $this->owner()->getPlayer();

        $leftMinion = $player->board->prev($this->owner->id());
        $rightMinion = $player->board->next($this->owner->id());

        if($leftMinion AND $leftMinion->id() == $character->id()) {
            return true;
        }

        if($rightMinion AND $rightMinion->id() == $character->id()) {
            return true;
        }

        return false;
    }
}