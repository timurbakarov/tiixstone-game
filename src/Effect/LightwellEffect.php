<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Event;
use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Character;
use Tiixstone\Block\HealCharacter;

class LightwellEffect extends Effect\Trigger\StartOfYourTurn
{
    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        $game->logBlock('lightwell-effect');

        $targets = array_filter($game->targetManager->allPlayerCharacters($game, $card->getPlayer()), function(Character $character) {
            return $character->health->isDamaged();
        });

        if($targets) {
            shuffle($targets);

            return [new HealCharacter(array_shift($targets), 3)];
        }

        return [];
    }
}