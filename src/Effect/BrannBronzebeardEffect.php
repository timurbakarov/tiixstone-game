<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;

class BrannBronzebeardEffect extends Effect implements Effect\Changer\BlockChanger
{
    /**
     * @param Game $game
     * @param Block $block
     * @return array
     */
    public function changeBlock(Game $game, Block $block): Block
    {
        if($block instanceof Block\Battlecry) {
            if($block->minion->getPlayer()->id() != $this->owner()->getPlayer()->id()) {
                return $block;
            }

            return $this->doubleBattlecry($game, $block);
        }

        return $block;
    }

    /**
     * @param Game $game
     * @param Block $block
     * @return Block
     */
    private function doubleBattlecry(Game $game, Block $block) : Block
    {
        return new Block\AggregateBlock([
            new Block\ClosureBlock($this->battlecryClosure($block)),
            new Block\ClosureBlock($this->logBlock()),
            new Block\ClosureBlock($this->battlecryClosure($block)),
        ]);
    }

    /**
     * @return \Closure
     */
    private function logBlock()
    {
        return function(Game $game) {
            $game->logBlock('brann-bronzebeard-effect', ['char' => $this->owner()]);

            return [];
        };
    }

    /**
     * @return \Closure
     */
    private function battlecryClosure(Block $block)
    {
        return function(Game $game) use($block) {
            return $block->run($game);
        };
    }
}