<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Event;
use Tiixstone\Effect;
use Tiixstone\Manager\TargetManager;

class HealingTotemEffect extends Effect\Trigger\EndOfYourTurn
{
    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        $game->logBlock('healing-totem');

        return [new Block\Heal\MassHeal(TargetManager::ANY_FRIENDLY_MINION, 1, $this->owner())];
    }
}