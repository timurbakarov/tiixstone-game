<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;

class FallenHeroEffect extends Effect implements Effect\Changer\BlockChanger
{
    /**
     * @param Game $game
     * @param Block $block
     * @return array
     */
    public function changeBlock(Game $game, Block $block) : Block
    {
        if(!($block instanceof Block\TakeDamage)) {
            return $block;
        }

        /** @var Card $source */
        $source = $block->source();

        if(!$source OR !$source->isHeroPower()) {
            return $block;
        }

        return new Block\ClosureBlock(function(Game $game) use($block) {
            return (new Block\TakeDamage($block->character(), $block->amount() + 1, $block->source()))->run($game);
        });
    }
}