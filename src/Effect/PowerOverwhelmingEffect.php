<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Block\MarkDestroyed;
use Tiixstone\Event\BeforeEndTurn;

class PowerOverwhelmingEffect extends Effect
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            BeforeEndTurn::class => 'beforeEndTurn',
        ];
    }

    /**
     * @param Game $game
     * @param BeforeEndTurn $event
     * @return array
     */
    public function beforeEndTurn(Game $game, BeforeEndTurn $event)
    {
        return [new MarkDestroyed($this->owner())];
    }
}