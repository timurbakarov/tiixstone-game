<?php
/**
 * Created by PhpStorm.
 * User: timur
 * Date: 4/5/18
 * Time: 1:16 PM
 */

namespace Tiixstone\Effect;


use Tiixstone\Card;
use Tiixstone\Effect;
use Tiixstone\Game;

class MountainGiantEffect extends Effect implements Effect\Changer\ManaCostChanger
{

    /**
     * @param Game $game
     * @param Card $card
     * @param int $cost
     * @return int
     */
    public function changeManaCost(Game $game, Card $card, int $cost): int
    {
        if($this->owner->isSelf($card)) {
            $cost = $cost - $game->getPlayerByCard($this->owner())->hand->count() + 1;
        }

        return $cost;
    }
}