<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;
use Tiixstone\Exception;

class ElusiveEffect extends Effect implements Targetable
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\ValidateUseHeroPower::class => 'validateUseHeroPower',
        ];
    }

    /**
     * @param Game $game
     * @param Event\ValidateUseHeroPower $event
     * @return array
     * @throws Exception
     */
    public function validateUseHeroPower(Game $game, Event\ValidateUseHeroPower $event)
    {
        if($event->target AND $this->owner->isSelf($event->target)) {
            throw new Exception('Нельзя использовать cилу героя на этого персонажа');
        }

        return [];
    }

    /**
     * @param Game $game
     * @param Card|Card\Character $target
     * @return mixed
     */
    public function apply(Game $game, Card $target)
    {
        if($game->effects->cardHasEffect($target, ElusiveEffect::class)) {
            $target->addEffect('elusive');
        }
    }
}