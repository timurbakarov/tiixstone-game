<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Block\PutCardInHand;
use Tiixstone\Event\AfterTakeDamage;
use Tiixstone\Card\Alias\AmethystSpellstone;
use Tiixstone\Block\RetrieveCardFromHand;
use Tiixstone\Card\Alias\LesserAmethystSpellstone;
use Tiixstone\Card\Alias\GreaterAmethystSpellstone;

class AmethystSpellstoneEffect extends Effect
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            AfterTakeDamage::class => 'afterTakeDamage',
        ];
    }

    /**
     * @param Game $game
     * @param AfterTakeDamage $event
     * @return array
     */
    public function afterTakeDamage(Game $game, AfterTakeDamage $event)
    {
        if(!$event->source) {
            return [];
        }

        if(get_class($this->owner()) == GreaterAmethystSpellstone::className()) {
            return [];
        }

        $player = $event->source->getPlayer();

        if($event->character->id() == $player->hero->id()) {
            return [
                new PutCardInHand($player, $this->nextCard(), $this->owner()),
                new RetrieveCardFromHand($player, $this->owner()),
            ];
        }

        return [];
    }

    /**
     * @return \Tiixstone\Card\LOOT_043t2|\Tiixstone\Card\LOOT_043t3
     */
    private function nextCard()
    {
        if(get_class($this->owner()) == LesserAmethystSpellstone::className()) {
            return AmethystSpellstone::create();
        } else {
            return GreaterAmethystSpellstone::create();
        }
    }
}