<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Attackable;

class OneAttackThisTurnEffect extends Effect\Buff\OneTurnBuff implements Effect\Changer\AttackChanger, BoardEffect
{
    /**
     * @param Game $game
     * @param Attackable $character
     * @param int $attackRate
     * @return int
     */
    public function changeAttack(Game $game, Attackable $character, int $attackRate): int
    {
        return $this->owner()->isSelf($character) ? $attackRate + 1 : $attackRate;
    }
}