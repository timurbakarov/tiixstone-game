<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Block;

class AcolyteOfPainEffect extends TriggerEffect
{
    /**
     * @return string
     */
    public function triggerAtEvent(): string
    {
        return Event\AfterTakeDamage::class;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event\AfterTakeDamage $event
     * @return bool
     */
    public function condition(Game $game, Card $card, Event $event): bool
    {
        return $card->isSelf($event->character);
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        return [new Block\DrawCard($card->getPlayer())];
    }
}