<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Exception;

class ImmuneEffect extends Effect implements Effect\Changer\BlockChanger, Targetable
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\ValidateUseHeroPower::class => 'validateUseHeroPower',
        ];
    }

    /**
     * @param Game $game
     * @param Event\ValidateUseHeroPower $event
     * @return array
     * @throws Exception
     */
    public function validateUseHeroPower(Game $game, Event\ValidateUseHeroPower $event)
    {
        if(!$event->target) {
            return [];
        }

        if(!$this->owner->isSelf($event->target)) {
            return [];
        }

        if($event->heroPower->getPlayer()->id() == $this->owner->getPlayer()->id()) {
            return [];
        }

        throw new Exception('Нельзя использовать cилу героя на персонажа с неуязвимостью');
    }

    /**
     * @param Game $game
     * @param Block $block
     * @return Block
     */
    public function changeBlock(Game $game, Block $block) : Block
    {
        if($block instanceof Block\TakeDamage AND $this->owner()->isSelf($block->character())) {
            return new Block\EmptyBlock();
        }

        return $block;
    }

    /**
     * @param Game $game
     * @param Card|Card\Character $target
     * @return mixed
     */
    public function apply(Game $game, Card $target)
    {
        if($game->effects->cardHasEffect($target, ImmuneEffect::class)) {
            $target->addEffect('immune');
        }
    }
}