<?php

namespace Tiixstone\Effect;

class ArchmageEffect extends SpellDamageBuff
{
    /**
     * @return int
     */
    public function spellDamageAmount(): int
    {
        return 1;
    }
}