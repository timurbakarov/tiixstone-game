<?php

namespace Tiixstone\Effect\Buff;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Attackable;

class BlessingOfMightEffect extends Effect implements Effect\Changer\AttackChanger
{
    /**
     * @param Game $game
     * @param Attackable $character
     * @param int $attackRate
     * @return int
     */
    public function changeAttack(Game $game, Attackable $character, int $attackRate): int
    {
        if($this->owner()->isSelf($character)) {
            return $attackRate + 3;
        }

        return $attackRate;
    }
}