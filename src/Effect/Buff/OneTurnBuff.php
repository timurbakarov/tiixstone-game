<?php

namespace Tiixstone\Effect\Buff;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Event\BeforeEndTurn;

class OneTurnBuff extends Effect
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            BeforeEndTurn::class => 'beforeEndTurn',
        ];
    }

    /**
     * @param Game $game
     * @param BeforeEndTurn $event
     * @return array
     */
    public function beforeEndTurn(Game $game, BeforeEndTurn $event)
    {
        $game->logBlock('buff-fades');

        $game->effects->removeByEffect($this);

        return [];
    }
}