<?php

namespace Tiixstone\Effect\Buff;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Character;
use Tiixstone\Card\Attackable;

class BlessingOfKingsEffect extends Effect implements Effect\Changer\AttackChanger, Effect\Changer\HealthChanger
{
    /**
     * @param Game $game
     * @param Attackable $character
     * @param int $attackRate
     * @return int
     */
    public function changeAttack(Game $game, Attackable $character, int $attackRate): int
    {
        if($this->owner()->isSelf($character)) {
            return $attackRate + 4;
        }

        return $attackRate;
    }

    /**
     * @param Game $game
     * @param Attackable $character
     * @param int $attackRate
     * @return int
     */
    public function changeHealth(Game $game, Character $character, int $health): int
    {
        if($this->owner()->isSelf($character)) {
            return $health + 4;
        }

        return $health;
    }
}