<?php

namespace Tiixstone\Effect\Buff;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Character;
use Tiixstone\Card\Attackable;
use Tiixstone\Effect\Changer\HealthChanger;

/**
 * Class DivineSpiritBuff
 * @package Tiixstone\Effect\Buff
 */
class DivineSpiritBuff extends Effect implements HealthChanger
{
    /**
     * @var
     */
    private $healthBuff;

    /**
     * @param Game $game
     * @param Card $card
     * @return array
     */
    public function init(Game $game, Card $card) : array
    {
        $this->healthBuff = $card->health->total();

        return [];
    }

    /**
     * @param Game $game
     * @param Attackable $character
     * @param int $attackRate
     * @return int
     */
    public function changeHealth(Game $game, Character $character, int $health) : int
    {
        if($character->isSelf($this->owner())) {
            return $health + $this->healthBuff;
        }

        return $health;
    }
}