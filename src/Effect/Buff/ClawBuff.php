<?php

namespace Tiixstone\Effect\Buff;

use Tiixstone\Game;
use Tiixstone\Card\Character;
use Tiixstone\Card\Attackable;
use Tiixstone\Effect\Changer\AttackChanger;

class ClawBuff extends OneTurnBuff implements AttackChanger
{
    /**
     * @param Game $game
     * @param Character $character
     * @param int $attackRate
     * @return int
     */
    public function changeAttack(Game $game, Attackable $character, int $attackRate): int
    {
        if($this->owner()->isSelf($character)) {
            return $attackRate + 2;
        }

        return $attackRate;
    }
}