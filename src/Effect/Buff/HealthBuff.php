<?php

namespace Tiixstone\Effect\Buff;

use Tiixstone\Game;
use Tiixstone\Effect\Buff;

abstract class HealthBuff extends Buff
{
    /**
     * @var int
     */
    protected $value;

    /**
     * @param Game $game
     * @return int
     */
    abstract public function value(Game $game) : int;
}