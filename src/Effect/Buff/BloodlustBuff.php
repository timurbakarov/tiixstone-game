<?php

namespace Tiixstone\Effect\Buff;

use Tiixstone\Card;
use Tiixstone\Game;

class BloodlustBuff extends OneTurnBuff
{
    /**
     * @param Game $game
     * @return int
     */
    public function attack(Game $game, Card $card, int $rate): int
    {
        $player = $game->getPlayerByCard($this->owner());

        if($player->board->has($card->id())) {
            return $rate + 3;
        }

        return $rate;
    }
}