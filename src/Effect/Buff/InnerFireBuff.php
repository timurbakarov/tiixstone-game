<?php

namespace Tiixstone\Effect\Buff;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Attackable;

class InnerFireBuff extends Effect implements Effect\Changer\AttackChanger
{
    /**
     * @var
     */
    private $amount;

    /**
     * @param Game $game
     * @param Card\Minion $card
     */
    public function init(Game $game, Card $card) : array
    {
        $this->amount = $card->health->total() - $card->attack->total();

        return [];
    }

    /**
     * @param Game $game
     * @param Attackable $character
     * @param int $attackRate
     * @return int
     */
    public function changeAttack(Game $game, Attackable $character, int $attackRate): int
    {
        if($character->isSelf($this->owner)) {
            return $attackRate + $this->amount;
        }

        return $attackRate;
    }
}