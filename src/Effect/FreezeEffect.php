<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;
use Tiixstone\Exception;

class FreezeEffect extends Effect implements Targetable
{
    /**
     * @vars
     */
    private $turnsToBeFrozen;

    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\BeforeEndTurn::class => 'beforeEndTurn',
        ];
    }

    /**
     * @param Game $game
     * @param Card|Card\Character $card
     * @return array
     */
    public function init(Game $game, Card $card) : array
    {
        $this->turnsToBeFrozen = $this->calculateTurnsCharacterShouldBeFrozen($game, $card);

        return [];
    }

    /**
     * @param Game $game
     * @param Event\BeforeEndTurn $event
     * @return array
     */
    public function beforeEndTurn(Game $game, Event\BeforeEndTurn $event)
    {
        if($this->turnsToBeFrozen <= 0) {
            $game->effects->removeByEffect($this);
        } else {
            $this->turnsToBeFrozen = $this->turnsToBeFrozen - 1;
        }

        return [];
    }

    /**
     * @param Game $game
     * @param Card $card
     * @return int
     */
    private function calculateTurnsCharacterShouldBeFrozen(Game $game, Card $card)
    {
        $turnsToBeFrozen = 0;

        $player = $card->getPlayer();

        if($game->idlePlayer()->id() == $player->id()) {
            $turnsToBeFrozen = 1;
        } elseif($game->attackTracker->attackTimes($card) > 0) {
            $turnsToBeFrozen = 2;
        }

        return $turnsToBeFrozen;
    }

    /**
     * @param Game $game
     * @param Card $target
     * @return mixed
     */
    public function apply(Game $game, Card $target)
    {
        if($target->isSelf($this->owner)) {
            $this->owner->addEffect('freeze');
        }
    }
}