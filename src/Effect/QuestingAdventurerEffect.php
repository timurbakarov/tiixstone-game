<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Event;

class QuestingAdventurerEffect extends TriggerEffect
{
    /**
     * @return string
     */
    public function triggerAtEvent(): string
    {
        return Event\BeforePlayCard::class;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event\BeforePlayCard $event
     * @return bool
     */
    public function condition(Game $game, Card $card, Event $event): bool
    {
        return $card->getPlayer()->id() == $event->card->getPlayer()->id();
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        return [
            new Block\GiveEffect($card, new AttackChangerEffect(1)),
            new Block\GiveEffect($card, new HealthChangerEffect(1)),
        ];
    }
}