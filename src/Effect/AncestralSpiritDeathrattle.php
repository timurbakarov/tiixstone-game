<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Card\Minion;
use Tiixstone\Block\SummonMinion;

class AncestralSpiritDeathrattle extends DeathrattleEffect
{
    /**
     * @param Game $game
     * @param Minion $minion
     * @param Minion|NULL $nextMinion
     * @return mixed
     */
    public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = NULL): array
    {
        return [new SummonMinion($minion->getPlayer(), $minion->copy())];
    }
}