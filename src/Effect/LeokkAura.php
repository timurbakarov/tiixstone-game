<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Character;
use Tiixstone\Card\Attackable;

class LeokkAura extends Effect implements Effect\Changer\AttackChanger
{
    /**
     * @param Game $game
     * @param Character $character
     * @param int $attackRate
     * @return int
     */
    public function changeAttack(Game $game, Attackable $character, int $attackRate): int
    {
        if($this->owner->isSelf($character)) {
            return $attackRate;
        }

        $player = $game->getPlayerByCard($this->owner());

        return $attackRate + ($player->board->has($character->id()) ? 1 : 0);
    }
}