<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Manager\TargetManager;

class ShadowboxerEffect extends Effect
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\AfterHeal::class => 'afterHeal',
        ];
    }

    /**
     * @param Game $game
     * @param Event\AfterHeal $event
     * @return array
     */
    public function afterHeal(Game $game, Event\AfterHeal $event)
    {
        return [new Block\Damage\DealRandomDamage(TargetManager::ANY_OPPONENT_CHARACTER, 1, $this->owner())];
    }
}