<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;

/**
 * Эффекты, которые применяются к определнным целям
 * Например Charge, Lifesteal, Enrage
 *
 * Interface Targetable
 * @package Tiixstone\Effect
 */
interface Targetable
{
    /**
     * @param Game $game
     * @param Card $target
     * @return mixed
     */
    public function apply(Game $game, Card $target);
}