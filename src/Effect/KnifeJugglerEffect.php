<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Event;
use Tiixstone\Manager\TargetManager;

class KnifeJugglerEffect extends TriggerEffect
{
    /**
     * @param Event\AfterSummon $event
     * @return bool
     */
    private function friendlyMinionSummoned(Event\AfterSummon $event)
    {
        return $this->owner()->id() != $event->minion->id()
            && $this->owner()->getPlayer()->board->has($event->minion->id());
    }

    /**
     * @return string
     */
    public function triggerAtEvent(): string
    {
        return Event\AfterSummon::class;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return bool
     */
    public function condition(Game $game, Card $card, Event $event): bool
    {
        return $this->friendlyMinionSummoned($event);
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        $game->logBlock('knife-juggler-effect');

        return [new Block\Damage\DealRandomDamage(TargetManager::ANY_OPPONENT_CHARACTER, 1, $this->owner())];
    }
}