<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Effect;
use Tiixstone\Game;

class ManaWraithEffect extends Effect implements Effect\Changer\ManaCostChanger
{
    /**
     * @param Game $game
     * @param Card $card
     * @param int $cost
     * @return int
     */
    public function changeManaCost(Game $game, Card $card, int $cost): int
    {
        if($card->isMinion()) {
            return $cost + 1;
        }

        return $cost;
    }
}