<?php

namespace Tiixstone\Effect\Target;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Effect\Target as BaseTarget;

class Race extends BaseTarget
{
    /**
     * @var \Tiixstone\Race
     */
    private $race;

    public function __construct(\Tiixstone\Race $race)
    {
        $this->race = $race;
    }

    /**
     * @param Game $game
     * @param Card $owner
     * @param Card|Card\Character $card
     * @return bool
     */
    public function condition(Game $game, Card $owner, Card $card) : bool
	{
        return $card instanceof Card\Character && $card->race()->is($this->race);
	}
}