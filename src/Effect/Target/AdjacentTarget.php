<?php

namespace Tiixstone\Effect\Target;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Effect\Target as BaseTarget;

class AdjacentTarget extends BaseTarget
{
	public function condition(Game $game, Card $owner, Card $card) : bool
	{
		if($owner->isSelf($card)) {
            return false;
        }

        $player = $owner->getPlayer();

        $leftMinion = $player->board->prev($owner->id());
        $rightMinion = $player->board->next($owner->id());

        if($leftMinion AND $leftMinion->id() == $card->id()) {
            return true;
        }

        if($rightMinion AND $rightMinion->id() == $card->id()) {
            return true;
        }

        return false;
	}
}