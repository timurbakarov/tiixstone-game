<?php

namespace Tiixstone\Effect\Target;

use Tiixstone\Card;
use Tiixstone\Effect\Target;
use Tiixstone\Game;

class SpecificCharacter extends Target
{
    /**
     * @var Card\Character
     */
    private $character;

    public function __construct(Card\Character $character)
    {
        $this->character = $character;
    }

    /**
     * @param Game $game
     * @param Card $owner
     * @param Card $card
     * @return bool
     */
    public function condition(Game $game, Card $owner, Card $card): bool
    {
        return $this->character->id() == $card->id();
    }
}