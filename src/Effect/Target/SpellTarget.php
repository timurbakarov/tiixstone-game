<?php

namespace Tiixstone\Effect\Target;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Effect\Target as BaseTarget;

class SpellTarget extends BaseTarget
{
    /**
     * @param Game $game
     * @param Card $owner
     * @param Card $card
     * @return bool
     */
    public function condition(Game $game, Card $owner, Card $card) : bool
	{
        return $card->isSpell();
	}
}