<?php

namespace Tiixstone\Effect\Target;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Effect\Target as BaseTarget;

class Hand extends BaseTarget
{
    /**
     * @param Game $game
     * @param Card $owner
     * @param Card $card
     * @return bool
     */
    public function condition(Game $game, Card $owner, Card $card) : bool
	{
        return $card->getPlayer()->hand->has($card->id());
	}
}