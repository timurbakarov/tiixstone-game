<?php

namespace Tiixstone\Effect\Target;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Effect\Target as BaseTarget;

class NotSelfTarget extends BaseTarget
{
    /**
     * @param Game $game
     * @param Card $owner
     * @param Card $target
     * @return bool
     */
    public function condition(Game $game, Card $owner, Card $target = null) : bool
	{
		return !$owner->isSelf($target);
	}
}