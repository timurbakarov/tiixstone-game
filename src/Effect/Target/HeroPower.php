<?php

namespace Tiixstone\Effect\Target;

use Tiixstone\Card;
use Tiixstone\Effect\Target;
use Tiixstone\Game;

class HeroPower extends Target
{
    /**
     * @param Game $game
     * @param Card $owner
     * @param Card $card
     * @return bool
     */
    public function condition(Game $game, Card $owner, Card $card): bool
    {
        return $card->isHeroPower();
    }
}