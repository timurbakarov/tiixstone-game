<?php

namespace Tiixstone\Effect\Target;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Effect\Target as BaseTarget;

class FriendlyMinionsTarget extends BaseTarget
{
	public function condition(Game $game, Card $owner, Card $card) : bool
	{
        return $owner->getPlayer()->board->has($card->id()) && !$owner->isSelf($card);
	}
}