<?php

namespace Tiixstone\Effect\Target;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Effect\Target;

class Not extends Target
{
    /**
     * @var Target
     */
    private $condition;

    public function __construct(Target $condition)
    {
        $this->condition = $condition;
    }

    /**
     * @param Game $game
     * @param Card $owner
     * @param Card $card
     * @return bool
     */
    public function condition(Game $game, Card $owner, Card $card): bool
    {
        return !$this->condition->condition($game, $owner, $card);
    }
}