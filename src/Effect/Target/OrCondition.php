<?php

namespace Tiixstone\Effect\Target;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Effect\Target;

class OrCondition extends Target
{
    /**
     * @var Target[]
     */
    private $conditions;

    public function __construct(array $conditions)
    {
        $this->conditions = $conditions;
    }

    /**
     * @param Game $game
     * @param Card $owner
     * @param Card $card
     * @return bool
     */
    public function condition(Game $game, Card $owner, Card $card): bool
    {
        foreach($this->conditions as $condition) {
            if($condition->condition($game, $owner, $card)) {
                return true;
            }
        }

        return false;
    }
}