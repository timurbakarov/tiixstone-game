<?php

namespace Tiixstone\Effect\Target;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Effect\Target as BaseTarget;

class HasEffect extends BaseTarget
{
    /**
     * @var
     */
    private $effect;

    public function __construct($effect)
    {
        $this->effect = $effect;
    }

    /**
     * @param Game $game
     * @param Card $owner
     * @param Card|Card\Character $card
     * @return bool
     */
    public function condition(Game $game, Card $owner, Card $card) : bool
	{
        return $game->effects->cardHasEffect($card, $this->effect);
	}
}