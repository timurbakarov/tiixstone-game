<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Event;
use Tiixstone\Card\Spell;
use Tiixstone\Manager\TargetManager;

class WildPyromancerEffect extends TriggerEffect
{
    /**
     * @param Spell $spell
     * @return bool
     */
    private function yourPlayer(Spell $spell)
    {
        return $this->owner->getPlayer()->id() == $spell->getPlayer()->id();
    }

    /**
     * @return string
     */
    public function triggerAtEvent(): string
    {
        return Event\AfterPlaySpell::class;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event\AfterPlaySpell $event
     * @return bool
     */
    public function condition(Game $game, Card $card, Event $event): bool
    {
        return $this->yourPlayer($event->spell);
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        return [new Block\DealSplashDamage(TargetManager::ANY_MINION, 1, $this->owner())];
    }
}