<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Effect\GlobalEffects\EchoGlobalEffect;

/**
 * Class EchoEffect
 * @package Tiixstone\Effect
 */
class EchoEffect extends \Tiixstone\Effect
{
    /**
     * @param Game $game
     * @param Card $card
     * @return array
     */
    public function init(Game $game, Card $card) : array
    {
        if(!$game->effects->hasGlobalEffect(EchoGlobalEffect::class)) {
            $game->effects->addGlobalEffects($game, [EchoGlobalEffect::class]);
        }

        return [];
    }
}