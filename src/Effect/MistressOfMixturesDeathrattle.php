<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Card\Minion;
use Tiixstone\Block\HealCharacter;

class MistressOfMixturesDeathrattle extends DeathrattleEffect
{
    /**
     * @param Game $game
     * @param Minion $minion
     * @param Minion|null $nextMinion
     * @return array
     */
    public function deathrattle(Game $game, Minion $minion, Minion $nextMinion = null): array
    {
        $game->logBlock('mistress-of-mixtures-deathrattle');

        return [
            new HealCharacter($minion->getPlayer()->hero, $this->heal()),
            new HealCharacter($minion->getPlayer()->getOpponent($game)->hero, $this->heal()),
        ];
    }

    /**
     * @return int
     */
    private function heal()
    {
        return 4;
    }
}