<?php

namespace Tiixstone\Effect;

use Tiixstone\Effect;
use Tiixstone\Player;
use Tiixstone\Exception;

class SpellDamageEffect extends Effect
{
    /**
     * @var int
     */
    private $amount;

    /**
     * @var bool
     */
    private $bothSide;

    public function __construct(int $amount, bool $bothSide = false)
    {
        if($amount < 1) {
            throw new Exception("Spell damage amount should be greater than 0");
        }

        $this->amount = $amount;
        $this->bothSide = $bothSide;
    }

    /**
     * @param Player $player
     * @return int
     */
    function spellDamage(Player $player) : int
    {
        if($this->bothSide) {
            return $this->amount;
        } else {
            return $player->id() == $this->owner()->getPlayer()->id() ? $this->amount : 0;
        }
    }
}