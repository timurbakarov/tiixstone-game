<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;
use Tiixstone\Block\MindControl;
use Tiixstone\Block\Effect\RemoveEffect;

/**
 * Class ReturnControlBack
 * @package Tiixstone\Effect
 */
class ReturnControlBack extends Effect
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\BeforeEndTurn::class => 'beforeEndTurn',
        ];
    }

    /**
     * @param Game $game
     * @param Event\BeforeEndTurn $event
     * @return array
     */
    public function beforeEndTurn(Game $game, Event\BeforeEndTurn $event)
    {
        return [new RemoveEffect([$this])];
    }

    /**
     * @param Game $game
     * @return array
     */
    public function onRemove(Game $game) : array
    {
        return [new MindControl($this->owner())];
    }
}