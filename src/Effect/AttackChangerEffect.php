<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Attackable;

class AttackChangerEffect extends Effect implements Effect\Changer\AttackChanger
{
    /**
     * @var int
     */
    private $value;

    /**
     * @var bool
     */
	private $fixed;

    public function __construct(int $value, bool $fixed = false)
    {
        $this->value = $value;
		$this->fixed = $fixed;
    }

    /**
     * @param Game $game
     * @param Card|Card\Character $card
     * @return array
     */
    public function init(Game $game, Card $card) : array
    {
        $game->logBlock('attack-change-effect', [
            'char' => $card,
            'value' => $this->value,
        ]);

        // Lightwarden and Power Word: Glory
        // https://www.youtube.com/watch?v=sFBe5lnIQlY&feature=youtu.be&t=3m8s
        if(!$this->fixed) {
            $card->attack->buff->increase($this->value);
        }

        return [];
    }

    /**
     * @param Game $game
     * @param Attackable $character
     * @param int $attackRate
     * @return int
     */
    public function changeAttack(Game $game, Attackable $character, int $attackRate): int
    {
		if($this->owner->isSelf($character)) {
			return $this->fixed ? $this->value - $character->attack->current->get() : $attackRate + $this->value;
		} else {
			return $attackRate;
		}
    }
}