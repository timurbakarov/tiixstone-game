<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Card\Attackable;

class DurabilityChangerEffect extends Effect implements Effect\Changer\DurabilityChanger
{
    /**
     * @var int
     */
    private $value;

    /**
     * @var bool
     */
	private $fixed;

    public function __construct(int $value, bool $fixed = false)
    {
        $this->value = $value;
		$this->fixed = $fixed;
    }

    /**
     * @param Game $game
     * @param Card|Card\Character $card
     */
    public function init(Game $game, Card $card) : array
    {
        $game->logBlock('durability-change-effect', [
            'char' => $card,
            'value' => $this->value,
        ]);

        return [];
    }

    /**
     * @param Game $game
     * @param Attackable $character
     * @param int $attackRate
     * @return int
     */
    public function changeAttack(Game $game, Card\Weapon $weapon, int $attackRate): int
    {
		if($this->owner->isSelf($weapon)) {
			return $this->fixed ? $this->value - $weapon->durability->total() : $attackRate + $this->value;
		} else {
			return $attackRate;
		}
    }
}