<?php

namespace Tiixstone\Effect;

use Tiixstone\Event;
use Tiixstone\Game;
use Tiixstone\Effect;
use Tiixstone\Player;
use Tiixstone\Block\MarkDestroyed;

class CorruptionEffect extends Effect
{
    /**
     * @var
     */
    private $player;

    /**
     * CorruptionEffect constructor.
     * @param Player $player
     */
    public function __construct(Player $player)
    {
        $this->player = $player;
    }

    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\TurnStarted::class => 'turnStarted',
        ];
    }

    /**
     * @param Game $game
     * @param Event\TurnStarted $event
     * @return array
     */
    public function turnStarted(Game $game, Event\TurnStarted $event)
    {
        if($event->player->id() != $this->player->id()) {
            return [];
        }

        return [new MarkDestroyed($this->owner())];
    }
}