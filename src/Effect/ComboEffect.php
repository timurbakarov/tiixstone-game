<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;

abstract class ComboEffect extends Effect implements Effect\Changer\BlockChanger
{
    /**
     * @return array
     */
    abstract public function combo(Game $game, Card $owner, Card\Character $target = null) : Block;

    /**
     * @param Game $game
     * @param Block $block
     * @return array
     */
    final public function changeBlock(Game $game, Block $block): Block
    {
        if($this->isComboCard($block) AND $this->hasCombo($game)) {
            return $this->combo($game, $this->owner(), $block->target);
        }

        return $block;
    }

    /**
     * @param Game $game
     * @return array
     */
    private function hasCombo(Game $game)
    {
        return $game->graveyard->playedCardsByTurn($game->turnNumber());
    }

    /**
     * @param Block $block
     * @return bool
     */
    private function isComboCard(Block $block) : bool
    {
        if($block instanceof Block\Battlecry AND $block->minion->isSelf($this->owner())) {
            return true;
        }

        if($block instanceof Block\CastSpell AND $block->spell->isSelf($this->owner())) {
            return true;
        }

        return false;
    }
}