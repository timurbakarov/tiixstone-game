<?php

namespace Tiixstone\Effect;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Effect;

class ChargeEffect extends Effect implements Targetable
{
    /**
     * @var Target
     */
    protected $targetCondition;

    /**
     * @param Target|null $targetCondition
     */
    public function __construct(Target $targetCondition = null)
    {
        $this->targetCondition = $targetCondition ?: new Effect\Target\SelfTarget();
    }

    /**
     * @param Game $game
     * @param Card|Card\Character $target
     * @return mixed|void
     */
    public function apply(Game $game, Card $target)
    {
        if($this->targetCondition->condition($game, $this->owner, $target)) {
            $target->addEffect('charge');
        }
    }
}