<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Card;

abstract class Target 
{
	abstract public function condition(Game $game, Card $owner, Card $card) : bool;
}