<?php

namespace Tiixstone\Effect;

use Tiixstone\Game;
use Tiixstone\Card\Attackable;
use Tiixstone\Effect\Buff\OneTurnBuff;
use Tiixstone\Effect\Changer\AttackChanger;

class ShapeshiftEffect extends OneTurnBuff implements AttackChanger
{
    /**
     * @param Game $game
     * @param Attackable $character
     * @param int $attackRate
     * @return int
     */
    public function changeAttack(Game $game, Attackable $character, int $attackRate): int
    {
        return $character->isSelf($this->owner()) ? $attackRate + 1 : $attackRate;
    }
}