<?php

namespace Tiixstone\Effect;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect;
use Tiixstone\Event\AfterDestroyMinion;
use Tiixstone\Game;

class VolcanicLumbererEffect extends Effect
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            AfterDestroyMinion::class => 'afterDestroyMinion',
        ];
    }

    /**
     * @param Game $game
     * @param AfterDestroyMinion $event
     * @return array
     */
    public function afterDestroyMinion(Game $game, AfterDestroyMinion $event)
    {
        if($game->currentPlayer()->id() != $event->minion->getPlayer()->id()) {
            return [];
        }

        return [new GiveEffect($this->owner(), new Effect\Trigger\EndOfTheTurn(new CostChangerEffect(-1)))];
    }
}