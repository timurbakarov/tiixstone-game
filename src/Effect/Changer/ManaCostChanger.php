<?php

namespace Tiixstone\Effect\Changer;

use Tiixstone\Card;
use Tiixstone\Game;

interface ManaCostChanger
{
    /**
     * @param Game $game
     * @param Card $card
     * @param int $cost
     * @return int
     */
    public function changeManaCost(Game $game, Card $card, int $cost) : int;
}