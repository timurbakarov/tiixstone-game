<?php

namespace Tiixstone\Effect\Changer;

use Tiixstone\Game;
use Tiixstone\Card\Weapon;

interface DurabilityChanger
{
    /**
     * @param Game $game
     * @param Weapon $weapon
     * @param int $durability
     * @return int
     */
    public function changeAttack(Game $game, Weapon $weapon, int $durability) : int;
}