<?php

namespace Tiixstone\Effect\Changer;

use Tiixstone\Game;
use Tiixstone\Card\Character;

interface SpellDamageChanger
{
    /**
     * @param Game $game
     * @param Character $character
     * @param int $spellDamage
     * @return int
     */
    public function changeSpellDamage(Game $game, Character $character, int $spellDamage) : int;
}