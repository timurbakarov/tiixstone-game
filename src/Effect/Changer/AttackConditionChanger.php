<?php

namespace Tiixstone\Effect\Changer;

use Tiixstone\Game;
use Tiixstone\AttackCondition;
use Tiixstone\Card\Attackable;

interface AttackConditionChanger
{
    /**
     * @param Game $game
     * @param Attackable $character
     * @return AttackCondition
     */
    public function changeAttackCondition(Game $game, Attackable $character) : AttackCondition;

    /**
     * @param Game $game
     * @param Attackable $character
     * @return bool
     */
    public function isTarget(Game $game, Attackable $character) : bool;
}