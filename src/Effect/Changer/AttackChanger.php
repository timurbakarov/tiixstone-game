<?php

namespace Tiixstone\Effect\Changer;

use Tiixstone\Game;
use Tiixstone\Card\Attackable;

interface AttackChanger
{
    /**
     * @param Game $game
     * @param Attackable $character
     * @param int $attackRate
     * @return int
     */
    public function changeAttack(Game $game, Attackable $character, int $attackRate) : int;
}