<?php

namespace Tiixstone\Effect\Changer;

use Tiixstone\Game;
use Tiixstone\Block;

interface BlockChanger
{
    /**
     * @param Game $game
     * @param Block $block
     * @return array
     */
    public function changeBlock(Game $game, Block $block) : Block;
}