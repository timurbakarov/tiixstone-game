<?php

namespace Tiixstone\Effect\Changer;

use Tiixstone\Card\Character;
use Tiixstone\Game;
use Tiixstone\Card\Attackable;

interface HealthChanger
{
    /**
     * @param Game $game
     * @param Attackable $character
     * @param int $attackRate
     * @return int
     */
    public function changeHealth(Game $game, Character $character, int $health) : int;
}