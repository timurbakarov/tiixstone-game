<?php

namespace Tiixstone\Effect\Trigger;

use Tiixstone\Block\DrawCard;
use Tiixstone\Card;
use Tiixstone\Effect\TriggerEffect;
use Tiixstone\Event;
use Tiixstone\Game;

class CultMasterEffect extends TriggerEffect
{

    /**
     * @return string
     */
    public function triggerAtEvent(): string
    {
        return Event\BeforeDestroyMinion::class;
    }

    /**
     * @param Game $game
     * @param Card|Card\Minion $card
     * @param Event|Event\BeforeDestroyMinion $event
     * @return bool
     */
    public function condition(Game $game, Card $card, Event $event): bool
    {
        if($card->isDestroyed()) {
            return false;
        }

        if($card->isSelf($event->minion)) {
            return false;
        }

        return $card->getPlayer()->id() == $event->minion->getPlayer()->id();
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        return [new DrawCard($card->getPlayer())];
    }
}