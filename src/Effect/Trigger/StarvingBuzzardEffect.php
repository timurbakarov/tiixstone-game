<?php

namespace Tiixstone\Effect\Trigger;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Event;
use Tiixstone\Block\DrawCard;
use Tiixstone\Effect\TriggerEffect;

class StarvingBuzzardEffect extends TriggerEffect
{
    /**
     * @return string
     */
    public function triggerAtEvent(): string
    {
        return Event\BeforeSummon::class;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event|Event\BeforeSummon $event
     * @return bool
     */
    public function condition(Game $game, Card $card, Event $event): bool
    {
        return $event->minion->race()->isBeast() && $event->player()->id()
            == $card->getPlayer()->id();
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        return [new DrawCard($card->getPlayer())];
    }
}