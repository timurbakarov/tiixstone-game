<?php

namespace Tiixstone\Effect\Trigger;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Block;
use Tiixstone\Effect;

/**
 * Class EndOfTheTurn
 * @package Tiixstone\Effect\Trigger
 */
class UntilYourNextTurn extends Effect\TriggerEffect
{
    /**
     * @var Effect
     */
    private $effect;

    /**
     * EndOfTheTurn constructor.
     * @param Effect $effect
     */
    public function __construct(Effect $effect)
    {
        $this->effect = $effect;
    }

    /**
     * @param Game $game
     * @throws \Tiixstone\Exception
     */
    public function init(Game $game, Card $card) : array
    {
        return [new Block\GiveEffect($card, $this->effect)];
    }

    /**
     * @return string
     */
    public function triggerAtEvent(): string
    {
        return Event\TurnStarted::class;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event\TurnStarted $event
     * @return bool
     */
    public function condition(Game $game, Card $card, Event $event): bool
    {
        return $event->player->id() == $this->effect->owner()->getPlayer()->id();
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        return [new Block\Effect\RemoveEffect([$this->effect, $this])];
    }
}