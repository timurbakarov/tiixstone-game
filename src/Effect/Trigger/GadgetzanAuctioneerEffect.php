<?php

namespace Tiixstone\Effect\Trigger;

use Tiixstone\Block\DrawCard;
use Tiixstone\Card;
use Tiixstone\Effect\TriggerEffect;
use Tiixstone\Event;
use Tiixstone\Game;

class GadgetzanAuctioneerEffect extends TriggerEffect
{
    /**
     * @return string
     */
    public function triggerAtEvent(): string
    {
        return Event\BeforePlaySpell::class;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return bool
     */
    public function condition(Game $game, Card $card, Event $event): bool
    {
        return $card->getPlayer()->isCurrent($game);
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        return [new DrawCard($card->getPlayer())];
    }
}