<?php

namespace Tiixstone\Effect\Trigger;

use Tiixstone\Block\TakeDamage;
use Tiixstone\Card;
use Tiixstone\Effect\TriggerEffect;
use Tiixstone\Event;
use Tiixstone\Game;

class WrathGuardEffect extends TriggerEffect
{
    /**
     * @return string
     */
    public function triggerAtEvent(): string
    {
        return Event\BeforeTakeDamage::class;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event|Event\BeforeTakeDamage $event
     * @return bool
     */
    public function condition(Game $game, Card $card, Event $event): bool
    {
        return $event->character->isSelf($card);
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event|Event\BeforeTakeDamage $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        return [new TakeDamage($card->getPlayer()->hero, $event->amount, $card)];
    }
}