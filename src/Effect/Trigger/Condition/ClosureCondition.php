<?php

namespace Tiixstone\Effect\Trigger\Condition;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect\Trigger\Condition;

class ClosureCondition extends Condition
{
    /**
     * @var \Closure
     */
    private $closure;

    public function __construct(\Closure $closure)
    {
        $this->closure = $closure;
    }
    
    /**
     * @param Game $game
     * @param Card $owner
     * @param Event $event
     * @return array
     */
    public function isSatisfiedBy(Game $game, Card $owner, Event $event): array 
    {
        return call_user_func_array($this->closure, [$game, $owner, $event]);
    }
}