<?php

namespace Tiixstone\Effect\Trigger;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Event;
use Tiixstone\Block\SummonMinion;
use Tiixstone\Effect\TriggerEffect;

class GrimPatronEffect extends TriggerEffect
{
    /**
     * @return string
     */
    public function triggerAtEvent(): string
    {
        return Event\AfterTakeDamage::class;
    }

    /**
     * @param Game $game
     * @param Card|Card\Minion $card
     * @param Event|Event\AfterTakeDamage $event
     * @return bool
     */
    public function condition(Game $game, Card $card, Event $event): bool
    {
        return $event->character->isSelf($card) && !$card->isDestroyed();
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        return [new SummonMinion($card->getPlayer(), Card\Alias\GrimPatron::create(), $card, 'right')];
    }
}