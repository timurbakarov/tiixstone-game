<?php

namespace Tiixstone\Effect\Trigger;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Event;
use Tiixstone\Block\GiveEffect;
use Tiixstone\Effect\CostChangerEffect;

class EmperorThaurissanEffect extends EndOfYourTurn
{
    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        $cards = $card->getPlayer()->hand->all();

        $blocks = [];
        foreach($cards as $handCard) {
            $blocks[] = new GiveEffect($handCard, new CostChangerEffect(-1));
        }

        return $blocks;
    }
}