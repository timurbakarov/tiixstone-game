<?php

namespace Tiixstone\Effect\Trigger;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Card;
use Tiixstone\Effect\AttackChangerEffect;
use Tiixstone\Effect\TriggerEffect;
use Tiixstone\Event;
use Tiixstone\Game;

class GurubashiBerserkerEffect extends TriggerEffect
{
    /**
     * @return string
     */
    public function triggerAtEvent(): string
    {
        return Event\BeforeTakeDamage::class;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event|Event\BeforeTakeDamage $event
     * @return bool
     */
    public function condition(Game $game, Card $card, Event $event): bool
    {
        return $card->isSelf($event->character);
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        return [new GiveEffect($card, new AttackChangerEffect(3))];
    }
}