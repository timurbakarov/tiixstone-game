<?php

namespace Tiixstone\Effect\Trigger;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Event;
use Tiixstone\Effect;
use Tiixstone\Manager\TargetManager;

class RagnarosEffect extends Effect\Trigger\EndOfYourTurn
{
    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        $game->logBlock('ragnaros-triggered');

        return [new Block\Damage\DealRandomDamage(TargetManager::ANY_OPPONENT_CHARACTER, 8, $this->owner())];
    }
}