<?php

namespace Tiixstone\Effect\Trigger;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;

/**
 * Эффект срабатывает в начале вашего хода
 *
 * Class StartOfYourTurn
 * @package Tiixstone\Effect\Trigger
 */
abstract class StartOfYourTurn extends Effect\TriggerEffect
{
    /**
     * @return string
     */
    final public function triggerAtEvent(): string
    {
        return Event\TurnStarted::class;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event\BeforeEndTurn $event
     * @return bool
     */
    final public function condition(Game $game, Card $card, Event $event): bool
    {
        return $event->player->id() == $card->getPlayer()->id();
    }
}