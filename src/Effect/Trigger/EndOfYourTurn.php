<?php

namespace Tiixstone\Effect\Trigger;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;

/**
 * Эффект срабатывает в конце вашего хода
 *
 * Class EndOfYourTurn
 * @package Tiixstone\Effect\Trigger
 */
abstract class EndOfYourTurn extends Effect\TriggerEffect
{
    /**
     * @return string
     */
    final public function triggerAtEvent(): string
    {
        return Event\BeforeEndTurn::class;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event\BeforeEndTurn $event
     * @return bool
     */
    final public function condition(Game $game, Card $card, Event $event): bool
    {
        return $event->player->id() == $card->getPlayer()->id();
    }

    /**
     * @param Game $game
     * @return array
     */
    public function afterActionBlocks(Game $game) : array
    {
        return $game->resolver->afterSequenceBlocks();
    }
}