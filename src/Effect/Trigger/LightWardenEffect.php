<?php

namespace Tiixstone\Effect\Trigger;

use Tiixstone\Block\GiveEffect;
use Tiixstone\Card;
use Tiixstone\Effect\AttackChangerEffect;
use Tiixstone\Effect\TriggerEffect;
use Tiixstone\Event;
use Tiixstone\Game;

class LightWardenEffect extends TriggerEffect
{

    /**
     * @return string
     */
    public function triggerAtEvent(): string
    {
        return Event\AfterHeal::class;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return bool
     */
    public function condition(Game $game, Card $card, Event $event): bool
    {
        return true;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        return [new GiveEffect($card, new AttackChangerEffect(2))];
    }
}