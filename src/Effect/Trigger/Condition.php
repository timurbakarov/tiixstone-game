<?php

namespace Tiixstone\Effect\Trigger;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;

abstract class Condition
{
    /**
     * @param Game $game
     * @param Card $owner
     * @param Event $event
     * @return array
     */
    abstract public function isSatisfiedBy(Game $game, Card $owner, Event $event) : array;
}