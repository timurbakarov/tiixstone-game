<?php

namespace Tiixstone\Effect\Trigger;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Event;
use Tiixstone\Effect;

class FloatingWatcherEffect extends Effect\TriggerEffect
{
    /**
     * @return string
     */
    public function triggerAtEvent(): string
    {
        return Event\BeforeTakeDamage::class;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event|Event\BeforeTakeDamage $event
     * @return bool
     */
    public function condition(Game $game, Card $card, Event $event): bool
    {
        return $card->getPlayer()->isCurrent($game) && $event->character->isSelf($card->getPlayer()->hero);
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        return [
            new Block\GiveEffect($card, new Effect\AttackChangerEffect(2)),
            new Block\GiveEffect($card, new Effect\HealthChangerEffect(2)),
        ];
    }
}