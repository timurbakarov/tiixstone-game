<?php

namespace Tiixstone\Effect\Trigger;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Block\DrawCard;

class ManaTideTotemEffect extends EndOfYourTurn
{
    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        return [new DrawCard($game->currentPlayer())];
    }
}