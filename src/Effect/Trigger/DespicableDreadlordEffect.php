<?php

namespace Tiixstone\Effect\Trigger;

use Tiixstone\Block\DealSplashDamage;
use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;
use Tiixstone\Manager\TargetManager;

class DespicableDreadlordEffect extends Effect\TriggerEffect
{
    /**
     * @return string
     */
    public function triggerAtEvent(): string
    {
        return Event\BeforeEndTurn::class;
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return bool
     */
    public function condition(Game $game, Card $card, Event $event): bool
    {
        return $card->getPlayer()->id() == $game->currentPlayer()->id();
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param Event $event
     * @return array
     */
    public function action(Game $game, Card $card, Event $event): array
    {
        return [new DealSplashDamage(TargetManager::ANY_OPPONENT_MINION, 1, $card)];
    }
}