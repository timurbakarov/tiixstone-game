<?php

namespace Tiixstone;

use Tiixstone\Card\Minion;
use Tiixstone\Card\Character;
use Tiixstone\Card\Attackable;

interface Battlecryable
{
    /**
     * @param Game $game
     * @param Attackable $attackable
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     * @return array
     */
    public function battlecry(Game $game, Attackable $attackable, Minion $positionMinion = null, Character $target = null);
}