<?php

namespace Tiixstone;

class Effects
{
    /**
     * @var array
     */
    public $effects = [];
	
	/**
     * @var array
     */
	private $auras = [];

    /**
     * @var array
     */
    public $globalEffects = [];

    /**
     * @param Game $game
     * @param array $effects
     * @return $this
     */
    public function addGlobalEffects(Game $game, array $effects)
    {
        foreach($effects as $effectName) {
            /** @var Effect $effect */
            $effect = new $effectName;

            $this->globalEffects[$effectName] = $effect;
        }

        return $this;
    }

    /**
     * @param string $name
     * @return bool|mixed
     */
    public function getGlobalEffect(string $name)
    {
        return $this->hasGlobalEffect($name) ? $this->globalEffects[$name] : false;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasGlobalEffect(string $name)
    {
        return isset($this->globalEffects[$name]);
    }

    /**
     * @param Game $game
     * @param Identifiable $card
     * @param $effectName
     * @return array
     * @throws Exception
     */
    public function add(Game $game, Identifiable $card = null, $effectName) : array
    {
        /** @var Effect $effect */
        if(is_string($effectName)) {
            $effect = new $effectName;
        } elseif(is_object($effectName)) {
            $effect = $effectName;
        } else {
            throw new Exception("Invalid effectName");
        }

        $effect->assignId();

        if($card) {
            $effect->setOwner($card);
        }
		
		if($effect instanceof Effect\Aura) {
			$this->auras[$effect->id()] = $effect;
		} else {
			$this->effects[$effect->id()] = $effect;
		}

		if($card AND $card instanceof Card) {
            return $effect->init($game, $card);
        } else {
		    return [];
        }
    }

    /**
     * @param Game $game
     * @param Card $card
     * @param array $effects
     * @return $this
     * @throws Exception
     */
    public function addMany(Game $game, Card $card, array $effects)
    {
        $blocks = [];

        foreach($effects as $effect) {
            $blocks = array_merge($blocks, $this->add($game, $card, $effect));
        }

        return $blocks;
    }

    /**
     * @param Card $card
     * @param Effect|null $except
     * @return $this
     */
    public function removeByCard(Card $card, Effect $except = null)
    {
        /** @var Effect $effect */
        foreach($this->effects() as $id => $effect) {
            if($effect->owner()->id() == $card->id()) {
                if(!$except OR $except->id() != $effect->id()) {
                    $this->removeByEffect($effect);
                }
            }
        }
		
		/** @var Effect $effect */
        foreach($this->auras() as $id => $effect) {
            if($effect->owner()->id() == $card->id()) {
                if(!$except OR $except->id() != $effect->id()) {
                    $this->removeByEffect($effect);
                }
            }
        }

        return $this;
    }

    /**
     * @param Effect $effect
     * @return $this
     */
    public function removeByEffect(Effect $effect)
    {
		if(isset($this->effects[$effect->id()])) {
			unset($this->effects[$effect->id()]);
		}
		
		if(isset($this->auras[$effect->id()])) {
			unset($this->auras[$effect->id()]);
		}

		if(method_exists($effect, 'onRemove'))
		
        return $this;
    }

    /**
     * @return array
     */
    public function effects()
    {
        return $this->effects;
    }
	
	public function auras()
	{
		return $this->auras;
	}

    /**
     * @param Card $card
     * @return array
     */
    public function getByCard(Card $card)
    {
        $result = [];

        /** @var Effect $effect */
        foreach($this->effects() as $effect) {
            if($effect->owner()->id() == $card->id()) {
                $result[] = $effect;
            }
        }

        /** @var Effect $effect */
        foreach($this->auras() as $effect) {
            if($effect->owner()->id() == $card->id()) {
                $result[] = $effect;
            }
        }

        return $result;
    }

    /**
     * @param string $effectName
     * @return array
     */
    public function getByEffect(string $effectName)
    {
        $effects = [];

        /** @var Effect $effect */
        foreach($this->effects() as $effect) {
            if(is_a($effect, $effectName)) {
                $effects[] = $effect;
            }
        }

        return $effects;
    }

    /**
     * @param Card $card
     * @param string $effectName
     * @return array
     */
    public function getEffectByCard(Card $card, string $effectName)
    {
        $effects = [];
        foreach($this->getByCard($card) as $effect) {
            if(is_a($effect, $effectName)) {
                $effects[] = $effect;
            }
        }

        return $effects;
    }

    /**
     * @param Card $card
     * @param string $effectName
     * @return bool
     */
    public function cardHasEffect(Card $card, string $effectName)
    {
        foreach($this->getByCard($card) as $effect) {
            if(is_a($effect, $effectName)) {
                return true;
            }
        }

        return false;
    }
}