<?php declare(strict_types=1);

namespace Tiixstone\Game;

class Settings
{
    /**
     * @return int
     */
    public function playerMaximumManaLimit() : int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function maxCardsInHand() : int
    {
        return 10;
    }

    /**
     * @return int
     */
    public function maxPlacesOnBoard() : int
    {
        return 7;
    }
}