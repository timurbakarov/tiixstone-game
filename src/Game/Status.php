<?php

namespace Tiixstone\Game;

use Tiixstone\Exception;

class Status
{
    const STATUS_PRE_START = 1;
    const STATUS_STARTED = 2;
    const STATUS_OVER = 3;

    /**
     * @var int
     */
    private $value;

    public function __construct()
    {
        $this->set(self::STATUS_PRE_START);
    }

    /**
     * @param int $status
     * @return $this
     * @throws Exception
     */
    public function set(int $status)
    {
        if(!in_array($status, $this->validStatuses())) {
            throw new Exception("Invalid status");
        }

        $this->value = $status;

        return $this;
    }

    /**
     * @param int $status
     * @return bool
     */
    public function is(int $status)
    {
        return $this->value == $status;
    }

    /**
     * @return array
     */
    private function validStatuses()
    {
        return [
            self::STATUS_PRE_START,
            self::STATUS_STARTED,
            self::STATUS_OVER,
        ];
    }
}