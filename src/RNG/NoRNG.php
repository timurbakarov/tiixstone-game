<?php

namespace Tiixstone\RNG;

use Tiixstone\RNG;

class NoRNG extends RNG
{
    /**
     * @var int
     */
    private $returnInt;

    /**
     * @param int $returnInt
     */
    public function __construct(int $returnInt)
    {
        $this->returnInt = $returnInt;
    }

    /**
     * @param int $min
     * @param int $max
     * @return int
     * @throws \Exception
     */
    public function int(int $min, int $max)
    {
        return $this->returnInt;
    }

    /**
     * @param array $data
     * @param int $count
     * @return mixed
     * @throws \Exception
     */
    public function randomFromArray(array $data, $count = 1)
    {
        return $data;
    }

    /**
     * @param array $data
     * @return array
     */
    public function shuffleArray(array $data)
    {
        return $data;
    }
}