<?php declare(strict_types=1);

namespace Tiixstone\Action;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Action;
use Tiixstone\Exception;

/**
 * Class PlayCard
 * @package Tiixstone\Action
 * @deprecated
 */
class PlayCard extends Action
{
    /**
     * @var int
     */
    private $cardId;

    /**
     * @var int
     */
    private $positionCardId;

    /**
     * @var int
     */
    private $targetId;

    /**
     * PlayCard constructor.
     *
     * @param int $card
     * @param Card|null $targetCard
     */
    public function __construct(string $cardId, string $positionCardId = null, string $targetId = null)
    {
        $this->cardId = $cardId;
        $this->positionCardId = $positionCardId;
        $this->targetId = $targetId;
    }

    /**
     * @param Game $game
     * @return array
     * @throws Exception
     * @throws bool
     */
    public function process(Game $game)
    {
        return PlayCardRefactored::fromCardsIds($game, $this->cardId, $this->positionCardId, $this->targetId)->process($game);
    }
}