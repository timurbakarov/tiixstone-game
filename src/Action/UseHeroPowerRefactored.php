<?php

namespace Tiixstone\Action;

use Tiixstone;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Event;
use Tiixstone\Action;
use Tiixstone\Exception;
use Tiixstone\Card\Character;

class UseHeroPowerRefactored extends Action
{
    /**
     * @var Character
     */
    private $target;

    public function __construct(Character $target = null)
    {
        $this->target = $target;
    }

    /**
     * @param Game $game
     * @param string|NULL $targetId
     * @return UseHeroPowerRefactored
     * @throws Exception
     */
    public static function fromCardsIds(Game $game, string $targetId = null)
    {
        $target = $targetId ? $game->cardsList->get($targetId) : null;

        return (new UseHeroPowerRefactored($target));
    }

    /**
     * @param Game $game
     * @return array
     */
    public function process(Game $game)
    {
        $power = $game->currentPlayer()->hero->power;

        return [
            new Block\Trigger(new Event\ValidateUseHeroPower($power, $this->target)),
            new Block\Trigger(new Event\BeforeUseHeroPower($power)),
            new Block\ClosureBlock(function(Game $game) {
                $game->logAction('hero-power-action', [
                    'player' => $game->currentPlayer(),
                    'hero-power' => $game->currentPlayer()->hero->power,
                    'target' => $this->target,
                ]);

                return [];
            }),
            $power->play($game, null, $this->target),
            new Block\Trigger(new Event\AfterUseHeroPower($power)),
        ];
    }
}