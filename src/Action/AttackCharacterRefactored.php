<?php declare(strict_types=1);

namespace Tiixstone\Action;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Event;
use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Exception;

class AttackCharacterRefactored extends Action
{
    /**
     * @var Card\Character
     */
    private $attacker;
    /**
     * @var Card\Character
     */
    private $target;

    public function __construct(Card\Character $attacker, Card\Character $target)
    {
        $this->attacker = $attacker;
        $this->target = $target;
    }

    /**
     * @param Game $game
     * @param string $attackerId
     * @param string|NULL $targetId
     * @return AttackCharacterRefactored
     * @throws Exception
     */
    public static function fromCardsIds(Game $game, string $attackerId, string $targetId = null)
    {
        /** @var Card\Character $attacker */
        $attacker = $game->cardsList->get($attackerId);

        /** @var Card\Character $target */
        $target = $game->cardsList->get($targetId);

        return (new AttackCharacterRefactored($attacker, $target));
    }


    /**
     * @param Game $game
     * @return array
     */
    public function process(Game $game)
    {
        return [
            new Block\Trigger(new Event\ValidateAttack($this->attacker, $this->target)),
            new Block\ClosureBlock($this->logAction()),
            new Block\Trigger(new Event\BeforeAttack($this->attacker, $this->target)),
            new Block\Combat($this->attacker, $this->target),
            new Block\Trigger(new Event\AfterAttack($this->attacker, $this->target)),
        ];
    }

    /**
     * @return \Closure
     */
    private function logAction()
    {
        return function(Game $game) {
            $game->logAction('attack-character', [
                'player' => $game->currentPlayer(),
                'attacker' => $this->attacker,
                'target' => $this->target,
            ]);

            return [];
        };
    }
}