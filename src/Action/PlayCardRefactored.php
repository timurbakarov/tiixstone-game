<?php declare(strict_types=1);

namespace Tiixstone\Action;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Block;
use Tiixstone\Action;
use Tiixstone\Exception;

class PlayCardRefactored extends Action
{
    /**
     * @var Card
     */
    private $card;

    /**
     * @var Card\Minion
     */
    private $positionMinion;

    /**
     * @var Card\Character
     */
    private $target;

    /**
     * PlayCard constructor.
     *
     * @param Card $card
     * @param Card\Minion|null $positionMinion
     * @param Card\Character|null $target
     */
    public function __construct(Card $card, Card\Minion $positionMinion = null, Card\Character $target = null)
    {
        $this->card = $card;
        $this->positionMinion = $positionMinion;
        $this->target = $target;
    }

    /**
     * @param Game $game
     * @param string $cardId
     * @param string|NULL $positionCardId
     * @param string|NULL $targetId
     * @return PlayCardRefactored
     * @throws Exception
     */
    public static function fromCardsIds(Game $game, string $cardId, string $positionCardId = null, string $targetId = null)
    {
        $card = $game->currentPlayer()->hand->get($cardId);

        $target = $targetId ? $game->cardsList->get($targetId) : null;

        if($positionCardId AND !$game->currentPlayer()->board->has($positionCardId)) {
            throw new Exception("Position minion does not exist");
        }

        /** @var Card\Minion $positionMinion */
        $positionMinion = $positionCardId
            ? $game->currentPlayer()->board->get($positionCardId)
            : null;

        return (new PlayCardRefactored($card, $positionMinion, $target));
    }

    /**
     * @param Game $game
     * @return array
     */
    public function process(Game $game)
    {
        return [
            new Block\Trigger(new Event\ValidatePlayCard($this->card, $this->positionMinion, $this->target)),
            new Block\ClosureBlock(function() use($game) {
                $game->logAction('play-card', [
                    'player' => $game->currentPlayer(),
                    'card' => $this->card,
                    'cost' => $this->card->cost->total(),
                    'target' => $this->target,
                ]);

                return [];
            }),
            new Block\Trigger(new Event\BeforePlayCard($this->card, $this->target)),
            new Block\RetrieveCardFromHand($game->currentPlayer(), $this->card),
            $this->card->play($game, $this->positionMinion, $this->target),
            new Block\Trigger(new Event\AfterPlayCard($this->card, $this->target)),
        ];
    }
}