<?php

namespace Tiixstone\Action;

use Tiixstone;
use Tiixstone\Game;
use Tiixstone\Action;
use Tiixstone\Exception;

class UseHeroPower extends Action
{
    /**
     * @var int
     */
    private $targetId;

    public function __construct(string $targetId = null)
    {
        $this->targetId = $targetId;
    }

    /**
     * @param Game $game
     * @return array
     * @throws Exception
     */
    public function process(Game $game)
    {
        return UseHeroPowerRefactored::fromCardsIds($game, $this->targetId)->process($game);
    }
}