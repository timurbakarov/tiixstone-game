<?php declare(strict_types=1);

namespace Tiixstone\Action;

use Tiixstone;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Event;
use Tiixstone\Action;

class EndTurn extends Action
{
    /**
     * @param Game $game
     * @return array
     */
    public function process(Game $game)
    {
        return [
            new Block\ClosureBlock(function(Game $game) {
                $game->logAction('end-turn', ['player' => $game->currentPlayer(), 'turnCount' => $game->turnNumber()]);

                return [];
            }),
            new Block\ClosureBlock(function(Game $game) {
                $game->logBlock('end-turn', ['player' => $game->currentPlayer(), 'turnCount' => $game->turnNumber()]);

                return [];
            }),
            new Block\Trigger(new Event\BeforeEndTurn($game->currentPlayer())),
            new Block\ChangeTurn(),
            new Block\Trigger(new Event\TurnStarted($game->idlePlayer())),
            new Block\ClosureBlock(function(Game $game) {
                $game->logBlock('turn-started', ['player' => $game->currentPlayer()]);

                return [];
            }),
        ];
    }
}