<?php declare(strict_types=1);

namespace Tiixstone\Action;

use Tiixstone\Game;
use Tiixstone\Action;

class AttackCharacter extends Action
{
    /**
     * @var int
     */
    private $attackerId;

    /**
     * @var int
     */
    private $targetId;

    public function __construct(string $attackerId, string $targetId)
    {
        $this->attackerId = $attackerId;
        $this->targetId = $targetId;
    }

    /**
     * @param Game $game
     * @return array
     * @throws \Tiixstone\Exception
     */
    public function process(Game $game)
    {
        return AttackCharacterRefactored::fromCardsIds($game, $this->attackerId, $this->targetId)->process($game);
    }
}