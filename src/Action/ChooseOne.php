<?php

namespace Tiixstone\Action;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Action;
use Tiixstone\Block\ClosureBlock;
use Tiixstone\Effect\ChooseCardEffect;
use Tiixstone\Block\Effect\RemoveEffect;

class ChooseOne extends Action
{
    /**
     * @var Card
     */
    private $card;

    /**
     * @param Card $card
     */
    public function __construct(Card $card)
    {
        $this->card = $card;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function process(Game $game)
    {
        /** @var ChooseCardEffect $effect */
        $effect = $game->effects->getByEffect(ChooseCardEffect::class)[0];

        $closure = $effect->closure();

        $game->logAction('choose-card', [
            'card' => $this->card,
        ]);

        return [
            new ClosureBlock($this->removeCardsFromList($game, $effect)),
            $this->runCardEffect($closure),
        ];
    }

    /**
     * @param \Closure $closure
     * @return mixed
     */
    private function runCardEffect(\Closure $closure)
    {
        return $closure($this->card);
    }

    /**
     * @param Game $game
     * @param ChooseCardEffect $effect
     * @return \Closure
     */
    private function removeCardsFromList(Game $game, ChooseCardEffect $effect)
    {
        return function() use($game, $effect) {
            foreach($effect->cards() as $card) {
                $game->cardsList->remove($card->id());
            }

            return [];
        };
    }

    /**
     * @param Game $game
     * @param string $cardId
     * @return ChooseOne
     * @throws \Tiixstone\Exception
     */
    public static function fromCardId(Game $game, string $cardId)
    {
        $card = $game->cardsList->get($cardId);

        return new self($card);
    }
}