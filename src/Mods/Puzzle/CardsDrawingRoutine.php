<?php

namespace Tiixstone\Mods\Puzzle;

use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Effect;
use Tiixstone\Cards\Item\TheCoin;
use Tiixstone\Block\DrawCard;
use Tiixstone\Block\PutCardInHand;

class CardsDrawingRoutine extends Effect
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            Event\GameStarted::class => 'gameStarted',
            Event\TurnStarted::class => 'turnStarted',
        ];
    }

    /**
     * @param Game $game
     * @return array
     */
    public function gameStarted(Game $game)
    {
        return [
            //new DrawCard($game->currentPlayer()),
            //new DrawCard($game->currentPlayer()),
            //new DrawCard($game->currentPlayer()),
            //
            //new DrawCard($game->idlePlayer()),
            //new DrawCard($game->idlePlayer()),
            //new DrawCard($game->idlePlayer()),
            //new DrawCard($game->idlePlayer()),
            //
            //new PutCardInHand($game->idlePlayer(), new TheCoin()),
        ];
    }

    /**
     * @param Game $game
     * @return array
     */
    public function turnStarted(Game $game)
    {
        if($game->turnNumber() == 1) {
            return [];
        }

        return [
            new DrawCard($game->currentPlayer()),
        ];
    }
}