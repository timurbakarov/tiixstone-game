<?php

namespace Tiixstone\Bot;

use Tiixstone\Card\Minion;
use Tiixstone\Game;
use Tiixstone\Action;

class SimplePeacefullBot extends AbstractBot
{
    /**
     * @param Game $game
     * @return Action
     */
    public function process(Game $game) : Action
    {
        $action = $this->playCardsFromHand($game);

        if(!$action) {
            //s$action = $this->tryAttack($game);
        }

        return $action ? $action : new Action\EndTurn();
    }

    /**
     * @param Game $game
     * @return bool
     */
    private function playCardsFromHand(Game $game)
    {
        foreach($game->currentPlayer()->hand->all() as $card) {
            $playCardAction = new Action\PlayCard($card->id());

            if($playCardAction->canBePlayed($game)) {
                return $playCardAction;
            }
        }

        return false;
    }

    /**
     * @param Game $game
     * @return bool|Action\PlayCard
     */
    private function tryAttack(Game $game)
    {
        /** @var Minion $minion */
        foreach($game->currentPlayer()->board->all() as $minion) {
            // пытаемся атаковать существом героя
            $attackCharacterAction = new Action\AttackCharacter($minion->id(), $game->idlePlayer()->hero->id());

            if($attackCharacterAction->canAttack($game)) {
                return $attackCharacterAction;
            }

            // пытаемся атаковать существом другие существа
            foreach($game->idlePlayer()->board->all() as $target) {
                $attackCharacterAction = new Action\AttackCharacter($minion->id(), $target->id());

                if($attackCharacterAction->canAttack($game)) {
                    return $attackCharacterAction;
                }
            }
        }

        // пытаемся героем героя оппонента
        $attackCharacterAction = new Action\AttackCharacter($game->currentPlayer()->hero->id(), $game->idlePlayer()->hero->id());

        if($attackCharacterAction->canAttack($game)) {
            return $attackCharacterAction;
        }

        // пытаемся атаковать героем существа
        foreach($game->idlePlayer()->board->all() as $target) {
            $attackCharacterAction = new Action\AttackCharacter($game->currentPlayer()->hero->id(), $target->id());

            if($attackCharacterAction->canAttack($game)) {
                return $attackCharacterAction;
            }


        }

        return false;
    }
}