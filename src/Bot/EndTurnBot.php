<?php

namespace Tiixstone\Bot;

use Tiixstone\Game;
use Tiixstone\Action;

class EndTurnBot extends AbstractBot
{
    /**
     * @param Game $game
     * @return Action
     */
    public function process(Game $game) : Action
    {
        return new Action\EndTurn();
    }
}