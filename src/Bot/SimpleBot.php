<?php

namespace Tiixstone\Bot;

use Tiixstone\Game;
use Tiixstone\Action;
use Tiixstone\Card\Minion;
use Tiixstone\Effect\FreezeEffect;
use Tiixstone\Effect\ImmuneEffect;
use Tiixstone\Effect\StealthEffect;
use Tiixstone\Effect\ElusiveEffect;
use Tiixstone\Effect\ChooseCardEffect;
use Tiixstone\Condition\PlayConditions;
use Tiixstone\Condition\AttackConditions;

class SimpleBot extends AbstractBot
{
    /**
     * @param Game $game
     * @return Action
     */
    public function process(Game $game) : Action
    {
        $action = $this->chooseCardAction($game);

        if(!$action) {
            $action = $this->playCardsFromHand($game);
        }

        if(!$action) {
            $action = $this->tryHeroPower($game);
        }

        if(!$action) {
            $action = $this->tryAttack($game);
        }

        return $action ? $action : new Action\EndTurn();
    }

    /**
     * @param Game $game
     * @return bool|Action\UseHeroPowerRefactored
     */
    private function tryHeroPower(Game $game)
    {
        $heroPower = $game->currentPlayer()->hero->power;

        if($heroPower->cost->total() > $game->currentPlayer()->availableMana()) {
            return false;
        }

        if($heroPower->usedThisTurn()) {
            return false;
        }

        $playConditions = $heroPower->playCondition();

        $result = $playConditions->check($game, $heroPower, null);

        if(!$result->hasError()) {
            return new Action\UseHeroPowerRefactored();
        }

        $targets = $game->targetManager->allCharacters($game);

        shuffle($targets);

        foreach($targets as $target) {
            $result = $playConditions->check($game, $heroPower, $target);

            if($result->success()) {
                return new Action\UseHeroPowerRefactored($target);
            }
        }

        return false;
    }

    /**
     * @param Game $game
     * @return bool|Action\ChooseOne
     */
    private function chooseCardAction(Game $game)
    {
        /** @var ChooseCardEffect $chooseCardEffect */
        $chooseCardEffect = $game->effects->getByEffect(ChooseCardEffect::class);

        if(!$chooseCardEffect) {
            return false;
        }

        $chooseCardEffect = $chooseCardEffect[0];

        $cards = $chooseCardEffect->cards();

        shuffle($cards);

        return new Action\ChooseOne($cards[0]);
    }

    /**
     * @param Game $game
     * @return bool|Action\PlayCardRefactored
     */
    private function playCardsFromHand(Game $game)
    {
        $playCard = null;
        $target = null;
        $position = null;

        $cards = $game->currentPlayer()->hand->all();

        shuffle($cards);

        foreach($cards as $card) {
            if($card->cost->total() > $game->currentPlayer()->availableMana()) {
                continue;
            }

            $playConditions = new PlayConditions();

            $targets = $game->targetManager->allCharacters($game);

            shuffle($targets);

            foreach($targets as $char) {
                if($playConditions->isSatisfiedBy($game, $card, $char)) {
                    $playCard = $card;
                    $target = $char;

                    break;
                }
            }

            if($playCard) {
                break;
            }

            // проверяем можно ли разыграть карту без цели
            if($playConditions->isSatisfiedBy($game, $card, null)) {
                $playCard = $card;

                break;
            }
        }

        if($playCard) {
            if($playCard->isMinion()) {
                $position = $this->getPositionToPlace($game, $playCard);
            }

            return new Action\PlayCardRefactored($playCard, $position, $target);
        } else {
            return false;
        }
    }

    private function getPositionToPlace(Game $game, Minion $playCard)
    {
        $availablePositions = $game->targetManager->allPlayerMinions($game, $playCard->getPlayer());

        shuffle($availablePositions);

        // Оставляем шанс, что не будет выбрана позиция
        $position = rand(0, count($availablePositions)) > 0 ? $availablePositions[0] : null;

        return $position;
    }

    /**
     * @param Game $game
     * @return bool|Action\AttackCharacterRefactored
     */
    private function tryAttack(Game $game)
    {
        $globalAttackConditions = new AttackConditions();

        $attacker = null;
        $defender = null;

        $chars = $game->targetManager->allFriendlyCharacters($game, $game->currentPlayer());

        shuffle($chars);

        /** @var  $character */
        foreach($chars as $character) {
            $attackCondition = $character->attackCondition();

            $targets = $game->targetManager->allEnemyCharacters($game, $game->currentPlayer());

            shuffle($targets);

            foreach($targets as $enemyCharacter) {
                if(!$globalAttackConditions->isSatisfiedBy($game, $character, $enemyCharacter)) {
                    continue;
                }

                $result = $attackCondition->check($game, $character, $enemyCharacter);

                if(!$result->hasError()) {
                    $attacker = $character;
                    $defender = $enemyCharacter;
                }
            }
        }

        if($attacker AND $defender) {
            return new Action\AttackCharacterRefactored($attacker, $defender);
        } else {
            return false;
        }
    }
}