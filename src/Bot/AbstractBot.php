<?php

namespace Tiixstone\Bot;

use Tiixstone\Action;
use Tiixstone\Game;

abstract class AbstractBot
{
    abstract public function process(Game $game) : Action;
}