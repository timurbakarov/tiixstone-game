<?php

namespace Tiixstone;

class Event extends Block
{
    public function run(Game $game) : array
    {
        throw new Exception("Method should not be called");
    }
}