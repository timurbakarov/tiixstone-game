<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Event;
use Tiixstone\Effect;

/**
 * Class Trigger
 * @package Tiixstone\Block
 */
class Trigger extends Block
{
    /**
     * @var Event
     */
    private $event;

    /**
     * Trigger constructor.
     * @param Event $event
     */
    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game): array
    {
        $game->logBlock('trigger-event', ['event' => $this->event]);

        $blocks = [];

        /** @var Effect $effect */
        foreach(array_merge($game->effects->globalEffects, $game->effects->effects()) as $effect) {
            if($handler = $this->getHandler($effect)) {
		        $blocks = array_merge(
		            $blocks,
                    $this->callEventHandler($effect, $handler, $game)
                );
            }
        }

        return $blocks;
    }

    /**
     * @return mixed
     */
    private function callEventHandler(Effect $effect, string $handler, Game $game)
    {
        return call_user_func_array([$effect, $handler], [$game, $this->event]);
    }

    /**
     * @param Effect $effect
     * @return bool
     */
    private function getHandler(Effect $effect)
    {
        $events = $effect->events();
        $eventName = get_class($this->event);

        return array_key_exists($eventName, $events) ? $events[$eventName] : false;
    }
}