<?php

namespace Tiixstone\Block;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Player;

/**
 * Class RetrieveCardFromHand
 * @package Tiixstone\Block
 */
class RetrieveCardFromHand extends Block
{
    /**
     * @var Player
     */
    private $player;

    /**
     * @var Card
     */
    private $card;

    /**
     * RetrieveCardFromHand constructor.
     * @param Player $player
     * @param Card $card
     */
    public function __construct(Player $player, Card $card)
    {
        $this->player = $player;
        $this->card = $card;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        $game->logBlock('retrieve-card-from-hand', ['player' => $this->player, 'card' => $this->card]);

        $this->player->hand->pull($this->card->id());

        return [];
    }
}