<?php

namespace Tiixstone\Block\HeroPower;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Player;

/**
 * Class Replace
 * @package Tiixstone\Block\HeroPower
 */
class Replace extends Block
{
    /**
     * @var Player
     */
    private $player;

    /**
     * @var Card\Power
     */
    private $power;

    /**
     * Replace constructor.
     * @param Player $player
     * @param Card\Power $power
     */
    public function __construct(Player $player, Card\Power $power)
    {
        $this->player = $player;
        $this->power = $power;
    }

    /**
     * @param Game $game
     * @return array
     * @throws \Tiixstone\Exception
     */
    public function run(Game $game): array
    {
        // remove effects of old hero power
        if($this->player->hero->power) {
            $game->effects->removeByCard($this->player->hero->power);
            $game->cardsList->remove($this->player->hero->power->id());
        }

        // set player for new hero power
        $this->power->setPlayer($this->player);

        // add to cards list
        $game->cardsList->append($this->power);

        // set hero power
        $this->player->hero->setPower($this->power);

        // effects
        $game->effects->addMany($game, $this->power, $this->power->effects());

        return [];
    }
}