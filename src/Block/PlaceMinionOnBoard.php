<?php

namespace Tiixstone\Block;

use Tiixstone\Exception;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Player;
use Tiixstone\Card\Minion;

/**
 * Class PlaceMinionOnBoard
 * @package Tiixstone\Block
 */
class PlaceMinionOnBoard extends Block
{
    /**
     * @var Player
     */
    private $player;

    /**
     * @var Minion
     */
    private $minion;

    /**
     * @var Minion
     */
    private $adjacentMinion;

    /**
     * @var string
     */
    private $side;

    /**
     * PlaceMinionOnBoard constructor.
     * @param Player $player
     * @param Minion $minion
     * @param Minion|NULL $adjacentMinion
     * @param string $side
     */
    public function __construct(Player $player, Minion $minion, Minion $adjacentMinion = null, string $side = 'left')
    {
        $this->player = $player;
        $this->minion = $minion;
        $this->adjacentMinion = $adjacentMinion;
        $this->side = $side;
    }

    /**
     * @param Game $game
     * @return array
     * @throws \Tiixstone\Exception
     */
    public function run(Game $game) : array
    {
        if($this->player->board->count() >= $game->settings->maxPlacesOnBoard()) {
            return [new MarkDestroyed($this->minion)];
        }

        // Существо ставится на поле относительно другого существа,
        // которое находится справа
        // Если поле не пустое и не указано существо, значит
        // существо надо поставить в крайнюю правую позицию
        if($this->player->board->isEmpty() OR !$this->adjacentMinion) {
            $this->player->board->append($this->minion);
        } else {
            if($this->side == 'left') {
                $this->player->board->addBefore($this->minion, $this->adjacentMinion->id());
            } elseif($this->side == 'right') {
                $positionMinion = $this->player->board->minionToPlaceNear($this->adjacentMinion);

                if($positionMinion) {
                    $this->player->board->addBefore($this->minion, $positionMinion->id());
                } else {
                    $this->player->board->append($this->minion);
                }

            } else {
                throw new Exception("Invalid side");
            }
        }

        $game->effects->addMany($game, $this->minion, $this->minion->boardEffects());

        $game->battlefield->add($this->minion);

        $this->minion->setPlayer($this->player);

        if(!$game->cardsList->has($this->minion->id())) {
            $game->cardsList->append($this->minion);
        }

        $game->logBlock('put-minion-into-battlefield', [
            'player' => $this->player,
            'minion' => $this->minion,
            'position' => $this->player->board->position($this->minion),
            'board' => [
                'player1' => $game->player1->board->all(),
                'player2' => $game->player2->board->all(),
            ],
        ]);

        return [];
    }
}