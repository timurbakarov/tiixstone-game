<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Player;
use Tiixstone\Card\Weapon;

/**
 * Class SetWeapon
 * @package Tiixstone\Block
 */
class SetWeapon extends Block
{
    /**
     * @var Player
     */
    private $player;

    /**
     * @var Weapon
     */
    private $weapon;

    /**
     * SetWeapon constructor.
     * @param Player $player
     * @param Weapon $weapon
     */
    public function __construct(Player $player, Weapon $weapon)
    {
        $this->player = $player;
        $this->weapon = $weapon;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game): array
    {
        $game->logBlock('set-weapon');

        $game->effects->addMany($game, $this->weapon, $this->weapon->equippedEffects());

        $this->player->hero->setWeapon($this->weapon);

        $this->weapon->setPlayer($this->player);

        if(!$game->cardsList->has($this->weapon->id())) {
            $game->cardsList->append($this->weapon);
        }

        return [];
    }
}