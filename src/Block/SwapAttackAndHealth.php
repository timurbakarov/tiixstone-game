<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Card\Minion;

/**
 * Class SwapAttackAndHealth
 * @package Tiixstone\Block
 */
class SwapAttackAndHealth extends Block
{
    /**
     * @var Minion
     */
    private $minion;

    /**
     * SwapAttackAndHealth constructor.
     * @param Minion $minion
     */
    public function __construct(Minion $minion)
    {
        $this->minion = $minion;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game): array
    {
        $attack = $this->minion->attack->total();
        $health = $this->minion->health->total();

        $blocks[] = $this->changeAttack($this->minion, $health);
        $blocks[] = $this->changeHealth($this->minion, $attack);

        foreach($game->effects->getByCard($this->minion) as $effect) {
            if($effect instanceof Effect\EnragedEffect) {
                continue;
            }

            if($effect instanceof Effect\Changer\AttackChanger OR $effect instanceof Effect\Changer\HealthChanger) {
                $game->effects->removeByEffect($effect);
            }
        }

        return $blocks;
    }

    /**
     * @param Minion $target
     * @param int $health
     * @return GiveEffect
     */
    private function changeAttack(Minion $target, int $health)
    {
        $difference = $health - $target->attack->current->get();

        return new GiveEffect($target, new Effect\AttackChangerEffect($difference));
    }

    /**
     * @param Minion $target
     * @param int $attack
     * @param int $health
     * @return GiveEffect
     */
    private function changeHealth(Minion $target, int $attack)
    {
        $target->health->current->set($target->defaultMaximumHealth());

        $difference = $attack - $target->health->current->get();

        return new GiveEffect($target, new Effect\HealthChangerEffect($difference));
    }
}