<?php

namespace Tiixstone\Block;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Character;
use Tiixstone\Event\AfterHeal;

/**
 * Class HealCharacter
 * @package Tiixstone\Block
 */
class HealCharacter extends Block
{
    /**
     * @var Character
     */
    private $character;

    /**
     * @var int
     */
    private $amount;

    /**
     * @var Card
     */
    private $source;

    /**
     * HealCharacter constructor.
     * @param Character $character
     * @param int $amount
     */
    public function __construct(Character $character, int $amount, Card $source = null)
    {
        $this->character = $character;
        $this->amount = $amount;
        $this->source = $source;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        $amount = $this->amount;

        if($this->isOverheal($amount)) {
            $amount = $this->character->health->maximum->get() - $this->character->health->current->get();
        }

        return [
            new ClosureBlock(function(Game $game) use($amount) {
                $this->character->health->heal($amount);

                return [];
            }),
            new ClosureBlock(function(Game $game) use($amount) {
                $game->logBlock('char-heal', [
                    'char' => $this->character,
                    'amount' => $this->amount,
                    'real' => $amount,
                ]);

                return [];
            }),
            new Trigger(new AfterHeal($this->character, $amount)),
        ];
    }

    /**
     * @return bool
     */
    private function isOverheal(int $amount)
    {
        return $this->character->health->current->get() + $amount > $this->character->health->maximum->get();
    }

    /**
     * @return Character
     */
    public function character()
    {
        return $this->character;
    }

    /**
     * @return int
     */
    public function amount()
    {
        return $this->amount;
    }

    /**
     * @return Card
     */
    public function source()
    {
        return $this->source;

    }
}