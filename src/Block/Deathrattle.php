<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Minion;
use Tiixstone\Effect\DeathrattleEffect;

/**
 * Class Deathrattle
 * @package Tiixstone\Block
 */
class Deathrattle extends Block
{
    /**
     * @var DeathrattleEffect
     */
    public $deathrattleEffect;

    /**
     * @var Minion
     */
    public $minion;

    /**
     * @var Minion
     */
    public $nextMinion;

    /**
     * Deathrattle constructor.
     * @param DeathrattleEffect $deathrattleEffect
     * @param Minion $minion
     * @param Minion|NULL $nextMinion
     */
    public function __construct(DeathrattleEffect $deathrattleEffect, Minion $minion, Minion $nextMinion = null)
    {
        $this->deathrattleEffect = $deathrattleEffect;
        $this->minion = $minion;
        $this->nextMinion = $nextMinion;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game): array
    {
        $game->logBlock('trigger-deathrattle', ['minion' => $this->minion]);

        return [$this->deathrattleEffect->deathrattle($game, $this->minion, $this->nextMinion)];
    }
}