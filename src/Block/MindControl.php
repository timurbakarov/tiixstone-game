<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Minion;

/**
 * Class MindControl
 * @package Tiixstone\Block
 */
class MindControl extends Block
{
    /**
     * @var Minion
     */
    private $minion;

    /**
     * MindControl constructor.
     * @param Minion $minion
     */
    public function __construct(Minion $minion)
    {
        $this->minion = $minion;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game): array
    {
        $game->logBlock('mind-control', ['minion' => $this->minion]);

        $enemyPlayer = $this->minion->getPlayer($game);
        $ourPlayer = $enemyPlayer->getOpponent($game);

        if($ourPlayer->board->isFull($game)) {
            return [
                new MarkDestroyed($this->minion),
            ];
        } else {
            return [
                // remove from opponent board
                new ClosureBlock(function(Game $game) {
                    $enemyPlayer = $this->minion->getPlayer($game);
                    $enemyPlayer->board->remove($this->minion->id());

                    return [];
                }),

                // place on our board
                new ClosureBlock(function(Game $game) {
                    $enemyPlayer = $this->minion->getPlayer($game);

                    $ourPlayer = $enemyPlayer->getOpponent($game);
                    $ourPlayer->board->append($this->minion);

                    $this->minion->setPlayer($ourPlayer);

                    return [];
                }),
            ];
        }
    }
}