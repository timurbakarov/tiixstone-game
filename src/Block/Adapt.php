<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Minion;
use Tiixstone\Effect\AdaptEffect;

class Adapt extends Block
{
    /**
     * @var Minion
     */
    private $minion;

    /**
     * @var int
     */
    private $times;

    /**
     * @param Minion $minion
     * @param int $times
     */
    public function __construct(Minion $minion, int $times = 1)
    {
        $this->minion = $minion;
        $this->times = $times;
    }

    /**
     * @param Game $game
     * @return array
     * @throws \Exception
     */
    public function run(Game $game): array
    {
        return [
            new GiveEffect($this->minion, new AdaptEffect($this->times)),
        ];
    }
}