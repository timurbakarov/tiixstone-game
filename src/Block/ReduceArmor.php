<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Hero;
use Tiixstone\Card\Character;

/**
 * Class ReduceArmor
 * @package Tiixstone\Block
 */
class ReduceArmor extends Block
{
    /**
     * @var Hero
     */
    private $hero;

    /**
     * @var int
     */
    private $amount;

    /**
     * ReduceArmor constructor.
     * @param Character $hero
     * @param int $amount
     */
    public function __construct(Character $hero, int $amount)
    {
        $this->hero = $hero;
        $this->amount = $amount;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        $game->logBlock('reduce-armor', ['char' => $this->hero, 'amount' => $this->amount]);

        $this->hero->armor->decrease($this->amount);

        return [];
    }
}