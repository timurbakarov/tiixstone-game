<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Power;
use Tiixstone\Card\Character;

/**
 * Class UsePower
 * @package Tiixstone\Block
 */
class UsePower extends Block
{
    /**
     * @var Power
     */
    private $power;

    /**
     * @var Character
     */
    private $target;

    /**
     * UsePower constructor.
     * @param Power $power
     * @param Character|NULL $target
     */
    public function __construct(Power $power, Character $target = null)
    {
        $this->power = $power;
        $this->target = $target;
    }
    
    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game): array
    {
        $game->logBlock('power-use');

        return $this->power->use($game, $this->target);
    }
}