<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Block;
use Tiixstone\Player;
use Tiixstone\Card\Minion;

/**
 * Class SummonMinion
 * @package Tiixstone\Block
 */
class SummonMinion extends Block
{
    /**
     * @var Player
     */
    private $player;

    /**
     * @var Minion
     */
    private $minion;

    /**
     * @var Minion
     */
    private $positionMinion;

    /**
     * @var string
     */
    private $side;

    /**
     * SummonMinion constructor.
     * @param Player $player
     * @param Minion $minion
     * @param Minion|NULL $positionMinion
     * @param string $side
     */
    public function __construct(Player $player, Minion $minion, Minion $positionMinion = null, string $side = 'left')
    {
        $this->player = $player;
        $this->minion = $minion;
        $this->positionMinion = $positionMinion;
        $this->side = $side;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        if($this->player->board->count() >= $game->settings->maxPlacesOnBoard()) {
            return [];
        }

        $game->logBlock('summon-minion', ['player' => $this->player, 'minion' => $this->minion]);
        
        return [
            new Trigger(new Event\BeforeSummon($this->player, $this->minion, $this->positionMinion, $this->side)),
            new PlaceMinionOnBoard($this->player, $this->minion, $this->positionMinion, $this->side),
            new Trigger(new Event\AfterSummon($this->player, $this->minion, $this->positionMinion, $this->side)),
        ];
    }
}