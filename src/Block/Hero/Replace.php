<?php

namespace Tiixstone\Block\Hero;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Hero;
use Tiixstone\Card\Alias\BloodFury;

/**
 * Class Replace
 * @package Tiixstone\Block\Hero
 */
class Replace extends Block
{
    /**
     * @var Hero
     */
    private $hero;

    public function __construct(Hero $hero)
    {
        $this->hero = $hero;
    }

    /**
     * @param Game $game
     * @return array
     * @throws \Tiixstone\Exception
     */
    public function run(Game $game): array
    {
        $game->logBlock('hero-replace');

        $game->effects->removeByCard($game->currentPlayer()->hero->power);
        $game->cardsList->remove($game->currentPlayer()->hero->power->id());

        if($game->currentPlayer()->hero->hasWeapon()) {
            $game->effects->removeByCard($game->currentPlayer()->hero->weapon);
            $game->cardsList->remove($game->currentPlayer()->hero->weapon->id());
        }

        $game->cardsList->remove($game->currentPlayer()->hero->id());
        $game->cardsList->append($this->hero);

        $this->setAttackTimesForNewHero($game, $game->currentPlayer()->hero);

        $game->currentPlayer()->hero = $this->hero;

        $this->hero->setPlayer($game->currentPlayer());

        return [
            new Block\HeroPower\Replace($game->currentPlayer(), $this->hero->defaultPower()),
            new Block\Weapon\EquipWeapon($game->currentPlayer(), BloodFury::create()),
        ];
    }

    /**
     * @param Game $game
     * @param Hero $oldHero
     * @return $this
     */
    private function setAttackTimesForNewHero(Game $game, Hero $oldHero)
    {
        for($i=0; $i<$game->attackTracker->attackTimes($oldHero); $i++) {
            $game->attackTracker->incrementAttackTimes($this->hero);
        }

        return $this;
    }
}