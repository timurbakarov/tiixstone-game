<?php

namespace Tiixstone\Block\Discard;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Block;

/**
 * Class DiscardCard
 * @package Tiixstone\Block
 */
class DiscardOne extends Block
{
    /**
     * @var Card
     */
    private $card;

    /**
     * DiscardCard constructor.
     * @param Card $card
     */
    public function __construct(Card $card)
    {
        $this->card = $card;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        $game->logBlock('discard-card', ['player' => $this->card->getPlayer(), 'card' => $this->card]);

        return [
            new Block\Trigger(new Event\BeforeDiscard($this->card)),
            new Block\RetrieveCardFromHand($this->card->getPlayer(), $this->card),
            new Block\Trigger(new Event\AfterDiscard($this->card)),
        ];
    }
}