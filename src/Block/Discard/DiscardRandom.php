<?php

namespace Tiixstone\Block\Discard;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Player;

/**
 * Class DiscardCard
 * @package Tiixstone\Block
 */
class DiscardRandom extends Block
{
    /**
     * @var Player
     */
    private $player;

    /**
     * DiscardCard constructor.
     * @param Card $card
     */
    public function __construct(Player $player)
    {
        $this->player = $player;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        $cards = $this->player->hand->all();

        if(!$cards) {
            return [];
        }

        return [new DiscardOne($game->RNG->randomFromArray($cards))];
    }
}