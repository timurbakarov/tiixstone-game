<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;

class AggregateBlock extends Block
{
    /**
     * @var array
     */
    private $blocks;

    /**
     * AggregateBlock constructor.
     * @param array $blocks
     */
    public function __construct(array $blocks)
    {
        $this->blocks = $blocks;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        return $this->blocks;
    }
}