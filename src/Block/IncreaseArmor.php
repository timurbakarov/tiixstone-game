<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Hero;

/**
 * Class IncreaseArmor
 * @package Tiixstone\Block
 */
class IncreaseArmor extends Block
{
    /**
     * @var Hero
     */
    private $hero;

    /**
     * @var int
     */
    private $amount;

    /**
     * IncreaseArmor constructor.
     * @param Hero $hero
     * @param int $amountIncrease
     */
    public function __construct(Hero $hero, int $amount)
    {
        $this->hero = $hero;
        $this->amount = $amount;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        $game->logBlock('increase-armor', ['char' => $this->hero, 'amount' => $this->amount]);
        
        $this->hero->armor->increase($this->amount);
        
        return [];
    }
}