<?php

namespace Tiixstone\Block\Weapon;

use Tiixstone\Game;
use Tiixstone\Event;
use Tiixstone\Block;
use Tiixstone\Player;
use Tiixstone\Card\Weapon;

/**
 * Class EquipWeapon
 * @package Tiixstone\Block\Weapon
 */
class EquipWeapon extends Block
{
    /**
     * @var Weapon
     */
    private $weapon;
    /**
     * @var Player
     */
    private $player;

    /**
     * EquipWeapon constructor.
     * @param Player $player
     * @param Weapon $weapon
     */
    public function __construct(Player $player, Weapon $weapon)
    {
        $this->player = $player;
        $this->weapon = $weapon;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        $game->logBlock('equip-weapon', ['char' => $this->player->hero, 'weapon' => $this->weapon]);

        return [
            new Block\Trigger(new Event\BeforeEquipWeapon($this->player, $this->weapon)),
            new Block\SetWeapon($this->player, $this->weapon),
            new Block\Trigger(new Event\AfterEquipWeapon($this->player, $this->weapon)),
        ];
    }
}