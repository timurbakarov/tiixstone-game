<?php

namespace Tiixstone\Block\Weapon;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Weapon;

/**
 * Class ReduceDurability
 * @package Tiixstone\Block\Weapon
 */
class ReduceDurability extends Block
{
    /**
     * @var Weapon
     */
    private $weapon;

    /**
     * @var int
     */
    private $amount;

    /**
     * ReduceDurability constructor.
     * @param Weapon $weapon
     * @param int $amount
     */
    public function __construct(Weapon $weapon, int $amount = 1)
    {
        $this->weapon = $weapon;
        $this->amount = $amount;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        $this->weapon->durability->current->decrease($this->amount);

        $game->logBlock('reduce-durability', ['weapon' => $this->weapon]);
        
        return [];
    }
}