<?php

namespace Tiixstone\Block;

use Tiixstone\Card;
use Tiixstone\Effect;
use Tiixstone\Game;
use Tiixstone\Block;

/**
 * Class Transform
 * @package Tiixstone\Block
 */
class Transform extends Block
{
    /**
     * @var Card\Minion
     */
    private $target;

    /**
     * @var Card\Minion
     */
    private $replace;

    /**
     * Transform constructor.
     * @param Card\Minion $target
     * @param Card\Minion $replace
     */
    public function __construct(Card\Minion $target, Card\Minion $replace)
    {
        $this->target = $target;
        $this->replace = $replace;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game): array
    {
        $game->logBlock('transform');

        /** @var  Card\Minion $copyMinion */
        $copyMinion = $this->replace->copy();

        $player = $this->target->getPlayer();
        $positionMinion = $player->board->minionToPlaceNear($this->target);

        return [
            new Battlefield\RemoveFromBattlefield($this->target),

            $this->copyStats($copyMinion),

            $this->copyEffects($game, $copyMinion),

            $this->copyAttackStats($copyMinion),

            new PlaceMinionOnBoard($player, $copyMinion, $positionMinion),
        ];
    }

    /**
     * @param Card\Minion $copyMinion
     * @return ClosureBlock
     */
    private function copyAttackStats(Card\Minion $copyMinion)
    {
        return new ClosureBlock(function(Game $game) use($copyMinion) {
            $isJustSummoned = $game->attackTracker->isJustSummoned($this->replace);
            $attackTimes = $game->attackTracker->attackTimes($this->replace);

            $game->attackTracker->setJustSummoned($copyMinion, is_bool($isJustSummoned) ? $isJustSummoned : false);
            $game->attackTracker->setAttackTimes($copyMinion, $attackTimes > 0 ? $attackTimes : 0);

            return [];
        });
    }

    /**
     * @param Game $game
     * @param Card\Minion $copyMinion
     * @return array
     */
    private function copyEffects(Game $game, Card\Minion $copyMinion)
    {
        return array_map(function(Effect $effect) use($copyMinion) {
            return new Block\GiveEffect($copyMinion, clone $effect);
        }, $this->replaceEffects($game));
    }

    /**
     * @param Card\Minion $copyMinion
     * @return ClosureBlock
     */
    private function copyStats(Card\Minion $copyMinion)
    {
        return new ClosureBlock(function(Game $game) use($copyMinion) {
            $copyMinion->attack->current->set($this->replace->attack->current->get());
            $copyMinion->attack->buff->set($this->replace->attack->buff->get());
            $copyMinion->health->current->set($this->replace->health->current->get());
            $copyMinion->health->maximum->set($this->replace->health->maximum->get());
            $copyMinion->health->buff->set($this->replace->health->buff->get());

            return [];
        });
    }

    /**
     * @param Game $game
     * @return array
     */
    private function replaceEffects(Game $game)
    {
        return $game->effects->getByCard($this->replace);
    }
}