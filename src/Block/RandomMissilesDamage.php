<?php

namespace Tiixstone\Block;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;

/**
 * Class RandomMissilesDamage
 * @package Tiixstone\Block
 */
class RandomMissilesDamage extends Block
{
    /**
     * @var
     */
	private $targetType;

    /**
     * @var int
     */
	private $damage;

    /**
     * @var int
     */
    private $misslesCount;

    /**
     * @var Card
     */
    private $souce;

    /**
     * RandomMissilesDamage constructor.
     * @param $targetType
     * @param int $damage
     * @param int $misslesCount
     * @param Card $souce
     */
    public function __construct($targetType, int $damage, int $misslesCount, Card $souce)
	{
		$this->targetType = $targetType;
		$this->damage = $damage;
        $this->misslesCount = $misslesCount;
        $this->souce = $souce;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        $blocks = [];

        for($i=0; $i<$this->misslesCount; $i++) {
            $blocks[] = new Block\Damage\DealRandomDamage($this->targetType, $this->damage, $this->souce);
        }

        return $blocks;
    }
}