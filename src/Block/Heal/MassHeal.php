<?php

namespace Tiixstone\Block\Heal;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;

/**
 * Class MassHeal
 * @package Tiixstone\Block\Heal
 */
class MassHeal extends Block
{
    /**
     * @var int
     */
    private $targetType;

    /**
     * @var int
     */
    private $amount;

    /**
     * @var Card
     */
    private $source;

    /**
     * MassHeal constructor.
     * @param int $targetType
     * @param int $amount
     * @param Card $source
     */
    public function __construct(int $targetType, int $amount, Card $source)
    {
        $this->targetType = $targetType;
        $this->amount = $amount;
        $this->source = $source;
    }

    /**
     * @param Game $game
     * @return array
     * @throws \Tiixstone\Exception
     */
    public function run(Game $game): array
    {
        $blocks = [];

        /** @var Card\Character $character */
        foreach ($game->targetManager->targets($game, $this->targetType, $this->source->getPlayer()) as $character) {
            $blocks[] = new Block\HealCharacter($character, $this->amount);
        }

        return $blocks;
    }
}