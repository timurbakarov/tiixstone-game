<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Player;

/**
 * Class DrawCard
 * @package Tiixstone\Block
 */
class DrawCard extends Block
{
    /**
     * @var Player
     */
    private $player;
    /**
     * @var int
     */
    private $times;

    /**
     * DrawCard constructor.
     * @param Player $player
     * @param int $times
     */
    public function __construct(Player $player, int $times = 1)
    {
        $this->player = $player;
        $this->times = $times;
    }

    /**
     * @param Game $game
     * @return array
     * @throws \Tiixstone\Exception
     */
    public function run(Game $game) : array
    {
        $blocks = [];

        if($this->times == 1) {
            $blocks[] = $this->drawCardBlock($game);
        } else {
            for($i=0; $i<$this->times; $i++) {
                $blocks[] = new DrawCard($this->player);
            }
        }

        return $blocks;
    }

    /**
     * @param Game $game
     * @return array|TakeFatigue
     * @throws \Tiixstone\Exception
     */
    public function drawCardBlock(Game $game)
    {
        if($this->noCardsInDeck()) {
            return new TakeFatigue($this->player);
        }

        $blocks = [];

        $card = $this->takeCardFromDeck();

        $blocks[] = new ClosureBlock(function(Game $game) use($card) {
            $game->logBlock('draw-card', [
                'player' => $this->player,
                'card' => $card,
            ]);

            return [];
        });

        if($this->handIsFull($game)) {
            $blocks[] = new Overdraw($this->player, $card);
        } else {
            $blocks[] = new PutCardInHand($this->player, $card);
        }

        return $blocks;
    }

    /**
     * @return bool
     */
    private function noCardsInDeck()
    {
        return $this->player->deck->count() <= 0;
    }

    /**
     * @return \Tiixstone\Card
     * @throws \Tiixstone\Exception
     */
    private function takeCardFromDeck()
    {
        return $this->player->deck->shift();
    }

    /**
     * @param Game $game
     * @return bool
     */
    private function handIsFull(Game $game)
    {
        return $this->player->hand->count() >= $game->settings->maxCardsInHand();
    }
}