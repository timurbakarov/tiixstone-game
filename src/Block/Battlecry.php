<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Minion;
use Tiixstone\Battlecryable;
use Tiixstone\Card\Character;

class Battlecry extends Block
{
    /**
     * @var Minion
     */
    public $minion;

    /**
     * @var Minion
     */
    public $positionMinion;

    /**
     * @var Character
     */
    public $target;

    /**
     * Battlecry constructor.
     * @param Battlecryable $minion
     * @param Minion|NULL $positionMinion
     * @param Character|NULL $target
     */
    public function __construct(Battlecryable $minion, Minion $positionMinion = null, Character $target = null)
    {
        $this->minion = $minion;
        $this->positionMinion = $positionMinion;
        $this->target = $target;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game): array
    {
        return [
            new ClosureBlock(function(Game $game) {
                $game->logBlock('battlecry', ['card' => $this->minion]);

                return [];
            }),
            $this->minion->battlecry($game, $this->minion, $this->positionMinion, $this->target),
        ];
    }
}