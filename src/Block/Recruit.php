<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Player;
use Tiixstone\Card\Minion;

/**
 * Class Recruit
 * @package Tiixstone\Block
 */
class Recruit extends Block
{
    /**
     * @var Player
     */
    private $player;
    /**
     * @var \Closure
     */
    private $condition;

    /**
     * Recruit constructor.
     * @param Player $player
     * @param \Closure $condition
     */
    public function __construct(Player $player, \Closure $condition)
    {
        $this->player = $player;
        $this->condition = $condition;
    }

    /**
     * @param Game $game
     * @return array
     * @throws \Tiixstone\Exception
     */
    public function run(Game $game): array
    {
        /** @var Minion[] $minions */
        $minions = call_user_func($this->condition, $game);

        if(!$minions) {
            return [];
        }

        $blocks = [];

        if(!is_array($minions)) {
            $minions = [$minions];
        }

        foreach($minions as $minion) {
            $this->player->deck->remove($minion->id());

            $blocks[] = new SummonMinion($this->player, $minion);
        }

        return $blocks;
    }
}