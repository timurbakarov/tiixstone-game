<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Player;

class Overload extends Block
{
    /**
     * @var Player
     */
    private $player;
    /**
     * @var int
     */
    private $amount;

    public function __construct(Player $player, int $amount)
    {
        $this->player = $player;
        $this->amount = $amount;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game): array
    {
        $game->logBlock('overload', ['player' => $this->player, 'amount' => $this->amount]);

        return [];
    }
}