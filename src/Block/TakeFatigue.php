<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Player;

/**
 * Class TakeFatigue
 * @package Tiixstone\Block
 */
class TakeFatigue extends Block
{
    /**
     * @var Player
     */
    private $player;

    /**
     * TakeFatigue constructor.
     * @param Player $player
     */
    public function __construct(Player $player)
    {
        $this->player = $player;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        $fatigue = $this->player->fatigue();

        $this->player->incrementFatigue();

        $game->logBlock('take-fatigue', [
            'player' => $this->player,
            'char' => $this->player->hero,
            'fatigue' => $fatigue,
        ]);

        return [new TakeDamage($this->player->hero, $fatigue)];
    }
}