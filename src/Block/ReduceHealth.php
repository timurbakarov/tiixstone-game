<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Character;

/**
 * Class ReduceHealth
 * @package Tiixstone\Block
 */
class ReduceHealth extends Block
{
    /**
     * @var Character
     */
    private $character;

    /**
     * @var int
     */
    private $amount;

    /**
     * ReduceHealth constructor.
     * @param Character $character
     * @param int $amount
     */
    public function __construct(Character $character, int $amount)
    {
        $this->character = $character;
        $this->amount = $amount;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        $game->logBlock('reduce-health', ['char' => $this->character, 'amount' => $this->amount]);

        $this->character->health->damage($this->amount);
        
        return [];
    }
}