<?php

namespace Tiixstone\Block;

use Tiixstone;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Player;
use Tiixstone\Exception;

/**
 * Class SetManaCrystals
 * @package Tiixstone\Block
 */
class SetManaCrystals extends Block
{
    /**
     * @var Player
     */
    private $player;

    /**
     * @var int
     */
    private $amount;

    /**
     * SetManaCrystals constructor.
     * @param Player $player
     * @param int $amount
     */
    public function __construct(Player $player, int $amount)
    {
        $this->player = $player;
        $this->amount = $amount;
    }

    /**
     * @param Game $game
     * @return array
     * @throws \Tiixstone\Exception
     */
    public function run(Game $game) : array
    {
        if($this->amount < 0) {
            throw new Exception("Maximum mana must be zero or greater than zero");
        }

        if($this->amount > $game->settings->playerMaximumManaLimit()) {
            throw new Exception("Maximum mana must be lower than " . $game->settings->playerMaximumManaLimit());
        }

        $this->player->setMaximumMana($this->amount);

        $game->logBlock('mana-crystals-changed', [
            'player' => $this->player,
            'amount' => $this->amount,
        ]);
        
        return [];
    }
}