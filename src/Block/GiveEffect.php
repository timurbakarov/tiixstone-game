<?php

namespace Tiixstone\Block;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Identifiable;

/**
 * Class GiveEffect
 * @package Tiixstone\Block
 */
class GiveEffect extends Block
{
    /**
     * @var Card
     */
    private $card;

    /**
     * @var string|Effect
     */
    private $effectName;

    /**
     * GiveEffect constructor.
     * @param Identifiable $card
     * @param $effectName
     */
    public function __construct(Identifiable $card, $effectName)
    {
        $this->card = $card;
        $this->effectName = $effectName;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game): array
    {
        return [
            new ClosureBlock(function() use($game) {
                $game->logBlock('give-effect-' . (is_object($this->effectName) ? get_class($this->effectName) : $this->effectName));

                return [];
            }),
            $game->effects->add($game, $this->card, $this->effectName)
        ];
    }
}