<?php

namespace Tiixstone\Block\Deck;

use Tiixstone\Block;
use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Player;

class AddCardToDeck extends Block
{
    /**
     * @var Player
     */
    private $player;

    /**
     * @var Card
     */
    private $card;

    public function __construct(Player $player, Card $card)
    {
        $this->player = $player;
        $this->card = $card;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game): array
    {
        $this->player->deck->append($this->card);
        $this->player->deck->shuffle();

        $game->cardsList->append($this->card);

        return [];
    }
}