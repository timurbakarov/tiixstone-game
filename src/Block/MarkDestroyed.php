<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Minion;
use Tiixstone\Card\Destroyable;

/**
 * Class MarkDestroyed
 * @package Tiixstone\Block
 */
class MarkDestroyed extends Block
{
    /**
     * @var Minion
     */
    private $card;

    /**
     * MarkDestroyed constructor.
     * @param Destroyable $card
     */
    public function __construct(Destroyable $card)
    {
        $this->card = $card;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        $game->logBlock('minion-marked-destroyed', ['minion' => $this->card]);

        $this->card->markDestroyed();

        return [];
    }
}