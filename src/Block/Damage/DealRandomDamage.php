<?php

namespace Tiixstone\Block\Damage;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;

/**
 * Class DealRandomDamage
 * @package Tiixstone\Block\Damage
 */
class DealRandomDamage extends Block
{
    /**
     * @var int
     */
    private $targetType;

    /**
     * @var int
     */
    private $damage;
    /**
     * @var Card
     */
    private $source;

    /**
     * DealRandomDamage constructor.
     * @param int $targetType
     * @param int $damage
     * @param Card|NULL $source
     */
    public function __construct(int $targetType, int $damage, Card $source = null)
    {
        $this->targetType = $targetType;
        $this->damage = $damage;
        $this->source = $source;
    }

    /**
     * @param Game $game
     * @return array
     * @throws \Tiixstone\Exception
     */
    public function run(Game $game): array
    {
        $targets = $game->targetManager->targets($game, $this->targetType, $this->source ? $this->source->getPlayer() : null);

        $targets = array_filter($targets, function(Card\Character $minion) {
            if($this->source AND $this->source->isMinion()) {
                if($this->source->isSelf($minion)) {
                    return false;
                }
            }

            return !$minion->health->isDead();
        });

        if($targets) {
            $key = array_rand($targets);

            $target = $targets[$key];

            return [
                new Block\ClosureBlock(function() use($game) {
                    $game->logBlock('random-damage');

                    return [];
                }),
                new Block\TakeDamage($target, $this->damage, $this->source),
            ];
        }

        return [];
    }
}