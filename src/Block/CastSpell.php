<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Spell;
use Tiixstone\Card\Character;

class CastSpell extends Block
{
    /**
     * @var Spell
     */
    public $spell;

    /**
     * @var Character
     */
    public $target;

    /**
     * CastSpell constructor.
     * @param Spell $spell
     * @param Character|NULL $target
     */
    public function __construct(Spell $spell, Character $target = null)
    {
        $this->spell = $spell;
        $this->target = $target;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        return $this->spell->cast($game, $this->target);
    }
}