<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Exception;
use Tiixstone\Card\Character;
use Tiixstone\Effect\DivineShieldEffect;

/**
 * Class PopupDivineShield
 * @package Tiixstone\Block
 */
class PopupDivineShield extends Block
{
    /**
     * @var Character
     */
    private $character;

    /**
     * PopupDivineShield constructor.
     * @param Character $character
     */
    public function __construct(Character $character)
    {
        $this->character = $character;
    }

    /**
     * @param Game $game
     * @return array
     * @throws Exception
     */
    public function run(Game $game): array
    {
        $game->logBlock('popup-divine-shield');

        if(!$divineShieldEffects = $game->effects->getEffectByCard($this->character, DivineShieldEffect::class)) {
            throw new Exception("Character does not have divine shield and it can not be popped up");
        }

        foreach($divineShieldEffects as $divineShieldEffect) {
            $game->effects->removeByEffect($divineShieldEffect);
        }

        return [];
    }
}