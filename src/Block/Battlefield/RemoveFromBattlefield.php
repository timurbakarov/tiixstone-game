<?php

namespace Tiixstone\Block\Battlefield;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Minion;

/**
 * Class RemoveFromBattlefield
 * @package Tiixstone\Block\Battlefield
 */
class RemoveFromBattlefield extends Block
{
    /**
     * @var Minion
     */
    private $minion;

    /**
     * RemoveFromBattlefield constructor.
     * @param Minion $minion
     */
    public function __construct(Minion $minion)
    {
        $this->minion = $minion;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game): array
    {
        $this->minion->getPlayer()->board->pull($this->minion->id());

        $game->battlefield->remove($this->minion);

        $game->effects->removeByCard($this->minion);

        return [];
    }
}