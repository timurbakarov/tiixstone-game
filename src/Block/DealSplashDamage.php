<?php

namespace Tiixstone\Block;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect\Target;

/**
 * Class DealSplashDamage
 * @package Tiixstone\Block
 */
class DealSplashDamage extends Block
{
    /**
     * @var int
     */
    private $targetType;

    /**
     * @var int
     */
    private $damage;

    /**
     * @var Card
     */
    private $source;

    /**
     * @var Target
     */
    private $targetCondition;

    /**
     * DealSplashDamage constructor.
     * @param int $targetType
     * @param int $damage
     * @param Card $source
     * @param Target|NULL $targetCondition
     */
    public function __construct(int $targetType, int $damage, Card $source, Target $targetCondition = null)
    {
        $this->targetType = $targetType;
        $this->damage = $damage;
        $this->source = $source;
        $this->targetCondition = $targetCondition;
    }

    /**
     * @param Game $game
     * @return array
     * @throws \Tiixstone\Exception
     */
    public function run(Game $game): array
    {
        $blocks = [];

        $targets = $game->targetManager->targets($game, $this->targetType, $this->source->getPlayer());

        foreach($targets as $target) {
            if($this->targetCondition AND !$this->targetCondition->condition($game, $this->source, $target)) {
                continue;
            }

            $blocks[] = new TakeDamage($target, $this->damage, $this->source);
        }

        return $blocks;
    }
}