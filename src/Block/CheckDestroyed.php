<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Destroyable;

/**
 * Class CheckDestroyed
 * @package Tiixstone\Block
 */
class CheckDestroyed extends Block
{
    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        $blocks = [];

        /** @var Destroyable $destroyable */
        foreach($game->targetManager->destroyable($game) as $destroyable) {
            if(!$destroyable->isDestroyed()) {
                continue;
            }

            $blocks = array_merge($blocks, $destroyable->destroy($game));
        }

        return $blocks;
    }
}