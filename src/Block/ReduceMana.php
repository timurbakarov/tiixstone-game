<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Player;

/**
 * Class ReduceMana
 * @package Tiixstone\Block
 */
class ReduceMana extends Block
{
    /**
     * @var Player
     */
    private $player;

    /**
     * @var int
     */
    private $amount;

    /**
     * ReduceMana constructor.
     * @param Player $player
     * @param int $amount
     */
    public function __construct(Player $player, int $amount)
    {
        $this->player = $player;
        $this->amount = $amount;
    }

    /**
     * @param Game $game
     * @return array
     * @throws \Tiixstone\Exception
     */
    public function run(Game $game) : array
    {
        $game->logBlock('reduce-mana', ['player' => $this->player, 'amount' => $this->amount]);

        if($this->player->availableMana() - $this->amount < 0) {
            $this->amount = $this->amount - ($this->amount - $this->player->availableMana());
        }

        return [
            new SetManaAvailable($this->player, $this->player->availableMana() - $this->amount),
        ];
    }
}