<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;

/**
 * Class ClosureBlock
 * @package Tiixstone\Block
 */
class ClosureBlock extends Block
{
    /**
     * @var \Closure
     */
    private $closure;

    /**
     * ClosureBlock constructor.
     * @param \Closure $closure
     */
    public function __construct(\Closure $closure)
    {
        $this->closure = $closure;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        $closure = $this->closure;

        return $closure($game);
    }
}