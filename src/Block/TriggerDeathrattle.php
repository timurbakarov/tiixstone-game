<?php

namespace Tiixstone\Block;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect\DeathrattleEffect;

class TriggerDeathrattle extends Block
{
    /**
     * @var Card|Card\Minion
     */
    private $card;

    /**
     * @param Card $card
     */
    public function __construct(Card $card)
    {
        $this->card = $card;
    }

    public function run(Game $game): array
    {
        $blocks = [];

        /** @var DeathrattleEffect $effect */
        foreach($game->effects->getByCard($this->card) as $effect) {
            if($effect instanceof DeathrattleEffect) {
                $blocks[] = $effect->deathrattle($game, $this->card, $this->card->getPlayer()->board->minionToPlaceNear($this->card));
            }
        }

        return $blocks;
    }
}