<?php

namespace Tiixstone\Block;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Player;
use Tiixstone\Exception;

/**
 * Class PutCardInHand
 * @package Tiixstone\Block
 */
class PutCardInHand extends Block
{
    /**
     * @var Player
     */
    private $player;

    /**
     * @var Card
     */
    private $card;

    /**
     * @var Card
     */
    private $nextTo;

    /**
     * PutCardInHand constructor.
     * @param Player $player
     * @param Card $card
     * @param Card|NULL $nextTo
     */
    public function __construct(Player $player, Card $card, Card $nextTo = null)
    {
        $this->player = $player;
        $this->card = $card;
        $this->nextTo = $nextTo;
    }

    /**
     * @param Game $game
     * @return array
     * @throws Exception
     */
    public function run(Game $game) : array
    {
        if($this->player->hand->count() >= $game->settings->maxCardsInHand()) {
            throw new Exception("Max cards in hand " . $game->settings->maxCardsInHand());
        }

        $game->logBlock('put-card-in-hand', ['player' => $this->player, 'card' => $this->card]);

        if($this->nextTo) {
            $this->player->hand->addBefore($this->card, $this->nextTo->id());
        } else {
            $this->player->hand->append($this->card);
        }
		
        $game->effects->addMany($game, $this->card, $this->card->handEffects());

        $this->card->setPlayer($this->player);

        if(!$game->cardsList->has($this->card->id())) {
            $game->cardsList->append($this->card);
        }
        
        return [];
    }
}