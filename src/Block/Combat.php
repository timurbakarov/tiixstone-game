<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Card;
use Tiixstone\Block;
use Tiixstone\Block\Weapon\ReduceDurability;

/**
 * Class Combat
 * @package Tiixstone\Block
 */
class Combat extends Block
{
    /**
     * @var Card\Character
     */
    private $attacker;

    /**
     * @var Card\Character
     */
    private $defender;

    public function __construct(Card\Character $attacker, Card\Character $defender)
    {
        $this->attacker = $attacker;
        $this->defender = $defender;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        $game->logBlock('combat', ['attacker' => $this->attacker, 'defender' => $this->defender]);

        $damage = $this->attacker->attack->total();

        $blocks[] = new TakeDamage($this->defender, $damage, $this->attacker);

        if($this->defender instanceof Card\Minion) {
            $damage = $this->defender->attack->total();
            $blocks[] = new TakeDamage($this->attacker, $damage, $this->defender);
        }

        if($this->attacker instanceof Card\Hero AND $this->attacker->weapon) {
            $blocks[] = new ReduceDurability($this->attacker->weapon);
        }

        return $blocks;
    }
}