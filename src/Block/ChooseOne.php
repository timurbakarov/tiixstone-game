<?php

namespace Tiixstone\Block;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect\ChooseCardEffect;

class ChooseOne extends Block
{
    /**
     * @var \Closure
     */
    private $closure;

    /**
     * @var Card[]
     */
    private $cards;

    /**
     * @param \Closure $closure
     * @param Card[] ...$cards
     */
    public function __construct(\Closure $closure, Card ...$cards)
    {
        $this->closure = $closure;
        $this->cards = $cards;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game): array
    {
        $chooseCardEffect = new ChooseCardEffect($this->closure, ...$this->cards);

        return [
            new GiveEffect(
                $game->currentPlayer()->hero,
                $chooseCardEffect
            ),
            new StopBlockResolving(function($leftBlocks) use($chooseCardEffect) {
                $chooseCardEffect->setLeftBlocks($leftBlocks);
            }),
        ];
    }
}