<?php

namespace Tiixstone\Block\Effect;

use Tiixstone\Effect;
use Tiixstone\Game;
use Tiixstone\Block;

/**
 * Class RemoveEffect
 * @package Tiixstone\Block\Effect
 */
class RemoveEffect extends Block
{
    /**
     * @var array
     */
    private $effects;

    /**
     * @var Effect
     */
    private $except;

    /**
     * RemoveEffect constructor.
     * @param array $effects
     * @param Effect|null $except
     */
    public function __construct(array $effects, Effect $except = null)
    {
        $this->effects = $effects;
        $this->except = $except;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game): array
    {
        $blocks = [];

        foreach($this->effects as $effect) {
            if($this->except AND $this->except->id() == $effect->id()) {
                continue;
            }

            $blocks = array_merge($blocks, call_user_func_array([$effect, 'onRemove'], [$game]));

            $game->effects->removeByEffect($effect);
        }

        return $blocks;
    }
}