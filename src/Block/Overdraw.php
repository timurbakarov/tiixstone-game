<?php

namespace Tiixstone\Block;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Player;

/**
 * Class Overdraw
 * @package Tiixstone\Block
 */
class Overdraw extends Block
{
    /**
     * @var Player
     */
    private $player;

    /**
     * @var Card
     */
    private $card;

    /**
     * Overdraw constructor.
     * @param Player $player
     * @param Card $card
     */
    public function __construct(Player $player, Card $card)
    {
        $this->player = $player;
        $this->card = $card;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game): array
    {
        $game->logBlock('overdraw-block', ['card' => $this->card]);

        return [];
    }
}