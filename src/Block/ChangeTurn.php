<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;

class ChangeTurn extends Block
{
    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        $game->setTurnNumber($game->turnNumber() + 1);

        $game->logBlock('turn-changed', ['player' => $game->currentPlayer()]);

        return [];
    }
}