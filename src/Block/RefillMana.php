<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Player;

/**
 * Class RefillMana
 * @package Tiixstone\Block
 */
class RefillMana extends Block
{
    /**
     * @var Player
     */
    private $player;

    /**
     * RefillMana constructor.
     * @param Player $player
     */
    public function __construct(Player $player)
    {
        $this->player = $player;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        return [new SetManaAvailable($this->player, $this->player->maximumMana())];
    }
}