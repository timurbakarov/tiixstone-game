<?php

namespace Tiixstone\Block;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Effect;
use Tiixstone\Card\Hero;
use Tiixstone\Card\Character;
use Tiixstone\Card\Attackable;

/**
 * Class RecalculateBuffs
 * @package Tiixstone\Block
 */
class RecalculateBuffs extends Block
{
    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        $allCards = $this->allCards($game);

        $effects = array_merge($game->effects->effects(), $game->effects->auras());

        foreach($allCards as $card) {
            if($card instanceof Attackable) {
                $card->attack->buff->set(0);
                $card->setAttackCondition($card->defaultAttackCondition());
            }

            if($card instanceof Character) {
                $card->health->buff->set(0);
                $card->flushEffects();
            }

            if($card instanceof Card\Weapon) {
                $card->durability->buff->set(0);
            }

            $card->cost->buff->set(0);

            foreach($effects as $effect) {
                $this->calculateTargetable($game, $effect, $card);

                $this->calculateAttack($game, $effect, $card);

                $this->calculateDurability($game, $effect, $card);

                $this->calculateHealth($game, $effect, $card);

                $this->calculateManaCost($game, $effect, $card);

                $this->calculateAttackCondition($game, $effect, $card);
            }

            if($card instanceof Hero) {
                if($card->hasWeapon()) {
                    $card->attack->buff->increase($card->weapon->attack->total());
                }
            }
        }

        $this->calculateSpellDamage($game);

        return [];
    }

    /**
     * @param Game $game
     * @param Effect $effect
     * @param Card $card
     * @return $this
     */
    private function calculateAttack(Game $game, Effect $effect, Card $card)
    {
        if($card instanceof Attackable) {
            if ($effect instanceof Effect\Changer\AttackChanger) {
                $card->attack->buff->set($effect->changeAttack($game, $card, $card->attack->buff->get()));
            }
        }

        return $this;
    }

    /**
     * @param Game $game
     * @param Effect $effect
     * @param Card $card
     * @return $this
     */
    private function calculateDurability(Game $game, Effect $effect, Card $card)
    {
        if($card instanceof Card\Weapon) {
            if ($effect instanceof Effect\Changer\DurabilityChanger) {
                $card->durability->buff->set($effect->changeAttack($game, $card, $card->durability->buff->get()));
            }
        }

        return $this;
    }

    /**
     * @param Game $game
     * @param Effect $effect
     * @param Card $card
     * @return $this
     */
    private function calculateHealth(Game $game, Effect $effect, Card $card)
    {
        if($card instanceof Character) {
            if ($effect instanceof Effect\Changer\HealthChanger) {
                $card->health->buff->set($effect->changeHealth($game, $card, $card->health->buff->get()));
            }
        }

        return $this;
    }

    /**
     * @param Game $game
     * @param Effect $effect
     * @param Card $card
     * @return $this
     */
    private function calculateManaCost(Game $game, Effect $effect, Card $card)
    {
        if($card instanceof Card) {
            if ($effect instanceof Effect\Changer\ManaCostChanger) {
                $card->cost->buff->set($effect->changeManaCost($game, $card, $card->cost->buff->get()));
            }
        }

        return $this;
    }

    /**
     * @param Game $game
     * @param Card $card
     */
    private function calculateAttackCondition(Game $game, Effect $effect, Card $card)
    {
        if($card instanceof Attackable) {
            if ($effect instanceof Effect\Changer\AttackConditionChanger) {
                if($effect->isTarget($game, $card)) {
                    $attackCondition = $effect->changeAttackCondition($game, $card);

                    if($attackCondition) {
                        $card->setAttackCondition($attackCondition);
                    }
                }
            }
        }
    }

    /**
     * @param Game $game
     * @param Card $card
     */
    private function calculateTargetable(Game $game, Effect $effect, Card $card)
    {
        if ($effect instanceof Effect\Targetable) {
            $effect->apply($game, $card);
        }
    }

    /**
     * @param Game $game
     */
    private function calculateSpellDamage(Game $game)
    {
        $player1SpellDamage = 0;
        $player2SpellDamage = 0;

        foreach($game->effects->effects() as $effect) {
            if ($effect instanceof Effect\SpellDamageEffect) {
                $player1SpellDamage += $effect->spellDamage($game->player1);
                $player2SpellDamage += $effect->spellDamage($game->player2);
            }
        }

        $game->spellDamageTracker->set($game->player1, $player1SpellDamage);
        $game->spellDamageTracker->set($game->player2, $player2SpellDamage);
    }

    /**
     * @param Game $game
     * @return array
     */
    private function allCards(Game $game)
    {
        $cards = array_merge(
            $game->player1->hand->all(),
            $game->player1->board->all(),
            $game->player1->deck->all(),
            $game->player2->hand->all(),
            $game->player2->board->all(),
            $game->player2->deck->all()
        );

        if($game->player1->hero->hasWeapon()) {
            $cards[] = $game->player1->hero->weapon;
        }

        if($game->player2->hero->hasWeapon()) {
            $cards[] = $game->player2->hero->weapon;
        }

        $cards = array_merge($cards, [
            $game->player1->hero,
            $game->player1->hero->power,
            $game->player2->hero,
            $game->player2->hero->power,
        ]);

        return $cards;
    }
}