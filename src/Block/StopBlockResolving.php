<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;

class StopBlockResolving extends Block
{
    /**
     * @var \Closure
     */
    private $closure;

    /**
     * @param \Closure $closure
     */
    public function __construct(\Closure $closure)
    {
        $this->closure = $closure;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game): array
    {
        $game->resolver->stopBlockResolving();

        $game->resolver->setStopBlockResolvingClosure($this->closure);

        return [];
    }
}