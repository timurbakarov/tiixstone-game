<?php

namespace Tiixstone\Block;

use Tiixstone\Card;
use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Character;
use Tiixstone\Event\AfterTakeDamage;
use Tiixstone\Event\BeforeTakeDamage;

/**
 * Class TakeDamage
 * @package Tiixstone\Block
 */
class TakeDamage extends Block
{
    /**
     * @var Character
     */
    private $character;

    /**
     * @var int
     */
    private $amount;

    /**
     * @var Card
     */
    private $source;

    /**
     * TakeDamage constructor.
     * @param Character $character
     * @param int $amount
     * @param Card|NULL $source
     */
    public function __construct(Character $character, int $amount, Card $source = null)
    {
        $this->character = $character;
        $this->amount = $amount;
        $this->source = $source;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        $blocks[] = new ClosureBlock(function() use($game) {
            $game->logBlock('take-damage', ['char' => $this->character, 'damage' => $this->amount]);

            return [];
        });

        if($this->amount == 0) {
            return $blocks;
        }

        $blocks[] = new Trigger(new BeforeTakeDamage($this->character, $this->amount));

        $amount = $this->amount;

        if($this->character->armor->get() > 0) {
            if($this->character->armor->get() >= $amount) {
                $blocks[] = new ReduceArmor($this->character, $amount);
            } else {
                $blocks[] = new ReduceArmor($this->character, $this->character->armor->get());
                $blocks[] = new ReduceHealth($this->character, $amount - $this->character->armor->get());
            }
        } else {
            $blocks[] = new ReduceHealth($this->character, $amount);
        }

        $blocks[] = new Trigger(new AfterTakeDamage($this->character, $amount, $this->source));

        return $blocks;
    }

    /**
     * @return Character
     */
    public function character()
    {
        return $this->character;
    }

    /**
     * @return int
     */
    public function amount()
    {
        return $this->amount;
    }

    /**
     * @return NULL|Card
     */
    public function source()
    {
        return $this->source;
    }
}