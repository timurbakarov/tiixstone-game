<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;
use Tiixstone\Card\Minion;

/**
 * Class ReturnMinionToHand
 * @package Tiixstone\Block
 */
class ReturnMinionToHand extends Block
{
    /**
     * @var Minion
     */
    private $minion;

    /**
     * ReturnMinionToHand constructor.
     * @param Minion $minion
     */
    public function __construct(Minion $minion)
    {
        $this->minion = $minion;
    }

    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game): array
    {
        $game->logBlock('return-minion-to-hand');

        $player = $this->minion->getPlayer();

        if($player->hand->count() >= $game->settings->maxCardsInHand()) {
            return [new MarkDestroyed($this->minion)];
        } else {
            $this->minion->resetStats();

            return [
                new Block\Battlefield\RemoveFromBattlefield($this->minion),
                new PutCardInHand($player, $this->minion),
            ];
        }
    }
}