<?php

namespace Tiixstone\Block;

use Tiixstone\Game;
use Tiixstone\Block;

/**
 * Class EmptyBlock
 * @package Tiixstone\Block
 */
class EmptyBlock extends Block
{
    /**
     * @param Game $game
     * @return array
     */
    public function run(Game $game) : array
    {
        return [];
    }
}