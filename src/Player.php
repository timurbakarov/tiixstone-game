<?php declare(strict_types=1);

namespace Tiixstone;

use Ramsey\Uuid\Uuid;
use Tiixstone\Card\Collection\Secrets;
use Tiixstone\Card\Hero;
use Tiixstone\Card\Collection\Deck;
use Tiixstone\Card\Collection\Hand;
use Tiixstone\Card\Collection\Board;
use Tiixstone\Card\Collection\Graveyard;

class Player implements Identifiable
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var int
     */
    protected $fatigue = 1;

    /**
     * @var int
     */
    protected $availableMana = 0;

    /**
     * @var int
     */
    protected $maximumMana = 0;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Hero
     */
    public $hero;

    /**
     * @var Hand
     */
    public $hand;

    /**
     * @var Deck
     */
    public $deck;

    /**
     * @var Battlefield
     */
    public $board;

    /**
     * @var Secrets
     */
    public $secrets;

    /**
     * @var
     */
    public $graveyard;

    /**
     * Player constructor.
     * @param string $name
     * @param Hero $hero
     * @param Deck $deck
     * @param Hand $hand
     * @param Board $board
     */
    public function __construct(string $name, Hero $hero, Deck $deck, Hand $hand, Board $board)
    {
        $this->id = Uuid::uuid4()->toString();
        $this->name = $name;
        $this->hero = $hero;
        $this->deck = $deck;
        $this->hand = $hand;
        $this->board = $board;
        $this->secrets = new Secrets();
        $this->graveyard = new Graveyard();

        $this->hero->setPlayer($this);
    }

    /**
     * @return string
     */
    public function id() : string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name() : string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function availableMana() : int
    {
        return $this->availableMana;
    }

    /**
     * @param int $availableMana
     * @return Player
     */
    public function setAvailableMana(int $availableMana) : self
    {
        $this->availableMana = $availableMana;

        return $this;
    }

    /**
     * @return Player
     */
    public function incrementAvailableMana() : self
    {
        $this->setAvailableMana($this->availableMana + 1);

        return $this;
    }

    /**
     * @return Player
     */
    public function reduceAvailableMana(int $amount) : self
    {
        $this->setAvailableMana($this->availableMana - $amount);

        return $this;
    }

    /**
     * @return int
     */
    public function maximumMana() : int
    {
        return $this->maximumMana;
    }

    /**
     * @param int $maximumMana
     * @return Player
     */
    public function setMaximumMana(int $maximumMana) : self
    {
        $this->maximumMana = $maximumMana;

        return $this;
    }

    /**
     * @return Player
     */
    public function incrementMaximumMana() : self
    {
        $this->setMaximumMana($this->maximumMana + 1);

        return $this;
    }

    /**
     * @return int
     */
    public function fatigue()
    {
        return $this->fatigue;
    }

    /**
     * @return $this
     */
    public function incrementFatigue() : self
    {
        $this->fatigue = $this->fatigue + 1;

        return $this;
    }

    /**
     * @param Game $game
     * @return bool
     */
    public function isCurrent(Game $game) : bool
    {
        return $this->id() == $game->currentPlayer()->id();
    }

    /**
     * @param Game $game
     * @return Player
     */
    public function getOpponent(Game $game) : Player
    {
        return $this->id() == $game->player1->id() ? $game->player2 : $game->player1;
    }
}